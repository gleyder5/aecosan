package es.grupoavalon.aecosan.connector.epago.ws;

import javax.jws.WebService;

import epago.gob.es.schemas.DefaultDatosNRCIn;
import epago.gob.es.schemas.DefaultDatosNRCOut;
import epago.gob.es.schemas.DefaultDatosPagoIn;
import epago.gob.es.schemas.DefaultDatosPagoOut;
import epago.gob.es.schemas.DefaultDatosPagoRegistroOut;
import epago.gob.es.schemas.Map;
import epago.gob.es.schemas.ObjectFactory;
import epago.gob.es.schemas.PPServiceInterface;

@WebService(serviceName = "PPServiceInterfaceService",
			targetNamespace = "http://es.gob.ePago/schemas",
			endpointInterface = "epago.gob.es.schemas.PPServiceInterface",
			portName = "PPServiceInterfaceSoap11")
public class PPServiceImpl implements PPServiceInterface {

	@Override
	public DefaultDatosNRCOut verificarNRC(DefaultDatosNRCIn datosNRCIn, int idOrganismo) {
		// NO SE NECESITA
		return null;
	}

	@Override
	public DefaultDatosPagoRegistroOut hacerPagoYRegistro(DefaultDatosPagoIn datosPagoIn, Map almacen,
			int idOrganismo) {
		// NO SE NECESITA
		return null;
	}

	@Override
	public DefaultDatosPagoOut consultarPago(DefaultDatosPagoIn datosPagoIn, int idOrganismo) {
		// NO SE NECESITA
		return null;
	}

	@Override
	public String getJustificante(String modelo, String codigoTasa, int idOrganismo) {
		return "JUSTIFICANTE";
	}

	@Override
	public DefaultDatosPagoOut hacerPago(DefaultDatosPagoIn datosPagoIn, int idOrganismo) {
		ObjectFactory factory = new ObjectFactory();
		DefaultDatosPagoOut retVal = factory.createDefaultDatosPagoOut();
		retVal.setApoderado(datosPagoIn.isApoderado());
		retVal.setNRC("NRC_VALUE");
		retVal.setFueCorrecto(true);
		retVal.setImporteOperacion(datosPagoIn.getImporte());
		return retVal;
	}

}
