package es.grupoavalon.aecosan.connector.epago;

import java.util.GregorianCalendar;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.epago.dto.DatosPagoInDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentRequestDTO;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorException;
import es.grupoavalon.aecosan.connector.epago.impl.EpagoConnectorApiImpl;
import es.grupoavalon.aecosan.connector.epago.testsetup.TestServer;
import es.grupoavalon.aecosan.connector.epago.util.DocumentType;
import es.grupoavalon.aecosan.connector.epago.util.PaymentType;

public class EpagoConnectorApiTest {
	private EpagoConnectorApi connector;
	private static final Logger logger = LoggerFactory.getLogger(EpagoConnectorApiTest.class);
	// private static final String TEST_WS_URL =
	// "http://localhost:9000/epago_connector?wsdl";
	private static final String TEST_WS_URL = "https://pre-epago.redsara.es/ppiban/pasarela.wsdl ";
	private static TestServer testServer = new TestServer();

	private static final String TEST_DATA_CODIGO_TASA = "001";
	private static final String TEST_DATA_MODELO = "790";
	private static final int TEST_DATA_ID_ORGANISMO = 24681;
	private static final String TEST_DATA_CCC = "ES5421040000119064547550";
	private static final String TEST_DATA_APELLIDO1 = "LOPEZ";
	private static final String TEST_DATA_APELLIDO2 = "RUIZ";
	private static final String TEST_DATA_CODIGOBANCO = "2104";
	private static final String TEST_DATA_DOCUMENTO_OBLIGADO = "50179623V";
	private static final GregorianCalendar TEST_DATA_FECHA_CADUCIDAD_TARJETA = new GregorianCalendar(2019, 5, 17, 23, 59);
	private static final float TEST_DATA_IMPORTE = 0.01f;
	private static final String TEST_DATA_NOMBRE = "JOSE";
	private static final String TEST_DATA_NUMERO_TARJETA = "4966510088840006";
	private static final DocumentType TEST_DATA_TIPO_DOCUMENTO_OBLIGADO = DocumentType.NIF;
	private static final String TEST_DATA_FIRMA1 = "FIRMA 1";
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@BeforeClass
	public static void beforeClass() {
		// testServer.serverCreate(new PPServiceImpl(), PPServiceImpl.class,
		// "epago_connector");
	}	

	@AfterClass
	public static void afterClass() {
		// testServer.serverDestroy();
	}

	@Before 
	public void before() throws Exception {
		connector = new EpagoConnectorApiImpl();
	}
	
	@Test
	public void testSendPaymentCCC() throws Exception {
			String justifier = getJustifier();
		EpagoSendPaymentRequestDTO dto = getRightPaymentDTO(justifier, true);
			connector.sendPayment(dto);
	}
	
	@Test
	public void testSendPaymentCard() throws Exception {
		String justifier = getJustifier();
		EpagoSendPaymentRequestDTO dto = getRightPaymentDTO(justifier, false);
		System.out.println(connector.sendPayment(dto));
	}

	@Test
	public void testGetJustifierAndOriginSign() throws Exception {
		try {
			EpagoGetJustifierAndSignOriginRequestDTO request = new EpagoGetJustifierAndSignOriginRequestDTO();
			request.setGetJustifierRequestDTO(getJustifierRequestDTO());
			request.setGetSignOriginRequestDTO(getSignOriginRequestDTO("", false, true));
			EpagoGetJustifierAndSignOriginResponseDTO response = connector.getJustifierAndSignOrigin(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testgetSignOrigin() throws Exception {
		String justifier = getJustifier();
		EpagoSendPaymentRequestDTO dto = getRightPaymentDTO(justifier, true);
		connector.sendPayment(dto);
	}

	private String getJustifier() 
		throws EpagoConnectorException 
	{
		EpagoGetJustifierResponseDTO response = connector.getJustifier(getJustifierRequestDTO());
		return response.getJustificante();
	}

	private EpagoGetJustifierRequestDTO getJustifierRequestDTO() {
		EpagoGetJustifierRequestDTO retVal = new EpagoGetJustifierRequestDTO();
		retVal.setUrl(TEST_WS_URL);
		retVal.setCodigoTasa(TEST_DATA_CODIGO_TASA);
		retVal.setIdOrganismo(TEST_DATA_ID_ORGANISMO);
		retVal.setModelo(TEST_DATA_MODELO);
		return retVal;
	}

	private EpagoSendPaymentRequestDTO getRightPaymentDTO(String justifier, boolean isCCC) 
		throws Exception
	{
		EpagoSendPaymentRequestDTO retVal = new EpagoSendPaymentRequestDTO();
		retVal.setUrl(TEST_WS_URL);
		retVal.setIdOrganismo(TEST_DATA_ID_ORGANISMO);
		retVal.setDatosPagoIn(getDatosPagoInDto(justifier, isCCC));
		return retVal;
	}

	private DatosPagoInDTO getDatosPagoInDto(String justifier, boolean isCCC) 
		throws EpagoConnectorException
	{
		DatosPagoInDTO retVal = new DatosPagoInDTO();
		if (isCCC) {
			retVal.setCcc(TEST_DATA_CCC);
			retVal.setTipoCargo(PaymentType.ACCOUNT_PAYMENT);
		} else {
			retVal.setFechaCaducidadTarjeta(TEST_DATA_FECHA_CADUCIDAD_TARJETA);
			retVal.setNumeroTarjeta(TEST_DATA_NUMERO_TARJETA);
			retVal.setTipoCargo(PaymentType.CREDIT_CARD);
		}
		retVal.setApellido1(TEST_DATA_APELLIDO1);
		retVal.setApellido2(TEST_DATA_APELLIDO2);
		retVal.setNombre(TEST_DATA_NOMBRE);
		retVal.setCodigoBanco(TEST_DATA_CODIGOBANCO);
		retVal.setCodigoTasa(TEST_DATA_CODIGO_TASA);
		retVal.setDocumentoObligado(TEST_DATA_DOCUMENTO_OBLIGADO);
		retVal.setFirma1(TEST_DATA_FIRMA1);
		retVal.setImporte(TEST_DATA_IMPORTE);
		retVal.setOrigenFirma(getOrigenFirma(justifier, isCCC, false));
		retVal.setTipoDocumentoObligado(TEST_DATA_TIPO_DOCUMENTO_OBLIGADO);
		retVal.setJustificante(justifier);
		
		return retVal;
	}
	
	private String getOrigenFirma(
			String justifier, 
			boolean isCCC, 
			boolean apoderado)
		throws EpagoConnectorException
	{
		EpagoGetSignOriginRequestDTO request = getSignOriginRequestDTO(justifier, isCCC, apoderado);
		EpagoGetSignOriginResponseDTO response = connector.getSignOrigin(request);
		return response.getOrigenFirma();
	}

	private EpagoGetSignOriginRequestDTO getSignOriginRequestDTO(
			String justifier, 
			boolean isCCC, 
			boolean apoderado) 
	{
		EpagoGetSignOriginRequestDTO retVal = new EpagoGetSignOriginRequestDTO();
		retVal.setJustificante(justifier);
		retVal.setApoderado(apoderado);
		if (isCCC) {
			retVal.setCcc(TEST_DATA_CCC);
		} else {
			retVal.setNumeroTarjeta(TEST_DATA_NUMERO_TARJETA);
			retVal.setCodigoBanco(TEST_DATA_CODIGOBANCO);
			retVal.setFechaCaducidadTarjeta(TEST_DATA_FECHA_CADUCIDAD_TARJETA);
		}
		retVal.setDocumentoObligado(TEST_DATA_DOCUMENTO_OBLIGADO);
		retVal.setImporte(TEST_DATA_IMPORTE);
		return retVal;
	}
	
	

}
