
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the epago.gob.es.schemas package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: epago.gob.es.schemas
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HacerPagoResponse }
     * 
     */
    public HacerPagoResponse createHacerPagoResponse() {
        return new HacerPagoResponse();
    }

    /**
     * Create an instance of {@link DefaultDatosPagoOut }
     * 
     */
    public DefaultDatosPagoOut createDefaultDatosPagoOut() {
        return new DefaultDatosPagoOut();
    }

    /**
     * Create an instance of {@link HacerPagoYRegistroResponse }
     * 
     */
    public HacerPagoYRegistroResponse createHacerPagoYRegistroResponse() {
        return new HacerPagoYRegistroResponse();
    }

    /**
     * Create an instance of {@link DefaultDatosPagoRegistroOut }
     * 
     */
    public DefaultDatosPagoRegistroOut createDefaultDatosPagoRegistroOut() {
        return new DefaultDatosPagoRegistroOut();
    }

    /**
     * Create an instance of {@link VerificarNRCResponse }
     * 
     */
    public VerificarNRCResponse createVerificarNRCResponse() {
        return new VerificarNRCResponse();
    }

    /**
     * Create an instance of {@link DefaultDatosNRCOut }
     * 
     */
    public DefaultDatosNRCOut createDefaultDatosNRCOut() {
        return new DefaultDatosNRCOut();
    }

    /**
     * Create an instance of {@link ConsultarPago }
     * 
     */
    public ConsultarPago createConsultarPago() {
        return new ConsultarPago();
    }

    /**
     * Create an instance of {@link DefaultDatosPagoIn }
     * 
     */
    public DefaultDatosPagoIn createDefaultDatosPagoIn() {
        return new DefaultDatosPagoIn();
    }

    /**
     * Create an instance of {@link ConsultarPagoResponse }
     * 
     */
    public ConsultarPagoResponse createConsultarPagoResponse() {
        return new ConsultarPagoResponse();
    }

    /**
     * Create an instance of {@link HacerPago }
     * 
     */
    public HacerPago createHacerPago() {
        return new HacerPago();
    }

    /**
     * Create an instance of {@link HacerPagoYRegistro }
     * 
     */
    public HacerPagoYRegistro createHacerPagoYRegistro() {
        return new HacerPagoYRegistro();
    }

    /**
     * Create an instance of {@link Map }
     * 
     */
    public Map createMap() {
        return new Map();
    }

    /**
     * Create an instance of {@link GetJustificanteResponse }
     * 
     */
    public GetJustificanteResponse createGetJustificanteResponse() {
        return new GetJustificanteResponse();
    }

    /**
     * Create an instance of {@link VerificarNRC }
     * 
     */
    public VerificarNRC createVerificarNRC() {
        return new VerificarNRC();
    }

    /**
     * Create an instance of {@link DefaultDatosNRCIn }
     * 
     */
    public DefaultDatosNRCIn createDefaultDatosNRCIn() {
        return new DefaultDatosNRCIn();
    }

    /**
     * Create an instance of {@link GetJustificante }
     * 
     */
    public GetJustificante createGetJustificante() {
        return new GetJustificante();
    }

    /**
     * Create an instance of {@link MapItem }
     * 
     */
    public MapItem createMapItem() {
        return new MapItem();
    }

    /**
     * Create an instance of {@link DefaultDatosRegistroOut }
     * 
     */
    public DefaultDatosRegistroOut createDefaultDatosRegistroOut() {
        return new DefaultDatosRegistroOut();
    }

}
