
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="datosPagoIn" type="{http://es.gob.ePago/schemas}DefaultDatosPagoIn"/>
 *         &lt;element name="idOrganismo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datosPagoIn",
    "idOrganismo"
})
@XmlRootElement(name = "hacerPago")
public class HacerPago {

    @XmlElement(required = true)
    protected DefaultDatosPagoIn datosPagoIn;
    protected int idOrganismo;

    /**
     * Gets the value of the datosPagoIn property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultDatosPagoIn }
     *     
     */
    public DefaultDatosPagoIn getDatosPagoIn() {
        return datosPagoIn;
    }

    /**
     * Sets the value of the datosPagoIn property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultDatosPagoIn }
     *     
     */
    public void setDatosPagoIn(DefaultDatosPagoIn value) {
        this.datosPagoIn = value;
    }

    /**
     * Gets the value of the idOrganismo property.
     * 
     */
    public int getIdOrganismo() {
        return idOrganismo;
    }

    /**
     * Sets the value of the idOrganismo property.
     * 
     */
    public void setIdOrganismo(int value) {
        this.idOrganismo = value;
    }

}
