
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoTasa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idOrganismo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "modelo",
    "codigoTasa",
    "idOrganismo"
})
@XmlRootElement(name = "getJustificante")
public class GetJustificante {

    @XmlElement(required = true)
    protected String modelo;
    @XmlElement(required = true)
    protected String codigoTasa;
    protected int idOrganismo;

    /**
     * Gets the value of the modelo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Gets the value of the codigoTasa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTasa() {
        return codigoTasa;
    }

    /**
     * Sets the value of the codigoTasa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTasa(String value) {
        this.codigoTasa = value;
    }

    /**
     * Gets the value of the idOrganismo property.
     * 
     */
    public int getIdOrganismo() {
        return idOrganismo;
    }

    /**
     * Sets the value of the idOrganismo property.
     * 
     */
    public void setIdOrganismo(int value) {
        this.idOrganismo = value;
    }

}
