
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DefaultDatosPagoIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultDatosPagoIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CCC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apellido1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apellido2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="certificado1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="certificado2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoBanco" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoTasa" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documentoObligado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documentoPagador" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaCaducidadTarjeta" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="firma1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="firma2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="hashOrigenFirma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="importe" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="justificante" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numeroTarjeta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origenFirma" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoCargo" type="{http://es.gob.ePago/schemas}TiposCargo"/>
 *         &lt;element name="tipoDocumentoObligado" type="{http://es.gob.ePago/schemas}TiposDocumento"/>
 *         &lt;element name="tipoDocumentoPagador" type="{http://es.gob.ePago/schemas}TiposDocumento"/>
 *         &lt;element name="apoderado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultDatosPagoIn", propOrder = {
    "ccc",
    "apellido1",
    "apellido2",
    "certificado1",
    "certificado2",
    "codigoBanco",
    "codigoTasa",
    "documentoObligado",
    "documentoPagador",
    "fechaCaducidadTarjeta",
    "firma1",
    "firma2",
    "hashOrigenFirma",
    "importe",
    "justificante",
    "nombre",
    "numeroTarjeta",
    "origenFirma",
    "tipoCargo",
    "tipoDocumentoObligado",
    "tipoDocumentoPagador",
    "apoderado"
})
public class DefaultDatosPagoIn {

    @XmlElement(name = "CCC", required = true, nillable = true)
    protected String ccc;
    @XmlElement(required = true, nillable = true)
    protected String apellido1;
    @XmlElement(required = true, nillable = true)
    protected String apellido2;
    @XmlElement(required = true, nillable = true)
    protected String certificado1;
    @XmlElement(required = true, nillable = true)
    protected String certificado2;
    @XmlElement(required = true, nillable = true)
    protected String codigoBanco;
    @XmlElement(required = true, nillable = true)
    protected String codigoTasa;
    @XmlElement(required = true, nillable = true)
    protected String documentoObligado;
    @XmlElement(required = true, nillable = true)
    protected String documentoPagador;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaCaducidadTarjeta;
    @XmlElement(required = true, nillable = true)
    protected String firma1;
    @XmlElement(required = true, nillable = true)
    protected String firma2;
    @XmlElement(required = true, nillable = true)
    protected String hashOrigenFirma;
    protected double importe;
    @XmlElement(required = true, nillable = true)
    protected String justificante;
    @XmlElement(required = true, nillable = true)
    protected String nombre;
    @XmlElement(required = true, nillable = true)
    protected String numeroTarjeta;
    @XmlElement(required = true, nillable = true)
    protected String origenFirma;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tipoCargo;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tipoDocumentoObligado;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tipoDocumentoPagador;
    protected boolean apoderado;

    /**
     * Gets the value of the ccc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCCC() {
        return ccc;
    }

    /**
     * Sets the value of the ccc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCCC(String value) {
        this.ccc = value;
    }

    /**
     * Gets the value of the apellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Sets the value of the apellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellido1(String value) {
        this.apellido1 = value;
    }

    /**
     * Gets the value of the apellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Sets the value of the apellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellido2(String value) {
        this.apellido2 = value;
    }

    /**
     * Gets the value of the certificado1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificado1() {
        return certificado1;
    }

    /**
     * Sets the value of the certificado1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificado1(String value) {
        this.certificado1 = value;
    }

    /**
     * Gets the value of the certificado2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCertificado2() {
        return certificado2;
    }

    /**
     * Sets the value of the certificado2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCertificado2(String value) {
        this.certificado2 = value;
    }

    /**
     * Gets the value of the codigoBanco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBanco() {
        return codigoBanco;
    }

    /**
     * Sets the value of the codigoBanco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBanco(String value) {
        this.codigoBanco = value;
    }

    /**
     * Gets the value of the codigoTasa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTasa() {
        return codigoTasa;
    }

    /**
     * Sets the value of the codigoTasa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTasa(String value) {
        this.codigoTasa = value;
    }

    /**
     * Gets the value of the documentoObligado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoObligado() {
        return documentoObligado;
    }

    /**
     * Sets the value of the documentoObligado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoObligado(String value) {
        this.documentoObligado = value;
    }

    /**
     * Gets the value of the documentoPagador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoPagador() {
        return documentoPagador;
    }

    /**
     * Sets the value of the documentoPagador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoPagador(String value) {
        this.documentoPagador = value;
    }

    /**
     * Gets the value of the fechaCaducidadTarjeta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCaducidadTarjeta() {
        return fechaCaducidadTarjeta;
    }

    /**
     * Sets the value of the fechaCaducidadTarjeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCaducidadTarjeta(XMLGregorianCalendar value) {
        this.fechaCaducidadTarjeta = value;
    }

    /**
     * Gets the value of the firma1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirma1() {
        return firma1;
    }

    /**
     * Sets the value of the firma1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirma1(String value) {
        this.firma1 = value;
    }

    /**
     * Gets the value of the firma2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirma2() {
        return firma2;
    }

    /**
     * Sets the value of the firma2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirma2(String value) {
        this.firma2 = value;
    }

    /**
     * Gets the value of the hashOrigenFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHashOrigenFirma() {
        return hashOrigenFirma;
    }

    /**
     * Sets the value of the hashOrigenFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHashOrigenFirma(String value) {
        this.hashOrigenFirma = value;
    }

    /**
     * Gets the value of the importe property.
     * 
     */
    public double getImporte() {
        return importe;
    }

    /**
     * Sets the value of the importe property.
     * 
     */
    public void setImporte(double value) {
        this.importe = value;
    }

    /**
     * Gets the value of the justificante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJustificante() {
        return justificante;
    }

    /**
     * Sets the value of the justificante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJustificante(String value) {
        this.justificante = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the numeroTarjeta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    /**
     * Sets the value of the numeroTarjeta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTarjeta(String value) {
        this.numeroTarjeta = value;
    }

    /**
     * Gets the value of the origenFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigenFirma() {
        return origenFirma;
    }

    /**
     * Sets the value of the origenFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigenFirma(String value) {
        this.origenFirma = value;
    }

    /**
     * Gets the value of the tipoCargo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoCargo() {
        return tipoCargo;
    }

    /**
     * Sets the value of the tipoCargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoCargo(Integer value) {
        this.tipoCargo = value;
    }

    /**
     * Gets the value of the tipoDocumentoObligado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoDocumentoObligado() {
        return tipoDocumentoObligado;
    }

    /**
     * Sets the value of the tipoDocumentoObligado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoDocumentoObligado(Integer value) {
        this.tipoDocumentoObligado = value;
    }

    /**
     * Gets the value of the tipoDocumentoPagador property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoDocumentoPagador() {
        return tipoDocumentoPagador;
    }

    /**
     * Sets the value of the tipoDocumentoPagador property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoDocumentoPagador(Integer value) {
        this.tipoDocumentoPagador = value;
    }

    /**
     * Gets the value of the apoderado property.
     * 
     */
    public boolean isApoderado() {
        return apoderado;
    }

    /**
     * Sets the value of the apoderado property.
     * 
     */
    public void setApoderado(boolean value) {
        this.apoderado = value;
    }
}
