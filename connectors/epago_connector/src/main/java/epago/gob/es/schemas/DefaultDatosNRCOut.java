
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DefaultDatosNRCOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultDatosNRCOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCodeTexto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCodeTextoUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="errorOrigenDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaProceso" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fueCorrecto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="registroAEAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultDatosNRCOut", propOrder = {
    "errorCode",
    "errorCodeTexto",
    "errorCodeTextoUsuario",
    "errorDescription",
    "errorOrigen",
    "errorOrigenDescription",
    "fechaProceso",
    "fueCorrecto",
    "referencia",
    "registroAEAT"
})
public class DefaultDatosNRCOut {

    @XmlElement(required = true, nillable = true)
    protected String errorCode;
    @XmlElement(required = true, nillable = true)
    protected String errorCodeTexto;
    @XmlElement(required = true, nillable = true)
    protected String errorCodeTextoUsuario;
    @XmlElement(required = true, nillable = true)
    protected String errorDescription;
    protected int errorOrigen;
    @XmlElement(required = true, nillable = true)
    protected String errorOrigenDescription;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaProceso;
    protected boolean fueCorrecto;
    @XmlElement(required = true, nillable = true)
    protected String referencia;
    @XmlElement(required = true, nillable = true)
    protected String registroAEAT;

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorCodeTexto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCodeTexto() {
        return errorCodeTexto;
    }

    /**
     * Sets the value of the errorCodeTexto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCodeTexto(String value) {
        this.errorCodeTexto = value;
    }

    /**
     * Gets the value of the errorCodeTextoUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCodeTextoUsuario() {
        return errorCodeTextoUsuario;
    }

    /**
     * Sets the value of the errorCodeTextoUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCodeTextoUsuario(String value) {
        this.errorCodeTextoUsuario = value;
    }

    /**
     * Gets the value of the errorDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Sets the value of the errorDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorDescription(String value) {
        this.errorDescription = value;
    }

    /**
     * Gets the value of the errorOrigen property.
     * 
     */
    public int getErrorOrigen() {
        return errorOrigen;
    }

    /**
     * Sets the value of the errorOrigen property.
     * 
     */
    public void setErrorOrigen(int value) {
        this.errorOrigen = value;
    }

    /**
     * Gets the value of the errorOrigenDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorOrigenDescription() {
        return errorOrigenDescription;
    }

    /**
     * Sets the value of the errorOrigenDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorOrigenDescription(String value) {
        this.errorOrigenDescription = value;
    }

    /**
     * Gets the value of the fechaProceso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaProceso() {
        return fechaProceso;
    }

    /**
     * Sets the value of the fechaProceso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaProceso(XMLGregorianCalendar value) {
        this.fechaProceso = value;
    }

    /**
     * Gets the value of the fueCorrecto property.
     * 
     */
    public boolean isFueCorrecto() {
        return fueCorrecto;
    }

    /**
     * Sets the value of the fueCorrecto property.
     * 
     */
    public void setFueCorrecto(boolean value) {
        this.fueCorrecto = value;
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    /**
     * Gets the value of the registroAEAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroAEAT() {
        return registroAEAT;
    }

    /**
     * Sets the value of the registroAEAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroAEAT(String value) {
        this.registroAEAT = value;
    }

}
