
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="consultarPagoReturn" type="{http://es.gob.ePago/schemas}DefaultDatosPagoOut"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarPagoReturn"
})
@XmlRootElement(name = "consultarPagoResponse")
public class ConsultarPagoResponse {

    @XmlElement(required = true)
    protected DefaultDatosPagoOut consultarPagoReturn;

    /**
     * Gets the value of the consultarPagoReturn property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultDatosPagoOut }
     *     
     */
    public DefaultDatosPagoOut getConsultarPagoReturn() {
        return consultarPagoReturn;
    }

    /**
     * Sets the value of the consultarPagoReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultDatosPagoOut }
     *     
     */
    public void setConsultarPagoReturn(DefaultDatosPagoOut value) {
        this.consultarPagoReturn = value;
    }

}
