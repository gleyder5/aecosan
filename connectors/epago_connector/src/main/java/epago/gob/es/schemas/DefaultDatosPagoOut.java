
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DefaultDatosPagoOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultDatosPagoOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorOrigen" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fechaProceso" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="fueCorrecto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="importeOperacion" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="merchan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="referencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="registroAEAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCodeTexto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorCodeTextoUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorOrigenDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apoderado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultDatosPagoOut", propOrder = {
    "nrc",
    "errorCode",
    "errorDescription",
    "errorOrigen",
    "fechaOperacion",
    "fechaProceso",
    "fueCorrecto",
    "importeOperacion",
    "merchan",
    "referencia",
    "registroAEAT",
    "errorCodeTexto",
    "errorCodeTextoUsuario",
    "errorOrigenDescription",
    "apoderado"
})
public class DefaultDatosPagoOut {

    @XmlElement(name = "NRC", required = true, nillable = true)
    protected String nrc;
    @XmlElement(required = true, nillable = true)
    protected String errorCode;
    @XmlElement(required = true, nillable = true)
    protected String errorDescription;
    protected int errorOrigen;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaOperacion;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaProceso;
    protected boolean fueCorrecto;
    protected double importeOperacion;
    @XmlElement(required = true, nillable = true)
    protected String merchan;
    @XmlElement(required = true, nillable = true)
    protected String referencia;
    @XmlElement(required = true, nillable = true)
    protected String registroAEAT;
    @XmlElement(required = true, nillable = true)
    protected String errorCodeTexto;
    @XmlElement(required = true, nillable = true)
    protected String errorCodeTextoUsuario;
    @XmlElement(required = true, nillable = true)
    protected String errorOrigenDescription;
    protected boolean apoderado;

    /**
     * Gets the value of the nrc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRC() {
        return nrc;
    }

    /**
     * Sets the value of the nrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRC(String value) {
        this.nrc = value;
    }

    /**
     * Gets the value of the errorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the value of the errorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Gets the value of the errorDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Sets the value of the errorDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorDescription(String value) {
        this.errorDescription = value;
    }

    /**
     * Gets the value of the errorOrigen property.
     * 
     */
    public int getErrorOrigen() {
        return errorOrigen;
    }

    /**
     * Sets the value of the errorOrigen property.
     * 
     */
    public void setErrorOrigen(int value) {
        this.errorOrigen = value;
    }

    /**
     * Gets the value of the fechaOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Sets the value of the fechaOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaOperacion(XMLGregorianCalendar value) {
        this.fechaOperacion = value;
    }

    /**
     * Gets the value of the fechaProceso property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaProceso() {
        return fechaProceso;
    }

    /**
     * Sets the value of the fechaProceso property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaProceso(XMLGregorianCalendar value) {
        this.fechaProceso = value;
    }

    /**
     * Gets the value of the fueCorrecto property.
     * 
     */
    public boolean isFueCorrecto() {
        return fueCorrecto;
    }

    /**
     * Sets the value of the fueCorrecto property.
     * 
     */
    public void setFueCorrecto(boolean value) {
        this.fueCorrecto = value;
    }

    /**
     * Gets the value of the importeOperacion property.
     * 
     */
    public double getImporteOperacion() {
        return importeOperacion;
    }

    /**
     * Sets the value of the importeOperacion property.
     * 
     */
    public void setImporteOperacion(double value) {
        this.importeOperacion = value;
    }

    /**
     * Gets the value of the merchan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchan() {
        return merchan;
    }

    /**
     * Sets the value of the merchan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchan(String value) {
        this.merchan = value;
    }

    /**
     * Gets the value of the referencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Sets the value of the referencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencia(String value) {
        this.referencia = value;
    }

    /**
     * Gets the value of the registroAEAT property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroAEAT() {
        return registroAEAT;
    }

    /**
     * Sets the value of the registroAEAT property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroAEAT(String value) {
        this.registroAEAT = value;
    }

    /**
     * Gets the value of the errorCodeTexto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCodeTexto() {
        return errorCodeTexto;
    }

    /**
     * Sets the value of the errorCodeTexto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCodeTexto(String value) {
        this.errorCodeTexto = value;
    }

    /**
     * Gets the value of the errorCodeTextoUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCodeTextoUsuario() {
        return errorCodeTextoUsuario;
    }

    /**
     * Sets the value of the errorCodeTextoUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCodeTextoUsuario(String value) {
        this.errorCodeTextoUsuario = value;
    }

    /**
     * Gets the value of the errorOrigenDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorOrigenDescription() {
        return errorOrigenDescription;
    }

    /**
     * Sets the value of the errorOrigenDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorOrigenDescription(String value) {
        this.errorOrigenDescription = value;
    }

    /**
     * Gets the value of the apoderado property.
     * 
     */
    public boolean isApoderado() {
        return apoderado;
    }

    /**
     * Sets the value of the apoderado property.
     * 
     */
    public void setApoderado(boolean value) {
        this.apoderado = value;
    }

	@Override
	public String toString() {
		return "DefaultDatosPagoOut [nrc=" + nrc + ", errorCode=" + errorCode + ", errorDescription=" + errorDescription + ", errorOrigen=" + errorOrigen + ", fechaOperacion=" + fechaOperacion
				+ ", fechaProceso=" + fechaProceso + ", fueCorrecto=" + fueCorrecto + ", importeOperacion=" + importeOperacion + ", merchan=" + merchan + ", referencia=" + referencia
				+ ", registroAEAT=" + registroAEAT + ", errorCodeTexto=" + errorCodeTexto + ", errorCodeTextoUsuario=" + errorCodeTextoUsuario + ", errorOrigenDescription=" + errorOrigenDescription
				+ ", apoderado=" + apoderado + "]";
	}

}
