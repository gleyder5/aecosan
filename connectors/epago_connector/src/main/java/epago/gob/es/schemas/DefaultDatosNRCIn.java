
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DefaultDatosNRCIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultDatosNRCIn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NRC" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apellido1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="apellido2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codigoBanco" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documentoObligado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="documentoPagador" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaOperacion" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="importeOperacion" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocumentoObligado" type="{http://es.gob.ePago/schemas}TiposDocumento"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultDatosNRCIn", propOrder = {
    "nrc",
    "apellido1",
    "apellido2",
    "codigoBanco",
    "documentoObligado",
    "documentoPagador",
    "fechaOperacion",
    "importeOperacion",
    "nombre",
    "tipoDocumentoObligado"
})
public class DefaultDatosNRCIn {

    @XmlElement(name = "NRC", required = true, nillable = true)
    protected String nrc;
    @XmlElement(required = true, nillable = true)
    protected String apellido1;
    @XmlElement(required = true, nillable = true)
    protected String apellido2;
    @XmlElement(required = true, nillable = true)
    protected String codigoBanco;
    @XmlElement(required = true, nillable = true)
    protected String documentoObligado;
    @XmlElement(required = true, nillable = true)
    protected String documentoPagador;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaOperacion;
    protected double importeOperacion;
    @XmlElement(required = true, nillable = true)
    protected String nombre;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer tipoDocumentoObligado;

    /**
     * Gets the value of the nrc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNRC() {
        return nrc;
    }

    /**
     * Sets the value of the nrc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNRC(String value) {
        this.nrc = value;
    }

    /**
     * Gets the value of the apellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellido1() {
        return apellido1;
    }

    /**
     * Sets the value of the apellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellido1(String value) {
        this.apellido1 = value;
    }

    /**
     * Gets the value of the apellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApellido2() {
        return apellido2;
    }

    /**
     * Sets the value of the apellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApellido2(String value) {
        this.apellido2 = value;
    }

    /**
     * Gets the value of the codigoBanco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoBanco() {
        return codigoBanco;
    }

    /**
     * Sets the value of the codigoBanco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoBanco(String value) {
        this.codigoBanco = value;
    }

    /**
     * Gets the value of the documentoObligado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoObligado() {
        return documentoObligado;
    }

    /**
     * Sets the value of the documentoObligado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoObligado(String value) {
        this.documentoObligado = value;
    }

    /**
     * Gets the value of the documentoPagador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoPagador() {
        return documentoPagador;
    }

    /**
     * Sets the value of the documentoPagador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoPagador(String value) {
        this.documentoPagador = value;
    }

    /**
     * Gets the value of the fechaOperacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaOperacion() {
        return fechaOperacion;
    }

    /**
     * Sets the value of the fechaOperacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaOperacion(XMLGregorianCalendar value) {
        this.fechaOperacion = value;
    }

    /**
     * Gets the value of the importeOperacion property.
     * 
     */
    public double getImporteOperacion() {
        return importeOperacion;
    }

    /**
     * Sets the value of the importeOperacion property.
     * 
     */
    public void setImporteOperacion(double value) {
        this.importeOperacion = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the tipoDocumentoObligado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTipoDocumentoObligado() {
        return tipoDocumentoObligado;
    }

    /**
     * Sets the value of the tipoDocumentoObligado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTipoDocumentoObligado(Integer value) {
        this.tipoDocumentoObligado = value;
    }

}
