
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DefaultDatosPagoRegistroOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DefaultDatosPagoRegistroOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="datosPagoOut" type="{http://es.gob.ePago/schemas}DefaultDatosPagoOut"/>
 *         &lt;element name="datosRegistroOut" type="{http://es.gob.ePago/schemas}DefaultDatosRegistroOut"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DefaultDatosPagoRegistroOut", propOrder = {
    "datosPagoOut",
    "datosRegistroOut"
})
public class DefaultDatosPagoRegistroOut {

    @XmlElement(required = true, nillable = true)
    protected DefaultDatosPagoOut datosPagoOut;
    @XmlElement(required = true, nillable = true)
    protected DefaultDatosRegistroOut datosRegistroOut;

    /**
     * Gets the value of the datosPagoOut property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultDatosPagoOut }
     *     
     */
    public DefaultDatosPagoOut getDatosPagoOut() {
        return datosPagoOut;
    }

    /**
     * Sets the value of the datosPagoOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultDatosPagoOut }
     *     
     */
    public void setDatosPagoOut(DefaultDatosPagoOut value) {
        this.datosPagoOut = value;
    }

    /**
     * Gets the value of the datosRegistroOut property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultDatosRegistroOut }
     *     
     */
    public DefaultDatosRegistroOut getDatosRegistroOut() {
        return datosRegistroOut;
    }

    /**
     * Sets the value of the datosRegistroOut property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultDatosRegistroOut }
     *     
     */
    public void setDatosRegistroOut(DefaultDatosRegistroOut value) {
        this.datosRegistroOut = value;
    }

}
