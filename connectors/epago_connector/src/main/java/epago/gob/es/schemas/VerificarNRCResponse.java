
package epago.gob.es.schemas;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="verificarNRCReturn" type="{http://es.gob.ePago/schemas}DefaultDatosNRCOut"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "verificarNRCReturn"
})
@XmlRootElement(name = "verificarNRCResponse")
public class VerificarNRCResponse {

    @XmlElement(required = true)
    protected DefaultDatosNRCOut verificarNRCReturn;

    /**
     * Gets the value of the verificarNRCReturn property.
     * 
     * @return
     *     possible object is
     *     {@link DefaultDatosNRCOut }
     *     
     */
    public DefaultDatosNRCOut getVerificarNRCReturn() {
        return verificarNRCReturn;
    }

    /**
     * Sets the value of the verificarNRCReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link DefaultDatosNRCOut }
     *     
     */
    public void setVerificarNRCReturn(DefaultDatosNRCOut value) {
        this.verificarNRCReturn = value;
    }

}
