package es.grupoavalon.aecosan.connector.epago.dto;

import javax.xml.bind.annotation.XmlElement;

public class EpagoGetJustifierAndSignOriginRequestDTO {
	
	@XmlElement (name = "getJustifierRequestDTO")
	private EpagoGetJustifierRequestDTO getJustifierRequestDTO;
	
	@XmlElement (name = "getSignOriginRequestDTO")
	private EpagoGetSignOriginRequestDTO getSignOriginRequestDTO;

	public EpagoGetJustifierRequestDTO getGetJustifierRequestDTO() {
		return getJustifierRequestDTO;
	}

	public void setGetJustifierRequestDTO(EpagoGetJustifierRequestDTO getJustifierRequestDTO) {
		this.getJustifierRequestDTO = getJustifierRequestDTO;
	}

	public EpagoGetSignOriginRequestDTO getGetSignOriginRequestDTO() {
		return getSignOriginRequestDTO;
	}

	public void setGetSignOriginRequestDTO(EpagoGetSignOriginRequestDTO getSignOriginRequestDTO) {
		this.getSignOriginRequestDTO = getSignOriginRequestDTO;
	}

	@Override
	public String toString() {
		return "EpagoGetJustifierAndSignOriginRequestDTO [getJustifierRequestDTO=" + getJustifierRequestDTO + ", getSignOriginRequestDTO=" + getSignOriginRequestDTO + "]";
	}

}
