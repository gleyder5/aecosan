package es.grupoavalon.aecosan.connector.epago.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import javax.xml.bind.JAXBException;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;

import epago.gob.es.schemas.PPServiceInterface;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorConnectionException;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorException;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;

public abstract class AbstractEpagoConnector {

	protected static URL instanciateUrl(String url) {
		URL wsUrl;
		try {
			wsUrl = new URL(url);
		} catch (MalformedURLException e) {
			throw new EpagoConnectorConnectionException("Invalid URL:" + url);
		}
		return wsUrl;
	}

	protected static Object[] invokeClientProxy(PPServiceInterface port, String method, Object... params) throws Exception {
		Client client = setupClientProxy(port);

		return client.invoke(method, params);
	}

	private static Client setupClientProxy(PPServiceInterface port) throws JAXBException {
		Client clientProxy = ClientProxy.getClient(port);
		clientProxy.getInInterceptors().add(new LoggingInInterceptor());
		clientProxy.getOutInterceptors().add(new LoggingOutInterceptor());
		return clientProxy;
	}

	@SuppressWarnings("unchecked")
	protected <T> T getRespuestaFromRaw(Object[] responseRaw) {
		T response;

		if (responseRaw != null && responseRaw.length > 0) {
			response = (T) responseRaw[0];
		} else {
			throw new EpagoConnectorException(EpagoConnectorException.RESPONSE_UNEXPECTED + Arrays.toString(responseRaw));
		}

		return response;
	}

	protected static void handleException(Exception e) {
		if (e instanceof EpagoConnectorException) {
			throw (EpagoConnectorException) e;
		} else {
			throw new EpagoConnectorException(EpagoConnectorException.GENERAL_ERROR + e.getLocalizedMessage(), e);
		}
	}
}
