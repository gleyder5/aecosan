package es.grupoavalon.aecosan.connector.epago.dto;

import javax.xml.bind.annotation.XmlElement;

public class EpagoSendPaymentRequestDTO {
	@XmlElement(name = "idOrganismo", required = true)
	private int idOrganismo;
	
	@XmlElement(name = "datosPagoIn", required = true)
	private DatosPagoInDTO datosPagoIn;
	
	@XmlElement(name = "url", required = true)
	private String url;
	
	public String getUrl() {
		return this.url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}

	public int getIdOrganismo() {
		return idOrganismo;
	}

	public void setIdOrganismo(int idOrganismo) {
		this.idOrganismo = idOrganismo;
	}

	public DatosPagoInDTO getDatosPagoIn() {
		return datosPagoIn;
	}

	public void setDatosPagoIn(DatosPagoInDTO datosPagoIn) {
		this.datosPagoIn = datosPagoIn;
	}

	@Override
	public String toString() {
		return "EpagoSendPaymentRequestDTO [idOrganismo=" + idOrganismo + ", datosPagoIn=" + datosPagoIn + ", url=" + url + "]";
	}

}
