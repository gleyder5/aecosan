package es.grupoavalon.aecosan.connector.epago.util;

public enum PaymentType {
	ACCOUNT_PAYMENT(0), CREDIT_CARD(1);

	private int code;

	private PaymentType(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		return "[" + this.code + "]";
	}
}
