package es.grupoavalon.aecosan.connector.epago.exception;

public class EpagoConnectorOperationException extends EpagoConnectorException {
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private String errorCodeTexto;
	private String errorOrigenDescription;
	
	public EpagoConnectorOperationException(
			String errorCode, 
			String errorDescription, 
			String errorCodeTexto,
			String errorOrigenDescription) 
	{
		super(errorDescription);
		this.errorCode = errorCode;
		this.errorCodeTexto = errorCodeTexto;
		this.errorOrigenDescription = errorOrigenDescription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorCodeTexto() {
		return errorCodeTexto;
	}

	public String getErrorOrigenDescription() {
		return errorOrigenDescription;
	}
}
