package es.grupoavalon.aecosan.connector.epago.dto;

import javax.xml.bind.annotation.XmlElement;

public class EpagoGetJustifierAndSignOriginResponseDTO {
	@XmlElement (name = "justificante")
	private String justificante;

	@XmlElement (name = "origenFirma")
	private String origenFirma;

	public String getJustificante() {
		return justificante;
	}

	public void setJustificante(String justificante) {
		this.justificante = justificante;
	}

	public String getOrigenFirma() {
		return origenFirma;
	}

	public void setOrigenFirma(String origenFirma) {
		this.origenFirma = origenFirma;
	}

	@Override
	public String toString() {
		return "EpagoGetJustifierAndSignOriginResponseDTO [justificante=" + justificante + ", origenFirma=" + origenFirma + "]";
	}

}
