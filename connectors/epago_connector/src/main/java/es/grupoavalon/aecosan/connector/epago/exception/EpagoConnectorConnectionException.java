package es.grupoavalon.aecosan.connector.epago.exception;

public class EpagoConnectorConnectionException extends EpagoConnectorException {
	private static final long serialVersionUID = 1L;
	
	public EpagoConnectorConnectionException(String message) {
		super(message);
	}

	public EpagoConnectorConnectionException(Exception e) {
		super(e);
	}
}
