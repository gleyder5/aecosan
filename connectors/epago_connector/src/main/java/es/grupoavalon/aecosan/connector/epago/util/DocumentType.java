package es.grupoavalon.aecosan.connector.epago.util;

public enum DocumentType {
	CIF(0, "CIF"), NIF(1, "NIF"), DNI(2, "DNI"), NIE(3, "NIE");

	private int code;
	private String name;

	private DocumentType(int code, String name) {
		this.code = code;
		this.name = name;
	}

	public int getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static DocumentType instanciateByName(String name) {
		DocumentType docType = null;
		if (CIF.getName().equalsIgnoreCase(name)) {
			docType = CIF;
		}
		if (NIF.getName().equalsIgnoreCase(name)) {
			docType = NIF;
		}
		if (DNI.getName().equalsIgnoreCase(name)) {
			docType = DNI;
		}
		if (NIE.getName().equalsIgnoreCase(name)) {
			docType = NIE;
		}
		return docType;
	}

	@Override
	public String toString() {
		return "[" + this.name + ":" + this.code + "]";
	}
}
