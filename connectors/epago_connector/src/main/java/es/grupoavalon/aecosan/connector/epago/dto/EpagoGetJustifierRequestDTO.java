package es.grupoavalon.aecosan.connector.epago.dto;

import javax.xml.bind.annotation.XmlElement;

public class EpagoGetJustifierRequestDTO {

	@XmlElement (name = "url")
	private String url;

	@XmlElement (name = "modelo")
	private String modelo;
	
	@XmlElement (name = "codigoTasa")
	private String codigoTasa;
	
	@XmlElement (name = "idOrganismo")
	private int idOrganismo;

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getCodigoTasa() {
		return codigoTasa;
	}

	public void setCodigoTasa(String codigoTasa) {
		this.codigoTasa = codigoTasa;
	}

	public int getIdOrganismo() {
		return idOrganismo;
	}

	public void setIdOrganismo(int idOrganismo) {
		this.idOrganismo = idOrganismo;
	}

	@Override
	public String toString() {
		return "EpagoGetJustifierRequestDTO [url=" + url + ", modelo=" + modelo + ", codigoTasa=" + codigoTasa + ", idOrganismo=" + idOrganismo + "]";
	}

}
