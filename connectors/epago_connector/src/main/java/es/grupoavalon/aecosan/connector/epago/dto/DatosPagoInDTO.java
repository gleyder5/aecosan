package es.grupoavalon.aecosan.connector.epago.dto;

import java.util.GregorianCalendar;

import javax.xml.bind.annotation.XmlElement;

import es.grupoavalon.aecosan.connector.epago.util.DocumentType;
import es.grupoavalon.aecosan.connector.epago.util.PaymentType;

public class DatosPagoInDTO {
	@XmlElement(name = "ccc")
	private String ccc;
	
	@XmlElement(name = "apellido1", required = true)
	private String apellido1;

	@XmlElement(name = "apellido2", required = true)
	private String apellido2;
	
	// %VBR% Estos dos campo pone que no se usan, en cambio en los xml de
	// ejemplo sí los envían.
	@XmlElement(name = "certificado1")
	private String certificado1;
	
	@XmlElement(name = "certificado2")
	private String certificado2;
	
	@XmlElement(name = "codigoBanco", required = true)
	private String codigoBanco;
	
	@XmlElement(name = "codigoTasa", required = true)
	private String codigoTasa;
	
	@XmlElement(name = "documentoObligado", required = true)
	private String documentoObligado;

	@XmlElement(name = "documentoPagador")
	private String documentoPagador;

	@XmlElement(name = "fechaCaducidadTarjeta")
	private GregorianCalendar fechaCaducidadTarjeta;
	
	// %VBR% Este campo es calculado. No sé si lo tendrán que mandar desde fuera
	// o lo hace el API
	@XmlElement(name = "firma1", required = true)
	private String firma1;
	
	// %VBR% Este campo es calculado. No sé si lo tendrán que mandar desde fuera
	// o lo hace el API
	@XmlElement(name = "firma2")
	private String firma2;
	
	@XmlElement(name = "importe", required = true)
	private double importe;

	@XmlElement(name = "justificante")
	private String justificante;
	
	@XmlElement(name = "nombre", required = true)
	private String nombre;

	@XmlElement(name = "numeroTarjeta")
	private String numeroTarjeta;
	
	// %VBR% Este campo es calculado. No sé si lo tendrán que mandar desde fuera
	// o lo hace el API
	@XmlElement(name = "origenFirma", required = true)
	private String origenFirma;

	@XmlElement(name = "tipoCargo", required = true)
	private PaymentType tipoCargo;

	@XmlElement(name = "tipoDocumentoObligado", required = true)
	private DocumentType tipoDocumentoObligado;

	@XmlElement(name = "tipoDocumentoPagador")
	private DocumentType tipoDocumentoPagador;

	@XmlElement(name = "apoderado")
	private boolean apoderado;

	public String getCcc() {
		return ccc;
	}

	public void setCcc(String ccc) {
		this.ccc = ccc;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getCertificado1() {
		return certificado1;
	}

	public void setCertificado1(String certificado1) {
		this.certificado1 = certificado1;
	}

	public String getCertificado2() {
		return certificado2;
	}

	public void setCertificado2(String certificado2) {
		this.certificado2 = certificado2;
	}

	public String getCodigoBanco() {
		return codigoBanco;
	}

	public void setCodigoBanco(String codigoBanco) {
		this.codigoBanco = codigoBanco;
	}

	public String getCodigoTasa() {
		return codigoTasa;
	}

	public void setCodigoTasa(String codigoTasa) {
		this.codigoTasa = codigoTasa;
	}

	public String getDocumentoObligado() {
		return documentoObligado;
	}

	public void setDocumentoObligado(String documentoObligado) {
		this.documentoObligado = documentoObligado;
	}

	public String getDocumentoPagador() {
		return documentoPagador;
	}

	public void setDocumentoPagador(String documentoPagador) {
		this.documentoPagador = documentoPagador;
	}

	public GregorianCalendar getFechaCaducidadTarjeta() {
		return fechaCaducidadTarjeta;
	}

	public void setFechaCaducidadTarjeta(GregorianCalendar fechaCaducidadTarjeta) {
		this.fechaCaducidadTarjeta = fechaCaducidadTarjeta;
	}

	public String getFirma1() {
		return firma1;
	}

	public void setFirma1(String firma1) {
		this.firma1 = firma1;
	}

	public String getFirma2() {
		return firma2;
	}

	public void setFirma2(String firma2) {
		this.firma2 = firma2;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public String getJustificante() {
		return justificante;
	}

	public void setJustificante(String justificante) {
		this.justificante = justificante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getOrigenFirma() {
		return origenFirma;
	}

	public void setOrigenFirma(String origenFirma) {
		this.origenFirma = origenFirma;
	}

	public PaymentType getTipoCargo() {
		return tipoCargo;
	}

	public void setTipoCargo(PaymentType tipoCargo) {
		this.tipoCargo = tipoCargo;
	}

	public DocumentType getTipoDocumentoObligado() {
		return tipoDocumentoObligado;
	}

	public void setTipoDocumentoObligado(DocumentType tipoDocumentoObligado) {
		this.tipoDocumentoObligado = tipoDocumentoObligado;
	}

	public DocumentType getTipoDocumentoPagador() {
		return tipoDocumentoPagador;
	}

	public void setTipoDocumentoPagador(DocumentType tipoDocumentoPagador) {
		this.tipoDocumentoPagador = tipoDocumentoPagador;
	}

	public boolean isApoderado() {
		return apoderado;
	}

	public void setApoderado(boolean apoderado) {
		this.apoderado = apoderado;
	}

	@Override
	public String toString() {
		return "DatosPagoInDTO [ccc=" + ccc + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", certificado1=" + certificado1 + ", certificado2=" + certificado2 + ", codigoBanco="
				+ codigoBanco + ", codigoTasa=" + codigoTasa + ", documentoObligado=" + documentoObligado + ", documentoPagador=" + documentoPagador + ", fechaCaducidadTarjeta="
				+ fechaCaducidadTarjeta + ", firma1=" + firma1 + ", firma2=" + firma2 + ", importe=" + importe + ", justificante=" + justificante + ", nombre=" + nombre + ", numeroTarjeta="
				+ numeroTarjeta + ", origenFirma=" + origenFirma + ", tipoCargo=" + tipoCargo + ", tipoDocumentoObligado=" + tipoDocumentoObligado + ", tipoDocumentoPagador=" + tipoDocumentoPagador
				+ ", apoderado=" + apoderado + "]";
	}

}
