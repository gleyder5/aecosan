package es.grupoavalon.aecosan.connector.epago.impl;

import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import epago.gob.es.schemas.DefaultDatosPagoIn;
import epago.gob.es.schemas.DefaultDatosPagoOut;
import epago.gob.es.schemas.ObjectFactory;
import epago.gob.es.schemas.PPServiceInterface;
import epago.gob.es.schemas.PPServiceInterfaceService;
import es.grupoavalon.aecosan.connector.epago.EpagoConnectorApi;
import es.grupoavalon.aecosan.connector.epago.dto.DatosPagoInDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentResponseDTO;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorOperationException;

public class EpagoConnectorApiImpl extends AbstractEpagoConnector implements EpagoConnectorApi {

	protected static final Logger logger = LoggerFactory.getLogger(EpagoConnectorApiImpl.class);

	@Override
	public EpagoSendPaymentResponseDTO sendPayment(EpagoSendPaymentRequestDTO paymentData) {
		EpagoSendPaymentResponseDTO paymentResponse = null;
		try {
			URL url = instanciateUrl(paymentData.getUrl());

			PPServiceInterface port = instanciateServicePort(url);
			DefaultDatosPagoIn request = getDefaultDatosPagoInFromDTO(paymentData);
			DefaultDatosPagoOut response = executeSendPayment(port, request, paymentData.getIdOrganismo());
			paymentResponse = processSendPaymentResponse(response);

		} catch (Exception e) {
			handleException(e);
		}

		return paymentResponse;
	}

	private DefaultDatosPagoIn getDefaultDatosPagoInFromDTO(EpagoSendPaymentRequestDTO paymentData) throws DatatypeConfigurationException {
		ObjectFactory factory = new ObjectFactory();
		DefaultDatosPagoIn retVal = factory.createDefaultDatosPagoIn();
		DatosPagoInDTO dto = paymentData.getDatosPagoIn();
		retVal.setApellido1(dto.getApellido1());
		retVal.setApellido2(dto.getApellido2());
		retVal.setApoderado(dto.isApoderado());
		retVal.setCCC(dto.getCcc());
		retVal.setCertificado1(dto.getCertificado1());
		retVal.setCertificado2(dto.getCertificado2());
		retVal.setCodigoBanco(String.valueOf(dto.getCodigoBanco()));
		retVal.setCodigoTasa(String.valueOf(dto.getCodigoTasa()));
		retVal.setDocumentoObligado(dto.getDocumentoObligado());
		retVal.setDocumentoPagador(dto.getDocumentoPagador());
		if (dto.getFechaCaducidadTarjeta() != null) {
			retVal.setFechaCaducidadTarjeta(DatatypeFactory.newInstance().newXMLGregorianCalendar(dto.getFechaCaducidadTarjeta()));
		}
		retVal.setFirma1(dto.getFirma1());
		retVal.setFirma2(dto.getFirma2());
		retVal.setHashOrigenFirma("");
		retVal.setImporte(dto.getImporte());
		retVal.setJustificante(dto.getJustificante());
		retVal.setNombre(dto.getNombre());
		retVal.setNumeroTarjeta(dto.getNumeroTarjeta());
		retVal.setOrigenFirma(dto.getOrigenFirma());
		retVal.setTipoCargo(dto.getTipoCargo().getCode());
		retVal.setTipoDocumentoObligado(dto.getTipoDocumentoObligado().getCode());
		if (dto.getTipoDocumentoPagador() != null) {
			retVal.setTipoDocumentoPagador(dto.getTipoDocumentoPagador().getCode());
		}
		return retVal;
	}

	private DefaultDatosPagoOut executeSendPayment(PPServiceInterface port, DefaultDatosPagoIn request, int organismo) throws Exception {
		Object[] responseRaw = invokeClientProxy(port, "hacerPago", request, organismo);
		return getRespuestaFromRaw(responseRaw);
	}

	private EpagoSendPaymentResponseDTO processSendPaymentResponse(DefaultDatosPagoOut response) {
		EpagoSendPaymentResponseDTO responseDTO;
		if (!response.isFueCorrecto()) {
			logger.debug(" error response received:" + response);
			throw new EpagoConnectorOperationException(response.getErrorCode(), response.getErrorDescription(), response.getErrorCodeTexto(), response.getErrorCodeTextoUsuario());
		} else {
			logger.debug("response ok received:" + response);
			responseDTO = instanciateEpagoSendPaymentResponseDTOFromWSResponse(response);
		}
		return responseDTO;
	}

	private EpagoSendPaymentResponseDTO instanciateEpagoSendPaymentResponseDTOFromWSResponse(DefaultDatosPagoOut response) {
		EpagoSendPaymentResponseDTO responseDTO = new EpagoSendPaymentResponseDTO();
		responseDTO.setNrc(response.getNRC());
		responseDTO.setMerchanAEAT(response.getMerchan());
		responseDTO.setReferenceAEAT(response.getReferencia());
		responseDTO.setRegisterAEAT(response.getRegistroAEAT());
		return responseDTO;
	}

	@Override
	public EpagoGetJustifierResponseDTO getJustifier(EpagoGetJustifierRequestDTO getJustifierRequest) {
		EpagoGetJustifierResponseDTO retVal = null;
		try {
			URL url = instanciateUrl(getJustifierRequest.getUrl());
			PPServiceInterface port = instanciateServicePort(url);
			retVal = executeGetJustificante(port, getJustifierRequest);
			
		} catch (Exception e) {
			handleException(e);
		}
		return retVal;
	}

	private EpagoGetJustifierResponseDTO executeGetJustificante(PPServiceInterface port, EpagoGetJustifierRequestDTO getJustifierRequest) throws Exception {

		Object[] responseRaw = invokeClientProxy(port, "getJustificante", getJustifierRequest.getModelo(), getJustifierRequest.getCodigoTasa(), getJustifierRequest.getIdOrganismo());
		String response = getRespuestaFromRaw(responseRaw);
		EpagoGetJustifierResponseDTO retVal = new EpagoGetJustifierResponseDTO();
		retVal.setJustificante(response);
		return retVal;
	}

	private PPServiceInterface instanciateServicePort(URL url) {
		PPServiceInterfaceService service = new PPServiceInterfaceService(url);
		return service.getPPServiceInterfaceSoap11();
	}



	@Override
	public EpagoGetJustifierAndSignOriginResponseDTO getJustifierAndSignOrigin(EpagoGetJustifierAndSignOriginRequestDTO request) {
		EpagoGetJustifierResponseDTO responseJustifier = getJustifier(request.getGetJustifierRequestDTO());
		EpagoGetSignOriginRequestDTO requestSign = request.getGetSignOriginRequestDTO();
		requestSign.setJustificante(responseJustifier.getJustificante());
		EpagoGetSignOriginResponseDTO responseSignOrigin = getSignOrigin(requestSign);

		return instanciateResponseEpagoGetSignOrigin(responseJustifier, responseSignOrigin);
	}

	private static EpagoGetJustifierAndSignOriginResponseDTO instanciateResponseEpagoGetSignOrigin(EpagoGetJustifierResponseDTO responseJustifier, EpagoGetSignOriginResponseDTO responseSignOrigin) {
		EpagoGetJustifierAndSignOriginResponseDTO retVal = new EpagoGetJustifierAndSignOriginResponseDTO();
		retVal.setJustificante(responseJustifier.getJustificante());
		retVal.setOrigenFirma(responseSignOrigin.getOrigenFirma());
		return retVal;
	}

	@Override
	public EpagoGetSignOriginResponseDTO getSignOrigin(EpagoGetSignOriginRequestDTO request) {
		EpagoGetSignOriginResponseDTO retVal = new EpagoGetSignOriginResponseDTO();

		retVal.setOrigenFirma(generateSignOrigin(request));
		return retVal;
	}

	private static String generateSignOrigin(EpagoGetSignOriginRequestDTO request) {
		StringBuilder sb = new StringBuilder();
		appendMainData(sb, request);

		if (StringUtils.isEmpty(request.getCcc())) {
			appendCreditCardInfo(sb, request);
		} else {
			appendAccountData(sb, request);
		}
		if (request.isApoderado()) {
			sb.append("Realizo el cargo con apoderamiento en la cuenta del obligado\n");
		}
		return sb.toString();
	}

	private static void appendMainData(StringBuilder sb, EpagoGetSignOriginRequestDTO request) {
		sb.append(StringUtils.rightPad("JUSTIFICANTE:", 24)).append(request.getJustificante()).append("\n");
		sb.append(StringUtils.rightPad("NIF/CIF:", 24)).append(request.getDocumentoObligado()).append("\n");
		sb.append(StringUtils.rightPad("IMPORTE DEL INGRESO:", 24)).append(formatedAmmount(request.getImporte())).append("\n");
	}

	private static String formatedAmmount(double number) {
		NumberFormat nf = NumberFormat.getInstance(new Locale("es", "ES"));
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setGroupingUsed(true);
		return nf.format(number);
	}

	private static void appendCreditCardInfo(StringBuilder sb, EpagoGetSignOriginRequestDTO request) {
		sb.append(StringUtils.rightPad("EMISOR DE TARJETA:", 24)).append(request.getCodigoBanco()).append("\n");
		sb.append(StringUtils.rightPad("NUMERO DE TARJETA:", 24)).append(getFormattedTarjeta(request.getNumeroTarjeta())).append("\n");
		SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
		sb.append(StringUtils.rightPad("CADUCA (MM/AA):", 24)).append(sdf.format(request.getFechaCaducidadTarjeta().getTime())).append("\n");
	}

	private static void appendAccountData(StringBuilder sb, EpagoGetSignOriginRequestDTO request) {
		sb.append(StringUtils.rightPad("IBAN:", 24)).append(StringUtils.rightPad(request.getCcc(), 34)).append("\n");
	}

	private static String getFormattedTarjeta(String tarjeta) {
		StringBuilder retVal = new StringBuilder();
		retVal.append(tarjeta.substring(0, 4)).append("-").append(tarjeta.substring(4, 8)).append("-").append(tarjeta.substring(8, 12)).append("-").append(tarjeta.substring(12, 16));
		return retVal.toString();
	}

}
