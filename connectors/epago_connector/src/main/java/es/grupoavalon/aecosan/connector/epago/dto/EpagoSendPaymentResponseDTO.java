package es.grupoavalon.aecosan.connector.epago.dto;

public class EpagoSendPaymentResponseDTO {

	private String nrc;

	private String merchanAEAT;

	private String referenceAEAT;

	private String registerAEAT;

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getMerchanAEAT() {
		return merchanAEAT;
	}

	public void setMerchanAEAT(String merchanAEAT) {
		this.merchanAEAT = merchanAEAT;
	}

	public String getReferenceAEAT() {
		return referenceAEAT;
	}

	public void setReferenceAEAT(String referenceAEAT) {
		this.referenceAEAT = referenceAEAT;
	}

	public String getRegisterAEAT() {
		return registerAEAT;
	}

	public void setRegisterAEAT(String registerAEAT) {
		this.registerAEAT = registerAEAT;
	}

	@Override
	public String toString() {
		return "EpagoSendPaymentResponseDTO [nrc=" + nrc + ", merchanAEAT=" + merchanAEAT + ", referenceAEAT=" + referenceAEAT + ", registerAEAT=" + registerAEAT + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((merchanAEAT == null) ? 0 : merchanAEAT.hashCode());
		result = prime * result + ((nrc == null) ? 0 : nrc.hashCode());
		result = prime * result + ((referenceAEAT == null) ? 0 : referenceAEAT.hashCode());
		result = prime * result + ((registerAEAT == null) ? 0 : registerAEAT.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EpagoSendPaymentResponseDTO other = (EpagoSendPaymentResponseDTO) obj;
		if (merchanAEAT == null) {
			if (other.merchanAEAT != null) {
				return false;
			}
		} else if (!merchanAEAT.equals(other.merchanAEAT)) {
			return false;
		}
		if (nrc == null) {
			if (other.nrc != null) {
				return false;
			}
		} else if (!nrc.equals(other.nrc)) {
			return false;
		}
		if (referenceAEAT == null) {
			if (other.referenceAEAT != null) {
				return false;
			}
		} else if (!referenceAEAT.equals(other.referenceAEAT)) {
			return false;
		}
		if (registerAEAT == null) {
			if (other.registerAEAT != null) {
				return false;
			}
		} else if (!registerAEAT.equals(other.registerAEAT)) {
			return false;
		}
		return true;
	}

}
