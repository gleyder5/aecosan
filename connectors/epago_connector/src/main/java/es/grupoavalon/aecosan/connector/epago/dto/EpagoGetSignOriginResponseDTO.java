package es.grupoavalon.aecosan.connector.epago.dto;

import javax.xml.bind.annotation.XmlElement;

public class EpagoGetSignOriginResponseDTO {

	@XmlElement (name = "origenFirma")
	private String origenFirma;

	public String getOrigenFirma() {
		return origenFirma;
	}

	public void setOrigenFirma(String origenFirma) {
		this.origenFirma = origenFirma;
	}
	
	
}
