package es.grupoavalon.aecosan.connector.epago.exception;

public class EpagoConnectorException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public static final String GENERAL_ERROR = "Can not execute the Paymnent process due an error:";

	public static final String RESPONSE_UNEXPECTED = "Can not process the response from the server was unexpected";

	public EpagoConnectorException(String message, Throwable cause) {
		super(message, cause);

	}

	public EpagoConnectorException(String message) {
		super(message);
	}
	
	public EpagoConnectorException(Throwable t) {
		super(t);
	}
}
