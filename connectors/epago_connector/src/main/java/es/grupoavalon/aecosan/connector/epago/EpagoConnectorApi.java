package es.grupoavalon.aecosan.connector.epago;

import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentResponseDTO;

public interface EpagoConnectorApi {

	EpagoGetJustifierResponseDTO getJustifier(EpagoGetJustifierRequestDTO getJustifierRequest);

	EpagoGetJustifierAndSignOriginResponseDTO getJustifierAndSignOrigin(EpagoGetJustifierAndSignOriginRequestDTO getJustifierAndOriginRequest);

	EpagoGetSignOriginResponseDTO getSignOrigin(EpagoGetSignOriginRequestDTO getSignOriginRequest);

	EpagoSendPaymentResponseDTO sendPayment(EpagoSendPaymentRequestDTO paymentData);
}
