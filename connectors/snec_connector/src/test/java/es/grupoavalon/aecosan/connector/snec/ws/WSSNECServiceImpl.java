package es.grupoavalon.aecosan.connector.snec.ws;

import javax.jws.WebService;

import es.grupoavalon.aecosan.connector.snec.SnecConnectorApiTest;
import es.msssi.snec.ws.Exception_Exception;
import es.msssi.snec.ws.WSSNECServicePortType;
import es.msssi.snec.ws.dto.xsd.ObjectFactory;
import es.msssi.snec.ws.dto.xsd.PeticionBusqueda;
import es.msssi.snec.ws.dto.xsd.PeticionDetalleNoti;
import es.msssi.snec.ws.dto.xsd.PeticionEnvioNoti;
import es.msssi.snec.ws.dto.xsd.RespuestaBusqueda;
import es.msssi.snec.ws.dto.xsd.RespuestaDetalleNoti;
import es.msssi.snec.ws.dto.xsd.RespuestaDetalleNotiC;
import es.msssi.snec.ws.dto.xsd.RespuestaEnvioNoti;

@WebService(serviceName = "WSSNECService",
			targetNamespace = "http://ws.snec.msssi.es",
			endpointInterface = "es.msssi.snec.ws.WSSNECServicePortType",
			portName = "WSSNECServiceHttpSoap12Endpoint")
public class WSSNECServiceImpl implements WSSNECServicePortType {

	@Override
	public RespuestaDetalleNoti consultaDetalleNotif(PeticionDetalleNoti detalleNotif) throws Exception_Exception {
		throw new Exception_Exception("NOT IMPLEMENTED");
	}

	@Override
	public RespuestaDetalleNotiC consultaNotificacionCompleta(PeticionDetalleNoti detalleNotif)
			throws Exception_Exception {
		throw new Exception_Exception("NOT IMPLEMENTED");
	}

	@Override
	public RespuestaBusqueda buscarNotificaciones(PeticionBusqueda criterios) throws Exception_Exception {
		throw new Exception_Exception("NOT IMPLEMENTED");
	}

	@Override
	public RespuestaEnvioNoti enviarNotif(PeticionEnvioNoti peticionEnvio) throws Exception_Exception {
		RespuestaEnvioNoti retVal = new RespuestaEnvioNoti();
		ObjectFactory factory = new ObjectFactory();
		if (SnecConnectorApiTest.CODIGOPROCE_KO.equals(peticionEnvio.getCodigoProce().getValue())) {
			retVal.setCodigoRespuesta(13);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("No se han encontrado procedimiento para ese código."));
		} else if (SnecConnectorApiTest.USERNAME_KO.equals(peticionEnvio.getUsername().getValue())) {
			retVal.setCodigoRespuesta(2);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("Usuario no válido en el sistema."));
		} else if (SnecConnectorApiTest.DNI_KO.equals(peticionEnvio.getDni().getValue())) {
			retVal.setCodigoRespuesta(9);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("Formato de DNI/CIF incorrecto."));
		} else if (peticionEnvio.getDni().getValue().length() == 0) {
			retVal.setCodigoRespuesta(6);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("El campo DNI/CIF es obligatorio."));
		} else if (peticionEnvio.getEmail().getValue().length() == 0) {
			retVal.setCodigoRespuesta(7);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("El campo EMAIL DESTINATARIO es obligatorio."));
		} else if (SnecConnectorApiTest.EMAIL_KO.equals(peticionEnvio.getEmail().getValue())) {
			retVal.setCodigoRespuesta(8);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("Formato de email incorrecto."));
		} else if (peticionEnvio.getApellido1().getValue().length() == 0) {
			retVal.setCodigoRespuesta(16);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("El campo Apellido1 es obligatorio."));
		} else if (peticionEnvio.getNombre().getValue().length() == 0) {
			retVal.setCodigoRespuesta(15);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("El campo Apellido1 es obligatorio."));
		}  else {

			retVal.setCodigoRespuesta(11);
			retVal.setDescripcion(factory.createRespuestaWSDescripcion("La operación se ha realizado correctamente."));
		}
		return retVal;
	}

}
