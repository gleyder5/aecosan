package es.grupoavalon.aecosan.connector.snec;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.snec.dto.AttachmentSnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorConnectionException;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorOperationException;
import es.grupoavalon.aecosan.connector.snec.impl.SnecConnectorApiImpl;
import es.grupoavalon.aecosan.connector.snec.testsetup.TestServer;

public class SnecConnectorApiTest {
	public static final String CODIGOPROCE_OK = "085044";
	public static final String CODIGOPROCE_KO = "codigoProce";
	public static final String USERNAME_OK = "oz8kNc7g";
	public static final String USERNAME_KO = "username";
	public static final String APELLIDO1 = "Apellido1";
	public static final String APELLIDO2 = "Apellido2";
	public static final String ASUNTO = "Asunto";
	public static final String CONTENIDO = "Contenido";
	public static final String DNI_OK = "16395797V";
	public static final String DNI_KO = "DNI";
	public static final String EMAIL_OK = "otto.abreu@grupoavalon.es";
	public static final String EMAIL_KO = "EMAIL";
	public static final String NOMBRE = "Nombre";

	private SnecNotificationDTO dto;
	private SnecConnectorApi connector;
	private static final Logger logger = LoggerFactory.getLogger(SnecConnectorApiTest.class);
	// private static final String TEST_WS_URL =
	// "http://localhost:9000/snec_connector?wsdl";
	private static final String TEST_WS_URL = "https://pre-snec.msc.es:443/snec/services/WSSNECService?wsdl";
	private static TestServer testServer = new TestServer();

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@BeforeClass
	public static void beforeClass() {
		// testServer.serverCreate(new WSSNECServiceImpl(),
		// WSSNECServiceImpl.class, "snec_connector");
	}

	@AfterClass
	public static void afterClass() {
		// testServer.serverDestroy();
	}

	@Before
	public void before() throws IOException {
		connector = new SnecConnectorApiImpl();
		dto = getRightNotificationDTO();
	}

	@Test
	public void testSendNotificationOK() throws Exception {

			try {
				connector.sendNotification(dto);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			throw e;
			}
	}

	@Test
	public void testSendNotificationKOCodigoProce() {
		dto.setCodigoProce(CODIGOPROCE_KO);
		sendKORequest(13);
	}

	@Test
	public void testSendNotificationKOUsername() {
		dto.setUsername(USERNAME_KO);
		sendKORequest(2);
	}

	@Test
	public void testSendNotificationEmptyDNI() {
		dto.setDni("");
		sendKORequest(6);
	}

	@Test
	public void testSendNotificationKODNI() {
		dto.setDni(DNI_KO);
		sendKORequest(9);
	}

	@Test
	public void testSendNotificationEmptyEmail() {
		dto.setEmail("");
		sendKORequest(7);
	}

	@Test
	public void testSendNotificationKOEmail() {
		dto.setEmail(EMAIL_KO);
		sendKORequest(8);
	}

	@Test
	public void testSendNotificationEmptyApellido() {
		dto.setApellido1("");
		sendKORequest(16);
	}

	@Test
	public void testSendNotificationEmptyNombre() {
		dto.setNombre("");
		sendKORequest(15);
	}

	private SnecNotificationDTO getRightNotificationDTO() throws IOException {
		SnecNotificationDTO retVal = new SnecNotificationDTO();
		retVal.setApellido1(APELLIDO1);
		retVal.setApellido2(APELLIDO2);
		retVal.setAsunto(ASUNTO);
		retVal.setCodigoProce(CODIGOPROCE_OK);
		retVal.setContenido(CONTENIDO);
		retVal.setDni(DNI_OK);
		retVal.setEmail(EMAIL_OK);
		retVal.setNombre(NOMBRE);
		retVal.setUsername(USERNAME_OK);
		retVal.setAdjuntos(getAdjuntosFromResources());
		retVal.setUrl(TEST_WS_URL);
		return retVal;
	}

	private List<AttachmentSnecNotificationDTO> getAdjuntosFromResources() throws IOException {
		ClassLoader classLoader = getClass().getClassLoader();
		List<AttachmentSnecNotificationDTO> retVal = new ArrayList<AttachmentSnecNotificationDTO>();
		File dir = new File(classLoader.getResource("files").getFile());
		File[] arrFiles = dir.listFiles();
		for (File file : arrFiles) {
			if (!".".equals(file.getName()) && !"..".equals(file.getName())) {
				AttachmentSnecNotificationDTO attachment = new AttachmentSnecNotificationDTO();
				attachment.setNombre(file.getName());
				// Ver si hace falta probar esto de alguna manera
				attachment.setEsFirmado("N");
				attachment.setContenido(getContentsFromFile(file));
				retVal.add(attachment);
			}
		}

		return retVal;
	}

	private void sendKORequest(int expectedCode) {
		try {
			connector.sendNotification(dto);
		} catch (SnecConnectorConnectionException e) {
			Assert.fail(e.getMessage());
		} catch (SnecConnectorOperationException e) {
			logger.debug("Operation Exception(" + e.getErrorCode() + "): " + e.getMessage());
			Assert.assertEquals(expectedCode, e.getErrorCode());
		}
	}

	private byte[] getContentsFromFile(File file) throws IOException {
		BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
		int length = (int) file.length();
		byte[] content = new byte[length];
		reader.read(content, 0, length);
		reader.close();
		return content;
	}
}
