
package es.msssi.snec.ws.dto.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import es.msssi.snec.nucleo.modelo.xsd.DocAdjunto;


/**
 * <p>Java class for RespuestaDetalleNotiC complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RespuestaDetalleNotiC">
 *   &lt;complexContent>
 *     &lt;extension base="{http://dto.ws.snec.msssi.es/xsd}RespuestaDetalleNoti">
 *       &lt;sequence>
 *         &lt;element name="CSV" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdfNotificacion" type="{http://modelo.nucleo.snec.msssi.es/xsd}DocAdjunto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaDetalleNotiC", propOrder = {
    "csv",
    "pdfNotificacion"
})
public class RespuestaDetalleNotiC
    extends RespuestaDetalleNoti
{

    @XmlElementRef(name = "CSV", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> csv;
    @XmlElementRef(name = "pdfNotificacion", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<DocAdjunto> pdfNotificacion;

    /**
     * Gets the value of the csv property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCSV() {
        return csv;
    }

    /**
     * Sets the value of the csv property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCSV(JAXBElement<String> value) {
        this.csv = value;
    }

    /**
     * Gets the value of the pdfNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DocAdjunto }{@code >}
     *     
     */
    public JAXBElement<DocAdjunto> getPdfNotificacion() {
        return pdfNotificacion;
    }

    /**
     * Sets the value of the pdfNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DocAdjunto }{@code >}
     *     
     */
    public void setPdfNotificacion(JAXBElement<DocAdjunto> value) {
        this.pdfNotificacion = value;
    }

}
