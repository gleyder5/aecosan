
package es.msssi.snec.nucleo.modelo.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocAdjunto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocAdjunto">
 *   &lt;complexContent>
 *     &lt;extension base="{http://modelo.nucleo.snec.msssi.es/xsd}GenericDTO">
 *       &lt;sequence>
 *         &lt;element name="contenido" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="csv" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esFirmado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPortafirma" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notifId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocAdjunto", propOrder = {
    "contenido",
    "csv",
    "esFirmado",
    "idPortafirma",
    "nombre",
    "notifId"
})
public class DocAdjunto
    extends GenericDTO
{

    @XmlElementRef(name = "contenido", namespace = "http://modelo.nucleo.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<byte[]> contenido;
    @XmlElementRef(name = "csv", namespace = "http://modelo.nucleo.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> csv;
    @XmlElementRef(name = "esFirmado", namespace = "http://modelo.nucleo.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> esFirmado;
    protected Integer idPortafirma;
    @XmlElementRef(name = "nombre", namespace = "http://modelo.nucleo.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> nombre;
    @XmlElementRef(name = "notifId", namespace = "http://modelo.nucleo.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> notifId;

    /**
     * Gets the value of the contenido property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getContenido() {
        return contenido;
    }

    /**
     * Sets the value of the contenido property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setContenido(JAXBElement<byte[]> value) {
        this.contenido = value;
    }

    /**
     * Gets the value of the csv property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCsv() {
        return csv;
    }

    /**
     * Sets the value of the csv property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCsv(JAXBElement<String> value) {
        this.csv = value;
    }

    /**
     * Gets the value of the esFirmado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEsFirmado() {
        return esFirmado;
    }

    /**
     * Sets the value of the esFirmado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEsFirmado(JAXBElement<String> value) {
        this.esFirmado = value;
    }

    /**
     * Gets the value of the idPortafirma property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdPortafirma() {
        return idPortafirma;
    }

    /**
     * Sets the value of the idPortafirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdPortafirma(Integer value) {
        this.idPortafirma = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNombre(JAXBElement<String> value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the notifId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotifId() {
        return notifId;
    }

    /**
     * Sets the value of the notifId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotifId(JAXBElement<String> value) {
        this.notifId = value;
    }

}
