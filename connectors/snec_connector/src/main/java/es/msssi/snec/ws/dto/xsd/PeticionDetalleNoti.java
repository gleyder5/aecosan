
package es.msssi.snec.ws.dto.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PeticionDetalleNoti complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PeticionDetalleNoti">
 *   &lt;complexContent>
 *     &lt;extension base="{http://dto.ws.snec.msssi.es/xsd}PeticionWS">
 *       &lt;sequence>
 *         &lt;element name="idNotif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PeticionDetalleNoti", propOrder = {
    "idNotif"
})
public class PeticionDetalleNoti
    extends PeticionWS
{

    @XmlElementRef(name = "idNotif", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> idNotif;

    /**
     * Gets the value of the idNotif property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdNotif() {
        return idNotif;
    }

    /**
     * Sets the value of the idNotif property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdNotif(JAXBElement<String> value) {
        this.idNotif = value;
    }

}
