
package es.msssi.snec.ws.dto.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RespuestaDetalleNoti complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RespuestaDetalleNoti">
 *   &lt;complexContent>
 *     &lt;extension base="{http://dto.ws.snec.msssi.es/xsd}RespuestaWS">
 *       &lt;sequence>
 *         &lt;element name="acuse_firmado" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="adjuntos" type="{http://dto.ws.snec.msssi.es/xsd}Adjunto" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="asunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contenido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_caducidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_envio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_lectura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_rechazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNotif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomProced" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaDetalleNoti", propOrder = {
    "acuseFirmado",
    "adjuntos",
    "asunto",
    "contenido",
    "estado",
    "fechaCaducidad",
    "fechaEnvio",
    "fechaLectura",
    "fechaRechazo",
    "idNotif",
    "nomProced",
    "nombre"
})
@XmlSeeAlso({
    RespuestaDetalleNotiC.class
})
public class RespuestaDetalleNoti
    extends RespuestaWS
{

    @XmlElementRef(name = "acuse_firmado", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<byte[]> acuseFirmado;
    @XmlElement(nillable = true)
    protected List<Adjunto> adjuntos;
    @XmlElementRef(name = "asunto", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> asunto;
    @XmlElementRef(name = "contenido", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> contenido;
    @XmlElementRef(name = "estado", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> estado;
    @XmlElementRef(name = "fecha_caducidad", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> fechaCaducidad;
    @XmlElementRef(name = "fecha_envio", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> fechaEnvio;
    @XmlElementRef(name = "fecha_lectura", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> fechaLectura;
    @XmlElementRef(name = "fecha_rechazo", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> fechaRechazo;
    @XmlElementRef(name = "idNotif", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> idNotif;
    @XmlElementRef(name = "nomProced", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> nomProced;
    @XmlElementRef(name = "nombre", namespace = "http://dto.ws.snec.msssi.es/xsd", type = JAXBElement.class)
    protected JAXBElement<String> nombre;

    /**
     * Gets the value of the acuseFirmado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public JAXBElement<byte[]> getAcuseFirmado() {
        return acuseFirmado;
    }

    /**
     * Sets the value of the acuseFirmado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link byte[]}{@code >}
     *     
     */
    public void setAcuseFirmado(JAXBElement<byte[]> value) {
        this.acuseFirmado = value;
    }

    /**
     * Gets the value of the adjuntos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the adjuntos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdjuntos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Adjunto }
     * 
     * 
     */
    public List<Adjunto> getAdjuntos() {
        if (adjuntos == null) {
            adjuntos = new ArrayList<Adjunto>();
        }
        return this.adjuntos;
    }

    /**
     * Gets the value of the asunto property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAsunto() {
        return asunto;
    }

    /**
     * Sets the value of the asunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAsunto(JAXBElement<String> value) {
        this.asunto = value;
    }

    /**
     * Gets the value of the contenido property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getContenido() {
        return contenido;
    }

    /**
     * Sets the value of the contenido property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setContenido(JAXBElement<String> value) {
        this.contenido = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEstado(JAXBElement<String> value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fechaCaducidad property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFechaCaducidad() {
        return fechaCaducidad;
    }

    /**
     * Sets the value of the fechaCaducidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFechaCaducidad(JAXBElement<String> value) {
        this.fechaCaducidad = value;
    }

    /**
     * Gets the value of the fechaEnvio property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFechaEnvio() {
        return fechaEnvio;
    }

    /**
     * Sets the value of the fechaEnvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFechaEnvio(JAXBElement<String> value) {
        this.fechaEnvio = value;
    }

    /**
     * Gets the value of the fechaLectura property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFechaLectura() {
        return fechaLectura;
    }

    /**
     * Sets the value of the fechaLectura property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFechaLectura(JAXBElement<String> value) {
        this.fechaLectura = value;
    }

    /**
     * Gets the value of the fechaRechazo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFechaRechazo() {
        return fechaRechazo;
    }

    /**
     * Sets the value of the fechaRechazo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFechaRechazo(JAXBElement<String> value) {
        this.fechaRechazo = value;
    }

    /**
     * Gets the value of the idNotif property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdNotif() {
        return idNotif;
    }

    /**
     * Sets the value of the idNotif property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdNotif(JAXBElement<String> value) {
        this.idNotif = value;
    }

    /**
     * Gets the value of the nomProced property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNomProced() {
        return nomProced;
    }

    /**
     * Sets the value of the nomProced property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNomProced(JAXBElement<String> value) {
        this.nomProced = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNombre(JAXBElement<String> value) {
        this.nombre = value;
    }

}
