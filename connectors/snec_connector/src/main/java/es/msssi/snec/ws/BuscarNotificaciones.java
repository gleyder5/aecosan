
package es.msssi.snec.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.snec.ws.dto.xsd.PeticionBusqueda;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="criterios" type="{http://dto.ws.snec.msssi.es/xsd}PeticionBusqueda" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "criterios"
})
@XmlRootElement(name = "buscarNotificaciones")
public class BuscarNotificaciones {

    @XmlElementRef(name = "criterios", namespace = "http://ws.snec.msssi.es", type = JAXBElement.class)
    protected JAXBElement<PeticionBusqueda> criterios;

    /**
     * Gets the value of the criterios property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PeticionBusqueda }{@code >}
     *     
     */
    public JAXBElement<PeticionBusqueda> getCriterios() {
        return criterios;
    }

    /**
     * Sets the value of the criterios property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PeticionBusqueda }{@code >}
     *     
     */
    public void setCriterios(JAXBElement<PeticionBusqueda> value) {
        this.criterios = value;
    }

}
