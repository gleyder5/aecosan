
package es.msssi.snec.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import es.msssi.snec.ws.dto.xsd.PeticionBusqueda;
import es.msssi.snec.ws.dto.xsd.PeticionDetalleNoti;
import es.msssi.snec.ws.dto.xsd.PeticionEnvioNoti;
import es.msssi.snec.ws.dto.xsd.RespuestaBusqueda;
import es.msssi.snec.ws.dto.xsd.RespuestaDetalleNoti;
import es.msssi.snec.ws.dto.xsd.RespuestaDetalleNotiC;
import es.msssi.snec.ws.dto.xsd.RespuestaEnvioNoti;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.msssi.snec.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BuscarNotificacionesCriterios_QNAME = new QName("http://ws.snec.msssi.es", "criterios");
    private final static QName _ConsultaNotificacionCompletaDetalleNotif_QNAME = new QName("http://ws.snec.msssi.es", "detalleNotif");
    private final static QName _EnviarNotifResponseReturn_QNAME = new QName("http://ws.snec.msssi.es", "return");
    private final static QName _ExceptionException_QNAME = new QName("http://ws.snec.msssi.es", "Exception");
    private final static QName _EnviarNotifPeticionEnvio_QNAME = new QName("http://ws.snec.msssi.es", "peticionEnvio");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.msssi.snec.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BuscarNotificacionesResponse }
     * 
     */
    public BuscarNotificacionesResponse createBuscarNotificacionesResponse() {
        return new BuscarNotificacionesResponse();
    }

    /**
     * Create an instance of {@link ConsultaDetalleNotifResponse }
     * 
     */
    public ConsultaDetalleNotifResponse createConsultaDetalleNotifResponse() {
        return new ConsultaDetalleNotifResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link SNECException }
     * 
     */
    public SNECException createSNECException() {
        return new SNECException();
    }

    /**
     * Create an instance of {@link ConsultaDetalleNotif }
     * 
     */
    public ConsultaDetalleNotif createConsultaDetalleNotif() {
        return new ConsultaDetalleNotif();
    }

    /**
     * Create an instance of {@link ConsultaNotificacionCompleta }
     * 
     */
    public ConsultaNotificacionCompleta createConsultaNotificacionCompleta() {
        return new ConsultaNotificacionCompleta();
    }

    /**
     * Create an instance of {@link ConsultaNotificacionCompletaResponse }
     * 
     */
    public ConsultaNotificacionCompletaResponse createConsultaNotificacionCompletaResponse() {
        return new ConsultaNotificacionCompletaResponse();
    }

    /**
     * Create an instance of {@link EnviarNotifResponse }
     * 
     */
    public EnviarNotifResponse createEnviarNotifResponse() {
        return new EnviarNotifResponse();
    }

    /**
     * Create an instance of {@link EnviarNotif }
     * 
     */
    public EnviarNotif createEnviarNotif() {
        return new EnviarNotif();
    }

    /**
     * Create an instance of {@link BuscarNotificaciones }
     * 
     */
    public BuscarNotificaciones createBuscarNotificaciones() {
        return new BuscarNotificaciones();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionBusqueda }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "criterios", scope = BuscarNotificaciones.class)
    public JAXBElement<PeticionBusqueda> createBuscarNotificacionesCriterios(PeticionBusqueda value) {
        return new JAXBElement<PeticionBusqueda>(_BuscarNotificacionesCriterios_QNAME, PeticionBusqueda.class, BuscarNotificaciones.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionDetalleNoti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "detalleNotif", scope = ConsultaNotificacionCompleta.class)
    public JAXBElement<PeticionDetalleNoti> createConsultaNotificacionCompletaDetalleNotif(PeticionDetalleNoti value) {
        return new JAXBElement<PeticionDetalleNoti>(_ConsultaNotificacionCompletaDetalleNotif_QNAME, PeticionDetalleNoti.class, ConsultaNotificacionCompleta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaEnvioNoti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "return", scope = EnviarNotifResponse.class)
    public JAXBElement<RespuestaEnvioNoti> createEnviarNotifResponseReturn(RespuestaEnvioNoti value) {
        return new JAXBElement<RespuestaEnvioNoti>(_EnviarNotifResponseReturn_QNAME, RespuestaEnvioNoti.class, EnviarNotifResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaDetalleNoti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "return", scope = ConsultaDetalleNotifResponse.class)
    public JAXBElement<RespuestaDetalleNoti> createConsultaDetalleNotifResponseReturn(RespuestaDetalleNoti value) {
        return new JAXBElement<RespuestaDetalleNoti>(_EnviarNotifResponseReturn_QNAME, RespuestaDetalleNoti.class, ConsultaDetalleNotifResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SNECException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "Exception", scope = Exception.class)
    public JAXBElement<SNECException> createExceptionException(SNECException value) {
        return new JAXBElement<SNECException>(_ExceptionException_QNAME, SNECException.class, Exception.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "Exception", scope = SNECException.class)
    public JAXBElement<Object> createSNECExceptionException(Object value) {
        return new JAXBElement<Object>(_ExceptionException_QNAME, Object.class, SNECException.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaBusqueda }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "return", scope = BuscarNotificacionesResponse.class)
    public JAXBElement<RespuestaBusqueda> createBuscarNotificacionesResponseReturn(RespuestaBusqueda value) {
        return new JAXBElement<RespuestaBusqueda>(_EnviarNotifResponseReturn_QNAME, RespuestaBusqueda.class, BuscarNotificacionesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionEnvioNoti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "peticionEnvio", scope = EnviarNotif.class)
    public JAXBElement<PeticionEnvioNoti> createEnviarNotifPeticionEnvio(PeticionEnvioNoti value) {
        return new JAXBElement<PeticionEnvioNoti>(_EnviarNotifPeticionEnvio_QNAME, PeticionEnvioNoti.class, EnviarNotif.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionDetalleNoti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "detalleNotif", scope = ConsultaDetalleNotif.class)
    public JAXBElement<PeticionDetalleNoti> createConsultaDetalleNotifDetalleNotif(PeticionDetalleNoti value) {
        return new JAXBElement<PeticionDetalleNoti>(_ConsultaNotificacionCompletaDetalleNotif_QNAME, PeticionDetalleNoti.class, ConsultaDetalleNotif.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RespuestaDetalleNotiC }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.snec.msssi.es", name = "return", scope = ConsultaNotificacionCompletaResponse.class)
    public JAXBElement<RespuestaDetalleNotiC> createConsultaNotificacionCompletaResponseReturn(RespuestaDetalleNotiC value) {
        return new JAXBElement<RespuestaDetalleNotiC>(_EnviarNotifResponseReturn_QNAME, RespuestaDetalleNotiC.class, ConsultaNotificacionCompletaResponse.class, value);
    }

}
