
package es.msssi.snec.ws.dto.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import es.msssi.snec.nucleo.modelo.xsd.DocAdjunto;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.msssi.snec.ws.dto.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RespuestaWSDescripcion_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "descripcion");
    private final static QName _RespuestaDetalleNotiFechaEnvio_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fecha_envio");
    private final static QName _RespuestaDetalleNotiContenido_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "contenido");
    private final static QName _RespuestaDetalleNotiFechaLectura_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fecha_lectura");
    private final static QName _RespuestaDetalleNotiAcuseFirmado_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "acuse_firmado");
    private final static QName _RespuestaDetalleNotiAsunto_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "asunto");
    private final static QName _RespuestaDetalleNotiNombre_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "nombre");
    private final static QName _RespuestaDetalleNotiEstado_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "estado");
    private final static QName _RespuestaDetalleNotiIdNotif_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "idNotif");
    private final static QName _RespuestaDetalleNotiNomProced_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "nomProced");
    private final static QName _RespuestaDetalleNotiFechaCaducidad_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fecha_caducidad");
    private final static QName _RespuestaDetalleNotiFechaRechazo_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fecha_rechazo");
    private final static QName _PeticionWSCodigoProce_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "codigoProce");
    private final static QName _PeticionWSUsername_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "username");
    private final static QName _AdjuntoEsFirmado_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "esFirmado");
    private final static QName _PeticionBusquedaEmail_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "email");
    private final static QName _PeticionBusquedaFechaHasta_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fechaHasta");
    private final static QName _PeticionBusquedaFechaDesde_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "fechaDesde");
    private final static QName _PeticionBusquedaDni_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "dni");
    private final static QName _PeticionEnvioNotiApellido1_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "apellido1");
    private final static QName _PeticionEnvioNotiApellido2_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "apellido2");
    private final static QName _DetalleConsultaUsuCrea_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "usu_crea");
    private final static QName _RespuestaDetalleNotiCPdfNotificacion_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "pdfNotificacion");
    private final static QName _RespuestaDetalleNotiCCSV_QNAME = new QName("http://dto.ws.snec.msssi.es/xsd", "CSV");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.msssi.snec.ws.dto.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RespuestaBusqueda }
     * 
     */
    public RespuestaBusqueda createRespuestaBusqueda() {
        return new RespuestaBusqueda();
    }

    /**
     * Create an instance of {@link RespuestaDetalleNoti }
     * 
     */
    public RespuestaDetalleNoti createRespuestaDetalleNoti() {
        return new RespuestaDetalleNoti();
    }

    /**
     * Create an instance of {@link PeticionDetalleNoti }
     * 
     */
    public PeticionDetalleNoti createPeticionDetalleNoti() {
        return new PeticionDetalleNoti();
    }

    /**
     * Create an instance of {@link RespuestaDetalleNotiC }
     * 
     */
    public RespuestaDetalleNotiC createRespuestaDetalleNotiC() {
        return new RespuestaDetalleNotiC();
    }

    /**
     * Create an instance of {@link RespuestaEnvioNoti }
     * 
     */
    public RespuestaEnvioNoti createRespuestaEnvioNoti() {
        return new RespuestaEnvioNoti();
    }

    /**
     * Create an instance of {@link PeticionEnvioNoti }
     * 
     */
    public PeticionEnvioNoti createPeticionEnvioNoti() {
        return new PeticionEnvioNoti();
    }

    /**
     * Create an instance of {@link PeticionBusqueda }
     * 
     */
    public PeticionBusqueda createPeticionBusqueda() {
        return new PeticionBusqueda();
    }

    /**
     * Create an instance of {@link Adjunto }
     * 
     */
    public Adjunto createAdjunto() {
        return new Adjunto();
    }

    /**
     * Create an instance of {@link PeticionWS }
     * 
     */
    public PeticionWS createPeticionWS() {
        return new PeticionWS();
    }

    /**
     * Create an instance of {@link DetalleConsulta }
     * 
     */
    public DetalleConsulta createDetalleConsulta() {
        return new DetalleConsulta();
    }

    /**
     * Create an instance of {@link RespuestaWS }
     * 
     */
    public RespuestaWS createRespuestaWS() {
        return new RespuestaWS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "descripcion", scope = RespuestaWS.class)
    public JAXBElement<String> createRespuestaWSDescripcion(String value) {
        return new JAXBElement<String>(_RespuestaWSDescripcion_QNAME, String.class, RespuestaWS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fecha_envio", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiFechaEnvio(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiFechaEnvio_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "contenido", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiContenido(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiContenido_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fecha_lectura", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiFechaLectura(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiFechaLectura_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "acuse_firmado", scope = RespuestaDetalleNoti.class)
    public JAXBElement<byte[]> createRespuestaDetalleNotiAcuseFirmado(byte[] value) {
        return new JAXBElement<byte[]>(_RespuestaDetalleNotiAcuseFirmado_QNAME, byte[].class, RespuestaDetalleNoti.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "asunto", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiAsunto(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiAsunto_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nombre", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiNombre(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNombre_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "estado", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiEstado(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiEstado_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "idNotif", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiIdNotif(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiIdNotif_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nomProced", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiNomProced(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNomProced_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fecha_caducidad", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiFechaCaducidad(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiFechaCaducidad_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fecha_rechazo", scope = RespuestaDetalleNoti.class)
    public JAXBElement<String> createRespuestaDetalleNotiFechaRechazo(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiFechaRechazo_QNAME, String.class, RespuestaDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "codigoProce", scope = PeticionWS.class)
    public JAXBElement<String> createPeticionWSCodigoProce(String value) {
        return new JAXBElement<String>(_PeticionWSCodigoProce_QNAME, String.class, PeticionWS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "username", scope = PeticionWS.class)
    public JAXBElement<String> createPeticionWSUsername(String value) {
        return new JAXBElement<String>(_PeticionWSUsername_QNAME, String.class, PeticionWS.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "idNotif", scope = PeticionDetalleNoti.class)
    public JAXBElement<String> createPeticionDetalleNotiIdNotif(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiIdNotif_QNAME, String.class, PeticionDetalleNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "contenido", scope = Adjunto.class)
    public JAXBElement<byte[]> createAdjuntoContenido(byte[] value) {
        return new JAXBElement<byte[]>(_RespuestaDetalleNotiContenido_QNAME, byte[].class, Adjunto.class, ((byte[]) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nombre", scope = Adjunto.class)
    public JAXBElement<String> createAdjuntoNombre(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNombre_QNAME, String.class, Adjunto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "esFirmado", scope = Adjunto.class)
    public JAXBElement<String> createAdjuntoEsFirmado(String value) {
        return new JAXBElement<String>(_AdjuntoEsFirmado_QNAME, String.class, Adjunto.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "email", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaEmail(String value) {
        return new JAXBElement<String>(_PeticionBusquedaEmail_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fechaHasta", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaFechaHasta(String value) {
        return new JAXBElement<String>(_PeticionBusquedaFechaHasta_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fechaDesde", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaFechaDesde(String value) {
        return new JAXBElement<String>(_PeticionBusquedaFechaDesde_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nombre", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaNombre(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNombre_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "idNotif", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaIdNotif(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiIdNotif_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "dni", scope = PeticionBusqueda.class)
    public JAXBElement<String> createPeticionBusquedaDni(String value) {
        return new JAXBElement<String>(_PeticionBusquedaDni_QNAME, String.class, PeticionBusqueda.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "email", scope = RespuestaEnvioNoti.class)
    public JAXBElement<String> createRespuestaEnvioNotiEmail(String value) {
        return new JAXBElement<String>(_PeticionBusquedaEmail_QNAME, String.class, RespuestaEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "idNotif", scope = RespuestaEnvioNoti.class)
    public JAXBElement<String> createRespuestaEnvioNotiIdNotif(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiIdNotif_QNAME, String.class, RespuestaEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "email", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiEmail(String value) {
        return new JAXBElement<String>(_PeticionBusquedaEmail_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "contenido", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiContenido(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiContenido_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "apellido1", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiApellido1(String value) {
        return new JAXBElement<String>(_PeticionEnvioNotiApellido1_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "asunto", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiAsunto(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiAsunto_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nombre", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiNombre(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNombre_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "apellido2", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiApellido2(String value) {
        return new JAXBElement<String>(_PeticionEnvioNotiApellido2_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "dni", scope = PeticionEnvioNoti.class)
    public JAXBElement<String> createPeticionEnvioNotiDni(String value) {
        return new JAXBElement<String>(_PeticionBusquedaDni_QNAME, String.class, PeticionEnvioNoti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "fecha_envio", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaFechaEnvio(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiFechaEnvio_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "email", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaEmail(String value) {
        return new JAXBElement<String>(_PeticionBusquedaEmail_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "usu_crea", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaUsuCrea(String value) {
        return new JAXBElement<String>(_DetalleConsultaUsuCrea_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "nombre", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaNombre(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiNombre_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "estado", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaEstado(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiEstado_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "idNotif", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaIdNotif(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiIdNotif_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "dni", scope = DetalleConsulta.class)
    public JAXBElement<String> createDetalleConsultaDni(String value) {
        return new JAXBElement<String>(_PeticionBusquedaDni_QNAME, String.class, DetalleConsulta.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocAdjunto }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "pdfNotificacion", scope = RespuestaDetalleNotiC.class)
    public JAXBElement<DocAdjunto> createRespuestaDetalleNotiCPdfNotificacion(DocAdjunto value) {
        return new JAXBElement<DocAdjunto>(_RespuestaDetalleNotiCPdfNotificacion_QNAME, DocAdjunto.class, RespuestaDetalleNotiC.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://dto.ws.snec.msssi.es/xsd", name = "CSV", scope = RespuestaDetalleNotiC.class)
    public JAXBElement<String> createRespuestaDetalleNotiCCSV(String value) {
        return new JAXBElement<String>(_RespuestaDetalleNotiCCSV_QNAME, String.class, RespuestaDetalleNotiC.class, value);
    }

}
