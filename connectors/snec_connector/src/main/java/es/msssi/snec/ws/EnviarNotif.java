
package es.msssi.snec.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.snec.ws.dto.xsd.PeticionEnvioNoti;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="peticionEnvio" type="{http://dto.ws.snec.msssi.es/xsd}PeticionEnvioNoti" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "peticionEnvio"
})
@XmlRootElement(name = "enviarNotif")
public class EnviarNotif {

    @XmlElementRef(name = "peticionEnvio", namespace = "http://ws.snec.msssi.es", type = JAXBElement.class)
    protected JAXBElement<PeticionEnvioNoti> peticionEnvio;

    /**
     * Gets the value of the peticionEnvio property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PeticionEnvioNoti }{@code >}
     *     
     */
    public JAXBElement<PeticionEnvioNoti> getPeticionEnvio() {
        return peticionEnvio;
    }

    /**
     * Sets the value of the peticionEnvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PeticionEnvioNoti }{@code >}
     *     
     */
    public void setPeticionEnvio(JAXBElement<PeticionEnvioNoti> value) {
        this.peticionEnvio = value;
    }

}
