
package es.msssi.snec.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.snec.ws.dto.xsd.PeticionDetalleNoti;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="detalleNotif" type="{http://dto.ws.snec.msssi.es/xsd}PeticionDetalleNoti" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "detalleNotif"
})
@XmlRootElement(name = "consultaDetalleNotif")
public class ConsultaDetalleNotif {

    @XmlElementRef(name = "detalleNotif", namespace = "http://ws.snec.msssi.es", type = JAXBElement.class)
    protected JAXBElement<PeticionDetalleNoti> detalleNotif;

    /**
     * Gets the value of the detalleNotif property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link PeticionDetalleNoti }{@code >}
     *     
     */
    public JAXBElement<PeticionDetalleNoti> getDetalleNotif() {
        return detalleNotif;
    }

    /**
     * Sets the value of the detalleNotif property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link PeticionDetalleNoti }{@code >}
     *     
     */
    public void setDetalleNotif(JAXBElement<PeticionDetalleNoti> value) {
        this.detalleNotif = value;
    }

}
