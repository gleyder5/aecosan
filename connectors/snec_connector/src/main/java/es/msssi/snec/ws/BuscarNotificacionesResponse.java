
package es.msssi.snec.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.snec.ws.dto.xsd.RespuestaBusqueda;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://dto.ws.snec.msssi.es/xsd}RespuestaBusqueda" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "_return"
})
@XmlRootElement(name = "buscarNotificacionesResponse")
public class BuscarNotificacionesResponse {

    @XmlElementRef(name = "return", namespace = "http://ws.snec.msssi.es", type = JAXBElement.class)
    protected JAXBElement<RespuestaBusqueda> _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RespuestaBusqueda }{@code >}
     *     
     */
    public JAXBElement<RespuestaBusqueda> getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RespuestaBusqueda }{@code >}
     *     
     */
    public void setReturn(JAXBElement<RespuestaBusqueda> value) {
        this._return = value;
    }

}
