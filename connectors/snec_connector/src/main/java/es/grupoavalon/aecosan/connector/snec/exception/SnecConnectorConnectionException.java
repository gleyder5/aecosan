package es.grupoavalon.aecosan.connector.snec.exception;

public class SnecConnectorConnectionException extends SnecConnectorException {

	private static final long serialVersionUID = 1L;


	public SnecConnectorConnectionException(String message) {
		super(message);
	}
	
	public SnecConnectorConnectionException(Throwable t) {
		super(t);
	}
}

