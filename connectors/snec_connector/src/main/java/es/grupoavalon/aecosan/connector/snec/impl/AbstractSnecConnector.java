package es.grupoavalon.aecosan.connector.snec.impl;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorConnectionException;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorException;
import es.msssi.snec.ws.WSSNECServicePortType;

public abstract class AbstractSnecConnector {
	protected static final Logger logger = LoggerFactory.getLogger(AbstractSnecConnector.class);

	protected static URL instanciateUrl(String url) {
		URL wsUrl;
		try {
			wsUrl = new URL(url);
		} catch (MalformedURLException e) {
			throw new SnecConnectorConnectionException("Invalid URL:" + url);
		}
		return wsUrl;
	}

	protected <T> Object[] invokeClientProxy(WSSNECServicePortType port, T peticionEnvio, String method) throws Exception {
		Client client = setupClientProxy(port);
		return client.invoke(method, peticionEnvio);
	}

	protected static Client setupClientProxy(WSSNECServicePortType port) {
		Client clientProxy = ClientProxy.getClient(port);
		setupConduit(clientProxy);
		return clientProxy;
	}

	protected static void setupConduit(Client clientProxy) {
		HTTPConduit httpConduit = (HTTPConduit) clientProxy.getConduit();
		HTTPClientPolicy policy = httpConduit.getClient();
		policy.setAutoRedirect(true);
		policy.setAllowChunking(false);
		httpConduit.setClient(policy);
	}



	protected static void handleException(Exception e) {
		if (e instanceof SnecConnectorException) {
			throw (SnecConnectorException) e;
		} else {
			logger.error(SnecConnectorException.GENERAL_ERROR + e.getLocalizedMessage(), e);
			throw new SnecConnectorException(SnecConnectorException.GENERAL_ERROR + e.getLocalizedMessage(), e);
		}
	}

}
