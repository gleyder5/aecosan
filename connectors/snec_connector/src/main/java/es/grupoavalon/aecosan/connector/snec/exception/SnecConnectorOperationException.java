package es.grupoavalon.aecosan.connector.snec.exception;

public class SnecConnectorOperationException extends SnecConnectorException {
	private static final long serialVersionUID = 1L;

	private final int errorCode;
	
	public SnecConnectorOperationException(int code, String message) {
		super("code:" + code + "-" + message);
		this.errorCode = code;
	}

	public int getErrorCode() {
		return this.errorCode;
	}
}
