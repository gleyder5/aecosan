package es.grupoavalon.aecosan.connector.snec.dto;

import javax.xml.bind.annotation.XmlElement;

public class AttachmentSnecNotificationDTO {

	@XmlElement(name = "contenido")
	private byte[] contenido;
	
	@XmlElement(name = "esFirmado")
	private String esFirmado;
	
	@XmlElement(name = "nombre")
	private String nombre;
	
	public byte[] getContenido() {
		return contenido;
	}

	public void setContenido(byte[] contenido) {
		this.contenido = contenido;
	}
	public String getEsFirmado() {
		return esFirmado;
	}
	public void setEsFirmado(String esFirmado) {
		this.esFirmado = esFirmado;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "AttachmentSnecNotificationDTO [esFirmado=" + esFirmado + ", nombre=" + nombre + "]";
	}
	
	
}
