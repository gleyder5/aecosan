package es.grupoavalon.aecosan.connector.snec.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SnecNotificationDTO {
	@XmlElement(name = "codigoProce", required = true)
	private String codigoProce;
	
	@XmlElement(name = "username", required = true)
	private String username;
	
	@XmlElement(name = "adjuntos")
	private List<AttachmentSnecNotificationDTO> adjuntos = new ArrayList<AttachmentSnecNotificationDTO>();
	
	@XmlElement(name = "apellido1", required = true)
	private String apellido1;
	
	@XmlElement(name = "apellido2")
	private String apellido2;
	
	@XmlElement(name = "asunto")
	private String asunto;
	
	@XmlElement(name = "contenido")
	private String contenido;
	
	@XmlElement(name = "dni")
	private String dni;
	
	@XmlElement(name = "email")
	private String email;
	
	@XmlElement(name = "nombre")
	private String nombre;
	
	@XmlElement(name = "url", required = true)
	private String url;
	
	public String getUrl() {
		return this.url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getCodigoProce() {
		return codigoProce;
	}
	public void setCodigoProce(String codigoProce) {
		this.codigoProce = codigoProce;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<AttachmentSnecNotificationDTO> getAdjuntos() {
		return adjuntos;
	}
	public void setAdjuntos(List<AttachmentSnecNotificationDTO> adjuntos) {
		this.adjuntos = adjuntos;
	}
	public String getApellido1() {
		return apellido1;
	}
	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}
	public String getApellido2() {
		return apellido2;
	}
	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "SnecNotificationDTO [codigoProce=" + codigoProce + ", username=" + username + ", adjuntos=" + adjuntos + ", apellido1=" + apellido1 + ", apellido2=" + apellido2 + ", asunto=" + asunto
				+ ", contenido=" + contenido + ", dni=" + dni + ", email=" + email + ", nombre=" + nombre + ", url=" + url + "]";
	}

}
