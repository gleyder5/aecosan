package es.grupoavalon.aecosan.connector.snec;

import es.grupoavalon.aecosan.connector.snec.dto.SnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecResponseDTO;

public interface SnecConnectorApi {

	SnecResponseDTO sendNotification(SnecNotificationDTO notification);
}
