package es.grupoavalon.aecosan.connector.snec.impl;

import java.net.URL;
import java.util.Arrays;

import es.grupoavalon.aecosan.connector.snec.SnecConnectorApi;
import es.grupoavalon.aecosan.connector.snec.SnecConnectorResponseLibrary;
import es.grupoavalon.aecosan.connector.snec.dto.AttachmentSnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecResponseDTO;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorException;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorOperationException;
import es.msssi.snec.ws.WSSNECService;
import es.msssi.snec.ws.WSSNECServicePortType;
import es.msssi.snec.ws.dto.xsd.Adjunto;
import es.msssi.snec.ws.dto.xsd.ObjectFactory;
import es.msssi.snec.ws.dto.xsd.PeticionEnvioNoti;
import es.msssi.snec.ws.dto.xsd.RespuestaEnvioNoti;

public class SnecConnectorApiImpl extends AbstractSnecConnector implements SnecConnectorApi {

	@Override
	public SnecResponseDTO sendNotification(SnecNotificationDTO notification) {
		SnecResponseDTO responseDTO = null;
		try {

			URL wsUrl = instanciateUrl(notification.getUrl());
			WSSNECService service = new WSSNECService(wsUrl);
			WSSNECServicePortType port = service.getWSSNECServiceHttpSoap12Endpoint();
			PeticionEnvioNoti peticionEnvio = getPeticionEnvioFromDTO(notification);

			responseDTO = executeWsCallEnviarNotif(port, peticionEnvio);

		} catch (Exception e) {
			handleException(e);
		}
		return responseDTO;
	}


	private PeticionEnvioNoti getPeticionEnvioFromDTO(SnecNotificationDTO notification) {
		PeticionEnvioNoti retVal = new PeticionEnvioNoti();
		ObjectFactory factory = new ObjectFactory();
		retVal.setApellido1(factory.createPeticionEnvioNotiApellido1(notification.getApellido1()));
		retVal.setApellido2(factory.createPeticionEnvioNotiApellido2(notification.getApellido2()));
		retVal.setAsunto(factory.createPeticionEnvioNotiAsunto(notification.getAsunto()));
		retVal.setCodigoProce(factory.createPeticionWSCodigoProce(notification.getCodigoProce()));
		retVal.setContenido(factory.createPeticionEnvioNotiContenido(notification.getContenido()));
		retVal.setDni(factory.createPeticionEnvioNotiDni(notification.getDni()));
		retVal.setEmail(factory.createPeticionEnvioNotiEmail(notification.getEmail()));
		retVal.setNombre(factory.createPeticionEnvioNotiNombre(notification.getNombre()));
		retVal.setUsername(factory.createPeticionWSUsername(notification.getUsername()));
		for (AttachmentSnecNotificationDTO attachment : notification.getAdjuntos()) {
			retVal.getAdjuntos().add(getAdjuntoFromDTO(attachment));
		}
		logger.info("Generating request for SNEC with data:" + notification);
		return retVal;
	}

	private Adjunto getAdjuntoFromDTO(AttachmentSnecNotificationDTO attachment) {
		ObjectFactory factory = new ObjectFactory();
		Adjunto retVal = factory.createAdjunto();
		retVal.setNombre(factory.createAdjuntoNombre(attachment.getNombre()));
		retVal.setEsFirmado(factory.createAdjuntoEsFirmado(attachment.getEsFirmado()));
		retVal.setContenido(factory.createAdjuntoContenido(attachment.getContenido()));
		return retVal;
	}

	private SnecResponseDTO executeWsCallEnviarNotif(WSSNECServicePortType port, PeticionEnvioNoti peticionEnvio) {
		SnecResponseDTO responseDTO = null;
		try {
			Object[] responseRaw = invokeClientProxyEnviarNotif(port, peticionEnvio);
			RespuestaEnvioNoti response = getRespuestaEnvioNotiFromRaw(responseRaw);
			responseDTO = processServerResponse(response);
		} catch (Exception e) {
			handleException(e);
		}
		logger.info("Generating response from SNEC with data:" + responseDTO);
		return responseDTO;
	}

	private Object[] invokeClientProxyEnviarNotif(WSSNECServicePortType port, PeticionEnvioNoti peticionEnvio) throws Exception {

		return super.invokeClientProxy(port, peticionEnvio, "enviarNotif");
	}


	private static RespuestaEnvioNoti getRespuestaEnvioNotiFromRaw(Object[] responseRaw) {
		RespuestaEnvioNoti response;

		if (responseRaw != null && responseRaw.length > 0) {
			response = (RespuestaEnvioNoti) responseRaw[0];
		} else {
			throw new SnecConnectorException(SnecConnectorException.RESPONSE_UNEXPECTED + Arrays.toString(responseRaw));
		}

		return response;
	}

	private static SnecResponseDTO processServerResponse(RespuestaEnvioNoti response) {
		SnecResponseDTO responseDTO;
		if (response.getCodigoRespuesta() != SnecConnectorResponseLibrary.NOTIFICATION_SUCCESSFULLY_SENT && response.getCodigoRespuesta() != SnecConnectorResponseLibrary.SUCCESSFUL_OPERATION) {
			logger.error("Response code was not successfull response code[" + response.getCodigoRespuesta() + "] description [" + response.getDescripcion().getValue() + "]");
			throw new SnecConnectorOperationException(response.getCodigoRespuesta(), response.getDescripcion().getValue());

		} else {
			logger.info("response code was successfull response code[" + response.getCodigoRespuesta() + "]");
			responseDTO = new SnecResponseDTO();
			if (response.getEmail() != null) {
				responseDTO.setEmail(response.getEmail().getValue());
			}
			if (response.getIdNotif() != null) {
				responseDTO.setNotificationNumber(response.getIdNotif().getValue());
			}
		}
		return responseDTO;
	}

}
