package es.grupoavalon.aecosan.connector.snec.dto;

public class SnecResponseDTO {

	private String email;
	private String notificationNumber;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNotificationNumber() {
		return notificationNumber;
	}

	public void setNotificationNumber(String notificationNumber) {
		this.notificationNumber = notificationNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((notificationNumber == null) ? 0 : notificationNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SnecResponseDTO other = (SnecResponseDTO) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (notificationNumber == null) {
			if (other.notificationNumber != null) {
				return false;
			}
		} else if (!notificationNumber.equals(other.notificationNumber)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SnecResponseDTO [email=" + email + ", notificationNumber=" + notificationNumber + "]";
	}
}
