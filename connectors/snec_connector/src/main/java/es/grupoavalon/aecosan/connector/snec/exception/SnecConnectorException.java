package es.grupoavalon.aecosan.connector.snec.exception;

public class SnecConnectorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public static final String GENERAL_ERROR = "Can not execute the SNEC notification due an error:";
	
	public static final String RESPONSE_UNEXPECTED = "Can not process the response from the server was unexpected";

	public SnecConnectorException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SnecConnectorException(String message) {
		super(message);
	}
	
	public SnecConnectorException(Throwable t) {
		super(t);
	}
}
