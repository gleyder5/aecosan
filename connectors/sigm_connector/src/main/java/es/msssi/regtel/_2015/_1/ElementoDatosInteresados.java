
package es.msssi.regtel._2015._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for elementoDatosInteresados complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="elementoDatosInteresados">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interesadoJuridico" type="{http://www.msssi.es/Regtel/2015/1}interesadoJuridico" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="interesadoFisico" type="{http://www.msssi.es/Regtel/2015/1}interesadoFisico" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "elementoDatosInteresados", propOrder = {
    "interesadoJuridico",
    "interesadoFisico"
})
public class ElementoDatosInteresados {

    @XmlElement(nillable = true)
    protected List<InteresadoJuridico> interesadoJuridico;
    @XmlElement(nillable = true)
    protected List<InteresadoFisico> interesadoFisico;

    /**
     * Gets the value of the interesadoJuridico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interesadoJuridico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInteresadoJuridico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InteresadoJuridico }
     * 
     * 
     */
    public List<InteresadoJuridico> getInteresadoJuridico() {
        if (interesadoJuridico == null) {
            interesadoJuridico = new ArrayList<InteresadoJuridico>();
        }
        return this.interesadoJuridico;
    }

    /**
     * Gets the value of the interesadoFisico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the interesadoFisico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInteresadoFisico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InteresadoFisico }
     * 
     * 
     */
    public List<InteresadoFisico> getInteresadoFisico() {
        if (interesadoFisico == null) {
            interesadoFisico = new ArrayList<InteresadoFisico>();
        }
        return this.interesadoFisico;
    }

}
