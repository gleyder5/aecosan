
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaSigm complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaSigm">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="estadoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorResponse" type="{http://www.msssi.es/Regtel/2015/1}errorResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaSigm", propOrder = {
    "estadoRespuesta",
    "errorResponse"
})
@XmlSeeAlso({
    RespuestaRegistro.class,
    RespuestaGenerarAcuse.class,
    RespuestaRecuperarDocumento.class,
    RespuestaDetalle.class,
    RespuestaBusqueda.class,
    RespuestaAdjuntarDocumento.class
})
public class RespuestaSigm {

    @XmlElement(required = true)
    protected String estadoRespuesta;
    protected ErrorResponse errorResponse;

    /**
     * Gets the value of the estadoRespuesta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoRespuesta() {
        return estadoRespuesta;
    }

    /**
     * Sets the value of the estadoRespuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoRespuesta(String value) {
        this.estadoRespuesta = value;
    }

    /**
     * Gets the value of the errorResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorResponse }
     *     
     */
    public ErrorResponse getErrorResponse() {
        return errorResponse;
    }

    /**
     * Sets the value of the errorResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorResponse }
     *     
     */
    public void setErrorResponse(ErrorResponse value) {
        this.errorResponse = value;
    }

}
