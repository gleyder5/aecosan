
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for historicoIntercambioRegistral complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="historicoIntercambioRegistral">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="historicoIntercambioRegistralEntrada" type="{http://www.msssi.es/Regtel/2015/1}historicoIntercambioRegistralEntrada"/>
 *         &lt;element name="historicoIntercambioRegistralSalida" type="{http://www.msssi.es/Regtel/2015/1}historicoIntercambioRegistralSalida"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historicoIntercambioRegistral", propOrder = {
    "historicoIntercambioRegistralEntrada",
    "historicoIntercambioRegistralSalida"
})
public class HistoricoIntercambioRegistral {

    @XmlElement(required = true)
    protected HistoricoIntercambioRegistralEntrada historicoIntercambioRegistralEntrada;
    @XmlElement(required = true)
    protected HistoricoIntercambioRegistralSalida historicoIntercambioRegistralSalida;

    /**
     * Gets the value of the historicoIntercambioRegistralEntrada property.
     * 
     * @return
     *     possible object is
     *     {@link HistoricoIntercambioRegistralEntrada }
     *     
     */
    public HistoricoIntercambioRegistralEntrada getHistoricoIntercambioRegistralEntrada() {
        return historicoIntercambioRegistralEntrada;
    }

    /**
     * Sets the value of the historicoIntercambioRegistralEntrada property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoricoIntercambioRegistralEntrada }
     *     
     */
    public void setHistoricoIntercambioRegistralEntrada(HistoricoIntercambioRegistralEntrada value) {
        this.historicoIntercambioRegistralEntrada = value;
    }

    /**
     * Gets the value of the historicoIntercambioRegistralSalida property.
     * 
     * @return
     *     possible object is
     *     {@link HistoricoIntercambioRegistralSalida }
     *     
     */
    public HistoricoIntercambioRegistralSalida getHistoricoIntercambioRegistralSalida() {
        return historicoIntercambioRegistralSalida;
    }

    /**
     * Sets the value of the historicoIntercambioRegistralSalida property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoricoIntercambioRegistralSalida }
     *     
     */
    public void setHistoricoIntercambioRegistralSalida(HistoricoIntercambioRegistralSalida value) {
        this.historicoIntercambioRegistralSalida = value;
    }

}
