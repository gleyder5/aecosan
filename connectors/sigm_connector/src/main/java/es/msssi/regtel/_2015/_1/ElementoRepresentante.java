
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for elementoRepresentante complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="elementoRepresentante">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="representanteJuridico" type="{http://www.msssi.es/Regtel/2015/1}representanteJuridico" minOccurs="0"/>
 *         &lt;element name="representanteFisico" type="{http://www.msssi.es/Regtel/2015/1}representanteFisico" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "elementoRepresentante", propOrder = {
    "representanteJuridico",
    "representanteFisico"
})
public class ElementoRepresentante {

    protected RepresentanteJuridico representanteJuridico;
    protected RepresentanteFisico representanteFisico;

    /**
     * Gets the value of the representanteJuridico property.
     * 
     * @return
     *     possible object is
     *     {@link RepresentanteJuridico }
     *     
     */
    public RepresentanteJuridico getRepresentanteJuridico() {
        return representanteJuridico;
    }

    /**
     * Sets the value of the representanteJuridico property.
     * 
     * @param value
     *     allowed object is
     *     {@link RepresentanteJuridico }
     *     
     */
    public void setRepresentanteJuridico(RepresentanteJuridico value) {
        this.representanteJuridico = value;
    }

    /**
     * Gets the value of the representanteFisico property.
     * 
     * @return
     *     possible object is
     *     {@link RepresentanteFisico }
     *     
     */
    public RepresentanteFisico getRepresentanteFisico() {
        return representanteFisico;
    }

    /**
     * Sets the value of the representanteFisico property.
     * 
     * @param value
     *     allowed object is
     *     {@link RepresentanteFisico }
     *     
     */
    public void setRepresentanteFisico(RepresentanteFisico value) {
        this.representanteFisico = value;
    }

}
