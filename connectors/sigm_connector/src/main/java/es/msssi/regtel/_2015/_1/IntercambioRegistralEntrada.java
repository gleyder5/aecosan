
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for intercambioRegistralEntrada complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="intercambioRegistralEntrada">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechaIntercambio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaEstado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="trazasIREntrada" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "intercambioRegistralEntrada", propOrder = {
    "fechaIntercambio",
    "estado",
    "fechaEstado",
    "usuario",
    "trazasIREntrada"
})
public class IntercambioRegistralEntrada {

    @XmlElement(required = true)
    protected String fechaIntercambio;
    @XmlElement(required = true)
    protected String estado;
    @XmlElement(required = true)
    protected String fechaEstado;
    @XmlElement(required = true)
    protected String usuario;
    @XmlElement(required = true)
    protected String trazasIREntrada;

    /**
     * Gets the value of the fechaIntercambio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaIntercambio() {
        return fechaIntercambio;
    }

    /**
     * Sets the value of the fechaIntercambio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaIntercambio(String value) {
        this.fechaIntercambio = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fechaEstado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaEstado() {
        return fechaEstado;
    }

    /**
     * Sets the value of the fechaEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaEstado(String value) {
        this.fechaEstado = value;
    }

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Gets the value of the trazasIREntrada property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTrazasIREntrada() {
        return trazasIREntrada;
    }

    /**
     * Sets the value of the trazasIREntrada property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTrazasIREntrada(String value) {
        this.trazasIREntrada = value;
    }

}
