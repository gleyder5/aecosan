
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaDetalle complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaDetalle">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.msssi.es/Regtel/2015/1}respuestaSigm">
 *       &lt;sequence>
 *         &lt;element name="registro" type="{http://www.msssi.es/Regtel/2015/1}registro"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaDetalle", propOrder = {
    "registro"
})
public class RespuestaDetalle
    extends RespuestaSigm
{

    @XmlElement(required = true)
    protected Registro registro;

    /**
     * Gets the value of the registro property.
     * 
     * @return
     *     possible object is
     *     {@link Registro }
     *     
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * Sets the value of the registro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Registro }
     *     
     */
    public void setRegistro(Registro value) {
        this.registro = value;
    }

}
