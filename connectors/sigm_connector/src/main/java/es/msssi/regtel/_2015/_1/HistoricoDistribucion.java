
package es.msssi.regtel._2015._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for historicoDistribucion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="historicoDistribucion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="distribucionRegistro" type="{http://www.msssi.es/Regtel/2015/1}distribucionRegistro" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historicoDistribucion", propOrder = {
    "distribucionRegistro"
})
public class HistoricoDistribucion {

    @XmlElement(required = true)
    protected List<DistribucionRegistro> distribucionRegistro;

    /**
     * Gets the value of the distribucionRegistro property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the distribucionRegistro property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDistribucionRegistro().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DistribucionRegistro }
     * 
     * 
     */
    public List<DistribucionRegistro> getDistribucionRegistro() {
        if (distribucionRegistro == null) {
            distribucionRegistro = new ArrayList<DistribucionRegistro>();
        }
        return this.distribucionRegistro;
    }

}
