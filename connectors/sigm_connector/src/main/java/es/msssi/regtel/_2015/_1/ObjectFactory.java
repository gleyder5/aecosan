
package es.msssi.regtel._2015._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.msssi.regtel._2015._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PeticionDetalle_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionDetalle");
    private final static QName _PeticionGenerarAcuse_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionGenerarAcuse");
    private final static QName _PeticionRecuperarDocumento_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionRecuperarDocumento");
    private final static QName _PeticionBusqueda_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionBusqueda");
    private final static QName _PeticionAdjuntarDocumento_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionAdjuntarDocumento");
    private final static QName _PeticionRegistro_QNAME = new QName("http://www.msssi.es/Regtel/2015/1", "peticionRegistro");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.msssi.regtel._2015._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PeticionGenerarAcuse }
     * 
     */
    public PeticionGenerarAcuse createPeticionGenerarAcuse() {
        return new PeticionGenerarAcuse();
    }

    /**
     * Create an instance of {@link PeticionAdjuntarDocumento }
     * 
     */
    public PeticionAdjuntarDocumento createPeticionAdjuntarDocumento() {
        return new PeticionAdjuntarDocumento();
    }

    /**
     * Create an instance of {@link PeticionDetalle }
     * 
     */
    public PeticionDetalle createPeticionDetalle() {
        return new PeticionDetalle();
    }

    /**
     * Create an instance of {@link PeticionBusqueda }
     * 
     */
    public PeticionBusqueda createPeticionBusqueda() {
        return new PeticionBusqueda();
    }

    /**
     * Create an instance of {@link PeticionRegistro }
     * 
     */
    public PeticionRegistro createPeticionRegistro() {
        return new PeticionRegistro();
    }

    /**
     * Create an instance of {@link PeticionRecuperarDocumento }
     * 
     */
    public PeticionRecuperarDocumento createPeticionRecuperarDocumento() {
        return new PeticionRecuperarDocumento();
    }

    /**
     * Create an instance of {@link Ficheros }
     * 
     */
    public Ficheros createFicheros() {
        return new Ficheros();
    }

    /**
     * Create an instance of {@link Documentos }
     * 
     */
    public Documentos createDocumentos() {
        return new Documentos();
    }

    /**
     * Create an instance of {@link RespuestaRegistro }
     * 
     */
    public RespuestaRegistro createRespuestaRegistro() {
        return new RespuestaRegistro();
    }

    /**
     * Create an instance of {@link Registros }
     * 
     */
    public Registros createRegistros() {
        return new Registros();
    }

    /**
     * Create an instance of {@link RepresentanteFisico }
     * 
     */
    public RepresentanteFisico createRepresentanteFisico() {
        return new RepresentanteFisico();
    }

    /**
     * Create an instance of {@link RespuestaGenerarAcuse }
     * 
     */
    public RespuestaGenerarAcuse createRespuestaGenerarAcuse() {
        return new RespuestaGenerarAcuse();
    }

    /**
     * Create an instance of {@link RespuestaRecuperarDocumento }
     * 
     */
    public RespuestaRecuperarDocumento createRespuestaRecuperarDocumento() {
        return new RespuestaRecuperarDocumento();
    }

    /**
     * Create an instance of {@link DistribucionRegistro }
     * 
     */
    public DistribucionRegistro createDistribucionRegistro() {
        return new DistribucionRegistro();
    }

    /**
     * Create an instance of {@link RespuestaDetalle }
     * 
     */
    public RespuestaDetalle createRespuestaDetalle() {
        return new RespuestaDetalle();
    }

    /**
     * Create an instance of {@link PeticionSigm }
     * 
     */
    public PeticionSigm createPeticionSigm() {
        return new PeticionSigm();
    }

    /**
     * Create an instance of {@link HistoricoIntercambioRegistralEntrada }
     * 
     */
    public HistoricoIntercambioRegistralEntrada createHistoricoIntercambioRegistralEntrada() {
        return new HistoricoIntercambioRegistralEntrada();
    }

    /**
     * Create an instance of {@link RespuestaSigm }
     * 
     */
    public RespuestaSigm createRespuestaSigm() {
        return new RespuestaSigm();
    }

    /**
     * Create an instance of {@link IntercambioRegistralEntrada }
     * 
     */
    public IntercambioRegistralEntrada createIntercambioRegistralEntrada() {
        return new IntercambioRegistralEntrada();
    }

    /**
     * Create an instance of {@link RespuestaBusqueda }
     * 
     */
    public RespuestaBusqueda createRespuestaBusqueda() {
        return new RespuestaBusqueda();
    }

    /**
     * Create an instance of {@link HistoricoDistribucion }
     * 
     */
    public HistoricoDistribucion createHistoricoDistribucion() {
        return new HistoricoDistribucion();
    }

    /**
     * Create an instance of {@link RespuestaAdjuntarDocumento }
     * 
     */
    public RespuestaAdjuntarDocumento createRespuestaAdjuntarDocumento() {
        return new RespuestaAdjuntarDocumento();
    }

    /**
     * Create an instance of {@link ErrorResponse }
     * 
     */
    public ErrorResponse createErrorResponse() {
        return new ErrorResponse();
    }

    /**
     * Create an instance of {@link Acuse }
     * 
     */
    public Acuse createAcuse() {
        return new Acuse();
    }

    /**
     * Create an instance of {@link MovimientoRegistro }
     * 
     */
    public MovimientoRegistro createMovimientoRegistro() {
        return new MovimientoRegistro();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link RepresentanteJuridico }
     * 
     */
    public RepresentanteJuridico createRepresentanteJuridico() {
        return new RepresentanteJuridico();
    }

    /**
     * Create an instance of {@link IntercambioRegistralSalida }
     * 
     */
    public IntercambioRegistralSalida createIntercambioRegistralSalida() {
        return new IntercambioRegistralSalida();
    }

    /**
     * Create an instance of {@link HistoricoIntercambioRegistral }
     * 
     */
    public HistoricoIntercambioRegistral createHistoricoIntercambioRegistral() {
        return new HistoricoIntercambioRegistral();
    }

    /**
     * Create an instance of {@link ElementoRepresentante }
     * 
     */
    public ElementoRepresentante createElementoRepresentante() {
        return new ElementoRepresentante();
    }

    /**
     * Create an instance of {@link Registro }
     * 
     */
    public Registro createRegistro() {
        return new Registro();
    }

    /**
     * Create an instance of {@link HistoricoMovimiento }
     * 
     */
    public HistoricoMovimiento createHistoricoMovimiento() {
        return new HistoricoMovimiento();
    }

    /**
     * Create an instance of {@link InteresadoFisico }
     * 
     */
    public InteresadoFisico createInteresadoFisico() {
        return new InteresadoFisico();
    }

    /**
     * Create an instance of {@link Fichero }
     * 
     */
    public Fichero createFichero() {
        return new Fichero();
    }

    /**
     * Create an instance of {@link ElementoDatosInteresados }
     * 
     */
    public ElementoDatosInteresados createElementoDatosInteresados() {
        return new ElementoDatosInteresados();
    }

    /**
     * Create an instance of {@link HistoricoIntercambioRegistralSalida }
     * 
     */
    public HistoricoIntercambioRegistralSalida createHistoricoIntercambioRegistralSalida() {
        return new HistoricoIntercambioRegistralSalida();
    }

    /**
     * Create an instance of {@link InteresadoJuridico }
     * 
     */
    public InteresadoJuridico createInteresadoJuridico() {
        return new InteresadoJuridico();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionDetalle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionDetalle")
    public JAXBElement<PeticionDetalle> createPeticionDetalle(PeticionDetalle value) {
        return new JAXBElement<PeticionDetalle>(_PeticionDetalle_QNAME, PeticionDetalle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionGenerarAcuse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionGenerarAcuse")
    public JAXBElement<PeticionGenerarAcuse> createPeticionGenerarAcuse(PeticionGenerarAcuse value) {
        return new JAXBElement<PeticionGenerarAcuse>(_PeticionGenerarAcuse_QNAME, PeticionGenerarAcuse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionRecuperarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionRecuperarDocumento")
    public JAXBElement<PeticionRecuperarDocumento> createPeticionRecuperarDocumento(PeticionRecuperarDocumento value) {
        return new JAXBElement<PeticionRecuperarDocumento>(_PeticionRecuperarDocumento_QNAME, PeticionRecuperarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionBusqueda }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionBusqueda")
    public JAXBElement<PeticionBusqueda> createPeticionBusqueda(PeticionBusqueda value) {
        return new JAXBElement<PeticionBusqueda>(_PeticionBusqueda_QNAME, PeticionBusqueda.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionAdjuntarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionAdjuntarDocumento")
    public JAXBElement<PeticionAdjuntarDocumento> createPeticionAdjuntarDocumento(PeticionAdjuntarDocumento value) {
        return new JAXBElement<PeticionAdjuntarDocumento>(_PeticionAdjuntarDocumento_QNAME, PeticionAdjuntarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PeticionRegistro }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.msssi.es/Regtel/2015/1", name = "peticionRegistro")
    public JAXBElement<PeticionRegistro> createPeticionRegistro(PeticionRegistro value) {
        return new JAXBElement<PeticionRegistro>(_PeticionRegistro_QNAME, PeticionRegistro.class, null, value);
    }

}
