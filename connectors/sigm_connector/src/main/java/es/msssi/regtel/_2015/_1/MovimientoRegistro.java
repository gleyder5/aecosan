
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for movimientoRegistro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="movimientoRegistro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecha" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="campo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valorAntiguo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valorNuevo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "movimientoRegistro", propOrder = {
    "usuario",
    "fecha",
    "campo",
    "valorAntiguo",
    "valorNuevo"
})
public class MovimientoRegistro {

    @XmlElement(required = true)
    protected String usuario;
    @XmlElement(required = true)
    protected String fecha;
    @XmlElement(required = true)
    protected String campo;
    @XmlElement(required = true)
    protected String valorAntiguo;
    @XmlElement(required = true)
    protected String valorNuevo;

    /**
     * Gets the value of the usuario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Sets the value of the usuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

    /**
     * Gets the value of the fecha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Sets the value of the fecha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecha(String value) {
        this.fecha = value;
    }

    /**
     * Gets the value of the campo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampo() {
        return campo;
    }

    /**
     * Sets the value of the campo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampo(String value) {
        this.campo = value;
    }

    /**
     * Gets the value of the valorAntiguo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorAntiguo() {
        return valorAntiguo;
    }

    /**
     * Sets the value of the valorAntiguo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorAntiguo(String value) {
        this.valorAntiguo = value;
    }

    /**
     * Gets the value of the valorNuevo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValorNuevo() {
        return valorNuevo;
    }

    /**
     * Sets the value of the valorNuevo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValorNuevo(String value) {
        this.valorNuevo = value;
    }

}
