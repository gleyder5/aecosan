
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for registro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="registro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estadoRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destino" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oficina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRegistroOriginal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaRegistroOriginal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroRegistroOriginal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoTransporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroTransporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoAsunto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="refExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comentario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interesados" type="{http://www.msssi.es/Regtel/2015/1}elementoDatosInteresados" minOccurs="0"/>
 *         &lt;element name="historicoIntercambioRegistral" type="{http://www.msssi.es/Regtel/2015/1}historicoIntercambioRegistral" minOccurs="0"/>
 *         &lt;element name="historicoDistribucion" type="{http://www.msssi.es/Regtel/2015/1}historicoDistribucion" minOccurs="0"/>
 *         &lt;element name="historicoMovimiento" type="{http://www.msssi.es/Regtel/2015/1}historicoMovimiento" minOccurs="0"/>
 *         &lt;element name="documentos" type="{http://www.msssi.es/Regtel/2015/1}documentos" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registro", propOrder = {
    "numeroRegistro",
    "fechaRegistro",
    "estadoRegistro",
    "origen",
    "destino",
    "tipoRegistro",
    "oficina",
    "tipoRegistroOriginal",
    "fechaRegistroOriginal",
    "numeroRegistroOriginal",
    "tipoTransporte",
    "numeroTransporte",
    "tipoAsunto",
    "resumen",
    "refExpediente",
    "comentario",
    "interesados",
    "historicoIntercambioRegistral",
    "historicoDistribucion",
    "historicoMovimiento",
    "documentos"
})
public class Registro {

    @XmlElement(required = true)
    protected String numeroRegistro;
    @XmlElement(required = true)
    protected String fechaRegistro;
    @XmlElement(required = true)
    protected String estadoRegistro;
    @XmlElement(required = true)
    protected String origen;
    @XmlElement(required = true)
    protected String destino;
    protected String tipoRegistro;
    protected String oficina;
    protected String tipoRegistroOriginal;
    protected String fechaRegistroOriginal;
    protected String numeroRegistroOriginal;
    protected String tipoTransporte;
    protected String numeroTransporte;
    protected String tipoAsunto;
    protected String resumen;
    protected String refExpediente;
    protected String comentario;
    protected ElementoDatosInteresados interesados;
    protected HistoricoIntercambioRegistral historicoIntercambioRegistral;
    protected HistoricoDistribucion historicoDistribucion;
    protected HistoricoMovimiento historicoMovimiento;
    protected Documentos documentos;

    /**
     * Gets the value of the numeroRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroRegistro() {
        return numeroRegistro;
    }

    /**
     * Sets the value of the numeroRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroRegistro(String value) {
        this.numeroRegistro = value;
    }

    /**
     * Gets the value of the fechaRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRegistro() {
        return fechaRegistro;
    }

    /**
     * Sets the value of the fechaRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRegistro(String value) {
        this.fechaRegistro = value;
    }

    /**
     * Gets the value of the estadoRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoRegistro() {
        return estadoRegistro;
    }

    /**
     * Sets the value of the estadoRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoRegistro(String value) {
        this.estadoRegistro = value;
    }

    /**
     * Gets the value of the origen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Sets the value of the origen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Gets the value of the destino property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestino() {
        return destino;
    }

    /**
     * Sets the value of the destino property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestino(String value) {
        this.destino = value;
    }

    /**
     * Gets the value of the tipoRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Sets the value of the tipoRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRegistro(String value) {
        this.tipoRegistro = value;
    }

    /**
     * Gets the value of the oficina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOficina() {
        return oficina;
    }

    /**
     * Sets the value of the oficina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOficina(String value) {
        this.oficina = value;
    }

    /**
     * Gets the value of the tipoRegistroOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRegistroOriginal() {
        return tipoRegistroOriginal;
    }

    /**
     * Sets the value of the tipoRegistroOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRegistroOriginal(String value) {
        this.tipoRegistroOriginal = value;
    }

    /**
     * Gets the value of the fechaRegistroOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaRegistroOriginal() {
        return fechaRegistroOriginal;
    }

    /**
     * Sets the value of the fechaRegistroOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRegistroOriginal(String value) {
        this.fechaRegistroOriginal = value;
    }

    /**
     * Gets the value of the numeroRegistroOriginal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroRegistroOriginal() {
        return numeroRegistroOriginal;
    }

    /**
     * Sets the value of the numeroRegistroOriginal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroRegistroOriginal(String value) {
        this.numeroRegistroOriginal = value;
    }

    /**
     * Gets the value of the tipoTransporte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTransporte() {
        return tipoTransporte;
    }

    /**
     * Sets the value of the tipoTransporte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTransporte(String value) {
        this.tipoTransporte = value;
    }

    /**
     * Gets the value of the numeroTransporte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroTransporte() {
        return numeroTransporte;
    }

    /**
     * Sets the value of the numeroTransporte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroTransporte(String value) {
        this.numeroTransporte = value;
    }

    /**
     * Gets the value of the tipoAsunto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAsunto() {
        return tipoAsunto;
    }

    /**
     * Sets the value of the tipoAsunto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAsunto(String value) {
        this.tipoAsunto = value;
    }

    /**
     * Gets the value of the resumen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResumen() {
        return resumen;
    }

    /**
     * Sets the value of the resumen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResumen(String value) {
        this.resumen = value;
    }

    /**
     * Gets the value of the refExpediente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefExpediente() {
        return refExpediente;
    }

    /**
     * Sets the value of the refExpediente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefExpediente(String value) {
        this.refExpediente = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the interesados property.
     * 
     * @return
     *     possible object is
     *     {@link ElementoDatosInteresados }
     *     
     */
    public ElementoDatosInteresados getInteresados() {
        return interesados;
    }

    /**
     * Sets the value of the interesados property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElementoDatosInteresados }
     *     
     */
    public void setInteresados(ElementoDatosInteresados value) {
        this.interesados = value;
    }

    /**
     * Gets the value of the historicoIntercambioRegistral property.
     * 
     * @return
     *     possible object is
     *     {@link HistoricoIntercambioRegistral }
     *     
     */
    public HistoricoIntercambioRegistral getHistoricoIntercambioRegistral() {
        return historicoIntercambioRegistral;
    }

    /**
     * Sets the value of the historicoIntercambioRegistral property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoricoIntercambioRegistral }
     *     
     */
    public void setHistoricoIntercambioRegistral(HistoricoIntercambioRegistral value) {
        this.historicoIntercambioRegistral = value;
    }

    /**
     * Gets the value of the historicoDistribucion property.
     * 
     * @return
     *     possible object is
     *     {@link HistoricoDistribucion }
     *     
     */
    public HistoricoDistribucion getHistoricoDistribucion() {
        return historicoDistribucion;
    }

    /**
     * Sets the value of the historicoDistribucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoricoDistribucion }
     *     
     */
    public void setHistoricoDistribucion(HistoricoDistribucion value) {
        this.historicoDistribucion = value;
    }

    /**
     * Gets the value of the historicoMovimiento property.
     * 
     * @return
     *     possible object is
     *     {@link HistoricoMovimiento }
     *     
     */
    public HistoricoMovimiento getHistoricoMovimiento() {
        return historicoMovimiento;
    }

    /**
     * Sets the value of the historicoMovimiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link HistoricoMovimiento }
     *     
     */
    public void setHistoricoMovimiento(HistoricoMovimiento value) {
        this.historicoMovimiento = value;
    }

    /**
     * Gets the value of the documentos property.
     * 
     * @return
     *     possible object is
     *     {@link Documentos }
     *     
     */
    public Documentos getDocumentos() {
        return documentos;
    }

    /**
     * Sets the value of the documentos property.
     * 
     * @param value
     *     allowed object is
     *     {@link Documentos }
     *     
     */
    public void setDocumentos(Documentos value) {
        this.documentos = value;
    }

}
