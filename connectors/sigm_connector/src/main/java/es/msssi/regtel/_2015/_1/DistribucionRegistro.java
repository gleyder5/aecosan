
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for distribucionRegistro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="distribucionRegistro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fechaDistribucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="origenDistribucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="destinoDistribucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaEstado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="comentario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estadosDistribucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "distribucionRegistro", propOrder = {
    "fechaDistribucion",
    "origenDistribucion",
    "destinoDistribucion",
    "estado",
    "fechaEstado",
    "comentario",
    "estadosDistribucion"
})
public class DistribucionRegistro {

    @XmlElement(required = true)
    protected String fechaDistribucion;
    @XmlElement(required = true)
    protected String origenDistribucion;
    @XmlElement(required = true)
    protected String destinoDistribucion;
    @XmlElement(required = true)
    protected String estado;
    @XmlElement(required = true)
    protected String fechaEstado;
    @XmlElement(required = true)
    protected String comentario;
    @XmlElement(required = true)
    protected String estadosDistribucion;

    /**
     * Gets the value of the fechaDistribucion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaDistribucion() {
        return fechaDistribucion;
    }

    /**
     * Sets the value of the fechaDistribucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaDistribucion(String value) {
        this.fechaDistribucion = value;
    }

    /**
     * Gets the value of the origenDistribucion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigenDistribucion() {
        return origenDistribucion;
    }

    /**
     * Sets the value of the origenDistribucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigenDistribucion(String value) {
        this.origenDistribucion = value;
    }

    /**
     * Gets the value of the destinoDistribucion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinoDistribucion() {
        return destinoDistribucion;
    }

    /**
     * Sets the value of the destinoDistribucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinoDistribucion(String value) {
        this.destinoDistribucion = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fechaEstado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaEstado() {
        return fechaEstado;
    }

    /**
     * Sets the value of the fechaEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaEstado(String value) {
        this.fechaEstado = value;
    }

    /**
     * Gets the value of the comentario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * Sets the value of the comentario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComentario(String value) {
        this.comentario = value;
    }

    /**
     * Gets the value of the estadosDistribucion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadosDistribucion() {
        return estadosDistribucion;
    }

    /**
     * Sets the value of the estadosDistribucion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadosDistribucion(String value) {
        this.estadosDistribucion = value;
    }

}
