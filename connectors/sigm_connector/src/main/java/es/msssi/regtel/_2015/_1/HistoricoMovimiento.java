
package es.msssi.regtel._2015._1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for historicoMovimiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="historicoMovimiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="movimientoRegistro" type="{http://www.msssi.es/Regtel/2015/1}movimientoRegistro" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historicoMovimiento", propOrder = {
    "movimientoRegistro"
})
public class HistoricoMovimiento {

    @XmlElement(required = true)
    protected List<MovimientoRegistro> movimientoRegistro;

    /**
     * Gets the value of the movimientoRegistro property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the movimientoRegistro property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMovimientoRegistro().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MovimientoRegistro }
     * 
     * 
     */
    public List<MovimientoRegistro> getMovimientoRegistro() {
        if (movimientoRegistro == null) {
            movimientoRegistro = new ArrayList<MovimientoRegistro>();
        }
        return this.movimientoRegistro;
    }

}
