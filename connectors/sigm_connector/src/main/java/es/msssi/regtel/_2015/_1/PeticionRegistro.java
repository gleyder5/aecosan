
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for peticionRegistro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="peticionRegistro">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.msssi.es/Regtel/2015/1}peticionSigm">
 *       &lt;sequence>
 *         &lt;element name="ficheros" type="{http://www.msssi.es/Regtel/2015/1}ficheros"/>
 *         &lt;element name="contenidoAcuse" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "peticionRegistro", propOrder = {
    "ficheros",
    "contenidoAcuse"
})
public class PeticionRegistro
    extends PeticionSigm
{

    @XmlElement(required = true)
    protected Ficheros ficheros;
    protected String contenidoAcuse;

    /**
     * Gets the value of the ficheros property.
     * 
     * @return
     *     possible object is
     *     {@link Ficheros }
     *     
     */
    public Ficheros getFicheros() {
        return ficheros;
    }

    /**
     * Sets the value of the ficheros property.
     * 
     * @param value
     *     allowed object is
     *     {@link Ficheros }
     *     
     */
    public void setFicheros(Ficheros value) {
        this.ficheros = value;
    }

    /**
     * Gets the value of the contenidoAcuse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContenidoAcuse() {
        return contenidoAcuse;
    }

    /**
     * Sets the value of the contenidoAcuse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContenidoAcuse(String value) {
        this.contenidoAcuse = value;
    }

}
