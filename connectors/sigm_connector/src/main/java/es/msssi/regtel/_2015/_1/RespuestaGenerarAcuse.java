
package es.msssi.regtel._2015._1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaGenerarAcuse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaGenerarAcuse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.msssi.es/Regtel/2015/1}respuestaSigm">
 *       &lt;sequence>
 *         &lt;element name="acuse" type="{http://www.msssi.es/Regtel/2015/1}acuse"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaGenerarAcuse", propOrder = {
    "acuse"
})
public class RespuestaGenerarAcuse
    extends RespuestaSigm
{

    @XmlElement(required = true)
    protected Acuse acuse;

    /**
     * Gets the value of the acuse property.
     * 
     * @return
     *     possible object is
     *     {@link Acuse }
     *     
     */
    public Acuse getAcuse() {
        return acuse;
    }

    /**
     * Sets the value of the acuse property.
     * 
     * @param value
     *     allowed object is
     *     {@link Acuse }
     *     
     */
    public void setAcuse(Acuse value) {
        this.acuse = value;
    }

}
