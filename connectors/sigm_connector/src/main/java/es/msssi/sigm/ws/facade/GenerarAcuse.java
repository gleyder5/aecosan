
package es.msssi.sigm.ws.facade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import es.msssi.regtel._2015._1.PeticionGenerarAcuse;


/**
 * <p>Java class for generarAcuse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generarAcuse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="request" type="{http://www.msssi.es/Regtel/2015/1}peticionGenerarAcuse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarAcuse", propOrder = {
    "request"
})
public class GenerarAcuse {

    protected PeticionGenerarAcuse request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link PeticionGenerarAcuse }
     *     
     */
    public PeticionGenerarAcuse getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link PeticionGenerarAcuse }
     *     
     */
    public void setRequest(PeticionGenerarAcuse value) {
        this.request = value;
    }

}
