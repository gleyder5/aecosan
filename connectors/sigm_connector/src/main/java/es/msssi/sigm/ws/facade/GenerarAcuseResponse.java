
package es.msssi.sigm.ws.facade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.regtel._2015._1.RespuestaGenerarAcuse;


/**
 * <p>Java class for generarAcuseResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generarAcuseResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://www.msssi.es/Regtel/2015/1}respuestaGenerarAcuse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generarAcuseResponse", propOrder = {
    "_return"
})
public class GenerarAcuseResponse {

    @XmlElement(name = "return")
    protected RespuestaGenerarAcuse _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaGenerarAcuse }
     *     
     */
    public RespuestaGenerarAcuse getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaGenerarAcuse }
     *     
     */
    public void setReturn(RespuestaGenerarAcuse value) {
        this._return = value;
    }

}
