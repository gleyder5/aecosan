
package es.msssi.sigm.ws.facade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import es.msssi.regtel._2015._1.PeticionDetalle;


/**
 * <p>Java class for detallar complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="detallar">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="request" type="{http://www.msssi.es/Regtel/2015/1}peticionDetalle" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "detallar", propOrder = {
    "request"
})
public class Detallar {

    protected PeticionDetalle request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link PeticionDetalle }
     *     
     */
    public PeticionDetalle getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link PeticionDetalle }
     *     
     */
    public void setRequest(PeticionDetalle value) {
        this.request = value;
    }

}
