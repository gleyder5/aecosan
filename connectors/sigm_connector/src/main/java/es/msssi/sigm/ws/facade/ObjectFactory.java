
package es.msssi.sigm.ws.facade;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.msssi.sigm.ws.facade package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Detallar_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "detallar");
    private final static QName _RecuperarDocumentoResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "recuperarDocumentoResponse");
    private final static QName _RecuperarDocumento_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "recuperarDocumento");
    private final static QName _GenerarAcuse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "generarAcuse");
    private final static QName _GenerarAcuseResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "generarAcuseResponse");
    private final static QName _RegistrarResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "registrarResponse");
    private final static QName _BuscarResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "buscarResponse");
    private final static QName _Registrar_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "registrar");
    private final static QName _DetallarResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "detallarResponse");
    private final static QName _AdjuntarDocumento_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "adjuntarDocumento");
    private final static QName _AdjuntarDocumentoResponse_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "adjuntarDocumentoResponse");
    private final static QName _Buscar_QNAME = new QName("http://facade.ws.sigm.msssi.es/", "buscar");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.msssi.sigm.ws.facade
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Detallar }
     * 
     */
    public Detallar createDetallar() {
        return new Detallar();
    }

    /**
     * Create an instance of {@link BuscarResponse }
     * 
     */
    public BuscarResponse createBuscarResponse() {
        return new BuscarResponse();
    }

    /**
     * Create an instance of {@link RecuperarDocumento }
     * 
     */
    public RecuperarDocumento createRecuperarDocumento() {
        return new RecuperarDocumento();
    }

    /**
     * Create an instance of {@link Registrar }
     * 
     */
    public Registrar createRegistrar() {
        return new Registrar();
    }

    /**
     * Create an instance of {@link RecuperarDocumentoResponse }
     * 
     */
    public RecuperarDocumentoResponse createRecuperarDocumentoResponse() {
        return new RecuperarDocumentoResponse();
    }

    /**
     * Create an instance of {@link GenerarAcuse }
     * 
     */
    public GenerarAcuse createGenerarAcuse() {
        return new GenerarAcuse();
    }

    /**
     * Create an instance of {@link DetallarResponse }
     * 
     */
    public DetallarResponse createDetallarResponse() {
        return new DetallarResponse();
    }

    /**
     * Create an instance of {@link GenerarAcuseResponse }
     * 
     */
    public GenerarAcuseResponse createGenerarAcuseResponse() {
        return new GenerarAcuseResponse();
    }

    /**
     * Create an instance of {@link AdjuntarDocumento }
     * 
     */
    public AdjuntarDocumento createAdjuntarDocumento() {
        return new AdjuntarDocumento();
    }

    /**
     * Create an instance of {@link AdjuntarDocumentoResponse }
     * 
     */
    public AdjuntarDocumentoResponse createAdjuntarDocumentoResponse() {
        return new AdjuntarDocumentoResponse();
    }

    /**
     * Create an instance of {@link RegistrarResponse }
     * 
     */
    public RegistrarResponse createRegistrarResponse() {
        return new RegistrarResponse();
    }

    /**
     * Create an instance of {@link Buscar }
     * 
     */
    public Buscar createBuscar() {
        return new Buscar();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Detallar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "detallar")
    public JAXBElement<Detallar> createDetallar(Detallar value) {
        return new JAXBElement<Detallar>(_Detallar_QNAME, Detallar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "recuperarDocumentoResponse")
    public JAXBElement<RecuperarDocumentoResponse> createRecuperarDocumentoResponse(RecuperarDocumentoResponse value) {
        return new JAXBElement<RecuperarDocumentoResponse>(_RecuperarDocumentoResponse_QNAME, RecuperarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "recuperarDocumento")
    public JAXBElement<RecuperarDocumento> createRecuperarDocumento(RecuperarDocumento value) {
        return new JAXBElement<RecuperarDocumento>(_RecuperarDocumento_QNAME, RecuperarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarAcuse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "generarAcuse")
    public JAXBElement<GenerarAcuse> createGenerarAcuse(GenerarAcuse value) {
        return new JAXBElement<GenerarAcuse>(_GenerarAcuse_QNAME, GenerarAcuse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerarAcuseResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "generarAcuseResponse")
    public JAXBElement<GenerarAcuseResponse> createGenerarAcuseResponse(GenerarAcuseResponse value) {
        return new JAXBElement<GenerarAcuseResponse>(_GenerarAcuseResponse_QNAME, GenerarAcuseResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "registrarResponse")
    public JAXBElement<RegistrarResponse> createRegistrarResponse(RegistrarResponse value) {
        return new JAXBElement<RegistrarResponse>(_RegistrarResponse_QNAME, RegistrarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BuscarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "buscarResponse")
    public JAXBElement<BuscarResponse> createBuscarResponse(BuscarResponse value) {
        return new JAXBElement<BuscarResponse>(_BuscarResponse_QNAME, BuscarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Registrar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "registrar")
    public JAXBElement<Registrar> createRegistrar(Registrar value) {
        return new JAXBElement<Registrar>(_Registrar_QNAME, Registrar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DetallarResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "detallarResponse")
    public JAXBElement<DetallarResponse> createDetallarResponse(DetallarResponse value) {
        return new JAXBElement<DetallarResponse>(_DetallarResponse_QNAME, DetallarResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjuntarDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "adjuntarDocumento")
    public JAXBElement<AdjuntarDocumento> createAdjuntarDocumento(AdjuntarDocumento value) {
        return new JAXBElement<AdjuntarDocumento>(_AdjuntarDocumento_QNAME, AdjuntarDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdjuntarDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "adjuntarDocumentoResponse")
    public JAXBElement<AdjuntarDocumentoResponse> createAdjuntarDocumentoResponse(AdjuntarDocumentoResponse value) {
        return new JAXBElement<AdjuntarDocumentoResponse>(_AdjuntarDocumentoResponse_QNAME, AdjuntarDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Buscar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://facade.ws.sigm.msssi.es/", name = "buscar")
    public JAXBElement<Buscar> createBuscar(Buscar value) {
        return new JAXBElement<Buscar>(_Buscar_QNAME, Buscar.class, null, value);
    }

}
