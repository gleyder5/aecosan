
package es.msssi.sigm.ws.facade;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import es.msssi.regtel._2015._1.RespuestaAdjuntarDocumento;


/**
 * <p>Java class for adjuntarDocumentoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="adjuntarDocumentoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://www.msssi.es/Regtel/2015/1}respuestaAdjuntarDocumento" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "adjuntarDocumentoResponse", propOrder = {
    "_return"
})
public class AdjuntarDocumentoResponse {

    @XmlElement(name = "return")
    protected RespuestaAdjuntarDocumento _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaAdjuntarDocumento }
     *     
     */
    public RespuestaAdjuntarDocumento getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaAdjuntarDocumento }
     *     
     */
    public void setReturn(RespuestaAdjuntarDocumento value) {
        this._return = value;
    }

}
