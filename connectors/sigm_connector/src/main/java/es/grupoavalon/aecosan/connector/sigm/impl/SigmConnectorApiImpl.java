package es.grupoavalon.aecosan.connector.sigm.impl;

import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.sigm.SigmConnectorApi;
import es.grupoavalon.aecosan.connector.sigm.dto.SigmAttachedFileRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmDocAttachDTORequest;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleAttachedFileResultDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTORequest;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTOResponse;
import es.grupoavalon.aecosan.connector.sigm.dto.register.AcuseDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.InteresadoFisicoDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.InteresadoJuridicoDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.RepresentanteFisicoDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.RepresentanteJuridicoDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterResponseDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SolicitudDTO;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorConnectionException;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorException;
import es.grupoavalon.aecosan.connector.sigm.util.ElementoSolicitud;
import es.grupoavalon.aecosan.connector.sigm.util.SigmConstants;
import es.msssi.regtel._2015._1.Acuse;
import es.msssi.regtel._2015._1.Documento;
import es.msssi.regtel._2015._1.ElementoDatosInteresados;
import es.msssi.regtel._2015._1.ElementoRepresentante;
import es.msssi.regtel._2015._1.Fichero;
import es.msssi.regtel._2015._1.Ficheros;
import es.msssi.regtel._2015._1.FileType;
import es.msssi.regtel._2015._1.InteresadoFisico;
import es.msssi.regtel._2015._1.InteresadoJuridico;
import es.msssi.regtel._2015._1.ObjectFactory;
import es.msssi.regtel._2015._1.PeticionAdjuntarDocumento;
import es.msssi.regtel._2015._1.PeticionRegistro;
import es.msssi.regtel._2015._1.Registro;
import es.msssi.regtel._2015._1.RepresentanteFisico;
import es.msssi.regtel._2015._1.RepresentanteJuridico;
import es.msssi.regtel._2015._1.RespuestaAdjuntarDocumento;
import es.msssi.regtel._2015._1.RespuestaRegistro;
import es.msssi.sigm.ws.facade.SigmService;

public class SigmConnectorApiImpl extends AbstractSigmWsConnector implements SigmConnectorApi {

	private static final Logger logger = LoggerFactory.getLogger(SigmConnectorApiImpl.class);

	@Override
	public SigmCreateRegisterResponseDTO createRegister(SigmCreateRegisterRequestDTO request) {

		SigmCreateRegisterResponseDTO response = null;
		try {

			SigmService clientPort = instanciateSigmService(request);
			PeticionRegistro peticionRegistro = getPeticionRegistroFromDTO(request);
			logger.info("request---Generating request for SIGM with data: PeticionRegistro: [IncludeAcuse: {}, Documents: {},Solicitud: {} ",request.getIncludeAcuse().getValue(), request.getAttachedFiles()!=null? request.getAttachedFiles().size():0,request.getSolicitud().toString());
			response = this.executeWsCallRegistrar(clientPort, peticionRegistro);
			logger.info("response---Getting Register Response:  {} " ,response.toString());

		} catch (SigmConnectorException e) {
			handleException(e);
		}

		return response;
	}


	private SigmCreateRegisterResponseDTO executeWsCallRegistrar(SigmService clientPort, PeticionRegistro peticionRegistro) {
		SigmCreateRegisterResponseDTO responseDto = null;
		try {
			Object[] responseRaw = invokeClientProxyRegistrar(clientPort, peticionRegistro);
			RespuestaRegistro response = getRespuestaFromRaw(responseRaw);
			proccessResponse(response);
			responseDto = getCreateRegisterResponseDTOFromRespuestaRegistro(response);
		} catch (Exception e) {
			handleException(e);
		}
		return responseDto;
	}

	private Object[] invokeClientProxyRegistrar(SigmService port, PeticionRegistro peticionRegistro) throws Exception {
		return super.invokeClientProxy(port, peticionRegistro, "registrar");
	}



	private SigmCreateRegisterResponseDTO getCreateRegisterResponseDTOFromRespuestaRegistro(RespuestaRegistro response) {
		SigmCreateRegisterResponseDTO retVal = new SigmCreateRegisterResponseDTO();
		retVal.setNumeroRegistro(response.getNumeroRegistro());
		retVal.setFechaRegistro(response.getFechaRegistro());
		retVal.setAcuse(getAcuseDTOFromAcuse(response.getAcuse()));
		retVal.setEstadoRegistro(response.getEstadoRegistro());
		return retVal;
	}

	private AcuseDTO getAcuseDTOFromAcuse(Acuse acuse) {
		AcuseDTO retVal = new AcuseDTO();
		retVal.setNombre(acuse.getNombre());
		retVal.setCsv(acuse.getCsv());
		if (acuse.getContenido() != null && acuse.getContenido().length > 0) {
			retVal.setContenidoAcuse(acuse.getContenido());
		}
		return retVal;
	}

	private PeticionRegistro getPeticionRegistroFromDTO(SigmCreateRegisterRequestDTO request) {
		ObjectFactory factory = new ObjectFactory();
		PeticionRegistro retVal = factory.createPeticionRegistro();
		Ficheros files = instanciateFicheros(request);
		retVal.setFicheros(files);
		retVal.setContenidoAcuse(request.getIncludeAcuse().getValue());
		return retVal;
	}

	private Ficheros instanciateFicheros(SigmCreateRegisterRequestDTO request) {
		ObjectFactory factory = new ObjectFactory();
		Ficheros files = factory.createFicheros();
		Fichero fileSolicitud = getFicheroSolicitudFromSolicitudDTO(factory, request.getSolicitud());
		files.getFichero().add(fileSolicitud);
		createAttachedFiles(request.getAttachedFiles(), files);
		return files;
	}



	private Fichero getFicheroSolicitudFromSolicitudDTO(ObjectFactory factory, SolicitudDTO solicitud) {
		try {
			Fichero retVal = factory.createFichero();
			StringWriter writer = new StringWriter();
			Registro reg = getRegistroFromSolicitudDTO(factory, solicitud);
			ElementoSolicitud elementoSolicitud = new ElementoSolicitud();
			elementoSolicitud.setDatos(reg);
			JAXBContext jaxbContext = JAXBContext.newInstance(ElementoSolicitud.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.marshal(new JAXBElement<ElementoSolicitud>(new QName("http://www.msssi.es/Regtel/2015/1", "elementoSolicitud"), ElementoSolicitud.class, null, elementoSolicitud), writer);
			String data = writer.toString();
			logger.debug("sgm xml content:" + data);
			writer.close();
			retVal.setContenido(data.getBytes());
			retVal.setFormato(SigmConstants.SOLICITUD_MIME_TYPE);
			retVal.setNombre(SigmConstants.SOLICITUD_FILENAME);
			retVal.setTipo(FileType.SOLICITUD);
			return retVal;
		} catch (Exception e) {
			throw new SigmConnectorConnectionException(e);
		}
	}

	private static void createAttachedFiles(List<SigmAttachedFileRequestDTO> attachedDtoFiles, Ficheros files) {
		if (attachedDtoFiles != null && !attachedDtoFiles.isEmpty()) {

			for (SigmAttachedFileRequestDTO fileDto : attachedDtoFiles) {
				Fichero file = instanciateFicheroFromAttachedDto(fileDto);
				files.getFichero().add(file);
			}
		}
	}

	private static Fichero instanciateFicheroFromAttachedDto(SigmAttachedFileRequestDTO fileDto) {
		ObjectFactory factory = new ObjectFactory();
		Fichero file = factory.createFichero();
		file.setContenido(fileDto.getFileBytes());
		file.setNombre(fileDto.getName());
		file.setTipo(FileType.ADJUNTO);
		file.setFormato(SigmConstants.ATTACHED_DEFAULT_PDF_FORMAT);
		return file;
	}

	private Registro getRegistroFromSolicitudDTO(ObjectFactory factory, SolicitudDTO solicitud) {
		Registro retVal = factory.createRegistro();
		retVal.setTipoRegistro(solicitud.getTipoRegistro());
		retVal.setOficina(solicitud.getOficina());
		retVal.setOrigen(solicitud.getOrigen());
		retVal.setDestino(solicitud.getDestino());
		retVal.setInteresados(getElementoDatosInteresadosFromInteresadosDTO(factory, solicitud.getInteresadosFisicos(), solicitud.getInteresadosJuridicos()));
		retVal.setFechaRegistroOriginal(solicitud.getFechaRegistroOriginal());
		retVal.setNumeroRegistroOriginal(solicitud.getNumeroRegistroOriginal());
		retVal.setTipoRegistroOriginal(solicitud.getTipoRegistroOriginal());
		retVal.setTipoTransporte(solicitud.getTipoTransporte());
		retVal.setNumeroTransporte(solicitud.getNumeroTransporte());
		retVal.setTipoAsunto(solicitud.getTipoAsunto());
		retVal.setResumen(solicitud.getResumen());
		retVal.setRefExpediente(solicitud.getRefExpediente());
		// %VBR% Estos campos vienen en la documentación, sin embargo este
		// objeto no los recibe. No sé si van a venir desde la capa business
		// %VBR%
		// retVal.setDatosExtendidos(getElementoDatosExtendidosFromDatosExtendidosDTO(solicitud.getDatosExtendidos()));
		return retVal;
	}

	private ElementoDatosInteresados getElementoDatosInteresadosFromInteresadosDTO(ObjectFactory factory, List<InteresadoFisicoDTO> interesadosFisicos, List<InteresadoJuridicoDTO> interesadosJuridicos) {
		ElementoDatosInteresados retVal = factory.createElementoDatosInteresados();
		if (interesadosFisicos != null) {
			for (InteresadoFisicoDTO interesado : interesadosFisicos) {
				retVal.getInteresadoFisico().add(getInteresadoFisicoFromInteresadoFisicoDTO(factory, interesado));
			}
		}
		if (interesadosJuridicos != null) {
			for (InteresadoJuridicoDTO interesado : interesadosJuridicos) {
				retVal.getInteresadoJuridico().add(getInteresadoJuridicoFromInteresadoJuridicoDTO(factory, interesado));
			}
		}
		return retVal;
	}

	private InteresadoJuridico getInteresadoJuridicoFromInteresadoJuridicoDTO(ObjectFactory factory, InteresadoJuridicoDTO interesado) {
		InteresadoJuridico retVal = factory.createInteresadoJuridico();
		retVal.setTipoDocumento(interesado.getTipoDocumento());
		retVal.setNumeroDocumento(interesado.getNumeroDocumento());
		retVal.setRazonSocial(interesado.getRazonSocial());
		retVal.setRepresentante(getElementoRepresentanteFromRepresentantesDTO(factory, interesado.getRepresentanteFisico(), interesado.getRepresentanteJuridico()));
		return retVal;
	}

	private ElementoRepresentante getElementoRepresentanteFromRepresentantesDTO(ObjectFactory factory, RepresentanteFisicoDTO representanteFisico, RepresentanteJuridicoDTO representanteJuridico) {
		if (representanteFisico == null && representanteJuridico == null) {
			return null;
		}
		ElementoRepresentante retVal = factory.createElementoRepresentante();
		if (representanteFisico != null) {
			retVal.setRepresentanteFisico(getRepresentanteFisicoFromRepresentateFisicoDTO(factory, representanteFisico));
		}
		if (representanteJuridico != null) {
			retVal.setRepresentanteJuridico(getRepresentanteJuridicoFromRepresentateFisicoDTO(factory, representanteJuridico));
		}
		return retVal;
	}

	private RepresentanteJuridico getRepresentanteJuridicoFromRepresentateFisicoDTO(ObjectFactory factory, RepresentanteJuridicoDTO representanteJuridico) {
		RepresentanteJuridico retVal = factory.createRepresentanteJuridico();
		retVal.setTipoDocumento(representanteJuridico.getTipoDocumento());
		retVal.setNumeroDocumento(representanteJuridico.getNumeroDocumento());
		retVal.setRazonSocial(representanteJuridico.getRazonSocial());
		return retVal;
	}

	private RepresentanteFisico getRepresentanteFisicoFromRepresentateFisicoDTO(ObjectFactory factory, RepresentanteFisicoDTO representanteFisico) {
		RepresentanteFisico retVal = factory.createRepresentanteFisico();
		retVal.setTipoDocumento(representanteFisico.getTipoDocumento());
		retVal.setNumeroDocumento(representanteFisico.getNumeroDocumento());
		retVal.setNombre(representanteFisico.getNombre());
		retVal.setPrimerApellido(representanteFisico.getPrimerApellido());
		retVal.setSegundoApellido(representanteFisico.getSegundoApellido());
		return retVal;
	}

	private InteresadoFisico getInteresadoFisicoFromInteresadoFisicoDTO(ObjectFactory factory, InteresadoFisicoDTO interesado) {
		InteresadoFisico retVal = factory.createInteresadoFisico();
		retVal.setTipoDocumento(interesado.getTipoDocumento());
		retVal.setNumeroDocumento(interesado.getNumeroDocumento());
		retVal.setNombre(interesado.getNombre());
		retVal.setPrimerApellido(interesado.getPrimerApellido());
		retVal.setSegundoApellido(interesado.getSegundoApellido());
		retVal.setRepresentante(getElementoRepresentanteFromRepresentantesDTO(factory, interesado.getRepresentanteFisico(), interesado.getRepresentanteJuridico()));
		return retVal;
	}

	@Override
	public SigmMultipleDocAttachDTOResponse attachMultipleDocument(SigmMultipleDocAttachDTORequest request) {

		SigmMultipleDocAttachDTOResponse response = new SigmMultipleDocAttachDTOResponse();
		List<SigmAttachedFileRequestDTO> filesToRegister = request.getDocuments();

		for (SigmAttachedFileRequestDTO sigmAttachedFileRequestDTO : filesToRegister) {
			SigmDocAttachDTORequest sigmDocAttachDTORequest = instanciateSigmDocAttachDTORequestFromMultipleRequestDTo(request, sigmAttachedFileRequestDTO);
			this.attachSingleDocument(response, sigmDocAttachDTORequest);
		}

		return response;
	}

	private void attachSingleDocument(SigmMultipleDocAttachDTOResponse response, SigmDocAttachDTORequest sigmDocAttachDTORequest) {
		try {
			this.attachDocument(sigmDocAttachDTORequest);
		} catch (Exception ex) {
			proccessErrorResponseWhileRegisteringMultpliesAttachments(ex, response, sigmDocAttachDTORequest.getDocument().getName());
		}
	}

	private static SigmDocAttachDTORequest instanciateSigmDocAttachDTORequestFromMultipleRequestDTo(SigmMultipleDocAttachDTORequest request, SigmAttachedFileRequestDTO fileToSend) {
		SigmDocAttachDTORequest sigmDocAttachDTORequest = new SigmDocAttachDTORequest();
		sigmDocAttachDTORequest.setUrl(request.getUrl());
		sigmDocAttachDTORequest.setUsername(request.getUsername());
		sigmDocAttachDTORequest.setPassword(request.getPassword());
		sigmDocAttachDTORequest.setDocument(fileToSend);
		sigmDocAttachDTORequest.setRegisterNumber(request.getRegisterNumber());
		return sigmDocAttachDTORequest;
	}

	private static void proccessErrorResponseWhileRegisteringMultpliesAttachments(Exception error, SigmMultipleDocAttachDTOResponse response, String fileName) {
		if (error instanceof SigmConnectorException) {
			SigmConnectorException ex = (SigmConnectorException) error;
			SigmMultipleAttachedFileResultDTO proccessError = instanciateSigmMultipleAttachedFileResultDTO(fileName, ex);
			response.setProccessError(proccessError);
		} else {
			handleException(error);
		}
	}

	private static SigmMultipleAttachedFileResultDTO instanciateSigmMultipleAttachedFileResultDTO(String fileName, SigmConnectorException ex) {
		SigmMultipleAttachedFileResultDTO attachedResult = new SigmMultipleAttachedFileResultDTO();
		attachedResult.setName(fileName);
		attachedResult.setProccesException(ex);

		return attachedResult;
	}

	@Override
	public void attachDocument(SigmDocAttachDTORequest request) {
		try {
			SigmService clientPort = instanciateSigmService(request);
			PeticionAdjuntarDocumento peticionAdjuntar = instanciatePeticionAdjuntarDocumentoFromDTO(request);
			logger.info("Creating request to Attach documents for SIGM: PeticionAdjunto : [TipoRegistro : {}, Numero de Registro :{}",peticionAdjuntar.getTipoRegistro(),peticionAdjuntar.getNumeroRegistro());
			executeWsAdjuntar(clientPort, peticionAdjuntar);
		} catch (Exception e) {
			handleException(e);
		}
	}

	private static PeticionAdjuntarDocumento instanciatePeticionAdjuntarDocumentoFromDTO(SigmDocAttachDTORequest request) {
		ObjectFactory factory = new ObjectFactory();
		PeticionAdjuntarDocumento peticionAdjuntar = factory.createPeticionAdjuntarDocumento();
		Documento document = instnaciateDocumentFormDTO(request.getDocument());
		peticionAdjuntar.setNumeroRegistro(request.getRegisterNumber());
		peticionAdjuntar.setDocumento(document);
		peticionAdjuntar.setTipoRegistro(SigmConstants.TIPO_REGISTRO_ENTRADA);
		return peticionAdjuntar;
	}

	private void executeWsAdjuntar(SigmService clientPort, PeticionAdjuntarDocumento peticionAdjuntar) throws Exception {
		Object[] responseRaw = invokeClientProxyAdjuntarDocumento(clientPort, peticionAdjuntar);
		RespuestaAdjuntarDocumento response = getRespuestaFromRaw(responseRaw);
		logger.info("Getting response from Attached documents of SIGM: EstadoRespuesta: {}, ErrorResponse[codigo: {}, descripcion: {}]  ",response.getEstadoRespuesta(),response.getErrorResponse()!=null ? response.getErrorResponse().getCodigo() :null,response.getErrorResponse()!=null ? response.getErrorResponse().getDescripcion(): null);
		proccessResponse(response);
	}

	private Object[] invokeClientProxyAdjuntarDocumento(SigmService port, PeticionAdjuntarDocumento peticionAdjuntar) throws Exception {
		return super.invokeClientProxy(port, peticionAdjuntar, "adjuntarDocumento");
	}

	private static Documento instnaciateDocumentFormDTO(SigmAttachedFileRequestDTO documentDTO) {
		ObjectFactory factory = new ObjectFactory();
		Documento document = factory.createDocumento();
		document.setContenido(documentDTO.getFileBytes());
		document.setNombre(documentDTO.getName());
		return document;
	}

}
