package es.grupoavalon.aecosan.connector.sigm.dto.attach;

import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorException;

public class SigmMultipleAttachedFileResultDTO {
	
	private String name;

	private SigmConnectorException proccesException;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SigmConnectorException getProccesException() {
		return proccesException;
	}

	public void setProccesException(SigmConnectorException proccesException) {
		this.proccesException = proccesException;
	}

}
