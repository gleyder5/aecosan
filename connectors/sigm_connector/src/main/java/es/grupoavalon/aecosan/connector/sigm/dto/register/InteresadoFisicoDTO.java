package es.grupoavalon.aecosan.connector.sigm.dto.register;

import javax.xml.bind.annotation.XmlElement;

public class InteresadoFisicoDTO {
	@XmlElement (name = "tipoDocumento")
	private String tipoDocumento;
	
	@XmlElement (name = "numeroDocumento") 
	private String numeroDocumento;

	@XmlElement (name = "nombre")
	private String nombre;
	
	@XmlElement (name = "primerApellido")
	private String primerApellido;
	
	@XmlElement (name = "segundoApellido")
	private String segundoApellido;
	
	@XmlElement (name = "representanteFisico")
	private RepresentanteFisicoDTO representanteFisico;
	
	@XmlElement (name = "representanteJuridico")
	private RepresentanteJuridicoDTO representanteJuridico;

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public RepresentanteFisicoDTO getRepresentanteFisico() {
		return representanteFisico;
	}

	public void setRepresentanteFisico(RepresentanteFisicoDTO representanteFisico) {
		this.representanteFisico = representanteFisico;
	}

	public RepresentanteJuridicoDTO getRepresentanteJuridico() {
		return representanteJuridico;
	}

	public void setRepresentanteJuridico(RepresentanteJuridicoDTO representanteJuridico) {
		this.representanteJuridico = representanteJuridico;
	}
}
