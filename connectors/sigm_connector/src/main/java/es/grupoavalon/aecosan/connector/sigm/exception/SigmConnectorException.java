package es.grupoavalon.aecosan.connector.sigm.exception;

public class SigmConnectorException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	// public static final String GENERAL_REGISTER_ERROR =
	// "Can not execute the SIGIM registration due an error:";

	public static final String GENERAL_ERROR = "Can not execute the SIGIM client due an error:";

	public static final String RESPONSE_UNEXPECTED = "Can not process the response from the server was unexpected";

	public SigmConnectorException(String message) {
		super(message);
	}

	public SigmConnectorException(Throwable t) {
		super(t);
	}

	public SigmConnectorException(String message, Throwable cause) {
		super(message, cause);
	}

}
