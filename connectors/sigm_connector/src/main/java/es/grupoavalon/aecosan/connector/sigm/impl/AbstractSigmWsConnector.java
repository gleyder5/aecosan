package es.grupoavalon.aecosan.connector.sigm.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.headers.Header;
import org.apache.cxf.jaxb.JAXBDataBinding;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.cxf.ws.security.wss4j.WSS4JOutInterceptor;
import org.apache.wss4j.dom.WSConstants;
import org.apache.wss4j.dom.handler.WSHandlerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.sigm.dto.AbstractSigmRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorConnectionException;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorException;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorOperationException;
import es.grupoavalon.aecosan.connector.sigm.util.SigmConstants;
import es.grupoavalon.aecosan.connector.sigm.util.SigmPasswordCallback;
import es.msssi.regtel._2015._1.RespuestaSigm;
import es.msssi.sigm.ws.facade.SigmService;
import es.msssi.sigm.ws.facade.SigmServiceService;

public abstract class AbstractSigmWsConnector {
	protected static final Logger logger = LoggerFactory.getLogger(AbstractSigmWsConnector.class);
	private UserPasswordHandler userPwHandler;

	protected SigmService instanciateSigmService(AbstractSigmRequestDTO requestDto) {
		URL url = instanciateUrl(requestDto.getUrl());
		setupUserPasswordhandler(requestDto.getUsername(), requestDto.getPassword());
		SigmServiceService service = new SigmServiceService(url);
		return service.getSigmServicePort();
	}

	protected static URL instanciateUrl(String url) {
		URL wsUrl;
		try {
			wsUrl = new URL(url);
		} catch (MalformedURLException e) {
			throw new SigmConnectorConnectionException("Invalid URL:" + url);
		}
		return wsUrl;
	}

	protected void setupUserPasswordhandler(String user, String pw) {
		this.userPwHandler = new UserPasswordHandler(user, pw);
	}

	protected <T> Object[] invokeClientProxy(SigmService port, T object, String method) throws Exception {
		Client client = setupClientProxy(port);
		return client.invoke(method, object);
	}

	protected Client setupClientProxy(SigmService port) throws JAXBException {
		Client clientProxy = ClientProxy.getClient(port);
		setSecurityHeaders(clientProxy);
		setupConduit(clientProxy);
		return clientProxy;
	}

	protected void setSecurityHeaders(Client clientProxy) throws JAXBException {
		Map<String, Object> outProps = getOutSecurityProperties();
		WSS4JOutInterceptor outInterceptor = new WSS4JOutInterceptor(outProps);
		outInterceptor.setAllowMTOM(Boolean.TRUE);
		clientProxy.getOutInterceptors().add(outInterceptor);
		addCustomSecurityHeaders(clientProxy);
	}

	protected Map<String, Object> getOutSecurityProperties() {
		Map<String, Object> outProps = new HashMap<String, Object>();
		outProps.put(WSHandlerConstants.ACTION, WSHandlerConstants.USERNAME_TOKEN);
		outProps.put(WSHandlerConstants.USER, this.userPwHandler.getUser());
		outProps.put(WSHandlerConstants.PASSWORD_TYPE, WSConstants.PW_TEXT);
		outProps.put(WSHandlerConstants.ADD_USERNAMETOKEN_CREATED, Boolean.TRUE.toString());
		outProps.put(WSHandlerConstants.PW_CALLBACK_REF, this.userPwHandler.getPasswordCallback());
		return outProps;
	}

	protected static void addCustomSecurityHeaders(Client clientProxy) throws JAXBException {
		List<Header> headersList = new ArrayList<Header>();
		for (Map.Entry<String, String> entry : SigmConstants.CUSTOM_SECURITY_HEADERS.entrySet()) {
			Header customSoapHeader = new Header(new QName("", entry.getKey()), entry.getValue(), new JAXBDataBinding(String.class));
			headersList.add(customSoapHeader);
		}
		clientProxy.getRequestContext().put(Header.HEADER_LIST, headersList);
	}


	protected static void setupConduit(Client clientProxy) {
		HTTPConduit httpConduit = (HTTPConduit) clientProxy.getConduit();
		HTTPClientPolicy policy = httpConduit.getClient();
		policy.setAutoRedirect(true);
		policy.setAllowChunking(false);
		httpConduit.setClient(policy);
	}

	@SuppressWarnings("unchecked")
	protected static <T> T getRespuestaFromRaw(Object[] responseRaw) {
		T response;

		if (responseRaw != null && responseRaw.length > 0) {
			response = (T) responseRaw[0];
		} else {
			throw new SigmConnectorException(SigmConnectorException.RESPONSE_UNEXPECTED + Arrays.toString(responseRaw));
		}

		return response;
	}

	protected static void proccessResponse(RespuestaSigm response) {
		if (response.getErrorResponse() != null) {
			throw new SigmConnectorOperationException(response.getErrorResponse());
		}
	}

	protected static void handleException(Exception e) {
		if (e instanceof SigmConnectorException) {
			throw (SigmConnectorException) e;
		} else {
			throw new SigmConnectorException(SigmConnectorException.GENERAL_ERROR + e.getLocalizedMessage(), e);
		}
	}

	private static class UserPasswordHandler {
		private SigmPasswordCallback passwordCallback;
		private String user;

		private UserPasswordHandler(String user, final String pw) {
			super();
			this.user = user;
			passwordCallback = new SigmPasswordCallback() {

				@Override
				protected String getPassword() {
					return pw;
				}
			};

		}

		public SigmPasswordCallback getPasswordCallback() {
			return passwordCallback;
		}

		public String getUser() {
			return user;
		}
	}

}
