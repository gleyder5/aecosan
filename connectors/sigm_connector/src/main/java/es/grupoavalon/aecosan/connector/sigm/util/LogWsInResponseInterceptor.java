package es.grupoavalon.aecosan.connector.sigm.util;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@NoJSR250Annotations
public class LogWsInResponseInterceptor extends AbstractSoapInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(LogWsInResponseInterceptor.class);

	public LogWsInResponseInterceptor() {
		super(Phase.RECEIVE);
	}

	@Override
	public void handleMessage(SoapMessage message) throws Fault {
		// get the remote address
		try {
			// now get the request xml
			InputStream is = message.getContent(InputStream.class);
			CachedOutputStream os = new CachedOutputStream();
			IOUtils.copy(is, os);
			os.flush();
			message.setContent(InputStream.class, os.getInputStream());
			is.close();

			logger.debug("The request is: " + IOUtils.toString(os.getInputStream()));
			os.close();
		} catch (Exception ex) {

		}

	}

}