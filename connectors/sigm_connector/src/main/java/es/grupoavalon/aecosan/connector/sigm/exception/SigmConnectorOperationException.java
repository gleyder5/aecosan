package es.grupoavalon.aecosan.connector.sigm.exception;

import es.msssi.regtel._2015._1.ErrorResponse;

public class SigmConnectorOperationException extends SigmConnectorException {
	private static final long serialVersionUID = 1L;

	private final String errorCode;
	
	public SigmConnectorOperationException(ErrorResponse errorResponse) {
		super(errorResponse.getCodigo() + ":" + errorResponse.getDescripcion());
		this.errorCode = errorResponse.getCodigo();
	}

	public String getErrorCode() {
		return errorCode;
	}
}
