package es.grupoavalon.aecosan.connector.sigm.dto.register;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SolicitudDTO {
	@XmlElement (name="tipoRegistro")
	private String tipoRegistro;

	@XmlElement (name="oficina")
	private String oficina;
	
	@XmlElement (name="origen")
	private String origen;
	
	@XmlElement (name="destino")
	private String destino;
	
	@XmlElement (name="interesadosFisicos")
	private List<InteresadoFisicoDTO> interesadosFisicos;

	@XmlElement (name="interesadosJuridicos")
	private List<InteresadoJuridicoDTO> interesadosJuridicos;

	@XmlElement (name="fechaRegistroOriginal")
	private String fechaRegistroOriginal;
	
	@XmlElement (name="numeroRegistroOriginal")
	private String numeroRegistroOriginal;
	
	@XmlElement (name="tipoRegistroOriginal")
	private String tipoRegistroOriginal;
	
	@XmlElement (name="tipoTransporte")
	private String tipoTransporte;
	
	@XmlElement (name="numeroTransporte")
	private String numeroTransporte;

	@XmlElement (name="tipoAsunto")
	private String tipoAsunto;
	
	@XmlElement (name="resumen")
	private String resumen;
	
	@XmlElement (name="refExpediente")
	private String refExpediente;
	
	public String getTipoRegistro() {
		return tipoRegistro;
	}

	public void setTipoRegistro(String tipoRegistro) {
		this.tipoRegistro = tipoRegistro;
	}

	public String getOficina() {
		return oficina;
	}

	public void setOficina(String oficina) {
		this.oficina = oficina;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public List<InteresadoFisicoDTO> getInteresadosFisicos() {
		return interesadosFisicos;
	}

	public void setInteresadosFisicos(List<InteresadoFisicoDTO> interesadosFisicos) {
		this.interesadosFisicos = interesadosFisicos;
	}

	public List<InteresadoJuridicoDTO> getInteresadosJuridicos() {
		return interesadosJuridicos;
	}

	public void setInteresadosJuridicos(List<InteresadoJuridicoDTO> interesadosJuridicos) {
		this.interesadosJuridicos = interesadosJuridicos;
	}

	public String getFechaRegistroOriginal() {
		return fechaRegistroOriginal;
	}

	public void setFechaRegistroOriginal(String fechaRegistroOriginal) {
		this.fechaRegistroOriginal = fechaRegistroOriginal;
	}

	public String getNumeroRegistroOriginal() {
		return numeroRegistroOriginal;
	}

	public void setNumeroRegistroOriginal(String numeroRegistroOriginal) {
		this.numeroRegistroOriginal = numeroRegistroOriginal;
	}

	public String getTipoRegistroOriginal() {
		return tipoRegistroOriginal;
	}

	public void setTipoRegistroOriginal(String tipoRegistroOriginal) {
		this.tipoRegistroOriginal = tipoRegistroOriginal;
	}

	public String getTipoTransporte() {
		return tipoTransporte;
	}

	public void setTipoTransporte(String tipoTransporte) {
		this.tipoTransporte = tipoTransporte;
	}

	public String getNumeroTransporte() {
		return numeroTransporte;
	}

	public void setNumeroTransporte(String numeroTransporte) {
		this.numeroTransporte = numeroTransporte;
	}

	public String getTipoAsunto() {
		return tipoAsunto;
	}

	public void setTipoAsunto(String tipoAsunto) {
		this.tipoAsunto = tipoAsunto;
	}

	public String getResumen() {
		return resumen;
	}

	public void setResumen(String resumen) {
		this.resumen = resumen;
	}

	public String getRefExpediente() {
		return refExpediente;
	}

	public void setRefExpediente(String refExpediente) {
		this.refExpediente = refExpediente;
	}

	@Override
	public String toString() {
		return "SolicitudDTO{" +
				"tipoRegistro='" + tipoRegistro + '\'' +
				", oficina='" + oficina + '\'' +
				", origen='" + origen + '\'' +
				", destino='" + destino + '\'' +
				", interesadosFisicos=" + interesadosFisicos +
				", interesadosJuridicos=" + interesadosJuridicos +
				", fechaRegistroOriginal='" + fechaRegistroOriginal + '\'' +
				", numeroRegistroOriginal='" + numeroRegistroOriginal + '\'' +
				", tipoRegistroOriginal='" + tipoRegistroOriginal + '\'' +
				", tipoTransporte='" + tipoTransporte + '\'' +
				", numeroTransporte='" + numeroTransporte + '\'' +
				", tipoAsunto='" + tipoAsunto + '\'' +
				", resumen='" + resumen + '\'' +
				", refExpediente='" + refExpediente + '\'' +
				'}';
	}
}
