package es.grupoavalon.aecosan.connector.sigm.dto.attach;

import es.grupoavalon.aecosan.connector.sigm.dto.AbstractSigmRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.SigmAttachedFileRequestDTO;

public class SigmDocAttachDTORequest extends AbstractSigmRequestDTO {
	
	private String registerNumber;
	
	private SigmAttachedFileRequestDTO document;
	
	public String getRegisterNumber() {
		return registerNumber;
	}

	public SigmAttachedFileRequestDTO getDocument() {
		return document;
	}

	public void setDocument(SigmAttachedFileRequestDTO document) {
		this.document = document;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	@Override
	public String toString() {
		return "SigmDocAttachDTORequest{" +
				"registerNumber='" + registerNumber + '\'' +
				", document=" + document +
				'}';
	}
}
