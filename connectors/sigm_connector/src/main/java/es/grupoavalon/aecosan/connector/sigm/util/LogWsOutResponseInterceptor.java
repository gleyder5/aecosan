package es.grupoavalon.aecosan.connector.sigm.util;

import java.io.OutputStream;

import org.apache.cxf.common.injection.NoJSR250Annotations;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@NoJSR250Annotations
public class LogWsOutResponseInterceptor  extends LoggingOutInterceptor {
	
	private static final Logger logger = LoggerFactory.getLogger(LogWsOutResponseInterceptor.class);
	
	
	public LogWsOutResponseInterceptor() {
		  super(Phase.PRE_STREAM);
		
	}
	  @Override
	    public void handleMessage(Message message) throws Fault {
	        OutputStream out = message.getContent(OutputStream.class);
	        final CacheAndWriteOutputStream newOut = new CacheAndWriteOutputStream(out);
	        message.setContent(OutputStream.class, newOut);
	        newOut.registerCallback(new LoggingCallback());
	    }

	    public class LoggingCallback implements CachedOutputStreamCallback {
	        @Override
			public void onFlush(CachedOutputStream cos) {
	        }

	        @Override
			public void onClose(CachedOutputStream cos) {
	            try {
	                StringBuilder builder = new StringBuilder();
	                cos.writeCacheTo(builder, limit);
				logger.debug(builder.toString());
	            } catch (Exception e) {
	            }
	        }
	    }
	
}
