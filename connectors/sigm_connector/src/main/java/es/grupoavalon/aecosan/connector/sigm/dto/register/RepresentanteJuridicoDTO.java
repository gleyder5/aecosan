package es.grupoavalon.aecosan.connector.sigm.dto.register;

import javax.xml.bind.annotation.XmlElement;

public class RepresentanteJuridicoDTO {

	@XmlElement(name = "tipoDocumento", required = true)
    private String tipoDocumento;
	
    @XmlElement(name = "numeroDocumento", required = true)
    private String numeroDocumento;
    
    @XmlElement(name = "razonSocial", required = true)
    private String razonSocial;

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
}
