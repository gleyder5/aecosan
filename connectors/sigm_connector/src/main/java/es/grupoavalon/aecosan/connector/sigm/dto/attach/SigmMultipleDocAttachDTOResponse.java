package es.grupoavalon.aecosan.connector.sigm.dto.attach;

import java.util.ArrayList;
import java.util.List;

public class SigmMultipleDocAttachDTOResponse {

	private List<SigmMultipleAttachedFileResultDTO> proccessErrors;

	public List<SigmMultipleAttachedFileResultDTO> getProccessErrors() {
		return proccessErrors;
	}

	public void setProccessErrors(List<SigmMultipleAttachedFileResultDTO> proccessErrors) {
		this.proccessErrors = proccessErrors;
	}

	public void setProccessError(SigmMultipleAttachedFileResultDTO proccessError) {
		if (this.proccessErrors == null) {
			this.proccessErrors = new ArrayList<SigmMultipleAttachedFileResultDTO>();
		}
		this.proccessErrors.add(proccessError);
	}

	public boolean isSuccess() {
		boolean success = true;
		if (this.proccessErrors != null && !this.proccessErrors.isEmpty()) {
			success = false;
		}
		return success;
	}

	
}
