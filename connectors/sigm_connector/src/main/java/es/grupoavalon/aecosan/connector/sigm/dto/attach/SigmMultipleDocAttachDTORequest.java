package es.grupoavalon.aecosan.connector.sigm.dto.attach;

import java.util.ArrayList;
import java.util.List;

import es.grupoavalon.aecosan.connector.sigm.dto.AbstractSigmRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.SigmAttachedFileRequestDTO;

public class SigmMultipleDocAttachDTORequest extends AbstractSigmRequestDTO{
	
	private String registerNumber;
	
	private List<SigmAttachedFileRequestDTO> documents;
	
	public String getRegisterNumber() {
		return registerNumber;
	}

	public List<SigmAttachedFileRequestDTO> getDocuments() {
		return documents;
	}

	public void setDocuments(List<SigmAttachedFileRequestDTO> documents) {
		this.documents = documents;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	
	public void setDocument(SigmAttachedFileRequestDTO document) {
		if(this.documents == null){
			this.documents = new ArrayList<SigmAttachedFileRequestDTO>();
		}
		this.documents.add(document);
	}
	

}
