package es.grupoavalon.aecosan.connector.sigm.dto;



public class SigmAttachedFileRequestDTO {
	
	private byte[] fileBytes;
	private String name;

	public byte[] getFileBytes() {
		return fileBytes;
	}

	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "SigmAttachedFileRequestDTO [ name=" + name + "]";
	}

}
