package es.grupoavalon.aecosan.connector.sigm.dto.register;

import javax.xml.bind.annotation.XmlElement;

public class SigmCreateRegisterResponseDTO {
	@XmlElement(name = "numeroRegistro")
	private String numeroRegistro;

	@XmlElement(name = "fechaRegistro")
	private String fechaRegistro;

	@XmlElement(name = "acuse")
	private AcuseDTO acuse;
	
	@XmlElement(name = "estaodRegistro")
	private String estadoRegistro;
	
	public String getNumeroRegistro() {
		return numeroRegistro;
	}

	public void setNumeroRegistro(String numeroRegistro) {
		this.numeroRegistro = numeroRegistro;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public AcuseDTO getAcuse() {
		return acuse;
	}

	public void setAcuse(AcuseDTO acuse) {
		this.acuse = acuse;
	}

	public String getEstadoRegistro() {
		return estadoRegistro;
	}

	public void setEstadoRegistro(String estadoRegistro) {
		this.estadoRegistro = estadoRegistro;
	}

	@Override
	public String toString() {
		return "SigmCreateRegisterResponseDTO{" +
				"numeroRegistro='" + numeroRegistro + '\'' +
				", fechaRegistro='" + fechaRegistro + '\'' +
				", acuse=" + acuse +
				", estadoRegistro='" + estadoRegistro + '\'' +
				'}';
	}
}

