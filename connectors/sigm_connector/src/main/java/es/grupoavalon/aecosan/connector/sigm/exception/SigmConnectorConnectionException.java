package es.grupoavalon.aecosan.connector.sigm.exception;

public class SigmConnectorConnectionException extends SigmConnectorException {
	private static final long serialVersionUID = 1L;

	public SigmConnectorConnectionException(String message) {
		super(message);
	}

	public SigmConnectorConnectionException(Throwable t) {
		super(t);
	}
}
