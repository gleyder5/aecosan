package es.grupoavalon.aecosan.connector.sigm.util;

import java.util.HashMap;
import java.util.Map;

public class SigmConstants {
	public static final String ERROR_GENERAL = "ERR00";
	public static final String ERROR_XML_CONVERSION = "ERR01";
	public static final String ERROR_REGISTER_NOT_FOUND = "ERR02";
	public static final String ERROR_NULL_DESTINY = "ERR03";
	public static final String ERROR_INVALID_SIGNATURE = "ERR04";
	public static final String ERROR_INVALID_USER = "ERR05";
	public static final String ERROR_MAX_RECORDS = "ERR06";
	public static final String ERROR_INVALID_NUMBER_OF_FILES = "ERR07";
	public static final String ERROR_FILENAME_NOT_FOUND = "ERR08";
	public static final String ERROR_DUPLICATED_FILENAME = "ERR09";
	public static final String ERROR_DOCUMENT_NOT_FOUND = "ERR10";
	public static final String ERROR_MISSING_EXTENSION = "ERR11";
	public static final String ERROR_FILENAME_TOO_LONG = "ERR12";
	public static final String ERROR_USER_PASSWORD_INVALID = "ERR13";
	
	public static final String TIPO_TRANSPORTE_MENSAJEROS = "01";
	public static final String TIPO_TRANSPORTE_CORREO_POSTAL = "02";
	public static final String TIPO_TRANSPORTE_CORREO_POSTAL_CERTIFICADO = "03";
	public static final String TIPO_TRANSPORTE_BUROFAX = "04";
	public static final String TIPO_TRANSPORTE_VENTANILLA = "05";
	public static final String TIPO_TRANSPORTE_FAX = "06";
	public static final String TIPO_TRANSPORTE_OTROS = "07";
	public static final String TIPO_TRANSPORTE_INTERNO = "08";
	
	public static final String TIPO_ASUNTO_SOLICITUD = "TSOL";
	public static final String TIPO_ASUNTO_SUBVENCION = "TSUB";
	public static final String TIPO_ASUNTO_COMUNICACION = "TCOM";
	public static final String TIPO_ASUNTO_CONCURSO = "TCON";
	public static final String TIPO_ASUNTO_OFERTA = "TOFE";
	public static final String TIPO_ASUNTO_FACTURA = "TFAC";
	public static final String TIPO_ASUNTO_REQUERIMIENTO = "TREQ";
	public static final String TIPO_ASUNTO_BECA = "TBEC";
	public static final String TIPO_ASUNTO_PARTE_BAJA = "TPBA";
	public static final String TIPO_ASUNTO_ESPECIALIDAD = "TESP";
	public static final String TIPO_ASUNTO_PRODUCTO = "TPRO";
	public static final String TIPO_ASUNTO_PROCESO_SELECTIVO = "TPSEL";
	public static final String TIPO_ASUNTO_REEMBOLSO_GASTOS_SANITARIOS = "TRGA";
	public static final String TIPO_ASUNTO_GENERICO = "TGEN";
	
	public static final String TIPO_DOCUMENTO_CIF = "1";
	public static final String TIPO_DOCUMENTO_NIF = "2";
	public static final String TIPO_DOCUMENTO_PASAPORTE = "3";
	public static final String TIPO_DOCUMENTO_NIE = "4";
	public static final String TIPO_DOCUMENTO_OTROS = "5";
	public static final String TIPO_DOCUMENTO_CODIGO_ORIGEN = "6";
	
	public static final String SOLICITUD_MIME_TYPE = "application/xml";
	public static final String SOLICITUD_FILENAME = "solicitud.xml";
	
	public static final String TIPO_REGISTRO_ENTRADA = "E";
	public static final String TIPO_REGISTRO_SALIDA = "S";

	public static final String CUSTOM_SECURITY_HEADER_WSSECTYPE = "WSSecType";
	public static final String CUSTOM_SECURITY_HEADER_WSSECTYPE_VALUE = "2";
	public static final String CUSTOM_SECURITY_HEADER_WSSECMODE = "WSSecMode";
	public static final String CUSTOM_SECURITY_HEADER_WSSECMODE_VALUE = "CLIENT";
	public static final String CUSTOM_SECURITY_HEADER_WSSECSYSTEM = "WSSecSystem";
	public static final String CUSTOM_SECURITY_HEADER_WSSECSYSTEM_VALUE = "SIGM";

	public static final Map<String, String> CUSTOM_SECURITY_HEADERS;

	static {
		CUSTOM_SECURITY_HEADERS = new HashMap<String, String>(3);
		CUSTOM_SECURITY_HEADERS.put(CUSTOM_SECURITY_HEADER_WSSECTYPE, CUSTOM_SECURITY_HEADER_WSSECTYPE_VALUE);
		CUSTOM_SECURITY_HEADERS.put(CUSTOM_SECURITY_HEADER_WSSECMODE, CUSTOM_SECURITY_HEADER_WSSECMODE_VALUE);
		CUSTOM_SECURITY_HEADERS.put(CUSTOM_SECURITY_HEADER_WSSECSYSTEM, CUSTOM_SECURITY_HEADER_WSSECSYSTEM_VALUE);
	}

	public static final String ATTACHED_DEFAULT_PDF_FORMAT = "application/pdf";

}
