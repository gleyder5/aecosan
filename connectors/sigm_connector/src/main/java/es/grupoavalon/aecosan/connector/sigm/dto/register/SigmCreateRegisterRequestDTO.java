package es.grupoavalon.aecosan.connector.sigm.dto.register;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import es.grupoavalon.aecosan.connector.sigm.dto.AbstractSigmRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.SigmAttachedFileRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.util.IncludeAcuse;

public class SigmCreateRegisterRequestDTO extends AbstractSigmRequestDTO {
	
	@XmlElement(name = "solicitud")
	private SolicitudDTO solicitud;
	
	private IncludeAcuse includeAcuse;

	private List<SigmAttachedFileRequestDTO> attachedFiles;

	public SolicitudDTO getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudDTO solicitud) {
		this.solicitud = solicitud;
	}

	public IncludeAcuse getIncludeAcuse() {
		if (this.includeAcuse == null) {
			this.includeAcuse = IncludeAcuse.YES;
		}
		return includeAcuse;
	}

	public void setIncludeAcuse(IncludeAcuse includeAcuse) {
		this.includeAcuse = includeAcuse;
	}

	public List<SigmAttachedFileRequestDTO> getAttachedFiles() {
		return attachedFiles;
	}

	public void setAttachedFiles(List<SigmAttachedFileRequestDTO> attachedFiles) {
		this.attachedFiles = attachedFiles;
	}

	public void setAttachedFile(SigmAttachedFileRequestDTO attachedFile) {
		if (this.attachedFiles == null) {
			this.attachedFiles = new ArrayList<SigmAttachedFileRequestDTO>();
		}
		this.attachedFiles.add(attachedFile);
	}

	@Override
	public String toString() {
		return "SigmCreateRegisterRequestDTO [solicitud=" + solicitud.toString()+ ", includeAcuse=" + includeAcuse.getValue() + ", attachedFiles=" + attachedFiles + "]";
	}

}
