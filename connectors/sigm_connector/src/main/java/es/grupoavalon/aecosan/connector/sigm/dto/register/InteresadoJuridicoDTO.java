package es.grupoavalon.aecosan.connector.sigm.dto.register;

import javax.xml.bind.annotation.XmlElement;

public class InteresadoJuridicoDTO {
	@XmlElement (name = "tipoDocumento")
	private String tipoDocumento;
	
	@XmlElement (name = "numeroDocumento") 
	private String numeroDocumento;
	
	@XmlElement (name = "razonSocial")
	private String razonSocial;
	
	@XmlElement (name = "representanteFisico")
	private RepresentanteFisicoDTO representanteFisico;
	
	@XmlElement (name = "representanteJuridico")
	private RepresentanteJuridicoDTO representanteJuridico;
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public RepresentanteFisicoDTO getRepresentanteFisico() {
		return representanteFisico;
	}

	public void setRepresentanteFisico(RepresentanteFisicoDTO representanteFisico) {
		this.representanteFisico = representanteFisico;
	}

	public RepresentanteJuridicoDTO getRepresentanteJuridico() {
		return representanteJuridico;
	}

	public void setRepresentanteJuridico(RepresentanteJuridicoDTO representanteJuridico) {
		this.representanteJuridico = representanteJuridico;
	}
}
