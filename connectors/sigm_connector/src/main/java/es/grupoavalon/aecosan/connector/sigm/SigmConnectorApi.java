package es.grupoavalon.aecosan.connector.sigm;

import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmDocAttachDTORequest;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTORequest;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTOResponse;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterResponseDTO;

public interface SigmConnectorApi {

	SigmCreateRegisterResponseDTO createRegister(SigmCreateRegisterRequestDTO notification);

	void attachDocument(SigmDocAttachDTORequest request);

	SigmMultipleDocAttachDTOResponse attachMultipleDocument(SigmMultipleDocAttachDTORequest request);

}
