package es.grupoavalon.aecosan.connector.sigm.util;

import javax.xml.bind.annotation.XmlElement;

import es.msssi.regtel._2015._1.Registro;

public class ElementoSolicitud {
	@XmlElement(name = "datos")
	private Registro datos;

	public void setDatos(Registro datos) {
		this.datos = datos;
	}
}
