package es.grupoavalon.aecosan.connector.sigm.dto.register;

import javax.xml.bind.annotation.XmlElement;

public class RepresentanteFisicoDTO {
    @XmlElement(name = "tipoDocumento", required = true)
    private String tipoDocumento;
    
    @XmlElement(name = "numeroDocumento", required = true)
    private String numeroDocumento;
    
    @XmlElement(name = "nombre", required = true)
    private String nombre;
    
    @XmlElement(name = "primerApellido", required = true)
    private String primerApellido;
    
    @XmlElement(name = "secundoApellido", required = true)
    private String segundoApellido;

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
}
