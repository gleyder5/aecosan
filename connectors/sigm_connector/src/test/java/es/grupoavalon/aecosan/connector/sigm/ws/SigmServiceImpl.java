package es.grupoavalon.aecosan.connector.sigm.ws;

import java.io.IOException;
import java.io.InputStream;

import javax.jws.WebService;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.interceptor.InInterceptors;

import es.grupoavalon.aecosan.connector.sigm.util.IncludeAcuse;
import es.grupoavalon.aecosan.connector.sigm.util.SigmConstants;
import es.msssi.regtel._2015._1.Acuse;
import es.msssi.regtel._2015._1.ErrorResponse;
import es.msssi.regtel._2015._1.ObjectFactory;
import es.msssi.regtel._2015._1.PeticionAdjuntarDocumento;
import es.msssi.regtel._2015._1.PeticionDetalle;
import es.msssi.regtel._2015._1.PeticionGenerarAcuse;
import es.msssi.regtel._2015._1.PeticionRecuperarDocumento;
import es.msssi.regtel._2015._1.PeticionRegistro;
import es.msssi.regtel._2015._1.RespuestaAdjuntarDocumento;
import es.msssi.regtel._2015._1.RespuestaDetalle;
import es.msssi.regtel._2015._1.RespuestaGenerarAcuse;
import es.msssi.regtel._2015._1.RespuestaRecuperarDocumento;
import es.msssi.regtel._2015._1.RespuestaRegistro;
import es.msssi.sigm.ws.facade.SigmService;

@WebService(serviceName = "SigmServiceService",
			targetNamespace = "http://facade.ws.sigm.msssi.es/",
			portName = "SigmServicePort")
@InInterceptors(interceptors = { "es.grupoavalon.aecosan.connector.sigm.ws.interceptors.SecurityHeaderInterceptor" })
public class SigmServiceImpl implements SigmService {

	private static final String ACUSE_PDF_FILENAME = "AcuseReciboRegistroEntrada201599900003715.pdf";


	@Override
	public RespuestaDetalle detallar(PeticionDetalle request) {
		// No se usa
		return null;
	}

	@Override
	public RespuestaAdjuntarDocumento adjuntarDocumento(PeticionAdjuntarDocumento request) {
		RespuestaAdjuntarDocumento response = null;
		ObjectFactory factory = new ObjectFactory();
		try {
			if (request.getDocumento().getContenido().length == 0) {
				response = instnaciateRespuestaAdjuntarDocumentoKo(factory);
			} else {
				response = instnaciateRespuestaAdjuntarDocumentoOk(factory);
			}
		} catch (Exception e) {
			response = instnaciateRespuestaAdjuntarDocumentoKo(factory);
		}
		return response;
	}

	private static final RespuestaAdjuntarDocumento instnaciateRespuestaAdjuntarDocumentoOk(ObjectFactory factory) {
		RespuestaAdjuntarDocumento response = factory.createRespuestaAdjuntarDocumento();
		response.setEstadoRespuesta("OK");
		return response;
	}

	private static final RespuestaAdjuntarDocumento instnaciateRespuestaAdjuntarDocumentoKo(ObjectFactory factory) {
		RespuestaAdjuntarDocumento response = factory.createRespuestaAdjuntarDocumento();
		response.setEstadoRespuesta("ERROR");
		ErrorResponse errorResponse = factory.createErrorResponse();
		errorResponse.setCodigo(SigmConstants.ERROR_GENERAL);
		response.setErrorResponse(errorResponse);
		return response;
	}

	@Override
	public RespuestaRegistro registrar(PeticionRegistro request) {
		ObjectFactory factory = new ObjectFactory();
		RespuestaRegistro retVal = instanciateOkResponse(factory);

		try {
			retVal.setAcuse(createRightAcuse(factory, request));
		} catch (IOException e) {
			instanciateErrorResponse(factory);
		}
		return retVal;
	}

	private static RespuestaRegistro instanciateOkResponse(ObjectFactory factory) {

		RespuestaRegistro retVal = factory.createRespuestaRegistro();
		retVal.setEstadoRespuesta("OK");
		retVal.setNumeroRegistro("201599900003394");
		retVal.setFechaRegistro("Mon Aug 31 19:45:20 CEST 2015");
		retVal.setEstadoRegistro("0");
		return retVal;
	}


	private Acuse createRightAcuse(ObjectFactory factory, PeticionRegistro request) throws IOException {
		Acuse retVal = factory.createAcuse();
		retVal.setNombre(ACUSE_PDF_FILENAME);
		retVal.setCsv("RDKLM-Q3V56-XC4ZS-UZT6M");
		if (StringUtils.equals(IncludeAcuse.YES.getValue(), request.getContenidoAcuse())) {
			retVal.setContenido(getAcuseDummieBytes());
		}
		return retVal;
	}

	private static final byte[] getAcuseDummieBytes() throws IOException {
		InputStream is = SigmServiceImpl.class.getClassLoader().getResourceAsStream(ACUSE_PDF_FILENAME);
		return IOUtils.toByteArray(is);
	}

	private static RespuestaRegistro instanciateErrorResponse(ObjectFactory factory) {
		RespuestaRegistro retVal = factory.createRespuestaRegistro();
		retVal = factory.createRespuestaRegistro();
		retVal.setEstadoRespuesta("ERROR");
		ErrorResponse errorResponse = factory.createErrorResponse();
		errorResponse.setCodigo(SigmConstants.ERROR_GENERAL);
		retVal.setErrorResponse(errorResponse);
		return retVal;
	}

	@Override
	public es.msssi.regtel._2015._1.RespuestaBusqueda buscar(es.msssi.regtel._2015._1.PeticionBusqueda request) {
		// No se usa
		return null;
	}

	@Override
	public RespuestaGenerarAcuse generarAcuse(PeticionGenerarAcuse request) {
		// No se usa
		return null;
	}

	@Override
	public RespuestaRecuperarDocumento recuperarDocumento(PeticionRecuperarDocumento request) {
		// No se usa
		return null;
	}
}
