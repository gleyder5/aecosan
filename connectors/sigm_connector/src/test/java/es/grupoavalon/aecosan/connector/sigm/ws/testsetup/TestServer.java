package es.grupoavalon.aecosan.connector.sigm.ws.testsetup;

import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

public final class TestServer {
	
	private static final String TEST_URL="http://localhost:9000/";
	
	private JaxWsServerFactoryBean factory;
	
	public void serverCreate(Object webserviceImpl, Class<?> webserviceClassInterface, String servicePublicName ){
		factory = new JaxWsServerFactoryBean();
		//register WebService interface
		factory.setServiceClass(webserviceClassInterface);
		//publish the interface
		factory.setAddress(TEST_URL+servicePublicName);
		factory.setServiceBean(webserviceImpl);
		//create WebService instance
		factory.create();
		System.out.println("Server started at:"+factory.getAddress());
	}
	
	
	public void serverDestroy(){
		this.factory.destroy();
		System.out.println("Server stoped at:"+factory.getAddress());
	}

}
