package es.grupoavalon.aecosan.connector.sigm;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterResponseDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SolicitudDTO;
import es.grupoavalon.aecosan.connector.sigm.impl.SigmConnectorApiImpl;
import es.grupoavalon.aecosan.connector.sigm.util.SigmConstants;
import es.grupoavalon.aecosan.connector.sigm.ws.SigmServiceImpl;
import es.grupoavalon.aecosan.connector.sigm.ws.testsetup.TestServer;


public class SigmConnectorApiTest {
	private static final Logger logger = LoggerFactory.getLogger(SigmConnectorApiTest.class);
	// private static final String TEST_WS_URL =
	// "https://sigmpreproduccion.msc.es/SIGEM_Webservices/SigmServiceService?WSDL";
	private static final String TEST_WS_URL = "http://localhost:9000/sigm_connector?wsdl";
	private static TestServer testServer = new TestServer();

	private static final String WS_USERNAME = "SECOSAN";
	private static final String WS_PASSWORD = "KXtOYhzQ2jxRGI5COCJC";
	
	private static final String TEST_DATA_DESTINO = "EA0008480";
	private static final String TEST_DATA_NUMERO_TRANSPORTE = null;
	private static final String TEST_DATA_OFICINA = "999";
	//private static final String TEST_DATA_ORIGEN = "EA0008480";
	private static final String TEST_DATA_RESUMEN = null;
	private static final String TEST_DATA_TIPO_ASUNTO = SigmConstants.TIPO_ASUNTO_GENERICO;
	private static final String TEST_DATA_TIPO_REGISTRO = SigmConstants.TIPO_REGISTRO_ENTRADA;
	private static final String TEST_DATA_TIPO_TRANSPORTE = SigmConstants.TIPO_TRANSPORTE_CORREO_POSTAL;
	/*private static final String TEST_DATA_INTERESADO_NOMBRE = "Nombre Interesado";
	private static final String TEST_DATA_INTERESADO_NUMERO_DOCUMENTO = "07498700X";
	private static final String TEST_DATA_INTERESADO_PRIMER_APELLIDO = "Apel. Interesado";
	private static final String TEST_DATA_INTERESADO_SEGUNDO_APELLIDO = "Apel. Interesado";
	private static final String TEST_DATA_INTERESADO_TIPO_DOCUMENTO = SigmConstants.TIPO_DOCUMENTO_NIF;*/
	
	private SigmConnectorApi connector;
	private SigmCreateRegisterRequestDTO request;
	
	@Rule
	public ExpectedException exception = ExpectedException.none();

	@BeforeClass
	public static void beforeClass() {
		testServer.serverCreate(new SigmServiceImpl(), SigmServiceImpl.class, "sigm_connector");
	}	

	@AfterClass
	public static void afterClass() {
		testServer.serverDestroy();
	}

	@Before
	public void before() throws Exception {
		connector = new SigmConnectorApiImpl();
		request = getRightRequestDTO();
	}

	@Test
	public void testSendNotificationOK() throws Exception {
		try {
			SigmCreateRegisterResponseDTO response = connector.createRegister(request);
			logger.debug("SALE: " + response.getNumeroRegistro());
		} catch (Exception e) {
			logger.error("Test error", e);
			throw e;
		}
	}
	
	private SigmCreateRegisterRequestDTO getRightRequestDTO() throws Exception {
		SigmCreateRegisterRequestDTO retVal = new SigmCreateRegisterRequestDTO();
		retVal.setUrl(TEST_WS_URL);
		retVal.setUsername(WS_USERNAME);
		retVal.setPassword(WS_PASSWORD);
		retVal.setSolicitud(getRightSolicitudDTO());
		return retVal;
	}

	private SolicitudDTO getRightSolicitudDTO() {
		SolicitudDTO retVal = new SolicitudDTO();
		retVal.setDestino(TEST_DATA_DESTINO);
		retVal.setFechaRegistroOriginal(null);
		//retVal.setInteresadosFisicos(getRightInteresadosFisicosDTO());
		retVal.setInteresadosJuridicos(null);
		retVal.setNumeroRegistroOriginal(null);
		retVal.setNumeroTransporte(TEST_DATA_NUMERO_TRANSPORTE);
		retVal.setOficina(TEST_DATA_OFICINA);
		//retVal.setOrigen(TEST_DATA_ORIGEN);
		retVal.setRefExpediente(null);
		retVal.setResumen(TEST_DATA_RESUMEN);
		retVal.setTipoAsunto(TEST_DATA_TIPO_ASUNTO);
		retVal.setTipoRegistro(TEST_DATA_TIPO_REGISTRO);
		retVal.setTipoRegistroOriginal(null);
		retVal.setTipoTransporte(TEST_DATA_TIPO_TRANSPORTE);
		return retVal;
	}

	/*private List<InteresadoFisicoDTO> getRightInteresadosFisicosDTO() {
		List<InteresadoFisicoDTO> retVal = new ArrayList<InteresadoFisicoDTO>();
		InteresadoFisicoDTO dto = new InteresadoFisicoDTO();
		dto.setNombre(TEST_DATA_INTERESADO_NOMBRE);
		dto.setNumeroDocumento(TEST_DATA_INTERESADO_NUMERO_DOCUMENTO);
		dto.setPrimerApellido(TEST_DATA_INTERESADO_PRIMER_APELLIDO);
		dto.setRepresentanteFisico(null);
		dto.setRepresentanteJuridico(null);
		dto.setSegundoApellido(TEST_DATA_INTERESADO_SEGUNDO_APELLIDO);
		dto.setTipoDocumento(TEST_DATA_INTERESADO_TIPO_DOCUMENTO);
		retVal.add(dto);
		return retVal;
	}*/

}
