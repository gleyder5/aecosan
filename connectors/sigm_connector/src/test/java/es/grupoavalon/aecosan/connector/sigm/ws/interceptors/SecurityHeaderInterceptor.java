package es.grupoavalon.aecosan.connector.sigm.ws.interceptors;

import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.binding.soap.interceptor.AbstractSoapInterceptor;
import org.apache.cxf.phase.Phase;
import org.apache.wss4j.dom.WSConstants;

public class SecurityHeaderInterceptor extends AbstractSoapInterceptor {

	private static final Set<QName> HEADERS = new HashSet<QName>();
	static {
		HEADERS.add(new QName(WSConstants.WSSE_NS, WSConstants.WSSE_LN));
		HEADERS.add(new QName(WSConstants.WSSE11_NS, WSConstants.WSSE_LN));
		HEADERS.add(new QName(WSConstants.ENC_NS, WSConstants.ENC_DATA_LN));
	}

	public SecurityHeaderInterceptor() {
		super(Phase.PRE_PROTOCOL);
	}

	public SecurityHeaderInterceptor(String phase) {
		super(phase);

	}

	@Override
	public Set<QName> getUnderstoodHeaders() {
		return HEADERS;
	}

	@Override
	public void handleMessage(SoapMessage soapMessage) {

	}

}
