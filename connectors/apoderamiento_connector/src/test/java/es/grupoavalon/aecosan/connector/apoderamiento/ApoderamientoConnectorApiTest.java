package es.grupoavalon.aecosan.connector.apoderamiento;

import java.util.GregorianCalendar;

import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoRequestDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarDescargaListaWsDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.impl.ApoderamientoConnectorApiImpl;
import es.grupoavalon.aecosan.connector.apoderamiento.ws.ApoderamientoServiceImpl;
import es.map.funcionario.ws.WSREACXFService;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.connector.apoderamiento.testsetup.TestServer;



public class ApoderamientoConnectorApiTest {
	private ApoderamientoConnectorApi connector;
	private static final Logger logger = LoggerFactory.getLogger(ApoderamientoConnectorApiTest.class);
	 private static final String TEST_WS_URL = "http://localhost:9000/apoderamiento_connector?wsdl";
//	private static final String TEST_WS_URL = "https://pre-epago.redsara.es/ppiban/pasarela.wsdl ";
	private static TestServer testServer = new TestServer();
//	private static TestWSServer  testServer = new TestWSServer();


	private static final String TEST_DATA_CODIGO_TASA = "001";
	private static final String TEST_DATA_MODELO = "790";
	private static final int TEST_DATA_ID_ORGANISMO = 24681;
	private static final String TEST_DATA_CCC = "ES5421040000119064547550";
	private static final String TEST_DATA_APELLIDO1 = "LOPEZ";
	private static final String TEST_DATA_APELLIDO2 = "RUIZ";
	private static final String TEST_DATA_CODIGOBANCO = "2104";
	private static final String TEST_DATA_DOCUMENTO_OBLIGADO = "50179623V";
	private static final GregorianCalendar TEST_DATA_FECHA_CADUCIDAD_TARJETA = new GregorianCalendar(2019, 5, 17, 23, 59);

	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@BeforeClass
	public static void beforeClass() {

		 testServer.serverCreate(new ApoderamientoServiceImpl(), WSREACXFService.class,
		 "apoderamiento_connector");
	}	

	@AfterClass
	public static void afterClass() {
//		 testServer.serverDestroy();
	}

	@Before 
	public void before() throws Exception {
		connector = new ApoderamientoConnectorApiImpl();
	}

	private ConsultarApoderamientoRequestDTO crearConsultaApoderamientoInvalidDate(){
		ConsultarApoderamientoRequestDTO retVal = new ConsultarApoderamientoRequestDTO();
		retVal.setUrl(TEST_WS_URL);
		retVal.setCIFApoderado("11111111T");
		retVal.setCIFPoderdante("00000000T");
		retVal.setCodApoderamiento(1);
		retVal.setEstado(1);
		retVal.setFechaFin("2017-02-15");
		retVal.setFechaInicio("2018-03-09");
		retVal.setNIFPoderdante("00000000T");
		retVal.setIdTramiteREA(1);
		retVal.setIdCategoriaREA(1);
		retVal.setIdOrganismo("1");
		return  retVal;
	}

	private ConsultarApoderamientoRequestDTO crearConsultaApoderamiento(){
		ConsultarApoderamientoRequestDTO retVal = new ConsultarApoderamientoRequestDTO();
		retVal.setUrl(TEST_WS_URL);
		retVal.setCIFApoderado("1111111T");
		retVal.setCIFPoderdante("00000T");
		retVal.setCodApoderamiento(1);
		retVal.setEstado(1);
		retVal.setFechaFin("2018-12-31");
		retVal.setFechaInicio("2018-12-31");
		retVal.setNIFPoderdante("00000T");
		retVal.setIdTramiteREA(1);
		retVal.setIdCategoriaREA(1);
		retVal.setIdOrganismo("1");
		return  retVal;
	}
	private  ConsultarApoderamientoResponseDTO consultarApoderamiento(ConsultarApoderamientoRequestDTO retVal){
		ConsultarApoderamientoResponseDTO consultarApoderamientoResponseDTO;
		consultarApoderamientoResponseDTO = connector.consultarApoderamiento(retVal);
		consultarApoderamientoResponseDTO.getConsultaApoderamientoWSResponseSalida();
		return  consultarApoderamientoResponseDTO;

	}

	@Test
	public void consultarApoderamientoBasicDataShouldBeOK(){

		ConsultarApoderamientoResponseDTO consultarApoderamientoResponseDTO ;
		consultarApoderamientoResponseDTO =  consultarApoderamiento(crearConsultaApoderamiento());
		Assert.assertNotNull(consultarApoderamientoResponseDTO);



	}

}
