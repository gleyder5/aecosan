package es.grupoavalon.aecosan.connector.apoderamiento.ws;

import es.map.funcionario.ws.*;

import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;



import java.util.List;

@WebService(serviceName = "WSREACXFService",
			targetNamespace = "http://es.gob.apoderamiento/schemas",
			endpointInterface = "es.map.funcionario.ws.WSREACXFService",
			portName = "WSREACXFServicePort")
public class ApoderamientoServiceImpl implements WSREACXFService {

	@Override
	public SubsanarApoderamientoWSResponseSalida subsanarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos, AsesorJuridicoWS asesorJuridicoWS) {
		return null;
	}

	@Override
	public ConsultarApoderamientoWSResponseSalida consultarApoderamiento(Integer codApoderamiento, String nifPoderdante, String cifPoderdante, String nifApoderado, String cifApoderado, String nifRepresentante, String idOrganismo, Integer idTramiteREA, String idTramiteOrganismo, Integer idCategoriaREA, Integer estado, String fechaInicio, String fechaFin) {
		ObjectFactory factory = new ObjectFactory();
		ConsultarApoderamientoWSResponseSalida consultarApoderamientoWSResponseSalida = factory.createConsultarApoderamientoWSResponseSalida();
		ConsultaDescargaListaWS consultaDescargaListaWS =  factory.createConsultaDescargaListaWS();
		PoderdanteApoderadoWS poderdanteApoderadoWS = factory.createPoderdanteApoderadoWS();
		PoderdanteApoderadoWS apoderadoWS = factory.createPoderdanteApoderadoWS();
		if(cifPoderdante.equals("00000000T")) {
			poderdanteApoderadoWS.setCIF(cifPoderdante);
			poderdanteApoderadoWS.setNombre("Nombre");
			poderdanteApoderadoWS.setPrimerApellido("Apellido");
			poderdanteApoderadoWS.setSegundoApellido("Segundo");

			consultaDescargaListaWS.setCodApoderamiento(codApoderamiento);
			consultaDescargaListaWS.setPoderdante(poderdanteApoderadoWS);

			try {
			//	consultaDescargaListaWS.setFechaFin(DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaFin));
			//	consultaDescargaListaWS.setFechaInicio(DatatypeFactory.newInstance().newXMLGregorianCalendar(fechaInicio));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			ErrorWS errorWS = new ErrorWS();
			errorWS.setCodError(100);
			errorWS.setDesError("Esta persona no se encuentra apoderada");
			consultarApoderamientoWSResponseSalida.setError(errorWS);
		}

		consultarApoderamientoWSResponseSalida.getLista().add(consultaDescargaListaWS);
		return consultarApoderamientoWSResponseSalida;

	}

	@Override
	public ConsultarCategoriasPorOrganismoWSResponseSalida consultarCategoriasPorOrganismo(String codOrganismo) {
		return null;
	}

	@Override
	public ModificarApoderamientoWSResponseSalida modificarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos, String fechaHastaVigencia) {
		return null;
	}

	@Override
	public RevocarApoderamientoWSResponseSalida revocarApoderamiento(String organismo, String numRegistro, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos) {
		return null;
	}

	@Override
	public RenunciarApoderamientoWSResponseSalida renunciarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEApoderado, String cifApoderado, List<Anexo> anexos) {
		return null;
	}

	@Override
	public DenegarApoderamientoWSResponseSalida denegarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos, AsesorJuridicoWS asesorJuridicoWS) {
		return null;
	}

	@Override
	public AltaApoderamientoWSResponseSalida altaApoderamiento(PoderdanteApoderadoWS poderdante, PoderdanteApoderadoWS apoderado, PfRepresentantePodWS representante, String organismo, List<Integer> listaCodCategorias, List<Integer> listaCodTramites, String fechaInicio, String fechaFin, List<Anexo> anexos) {
		return null;
	}

	@Override
	public ConsultarTramitesPorOrganismoWSResponseSalida consultarTramitesPorOrganismo(String codOrganismo) {
		return null;
	}

	@Override
	public NoSubsanarApoderamientoWSResponseSalida noSubsanarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos, AsesorJuridicoWS asesorJuridicoWS) {
		return null;
	}

	@Override
	public AprobarApoderamientoWSResponseSalida aprobarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEPoderdante, String cifPoderdante, List<Anexo> anexos, AsesorJuridicoWS asesorJuridicoWS) {
		return null;
	}

	@Override
	public AceptarApoderamientoWSResponseSalida aceptarApoderamiento(String organismo, Integer codApoderamiento, String nifNIEApoderado, String cifApoderado, List<Anexo> anexos) {
		return null;
	}
}
