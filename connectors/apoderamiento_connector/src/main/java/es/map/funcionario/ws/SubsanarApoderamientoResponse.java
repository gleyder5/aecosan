
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for subsanarApoderamientoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="subsanarApoderamientoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.funcionario.map.es/}subsanarApoderamientoWSResponseSalida" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "subsanarApoderamientoResponse", propOrder = {
    "_return"
})
public class SubsanarApoderamientoResponse {

    @XmlElement(name = "return", namespace = "http://ws.funcionario.map.es/")
    protected SubsanarApoderamientoWSResponseSalida _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link SubsanarApoderamientoWSResponseSalida }
     *     
     */
    public SubsanarApoderamientoWSResponseSalida getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubsanarApoderamientoWSResponseSalida }
     *     
     */
    public void setReturn(SubsanarApoderamientoWSResponseSalida value) {
        this._return = value;
    }

}
