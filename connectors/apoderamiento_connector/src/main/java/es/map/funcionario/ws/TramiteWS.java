
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tramiteWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tramiteWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTramiteREA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nombreTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vigencia" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tramiteWS", propOrder = {
    "codOrganismo",
    "codTramite",
    "descripcionTramite",
    "idTramiteREA",
    "nombreTramite",
    "vigencia"
})
public class TramiteWS {

    protected String codOrganismo;
    protected String codTramite;
    protected String descripcionTramite;
    protected int idTramiteREA;
    protected String nombreTramite;
    protected int vigencia;

    /**
     * Gets the value of the codOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrganismo() {
        return codOrganismo;
    }

    /**
     * Sets the value of the codOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrganismo(String value) {
        this.codOrganismo = value;
    }

    /**
     * Gets the value of the codTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTramite() {
        return codTramite;
    }

    /**
     * Sets the value of the codTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTramite(String value) {
        this.codTramite = value;
    }

    /**
     * Gets the value of the descripcionTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionTramite() {
        return descripcionTramite;
    }

    /**
     * Sets the value of the descripcionTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionTramite(String value) {
        this.descripcionTramite = value;
    }

    /**
     * Gets the value of the idTramiteREA property.
     * 
     */
    public int getIdTramiteREA() {
        return idTramiteREA;
    }

    /**
     * Sets the value of the idTramiteREA property.
     * 
     */
    public void setIdTramiteREA(int value) {
        this.idTramiteREA = value;
    }

    /**
     * Gets the value of the nombreTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreTramite() {
        return nombreTramite;
    }

    /**
     * Sets the value of the nombreTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreTramite(String value) {
        this.nombreTramite = value;
    }

    /**
     * Gets the value of the vigencia property.
     * 
     */
    public int getVigencia() {
        return vigencia;
    }

    /**
     * Sets the value of the vigencia property.
     * 
     */
    public void setVigencia(int value) {
        this.vigencia = value;
    }

}
