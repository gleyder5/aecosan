
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaPersonaJuridica complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaPersonaJuridica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descRazon" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idPersonaJuridica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="reaApoderamientosForIdPJuridicaApo" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaApoderamientosForIdPJuridicaPod" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaRepresentacions" type="{http://ws.funcionario.map.es/}reaRepresentacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaPersonaJuridica", propOrder = {
    "codCif",
    "descRazon",
    "fhCreacion",
    "fhModificacion",
    "idPersonaJuridica",
    "reaApoderamientosForIdPJuridicaApo",
    "reaApoderamientosForIdPJuridicaPod",
    "reaRepresentacions",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion"
})
public class ReaPersonaJuridica {

    protected String codCif;
    protected String descRazon;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Long idPersonaJuridica;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientosForIdPJuridicaApo;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientosForIdPJuridicaPod;
    @XmlElement(nillable = true)
    protected List<ReaRepresentacion> reaRepresentacions;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;

    /**
     * Gets the value of the codCif property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCif() {
        return codCif;
    }

    /**
     * Sets the value of the codCif property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCif(String value) {
        this.codCif = value;
    }

    /**
     * Gets the value of the descRazon property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRazon() {
        return descRazon;
    }

    /**
     * Sets the value of the descRazon property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRazon(String value) {
        this.descRazon = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idPersonaJuridica property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPersonaJuridica() {
        return idPersonaJuridica;
    }

    /**
     * Sets the value of the idPersonaJuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPersonaJuridica(Long value) {
        this.idPersonaJuridica = value;
    }

    /**
     * Gets the value of the reaApoderamientosForIdPJuridicaApo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientosForIdPJuridicaApo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientosForIdPJuridicaApo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientosForIdPJuridicaApo() {
        if (reaApoderamientosForIdPJuridicaApo == null) {
            reaApoderamientosForIdPJuridicaApo = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientosForIdPJuridicaApo;
    }

    /**
     * Gets the value of the reaApoderamientosForIdPJuridicaPod property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientosForIdPJuridicaPod property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientosForIdPJuridicaPod().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientosForIdPJuridicaPod() {
        if (reaApoderamientosForIdPJuridicaPod == null) {
            reaApoderamientosForIdPJuridicaPod = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientosForIdPJuridicaPod;
    }

    /**
     * Gets the value of the reaRepresentacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaRepresentacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaRepresentacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaRepresentacion }
     * 
     * 
     */
    public List<ReaRepresentacion> getReaRepresentacions() {
        if (reaRepresentacions == null) {
            reaRepresentacions = new ArrayList<ReaRepresentacion>();
        }
        return this.reaRepresentacions;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

}
