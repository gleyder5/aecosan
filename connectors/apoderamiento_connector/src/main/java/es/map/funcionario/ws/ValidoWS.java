
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validoWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validoWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codApoderamiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentoJustificante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validoWS", propOrder = {
    "codApoderamiento",
    "codOrganismo",
    "documentoJustificante",
    "estado",
    "numRegistro"
})
public class ValidoWS {

    protected int codApoderamiento;
    protected String codOrganismo;
    protected String documentoJustificante;
    protected int estado;
    protected String numRegistro;

    /**
     * Gets the value of the codApoderamiento property.
     * 
     */
    public int getCodApoderamiento() {
        return codApoderamiento;
    }

    /**
     * Sets the value of the codApoderamiento property.
     * 
     */
    public void setCodApoderamiento(int value) {
        this.codApoderamiento = value;
    }

    /**
     * Gets the value of the codOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrganismo() {
        return codOrganismo;
    }

    /**
     * Sets the value of the codOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrganismo(String value) {
        this.codOrganismo = value;
    }

    /**
     * Gets the value of the documentoJustificante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoJustificante() {
        return documentoJustificante;
    }

    /**
     * Sets the value of the documentoJustificante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoJustificante(String value) {
        this.documentoJustificante = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     */
    public void setEstado(int value) {
        this.estado = value;
    }

    /**
     * Gets the value of the numRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRegistro() {
        return numRegistro;
    }

    /**
     * Sets the value of the numRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRegistro(String value) {
        this.numRegistro = value;
    }

}
