
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for consultaDescargaListaWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultaDescargaListaWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anexos" type="{http://ws.funcionario.map.es/}anexo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="apoderado" type="{http://ws.funcionario.map.es/}poderdanteApoderadoWS" minOccurs="0"/>
 *         &lt;element name="codApoderamiento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="fechaFin" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="listaCategoriasPorOrganismo" type="{http://ws.funcionario.map.es/}categoriaWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaTramitesPorOrganismo" type="{http://ws.funcionario.map.es/}tramiteWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poderdante" type="{http://ws.funcionario.map.es/}poderdanteApoderadoWS" minOccurs="0"/>
 *         &lt;element name="representante" type="{http://ws.funcionario.map.es/}poderdanteApoderadoWS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultaDescargaListaWS", propOrder = {
    "anexos",
    "apoderado",
    "codApoderamiento",
    "codOrganismo",
    "estado",
    "fechaFin",
    "fechaInicio",
    "listaCategoriasPorOrganismo",
    "listaTramitesPorOrganismo",
    "numRegistro",
    "poderdante",
    "representante"
})
public class ConsultaDescargaListaWS {

    @XmlElement(nillable = true)
    protected List<Anexo> anexos;
    protected PoderdanteApoderadoWS apoderado;
    protected int codApoderamiento;
    protected String codOrganismo;
    protected int estado;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaFin;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaInicio;
    @XmlElement(nillable = true)
    protected List<CategoriaWS> listaCategoriasPorOrganismo;
    @XmlElement(nillable = true)
    protected List<TramiteWS> listaTramitesPorOrganismo;
    protected String numRegistro;
    protected PoderdanteApoderadoWS poderdante;
    protected PoderdanteApoderadoWS representante;

    /**
     * Gets the value of the anexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Anexo }
     * 
     * 
     */
    public List<Anexo> getAnexos() {
        if (anexos == null) {
            anexos = new ArrayList<Anexo>();
        }
        return this.anexos;
    }

    /**
     * Gets the value of the apoderado property.
     * 
     * @return
     *     possible object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public PoderdanteApoderadoWS getApoderado() {
        return apoderado;
    }

    /**
     * Sets the value of the apoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public void setApoderado(PoderdanteApoderadoWS value) {
        this.apoderado = value;
    }

    /**
     * Gets the value of the codApoderamiento property.
     * 
     */
    public int getCodApoderamiento() {
        return codApoderamiento;
    }

    /**
     * Sets the value of the codApoderamiento property.
     * 
     */
    public void setCodApoderamiento(int value) {
        this.codApoderamiento = value;
    }

    /**
     * Gets the value of the codOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrganismo() {
        return codOrganismo;
    }

    /**
     * Sets the value of the codOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrganismo(String value) {
        this.codOrganismo = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     */
    public int getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     */
    public void setEstado(int value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fechaFin property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaFin() {
        return fechaFin;
    }

    /**
     * Sets the value of the fechaFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaFin(XMLGregorianCalendar value) {
        this.fechaFin = value;
    }

    /**
     * Gets the value of the fechaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Sets the value of the fechaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaInicio(XMLGregorianCalendar value) {
        this.fechaInicio = value;
    }

    /**
     * Gets the value of the listaCategoriasPorOrganismo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCategoriasPorOrganismo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCategoriasPorOrganismo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoriaWS }
     * 
     * 
     */
    public List<CategoriaWS> getListaCategoriasPorOrganismo() {
        if (listaCategoriasPorOrganismo == null) {
            listaCategoriasPorOrganismo = new ArrayList<CategoriaWS>();
        }
        return this.listaCategoriasPorOrganismo;
    }

    /**
     * Gets the value of the listaTramitesPorOrganismo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaTramitesPorOrganismo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaTramitesPorOrganismo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TramiteWS }
     * 
     * 
     */
    public List<TramiteWS> getListaTramitesPorOrganismo() {
        if (listaTramitesPorOrganismo == null) {
            listaTramitesPorOrganismo = new ArrayList<TramiteWS>();
        }
        return this.listaTramitesPorOrganismo;
    }

    /**
     * Gets the value of the numRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRegistro() {
        return numRegistro;
    }

    /**
     * Sets the value of the numRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRegistro(String value) {
        this.numRegistro = value;
    }

    /**
     * Gets the value of the poderdante property.
     * 
     * @return
     *     possible object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public PoderdanteApoderadoWS getPoderdante() {
        return poderdante;
    }

    /**
     * Sets the value of the poderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public void setPoderdante(PoderdanteApoderadoWS value) {
        this.poderdante = value;
    }

    /**
     * Gets the value of the representante property.
     * 
     * @return
     *     possible object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public PoderdanteApoderadoWS getRepresentante() {
        return representante;
    }

    /**
     * Sets the value of the representante property.
     * 
     * @param value
     *     allowed object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public void setRepresentante(PoderdanteApoderadoWS value) {
        this.representante = value;
    }

}
