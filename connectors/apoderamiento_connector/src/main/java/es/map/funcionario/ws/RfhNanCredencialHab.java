
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rfhNanCredencialHab complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhNanCredencialHab">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://ws.funcionario.map.es/}rfhNanCredencialHabId" minOccurs="0"/>
 *         &lt;element name="rfhCredencial" type="{http://ws.funcionario.map.es/}rfhCredencial" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacion" type="{http://ws.funcionario.map.es/}rfhHabilitacion" minOccurs="0"/>
 *         &lt;element name="rfhHistHabilitacion" type="{http://ws.funcionario.map.es/}rfhHistHabilitacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhNanCredencialHab", propOrder = {
    "id",
    "rfhCredencial",
    "rfhHabilitacion",
    "rfhHistHabilitacion"
})
public class RfhNanCredencialHab {

    protected RfhNanCredencialHabId id;
    protected RfhCredencial rfhCredencial;
    protected RfhHabilitacion rfhHabilitacion;
    protected RfhHistHabilitacion rfhHistHabilitacion;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link RfhNanCredencialHabId }
     *     
     */
    public RfhNanCredencialHabId getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhNanCredencialHabId }
     *     
     */
    public void setId(RfhNanCredencialHabId value) {
        this.id = value;
    }

    /**
     * Gets the value of the rfhCredencial property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCredencial }
     *     
     */
    public RfhCredencial getRfhCredencial() {
        return rfhCredencial;
    }

    /**
     * Sets the value of the rfhCredencial property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCredencial }
     *     
     */
    public void setRfhCredencial(RfhCredencial value) {
        this.rfhCredencial = value;
    }

    /**
     * Gets the value of the rfhHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link RfhHabilitacion }
     *     
     */
    public RfhHabilitacion getRfhHabilitacion() {
        return rfhHabilitacion;
    }

    /**
     * Sets the value of the rfhHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhHabilitacion }
     *     
     */
    public void setRfhHabilitacion(RfhHabilitacion value) {
        this.rfhHabilitacion = value;
    }

    /**
     * Gets the value of the rfhHistHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link RfhHistHabilitacion }
     *     
     */
    public RfhHistHabilitacion getRfhHistHabilitacion() {
        return rfhHistHabilitacion;
    }

    /**
     * Sets the value of the rfhHistHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhHistHabilitacion }
     *     
     */
    public void setRfhHistHabilitacion(RfhHistHabilitacion value) {
        this.rfhHistHabilitacion = value;
    }

}
