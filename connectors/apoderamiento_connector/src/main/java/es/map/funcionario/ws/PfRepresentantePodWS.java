
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pfRepresentantePodWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pfRepresentantePodWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="delegacion" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaAnexos" type="{http://ws.funcionario.map.es/}anexoBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nifnie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="primerApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="segundoApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pfRepresentantePodWS", propOrder = {
    "delegacion",
    "email",
    "listaAnexos",
    "nifnie",
    "nombre",
    "primerApellido",
    "segundoApellido",
    "telefono"
})
public class PfRepresentantePodWS {

    protected boolean delegacion;
    protected String email;
    @XmlElement(nillable = true)
    protected List<AnexoBean> listaAnexos;
    protected String nifnie;
    protected String nombre;
    protected String primerApellido;
    protected String segundoApellido;
    protected int telefono;

    /**
     * Gets the value of the delegacion property.
     * 
     */
    public boolean isDelegacion() {
        return delegacion;
    }

    /**
     * Sets the value of the delegacion property.
     * 
     */
    public void setDelegacion(boolean value) {
        this.delegacion = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the listaAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnexoBean }
     * 
     * 
     */
    public List<AnexoBean> getListaAnexos() {
        if (listaAnexos == null) {
            listaAnexos = new ArrayList<AnexoBean>();
        }
        return this.listaAnexos;
    }

    /**
     * Gets the value of the nifnie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNifnie() {
        return nifnie;
    }

    /**
     * Sets the value of the nifnie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNifnie(String value) {
        this.nifnie = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the primerApellido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * Sets the value of the primerApellido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimerApellido(String value) {
        this.primerApellido = value;
    }

    /**
     * Gets the value of the segundoApellido property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * Sets the value of the segundoApellido property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegundoApellido(String value) {
        this.segundoApellido = value;
    }

    /**
     * Gets the value of the telefono property.
     * 
     */
    public int getTelefono() {
        return telefono;
    }

    /**
     * Sets the value of the telefono property.
     * 
     */
    public void setTelefono(int value) {
        this.telefono = value;
    }

}
