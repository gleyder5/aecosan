
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaRepresentacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaRepresentacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idDelegacion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idRepresentacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="reaAnexoRepresentacions" type="{http://ws.funcionario.map.es/}reaAnexoRepresentacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaCatEstadoRepresentacion" type="{http://ws.funcionario.map.es/}reaCatEstadoRepresentacion" minOccurs="0"/>
 *         &lt;element name="reaPersonaFisica" type="{http://ws.funcionario.map.es/}reaPersonaFisica" minOccurs="0"/>
 *         &lt;element name="reaPersonaFisicaRep" type="{http://ws.funcionario.map.es/}reaPersonaFisica" minOccurs="0"/>
 *         &lt;element name="reaPersonaJuridica" type="{http://ws.funcionario.map.es/}reaPersonaJuridica" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaRepresentacion", propOrder = {
    "fhCreacion",
    "fhModificacion",
    "idDelegacion",
    "idRepresentacion",
    "reaAnexoRepresentacions",
    "reaCatEstadoRepresentacion",
    "reaPersonaFisica",
    "reaPersonaFisicaRep",
    "reaPersonaJuridica",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion"
})
public class ReaRepresentacion {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idDelegacion;
    protected Long idRepresentacion;
    @XmlElement(nillable = true)
    protected List<ReaAnexoRepresentacion> reaAnexoRepresentacions;
    protected ReaCatEstadoRepresentacion reaCatEstadoRepresentacion;
    protected ReaPersonaFisica reaPersonaFisica;
    protected ReaPersonaFisica reaPersonaFisicaRep;
    protected ReaPersonaJuridica reaPersonaJuridica;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idDelegacion property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdDelegacion() {
        return idDelegacion;
    }

    /**
     * Sets the value of the idDelegacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdDelegacion(Boolean value) {
        this.idDelegacion = value;
    }

    /**
     * Gets the value of the idRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdRepresentacion() {
        return idRepresentacion;
    }

    /**
     * Sets the value of the idRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdRepresentacion(Long value) {
        this.idRepresentacion = value;
    }

    /**
     * Gets the value of the reaAnexoRepresentacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAnexoRepresentacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAnexoRepresentacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexoRepresentacion }
     * 
     * 
     */
    public List<ReaAnexoRepresentacion> getReaAnexoRepresentacions() {
        if (reaAnexoRepresentacions == null) {
            reaAnexoRepresentacions = new ArrayList<ReaAnexoRepresentacion>();
        }
        return this.reaAnexoRepresentacions;
    }

    /**
     * Gets the value of the reaCatEstadoRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatEstadoRepresentacion }
     *     
     */
    public ReaCatEstadoRepresentacion getReaCatEstadoRepresentacion() {
        return reaCatEstadoRepresentacion;
    }

    /**
     * Sets the value of the reaCatEstadoRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatEstadoRepresentacion }
     *     
     */
    public void setReaCatEstadoRepresentacion(ReaCatEstadoRepresentacion value) {
        this.reaCatEstadoRepresentacion = value;
    }

    /**
     * Gets the value of the reaPersonaFisica property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public ReaPersonaFisica getReaPersonaFisica() {
        return reaPersonaFisica;
    }

    /**
     * Sets the value of the reaPersonaFisica property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public void setReaPersonaFisica(ReaPersonaFisica value) {
        this.reaPersonaFisica = value;
    }

    /**
     * Gets the value of the reaPersonaFisicaRep property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public ReaPersonaFisica getReaPersonaFisicaRep() {
        return reaPersonaFisicaRep;
    }

    /**
     * Sets the value of the reaPersonaFisicaRep property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public void setReaPersonaFisicaRep(ReaPersonaFisica value) {
        this.reaPersonaFisicaRep = value;
    }

    /**
     * Gets the value of the reaPersonaJuridica property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public ReaPersonaJuridica getReaPersonaJuridica() {
        return reaPersonaJuridica;
    }

    /**
     * Sets the value of the reaPersonaJuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public void setReaPersonaJuridica(ReaPersonaJuridica value) {
        this.reaPersonaJuridica = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

}
