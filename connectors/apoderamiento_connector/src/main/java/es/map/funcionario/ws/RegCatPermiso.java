
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regCatPermiso complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCatPermiso">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denomPermiso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPermiso" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regCatTipoNotificacions" type="{http://ws.funcionario.map.es/}regCatTipoNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioPermisos" type="{http://ws.funcionario.map.es/}regNanUsuarioPermiso" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCatPermiso", propOrder = {
    "denomPermiso",
    "idPermiso",
    "regCatTipoNotificacions",
    "regNanUsuarioPermisos"
})
public class RegCatPermiso {

    protected String denomPermiso;
    protected Integer idPermiso;
    @XmlElement(nillable = true)
    protected List<RegCatTipoNotificacion> regCatTipoNotificacions;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioPermiso> regNanUsuarioPermisos;

    /**
     * Gets the value of the denomPermiso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomPermiso() {
        return denomPermiso;
    }

    /**
     * Sets the value of the denomPermiso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomPermiso(String value) {
        this.denomPermiso = value;
    }

    /**
     * Gets the value of the idPermiso property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdPermiso() {
        return idPermiso;
    }

    /**
     * Sets the value of the idPermiso property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdPermiso(Integer value) {
        this.idPermiso = value;
    }

    /**
     * Gets the value of the regCatTipoNotificacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCatTipoNotificacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCatTipoNotificacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCatTipoNotificacion }
     * 
     * 
     */
    public List<RegCatTipoNotificacion> getRegCatTipoNotificacions() {
        if (regCatTipoNotificacions == null) {
            regCatTipoNotificacions = new ArrayList<RegCatTipoNotificacion>();
        }
        return this.regCatTipoNotificacions;
    }

    /**
     * Gets the value of the regNanUsuarioPermisos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioPermisos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioPermisos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioPermiso }
     * 
     * 
     */
    public List<RegNanUsuarioPermiso> getRegNanUsuarioPermisos() {
        if (regNanUsuarioPermisos == null) {
            regNanUsuarioPermisos = new ArrayList<RegNanUsuarioPermiso>();
        }
        return this.regNanUsuarioPermisos;
    }

}
