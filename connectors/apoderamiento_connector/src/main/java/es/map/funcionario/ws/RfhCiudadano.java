
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhCiudadano complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhCiudadano">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codNifNie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDomicilio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idCodpostal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idSeqCiudadano" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regProvincia" type="{http://ws.funcionario.map.es/}regProvincia" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhHistCiudadanos" type="{http://ws.funcionario.map.es/}rfhHistCiudadano" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhTramitacions" type="{http://ws.funcionario.map.es/}rfhTramitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhCiudadano", propOrder = {
    "codNifNie",
    "descApellido1",
    "descApellido2",
    "descDomicilio",
    "descMunicipio",
    "descNombre",
    "fhCreacion",
    "fhModificacion",
    "idCodpostal",
    "idSeqCiudadano",
    "idTipoDocumento",
    "regProvincia",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhHistCiudadanos",
    "rfhTramitacions"
})
public class RfhCiudadano {

    protected String codNifNie;
    protected String descApellido1;
    protected String descApellido2;
    protected String descDomicilio;
    protected String descMunicipio;
    protected String descNombre;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Integer idCodpostal;
    protected Long idSeqCiudadano;
    protected Integer idTipoDocumento;
    protected RegProvincia regProvincia;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhHistCiudadano> rfhHistCiudadanos;
    @XmlElement(nillable = true)
    protected List<RfhTramitacion> rfhTramitacions;

    /**
     * Gets the value of the codNifNie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNifNie() {
        return codNifNie;
    }

    /**
     * Sets the value of the codNifNie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNifNie(String value) {
        this.codNifNie = value;
    }

    /**
     * Gets the value of the descApellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido1() {
        return descApellido1;
    }

    /**
     * Sets the value of the descApellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido1(String value) {
        this.descApellido1 = value;
    }

    /**
     * Gets the value of the descApellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido2() {
        return descApellido2;
    }

    /**
     * Sets the value of the descApellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido2(String value) {
        this.descApellido2 = value;
    }

    /**
     * Gets the value of the descDomicilio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDomicilio() {
        return descDomicilio;
    }

    /**
     * Sets the value of the descDomicilio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDomicilio(String value) {
        this.descDomicilio = value;
    }

    /**
     * Gets the value of the descMunicipio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMunicipio() {
        return descMunicipio;
    }

    /**
     * Sets the value of the descMunicipio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMunicipio(String value) {
        this.descMunicipio = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idCodpostal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdCodpostal() {
        return idCodpostal;
    }

    /**
     * Sets the value of the idCodpostal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdCodpostal(Integer value) {
        this.idCodpostal = value;
    }

    /**
     * Gets the value of the idSeqCiudadano property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdSeqCiudadano() {
        return idSeqCiudadano;
    }

    /**
     * Sets the value of the idSeqCiudadano property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdSeqCiudadano(Long value) {
        this.idSeqCiudadano = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoDocumento(Integer value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the regProvincia property.
     * 
     * @return
     *     possible object is
     *     {@link RegProvincia }
     *     
     */
    public RegProvincia getRegProvincia() {
        return regProvincia;
    }

    /**
     * Sets the value of the regProvincia property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegProvincia }
     *     
     */
    public void setRegProvincia(RegProvincia value) {
        this.regProvincia = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhHistCiudadanos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHistCiudadanos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHistCiudadanos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHistCiudadano }
     * 
     * 
     */
    public List<RfhHistCiudadano> getRfhHistCiudadanos() {
        if (rfhHistCiudadanos == null) {
            rfhHistCiudadanos = new ArrayList<RfhHistCiudadano>();
        }
        return this.rfhHistCiudadanos;
    }

    /**
     * Gets the value of the rfhTramitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhTramitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhTramitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhTramitacion }
     * 
     * 
     */
    public List<RfhTramitacion> getRfhTramitacions() {
        if (rfhTramitacions == null) {
            rfhTramitacions = new ArrayList<RfhTramitacion>();
        }
        return this.rfhTramitacions;
    }

}
