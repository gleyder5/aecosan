
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rfhNanCredencialHabId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhNanCredencialHabId">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idCredencial" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idHabilitacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhNanCredencialHabId", propOrder = {
    "idCredencial",
    "idHabilitacion"
})
public class RfhNanCredencialHabId {

    protected long idCredencial;
    protected Long idHabilitacion;

    /**
     * Gets the value of the idCredencial property.
     * 
     */
    public long getIdCredencial() {
        return idCredencial;
    }

    /**
     * Sets the value of the idCredencial property.
     * 
     */
    public void setIdCredencial(long value) {
        this.idCredencial = value;
    }

    /**
     * Gets the value of the idHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHabilitacion() {
        return idHabilitacion;
    }

    /**
     * Sets the value of the idHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHabilitacion(Long value) {
        this.idHabilitacion = value;
    }

}
