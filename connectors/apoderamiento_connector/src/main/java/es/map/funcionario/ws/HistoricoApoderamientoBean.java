
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for historicoApoderamientoBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="historicoApoderamientoBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denominacionOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denominacionOrganismoOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRegistroRec" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idHistoricoApoderamiento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idRegistroRec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TAnexos" type="{http://ws.funcionario.map.es/}reaAnexo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TApoderamiento" type="{http://ws.funcionario.map.es/}reaApoderamiento" minOccurs="0"/>
 *         &lt;element name="TTEstado" type="{http://ws.funcionario.map.es/}reaCatEstado" minOccurs="0"/>
 *         &lt;element name="usuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="usuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "historicoApoderamientoBean", propOrder = {
    "denominacionOrganismo",
    "denominacionOrganismoOrigen",
    "fhCreacion",
    "fhModificacion",
    "fhRegistroRec",
    "idHistoricoApoderamiento",
    "idRegistroRec",
    "numTotal",
    "tAnexos",
    "tApoderamiento",
    "ttEstado",
    "usuarioCreacion",
    "usuarioModificacion",
    "vigenciaDesde",
    "vigenciaHasta"
})
public class HistoricoApoderamientoBean {

    protected String denominacionOrganismo;
    protected String denominacionOrganismoOrigen;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRegistroRec;
    protected Long idHistoricoApoderamiento;
    protected String idRegistroRec;
    protected int numTotal;
    @XmlElement(name = "TAnexos", nillable = true)
    protected List<ReaAnexo> tAnexos;
    @XmlElement(name = "TApoderamiento")
    protected ReaApoderamiento tApoderamiento;
    @XmlElement(name = "TTEstado")
    protected ReaCatEstado ttEstado;
    protected RegUsuario usuarioCreacion;
    protected RegUsuario usuarioModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaDesde;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaHasta;

    /**
     * Gets the value of the denominacionOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominacionOrganismo() {
        return denominacionOrganismo;
    }

    /**
     * Sets the value of the denominacionOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominacionOrganismo(String value) {
        this.denominacionOrganismo = value;
    }

    /**
     * Gets the value of the denominacionOrganismoOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominacionOrganismoOrigen() {
        return denominacionOrganismoOrigen;
    }

    /**
     * Sets the value of the denominacionOrganismoOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominacionOrganismoOrigen(String value) {
        this.denominacionOrganismoOrigen = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the fhRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRegistroRec() {
        return fhRegistroRec;
    }

    /**
     * Sets the value of the fhRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRegistroRec(XMLGregorianCalendar value) {
        this.fhRegistroRec = value;
    }

    /**
     * Gets the value of the idHistoricoApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistoricoApoderamiento() {
        return idHistoricoApoderamiento;
    }

    /**
     * Sets the value of the idHistoricoApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistoricoApoderamiento(Long value) {
        this.idHistoricoApoderamiento = value;
    }

    /**
     * Gets the value of the idRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistroRec() {
        return idRegistroRec;
    }

    /**
     * Sets the value of the idRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistroRec(String value) {
        this.idRegistroRec = value;
    }

    /**
     * Gets the value of the numTotal property.
     * 
     */
    public int getNumTotal() {
        return numTotal;
    }

    /**
     * Sets the value of the numTotal property.
     * 
     */
    public void setNumTotal(int value) {
        this.numTotal = value;
    }

    /**
     * Gets the value of the tAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexo }
     * 
     * 
     */
    public List<ReaAnexo> getTAnexos() {
        if (tAnexos == null) {
            tAnexos = new ArrayList<ReaAnexo>();
        }
        return this.tAnexos;
    }

    /**
     * Gets the value of the tApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link ReaApoderamiento }
     *     
     */
    public ReaApoderamiento getTApoderamiento() {
        return tApoderamiento;
    }

    /**
     * Sets the value of the tApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaApoderamiento }
     *     
     */
    public void setTApoderamiento(ReaApoderamiento value) {
        this.tApoderamiento = value;
    }

    /**
     * Gets the value of the ttEstado property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatEstado }
     *     
     */
    public ReaCatEstado getTTEstado() {
        return ttEstado;
    }

    /**
     * Sets the value of the ttEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatEstado }
     *     
     */
    public void setTTEstado(ReaCatEstado value) {
        this.ttEstado = value;
    }

    /**
     * Gets the value of the usuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getUsuarioCreacion() {
        return usuarioCreacion;
    }

    /**
     * Sets the value of the usuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setUsuarioCreacion(RegUsuario value) {
        this.usuarioCreacion = value;
    }

    /**
     * Gets the value of the usuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getUsuarioModificacion() {
        return usuarioModificacion;
    }

    /**
     * Sets the value of the usuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setUsuarioModificacion(RegUsuario value) {
        this.usuarioModificacion = value;
    }

    /**
     * Gets the value of the vigenciaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaDesde() {
        return vigenciaDesde;
    }

    /**
     * Sets the value of the vigenciaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaDesde(XMLGregorianCalendar value) {
        this.vigenciaDesde = value;
    }

    /**
     * Gets the value of the vigenciaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaHasta() {
        return vigenciaHasta;
    }

    /**
     * Sets the value of the vigenciaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaHasta(XMLGregorianCalendar value) {
        this.vigenciaHasta = value;
    }

}
