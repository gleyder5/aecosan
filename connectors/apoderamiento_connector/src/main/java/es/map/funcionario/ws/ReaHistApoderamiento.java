
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaHistApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaHistApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRegistroRec" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idHistoricoApod" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idRegistroRec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reaAnexos" type="{http://ws.funcionario.map.es/}reaAnexo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaApoderamiento" type="{http://ws.funcionario.map.es/}reaApoderamiento" minOccurs="0"/>
 *         &lt;element name="reaCatEstado" type="{http://ws.funcionario.map.es/}reaCatEstado" minOccurs="0"/>
 *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaHistApoderamiento", propOrder = {
    "fhCreacion",
    "fhModificacion",
    "fhRegistroRec",
    "idHistoricoApod",
    "idRegistroRec",
    "idUsuarioCreacion",
    "idUsuarioModificacion",
    "reaAnexos",
    "reaApoderamiento",
    "reaCatEstado",
    "vigenciaDesde",
    "vigenciaHasta"
})
public class ReaHistApoderamiento {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRegistroRec;
    protected Long idHistoricoApod;
    protected String idRegistroRec;
    protected String idUsuarioCreacion;
    protected String idUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<ReaAnexo> reaAnexos;
    protected ReaApoderamiento reaApoderamiento;
    protected ReaCatEstado reaCatEstado;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaDesde;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaHasta;

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the fhRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRegistroRec() {
        return fhRegistroRec;
    }

    /**
     * Sets the value of the fhRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRegistroRec(XMLGregorianCalendar value) {
        this.fhRegistroRec = value;
    }

    /**
     * Gets the value of the idHistoricoApod property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistoricoApod() {
        return idHistoricoApod;
    }

    /**
     * Sets the value of the idHistoricoApod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistoricoApod(Long value) {
        this.idHistoricoApod = value;
    }

    /**
     * Gets the value of the idRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistroRec() {
        return idRegistroRec;
    }

    /**
     * Sets the value of the idRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistroRec(String value) {
        this.idRegistroRec = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuarioCreacion(String value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the idUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    /**
     * Sets the value of the idUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuarioModificacion(String value) {
        this.idUsuarioModificacion = value;
    }

    /**
     * Gets the value of the reaAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexo }
     * 
     * 
     */
    public List<ReaAnexo> getReaAnexos() {
        if (reaAnexos == null) {
            reaAnexos = new ArrayList<ReaAnexo>();
        }
        return this.reaAnexos;
    }

    /**
     * Gets the value of the reaApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link ReaApoderamiento }
     *     
     */
    public ReaApoderamiento getReaApoderamiento() {
        return reaApoderamiento;
    }

    /**
     * Sets the value of the reaApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaApoderamiento }
     *     
     */
    public void setReaApoderamiento(ReaApoderamiento value) {
        this.reaApoderamiento = value;
    }

    /**
     * Gets the value of the reaCatEstado property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatEstado }
     *     
     */
    public ReaCatEstado getReaCatEstado() {
        return reaCatEstado;
    }

    /**
     * Sets the value of the reaCatEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatEstado }
     *     
     */
    public void setReaCatEstado(ReaCatEstado value) {
        this.reaCatEstado = value;
    }

    /**
     * Gets the value of the vigenciaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaDesde() {
        return vigenciaDesde;
    }

    /**
     * Sets the value of the vigenciaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaDesde(XMLGregorianCalendar value) {
        this.vigenciaDesde = value;
    }

    /**
     * Gets the value of the vigenciaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaHasta() {
        return vigenciaHasta;
    }

    /**
     * Sets the value of the vigenciaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaHasta(XMLGregorianCalendar value) {
        this.vigenciaHasta = value;
    }

}
