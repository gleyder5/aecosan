
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultarApoderamientoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultarApoderamientoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.funcionario.map.es/}consultarApoderamientoWSResponseSalida" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultarApoderamientoResponse", propOrder = {
    "_return"
})
public class ConsultarApoderamientoResponse {

    @XmlElement(name = "return", namespace = "http://ws.funcionario.map.es/")
    protected ConsultarApoderamientoWSResponseSalida _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link ConsultarApoderamientoWSResponseSalida }
     *     
     */
    public ConsultarApoderamientoWSResponseSalida getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsultarApoderamientoWSResponseSalida }
     *     
     */
    public void setReturn(ConsultarApoderamientoWSResponseSalida value) {
        this._return = value;
    }

}
