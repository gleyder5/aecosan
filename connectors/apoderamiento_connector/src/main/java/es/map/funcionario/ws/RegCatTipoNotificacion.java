
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regCatTipoNotificacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCatTipoNotificacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descTipoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoNotificacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regCatPermisos" type="{http://ws.funcionario.map.es/}regCatPermiso" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regCatTipoMetodoNots" type="{http://ws.funcionario.map.es/}regCatTipoMetodoNot" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCatTipoNotificacion", propOrder = {
    "descTipoNotificacion",
    "idTipoNotificacion",
    "regCatPermisos",
    "regCatTipoMetodoNots"
})
public class RegCatTipoNotificacion {

    protected String descTipoNotificacion;
    protected Integer idTipoNotificacion;
    @XmlElement(nillable = true)
    protected List<RegCatPermiso> regCatPermisos;
    @XmlElement(nillable = true)
    protected List<RegCatTipoMetodoNot> regCatTipoMetodoNots;

    /**
     * Gets the value of the descTipoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoNotificacion() {
        return descTipoNotificacion;
    }

    /**
     * Sets the value of the descTipoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoNotificacion(String value) {
        this.descTipoNotificacion = value;
    }

    /**
     * Gets the value of the idTipoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    /**
     * Sets the value of the idTipoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoNotificacion(Integer value) {
        this.idTipoNotificacion = value;
    }

    /**
     * Gets the value of the regCatPermisos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCatPermisos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCatPermisos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCatPermiso }
     * 
     * 
     */
    public List<RegCatPermiso> getRegCatPermisos() {
        if (regCatPermisos == null) {
            regCatPermisos = new ArrayList<RegCatPermiso>();
        }
        return this.regCatPermisos;
    }

    /**
     * Gets the value of the regCatTipoMetodoNots property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCatTipoMetodoNots property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCatTipoMetodoNots().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCatTipoMetodoNot }
     * 
     * 
     */
    public List<RegCatTipoMetodoNot> getRegCatTipoMetodoNots() {
        if (regCatTipoMetodoNots == null) {
            regCatTipoMetodoNots = new ArrayList<RegCatTipoMetodoNot>();
        }
        return this.regCatTipoMetodoNots;
    }

}
