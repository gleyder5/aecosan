
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regOrganismoAdherido complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regOrganismoAdherido">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="altaManual" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="codCif" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idOrganismo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reaAdministracions" type="{http://ws.funcionario.map.es/}reaAdministracion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaApoderamientosForIdOrgOrigen" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaApoderamientosForIdOrganismo" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regCategorias" type="{http://ws.funcionario.map.es/}regCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regHistTramites" type="{http://ws.funcionario.map.es/}regHistTramite" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regLogAccesoses" type="{http://ws.funcionario.map.es/}regLogAccesos" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioOrganismos" type="{http://ws.funcionario.map.es/}regNanUsuarioOrganismo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNotificacions" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regTramites" type="{http://ws.funcionario.map.es/}regTramite" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhFuncionarios" type="{http://ws.funcionario.map.es/}rfhFuncionario" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regOrganismoAdherido", propOrder = {
    "altaManual",
    "codCif",
    "codUoOrganismo",
    "denomOrganismo",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idOrganismo",
    "reaAdministracions",
    "reaApoderamientosForIdOrgOrigen",
    "reaApoderamientosForIdOrganismo",
    "regCategorias",
    "regHistTramites",
    "regLogAccesoses",
    "regNanUsuarioOrganismos",
    "regNotificacions",
    "regTramites",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhFuncionarios"
})
public class RegOrganismoAdherido {

    protected Boolean altaManual;
    protected String codCif;
    protected String codUoOrganismo;
    protected String denomOrganismo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Integer idOrganismo;
    @XmlElement(nillable = true)
    protected List<ReaAdministracion> reaAdministracions;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientosForIdOrgOrigen;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientosForIdOrganismo;
    @XmlElement(nillable = true)
    protected List<RegCategoria> regCategorias;
    @XmlElement(nillable = true)
    protected List<RegHistTramite> regHistTramites;
    @XmlElement(nillable = true)
    protected List<RegLogAccesos> regLogAccesoses;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioOrganismo> regNanUsuarioOrganismos;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacions;
    @XmlElement(nillable = true)
    protected List<RegTramite> regTramites;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhFuncionario> rfhFuncionarios;

    /**
     * Gets the value of the altaManual property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAltaManual() {
        return altaManual;
    }

    /**
     * Sets the value of the altaManual property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAltaManual(Boolean value) {
        this.altaManual = value;
    }

    /**
     * Gets the value of the codCif property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCif() {
        return codCif;
    }

    /**
     * Sets the value of the codCif property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCif(String value) {
        this.codCif = value;
    }

    /**
     * Gets the value of the codUoOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoOrganismo() {
        return codUoOrganismo;
    }

    /**
     * Sets the value of the codUoOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoOrganismo(String value) {
        this.codUoOrganismo = value;
    }

    /**
     * Gets the value of the denomOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomOrganismo() {
        return denomOrganismo;
    }

    /**
     * Sets the value of the denomOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomOrganismo(String value) {
        this.denomOrganismo = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdOrganismo() {
        return idOrganismo;
    }

    /**
     * Sets the value of the idOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdOrganismo(Integer value) {
        this.idOrganismo = value;
    }

    /**
     * Gets the value of the reaAdministracions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAdministracions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAdministracions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAdministracion }
     * 
     * 
     */
    public List<ReaAdministracion> getReaAdministracions() {
        if (reaAdministracions == null) {
            reaAdministracions = new ArrayList<ReaAdministracion>();
        }
        return this.reaAdministracions;
    }

    /**
     * Gets the value of the reaApoderamientosForIdOrgOrigen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientosForIdOrgOrigen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientosForIdOrgOrigen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientosForIdOrgOrigen() {
        if (reaApoderamientosForIdOrgOrigen == null) {
            reaApoderamientosForIdOrgOrigen = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientosForIdOrgOrigen;
    }

    /**
     * Gets the value of the reaApoderamientosForIdOrganismo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientosForIdOrganismo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientosForIdOrganismo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientosForIdOrganismo() {
        if (reaApoderamientosForIdOrganismo == null) {
            reaApoderamientosForIdOrganismo = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientosForIdOrganismo;
    }

    /**
     * Gets the value of the regCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCategoria }
     * 
     * 
     */
    public List<RegCategoria> getRegCategorias() {
        if (regCategorias == null) {
            regCategorias = new ArrayList<RegCategoria>();
        }
        return this.regCategorias;
    }

    /**
     * Gets the value of the regHistTramites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regHistTramites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegHistTramites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegHistTramite }
     * 
     * 
     */
    public List<RegHistTramite> getRegHistTramites() {
        if (regHistTramites == null) {
            regHistTramites = new ArrayList<RegHistTramite>();
        }
        return this.regHistTramites;
    }

    /**
     * Gets the value of the regLogAccesoses property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regLogAccesoses property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegLogAccesoses().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegLogAccesos }
     * 
     * 
     */
    public List<RegLogAccesos> getRegLogAccesoses() {
        if (regLogAccesoses == null) {
            regLogAccesoses = new ArrayList<RegLogAccesos>();
        }
        return this.regLogAccesoses;
    }

    /**
     * Gets the value of the regNanUsuarioOrganismos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioOrganismos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioOrganismos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioOrganismo }
     * 
     * 
     */
    public List<RegNanUsuarioOrganismo> getRegNanUsuarioOrganismos() {
        if (regNanUsuarioOrganismos == null) {
            regNanUsuarioOrganismos = new ArrayList<RegNanUsuarioOrganismo>();
        }
        return this.regNanUsuarioOrganismos;
    }

    /**
     * Gets the value of the regNotificacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacions() {
        if (regNotificacions == null) {
            regNotificacions = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacions;
    }

    /**
     * Gets the value of the regTramites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regTramites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegTramites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegTramite }
     * 
     * 
     */
    public List<RegTramite> getRegTramites() {
        if (regTramites == null) {
            regTramites = new ArrayList<RegTramite>();
        }
        return this.regTramites;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhFuncionarios property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhFuncionarios property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhFuncionarios().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhFuncionario }
     * 
     * 
     */
    public List<RfhFuncionario> getRfhFuncionarios() {
        if (rfhFuncionarios == null) {
            rfhFuncionarios = new ArrayList<RfhFuncionario>();
        }
        return this.rfhFuncionarios;
    }

}
