
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reaCatEstadoRepresentacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaCatEstadoRepresentacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descDenominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEstado" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reaRepresentacions" type="{http://ws.funcionario.map.es/}reaRepresentacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaCatEstadoRepresentacion", propOrder = {
    "descDenominacion",
    "descDescripcion",
    "idEstado",
    "reaRepresentacions"
})
public class ReaCatEstadoRepresentacion {

    protected String descDenominacion;
    protected String descDescripcion;
    protected Integer idEstado;
    @XmlElement(nillable = true)
    protected List<ReaRepresentacion> reaRepresentacions;

    /**
     * Gets the value of the descDenominacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDenominacion() {
        return descDenominacion;
    }

    /**
     * Sets the value of the descDenominacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDenominacion(String value) {
        this.descDenominacion = value;
    }

    /**
     * Gets the value of the descDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDescripcion() {
        return descDescripcion;
    }

    /**
     * Sets the value of the descDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDescripcion(String value) {
        this.descDescripcion = value;
    }

    /**
     * Gets the value of the idEstado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * Sets the value of the idEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstado(Integer value) {
        this.idEstado = value;
    }

    /**
     * Gets the value of the reaRepresentacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaRepresentacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaRepresentacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaRepresentacion }
     * 
     * 
     */
    public List<ReaRepresentacion> getReaRepresentacions() {
        if (reaRepresentacions == null) {
            reaRepresentacions = new ArrayList<ReaRepresentacion>();
        }
        return this.reaRepresentacions;
    }

}
