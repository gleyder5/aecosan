
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaAnexoRepresentacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaAnexoRepresentacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoElaboracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codIdentificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNombreAlfresco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codOrgano" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codOrigen" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="codTipoFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codVersionNti" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contenido" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="contenidoFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCaptura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idAnexoRepresentacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="reaCatFormato" type="{http://ws.funcionario.map.es/}reaCatFormato" minOccurs="0"/>
 *         &lt;element name="reaCatTipoDocumento" type="{http://ws.funcionario.map.es/}reaCatTipoDocumento" minOccurs="0"/>
 *         &lt;element name="reaCatTipodocEni" type="{http://ws.funcionario.map.es/}reaCatTipodocEni" minOccurs="0"/>
 *         &lt;element name="reaHistRepresentacion" type="{http://ws.funcionario.map.es/}reaHistRepresentacion" minOccurs="0"/>
 *         &lt;element name="reaRepresentacion" type="{http://ws.funcionario.map.es/}reaRepresentacion" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="resenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaAnexoRepresentacion", propOrder = {
    "codCve",
    "codEstadoElaboracion",
    "codIdentificador",
    "codNombreAlfresco",
    "codOrgano",
    "codOrigen",
    "codTipoFirma",
    "codVersionNti",
    "contenido",
    "contenidoFirma",
    "descDescripcion",
    "descNombre",
    "fhCaptura",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idAnexoRepresentacion",
    "reaCatFormato",
    "reaCatTipoDocumento",
    "reaCatTipodocEni",
    "reaHistRepresentacion",
    "reaRepresentacion",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "resenha"
})
public class ReaAnexoRepresentacion {

    protected String codCve;
    protected String codEstadoElaboracion;
    protected String codIdentificador;
    protected String codNombreAlfresco;
    protected String codOrgano;
    protected Boolean codOrigen;
    protected String codTipoFirma;
    protected String codVersionNti;
    protected byte[] contenido;
    protected String contenidoFirma;
    protected String descDescripcion;
    protected String descNombre;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCaptura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Long idAnexoRepresentacion;
    protected ReaCatFormato reaCatFormato;
    protected ReaCatTipoDocumento reaCatTipoDocumento;
    protected ReaCatTipodocEni reaCatTipodocEni;
    protected ReaHistRepresentacion reaHistRepresentacion;
    protected ReaRepresentacion reaRepresentacion;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    protected String resenha;

    /**
     * Gets the value of the codCve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCve() {
        return codCve;
    }

    /**
     * Sets the value of the codCve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCve(String value) {
        this.codCve = value;
    }

    /**
     * Gets the value of the codEstadoElaboracion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoElaboracion() {
        return codEstadoElaboracion;
    }

    /**
     * Sets the value of the codEstadoElaboracion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoElaboracion(String value) {
        this.codEstadoElaboracion = value;
    }

    /**
     * Gets the value of the codIdentificador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodIdentificador() {
        return codIdentificador;
    }

    /**
     * Sets the value of the codIdentificador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodIdentificador(String value) {
        this.codIdentificador = value;
    }

    /**
     * Gets the value of the codNombreAlfresco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNombreAlfresco() {
        return codNombreAlfresco;
    }

    /**
     * Sets the value of the codNombreAlfresco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNombreAlfresco(String value) {
        this.codNombreAlfresco = value;
    }

    /**
     * Gets the value of the codOrgano property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrgano() {
        return codOrgano;
    }

    /**
     * Sets the value of the codOrgano property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrgano(String value) {
        this.codOrgano = value;
    }

    /**
     * Gets the value of the codOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCodOrigen() {
        return codOrigen;
    }

    /**
     * Sets the value of the codOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCodOrigen(Boolean value) {
        this.codOrigen = value;
    }

    /**
     * Gets the value of the codTipoFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoFirma() {
        return codTipoFirma;
    }

    /**
     * Sets the value of the codTipoFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoFirma(String value) {
        this.codTipoFirma = value;
    }

    /**
     * Gets the value of the codVersionNti property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodVersionNti() {
        return codVersionNti;
    }

    /**
     * Sets the value of the codVersionNti property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodVersionNti(String value) {
        this.codVersionNti = value;
    }

    /**
     * Gets the value of the contenido property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContenido() {
        return contenido;
    }

    /**
     * Sets the value of the contenido property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContenido(byte[] value) {
        this.contenido = value;
    }

    /**
     * Gets the value of the contenidoFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContenidoFirma() {
        return contenidoFirma;
    }

    /**
     * Sets the value of the contenidoFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContenidoFirma(String value) {
        this.contenidoFirma = value;
    }

    /**
     * Gets the value of the descDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDescripcion() {
        return descDescripcion;
    }

    /**
     * Sets the value of the descDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDescripcion(String value) {
        this.descDescripcion = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the fhCaptura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCaptura() {
        return fhCaptura;
    }

    /**
     * Sets the value of the fhCaptura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCaptura(XMLGregorianCalendar value) {
        this.fhCaptura = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idAnexoRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdAnexoRepresentacion() {
        return idAnexoRepresentacion;
    }

    /**
     * Sets the value of the idAnexoRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdAnexoRepresentacion(Long value) {
        this.idAnexoRepresentacion = value;
    }

    /**
     * Gets the value of the reaCatFormato property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatFormato }
     *     
     */
    public ReaCatFormato getReaCatFormato() {
        return reaCatFormato;
    }

    /**
     * Sets the value of the reaCatFormato property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatFormato }
     *     
     */
    public void setReaCatFormato(ReaCatFormato value) {
        this.reaCatFormato = value;
    }

    /**
     * Gets the value of the reaCatTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatTipoDocumento }
     *     
     */
    public ReaCatTipoDocumento getReaCatTipoDocumento() {
        return reaCatTipoDocumento;
    }

    /**
     * Sets the value of the reaCatTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatTipoDocumento }
     *     
     */
    public void setReaCatTipoDocumento(ReaCatTipoDocumento value) {
        this.reaCatTipoDocumento = value;
    }

    /**
     * Gets the value of the reaCatTipodocEni property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatTipodocEni }
     *     
     */
    public ReaCatTipodocEni getReaCatTipodocEni() {
        return reaCatTipodocEni;
    }

    /**
     * Sets the value of the reaCatTipodocEni property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatTipodocEni }
     *     
     */
    public void setReaCatTipodocEni(ReaCatTipodocEni value) {
        this.reaCatTipodocEni = value;
    }

    /**
     * Gets the value of the reaHistRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link ReaHistRepresentacion }
     *     
     */
    public ReaHistRepresentacion getReaHistRepresentacion() {
        return reaHistRepresentacion;
    }

    /**
     * Sets the value of the reaHistRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaHistRepresentacion }
     *     
     */
    public void setReaHistRepresentacion(ReaHistRepresentacion value) {
        this.reaHistRepresentacion = value;
    }

    /**
     * Gets the value of the reaRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link ReaRepresentacion }
     *     
     */
    public ReaRepresentacion getReaRepresentacion() {
        return reaRepresentacion;
    }

    /**
     * Sets the value of the reaRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaRepresentacion }
     *     
     */
    public void setReaRepresentacion(ReaRepresentacion value) {
        this.reaRepresentacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the resenha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResenha() {
        return resenha;
    }

    /**
     * Sets the value of the resenha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResenha(String value) {
        this.resenha = value;
    }

}
