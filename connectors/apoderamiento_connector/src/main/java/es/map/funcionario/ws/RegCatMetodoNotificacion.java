
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regCatMetodoNotificacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCatMetodoNotificacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descMetodoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idMetodoNotificacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regCatTipoMetodoNots" type="{http://ws.funcionario.map.es/}regCatTipoMetodoNot" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCatMetodoNotificacion", propOrder = {
    "descMetodoNotificacion",
    "idMetodoNotificacion",
    "regCatTipoMetodoNots"
})
public class RegCatMetodoNotificacion {

    protected String descMetodoNotificacion;
    protected Integer idMetodoNotificacion;
    @XmlElement(nillable = true)
    protected List<RegCatTipoMetodoNot> regCatTipoMetodoNots;

    /**
     * Gets the value of the descMetodoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMetodoNotificacion() {
        return descMetodoNotificacion;
    }

    /**
     * Sets the value of the descMetodoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMetodoNotificacion(String value) {
        this.descMetodoNotificacion = value;
    }

    /**
     * Gets the value of the idMetodoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdMetodoNotificacion() {
        return idMetodoNotificacion;
    }

    /**
     * Sets the value of the idMetodoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdMetodoNotificacion(Integer value) {
        this.idMetodoNotificacion = value;
    }

    /**
     * Gets the value of the regCatTipoMetodoNots property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCatTipoMetodoNots property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCatTipoMetodoNots().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCatTipoMetodoNot }
     * 
     * 
     */
    public List<RegCatTipoMetodoNot> getRegCatTipoMetodoNots() {
        if (regCatTipoMetodoNots == null) {
            regCatTipoMetodoNots = new ArrayList<RegCatTipoMetodoNot>();
        }
        return this.regCatTipoMetodoNots;
    }

}
