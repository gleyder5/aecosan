
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idApoderamiento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="reaAnexos" type="{http://ws.funcionario.map.es/}reaAnexo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaCatEstado" type="{http://ws.funcionario.map.es/}reaCatEstado" minOccurs="0"/>
 *         &lt;element name="reaHistApoderamientos" type="{http://ws.funcionario.map.es/}reaHistApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaPersonaFisicaByIdPFisicaApo" type="{http://ws.funcionario.map.es/}reaPersonaFisica" minOccurs="0"/>
 *         &lt;element name="reaPersonaFisicaByIdPFisicaPod" type="{http://ws.funcionario.map.es/}reaPersonaFisica" minOccurs="0"/>
 *         &lt;element name="reaPersonaFisicaByIdPFisicaRepPod" type="{http://ws.funcionario.map.es/}reaPersonaFisica" minOccurs="0"/>
 *         &lt;element name="reaPersonaJuridicaByIdPJuridicaApo" type="{http://ws.funcionario.map.es/}reaPersonaJuridica" minOccurs="0"/>
 *         &lt;element name="reaPersonaJuridicaByIdPJuridicaPod" type="{http://ws.funcionario.map.es/}reaPersonaJuridica" minOccurs="0"/>
 *         &lt;element name="regNanApodCategorias" type="{http://ws.funcionario.map.es/}regNanApodCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdheridoByIdOrgOrigen" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdheridoByIdOrganismo" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regTramites" type="{http://ws.funcionario.map.es/}regTramite" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="vigenciaDesde" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="vigenciaHasta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaApoderamiento", propOrder = {
    "denominacion",
    "fhCreacion",
    "fhModificacion",
    "idApoderamiento",
    "idUsuarioCreacion",
    "idUsuarioModificacion",
    "reaAnexos",
    "reaCatEstado",
    "reaHistApoderamientos",
    "reaPersonaFisicaByIdPFisicaApo",
    "reaPersonaFisicaByIdPFisicaPod",
    "reaPersonaFisicaByIdPFisicaRepPod",
    "reaPersonaJuridicaByIdPJuridicaApo",
    "reaPersonaJuridicaByIdPJuridicaPod",
    "regNanApodCategorias",
    "regOrganismoAdheridoByIdOrgOrigen",
    "regOrganismoAdheridoByIdOrganismo",
    "regTramites",
    "vigenciaDesde",
    "vigenciaHasta"
})
public class ReaApoderamiento {

    protected String denominacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Long idApoderamiento;
    protected Long idUsuarioCreacion;
    protected Long idUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<ReaAnexo> reaAnexos;
    protected ReaCatEstado reaCatEstado;
    @XmlElement(nillable = true)
    protected List<ReaHistApoderamiento> reaHistApoderamientos;
    protected ReaPersonaFisica reaPersonaFisicaByIdPFisicaApo;
    protected ReaPersonaFisica reaPersonaFisicaByIdPFisicaPod;
    protected ReaPersonaFisica reaPersonaFisicaByIdPFisicaRepPod;
    protected ReaPersonaJuridica reaPersonaJuridicaByIdPJuridicaApo;
    protected ReaPersonaJuridica reaPersonaJuridicaByIdPJuridicaPod;
    @XmlElement(nillable = true)
    protected List<RegNanApodCategoria> regNanApodCategorias;
    protected RegOrganismoAdherido regOrganismoAdheridoByIdOrgOrigen;
    protected RegOrganismoAdherido regOrganismoAdheridoByIdOrganismo;
    @XmlElement(nillable = true)
    protected List<RegTramite> regTramites;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaDesde;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar vigenciaHasta;

    /**
     * Gets the value of the denominacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominacion() {
        return denominacion;
    }

    /**
     * Sets the value of the denominacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominacion(String value) {
        this.denominacion = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdApoderamiento() {
        return idApoderamiento;
    }

    /**
     * Sets the value of the idApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdApoderamiento(Long value) {
        this.idApoderamiento = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the idUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    /**
     * Sets the value of the idUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioModificacion(Long value) {
        this.idUsuarioModificacion = value;
    }

    /**
     * Gets the value of the reaAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexo }
     * 
     * 
     */
    public List<ReaAnexo> getReaAnexos() {
        if (reaAnexos == null) {
            reaAnexos = new ArrayList<ReaAnexo>();
        }
        return this.reaAnexos;
    }

    /**
     * Gets the value of the reaCatEstado property.
     * 
     * @return
     *     possible object is
     *     {@link ReaCatEstado }
     *     
     */
    public ReaCatEstado getReaCatEstado() {
        return reaCatEstado;
    }

    /**
     * Sets the value of the reaCatEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaCatEstado }
     *     
     */
    public void setReaCatEstado(ReaCatEstado value) {
        this.reaCatEstado = value;
    }

    /**
     * Gets the value of the reaHistApoderamientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaHistApoderamientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaHistApoderamientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaHistApoderamiento }
     * 
     * 
     */
    public List<ReaHistApoderamiento> getReaHistApoderamientos() {
        if (reaHistApoderamientos == null) {
            reaHistApoderamientos = new ArrayList<ReaHistApoderamiento>();
        }
        return this.reaHistApoderamientos;
    }

    /**
     * Gets the value of the reaPersonaFisicaByIdPFisicaApo property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public ReaPersonaFisica getReaPersonaFisicaByIdPFisicaApo() {
        return reaPersonaFisicaByIdPFisicaApo;
    }

    /**
     * Sets the value of the reaPersonaFisicaByIdPFisicaApo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public void setReaPersonaFisicaByIdPFisicaApo(ReaPersonaFisica value) {
        this.reaPersonaFisicaByIdPFisicaApo = value;
    }

    /**
     * Gets the value of the reaPersonaFisicaByIdPFisicaPod property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public ReaPersonaFisica getReaPersonaFisicaByIdPFisicaPod() {
        return reaPersonaFisicaByIdPFisicaPod;
    }

    /**
     * Sets the value of the reaPersonaFisicaByIdPFisicaPod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public void setReaPersonaFisicaByIdPFisicaPod(ReaPersonaFisica value) {
        this.reaPersonaFisicaByIdPFisicaPod = value;
    }

    /**
     * Gets the value of the reaPersonaFisicaByIdPFisicaRepPod property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public ReaPersonaFisica getReaPersonaFisicaByIdPFisicaRepPod() {
        return reaPersonaFisicaByIdPFisicaRepPod;
    }

    /**
     * Sets the value of the reaPersonaFisicaByIdPFisicaRepPod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaFisica }
     *     
     */
    public void setReaPersonaFisicaByIdPFisicaRepPod(ReaPersonaFisica value) {
        this.reaPersonaFisicaByIdPFisicaRepPod = value;
    }

    /**
     * Gets the value of the reaPersonaJuridicaByIdPJuridicaApo property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public ReaPersonaJuridica getReaPersonaJuridicaByIdPJuridicaApo() {
        return reaPersonaJuridicaByIdPJuridicaApo;
    }

    /**
     * Sets the value of the reaPersonaJuridicaByIdPJuridicaApo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public void setReaPersonaJuridicaByIdPJuridicaApo(ReaPersonaJuridica value) {
        this.reaPersonaJuridicaByIdPJuridicaApo = value;
    }

    /**
     * Gets the value of the reaPersonaJuridicaByIdPJuridicaPod property.
     * 
     * @return
     *     possible object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public ReaPersonaJuridica getReaPersonaJuridicaByIdPJuridicaPod() {
        return reaPersonaJuridicaByIdPJuridicaPod;
    }

    /**
     * Sets the value of the reaPersonaJuridicaByIdPJuridicaPod property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaPersonaJuridica }
     *     
     */
    public void setReaPersonaJuridicaByIdPJuridicaPod(ReaPersonaJuridica value) {
        this.reaPersonaJuridicaByIdPJuridicaPod = value;
    }

    /**
     * Gets the value of the regNanApodCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanApodCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanApodCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanApodCategoria }
     * 
     * 
     */
    public List<RegNanApodCategoria> getRegNanApodCategorias() {
        if (regNanApodCategorias == null) {
            regNanApodCategorias = new ArrayList<RegNanApodCategoria>();
        }
        return this.regNanApodCategorias;
    }

    /**
     * Gets the value of the regOrganismoAdheridoByIdOrgOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdheridoByIdOrgOrigen() {
        return regOrganismoAdheridoByIdOrgOrigen;
    }

    /**
     * Sets the value of the regOrganismoAdheridoByIdOrgOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdheridoByIdOrgOrigen(RegOrganismoAdherido value) {
        this.regOrganismoAdheridoByIdOrgOrigen = value;
    }

    /**
     * Gets the value of the regOrganismoAdheridoByIdOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdheridoByIdOrganismo() {
        return regOrganismoAdheridoByIdOrganismo;
    }

    /**
     * Sets the value of the regOrganismoAdheridoByIdOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdheridoByIdOrganismo(RegOrganismoAdherido value) {
        this.regOrganismoAdheridoByIdOrganismo = value;
    }

    /**
     * Gets the value of the regTramites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regTramites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegTramites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegTramite }
     * 
     * 
     */
    public List<RegTramite> getRegTramites() {
        if (regTramites == null) {
            regTramites = new ArrayList<RegTramite>();
        }
        return this.regTramites;
    }

    /**
     * Gets the value of the vigenciaDesde property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaDesde() {
        return vigenciaDesde;
    }

    /**
     * Sets the value of the vigenciaDesde property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaDesde(XMLGregorianCalendar value) {
        this.vigenciaDesde = value;
    }

    /**
     * Gets the value of the vigenciaHasta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVigenciaHasta() {
        return vigenciaHasta;
    }

    /**
     * Sets the value of the vigenciaHasta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVigenciaHasta(XMLGregorianCalendar value) {
        this.vigenciaHasta = value;
    }

}
