
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for revocarApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="revocarApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Organismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodApoderamiento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NIF_NIE__Poderdante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIF_Poderdante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anexos" type="{http://ws.funcionario.map.es/}anexo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "revocarApoderamiento", propOrder = {
    "organismo",
    "numRegistro",
    "codApoderamiento",
    "nifniePoderdante",
    "cifPoderdante",
    "anexos"
})
public class RevocarApoderamiento {

    @XmlElement(name = "Organismo")
    protected String organismo;
    @XmlElement(name = "NumRegistro")
    protected String numRegistro;
    @XmlElement(name = "CodApoderamiento")
    protected Integer codApoderamiento;
    @XmlElement(name = "NIF_NIE__Poderdante")
    protected String nifniePoderdante;
    @XmlElement(name = "CIF_Poderdante")
    protected String cifPoderdante;
    protected List<Anexo> anexos;

    /**
     * Gets the value of the organismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganismo() {
        return organismo;
    }

    /**
     * Sets the value of the organismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganismo(String value) {
        this.organismo = value;
    }

    /**
     * Gets the value of the numRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRegistro() {
        return numRegistro;
    }

    /**
     * Sets the value of the numRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRegistro(String value) {
        this.numRegistro = value;
    }

    /**
     * Gets the value of the codApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodApoderamiento() {
        return codApoderamiento;
    }

    /**
     * Sets the value of the codApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodApoderamiento(Integer value) {
        this.codApoderamiento = value;
    }

    /**
     * Gets the value of the nifniePoderdante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIFNIEPoderdante() {
        return nifniePoderdante;
    }

    /**
     * Sets the value of the nifniePoderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIFNIEPoderdante(String value) {
        this.nifniePoderdante = value;
    }

    /**
     * Gets the value of the cifPoderdante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFPoderdante() {
        return cifPoderdante;
    }

    /**
     * Sets the value of the cifPoderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFPoderdante(String value) {
        this.cifPoderdante = value;
    }

    /**
     * Gets the value of the anexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Anexo }
     * 
     * 
     */
    public List<Anexo> getAnexos() {
        if (anexos == null) {
            anexos = new ArrayList<Anexo>();
        }
        return this.anexos;
    }

}
