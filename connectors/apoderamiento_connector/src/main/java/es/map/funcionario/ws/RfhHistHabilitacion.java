
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhHistHabilitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhHistHabilitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descCausaDenega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCausaRevocacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhArchivo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhFinHabPropuesta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhFinHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhInicioHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRevocacionHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idAt" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idEstadoHabilitacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idEstadoTramitacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idFuncionario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idHabilitacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idHistFuncionario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idHistHabilitacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idSupervisor" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTramite" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUsuarioArchivo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="regHistTramite" type="{http://ws.funcionario.map.es/}regHistTramite" minOccurs="0"/>
 *         &lt;element name="rfhNanCredencialHabs" type="{http://ws.funcionario.map.es/}rfhNanCredencialHab" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhHistHabilitacion", propOrder = {
    "descCausaDenega",
    "descCausaRevocacion",
    "fhArchivo",
    "fhCreacion",
    "fhFinHabPropuesta",
    "fhFinHabilitacion",
    "fhInicioHabilitacion",
    "fhRevocacionHabilitacion",
    "idAt",
    "idEstadoHabilitacion",
    "idEstadoTramitacion",
    "idFuncionario",
    "idHabilitacion",
    "idHistFuncionario",
    "idHistHabilitacion",
    "idSupervisor",
    "idTipoSolicitud",
    "idTramite",
    "idUsuarioArchivo",
    "idUsuarioCreacion",
    "regHistTramite",
    "rfhNanCredencialHabs"
})
public class RfhHistHabilitacion {

    protected String descCausaDenega;
    protected String descCausaRevocacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhArchivo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhFinHabPropuesta;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhFinHabilitacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhInicioHabilitacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRevocacionHabilitacion;
    protected Long idAt;
    protected Integer idEstadoHabilitacion;
    protected Integer idEstadoTramitacion;
    protected Long idFuncionario;
    protected Long idHabilitacion;
    protected Long idHistFuncionario;
    protected Long idHistHabilitacion;
    protected Long idSupervisor;
    protected String idTipoSolicitud;
    protected Integer idTramite;
    protected Long idUsuarioArchivo;
    protected Long idUsuarioCreacion;
    protected RegHistTramite regHistTramite;
    @XmlElement(nillable = true)
    protected List<RfhNanCredencialHab> rfhNanCredencialHabs;

    /**
     * Gets the value of the descCausaDenega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausaDenega() {
        return descCausaDenega;
    }

    /**
     * Sets the value of the descCausaDenega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausaDenega(String value) {
        this.descCausaDenega = value;
    }

    /**
     * Gets the value of the descCausaRevocacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausaRevocacion() {
        return descCausaRevocacion;
    }

    /**
     * Sets the value of the descCausaRevocacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausaRevocacion(String value) {
        this.descCausaRevocacion = value;
    }

    /**
     * Gets the value of the fhArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhArchivo() {
        return fhArchivo;
    }

    /**
     * Sets the value of the fhArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhArchivo(XMLGregorianCalendar value) {
        this.fhArchivo = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhFinHabPropuesta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhFinHabPropuesta() {
        return fhFinHabPropuesta;
    }

    /**
     * Sets the value of the fhFinHabPropuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhFinHabPropuesta(XMLGregorianCalendar value) {
        this.fhFinHabPropuesta = value;
    }

    /**
     * Gets the value of the fhFinHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhFinHabilitacion() {
        return fhFinHabilitacion;
    }

    /**
     * Sets the value of the fhFinHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhFinHabilitacion(XMLGregorianCalendar value) {
        this.fhFinHabilitacion = value;
    }

    /**
     * Gets the value of the fhInicioHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhInicioHabilitacion() {
        return fhInicioHabilitacion;
    }

    /**
     * Sets the value of the fhInicioHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhInicioHabilitacion(XMLGregorianCalendar value) {
        this.fhInicioHabilitacion = value;
    }

    /**
     * Gets the value of the fhRevocacionHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRevocacionHabilitacion() {
        return fhRevocacionHabilitacion;
    }

    /**
     * Sets the value of the fhRevocacionHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRevocacionHabilitacion(XMLGregorianCalendar value) {
        this.fhRevocacionHabilitacion = value;
    }

    /**
     * Gets the value of the idAt property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdAt() {
        return idAt;
    }

    /**
     * Sets the value of the idAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdAt(Long value) {
        this.idAt = value;
    }

    /**
     * Gets the value of the idEstadoHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstadoHabilitacion() {
        return idEstadoHabilitacion;
    }

    /**
     * Sets the value of the idEstadoHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstadoHabilitacion(Integer value) {
        this.idEstadoHabilitacion = value;
    }

    /**
     * Gets the value of the idEstadoTramitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstadoTramitacion() {
        return idEstadoTramitacion;
    }

    /**
     * Sets the value of the idEstadoTramitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstadoTramitacion(Integer value) {
        this.idEstadoTramitacion = value;
    }

    /**
     * Gets the value of the idFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdFuncionario() {
        return idFuncionario;
    }

    /**
     * Sets the value of the idFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdFuncionario(Long value) {
        this.idFuncionario = value;
    }

    /**
     * Gets the value of the idHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHabilitacion() {
        return idHabilitacion;
    }

    /**
     * Sets the value of the idHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHabilitacion(Long value) {
        this.idHabilitacion = value;
    }

    /**
     * Gets the value of the idHistFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistFuncionario() {
        return idHistFuncionario;
    }

    /**
     * Sets the value of the idHistFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistFuncionario(Long value) {
        this.idHistFuncionario = value;
    }

    /**
     * Gets the value of the idHistHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistHabilitacion() {
        return idHistHabilitacion;
    }

    /**
     * Sets the value of the idHistHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistHabilitacion(Long value) {
        this.idHistHabilitacion = value;
    }

    /**
     * Gets the value of the idSupervisor property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdSupervisor() {
        return idSupervisor;
    }

    /**
     * Sets the value of the idSupervisor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdSupervisor(Long value) {
        this.idSupervisor = value;
    }

    /**
     * Gets the value of the idTipoSolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoSolicitud() {
        return idTipoSolicitud;
    }

    /**
     * Sets the value of the idTipoSolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoSolicitud(String value) {
        this.idTipoSolicitud = value;
    }

    /**
     * Gets the value of the idTramite property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTramite() {
        return idTramite;
    }

    /**
     * Sets the value of the idTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTramite(Integer value) {
        this.idTramite = value;
    }

    /**
     * Gets the value of the idUsuarioArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioArchivo() {
        return idUsuarioArchivo;
    }

    /**
     * Sets the value of the idUsuarioArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioArchivo(Long value) {
        this.idUsuarioArchivo = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regHistTramite property.
     * 
     * @return
     *     possible object is
     *     {@link RegHistTramite }
     *     
     */
    public RegHistTramite getRegHistTramite() {
        return regHistTramite;
    }

    /**
     * Sets the value of the regHistTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegHistTramite }
     *     
     */
    public void setRegHistTramite(RegHistTramite value) {
        this.regHistTramite = value;
    }

    /**
     * Gets the value of the rfhNanCredencialHabs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhNanCredencialHabs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhNanCredencialHabs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhNanCredencialHab }
     * 
     * 
     */
    public List<RfhNanCredencialHab> getRfhNanCredencialHabs() {
        if (rfhNanCredencialHabs == null) {
            rfhNanCredencialHabs = new ArrayList<RfhNanCredencialHab>();
        }
        return this.rfhNanCredencialHabs;
    }

}
