
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for revocarApoderamientoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="revocarApoderamientoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.funcionario.map.es/}revocarApoderamientoWSResponseSalida" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "revocarApoderamientoResponse", propOrder = {
    "_return"
})
public class RevocarApoderamientoResponse {

    @XmlElement(name = "return", namespace = "http://ws.funcionario.map.es/")
    protected RevocarApoderamientoWSResponseSalida _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link RevocarApoderamientoWSResponseSalida }
     *     
     */
    public RevocarApoderamientoWSResponseSalida getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link RevocarApoderamientoWSResponseSalida }
     *     
     */
    public void setReturn(RevocarApoderamientoWSResponseSalida value) {
        this._return = value;
    }

}
