
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tablaResultado complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tablaResultado">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="campoOrdenacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desmarcarTodos" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="direccionOrdenacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idsMostradosPaginaActual" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idsSeleccionadosOtrasPaginas" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idsSeleccionadosTodos" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="marcarTodos" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="numPaginaActual" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numPaginasTotales" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numRegistrosMostrar" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="seleccionaTodos" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tablaResultado", propOrder = {
    "campoOrdenacion",
    "desmarcarTodos",
    "direccionOrdenacion",
    "idsMostradosPaginaActual",
    "idsSeleccionadosOtrasPaginas",
    "idsSeleccionadosTodos",
    "marcarTodos",
    "numPaginaActual",
    "numPaginasTotales",
    "numRegistrosMostrar",
    "seleccionaTodos"
})
public class TablaResultado {

    protected String campoOrdenacion;
    protected boolean desmarcarTodos;
    protected String direccionOrdenacion;
    @XmlElement(nillable = true)
    protected List<String> idsMostradosPaginaActual;
    @XmlElement(nillable = true)
    protected List<String> idsSeleccionadosOtrasPaginas;
    @XmlElement(nillable = true)
    protected List<String> idsSeleccionadosTodos;
    protected boolean marcarTodos;
    protected int numPaginaActual;
    protected int numPaginasTotales;
    protected int numRegistrosMostrar;
    protected boolean seleccionaTodos;

    /**
     * Gets the value of the campoOrdenacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampoOrdenacion() {
        return campoOrdenacion;
    }

    /**
     * Sets the value of the campoOrdenacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampoOrdenacion(String value) {
        this.campoOrdenacion = value;
    }

    /**
     * Gets the value of the desmarcarTodos property.
     * 
     */
    public boolean isDesmarcarTodos() {
        return desmarcarTodos;
    }

    /**
     * Sets the value of the desmarcarTodos property.
     * 
     */
    public void setDesmarcarTodos(boolean value) {
        this.desmarcarTodos = value;
    }

    /**
     * Gets the value of the direccionOrdenacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDireccionOrdenacion() {
        return direccionOrdenacion;
    }

    /**
     * Sets the value of the direccionOrdenacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDireccionOrdenacion(String value) {
        this.direccionOrdenacion = value;
    }

    /**
     * Gets the value of the idsMostradosPaginaActual property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsMostradosPaginaActual property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsMostradosPaginaActual().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsMostradosPaginaActual() {
        if (idsMostradosPaginaActual == null) {
            idsMostradosPaginaActual = new ArrayList<String>();
        }
        return this.idsMostradosPaginaActual;
    }

    /**
     * Gets the value of the idsSeleccionadosOtrasPaginas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsSeleccionadosOtrasPaginas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsSeleccionadosOtrasPaginas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsSeleccionadosOtrasPaginas() {
        if (idsSeleccionadosOtrasPaginas == null) {
            idsSeleccionadosOtrasPaginas = new ArrayList<String>();
        }
        return this.idsSeleccionadosOtrasPaginas;
    }

    /**
     * Gets the value of the idsSeleccionadosTodos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsSeleccionadosTodos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsSeleccionadosTodos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsSeleccionadosTodos() {
        if (idsSeleccionadosTodos == null) {
            idsSeleccionadosTodos = new ArrayList<String>();
        }
        return this.idsSeleccionadosTodos;
    }

    /**
     * Gets the value of the marcarTodos property.
     * 
     */
    public boolean isMarcarTodos() {
        return marcarTodos;
    }

    /**
     * Sets the value of the marcarTodos property.
     * 
     */
    public void setMarcarTodos(boolean value) {
        this.marcarTodos = value;
    }

    /**
     * Gets the value of the numPaginaActual property.
     * 
     */
    public int getNumPaginaActual() {
        return numPaginaActual;
    }

    /**
     * Sets the value of the numPaginaActual property.
     * 
     */
    public void setNumPaginaActual(int value) {
        this.numPaginaActual = value;
    }

    /**
     * Gets the value of the numPaginasTotales property.
     * 
     */
    public int getNumPaginasTotales() {
        return numPaginasTotales;
    }

    /**
     * Sets the value of the numPaginasTotales property.
     * 
     */
    public void setNumPaginasTotales(int value) {
        this.numPaginasTotales = value;
    }

    /**
     * Gets the value of the numRegistrosMostrar property.
     * 
     */
    public int getNumRegistrosMostrar() {
        return numRegistrosMostrar;
    }

    /**
     * Sets the value of the numRegistrosMostrar property.
     * 
     */
    public void setNumRegistrosMostrar(int value) {
        this.numRegistrosMostrar = value;
    }

    /**
     * Gets the value of the seleccionaTodos property.
     * 
     */
    public boolean isSeleccionaTodos() {
        return seleccionaTodos;
    }

    /**
     * Sets the value of the seleccionaTodos property.
     * 
     */
    public void setSeleccionaTodos(boolean value) {
        this.seleccionaTodos = value;
    }

}
