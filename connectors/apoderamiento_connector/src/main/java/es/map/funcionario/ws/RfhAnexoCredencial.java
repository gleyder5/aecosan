
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhAnexoCredencial complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhAnexoCredencial">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFirmaElectronica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNombreAlfresco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRegistroRec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhDescarga" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRegistroRec" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idAnexoCredencial" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idDescarga" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="rfhCatTipoDocumento" type="{http://ws.funcionario.map.es/}rfhCatTipoDocumento" minOccurs="0"/>
 *         &lt;element name="rfhCredencial" type="{http://ws.funcionario.map.es/}rfhCredencial" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhAnexoCredencial", propOrder = {
    "codCve",
    "codFirmaElectronica",
    "codNombreAlfresco",
    "codRegistroRec",
    "fhDescarga",
    "fhRegistroRec",
    "idAnexoCredencial",
    "idDescarga",
    "rfhCatTipoDocumento",
    "rfhCredencial"
})
public class RfhAnexoCredencial {

    protected String codCve;
    protected String codFirmaElectronica;
    protected String codNombreAlfresco;
    protected String codRegistroRec;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhDescarga;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRegistroRec;
    protected Integer idAnexoCredencial;
    protected Boolean idDescarga;
    protected RfhCatTipoDocumento rfhCatTipoDocumento;
    protected RfhCredencial rfhCredencial;

    /**
     * Gets the value of the codCve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCve() {
        return codCve;
    }

    /**
     * Sets the value of the codCve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCve(String value) {
        this.codCve = value;
    }

    /**
     * Gets the value of the codFirmaElectronica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFirmaElectronica() {
        return codFirmaElectronica;
    }

    /**
     * Sets the value of the codFirmaElectronica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFirmaElectronica(String value) {
        this.codFirmaElectronica = value;
    }

    /**
     * Gets the value of the codNombreAlfresco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNombreAlfresco() {
        return codNombreAlfresco;
    }

    /**
     * Sets the value of the codNombreAlfresco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNombreAlfresco(String value) {
        this.codNombreAlfresco = value;
    }

    /**
     * Gets the value of the codRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegistroRec() {
        return codRegistroRec;
    }

    /**
     * Sets the value of the codRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegistroRec(String value) {
        this.codRegistroRec = value;
    }

    /**
     * Gets the value of the fhDescarga property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhDescarga() {
        return fhDescarga;
    }

    /**
     * Sets the value of the fhDescarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhDescarga(XMLGregorianCalendar value) {
        this.fhDescarga = value;
    }

    /**
     * Gets the value of the fhRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRegistroRec() {
        return fhRegistroRec;
    }

    /**
     * Sets the value of the fhRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRegistroRec(XMLGregorianCalendar value) {
        this.fhRegistroRec = value;
    }

    /**
     * Gets the value of the idAnexoCredencial property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdAnexoCredencial() {
        return idAnexoCredencial;
    }

    /**
     * Sets the value of the idAnexoCredencial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdAnexoCredencial(Integer value) {
        this.idAnexoCredencial = value;
    }

    /**
     * Gets the value of the idDescarga property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdDescarga() {
        return idDescarga;
    }

    /**
     * Sets the value of the idDescarga property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdDescarga(Boolean value) {
        this.idDescarga = value;
    }

    /**
     * Gets the value of the rfhCatTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCatTipoDocumento }
     *     
     */
    public RfhCatTipoDocumento getRfhCatTipoDocumento() {
        return rfhCatTipoDocumento;
    }

    /**
     * Sets the value of the rfhCatTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCatTipoDocumento }
     *     
     */
    public void setRfhCatTipoDocumento(RfhCatTipoDocumento value) {
        this.rfhCatTipoDocumento = value;
    }

    /**
     * Gets the value of the rfhCredencial property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCredencial }
     *     
     */
    public RfhCredencial getRfhCredencial() {
        return rfhCredencial;
    }

    /**
     * Sets the value of the rfhCredencial property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCredencial }
     *     
     */
    public void setRfhCredencial(RfhCredencial value) {
        this.rfhCredencial = value;
    }

}
