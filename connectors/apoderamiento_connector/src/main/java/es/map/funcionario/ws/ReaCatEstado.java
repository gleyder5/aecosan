
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reaCatEstado complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaCatEstado">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denominacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEstado" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reaApoderamientos" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaHistApoderamientos" type="{http://ws.funcionario.map.es/}reaHistApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaCatEstado", propOrder = {
    "denominacion",
    "descripcion",
    "idEstado",
    "reaApoderamientos",
    "reaHistApoderamientos"
})
public class ReaCatEstado {

    protected String denominacion;
    protected String descripcion;
    protected Integer idEstado;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientos;
    @XmlElement(nillable = true)
    protected List<ReaHistApoderamiento> reaHistApoderamientos;

    /**
     * Gets the value of the denominacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominacion() {
        return denominacion;
    }

    /**
     * Sets the value of the denominacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominacion(String value) {
        this.denominacion = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the idEstado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * Sets the value of the idEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstado(Integer value) {
        this.idEstado = value;
    }

    /**
     * Gets the value of the reaApoderamientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientos() {
        if (reaApoderamientos == null) {
            reaApoderamientos = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientos;
    }

    /**
     * Gets the value of the reaHistApoderamientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaHistApoderamientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaHistApoderamientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaHistApoderamiento }
     * 
     * 
     */
    public List<ReaHistApoderamiento> getReaHistApoderamientos() {
        if (reaHistApoderamientos == null) {
            reaHistApoderamientos = new ArrayList<ReaHistApoderamiento>();
        }
        return this.reaHistApoderamientos;
    }

}
