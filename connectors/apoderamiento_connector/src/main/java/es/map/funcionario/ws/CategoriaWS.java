
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for categoriaWS complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="categoriaWS">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCategoria" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcionCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaTramites" type="{http://ws.funcionario.map.es/}tramiteWS" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nombreCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categoriaWS", propOrder = {
    "codCategoria",
    "codOrganismo",
    "descripcionCategoria",
    "listaTramites",
    "nombreCategoria"
})
public class CategoriaWS {

    protected int codCategoria;
    protected String codOrganismo;
    protected String descripcionCategoria;
    @XmlElement(nillable = true)
    protected List<TramiteWS> listaTramites;
    protected String nombreCategoria;

    /**
     * Gets the value of the codCategoria property.
     * 
     */
    public int getCodCategoria() {
        return codCategoria;
    }

    /**
     * Sets the value of the codCategoria property.
     * 
     */
    public void setCodCategoria(int value) {
        this.codCategoria = value;
    }

    /**
     * Gets the value of the codOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrganismo() {
        return codOrganismo;
    }

    /**
     * Sets the value of the codOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrganismo(String value) {
        this.codOrganismo = value;
    }

    /**
     * Gets the value of the descripcionCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    /**
     * Sets the value of the descripcionCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionCategoria(String value) {
        this.descripcionCategoria = value;
    }

    /**
     * Gets the value of the listaTramites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaTramites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaTramites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TramiteWS }
     * 
     * 
     */
    public List<TramiteWS> getListaTramites() {
        if (listaTramites == null) {
            listaTramites = new ArrayList<TramiteWS>();
        }
        return this.listaTramites;
    }

    /**
     * Gets the value of the nombreCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCategoria() {
        return nombreCategoria;
    }

    /**
     * Sets the value of the nombreCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCategoria(String value) {
        this.nombreCategoria = value;
    }

}
