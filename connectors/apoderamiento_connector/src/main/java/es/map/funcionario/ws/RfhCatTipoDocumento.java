
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rfhCatTipoDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhCatTipoDocumento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rfhAnexoCredencials" type="{http://ws.funcionario.map.es/}rfhAnexoCredencial" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhCatTipoDocumento", propOrder = {
    "descTipoDocumento",
    "idTipoDocumento",
    "rfhAnexoCredencials"
})
public class RfhCatTipoDocumento {

    protected String descTipoDocumento;
    protected Integer idTipoDocumento;
    @XmlElement(nillable = true)
    protected List<RfhAnexoCredencial> rfhAnexoCredencials;

    /**
     * Gets the value of the descTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoDocumento() {
        return descTipoDocumento;
    }

    /**
     * Sets the value of the descTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoDocumento(String value) {
        this.descTipoDocumento = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoDocumento(Integer value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the rfhAnexoCredencials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhAnexoCredencials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhAnexoCredencials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhAnexoCredencial }
     * 
     * 
     */
    public List<RfhAnexoCredencial> getRfhAnexoCredencials() {
        if (rfhAnexoCredencials == null) {
            rfhAnexoCredencials = new ArrayList<RfhAnexoCredencial>();
        }
        return this.rfhAnexoCredencials;
    }

}
