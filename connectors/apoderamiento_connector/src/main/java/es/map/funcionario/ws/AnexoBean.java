
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anexoBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="anexoBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.funcionario.map.es/}busquedaGenericaBean">
 *       &lt;sequence>
 *         &lt;element name="activo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="anexoNuevo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="apoderamiento" type="{http://ws.funcionario.map.es/}reaApoderamiento" minOccurs="0"/>
 *         &lt;element name="codEstadoElaboracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codIndentificador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codOrgano" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codVersionNti" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contenidoAnexo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="contenidoDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="contenidoFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contenidoFirmaDocRevocacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cve" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entorno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esJustificante" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="estadoHistoricoDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadoRepresentacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="extracto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaCaptura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaHistoricoDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhAlta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="historicoApods" type="{http://ws.funcionario.map.es/}historicoApoderamientoBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idAnexo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idApoderamiento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idFormato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRepresentante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipoDocumentoEni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nombreAlfresco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numTotal" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="origen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resenha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tamano" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumentoEni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tlHuellaDigital" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="xmlFirmadoBase64" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "anexoBean", propOrder = {
    "activo",
    "anexoNuevo",
    "apoderamiento",
    "codEstadoElaboracion",
    "codIndentificador",
    "codOrgano",
    "codTipoFirma",
    "codVersionNti",
    "contenidoAnexo",
    "contenidoDocumento",
    "contenidoFirma",
    "contenidoFirmaDocRevocacion",
    "cve",
    "descripcion",
    "entorno",
    "esJustificante",
    "estadoHistoricoDetalle",
    "estadoRepresentacion",
    "extension",
    "extracto",
    "fechaCaptura",
    "fechaCreacion",
    "fechaHistoricoDetalle",
    "fechaModificacion",
    "fhAlta",
    "historicoApods",
    "idAnexo",
    "idApoderamiento",
    "idFormato",
    "idOrigen",
    "idRepresentante",
    "idTipoDocumento",
    "idTipoDocumentoEni",
    "idUsuarioCreacion",
    "idUsuarioModificacion",
    "nombre",
    "nombreAlfresco",
    "numTotal",
    "origen",
    "resenha",
    "tamano",
    "tipoDocumento",
    "tipoDocumentoEni",
    "tlHuellaDigital",
    "xmlFirmadoBase64"
})
public class AnexoBean
    extends BusquedaGenericaBean
{

    protected boolean activo;
    protected Boolean anexoNuevo;
    protected ReaApoderamiento apoderamiento;
    protected String codEstadoElaboracion;
    protected String codIndentificador;
    protected String codOrgano;
    protected String codTipoFirma;
    protected String codVersionNti;
    protected byte[] contenidoAnexo;
    protected byte[] contenidoDocumento;
    protected String contenidoFirma;
    protected String contenidoFirmaDocRevocacion;
    protected String cve;
    protected String descripcion;
    protected String entorno;
    protected Boolean esJustificante;
    protected String estadoHistoricoDetalle;
    protected String estadoRepresentacion;
    protected String extension;
    protected String extracto;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaCaptura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaCreacion;
    protected String fechaHistoricoDetalle;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhAlta;
    @XmlElement(nillable = true)
    protected List<HistoricoApoderamientoBean> historicoApods;
    protected Long idAnexo;
    protected Long idApoderamiento;
    protected Integer idFormato;
    protected String idOrigen;
    protected String idRepresentante;
    protected String idTipoDocumento;
    protected String idTipoDocumentoEni;
    protected Long idUsuarioCreacion;
    protected Long idUsuarioModificacion;
    protected String nombre;
    protected String nombreAlfresco;
    protected int numTotal;
    protected String origen;
    protected String resenha;
    protected int tamano;
    protected String tipoDocumento;
    protected String tipoDocumentoEni;
    protected String tlHuellaDigital;
    protected String xmlFirmadoBase64;

    /**
     * Gets the value of the activo property.
     * 
     */
    public boolean isActivo() {
        return activo;
    }

    /**
     * Sets the value of the activo property.
     * 
     */
    public void setActivo(boolean value) {
        this.activo = value;
    }

    /**
     * Gets the value of the anexoNuevo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAnexoNuevo() {
        return anexoNuevo;
    }

    /**
     * Sets the value of the anexoNuevo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAnexoNuevo(Boolean value) {
        this.anexoNuevo = value;
    }

    /**
     * Gets the value of the apoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link ReaApoderamiento }
     *     
     */
    public ReaApoderamiento getApoderamiento() {
        return apoderamiento;
    }

    /**
     * Sets the value of the apoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReaApoderamiento }
     *     
     */
    public void setApoderamiento(ReaApoderamiento value) {
        this.apoderamiento = value;
    }

    /**
     * Gets the value of the codEstadoElaboracion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoElaboracion() {
        return codEstadoElaboracion;
    }

    /**
     * Sets the value of the codEstadoElaboracion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoElaboracion(String value) {
        this.codEstadoElaboracion = value;
    }

    /**
     * Gets the value of the codIndentificador property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodIndentificador() {
        return codIndentificador;
    }

    /**
     * Sets the value of the codIndentificador property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodIndentificador(String value) {
        this.codIndentificador = value;
    }

    /**
     * Gets the value of the codOrgano property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrgano() {
        return codOrgano;
    }

    /**
     * Sets the value of the codOrgano property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrgano(String value) {
        this.codOrgano = value;
    }

    /**
     * Gets the value of the codTipoFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoFirma() {
        return codTipoFirma;
    }

    /**
     * Sets the value of the codTipoFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoFirma(String value) {
        this.codTipoFirma = value;
    }

    /**
     * Gets the value of the codVersionNti property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodVersionNti() {
        return codVersionNti;
    }

    /**
     * Sets the value of the codVersionNti property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodVersionNti(String value) {
        this.codVersionNti = value;
    }

    /**
     * Gets the value of the contenidoAnexo property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContenidoAnexo() {
        return contenidoAnexo;
    }

    /**
     * Sets the value of the contenidoAnexo property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContenidoAnexo(byte[] value) {
        this.contenidoAnexo = value;
    }

    /**
     * Gets the value of the contenidoDocumento property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContenidoDocumento() {
        return contenidoDocumento;
    }

    /**
     * Sets the value of the contenidoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContenidoDocumento(byte[] value) {
        this.contenidoDocumento = value;
    }

    /**
     * Gets the value of the contenidoFirma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContenidoFirma() {
        return contenidoFirma;
    }

    /**
     * Sets the value of the contenidoFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContenidoFirma(String value) {
        this.contenidoFirma = value;
    }

    /**
     * Gets the value of the contenidoFirmaDocRevocacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContenidoFirmaDocRevocacion() {
        return contenidoFirmaDocRevocacion;
    }

    /**
     * Sets the value of the contenidoFirmaDocRevocacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContenidoFirmaDocRevocacion(String value) {
        this.contenidoFirmaDocRevocacion = value;
    }

    /**
     * Gets the value of the cve property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCve() {
        return cve;
    }

    /**
     * Sets the value of the cve property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCve(String value) {
        this.cve = value;
    }

    /**
     * Gets the value of the descripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Sets the value of the descripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcion(String value) {
        this.descripcion = value;
    }

    /**
     * Gets the value of the entorno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntorno() {
        return entorno;
    }

    /**
     * Sets the value of the entorno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntorno(String value) {
        this.entorno = value;
    }

    /**
     * Gets the value of the esJustificante property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEsJustificante() {
        return esJustificante;
    }

    /**
     * Sets the value of the esJustificante property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEsJustificante(Boolean value) {
        this.esJustificante = value;
    }

    /**
     * Gets the value of the estadoHistoricoDetalle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoHistoricoDetalle() {
        return estadoHistoricoDetalle;
    }

    /**
     * Sets the value of the estadoHistoricoDetalle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoHistoricoDetalle(String value) {
        this.estadoHistoricoDetalle = value;
    }

    /**
     * Gets the value of the estadoRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoRepresentacion() {
        return estadoRepresentacion;
    }

    /**
     * Sets the value of the estadoRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoRepresentacion(String value) {
        this.estadoRepresentacion = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Gets the value of the extracto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtracto() {
        return extracto;
    }

    /**
     * Sets the value of the extracto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtracto(String value) {
        this.extracto = value;
    }

    /**
     * Gets the value of the fechaCaptura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCaptura() {
        return fechaCaptura;
    }

    /**
     * Sets the value of the fechaCaptura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCaptura(XMLGregorianCalendar value) {
        this.fechaCaptura = value;
    }

    /**
     * Gets the value of the fechaCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Sets the value of the fechaCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaCreacion(XMLGregorianCalendar value) {
        this.fechaCreacion = value;
    }

    /**
     * Gets the value of the fechaHistoricoDetalle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaHistoricoDetalle() {
        return fechaHistoricoDetalle;
    }

    /**
     * Sets the value of the fechaHistoricoDetalle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaHistoricoDetalle(String value) {
        this.fechaHistoricoDetalle = value;
    }

    /**
     * Gets the value of the fechaModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaModificacion() {
        return fechaModificacion;
    }

    /**
     * Sets the value of the fechaModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaModificacion(XMLGregorianCalendar value) {
        this.fechaModificacion = value;
    }

    /**
     * Gets the value of the fhAlta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhAlta() {
        return fhAlta;
    }

    /**
     * Sets the value of the fhAlta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhAlta(XMLGregorianCalendar value) {
        this.fhAlta = value;
    }

    /**
     * Gets the value of the historicoApods property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historicoApods property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoricoApods().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoricoApoderamientoBean }
     * 
     * 
     */
    public List<HistoricoApoderamientoBean> getHistoricoApods() {
        if (historicoApods == null) {
            historicoApods = new ArrayList<HistoricoApoderamientoBean>();
        }
        return this.historicoApods;
    }

    /**
     * Gets the value of the idAnexo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdAnexo() {
        return idAnexo;
    }

    /**
     * Sets the value of the idAnexo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdAnexo(Long value) {
        this.idAnexo = value;
    }

    /**
     * Gets the value of the idApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdApoderamiento() {
        return idApoderamiento;
    }

    /**
     * Sets the value of the idApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdApoderamiento(Long value) {
        this.idApoderamiento = value;
    }

    /**
     * Gets the value of the idFormato property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdFormato() {
        return idFormato;
    }

    /**
     * Sets the value of the idFormato property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdFormato(Integer value) {
        this.idFormato = value;
    }

    /**
     * Gets the value of the idOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOrigen() {
        return idOrigen;
    }

    /**
     * Sets the value of the idOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOrigen(String value) {
        this.idOrigen = value;
    }

    /**
     * Gets the value of the idRepresentante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRepresentante() {
        return idRepresentante;
    }

    /**
     * Sets the value of the idRepresentante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRepresentante(String value) {
        this.idRepresentante = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoDocumento(String value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the idTipoDocumentoEni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoDocumentoEni() {
        return idTipoDocumentoEni;
    }

    /**
     * Sets the value of the idTipoDocumentoEni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoDocumentoEni(String value) {
        this.idTipoDocumentoEni = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the idUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    /**
     * Sets the value of the idUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioModificacion(Long value) {
        this.idUsuarioModificacion = value;
    }

    /**
     * Gets the value of the nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets the value of the nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Gets the value of the nombreAlfresco property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreAlfresco() {
        return nombreAlfresco;
    }

    /**
     * Sets the value of the nombreAlfresco property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreAlfresco(String value) {
        this.nombreAlfresco = value;
    }

    /**
     * Gets the value of the numTotal property.
     * 
     */
    public int getNumTotal() {
        return numTotal;
    }

    /**
     * Sets the value of the numTotal property.
     * 
     */
    public void setNumTotal(int value) {
        this.numTotal = value;
    }

    /**
     * Gets the value of the origen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrigen() {
        return origen;
    }

    /**
     * Sets the value of the origen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrigen(String value) {
        this.origen = value;
    }

    /**
     * Gets the value of the resenha property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResenha() {
        return resenha;
    }

    /**
     * Sets the value of the resenha property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResenha(String value) {
        this.resenha = value;
    }

    /**
     * Gets the value of the tamano property.
     * 
     */
    public int getTamano() {
        return tamano;
    }

    /**
     * Sets the value of the tamano property.
     * 
     */
    public void setTamano(int value) {
        this.tamano = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the tipoDocumentoEni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumentoEni() {
        return tipoDocumentoEni;
    }

    /**
     * Sets the value of the tipoDocumentoEni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumentoEni(String value) {
        this.tipoDocumentoEni = value;
    }

    /**
     * Gets the value of the tlHuellaDigital property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTlHuellaDigital() {
        return tlHuellaDigital;
    }

    /**
     * Sets the value of the tlHuellaDigital property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTlHuellaDigital(String value) {
        this.tlHuellaDigital = value;
    }

    /**
     * Gets the value of the xmlFirmadoBase64 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXmlFirmadoBase64() {
        return xmlFirmadoBase64;
    }

    /**
     * Sets the value of the xmlFirmadoBase64 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXmlFirmadoBase64(String value) {
        this.xmlFirmadoBase64 = value;
    }

}
