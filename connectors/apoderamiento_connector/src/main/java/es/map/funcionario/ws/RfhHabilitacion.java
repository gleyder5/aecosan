
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhHabilitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhHabilitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descCausaDenega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCausaRevocacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhFinHabPropuesta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhFinHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhInicioHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRevocacionHabilitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idHabilitacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="regHistTramite" type="{http://ws.funcionario.map.es/}regHistTramite" minOccurs="0"/>
 *         &lt;element name="regNotificacions" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regTramite" type="{http://ws.funcionario.map.es/}regTramite" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdAt" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdSupervisor" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhCatEstadoHabilitacion" type="{http://ws.funcionario.map.es/}rfhCatEstadoHabilitacion" minOccurs="0"/>
 *         &lt;element name="rfhCatEstadoTramitacion" type="{http://ws.funcionario.map.es/}rfhCatEstadoTramitacion" minOccurs="0"/>
 *         &lt;element name="rfhFuncionario" type="{http://ws.funcionario.map.es/}rfhFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhHistFuncionario" type="{http://ws.funcionario.map.es/}rfhHistFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhNanCredencialHabs" type="{http://ws.funcionario.map.es/}rfhNanCredencialHab" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhHabilitacion", propOrder = {
    "descCausaDenega",
    "descCausaRevocacion",
    "fhCreacion",
    "fhFinHabPropuesta",
    "fhFinHabilitacion",
    "fhInicioHabilitacion",
    "fhModificacion",
    "fhRevocacionHabilitacion",
    "idHabilitacion",
    "idTipoSolicitud",
    "regHistTramite",
    "regNotificacions",
    "regTramite",
    "regUsuarioByIdAt",
    "regUsuarioByIdSupervisor",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhCatEstadoHabilitacion",
    "rfhCatEstadoTramitacion",
    "rfhFuncionario",
    "rfhHistFuncionario",
    "rfhNanCredencialHabs"
})
public class RfhHabilitacion {

    protected String descCausaDenega;
    protected String descCausaRevocacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhFinHabPropuesta;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhFinHabilitacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhInicioHabilitacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRevocacionHabilitacion;
    protected Long idHabilitacion;
    protected String idTipoSolicitud;
    protected RegHistTramite regHistTramite;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacions;
    protected RegTramite regTramite;
    protected RegUsuario regUsuarioByIdAt;
    protected RegUsuario regUsuarioByIdSupervisor;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    protected RfhCatEstadoHabilitacion rfhCatEstadoHabilitacion;
    protected RfhCatEstadoTramitacion rfhCatEstadoTramitacion;
    protected RfhFuncionario rfhFuncionario;
    protected RfhHistFuncionario rfhHistFuncionario;
    @XmlElement(nillable = true)
    protected List<RfhNanCredencialHab> rfhNanCredencialHabs;

    /**
     * Gets the value of the descCausaDenega property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausaDenega() {
        return descCausaDenega;
    }

    /**
     * Sets the value of the descCausaDenega property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausaDenega(String value) {
        this.descCausaDenega = value;
    }

    /**
     * Gets the value of the descCausaRevocacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausaRevocacion() {
        return descCausaRevocacion;
    }

    /**
     * Sets the value of the descCausaRevocacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausaRevocacion(String value) {
        this.descCausaRevocacion = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhFinHabPropuesta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhFinHabPropuesta() {
        return fhFinHabPropuesta;
    }

    /**
     * Sets the value of the fhFinHabPropuesta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhFinHabPropuesta(XMLGregorianCalendar value) {
        this.fhFinHabPropuesta = value;
    }

    /**
     * Gets the value of the fhFinHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhFinHabilitacion() {
        return fhFinHabilitacion;
    }

    /**
     * Sets the value of the fhFinHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhFinHabilitacion(XMLGregorianCalendar value) {
        this.fhFinHabilitacion = value;
    }

    /**
     * Gets the value of the fhInicioHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhInicioHabilitacion() {
        return fhInicioHabilitacion;
    }

    /**
     * Sets the value of the fhInicioHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhInicioHabilitacion(XMLGregorianCalendar value) {
        this.fhInicioHabilitacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the fhRevocacionHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRevocacionHabilitacion() {
        return fhRevocacionHabilitacion;
    }

    /**
     * Sets the value of the fhRevocacionHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRevocacionHabilitacion(XMLGregorianCalendar value) {
        this.fhRevocacionHabilitacion = value;
    }

    /**
     * Gets the value of the idHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHabilitacion() {
        return idHabilitacion;
    }

    /**
     * Sets the value of the idHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHabilitacion(Long value) {
        this.idHabilitacion = value;
    }

    /**
     * Gets the value of the idTipoSolicitud property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoSolicitud() {
        return idTipoSolicitud;
    }

    /**
     * Sets the value of the idTipoSolicitud property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoSolicitud(String value) {
        this.idTipoSolicitud = value;
    }

    /**
     * Gets the value of the regHistTramite property.
     * 
     * @return
     *     possible object is
     *     {@link RegHistTramite }
     *     
     */
    public RegHistTramite getRegHistTramite() {
        return regHistTramite;
    }

    /**
     * Sets the value of the regHistTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegHistTramite }
     *     
     */
    public void setRegHistTramite(RegHistTramite value) {
        this.regHistTramite = value;
    }

    /**
     * Gets the value of the regNotificacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacions() {
        if (regNotificacions == null) {
            regNotificacions = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacions;
    }

    /**
     * Gets the value of the regTramite property.
     * 
     * @return
     *     possible object is
     *     {@link RegTramite }
     *     
     */
    public RegTramite getRegTramite() {
        return regTramite;
    }

    /**
     * Sets the value of the regTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTramite }
     *     
     */
    public void setRegTramite(RegTramite value) {
        this.regTramite = value;
    }

    /**
     * Gets the value of the regUsuarioByIdAt property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdAt() {
        return regUsuarioByIdAt;
    }

    /**
     * Sets the value of the regUsuarioByIdAt property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdAt(RegUsuario value) {
        this.regUsuarioByIdAt = value;
    }

    /**
     * Gets the value of the regUsuarioByIdSupervisor property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdSupervisor() {
        return regUsuarioByIdSupervisor;
    }

    /**
     * Sets the value of the regUsuarioByIdSupervisor property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdSupervisor(RegUsuario value) {
        this.regUsuarioByIdSupervisor = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhCatEstadoHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCatEstadoHabilitacion }
     *     
     */
    public RfhCatEstadoHabilitacion getRfhCatEstadoHabilitacion() {
        return rfhCatEstadoHabilitacion;
    }

    /**
     * Sets the value of the rfhCatEstadoHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCatEstadoHabilitacion }
     *     
     */
    public void setRfhCatEstadoHabilitacion(RfhCatEstadoHabilitacion value) {
        this.rfhCatEstadoHabilitacion = value;
    }

    /**
     * Gets the value of the rfhCatEstadoTramitacion property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCatEstadoTramitacion }
     *     
     */
    public RfhCatEstadoTramitacion getRfhCatEstadoTramitacion() {
        return rfhCatEstadoTramitacion;
    }

    /**
     * Sets the value of the rfhCatEstadoTramitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCatEstadoTramitacion }
     *     
     */
    public void setRfhCatEstadoTramitacion(RfhCatEstadoTramitacion value) {
        this.rfhCatEstadoTramitacion = value;
    }

    /**
     * Gets the value of the rfhFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhFuncionario }
     *     
     */
    public RfhFuncionario getRfhFuncionario() {
        return rfhFuncionario;
    }

    /**
     * Sets the value of the rfhFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhFuncionario }
     *     
     */
    public void setRfhFuncionario(RfhFuncionario value) {
        this.rfhFuncionario = value;
    }

    /**
     * Gets the value of the rfhHistFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhHistFuncionario }
     *     
     */
    public RfhHistFuncionario getRfhHistFuncionario() {
        return rfhHistFuncionario;
    }

    /**
     * Sets the value of the rfhHistFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhHistFuncionario }
     *     
     */
    public void setRfhHistFuncionario(RfhHistFuncionario value) {
        this.rfhHistFuncionario = value;
    }

    /**
     * Gets the value of the rfhNanCredencialHabs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhNanCredencialHabs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhNanCredencialHabs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhNanCredencialHab }
     * 
     * 
     */
    public List<RfhNanCredencialHab> getRfhNanCredencialHabs() {
        if (rfhNanCredencialHabs == null) {
            rfhNanCredencialHabs = new ArrayList<RfhNanCredencialHab>();
        }
        return this.rfhNanCredencialHabs;
    }

}
