
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for denegarApoderamientoWSResponseSalida complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="denegarApoderamientoWSResponseSalida">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="error" type="{http://ws.funcionario.map.es/}errorWS" minOccurs="0"/>
 *         &lt;element name="valido" type="{http://ws.funcionario.map.es/}validoWS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "denegarApoderamientoWSResponseSalida", propOrder = {
    "error",
    "valido"
})
public class DenegarApoderamientoWSResponseSalida {

    protected ErrorWS error;
    protected ValidoWS valido;

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorWS }
     *     
     */
    public ErrorWS getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorWS }
     *     
     */
    public void setError(ErrorWS value) {
        this.error = value;
    }

    /**
     * Gets the value of the valido property.
     * 
     * @return
     *     possible object is
     *     {@link ValidoWS }
     *     
     */
    public ValidoWS getValido() {
        return valido;
    }

    /**
     * Sets the value of the valido property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidoWS }
     *     
     */
    public void setValido(ValidoWS value) {
        this.valido = value;
    }

}
