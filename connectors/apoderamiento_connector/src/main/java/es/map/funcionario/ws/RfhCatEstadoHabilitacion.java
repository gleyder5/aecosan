
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rfhCatEstadoHabilitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhCatEstadoHabilitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descEstadoHabilitacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEstadoHabilitacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacions" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhCatEstadoHabilitacion", propOrder = {
    "descEstadoHabilitacion",
    "idEstadoHabilitacion",
    "rfhHabilitacions"
})
public class RfhCatEstadoHabilitacion {

    protected String descEstadoHabilitacion;
    protected Integer idEstadoHabilitacion;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacions;

    /**
     * Gets the value of the descEstadoHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoHabilitacion() {
        return descEstadoHabilitacion;
    }

    /**
     * Sets the value of the descEstadoHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoHabilitacion(String value) {
        this.descEstadoHabilitacion = value;
    }

    /**
     * Gets the value of the idEstadoHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstadoHabilitacion() {
        return idEstadoHabilitacion;
    }

    /**
     * Sets the value of the idEstadoHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstadoHabilitacion(Integer value) {
        this.idEstadoHabilitacion = value;
    }

    /**
     * Gets the value of the rfhHabilitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacions() {
        if (rfhHabilitacions == null) {
            rfhHabilitacions = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacions;
    }

}
