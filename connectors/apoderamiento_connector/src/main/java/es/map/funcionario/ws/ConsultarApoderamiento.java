
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultarApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultarApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodApoderamiento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NIFPoderdante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIFPoderdante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIFApoderado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIFApoderado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NIFRepresentante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTramiteREA" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idTramiteOrganismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCategoriaREA" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Estado" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultarApoderamiento", propOrder = {
    "codApoderamiento",
    "nifPoderdante",
    "cifPoderdante",
    "nifApoderado",
    "cifApoderado",
    "nifRepresentante",
    "idOrganismo",
    "idTramiteREA",
    "idTramiteOrganismo",
    "idCategoriaREA",
    "estado",
    "fechaInicio",
    "fechaFin"
})
public class ConsultarApoderamiento {

    @XmlElement(name = "CodApoderamiento")
    protected Integer codApoderamiento;
    @XmlElement(name = "NIFPoderdante")
    protected String nifPoderdante;
    @XmlElement(name = "CIFPoderdante")
    protected String cifPoderdante;
    @XmlElement(name = "NIFApoderado")
    protected String nifApoderado;
    @XmlElement(name = "CIFApoderado")
    protected String cifApoderado;
    @XmlElement(name = "NIFRepresentante")
    protected String nifRepresentante;
    protected String idOrganismo;
    protected Integer idTramiteREA;
    protected String idTramiteOrganismo;
    protected Integer idCategoriaREA;
    @XmlElement(name = "Estado")
    protected Integer estado;
    protected String fechaInicio;
    protected String fechaFin;

    /**
     * Gets the value of the codApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodApoderamiento() {
        return codApoderamiento;
    }

    /**
     * Sets the value of the codApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodApoderamiento(Integer value) {
        this.codApoderamiento = value;
    }

    /**
     * Gets the value of the nifPoderdante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIFPoderdante() {
        return nifPoderdante;
    }

    /**
     * Sets the value of the nifPoderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIFPoderdante(String value) {
        this.nifPoderdante = value;
    }

    /**
     * Gets the value of the cifPoderdante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFPoderdante() {
        return cifPoderdante;
    }

    /**
     * Sets the value of the cifPoderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFPoderdante(String value) {
        this.cifPoderdante = value;
    }

    /**
     * Gets the value of the nifApoderado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIFApoderado() {
        return nifApoderado;
    }

    /**
     * Sets the value of the nifApoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIFApoderado(String value) {
        this.nifApoderado = value;
    }

    /**
     * Gets the value of the cifApoderado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFApoderado() {
        return cifApoderado;
    }

    /**
     * Sets the value of the cifApoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFApoderado(String value) {
        this.cifApoderado = value;
    }

    /**
     * Gets the value of the nifRepresentante property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIFRepresentante() {
        return nifRepresentante;
    }

    /**
     * Sets the value of the nifRepresentante property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIFRepresentante(String value) {
        this.nifRepresentante = value;
    }

    /**
     * Gets the value of the idOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdOrganismo() {
        return idOrganismo;
    }

    /**
     * Sets the value of the idOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdOrganismo(String value) {
        this.idOrganismo = value;
    }

    /**
     * Gets the value of the idTramiteREA property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTramiteREA() {
        return idTramiteREA;
    }

    /**
     * Sets the value of the idTramiteREA property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTramiteREA(Integer value) {
        this.idTramiteREA = value;
    }

    /**
     * Gets the value of the idTramiteOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTramiteOrganismo() {
        return idTramiteOrganismo;
    }

    /**
     * Sets the value of the idTramiteOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTramiteOrganismo(String value) {
        this.idTramiteOrganismo = value;
    }

    /**
     * Gets the value of the idCategoriaREA property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdCategoriaREA() {
        return idCategoriaREA;
    }

    /**
     * Sets the value of the idCategoriaREA property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdCategoriaREA(Integer value) {
        this.idCategoriaREA = value;
    }

    /**
     * Gets the value of the estado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEstado() {
        return estado;
    }

    /**
     * Sets the value of the estado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEstado(Integer value) {
        this.estado = value;
    }

    /**
     * Gets the value of the fechaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Sets the value of the fechaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicio(String value) {
        this.fechaInicio = value;
    }

    /**
     * Gets the value of the fechaFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * Sets the value of the fechaFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFin(String value) {
        this.fechaFin = value;
    }

}
