
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for altaApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="altaApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Poderdante" type="{http://ws.funcionario.map.es/}poderdanteApoderadoWS" minOccurs="0"/>
 *         &lt;element name="Apoderado" type="{http://ws.funcionario.map.es/}poderdanteApoderadoWS" minOccurs="0"/>
 *         &lt;element name="Representante" type="{http://ws.funcionario.map.es/}pfRepresentantePodWS" minOccurs="0"/>
 *         &lt;element name="Organismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaCodCategorias" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaCodTramites" type="{http://www.w3.org/2001/XMLSchema}int" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fechaInicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaFin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anexos" type="{http://ws.funcionario.map.es/}anexo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "altaApoderamiento", propOrder = {
    "poderdante",
    "apoderado",
    "representante",
    "organismo",
    "listaCodCategorias",
    "listaCodTramites",
    "fechaInicio",
    "fechaFin",
    "anexos"
})
public class AltaApoderamiento {

    @XmlElement(name = "Poderdante")
    protected PoderdanteApoderadoWS poderdante;
    @XmlElement(name = "Apoderado")
    protected PoderdanteApoderadoWS apoderado;
    @XmlElement(name = "Representante")
    protected PfRepresentantePodWS representante;
    @XmlElement(name = "Organismo")
    protected String organismo;
    @XmlElement(type = Integer.class)
    protected List<Integer> listaCodCategorias;
    @XmlElement(type = Integer.class)
    protected List<Integer> listaCodTramites;
    protected String fechaInicio;
    protected String fechaFin;
    protected List<Anexo> anexos;

    /**
     * Gets the value of the poderdante property.
     * 
     * @return
     *     possible object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public PoderdanteApoderadoWS getPoderdante() {
        return poderdante;
    }

    /**
     * Sets the value of the poderdante property.
     * 
     * @param value
     *     allowed object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public void setPoderdante(PoderdanteApoderadoWS value) {
        this.poderdante = value;
    }

    /**
     * Gets the value of the apoderado property.
     * 
     * @return
     *     possible object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public PoderdanteApoderadoWS getApoderado() {
        return apoderado;
    }

    /**
     * Sets the value of the apoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link PoderdanteApoderadoWS }
     *     
     */
    public void setApoderado(PoderdanteApoderadoWS value) {
        this.apoderado = value;
    }

    /**
     * Gets the value of the representante property.
     * 
     * @return
     *     possible object is
     *     {@link PfRepresentantePodWS }
     *     
     */
    public PfRepresentantePodWS getRepresentante() {
        return representante;
    }

    /**
     * Sets the value of the representante property.
     * 
     * @param value
     *     allowed object is
     *     {@link PfRepresentantePodWS }
     *     
     */
    public void setRepresentante(PfRepresentantePodWS value) {
        this.representante = value;
    }

    /**
     * Gets the value of the organismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganismo() {
        return organismo;
    }

    /**
     * Sets the value of the organismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganismo(String value) {
        this.organismo = value;
    }

    /**
     * Gets the value of the listaCodCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCodCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCodCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getListaCodCategorias() {
        if (listaCodCategorias == null) {
            listaCodCategorias = new ArrayList<Integer>();
        }
        return this.listaCodCategorias;
    }

    /**
     * Gets the value of the listaCodTramites property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCodTramites property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCodTramites().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Integer }
     * 
     * 
     */
    public List<Integer> getListaCodTramites() {
        if (listaCodTramites == null) {
            listaCodTramites = new ArrayList<Integer>();
        }
        return this.listaCodTramites;
    }

    /**
     * Gets the value of the fechaInicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Sets the value of the fechaInicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicio(String value) {
        this.fechaInicio = value;
    }

    /**
     * Gets the value of the fechaFin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaFin() {
        return fechaFin;
    }

    /**
     * Sets the value of the fechaFin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaFin(String value) {
        this.fechaFin = value;
    }

    /**
     * Gets the value of the anexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Anexo }
     * 
     * 
     */
    public List<Anexo> getAnexos() {
        if (anexos == null) {
            anexos = new ArrayList<Anexo>();
        }
        return this.anexos;
    }

}
