
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regCategoria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCategoria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="denomCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCategoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idCategoria" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idMultiOrganismo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idRea" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idRfh" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="regNanApodCategorias" type="{http://ws.funcionario.map.es/}regNanApodCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanTramiteCategorias" type="{http://ws.funcionario.map.es/}regNanTramiteCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdherido" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCategoria", propOrder = {
    "denomCategoria",
    "descCategoria",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idCategoria",
    "idMultiOrganismo",
    "idRea",
    "idRfh",
    "regNanApodCategorias",
    "regNanTramiteCategorias",
    "regOrganismoAdherido",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion"
})
public class RegCategoria {

    protected String denomCategoria;
    protected String descCategoria;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Integer idCategoria;
    protected Boolean idMultiOrganismo;
    protected Boolean idRea;
    protected Boolean idRfh;
    @XmlElement(nillable = true)
    protected List<RegNanApodCategoria> regNanApodCategorias;
    @XmlElement(nillable = true)
    protected List<RegNanTramiteCategoria> regNanTramiteCategorias;
    protected RegOrganismoAdherido regOrganismoAdherido;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;

    /**
     * Gets the value of the denomCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomCategoria() {
        return denomCategoria;
    }

    /**
     * Sets the value of the denomCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomCategoria(String value) {
        this.denomCategoria = value;
    }

    /**
     * Gets the value of the descCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCategoria() {
        return descCategoria;
    }

    /**
     * Sets the value of the descCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCategoria(String value) {
        this.descCategoria = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdCategoria() {
        return idCategoria;
    }

    /**
     * Sets the value of the idCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdCategoria(Integer value) {
        this.idCategoria = value;
    }

    /**
     * Gets the value of the idMultiOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdMultiOrganismo() {
        return idMultiOrganismo;
    }

    /**
     * Sets the value of the idMultiOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdMultiOrganismo(Boolean value) {
        this.idMultiOrganismo = value;
    }

    /**
     * Gets the value of the idRea property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdRea() {
        return idRea;
    }

    /**
     * Sets the value of the idRea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdRea(Boolean value) {
        this.idRea = value;
    }

    /**
     * Gets the value of the idRfh property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdRfh() {
        return idRfh;
    }

    /**
     * Sets the value of the idRfh property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdRfh(Boolean value) {
        this.idRfh = value;
    }

    /**
     * Gets the value of the regNanApodCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanApodCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanApodCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanApodCategoria }
     * 
     * 
     */
    public List<RegNanApodCategoria> getRegNanApodCategorias() {
        if (regNanApodCategorias == null) {
            regNanApodCategorias = new ArrayList<RegNanApodCategoria>();
        }
        return this.regNanApodCategorias;
    }

    /**
     * Gets the value of the regNanTramiteCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanTramiteCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanTramiteCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanTramiteCategoria }
     * 
     * 
     */
    public List<RegNanTramiteCategoria> getRegNanTramiteCategorias() {
        if (regNanTramiteCategorias == null) {
            regNanTramiteCategorias = new ArrayList<RegNanTramiteCategoria>();
        }
        return this.regNanTramiteCategorias;
    }

    /**
     * Gets the value of the regOrganismoAdherido property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdherido() {
        return regOrganismoAdherido;
    }

    /**
     * Sets the value of the regOrganismoAdherido property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdherido(RegOrganismoAdherido value) {
        this.regOrganismoAdherido = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

}
