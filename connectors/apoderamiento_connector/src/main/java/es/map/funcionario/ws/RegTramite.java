
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regTramite complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regTramite">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTramite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idConsentimientoApod" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idConsentimientoHab" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idRea" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idRfh" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idTramite" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUnicidad" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="numVigenciaMaxApod" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="reaApoderamientos" type="{http://ws.funcionario.map.es/}reaApoderamiento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanTramiteCategorias" type="{http://ws.funcionario.map.es/}regNanTramiteCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdherido" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacions" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regTramite", propOrder = {
    "codTramite",
    "denomTramite",
    "descTramite",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idConsentimientoApod",
    "idConsentimientoHab",
    "idRea",
    "idRfh",
    "idTramite",
    "idUnicidad",
    "numVigenciaMaxApod",
    "reaApoderamientos",
    "regNanTramiteCategorias",
    "regOrganismoAdherido",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhHabilitacions"
})
public class RegTramite {

    protected String codTramite;
    protected String denomTramite;
    protected String descTramite;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Boolean idConsentimientoApod;
    protected Boolean idConsentimientoHab;
    protected Boolean idRea;
    protected Boolean idRfh;
    protected Integer idTramite;
    protected Boolean idUnicidad;
    protected Integer numVigenciaMaxApod;
    @XmlElement(nillable = true)
    protected List<ReaApoderamiento> reaApoderamientos;
    @XmlElement(nillable = true)
    protected List<RegNanTramiteCategoria> regNanTramiteCategorias;
    protected RegOrganismoAdherido regOrganismoAdherido;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacions;

    /**
     * Gets the value of the codTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTramite() {
        return codTramite;
    }

    /**
     * Sets the value of the codTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTramite(String value) {
        this.codTramite = value;
    }

    /**
     * Gets the value of the denomTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomTramite() {
        return denomTramite;
    }

    /**
     * Sets the value of the denomTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomTramite(String value) {
        this.denomTramite = value;
    }

    /**
     * Gets the value of the descTramite property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTramite() {
        return descTramite;
    }

    /**
     * Sets the value of the descTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTramite(String value) {
        this.descTramite = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idConsentimientoApod property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdConsentimientoApod() {
        return idConsentimientoApod;
    }

    /**
     * Sets the value of the idConsentimientoApod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdConsentimientoApod(Boolean value) {
        this.idConsentimientoApod = value;
    }

    /**
     * Gets the value of the idConsentimientoHab property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdConsentimientoHab() {
        return idConsentimientoHab;
    }

    /**
     * Sets the value of the idConsentimientoHab property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdConsentimientoHab(Boolean value) {
        this.idConsentimientoHab = value;
    }

    /**
     * Gets the value of the idRea property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdRea() {
        return idRea;
    }

    /**
     * Sets the value of the idRea property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdRea(Boolean value) {
        this.idRea = value;
    }

    /**
     * Gets the value of the idRfh property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdRfh() {
        return idRfh;
    }

    /**
     * Sets the value of the idRfh property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdRfh(Boolean value) {
        this.idRfh = value;
    }

    /**
     * Gets the value of the idTramite property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTramite() {
        return idTramite;
    }

    /**
     * Sets the value of the idTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTramite(Integer value) {
        this.idTramite = value;
    }

    /**
     * Gets the value of the idUnicidad property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdUnicidad() {
        return idUnicidad;
    }

    /**
     * Sets the value of the idUnicidad property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdUnicidad(Boolean value) {
        this.idUnicidad = value;
    }

    /**
     * Gets the value of the numVigenciaMaxApod property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumVigenciaMaxApod() {
        return numVigenciaMaxApod;
    }

    /**
     * Sets the value of the numVigenciaMaxApod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumVigenciaMaxApod(Integer value) {
        this.numVigenciaMaxApod = value;
    }

    /**
     * Gets the value of the reaApoderamientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaApoderamientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaApoderamientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaApoderamiento }
     * 
     * 
     */
    public List<ReaApoderamiento> getReaApoderamientos() {
        if (reaApoderamientos == null) {
            reaApoderamientos = new ArrayList<ReaApoderamiento>();
        }
        return this.reaApoderamientos;
    }

    /**
     * Gets the value of the regNanTramiteCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanTramiteCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanTramiteCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanTramiteCategoria }
     * 
     * 
     */
    public List<RegNanTramiteCategoria> getRegNanTramiteCategorias() {
        if (regNanTramiteCategorias == null) {
            regNanTramiteCategorias = new ArrayList<RegNanTramiteCategoria>();
        }
        return this.regNanTramiteCategorias;
    }

    /**
     * Gets the value of the regOrganismoAdherido property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdherido() {
        return regOrganismoAdherido;
    }

    /**
     * Sets the value of the regOrganismoAdherido property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdherido(RegOrganismoAdherido value) {
        this.regOrganismoAdherido = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhHabilitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacions() {
        if (rfhHabilitacions == null) {
            rfhHabilitacions = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacions;
    }

}
