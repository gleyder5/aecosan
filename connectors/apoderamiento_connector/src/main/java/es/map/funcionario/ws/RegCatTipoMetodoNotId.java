
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regCatTipoMetodoNotId complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCatTipoMetodoNotId">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idMetodoNotificacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idTipoNotificacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCatTipoMetodoNotId", propOrder = {
    "idMetodoNotificacion",
    "idTipoNotificacion"
})
public class RegCatTipoMetodoNotId {

    protected Integer idMetodoNotificacion;
    protected Integer idTipoNotificacion;

    /**
     * Gets the value of the idMetodoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdMetodoNotificacion() {
        return idMetodoNotificacion;
    }

    /**
     * Sets the value of the idMetodoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdMetodoNotificacion(Integer value) {
        this.idMetodoNotificacion = value;
    }

    /**
     * Gets the value of the idTipoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoNotificacion() {
        return idTipoNotificacion;
    }

    /**
     * Sets the value of the idTipoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoNotificacion(Integer value) {
        this.idTipoNotificacion = value;
    }

}
