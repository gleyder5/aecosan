
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consultarCategoriasPorOrganismoWSResponseSalida complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consultarCategoriasPorOrganismoWSResponseSalida">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="error" type="{http://ws.funcionario.map.es/}errorWS" minOccurs="0"/>
 *         &lt;element name="listaCategorias" type="{http://ws.funcionario.map.es/}categoriaWS" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consultarCategoriasPorOrganismoWSResponseSalida", propOrder = {
    "error",
    "listaCategorias"
})
public class ConsultarCategoriasPorOrganismoWSResponseSalida {

    protected ErrorWS error;
    @XmlElement(nillable = true)
    protected List<CategoriaWS> listaCategorias;

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link ErrorWS }
     *     
     */
    public ErrorWS getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link ErrorWS }
     *     
     */
    public void setError(ErrorWS value) {
        this.error = value;
    }

    /**
     * Gets the value of the listaCategorias property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCategorias property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCategorias().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CategoriaWS }
     * 
     * 
     */
    public List<CategoriaWS> getListaCategorias() {
        if (listaCategorias == null) {
            listaCategorias = new ArrayList<CategoriaWS>();
        }
        return this.listaCategorias;
    }

}
