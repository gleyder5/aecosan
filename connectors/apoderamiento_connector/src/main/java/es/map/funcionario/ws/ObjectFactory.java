
package es.map.funcionario.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the es.map.funcionario.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsultarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "consultarApoderamientoResponse");
    private final static QName _AceptarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "aceptarApoderamientoResponse");
    private final static QName _AltaApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "altaApoderamiento");
    private final static QName _ModificarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "modificarApoderamientoResponse");
    private final static QName _AprobarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "aprobarApoderamiento");
    private final static QName _DenegarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "denegarApoderamientoResponse");
    private final static QName _AltaApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "altaApoderamientoResponse");
    private final static QName _NoSubsanarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "noSubsanarApoderamientoResponse");
    private final static QName _RenunciarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "renunciarApoderamientoResponse");
    private final static QName _NoSubsanarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "noSubsanarApoderamiento");
    private final static QName _ConsultarCategoriasPorOrganismoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "consultarCategoriasPorOrganismoResponse");
    private final static QName _DenegarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "denegarApoderamiento");
    private final static QName _RevocarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "revocarApoderamiento");
    private final static QName _RenunciarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "renunciarApoderamiento");
    private final static QName _SubsanarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "subsanarApoderamiento");
    private final static QName _RevocarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "revocarApoderamientoResponse");
    private final static QName _ConsultarTramitesPorOrganismo_QNAME = new QName("http://ws.funcionario.map.es/", "consultarTramitesPorOrganismo");
    private final static QName _AceptarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "aceptarApoderamiento");
    private final static QName _AprobarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "aprobarApoderamientoResponse");
    private final static QName _ConsultarTramitesPorOrganismoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "consultarTramitesPorOrganismoResponse");
    private final static QName _ConsultarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "consultarApoderamiento");
    private final static QName _SubsanarApoderamientoResponse_QNAME = new QName("http://ws.funcionario.map.es/", "subsanarApoderamientoResponse");
    private final static QName _ConsultarCategoriasPorOrganismo_QNAME = new QName("http://ws.funcionario.map.es/", "consultarCategoriasPorOrganismo");
    private final static QName _ModificarApoderamiento_QNAME = new QName("http://ws.funcionario.map.es/", "modificarApoderamiento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: es.map.funcionario.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AprobarApoderamiento }
     * 
     */
    public AprobarApoderamiento createAprobarApoderamiento() {
        return new AprobarApoderamiento();
    }

    /**
     * Create an instance of {@link DenegarApoderamientoResponse }
     * 
     */
    public DenegarApoderamientoResponse createDenegarApoderamientoResponse() {
        return new DenegarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link NoSubsanarApoderamientoResponse }
     * 
     */
    public NoSubsanarApoderamientoResponse createNoSubsanarApoderamientoResponse() {
        return new NoSubsanarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link AltaApoderamientoResponse }
     * 
     */
    public AltaApoderamientoResponse createAltaApoderamientoResponse() {
        return new AltaApoderamientoResponse();
    }

    /**
     * Create an instance of {@link RenunciarApoderamientoResponse }
     * 
     */
    public RenunciarApoderamientoResponse createRenunciarApoderamientoResponse() {
        return new RenunciarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link NoSubsanarApoderamiento }
     * 
     */
    public NoSubsanarApoderamiento createNoSubsanarApoderamiento() {
        return new NoSubsanarApoderamiento();
    }

    /**
     * Create an instance of {@link ConsultarCategoriasPorOrganismoResponse }
     * 
     */
    public ConsultarCategoriasPorOrganismoResponse createConsultarCategoriasPorOrganismoResponse() {
        return new ConsultarCategoriasPorOrganismoResponse();
    }

    /**
     * Create an instance of {@link DenegarApoderamiento }
     * 
     */
    public DenegarApoderamiento createDenegarApoderamiento() {
        return new DenegarApoderamiento();
    }

    /**
     * Create an instance of {@link AceptarApoderamientoResponse }
     * 
     */
    public AceptarApoderamientoResponse createAceptarApoderamientoResponse() {
        return new AceptarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link AltaApoderamiento }
     * 
     */
    public AltaApoderamiento createAltaApoderamiento() {
        return new AltaApoderamiento();
    }

    /**
     * Create an instance of {@link ConsultarApoderamientoResponse }
     * 
     */
    public ConsultarApoderamientoResponse createConsultarApoderamientoResponse() {
        return new ConsultarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link ModificarApoderamientoResponse }
     * 
     */
    public ModificarApoderamientoResponse createModificarApoderamientoResponse() {
        return new ModificarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link AprobarApoderamientoResponse }
     * 
     */
    public AprobarApoderamientoResponse createAprobarApoderamientoResponse() {
        return new AprobarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link AceptarApoderamiento }
     * 
     */
    public AceptarApoderamiento createAceptarApoderamiento() {
        return new AceptarApoderamiento();
    }

    /**
     * Create an instance of {@link SubsanarApoderamientoResponse }
     * 
     */
    public SubsanarApoderamientoResponse createSubsanarApoderamientoResponse() {
        return new SubsanarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link ConsultarApoderamiento }
     * 
     */
    public ConsultarApoderamiento createConsultarApoderamiento() {
        return new ConsultarApoderamiento();
    }

    /**
     * Create an instance of {@link ConsultarTramitesPorOrganismoResponse }
     * 
     */
    public ConsultarTramitesPorOrganismoResponse createConsultarTramitesPorOrganismoResponse() {
        return new ConsultarTramitesPorOrganismoResponse();
    }

    /**
     * Create an instance of {@link ModificarApoderamiento }
     * 
     */
    public ModificarApoderamiento createModificarApoderamiento() {
        return new ModificarApoderamiento();
    }

    /**
     * Create an instance of {@link ConsultarCategoriasPorOrganismo }
     * 
     */
    public ConsultarCategoriasPorOrganismo createConsultarCategoriasPorOrganismo() {
        return new ConsultarCategoriasPorOrganismo();
    }

    /**
     * Create an instance of {@link RevocarApoderamiento }
     * 
     */
    public RevocarApoderamiento createRevocarApoderamiento() {
        return new RevocarApoderamiento();
    }

    /**
     * Create an instance of {@link SubsanarApoderamiento }
     * 
     */
    public SubsanarApoderamiento createSubsanarApoderamiento() {
        return new SubsanarApoderamiento();
    }

    /**
     * Create an instance of {@link RenunciarApoderamiento }
     * 
     */
    public RenunciarApoderamiento createRenunciarApoderamiento() {
        return new RenunciarApoderamiento();
    }

    /**
     * Create an instance of {@link ConsultarTramitesPorOrganismo }
     * 
     */
    public ConsultarTramitesPorOrganismo createConsultarTramitesPorOrganismo() {
        return new ConsultarTramitesPorOrganismo();
    }

    /**
     * Create an instance of {@link RevocarApoderamientoResponse }
     * 
     */
    public RevocarApoderamientoResponse createRevocarApoderamientoResponse() {
        return new RevocarApoderamientoResponse();
    }

    /**
     * Create an instance of {@link ReaHistApoderamiento }
     * 
     */
    public ReaHistApoderamiento createReaHistApoderamiento() {
        return new ReaHistApoderamiento();
    }

    /**
     * Create an instance of {@link CategoriaWS }
     * 
     */
    public CategoriaWS createCategoriaWS() {
        return new CategoriaWS();
    }

    /**
     * Create an instance of {@link RfhHistTramitacion }
     * 
     */
    public RfhHistTramitacion createRfhHistTramitacion() {
        return new RfhHistTramitacion();
    }

    /**
     * Create an instance of {@link RegNanUsuarioOrganismoId }
     * 
     */
    public RegNanUsuarioOrganismoId createRegNanUsuarioOrganismoId() {
        return new RegNanUsuarioOrganismoId();
    }

    /**
     * Create an instance of {@link RfhHistCiudadano }
     * 
     */
    public RfhHistCiudadano createRfhHistCiudadano() {
        return new RfhHistCiudadano();
    }

    /**
     * Create an instance of {@link RegCatPermiso }
     * 
     */
    public RegCatPermiso createRegCatPermiso() {
        return new RegCatPermiso();
    }

    /**
     * Create an instance of {@link RfhFuncionario }
     * 
     */
    public RfhFuncionario createRfhFuncionario() {
        return new RfhFuncionario();
    }

    /**
     * Create an instance of {@link RfhAnexoTramitacion }
     * 
     */
    public RfhAnexoTramitacion createRfhAnexoTramitacion() {
        return new RfhAnexoTramitacion();
    }

    /**
     * Create an instance of {@link ReaPersonaFisica }
     * 
     */
    public ReaPersonaFisica createReaPersonaFisica() {
        return new ReaPersonaFisica();
    }

    /**
     * Create an instance of {@link RegCatMetodoNotificacion }
     * 
     */
    public RegCatMetodoNotificacion createRegCatMetodoNotificacion() {
        return new RegCatMetodoNotificacion();
    }

    /**
     * Create an instance of {@link RegUsuario }
     * 
     */
    public RegUsuario createRegUsuario() {
        return new RegUsuario();
    }

    /**
     * Create an instance of {@link RfhCiudadano }
     * 
     */
    public RfhCiudadano createRfhCiudadano() {
        return new RfhCiudadano();
    }

    /**
     * Create an instance of {@link RfhCredencial }
     * 
     */
    public RfhCredencial createRfhCredencial() {
        return new RfhCredencial();
    }

    /**
     * Create an instance of {@link RegProvincia }
     * 
     */
    public RegProvincia createRegProvincia() {
        return new RegProvincia();
    }

    /**
     * Create an instance of {@link AsesorJuridicoWS }
     * 
     */
    public AsesorJuridicoWS createAsesorJuridicoWS() {
        return new AsesorJuridicoWS();
    }

    /**
     * Create an instance of {@link RfhHistHabilitacion }
     * 
     */
    public RfhHistHabilitacion createRfhHistHabilitacion() {
        return new RfhHistHabilitacion();
    }

    /**
     * Create an instance of {@link RfhCatEstadoHabilitacion }
     * 
     */
    public RfhCatEstadoHabilitacion createRfhCatEstadoHabilitacion() {
        return new RfhCatEstadoHabilitacion();
    }

    /**
     * Create an instance of {@link RfhNanCredencialHab }
     * 
     */
    public RfhNanCredencialHab createRfhNanCredencialHab() {
        return new RfhNanCredencialHab();
    }

    /**
     * Create an instance of {@link RegNanUsuarioPermisoId }
     * 
     */
    public RegNanUsuarioPermisoId createRegNanUsuarioPermisoId() {
        return new RegNanUsuarioPermisoId();
    }

    /**
     * Create an instance of {@link RfhCatEstadoTramitacion }
     * 
     */
    public RfhCatEstadoTramitacion createRfhCatEstadoTramitacion() {
        return new RfhCatEstadoTramitacion();
    }

    /**
     * Create an instance of {@link ReaCatEstado }
     * 
     */
    public ReaCatEstado createReaCatEstado() {
        return new ReaCatEstado();
    }

    /**
     * Create an instance of {@link RfhCatTipoDocumento }
     * 
     */
    public RfhCatTipoDocumento createRfhCatTipoDocumento() {
        return new RfhCatTipoDocumento();
    }

    /**
     * Create an instance of {@link ReaPersonaJuridica }
     * 
     */
    public ReaPersonaJuridica createReaPersonaJuridica() {
        return new ReaPersonaJuridica();
    }

    /**
     * Create an instance of {@link AnexoBean }
     * 
     */
    public AnexoBean createAnexoBean() {
        return new AnexoBean();
    }

    /**
     * Create an instance of {@link RegNanTramiteCategoriaId }
     * 
     */
    public RegNanTramiteCategoriaId createRegNanTramiteCategoriaId() {
        return new RegNanTramiteCategoriaId();
    }

    /**
     * Create an instance of {@link ReaAnexoRepresentacion }
     * 
     */
    public ReaAnexoRepresentacion createReaAnexoRepresentacion() {
        return new ReaAnexoRepresentacion();
    }

    /**
     * Create an instance of {@link BusquedaGenericaBean }
     * 
     */
    public BusquedaGenericaBean createBusquedaGenericaBean() {
        return new BusquedaGenericaBean();
    }

    /**
     * Create an instance of {@link ValidoWS }
     * 
     */
    public ValidoWS createValidoWS() {
        return new ValidoWS();
    }

    /**
     * Create an instance of {@link ArrayList }
     * 
     */
    public ArrayList createArrayList() {
        return new ArrayList();
    }

    /**
     * Create an instance of {@link RegNanApodCategoria }
     * 
     */
    public RegNanApodCategoria createRegNanApodCategoria() {
        return new RegNanApodCategoria();
    }

    /**
     * Create an instance of {@link ConsultaDescargaListaWS }
     * 
     */
    public ConsultaDescargaListaWS createConsultaDescargaListaWS() {
        return new ConsultaDescargaListaWS();
    }

    /**
     * Create an instance of {@link ReaRepresentacion }
     * 
     */
    public ReaRepresentacion createReaRepresentacion() {
        return new ReaRepresentacion();
    }

    /**
     * Create an instance of {@link RegCategoria }
     * 
     */
    public RegCategoria createRegCategoria() {
        return new RegCategoria();
    }

    /**
     * Create an instance of {@link PoderdanteApoderadoWS }
     * 
     */
    public PoderdanteApoderadoWS createPoderdanteApoderadoWS() {
        return new PoderdanteApoderadoWS();
    }

    /**
     * Create an instance of {@link ReaHistRepresentacion }
     * 
     */
    public ReaHistRepresentacion createReaHistRepresentacion() {
        return new ReaHistRepresentacion();
    }

    /**
     * Create an instance of {@link RegLogAccesos }
     * 
     */
    public RegLogAccesos createRegLogAccesos() {
        return new RegLogAccesos();
    }

    /**
     * Create an instance of {@link RfhTramitacion }
     * 
     */
    public RfhTramitacion createRfhTramitacion() {
        return new RfhTramitacion();
    }

    /**
     * Create an instance of {@link ConsultarCategoriasPorOrganismoWSResponseSalida }
     * 
     */
    public ConsultarCategoriasPorOrganismoWSResponseSalida createConsultarCategoriasPorOrganismoWSResponseSalida() {
        return new ConsultarCategoriasPorOrganismoWSResponseSalida();
    }

    /**
     * Create an instance of {@link RegNanUsuarioOrganismo }
     * 
     */
    public RegNanUsuarioOrganismo createRegNanUsuarioOrganismo() {
        return new RegNanUsuarioOrganismo();
    }

    /**
     * Create an instance of {@link RegTramite }
     * 
     */
    public RegTramite createRegTramite() {
        return new RegTramite();
    }

    /**
     * Create an instance of {@link RegNotificacion }
     * 
     */
    public RegNotificacion createRegNotificacion() {
        return new RegNotificacion();
    }

    /**
     * Create an instance of {@link NoSubsanarApoderamientoWSResponseSalida }
     * 
     */
    public NoSubsanarApoderamientoWSResponseSalida createNoSubsanarApoderamientoWSResponseSalida() {
        return new NoSubsanarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link RegCatAplicacion }
     * 
     */
    public RegCatAplicacion createRegCatAplicacion() {
        return new RegCatAplicacion();
    }

    /**
     * Create an instance of {@link RfhHistFuncionario }
     * 
     */
    public RfhHistFuncionario createRfhHistFuncionario() {
        return new RfhHistFuncionario();
    }

    /**
     * Create an instance of {@link DenegarApoderamientoWSResponseSalida }
     * 
     */
    public DenegarApoderamientoWSResponseSalida createDenegarApoderamientoWSResponseSalida() {
        return new DenegarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link SubsanarApoderamientoWSResponseSalida }
     * 
     */
    public SubsanarApoderamientoWSResponseSalida createSubsanarApoderamientoWSResponseSalida() {
        return new SubsanarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link TramiteWS }
     * 
     */
    public TramiteWS createTramiteWS() {
        return new TramiteWS();
    }

    /**
     * Create an instance of {@link ReaAdministracion }
     * 
     */
    public ReaAdministracion createReaAdministracion() {
        return new ReaAdministracion();
    }

    /**
     * Create an instance of {@link RegCatTipoNotificacion }
     * 
     */
    public RegCatTipoNotificacion createRegCatTipoNotificacion() {
        return new RegCatTipoNotificacion();
    }

    /**
     * Create an instance of {@link AltaApoderamientoWSResponseSalida }
     * 
     */
    public AltaApoderamientoWSResponseSalida createAltaApoderamientoWSResponseSalida() {
        return new AltaApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link RegOrganismoAdherido }
     * 
     */
    public RegOrganismoAdherido createRegOrganismoAdherido() {
        return new RegOrganismoAdherido();
    }

    /**
     * Create an instance of {@link Anexo }
     * 
     */
    public Anexo createAnexo() {
        return new Anexo();
    }

    /**
     * Create an instance of {@link RenunciarApoderamientoWSResponseSalida }
     * 
     */
    public RenunciarApoderamientoWSResponseSalida createRenunciarApoderamientoWSResponseSalida() {
        return new RenunciarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link AprobarApoderamientoWSResponseSalida }
     * 
     */
    public AprobarApoderamientoWSResponseSalida createAprobarApoderamientoWSResponseSalida() {
        return new AprobarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link RfhAnexoCredencial }
     * 
     */
    public RfhAnexoCredencial createRfhAnexoCredencial() {
        return new RfhAnexoCredencial();
    }

    /**
     * Create an instance of {@link ConsultarTramitesPorOrganismoWSResponseSalida }
     * 
     */
    public ConsultarTramitesPorOrganismoWSResponseSalida createConsultarTramitesPorOrganismoWSResponseSalida() {
        return new ConsultarTramitesPorOrganismoWSResponseSalida();
    }

    /**
     * Create an instance of {@link PfRepresentantePodWS }
     * 
     */
    public PfRepresentantePodWS createPfRepresentantePodWS() {
        return new PfRepresentantePodWS();
    }

    /**
     * Create an instance of {@link AceptarApoderamientoWSResponseSalida }
     * 
     */
    public AceptarApoderamientoWSResponseSalida createAceptarApoderamientoWSResponseSalida() {
        return new AceptarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link TablaResultado }
     * 
     */
    public TablaResultado createTablaResultado() {
        return new TablaResultado();
    }

    /**
     * Create an instance of {@link RegNanTramiteCategoria }
     * 
     */
    public RegNanTramiteCategoria createRegNanTramiteCategoria() {
        return new RegNanTramiteCategoria();
    }

    /**
     * Create an instance of {@link RegNanUsuarioPermiso }
     * 
     */
    public RegNanUsuarioPermiso createRegNanUsuarioPermiso() {
        return new RegNanUsuarioPermiso();
    }

    /**
     * Create an instance of {@link RegNanApodCategoriaId }
     * 
     */
    public RegNanApodCategoriaId createRegNanApodCategoriaId() {
        return new RegNanApodCategoriaId();
    }

    /**
     * Create an instance of {@link ErrorWS }
     * 
     */
    public ErrorWS createErrorWS() {
        return new ErrorWS();
    }

    /**
     * Create an instance of {@link RegCatTipoMetodoNot }
     * 
     */
    public RegCatTipoMetodoNot createRegCatTipoMetodoNot() {
        return new RegCatTipoMetodoNot();
    }

    /**
     * Create an instance of {@link RfhNanCredencialHabId }
     * 
     */
    public RfhNanCredencialHabId createRfhNanCredencialHabId() {
        return new RfhNanCredencialHabId();
    }

    /**
     * Create an instance of {@link RegHistTramite }
     * 
     */
    public RegHistTramite createRegHistTramite() {
        return new RegHistTramite();
    }

    /**
     * Create an instance of {@link ConsultarApoderamientoWSResponseSalida }
     * 
     */
    public ConsultarApoderamientoWSResponseSalida createConsultarApoderamientoWSResponseSalida() {
        return new ConsultarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link ReaCatFormato }
     * 
     */
    public ReaCatFormato createReaCatFormato() {
        return new ReaCatFormato();
    }

    /**
     * Create an instance of {@link ReaCatTipoDocumento }
     * 
     */
    public ReaCatTipoDocumento createReaCatTipoDocumento() {
        return new ReaCatTipoDocumento();
    }

    /**
     * Create an instance of {@link RfhHabilitacion }
     * 
     */
    public RfhHabilitacion createRfhHabilitacion() {
        return new RfhHabilitacion();
    }

    /**
     * Create an instance of {@link ReaCatTipodocEni }
     * 
     */
    public ReaCatTipodocEni createReaCatTipodocEni() {
        return new ReaCatTipodocEni();
    }

    /**
     * Create an instance of {@link RegCatTipoMetodoNotId }
     * 
     */
    public RegCatTipoMetodoNotId createRegCatTipoMetodoNotId() {
        return new RegCatTipoMetodoNotId();
    }

    /**
     * Create an instance of {@link ModificarApoderamientoWSResponseSalida }
     * 
     */
    public ModificarApoderamientoWSResponseSalida createModificarApoderamientoWSResponseSalida() {
        return new ModificarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link ReaAnexo }
     * 
     */
    public ReaAnexo createReaAnexo() {
        return new ReaAnexo();
    }

    /**
     * Create an instance of {@link RevocarApoderamientoWSResponseSalida }
     * 
     */
    public RevocarApoderamientoWSResponseSalida createRevocarApoderamientoWSResponseSalida() {
        return new RevocarApoderamientoWSResponseSalida();
    }

    /**
     * Create an instance of {@link HistoricoApoderamientoBean }
     * 
     */
    public HistoricoApoderamientoBean createHistoricoApoderamientoBean() {
        return new HistoricoApoderamientoBean();
    }

    /**
     * Create an instance of {@link ReaApoderamiento }
     * 
     */
    public ReaApoderamiento createReaApoderamiento() {
        return new ReaApoderamiento();
    }

    /**
     * Create an instance of {@link ReaCatEstadoRepresentacion }
     * 
     */
    public ReaCatEstadoRepresentacion createReaCatEstadoRepresentacion() {
        return new ReaCatEstadoRepresentacion();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarApoderamientoResponse")
    public JAXBElement<ConsultarApoderamientoResponse> createConsultarApoderamientoResponse(ConsultarApoderamientoResponse value) {
        return new JAXBElement<ConsultarApoderamientoResponse>(_ConsultarApoderamientoResponse_QNAME, ConsultarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AceptarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "aceptarApoderamientoResponse")
    public JAXBElement<AceptarApoderamientoResponse> createAceptarApoderamientoResponse(AceptarApoderamientoResponse value) {
        return new JAXBElement<AceptarApoderamientoResponse>(_AceptarApoderamientoResponse_QNAME, AceptarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "altaApoderamiento")
    public JAXBElement<AltaApoderamiento> createAltaApoderamiento(AltaApoderamiento value) {
        return new JAXBElement<AltaApoderamiento>(_AltaApoderamiento_QNAME, AltaApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "modificarApoderamientoResponse")
    public JAXBElement<ModificarApoderamientoResponse> createModificarApoderamientoResponse(ModificarApoderamientoResponse value) {
        return new JAXBElement<ModificarApoderamientoResponse>(_ModificarApoderamientoResponse_QNAME, ModificarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "aprobarApoderamiento")
    public JAXBElement<AprobarApoderamiento> createAprobarApoderamiento(AprobarApoderamiento value) {
        return new JAXBElement<AprobarApoderamiento>(_AprobarApoderamiento_QNAME, AprobarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DenegarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "denegarApoderamientoResponse")
    public JAXBElement<DenegarApoderamientoResponse> createDenegarApoderamientoResponse(DenegarApoderamientoResponse value) {
        return new JAXBElement<DenegarApoderamientoResponse>(_DenegarApoderamientoResponse_QNAME, DenegarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "altaApoderamientoResponse")
    public JAXBElement<AltaApoderamientoResponse> createAltaApoderamientoResponse(AltaApoderamientoResponse value) {
        return new JAXBElement<AltaApoderamientoResponse>(_AltaApoderamientoResponse_QNAME, AltaApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSubsanarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "noSubsanarApoderamientoResponse")
    public JAXBElement<NoSubsanarApoderamientoResponse> createNoSubsanarApoderamientoResponse(NoSubsanarApoderamientoResponse value) {
        return new JAXBElement<NoSubsanarApoderamientoResponse>(_NoSubsanarApoderamientoResponse_QNAME, NoSubsanarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenunciarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "renunciarApoderamientoResponse")
    public JAXBElement<RenunciarApoderamientoResponse> createRenunciarApoderamientoResponse(RenunciarApoderamientoResponse value) {
        return new JAXBElement<RenunciarApoderamientoResponse>(_RenunciarApoderamientoResponse_QNAME, RenunciarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NoSubsanarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "noSubsanarApoderamiento")
    public JAXBElement<NoSubsanarApoderamiento> createNoSubsanarApoderamiento(NoSubsanarApoderamiento value) {
        return new JAXBElement<NoSubsanarApoderamiento>(_NoSubsanarApoderamiento_QNAME, NoSubsanarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCategoriasPorOrganismoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarCategoriasPorOrganismoResponse")
    public JAXBElement<ConsultarCategoriasPorOrganismoResponse> createConsultarCategoriasPorOrganismoResponse(ConsultarCategoriasPorOrganismoResponse value) {
        return new JAXBElement<ConsultarCategoriasPorOrganismoResponse>(_ConsultarCategoriasPorOrganismoResponse_QNAME, ConsultarCategoriasPorOrganismoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DenegarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "denegarApoderamiento")
    public JAXBElement<DenegarApoderamiento> createDenegarApoderamiento(DenegarApoderamiento value) {
        return new JAXBElement<DenegarApoderamiento>(_DenegarApoderamiento_QNAME, DenegarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevocarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "revocarApoderamiento")
    public JAXBElement<RevocarApoderamiento> createRevocarApoderamiento(RevocarApoderamiento value) {
        return new JAXBElement<RevocarApoderamiento>(_RevocarApoderamiento_QNAME, RevocarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenunciarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "renunciarApoderamiento")
    public JAXBElement<RenunciarApoderamiento> createRenunciarApoderamiento(RenunciarApoderamiento value) {
        return new JAXBElement<RenunciarApoderamiento>(_RenunciarApoderamiento_QNAME, RenunciarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubsanarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "subsanarApoderamiento")
    public JAXBElement<SubsanarApoderamiento> createSubsanarApoderamiento(SubsanarApoderamiento value) {
        return new JAXBElement<SubsanarApoderamiento>(_SubsanarApoderamiento_QNAME, SubsanarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RevocarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "revocarApoderamientoResponse")
    public JAXBElement<RevocarApoderamientoResponse> createRevocarApoderamientoResponse(RevocarApoderamientoResponse value) {
        return new JAXBElement<RevocarApoderamientoResponse>(_RevocarApoderamientoResponse_QNAME, RevocarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarTramitesPorOrganismo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarTramitesPorOrganismo")
    public JAXBElement<ConsultarTramitesPorOrganismo> createConsultarTramitesPorOrganismo(ConsultarTramitesPorOrganismo value) {
        return new JAXBElement<ConsultarTramitesPorOrganismo>(_ConsultarTramitesPorOrganismo_QNAME, ConsultarTramitesPorOrganismo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AceptarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "aceptarApoderamiento")
    public JAXBElement<AceptarApoderamiento> createAceptarApoderamiento(AceptarApoderamiento value) {
        return new JAXBElement<AceptarApoderamiento>(_AceptarApoderamiento_QNAME, AceptarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AprobarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "aprobarApoderamientoResponse")
    public JAXBElement<AprobarApoderamientoResponse> createAprobarApoderamientoResponse(AprobarApoderamientoResponse value) {
        return new JAXBElement<AprobarApoderamientoResponse>(_AprobarApoderamientoResponse_QNAME, AprobarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarTramitesPorOrganismoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarTramitesPorOrganismoResponse")
    public JAXBElement<ConsultarTramitesPorOrganismoResponse> createConsultarTramitesPorOrganismoResponse(ConsultarTramitesPorOrganismoResponse value) {
        return new JAXBElement<ConsultarTramitesPorOrganismoResponse>(_ConsultarTramitesPorOrganismoResponse_QNAME, ConsultarTramitesPorOrganismoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarApoderamiento")
    public JAXBElement<ConsultarApoderamiento> createConsultarApoderamiento(ConsultarApoderamiento value) {
        return new JAXBElement<ConsultarApoderamiento>(_ConsultarApoderamiento_QNAME, ConsultarApoderamiento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubsanarApoderamientoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "subsanarApoderamientoResponse")
    public JAXBElement<SubsanarApoderamientoResponse> createSubsanarApoderamientoResponse(SubsanarApoderamientoResponse value) {
        return new JAXBElement<SubsanarApoderamientoResponse>(_SubsanarApoderamientoResponse_QNAME, SubsanarApoderamientoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultarCategoriasPorOrganismo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "consultarCategoriasPorOrganismo")
    public JAXBElement<ConsultarCategoriasPorOrganismo> createConsultarCategoriasPorOrganismo(ConsultarCategoriasPorOrganismo value) {
        return new JAXBElement<ConsultarCategoriasPorOrganismo>(_ConsultarCategoriasPorOrganismo_QNAME, ConsultarCategoriasPorOrganismo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarApoderamiento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.funcionario.map.es/", name = "modificarApoderamiento")
    public JAXBElement<ModificarApoderamiento> createModificarApoderamiento(ModificarApoderamiento value) {
        return new JAXBElement<ModificarApoderamiento>(_ModificarApoderamiento_QNAME, ModificarApoderamiento.class, null, value);
    }

}
