
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for aceptarApoderamiento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="aceptarApoderamiento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Organismo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodApoderamiento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NIF_NIE__Apoderado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIF_Apoderado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="anexos" type="{http://ws.funcionario.map.es/}anexo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aceptarApoderamiento", propOrder = {
    "organismo",
    "codApoderamiento",
    "nifnieApoderado",
    "cifApoderado",
    "anexos"
})
public class AceptarApoderamiento {

    @XmlElement(name = "Organismo")
    protected String organismo;
    @XmlElement(name = "CodApoderamiento")
    protected Integer codApoderamiento;
    @XmlElement(name = "NIF_NIE__Apoderado")
    protected String nifnieApoderado;
    @XmlElement(name = "CIF_Apoderado")
    protected String cifApoderado;
    protected List<Anexo> anexos;

    /**
     * Gets the value of the organismo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganismo() {
        return organismo;
    }

    /**
     * Sets the value of the organismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganismo(String value) {
        this.organismo = value;
    }

    /**
     * Gets the value of the codApoderamiento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCodApoderamiento() {
        return codApoderamiento;
    }

    /**
     * Sets the value of the codApoderamiento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCodApoderamiento(Integer value) {
        this.codApoderamiento = value;
    }

    /**
     * Gets the value of the nifnieApoderado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNIFNIEApoderado() {
        return nifnieApoderado;
    }

    /**
     * Sets the value of the nifnieApoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNIFNIEApoderado(String value) {
        this.nifnieApoderado = value;
    }

    /**
     * Gets the value of the cifApoderado property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIFApoderado() {
        return cifApoderado;
    }

    /**
     * Sets the value of the cifApoderado property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIFApoderado(String value) {
        this.cifApoderado = value;
    }

    /**
     * Gets the value of the anexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Anexo }
     * 
     * 
     */
    public List<Anexo> getAnexos() {
        if (anexos == null) {
            anexos = new ArrayList<Anexo>();
        }
        return this.anexos;
    }

}
