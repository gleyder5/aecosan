
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhFuncionario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhFuncionario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCuerpoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNifNie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPuestoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoCentroDirectivoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoDestinoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoOrganismoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomCuerpoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomPuestoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoCentroDirectivoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoDestinoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoOrganismoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDesactiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhUltimaActivacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhUltimaBaja" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idFuncionario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="regNotificacionsForIdFuncionario" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNotificacionsForIdFuncionarioDestinatario" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdherido" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhCredencials" type="{http://ws.funcionario.map.es/}rfhCredencial" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacions" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhTramitacions" type="{http://ws.funcionario.map.es/}rfhTramitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhFuncionario", propOrder = {
    "codCuerpoRcp",
    "codNifNie",
    "codPuestoRcp",
    "codUoCentroDirectivoRcp",
    "codUoDestinoRcp",
    "codUoOrganismoRcp",
    "denomCuerpoRcp",
    "denomPuestoRcp",
    "denomUoCentroDirectivoRcp",
    "denomUoDestinoRcp",
    "denomUoOrganismoRcp",
    "descApellido1",
    "descApellido2",
    "descDesactiva",
    "descEmail",
    "descNombre",
    "fhCreacion",
    "fhModificacion",
    "fhUltimaActivacion",
    "fhUltimaBaja",
    "idActivo",
    "idFuncionario",
    "idTipoDocumento",
    "regNotificacionsForIdFuncionario",
    "regNotificacionsForIdFuncionarioDestinatario",
    "regOrganismoAdherido",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhCredencials",
    "rfhHabilitacions",
    "rfhTramitacions"
})
public class RfhFuncionario {

    protected String codCuerpoRcp;
    protected String codNifNie;
    protected String codPuestoRcp;
    protected String codUoCentroDirectivoRcp;
    protected String codUoDestinoRcp;
    protected String codUoOrganismoRcp;
    protected String denomCuerpoRcp;
    protected String denomPuestoRcp;
    protected String denomUoCentroDirectivoRcp;
    protected String denomUoDestinoRcp;
    protected String denomUoOrganismoRcp;
    protected String descApellido1;
    protected String descApellido2;
    protected String descDesactiva;
    protected String descEmail;
    protected String descNombre;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhUltimaActivacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhUltimaBaja;
    protected Boolean idActivo;
    protected Long idFuncionario;
    protected Integer idTipoDocumento;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacionsForIdFuncionario;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacionsForIdFuncionarioDestinatario;
    protected RegOrganismoAdherido regOrganismoAdherido;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhCredencial> rfhCredencials;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacions;
    @XmlElement(nillable = true)
    protected List<RfhTramitacion> rfhTramitacions;

    /**
     * Gets the value of the codCuerpoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCuerpoRcp() {
        return codCuerpoRcp;
    }

    /**
     * Sets the value of the codCuerpoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCuerpoRcp(String value) {
        this.codCuerpoRcp = value;
    }

    /**
     * Gets the value of the codNifNie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNifNie() {
        return codNifNie;
    }

    /**
     * Sets the value of the codNifNie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNifNie(String value) {
        this.codNifNie = value;
    }

    /**
     * Gets the value of the codPuestoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPuestoRcp() {
        return codPuestoRcp;
    }

    /**
     * Sets the value of the codPuestoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPuestoRcp(String value) {
        this.codPuestoRcp = value;
    }

    /**
     * Gets the value of the codUoCentroDirectivoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoCentroDirectivoRcp() {
        return codUoCentroDirectivoRcp;
    }

    /**
     * Sets the value of the codUoCentroDirectivoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoCentroDirectivoRcp(String value) {
        this.codUoCentroDirectivoRcp = value;
    }

    /**
     * Gets the value of the codUoDestinoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoDestinoRcp() {
        return codUoDestinoRcp;
    }

    /**
     * Sets the value of the codUoDestinoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoDestinoRcp(String value) {
        this.codUoDestinoRcp = value;
    }

    /**
     * Gets the value of the codUoOrganismoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoOrganismoRcp() {
        return codUoOrganismoRcp;
    }

    /**
     * Sets the value of the codUoOrganismoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoOrganismoRcp(String value) {
        this.codUoOrganismoRcp = value;
    }

    /**
     * Gets the value of the denomCuerpoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomCuerpoRcp() {
        return denomCuerpoRcp;
    }

    /**
     * Sets the value of the denomCuerpoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomCuerpoRcp(String value) {
        this.denomCuerpoRcp = value;
    }

    /**
     * Gets the value of the denomPuestoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomPuestoRcp() {
        return denomPuestoRcp;
    }

    /**
     * Sets the value of the denomPuestoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomPuestoRcp(String value) {
        this.denomPuestoRcp = value;
    }

    /**
     * Gets the value of the denomUoCentroDirectivoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoCentroDirectivoRcp() {
        return denomUoCentroDirectivoRcp;
    }

    /**
     * Sets the value of the denomUoCentroDirectivoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoCentroDirectivoRcp(String value) {
        this.denomUoCentroDirectivoRcp = value;
    }

    /**
     * Gets the value of the denomUoDestinoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoDestinoRcp() {
        return denomUoDestinoRcp;
    }

    /**
     * Sets the value of the denomUoDestinoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoDestinoRcp(String value) {
        this.denomUoDestinoRcp = value;
    }

    /**
     * Gets the value of the denomUoOrganismoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoOrganismoRcp() {
        return denomUoOrganismoRcp;
    }

    /**
     * Sets the value of the denomUoOrganismoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoOrganismoRcp(String value) {
        this.denomUoOrganismoRcp = value;
    }

    /**
     * Gets the value of the descApellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido1() {
        return descApellido1;
    }

    /**
     * Sets the value of the descApellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido1(String value) {
        this.descApellido1 = value;
    }

    /**
     * Gets the value of the descApellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido2() {
        return descApellido2;
    }

    /**
     * Sets the value of the descApellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido2(String value) {
        this.descApellido2 = value;
    }

    /**
     * Gets the value of the descDesactiva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDesactiva() {
        return descDesactiva;
    }

    /**
     * Sets the value of the descDesactiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDesactiva(String value) {
        this.descDesactiva = value;
    }

    /**
     * Gets the value of the descEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEmail() {
        return descEmail;
    }

    /**
     * Sets the value of the descEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEmail(String value) {
        this.descEmail = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the fhUltimaActivacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhUltimaActivacion() {
        return fhUltimaActivacion;
    }

    /**
     * Sets the value of the fhUltimaActivacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhUltimaActivacion(XMLGregorianCalendar value) {
        this.fhUltimaActivacion = value;
    }

    /**
     * Gets the value of the fhUltimaBaja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhUltimaBaja() {
        return fhUltimaBaja;
    }

    /**
     * Sets the value of the fhUltimaBaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhUltimaBaja(XMLGregorianCalendar value) {
        this.fhUltimaBaja = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdFuncionario() {
        return idFuncionario;
    }

    /**
     * Sets the value of the idFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdFuncionario(Long value) {
        this.idFuncionario = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoDocumento(Integer value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the regNotificacionsForIdFuncionario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacionsForIdFuncionario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacionsForIdFuncionario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacionsForIdFuncionario() {
        if (regNotificacionsForIdFuncionario == null) {
            regNotificacionsForIdFuncionario = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacionsForIdFuncionario;
    }

    /**
     * Gets the value of the regNotificacionsForIdFuncionarioDestinatario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacionsForIdFuncionarioDestinatario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacionsForIdFuncionarioDestinatario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacionsForIdFuncionarioDestinatario() {
        if (regNotificacionsForIdFuncionarioDestinatario == null) {
            regNotificacionsForIdFuncionarioDestinatario = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacionsForIdFuncionarioDestinatario;
    }

    /**
     * Gets the value of the regOrganismoAdherido property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdherido() {
        return regOrganismoAdherido;
    }

    /**
     * Sets the value of the regOrganismoAdherido property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdherido(RegOrganismoAdherido value) {
        this.regOrganismoAdherido = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhCredencials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhCredencials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhCredencials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhCredencial }
     * 
     * 
     */
    public List<RfhCredencial> getRfhCredencials() {
        if (rfhCredencials == null) {
            rfhCredencials = new ArrayList<RfhCredencial>();
        }
        return this.rfhCredencials;
    }

    /**
     * Gets the value of the rfhHabilitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacions() {
        if (rfhHabilitacions == null) {
            rfhHabilitacions = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacions;
    }

    /**
     * Gets the value of the rfhTramitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhTramitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhTramitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhTramitacion }
     * 
     * 
     */
    public List<RfhTramitacion> getRfhTramitacions() {
        if (rfhTramitacions == null) {
            rfhTramitacions = new ArrayList<RfhTramitacion>();
        }
        return this.rfhTramitacions;
    }

}
