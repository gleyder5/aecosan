
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhHistFuncionario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhHistFuncionario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCuerpoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNifNie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPuestoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoCentroDirectivoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoDestinoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codUoOrganismoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomCuerpoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomPuestoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoCentroDirectivoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoDestinoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denomUoOrganismoRcp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDesactiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhArchivo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhBaja" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhUltimaActivacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idFuncionario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idHistFuncionario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idOrganismo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUsuarioArchivo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacions" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhHistFuncionario", propOrder = {
    "codCuerpoRcp",
    "codNifNie",
    "codPuestoRcp",
    "codUoCentroDirectivoRcp",
    "codUoDestinoRcp",
    "codUoOrganismoRcp",
    "denomCuerpoRcp",
    "denomPuestoRcp",
    "denomUoCentroDirectivoRcp",
    "denomUoDestinoRcp",
    "denomUoOrganismoRcp",
    "descApellido1",
    "descApellido2",
    "descDesactiva",
    "descEmail",
    "descNombre",
    "fhArchivo",
    "fhBaja",
    "fhCreacion",
    "fhUltimaActivacion",
    "idActivo",
    "idFuncionario",
    "idHistFuncionario",
    "idOrganismo",
    "idTipoDocumento",
    "idUsuarioArchivo",
    "idUsuarioCreacion",
    "rfhHabilitacions"
})
public class RfhHistFuncionario {

    protected String codCuerpoRcp;
    protected String codNifNie;
    protected String codPuestoRcp;
    protected String codUoCentroDirectivoRcp;
    protected String codUoDestinoRcp;
    protected String codUoOrganismoRcp;
    protected String denomCuerpoRcp;
    protected String denomPuestoRcp;
    protected String denomUoCentroDirectivoRcp;
    protected String denomUoDestinoRcp;
    protected String denomUoOrganismoRcp;
    protected String descApellido1;
    protected String descApellido2;
    protected String descDesactiva;
    protected String descEmail;
    protected String descNombre;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhArchivo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhBaja;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhUltimaActivacion;
    protected Boolean idActivo;
    protected Long idFuncionario;
    protected Long idHistFuncionario;
    protected Integer idOrganismo;
    protected Integer idTipoDocumento;
    protected Long idUsuarioArchivo;
    protected Long idUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacions;

    /**
     * Gets the value of the codCuerpoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCuerpoRcp() {
        return codCuerpoRcp;
    }

    /**
     * Sets the value of the codCuerpoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCuerpoRcp(String value) {
        this.codCuerpoRcp = value;
    }

    /**
     * Gets the value of the codNifNie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNifNie() {
        return codNifNie;
    }

    /**
     * Sets the value of the codNifNie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNifNie(String value) {
        this.codNifNie = value;
    }

    /**
     * Gets the value of the codPuestoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPuestoRcp() {
        return codPuestoRcp;
    }

    /**
     * Sets the value of the codPuestoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPuestoRcp(String value) {
        this.codPuestoRcp = value;
    }

    /**
     * Gets the value of the codUoCentroDirectivoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoCentroDirectivoRcp() {
        return codUoCentroDirectivoRcp;
    }

    /**
     * Sets the value of the codUoCentroDirectivoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoCentroDirectivoRcp(String value) {
        this.codUoCentroDirectivoRcp = value;
    }

    /**
     * Gets the value of the codUoDestinoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoDestinoRcp() {
        return codUoDestinoRcp;
    }

    /**
     * Sets the value of the codUoDestinoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoDestinoRcp(String value) {
        this.codUoDestinoRcp = value;
    }

    /**
     * Gets the value of the codUoOrganismoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUoOrganismoRcp() {
        return codUoOrganismoRcp;
    }

    /**
     * Sets the value of the codUoOrganismoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUoOrganismoRcp(String value) {
        this.codUoOrganismoRcp = value;
    }

    /**
     * Gets the value of the denomCuerpoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomCuerpoRcp() {
        return denomCuerpoRcp;
    }

    /**
     * Sets the value of the denomCuerpoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomCuerpoRcp(String value) {
        this.denomCuerpoRcp = value;
    }

    /**
     * Gets the value of the denomPuestoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomPuestoRcp() {
        return denomPuestoRcp;
    }

    /**
     * Sets the value of the denomPuestoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomPuestoRcp(String value) {
        this.denomPuestoRcp = value;
    }

    /**
     * Gets the value of the denomUoCentroDirectivoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoCentroDirectivoRcp() {
        return denomUoCentroDirectivoRcp;
    }

    /**
     * Sets the value of the denomUoCentroDirectivoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoCentroDirectivoRcp(String value) {
        this.denomUoCentroDirectivoRcp = value;
    }

    /**
     * Gets the value of the denomUoDestinoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoDestinoRcp() {
        return denomUoDestinoRcp;
    }

    /**
     * Sets the value of the denomUoDestinoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoDestinoRcp(String value) {
        this.denomUoDestinoRcp = value;
    }

    /**
     * Gets the value of the denomUoOrganismoRcp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenomUoOrganismoRcp() {
        return denomUoOrganismoRcp;
    }

    /**
     * Sets the value of the denomUoOrganismoRcp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenomUoOrganismoRcp(String value) {
        this.denomUoOrganismoRcp = value;
    }

    /**
     * Gets the value of the descApellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido1() {
        return descApellido1;
    }

    /**
     * Sets the value of the descApellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido1(String value) {
        this.descApellido1 = value;
    }

    /**
     * Gets the value of the descApellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido2() {
        return descApellido2;
    }

    /**
     * Sets the value of the descApellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido2(String value) {
        this.descApellido2 = value;
    }

    /**
     * Gets the value of the descDesactiva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDesactiva() {
        return descDesactiva;
    }

    /**
     * Sets the value of the descDesactiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDesactiva(String value) {
        this.descDesactiva = value;
    }

    /**
     * Gets the value of the descEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEmail() {
        return descEmail;
    }

    /**
     * Sets the value of the descEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEmail(String value) {
        this.descEmail = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the fhArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhArchivo() {
        return fhArchivo;
    }

    /**
     * Sets the value of the fhArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhArchivo(XMLGregorianCalendar value) {
        this.fhArchivo = value;
    }

    /**
     * Gets the value of the fhBaja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhBaja() {
        return fhBaja;
    }

    /**
     * Sets the value of the fhBaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhBaja(XMLGregorianCalendar value) {
        this.fhBaja = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhUltimaActivacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhUltimaActivacion() {
        return fhUltimaActivacion;
    }

    /**
     * Sets the value of the fhUltimaActivacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhUltimaActivacion(XMLGregorianCalendar value) {
        this.fhUltimaActivacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdFuncionario() {
        return idFuncionario;
    }

    /**
     * Sets the value of the idFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdFuncionario(Long value) {
        this.idFuncionario = value;
    }

    /**
     * Gets the value of the idHistFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistFuncionario() {
        return idHistFuncionario;
    }

    /**
     * Sets the value of the idHistFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistFuncionario(Long value) {
        this.idHistFuncionario = value;
    }

    /**
     * Gets the value of the idOrganismo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdOrganismo() {
        return idOrganismo;
    }

    /**
     * Sets the value of the idOrganismo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdOrganismo(Integer value) {
        this.idOrganismo = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoDocumento(Integer value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the idUsuarioArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioArchivo() {
        return idUsuarioArchivo;
    }

    /**
     * Sets the value of the idUsuarioArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioArchivo(Long value) {
        this.idUsuarioArchivo = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the rfhHabilitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacions() {
        if (rfhHabilitacions == null) {
            rfhHabilitacions = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacions;
    }

}
