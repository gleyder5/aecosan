
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reaCatTipodocEni complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaCatTipodocEni">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipodocEni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reaAnexoRepresentacions" type="{http://ws.funcionario.map.es/}reaAnexoRepresentacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reaAnexos" type="{http://ws.funcionario.map.es/}reaAnexo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaCatTipodocEni", propOrder = {
    "descDescripcion",
    "descNombre",
    "idTipodocEni",
    "reaAnexoRepresentacions",
    "reaAnexos"
})
public class ReaCatTipodocEni {

    protected String descDescripcion;
    protected String descNombre;
    protected String idTipodocEni;
    @XmlElement(nillable = true)
    protected List<ReaAnexoRepresentacion> reaAnexoRepresentacions;
    @XmlElement(nillable = true)
    protected List<ReaAnexo> reaAnexos;

    /**
     * Gets the value of the descDescripcion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDescripcion() {
        return descDescripcion;
    }

    /**
     * Sets the value of the descDescripcion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDescripcion(String value) {
        this.descDescripcion = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the idTipodocEni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipodocEni() {
        return idTipodocEni;
    }

    /**
     * Sets the value of the idTipodocEni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipodocEni(String value) {
        this.idTipodocEni = value;
    }

    /**
     * Gets the value of the reaAnexoRepresentacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAnexoRepresentacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAnexoRepresentacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexoRepresentacion }
     * 
     * 
     */
    public List<ReaAnexoRepresentacion> getReaAnexoRepresentacions() {
        if (reaAnexoRepresentacions == null) {
            reaAnexoRepresentacions = new ArrayList<ReaAnexoRepresentacion>();
        }
        return this.reaAnexoRepresentacions;
    }

    /**
     * Gets the value of the reaAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reaAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReaAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReaAnexo }
     * 
     * 
     */
    public List<ReaAnexo> getReaAnexos() {
        if (reaAnexos == null) {
            reaAnexos = new ArrayList<ReaAnexo>();
        }
        return this.reaAnexos;
    }

}
