
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for renunciarApoderamientoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="renunciarApoderamientoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.funcionario.map.es/}renunciarApoderamientoWSResponseSalida" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "renunciarApoderamientoResponse", propOrder = {
    "_return"
})
public class RenunciarApoderamientoResponse {

    @XmlElement(name = "return", namespace = "http://ws.funcionario.map.es/")
    protected RenunciarApoderamientoWSResponseSalida _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link RenunciarApoderamientoWSResponseSalida }
     *     
     */
    public RenunciarApoderamientoWSResponseSalida getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link RenunciarApoderamientoWSResponseSalida }
     *     
     */
    public void setReturn(RenunciarApoderamientoWSResponseSalida value) {
        this._return = value;
    }

}
