
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regNanTramiteCategoria complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regNanTramiteCategoria">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://ws.funcionario.map.es/}regNanTramiteCategoriaId" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="regCategoria" type="{http://ws.funcionario.map.es/}regCategoria" minOccurs="0"/>
 *         &lt;element name="regTramite" type="{http://ws.funcionario.map.es/}regTramite" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regNanTramiteCategoria", propOrder = {
    "fhCreacion",
    "fhModificacion",
    "id",
    "idActivo",
    "regCategoria",
    "regTramite",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion"
})
public class RegNanTramiteCategoria {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected RegNanTramiteCategoriaId id;
    protected Boolean idActivo;
    protected RegCategoria regCategoria;
    protected RegTramite regTramite;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link RegNanTramiteCategoriaId }
     *     
     */
    public RegNanTramiteCategoriaId getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegNanTramiteCategoriaId }
     *     
     */
    public void setId(RegNanTramiteCategoriaId value) {
        this.id = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the regCategoria property.
     * 
     * @return
     *     possible object is
     *     {@link RegCategoria }
     *     
     */
    public RegCategoria getRegCategoria() {
        return regCategoria;
    }

    /**
     * Sets the value of the regCategoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCategoria }
     *     
     */
    public void setRegCategoria(RegCategoria value) {
        this.regCategoria = value;
    }

    /**
     * Gets the value of the regTramite property.
     * 
     * @return
     *     possible object is
     *     {@link RegTramite }
     *     
     */
    public RegTramite getRegTramite() {
        return regTramite;
    }

    /**
     * Sets the value of the regTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTramite }
     *     
     */
    public void setRegTramite(RegTramite value) {
        this.regTramite = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

}
