
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhTramitacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhTramitacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codRegistroRec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhFirma" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhRegistroRec" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhTramitacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idDescargaFormulario" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idEstTramitacion" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idHabilitacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idSeqTramita" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="regTramite" type="{http://ws.funcionario.map.es/}regTramite" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhCiudadano" type="{http://ws.funcionario.map.es/}rfhCiudadano" minOccurs="0"/>
 *         &lt;element name="rfhFuncionario" type="{http://ws.funcionario.map.es/}rfhFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhHistTramitacions" type="{http://ws.funcionario.map.es/}rfhHistTramitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhTramitacion", propOrder = {
    "codRegistroRec",
    "descObservaciones",
    "fhCreacion",
    "fhFirma",
    "fhModificacion",
    "fhRegistroRec",
    "fhTramitacion",
    "idActivo",
    "idDescargaFormulario",
    "idEstTramitacion",
    "idHabilitacion",
    "idSeqTramita",
    "regTramite",
    "regUsuarioByIdUsuarioCreacion",
    "regUsuarioByIdUsuarioModificacion",
    "rfhCiudadano",
    "rfhFuncionario",
    "rfhHistTramitacions"
})
public class RfhTramitacion {

    protected String codRegistroRec;
    protected String descObservaciones;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhFirma;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhRegistroRec;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhTramitacion;
    protected Boolean idActivo;
    protected Boolean idDescargaFormulario;
    protected Integer idEstTramitacion;
    protected Long idHabilitacion;
    protected Long idSeqTramita;
    protected RegTramite regTramite;
    protected RegUsuario regUsuarioByIdUsuarioCreacion;
    protected RegUsuario regUsuarioByIdUsuarioModificacion;
    protected RfhCiudadano rfhCiudadano;
    protected RfhFuncionario rfhFuncionario;
    @XmlElement(nillable = true)
    protected List<RfhHistTramitacion> rfhHistTramitacions;

    /**
     * Gets the value of the codRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegistroRec() {
        return codRegistroRec;
    }

    /**
     * Sets the value of the codRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegistroRec(String value) {
        this.codRegistroRec = value;
    }

    /**
     * Gets the value of the descObservaciones property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Sets the value of the descObservaciones property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhFirma property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhFirma() {
        return fhFirma;
    }

    /**
     * Sets the value of the fhFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhFirma(XMLGregorianCalendar value) {
        this.fhFirma = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the fhRegistroRec property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhRegistroRec() {
        return fhRegistroRec;
    }

    /**
     * Sets the value of the fhRegistroRec property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhRegistroRec(XMLGregorianCalendar value) {
        this.fhRegistroRec = value;
    }

    /**
     * Gets the value of the fhTramitacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhTramitacion() {
        return fhTramitacion;
    }

    /**
     * Sets the value of the fhTramitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhTramitacion(XMLGregorianCalendar value) {
        this.fhTramitacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idDescargaFormulario property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdDescargaFormulario() {
        return idDescargaFormulario;
    }

    /**
     * Sets the value of the idDescargaFormulario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdDescargaFormulario(Boolean value) {
        this.idDescargaFormulario = value;
    }

    /**
     * Gets the value of the idEstTramitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstTramitacion() {
        return idEstTramitacion;
    }

    /**
     * Sets the value of the idEstTramitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstTramitacion(Integer value) {
        this.idEstTramitacion = value;
    }

    /**
     * Gets the value of the idHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHabilitacion() {
        return idHabilitacion;
    }

    /**
     * Sets the value of the idHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHabilitacion(Long value) {
        this.idHabilitacion = value;
    }

    /**
     * Gets the value of the idSeqTramita property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdSeqTramita() {
        return idSeqTramita;
    }

    /**
     * Sets the value of the idSeqTramita property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdSeqTramita(Long value) {
        this.idSeqTramita = value;
    }

    /**
     * Gets the value of the regTramite property.
     * 
     * @return
     *     possible object is
     *     {@link RegTramite }
     *     
     */
    public RegTramite getRegTramite() {
        return regTramite;
    }

    /**
     * Sets the value of the regTramite property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTramite }
     *     
     */
    public void setRegTramite(RegTramite value) {
        this.regTramite = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioCreacion() {
        return regUsuarioByIdUsuarioCreacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioCreacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioCreacion = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioModificacion() {
        return regUsuarioByIdUsuarioModificacion;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioModificacion(RegUsuario value) {
        this.regUsuarioByIdUsuarioModificacion = value;
    }

    /**
     * Gets the value of the rfhCiudadano property.
     * 
     * @return
     *     possible object is
     *     {@link RfhCiudadano }
     *     
     */
    public RfhCiudadano getRfhCiudadano() {
        return rfhCiudadano;
    }

    /**
     * Sets the value of the rfhCiudadano property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhCiudadano }
     *     
     */
    public void setRfhCiudadano(RfhCiudadano value) {
        this.rfhCiudadano = value;
    }

    /**
     * Gets the value of the rfhFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhFuncionario }
     *     
     */
    public RfhFuncionario getRfhFuncionario() {
        return rfhFuncionario;
    }

    /**
     * Sets the value of the rfhFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhFuncionario }
     *     
     */
    public void setRfhFuncionario(RfhFuncionario value) {
        this.rfhFuncionario = value;
    }

    /**
     * Gets the value of the rfhHistTramitacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHistTramitacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHistTramitacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHistTramitacion }
     * 
     * 
     */
    public List<RfhHistTramitacion> getRfhHistTramitacions() {
        if (rfhHistTramitacions == null) {
            rfhHistTramitacions = new ArrayList<RfhHistTramitacion>();
        }
        return this.rfhHistTramitacions;
    }

}
