
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for regCatTipoMetodoNot complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regCatTipoMetodoNot">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://ws.funcionario.map.es/}regCatTipoMetodoNotId" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="regCatMetodoNotificacion" type="{http://ws.funcionario.map.es/}regCatMetodoNotificacion" minOccurs="0"/>
 *         &lt;element name="regCatTipoNotificacion" type="{http://ws.funcionario.map.es/}regCatTipoNotificacion" minOccurs="0"/>
 *         &lt;element name="regNotificacions" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regCatTipoMetodoNot", propOrder = {
    "id",
    "idActivo",
    "regCatMetodoNotificacion",
    "regCatTipoNotificacion",
    "regNotificacions"
})
public class RegCatTipoMetodoNot {

    protected RegCatTipoMetodoNotId id;
    protected Boolean idActivo;
    protected RegCatMetodoNotificacion regCatMetodoNotificacion;
    protected RegCatTipoNotificacion regCatTipoNotificacion;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacions;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link RegCatTipoMetodoNotId }
     *     
     */
    public RegCatTipoMetodoNotId getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCatTipoMetodoNotId }
     *     
     */
    public void setId(RegCatTipoMetodoNotId value) {
        this.id = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the regCatMetodoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegCatMetodoNotificacion }
     *     
     */
    public RegCatMetodoNotificacion getRegCatMetodoNotificacion() {
        return regCatMetodoNotificacion;
    }

    /**
     * Sets the value of the regCatMetodoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCatMetodoNotificacion }
     *     
     */
    public void setRegCatMetodoNotificacion(RegCatMetodoNotificacion value) {
        this.regCatMetodoNotificacion = value;
    }

    /**
     * Gets the value of the regCatTipoNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link RegCatTipoNotificacion }
     *     
     */
    public RegCatTipoNotificacion getRegCatTipoNotificacion() {
        return regCatTipoNotificacion;
    }

    /**
     * Sets the value of the regCatTipoNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCatTipoNotificacion }
     *     
     */
    public void setRegCatTipoNotificacion(RegCatTipoNotificacion value) {
        this.regCatTipoNotificacion = value;
    }

    /**
     * Gets the value of the regNotificacions property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacions property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacions().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacions() {
        if (regNotificacions == null) {
            regNotificacions = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacions;
    }

}
