
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regUsuario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regUsuario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codNifNie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descApellido2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAplicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCargo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCiudad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDepartamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descOrganizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descPais" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descProvincia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="regCategoriasForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regCategoriasForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanApodCategoriasForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regNanApodCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanApodCategoriasForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regNanApodCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanTramiteCategoriasForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regNanTramiteCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanTramiteCategoriasForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regNanTramiteCategoria" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioOrganismosForIdUsuario" type="{http://ws.funcionario.map.es/}regNanUsuarioOrganismo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioOrganismosForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regNanUsuarioOrganismo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioOrganismosForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regNanUsuarioOrganismo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioPermisosForIdUsuario" type="{http://ws.funcionario.map.es/}regNanUsuarioPermiso" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioPermisosForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regNanUsuarioPermiso" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNanUsuarioPermisosForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regNanUsuarioPermiso" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNotificacionsForIdUsuarioDestinatario" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regNotificacionsForIdUsuarioOrigen" type="{http://ws.funcionario.map.es/}regNotificacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdheridosForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdheridosForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regTramitesForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}regTramite" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="regTramitesForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}regTramite" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhCredencials" type="{http://ws.funcionario.map.es/}rfhCredencial" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhFuncionariosForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}rfhFuncionario" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhFuncionariosForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}rfhFuncionario" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacionsForIdUsuarioCreacion" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacionsForIdUsuarioModificacion" type="{http://ws.funcionario.map.es/}rfhHabilitacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regUsuario", propOrder = {
    "codNifNie",
    "descApellido1",
    "descApellido2",
    "descAplicacion",
    "descCargo",
    "descCiudad",
    "descDepartamento",
    "descEmail",
    "descNombre",
    "descOrganizacion",
    "descPais",
    "descProvincia",
    "descTelefono",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idTipoDocumento",
    "idUsuario",
    "idUsuarioCreacion",
    "idUsuarioModificacion",
    "regCategoriasForIdUsuarioCreacion",
    "regCategoriasForIdUsuarioModificacion",
    "regNanApodCategoriasForIdUsuarioCreacion",
    "regNanApodCategoriasForIdUsuarioModificacion",
    "regNanTramiteCategoriasForIdUsuarioCreacion",
    "regNanTramiteCategoriasForIdUsuarioModificacion",
    "regNanUsuarioOrganismosForIdUsuario",
    "regNanUsuarioOrganismosForIdUsuarioCreacion",
    "regNanUsuarioOrganismosForIdUsuarioModificacion",
    "regNanUsuarioPermisosForIdUsuario",
    "regNanUsuarioPermisosForIdUsuarioCreacion",
    "regNanUsuarioPermisosForIdUsuarioModificacion",
    "regNotificacionsForIdUsuarioDestinatario",
    "regNotificacionsForIdUsuarioOrigen",
    "regOrganismoAdheridosForIdUsuarioCreacion",
    "regOrganismoAdheridosForIdUsuarioModificacion",
    "regTramitesForIdUsuarioCreacion",
    "regTramitesForIdUsuarioModificacion",
    "rfhCredencials",
    "rfhFuncionariosForIdUsuarioCreacion",
    "rfhFuncionariosForIdUsuarioModificacion",
    "rfhHabilitacionsForIdUsuarioCreacion",
    "rfhHabilitacionsForIdUsuarioModificacion"
})
public class RegUsuario {

    protected String codNifNie;
    protected String descApellido1;
    protected String descApellido2;
    protected String descAplicacion;
    protected String descCargo;
    protected String descCiudad;
    protected String descDepartamento;
    protected String descEmail;
    protected String descNombre;
    protected String descOrganizacion;
    protected String descPais;
    protected String descProvincia;
    protected String descTelefono;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Integer idTipoDocumento;
    protected Long idUsuario;
    protected Long idUsuarioCreacion;
    protected Long idUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegCategoria> regCategoriasForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegCategoria> regCategoriasForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegNanApodCategoria> regNanApodCategoriasForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegNanApodCategoria> regNanApodCategoriasForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegNanTramiteCategoria> regNanTramiteCategoriasForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegNanTramiteCategoria> regNanTramiteCategoriasForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioOrganismo> regNanUsuarioOrganismosForIdUsuario;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioOrganismo> regNanUsuarioOrganismosForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioOrganismo> regNanUsuarioOrganismosForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioPermiso> regNanUsuarioPermisosForIdUsuario;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioPermiso> regNanUsuarioPermisosForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegNanUsuarioPermiso> regNanUsuarioPermisosForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacionsForIdUsuarioDestinatario;
    @XmlElement(nillable = true)
    protected List<RegNotificacion> regNotificacionsForIdUsuarioOrigen;
    @XmlElement(nillable = true)
    protected List<RegOrganismoAdherido> regOrganismoAdheridosForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegOrganismoAdherido> regOrganismoAdheridosForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RegTramite> regTramitesForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RegTramite> regTramitesForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhCredencial> rfhCredencials;
    @XmlElement(nillable = true)
    protected List<RfhFuncionario> rfhFuncionariosForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RfhFuncionario> rfhFuncionariosForIdUsuarioModificacion;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacionsForIdUsuarioCreacion;
    @XmlElement(nillable = true)
    protected List<RfhHabilitacion> rfhHabilitacionsForIdUsuarioModificacion;

    /**
     * Gets the value of the codNifNie property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNifNie() {
        return codNifNie;
    }

    /**
     * Sets the value of the codNifNie property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNifNie(String value) {
        this.codNifNie = value;
    }

    /**
     * Gets the value of the descApellido1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido1() {
        return descApellido1;
    }

    /**
     * Sets the value of the descApellido1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido1(String value) {
        this.descApellido1 = value;
    }

    /**
     * Gets the value of the descApellido2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescApellido2() {
        return descApellido2;
    }

    /**
     * Sets the value of the descApellido2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescApellido2(String value) {
        this.descApellido2 = value;
    }

    /**
     * Gets the value of the descAplicacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAplicacion() {
        return descAplicacion;
    }

    /**
     * Sets the value of the descAplicacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAplicacion(String value) {
        this.descAplicacion = value;
    }

    /**
     * Gets the value of the descCargo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCargo() {
        return descCargo;
    }

    /**
     * Sets the value of the descCargo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCargo(String value) {
        this.descCargo = value;
    }

    /**
     * Gets the value of the descCiudad property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCiudad() {
        return descCiudad;
    }

    /**
     * Sets the value of the descCiudad property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCiudad(String value) {
        this.descCiudad = value;
    }

    /**
     * Gets the value of the descDepartamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDepartamento() {
        return descDepartamento;
    }

    /**
     * Sets the value of the descDepartamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDepartamento(String value) {
        this.descDepartamento = value;
    }

    /**
     * Gets the value of the descEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEmail() {
        return descEmail;
    }

    /**
     * Sets the value of the descEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEmail(String value) {
        this.descEmail = value;
    }

    /**
     * Gets the value of the descNombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNombre() {
        return descNombre;
    }

    /**
     * Sets the value of the descNombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNombre(String value) {
        this.descNombre = value;
    }

    /**
     * Gets the value of the descOrganizacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescOrganizacion() {
        return descOrganizacion;
    }

    /**
     * Sets the value of the descOrganizacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescOrganizacion(String value) {
        this.descOrganizacion = value;
    }

    /**
     * Gets the value of the descPais property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescPais() {
        return descPais;
    }

    /**
     * Sets the value of the descPais property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescPais(String value) {
        this.descPais = value;
    }

    /**
     * Gets the value of the descProvincia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescProvincia() {
        return descProvincia;
    }

    /**
     * Sets the value of the descProvincia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescProvincia(String value) {
        this.descProvincia = value;
    }

    /**
     * Gets the value of the descTelefono property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTelefono() {
        return descTelefono;
    }

    /**
     * Sets the value of the descTelefono property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTelefono(String value) {
        this.descTelefono = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdTipoDocumento() {
        return idTipoDocumento;
    }

    /**
     * Sets the value of the idTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdTipoDocumento(Integer value) {
        this.idTipoDocumento = value;
    }

    /**
     * Gets the value of the idUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuario() {
        return idUsuario;
    }

    /**
     * Sets the value of the idUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuario(Long value) {
        this.idUsuario = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

    /**
     * Gets the value of the idUsuarioModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    /**
     * Sets the value of the idUsuarioModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioModificacion(Long value) {
        this.idUsuarioModificacion = value;
    }

    /**
     * Gets the value of the regCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCategoriasForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCategoria }
     * 
     * 
     */
    public List<RegCategoria> getRegCategoriasForIdUsuarioCreacion() {
        if (regCategoriasForIdUsuarioCreacion == null) {
            regCategoriasForIdUsuarioCreacion = new ArrayList<RegCategoria>();
        }
        return this.regCategoriasForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegCategoriasForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegCategoria }
     * 
     * 
     */
    public List<RegCategoria> getRegCategoriasForIdUsuarioModificacion() {
        if (regCategoriasForIdUsuarioModificacion == null) {
            regCategoriasForIdUsuarioModificacion = new ArrayList<RegCategoria>();
        }
        return this.regCategoriasForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regNanApodCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanApodCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanApodCategoriasForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanApodCategoria }
     * 
     * 
     */
    public List<RegNanApodCategoria> getRegNanApodCategoriasForIdUsuarioCreacion() {
        if (regNanApodCategoriasForIdUsuarioCreacion == null) {
            regNanApodCategoriasForIdUsuarioCreacion = new ArrayList<RegNanApodCategoria>();
        }
        return this.regNanApodCategoriasForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regNanApodCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanApodCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanApodCategoriasForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanApodCategoria }
     * 
     * 
     */
    public List<RegNanApodCategoria> getRegNanApodCategoriasForIdUsuarioModificacion() {
        if (regNanApodCategoriasForIdUsuarioModificacion == null) {
            regNanApodCategoriasForIdUsuarioModificacion = new ArrayList<RegNanApodCategoria>();
        }
        return this.regNanApodCategoriasForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regNanTramiteCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanTramiteCategoriasForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanTramiteCategoriasForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanTramiteCategoria }
     * 
     * 
     */
    public List<RegNanTramiteCategoria> getRegNanTramiteCategoriasForIdUsuarioCreacion() {
        if (regNanTramiteCategoriasForIdUsuarioCreacion == null) {
            regNanTramiteCategoriasForIdUsuarioCreacion = new ArrayList<RegNanTramiteCategoria>();
        }
        return this.regNanTramiteCategoriasForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regNanTramiteCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanTramiteCategoriasForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanTramiteCategoriasForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanTramiteCategoria }
     * 
     * 
     */
    public List<RegNanTramiteCategoria> getRegNanTramiteCategoriasForIdUsuarioModificacion() {
        if (regNanTramiteCategoriasForIdUsuarioModificacion == null) {
            regNanTramiteCategoriasForIdUsuarioModificacion = new ArrayList<RegNanTramiteCategoria>();
        }
        return this.regNanTramiteCategoriasForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regNanUsuarioOrganismosForIdUsuario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioOrganismosForIdUsuario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioOrganismosForIdUsuario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioOrganismo }
     * 
     * 
     */
    public List<RegNanUsuarioOrganismo> getRegNanUsuarioOrganismosForIdUsuario() {
        if (regNanUsuarioOrganismosForIdUsuario == null) {
            regNanUsuarioOrganismosForIdUsuario = new ArrayList<RegNanUsuarioOrganismo>();
        }
        return this.regNanUsuarioOrganismosForIdUsuario;
    }

    /**
     * Gets the value of the regNanUsuarioOrganismosForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioOrganismosForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioOrganismosForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioOrganismo }
     * 
     * 
     */
    public List<RegNanUsuarioOrganismo> getRegNanUsuarioOrganismosForIdUsuarioCreacion() {
        if (regNanUsuarioOrganismosForIdUsuarioCreacion == null) {
            regNanUsuarioOrganismosForIdUsuarioCreacion = new ArrayList<RegNanUsuarioOrganismo>();
        }
        return this.regNanUsuarioOrganismosForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regNanUsuarioOrganismosForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioOrganismosForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioOrganismosForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioOrganismo }
     * 
     * 
     */
    public List<RegNanUsuarioOrganismo> getRegNanUsuarioOrganismosForIdUsuarioModificacion() {
        if (regNanUsuarioOrganismosForIdUsuarioModificacion == null) {
            regNanUsuarioOrganismosForIdUsuarioModificacion = new ArrayList<RegNanUsuarioOrganismo>();
        }
        return this.regNanUsuarioOrganismosForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regNanUsuarioPermisosForIdUsuario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioPermisosForIdUsuario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioPermisosForIdUsuario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioPermiso }
     * 
     * 
     */
    public List<RegNanUsuarioPermiso> getRegNanUsuarioPermisosForIdUsuario() {
        if (regNanUsuarioPermisosForIdUsuario == null) {
            regNanUsuarioPermisosForIdUsuario = new ArrayList<RegNanUsuarioPermiso>();
        }
        return this.regNanUsuarioPermisosForIdUsuario;
    }

    /**
     * Gets the value of the regNanUsuarioPermisosForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioPermisosForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioPermisosForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioPermiso }
     * 
     * 
     */
    public List<RegNanUsuarioPermiso> getRegNanUsuarioPermisosForIdUsuarioCreacion() {
        if (regNanUsuarioPermisosForIdUsuarioCreacion == null) {
            regNanUsuarioPermisosForIdUsuarioCreacion = new ArrayList<RegNanUsuarioPermiso>();
        }
        return this.regNanUsuarioPermisosForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regNanUsuarioPermisosForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNanUsuarioPermisosForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNanUsuarioPermisosForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNanUsuarioPermiso }
     * 
     * 
     */
    public List<RegNanUsuarioPermiso> getRegNanUsuarioPermisosForIdUsuarioModificacion() {
        if (regNanUsuarioPermisosForIdUsuarioModificacion == null) {
            regNanUsuarioPermisosForIdUsuarioModificacion = new ArrayList<RegNanUsuarioPermiso>();
        }
        return this.regNanUsuarioPermisosForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regNotificacionsForIdUsuarioDestinatario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacionsForIdUsuarioDestinatario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacionsForIdUsuarioDestinatario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacionsForIdUsuarioDestinatario() {
        if (regNotificacionsForIdUsuarioDestinatario == null) {
            regNotificacionsForIdUsuarioDestinatario = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacionsForIdUsuarioDestinatario;
    }

    /**
     * Gets the value of the regNotificacionsForIdUsuarioOrigen property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regNotificacionsForIdUsuarioOrigen property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegNotificacionsForIdUsuarioOrigen().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegNotificacion }
     * 
     * 
     */
    public List<RegNotificacion> getRegNotificacionsForIdUsuarioOrigen() {
        if (regNotificacionsForIdUsuarioOrigen == null) {
            regNotificacionsForIdUsuarioOrigen = new ArrayList<RegNotificacion>();
        }
        return this.regNotificacionsForIdUsuarioOrigen;
    }

    /**
     * Gets the value of the regOrganismoAdheridosForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regOrganismoAdheridosForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegOrganismoAdheridosForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegOrganismoAdherido }
     * 
     * 
     */
    public List<RegOrganismoAdherido> getRegOrganismoAdheridosForIdUsuarioCreacion() {
        if (regOrganismoAdheridosForIdUsuarioCreacion == null) {
            regOrganismoAdheridosForIdUsuarioCreacion = new ArrayList<RegOrganismoAdherido>();
        }
        return this.regOrganismoAdheridosForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regOrganismoAdheridosForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regOrganismoAdheridosForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegOrganismoAdheridosForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegOrganismoAdherido }
     * 
     * 
     */
    public List<RegOrganismoAdherido> getRegOrganismoAdheridosForIdUsuarioModificacion() {
        if (regOrganismoAdheridosForIdUsuarioModificacion == null) {
            regOrganismoAdheridosForIdUsuarioModificacion = new ArrayList<RegOrganismoAdherido>();
        }
        return this.regOrganismoAdheridosForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the regTramitesForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regTramitesForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegTramitesForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegTramite }
     * 
     * 
     */
    public List<RegTramite> getRegTramitesForIdUsuarioCreacion() {
        if (regTramitesForIdUsuarioCreacion == null) {
            regTramitesForIdUsuarioCreacion = new ArrayList<RegTramite>();
        }
        return this.regTramitesForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the regTramitesForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the regTramitesForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRegTramitesForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegTramite }
     * 
     * 
     */
    public List<RegTramite> getRegTramitesForIdUsuarioModificacion() {
        if (regTramitesForIdUsuarioModificacion == null) {
            regTramitesForIdUsuarioModificacion = new ArrayList<RegTramite>();
        }
        return this.regTramitesForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the rfhCredencials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhCredencials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhCredencials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhCredencial }
     * 
     * 
     */
    public List<RfhCredencial> getRfhCredencials() {
        if (rfhCredencials == null) {
            rfhCredencials = new ArrayList<RfhCredencial>();
        }
        return this.rfhCredencials;
    }

    /**
     * Gets the value of the rfhFuncionariosForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhFuncionariosForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhFuncionariosForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhFuncionario }
     * 
     * 
     */
    public List<RfhFuncionario> getRfhFuncionariosForIdUsuarioCreacion() {
        if (rfhFuncionariosForIdUsuarioCreacion == null) {
            rfhFuncionariosForIdUsuarioCreacion = new ArrayList<RfhFuncionario>();
        }
        return this.rfhFuncionariosForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the rfhFuncionariosForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhFuncionariosForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhFuncionariosForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhFuncionario }
     * 
     * 
     */
    public List<RfhFuncionario> getRfhFuncionariosForIdUsuarioModificacion() {
        if (rfhFuncionariosForIdUsuarioModificacion == null) {
            rfhFuncionariosForIdUsuarioModificacion = new ArrayList<RfhFuncionario>();
        }
        return this.rfhFuncionariosForIdUsuarioModificacion;
    }

    /**
     * Gets the value of the rfhHabilitacionsForIdUsuarioCreacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacionsForIdUsuarioCreacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacionsForIdUsuarioCreacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacionsForIdUsuarioCreacion() {
        if (rfhHabilitacionsForIdUsuarioCreacion == null) {
            rfhHabilitacionsForIdUsuarioCreacion = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacionsForIdUsuarioCreacion;
    }

    /**
     * Gets the value of the rfhHabilitacionsForIdUsuarioModificacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhHabilitacionsForIdUsuarioModificacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhHabilitacionsForIdUsuarioModificacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhHabilitacion }
     * 
     * 
     */
    public List<RfhHabilitacion> getRfhHabilitacionsForIdUsuarioModificacion() {
        if (rfhHabilitacionsForIdUsuarioModificacion == null) {
            rfhHabilitacionsForIdUsuarioModificacion = new ArrayList<RfhHabilitacion>();
        }
        return this.rfhHabilitacionsForIdUsuarioModificacion;
    }

}
