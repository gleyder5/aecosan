
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for reaHistRepresentacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reaHistRepresentacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fhArchivo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idDelegacion" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idEstado" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="idHistRepresentacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idPersonaFisica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idPersonaFisicaRep" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idPersonaJuridica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idRepresentacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioArchivo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reaHistRepresentacion", propOrder = {
    "fhArchivo",
    "fhCreacion",
    "idDelegacion",
    "idEstado",
    "idHistRepresentacion",
    "idPersonaFisica",
    "idPersonaFisicaRep",
    "idPersonaJuridica",
    "idRepresentacion",
    "idUsuarioArchivo",
    "idUsuarioCreacion"
})
public class ReaHistRepresentacion {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhArchivo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    protected Boolean idDelegacion;
    protected Integer idEstado;
    protected Long idHistRepresentacion;
    protected Long idPersonaFisica;
    protected Long idPersonaFisicaRep;
    protected Long idPersonaJuridica;
    protected Long idRepresentacion;
    protected Long idUsuarioArchivo;
    protected Long idUsuarioCreacion;

    /**
     * Gets the value of the fhArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhArchivo() {
        return fhArchivo;
    }

    /**
     * Sets the value of the fhArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhArchivo(XMLGregorianCalendar value) {
        this.fhArchivo = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the idDelegacion property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdDelegacion() {
        return idDelegacion;
    }

    /**
     * Sets the value of the idDelegacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdDelegacion(Boolean value) {
        this.idDelegacion = value;
    }

    /**
     * Gets the value of the idEstado property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIdEstado() {
        return idEstado;
    }

    /**
     * Sets the value of the idEstado property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIdEstado(Integer value) {
        this.idEstado = value;
    }

    /**
     * Gets the value of the idHistRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdHistRepresentacion() {
        return idHistRepresentacion;
    }

    /**
     * Sets the value of the idHistRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdHistRepresentacion(Long value) {
        this.idHistRepresentacion = value;
    }

    /**
     * Gets the value of the idPersonaFisica property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPersonaFisica() {
        return idPersonaFisica;
    }

    /**
     * Sets the value of the idPersonaFisica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPersonaFisica(Long value) {
        this.idPersonaFisica = value;
    }

    /**
     * Gets the value of the idPersonaFisicaRep property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPersonaFisicaRep() {
        return idPersonaFisicaRep;
    }

    /**
     * Sets the value of the idPersonaFisicaRep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPersonaFisicaRep(Long value) {
        this.idPersonaFisicaRep = value;
    }

    /**
     * Gets the value of the idPersonaJuridica property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPersonaJuridica() {
        return idPersonaJuridica;
    }

    /**
     * Sets the value of the idPersonaJuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPersonaJuridica(Long value) {
        this.idPersonaJuridica = value;
    }

    /**
     * Gets the value of the idRepresentacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdRepresentacion() {
        return idRepresentacion;
    }

    /**
     * Sets the value of the idRepresentacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdRepresentacion(Long value) {
        this.idRepresentacion = value;
    }

    /**
     * Gets the value of the idUsuarioArchivo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioArchivo() {
        return idUsuarioArchivo;
    }

    /**
     * Sets the value of the idUsuarioArchivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioArchivo(Long value) {
        this.idUsuarioArchivo = value;
    }

    /**
     * Gets the value of the idUsuarioCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    /**
     * Sets the value of the idUsuarioCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUsuarioCreacion(Long value) {
        this.idUsuarioCreacion = value;
    }

}
