
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for aceptarApoderamientoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="aceptarApoderamientoResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="return" type="{http://ws.funcionario.map.es/}aceptarApoderamientoWSResponseSalida" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aceptarApoderamientoResponse", propOrder = {
    "_return"
})
public class AceptarApoderamientoResponse {

    @XmlElement(name = "return", namespace = "http://ws.funcionario.map.es/")
    protected AceptarApoderamientoWSResponseSalida _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link AceptarApoderamientoWSResponseSalida }
     *     
     */
    public AceptarApoderamientoWSResponseSalida getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link AceptarApoderamientoWSResponseSalida }
     *     
     */
    public void setReturn(AceptarApoderamientoWSResponseSalida value) {
        this._return = value;
    }

}
