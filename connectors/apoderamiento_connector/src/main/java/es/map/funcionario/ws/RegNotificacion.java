
package es.map.funcionario.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for regNotificacion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="regNotificacion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descCausaDenegacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idLectura" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idNotificacion" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="regCatTipoMetodoNot" type="{http://ws.funcionario.map.es/}regCatTipoMetodoNot" minOccurs="0"/>
 *         &lt;element name="regOrganismoAdherido" type="{http://ws.funcionario.map.es/}regOrganismoAdherido" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioDestinatario" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="regUsuarioByIdUsuarioOrigen" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhFuncionarioByIdFuncionario" type="{http://ws.funcionario.map.es/}rfhFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhFuncionarioByIdFuncionarioDestinatario" type="{http://ws.funcionario.map.es/}rfhFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhHabilitacion" type="{http://ws.funcionario.map.es/}rfhHabilitacion" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "regNotificacion", propOrder = {
    "descCausaDenegacion",
    "fhCreacion",
    "fhModificacion",
    "idActivo",
    "idLectura",
    "idNotificacion",
    "regCatTipoMetodoNot",
    "regOrganismoAdherido",
    "regUsuarioByIdUsuarioDestinatario",
    "regUsuarioByIdUsuarioOrigen",
    "rfhFuncionarioByIdFuncionario",
    "rfhFuncionarioByIdFuncionarioDestinatario",
    "rfhHabilitacion"
})
public class RegNotificacion {

    protected String descCausaDenegacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idActivo;
    protected Boolean idLectura;
    protected Long idNotificacion;
    protected RegCatTipoMetodoNot regCatTipoMetodoNot;
    protected RegOrganismoAdherido regOrganismoAdherido;
    protected RegUsuario regUsuarioByIdUsuarioDestinatario;
    protected RegUsuario regUsuarioByIdUsuarioOrigen;
    protected RfhFuncionario rfhFuncionarioByIdFuncionario;
    protected RfhFuncionario rfhFuncionarioByIdFuncionarioDestinatario;
    protected RfhHabilitacion rfhHabilitacion;

    /**
     * Gets the value of the descCausaDenegacion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausaDenegacion() {
        return descCausaDenegacion;
    }

    /**
     * Sets the value of the descCausaDenegacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausaDenegacion(String value) {
        this.descCausaDenegacion = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idActivo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdActivo() {
        return idActivo;
    }

    /**
     * Sets the value of the idActivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdActivo(Boolean value) {
        this.idActivo = value;
    }

    /**
     * Gets the value of the idLectura property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdLectura() {
        return idLectura;
    }

    /**
     * Sets the value of the idLectura property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdLectura(Boolean value) {
        this.idLectura = value;
    }

    /**
     * Gets the value of the idNotificacion property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdNotificacion() {
        return idNotificacion;
    }

    /**
     * Sets the value of the idNotificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdNotificacion(Long value) {
        this.idNotificacion = value;
    }

    /**
     * Gets the value of the regCatTipoMetodoNot property.
     * 
     * @return
     *     possible object is
     *     {@link RegCatTipoMetodoNot }
     *     
     */
    public RegCatTipoMetodoNot getRegCatTipoMetodoNot() {
        return regCatTipoMetodoNot;
    }

    /**
     * Sets the value of the regCatTipoMetodoNot property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCatTipoMetodoNot }
     *     
     */
    public void setRegCatTipoMetodoNot(RegCatTipoMetodoNot value) {
        this.regCatTipoMetodoNot = value;
    }

    /**
     * Gets the value of the regOrganismoAdherido property.
     * 
     * @return
     *     possible object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public RegOrganismoAdherido getRegOrganismoAdherido() {
        return regOrganismoAdherido;
    }

    /**
     * Sets the value of the regOrganismoAdherido property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegOrganismoAdherido }
     *     
     */
    public void setRegOrganismoAdherido(RegOrganismoAdherido value) {
        this.regOrganismoAdherido = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioDestinatario() {
        return regUsuarioByIdUsuarioDestinatario;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioDestinatario(RegUsuario value) {
        this.regUsuarioByIdUsuarioDestinatario = value;
    }

    /**
     * Gets the value of the regUsuarioByIdUsuarioOrigen property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuarioByIdUsuarioOrigen() {
        return regUsuarioByIdUsuarioOrigen;
    }

    /**
     * Sets the value of the regUsuarioByIdUsuarioOrigen property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuarioByIdUsuarioOrigen(RegUsuario value) {
        this.regUsuarioByIdUsuarioOrigen = value;
    }

    /**
     * Gets the value of the rfhFuncionarioByIdFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhFuncionario }
     *     
     */
    public RfhFuncionario getRfhFuncionarioByIdFuncionario() {
        return rfhFuncionarioByIdFuncionario;
    }

    /**
     * Sets the value of the rfhFuncionarioByIdFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhFuncionario }
     *     
     */
    public void setRfhFuncionarioByIdFuncionario(RfhFuncionario value) {
        this.rfhFuncionarioByIdFuncionario = value;
    }

    /**
     * Gets the value of the rfhFuncionarioByIdFuncionarioDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhFuncionario }
     *     
     */
    public RfhFuncionario getRfhFuncionarioByIdFuncionarioDestinatario() {
        return rfhFuncionarioByIdFuncionarioDestinatario;
    }

    /**
     * Sets the value of the rfhFuncionarioByIdFuncionarioDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhFuncionario }
     *     
     */
    public void setRfhFuncionarioByIdFuncionarioDestinatario(RfhFuncionario value) {
        this.rfhFuncionarioByIdFuncionarioDestinatario = value;
    }

    /**
     * Gets the value of the rfhHabilitacion property.
     * 
     * @return
     *     possible object is
     *     {@link RfhHabilitacion }
     *     
     */
    public RfhHabilitacion getRfhHabilitacion() {
        return rfhHabilitacion;
    }

    /**
     * Sets the value of the rfhHabilitacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhHabilitacion }
     *     
     */
    public void setRfhHabilitacion(RfhHabilitacion value) {
        this.rfhHabilitacion = value;
    }

}
