package es.grupoavalon.aecosan.connector.apoderamiento.util;

public enum Errores {

    ERR_SISTEMA(0000,"Error del sistema."),
    ERR_IDENTIFICADOR_INVALIDO(0001 ,"El identificador de la aplicación solicitante no es válido."),
    ERR_CAMPO_INCORRECTO(0002 ,"El formato del campo no es correcto: [nombreCampo]."),
    ERR_PARAMETROS_VACIOS(0100 ,"Es obligatorio informar al menos un parámetro de búsqueda."),
    ERR_ESTADO_INVALIDO(0101 ,"El estado del apoderamiento consultado no es válido."),
    ERR_APODERAMIENTO_NO_ENCONTRADO(0102 ,"No existe ningún apoderamiento con los parámetros especificados."),
    ERR_FECHA_FIN(0103 ,"Periodo de registro: la fecha de fin de registro no puede ser anterior a la de inicio."),
    ERR_FECHA_INCIO_FIN(0104 ,"Periodo de registro: la fecha de inicio y fin de registro no pueden ser posteriores a la de hoy."),
    ERR_EXCESO_REGISTROS(0105 ,"La consulta de apoderamientos ha devuelto");
    private int code;
    private String description;
    private Errores(int code, String description) {
        this.description = description;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
