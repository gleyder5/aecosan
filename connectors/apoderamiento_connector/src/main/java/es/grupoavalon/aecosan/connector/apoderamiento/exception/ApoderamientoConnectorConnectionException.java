package es.grupoavalon.aecosan.connector.apoderamiento.exception;

import es.grupoavalon.aecosan.connector.apoderamiento.exception.ConnectorException;

public class ApoderamientoConnectorConnectionException extends ConnectorException {
	private static final long serialVersionUID = 1L;
	
	public ApoderamientoConnectorConnectionException(String message) {
		super(message);
	}

	public ApoderamientoConnectorConnectionException(Exception e) {
		super(e);
	}
}
