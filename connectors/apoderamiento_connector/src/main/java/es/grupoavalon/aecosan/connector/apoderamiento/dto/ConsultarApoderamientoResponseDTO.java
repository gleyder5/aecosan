package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import javax.xml.bind.annotation.XmlElement;

public class ConsultarApoderamientoResponseDTO {

	@XmlElement (name = "ConsultaApoderamientoWSResponseSalidaDTO")
	private ConsultarApoderamientoWSResponseSalidaDTO consultaApoderamientoWSResponseSalida;

	public ConsultarApoderamientoWSResponseSalidaDTO getConsultaApoderamientoWSResponseSalida() {
		return consultaApoderamientoWSResponseSalida;
	}

	public void setConsultaApoderamientoWSResponseSalida(ConsultarApoderamientoWSResponseSalidaDTO consultaApoderamientoWSResponseSalida) {
		this.consultaApoderamientoWSResponseSalida = consultaApoderamientoWSResponseSalida;
	}
}
