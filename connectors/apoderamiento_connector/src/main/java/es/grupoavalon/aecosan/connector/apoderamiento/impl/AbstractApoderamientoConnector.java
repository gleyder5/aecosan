package es.grupoavalon.aecosan.connector.apoderamiento.impl;



import es.grupoavalon.aecosan.connector.apoderamiento.exception.ConnectorException;
import es.map.funcionario.ws.WSREACXFService;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;


import javax.xml.bind.JAXBException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public abstract class AbstractApoderamientoConnector {

	protected static URL instanciateUrl(String url) {
		URL wsUrl;
		try {
			wsUrl = new URL(url);
		} catch (MalformedURLException e) {
			throw new ConnectorException("Invalid URL:" + url);
		}
		return wsUrl;
	}

	protected static Object[] invokeClientProxy(WSREACXFService port, String method, Object... params) throws Exception {
		Client client = setupClientProxy(port);
		return client.invoke(method, params);
	}

	private static Client setupClientProxy(WSREACXFService port) throws JAXBException {
		Client clientProxy = ClientProxy.getClient(port);

		return clientProxy;
	}

	@SuppressWarnings("unchecked")
	protected <T> T getRespuestaFromRaw(Object[] responseRaw) {
		T response;

		if (responseRaw != null && responseRaw.length > 0) {
			response = (T) responseRaw[0];
		} else {
			throw new ConnectorException(ConnectorException.RESPONSE_UNEXPECTED + Arrays.toString(responseRaw));
		}

		return response;
	}

	protected static void handleException(Exception e) {
		if (e instanceof ConnectorException) {
			throw (ConnectorException) e;
		} else {
			throw new ConnectorException(ConnectorException.GENERAL_ERROR + e.getLocalizedMessage(), e);
		}
	}
}
