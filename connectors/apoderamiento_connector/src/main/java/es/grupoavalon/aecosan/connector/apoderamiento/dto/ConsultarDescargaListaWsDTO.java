package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import org.joda.time.DateTime;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class ConsultarDescargaListaWsDTO {
	@XmlElement(name = "anexos")
	public List<AnexoDTO> anexos;

	@XmlElement(name = "apoderado")
	public PoderdanteApoderadoWSDTO apoderado;

	@XmlElement(name = "codOrganismo")
	public String codOrganismo;

	@XmlElement(name = "estado")
    public  int estado;

	@XmlElement(name = "fechaFin")
	public DateTime fechaFin;

	@XmlElement(name = "fechaInicio")
	public DateTime fechaInicio;

	@XmlElement(name = "listaCategoriasPorOrganismo")
	public List<CategoriaWsDTO> listaCategoriasPorOrganismo;

	@XmlElement(name = "listaTramitesPorOrganismo")
	public List<TramiteWsDTO> listaTramitesPorOrganismo;

	@XmlElement(name = "numRegistro")
	public String numRegistro;

	@XmlElement(name = "poderdante")
	public PoderdanteApoderadoWSDTO poderdante;

	@XmlElement(name = "representante")
	public PoderdanteApoderadoWSDTO representante;

    public List<AnexoDTO> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<AnexoDTO> anexos) {
        this.anexos = anexos;
    }

    public PoderdanteApoderadoWSDTO getApoderado() {
        return apoderado;
    }

    public void setApoderado(PoderdanteApoderadoWSDTO apoderado) {
        this.apoderado = apoderado;
    }

    public String getCodOrganismo() {
        return codOrganismo;
    }

    public void setCodOrganismo(String codOrganismo) {
        this.codOrganismo = codOrganismo;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public DateTime getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(DateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public DateTime getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(DateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public List<CategoriaWsDTO> getListaCategoriasPorOrganismo() {
        return listaCategoriasPorOrganismo;
    }

    public void setListaCategoriasPorOrganismo(List<CategoriaWsDTO> listaCategoriasPorOrganismo) {
        this.listaCategoriasPorOrganismo = listaCategoriasPorOrganismo;
    }

    public List<TramiteWsDTO> getListaTramitesPorOrganismo() {
        return listaTramitesPorOrganismo;
    }

    public void setListaTramitesPorOrganismo(List<TramiteWsDTO> listaTramitesPorOrganismo) {
        this.listaTramitesPorOrganismo = listaTramitesPorOrganismo;
    }

    public String getNumRegistro() {
        return numRegistro;
    }

    public void setNumRegistro(String numRegistro) {
        this.numRegistro = numRegistro;
    }

    public PoderdanteApoderadoWSDTO getPoderdante() {
        return poderdante;
    }

    public void setPoderdante(PoderdanteApoderadoWSDTO poderdante) {
        this.poderdante = poderdante;
    }

    public PoderdanteApoderadoWSDTO getRepresentante() {
        return representante;
    }

    public void setRepresentante(PoderdanteApoderadoWSDTO representante) {
        this.representante = representante;
    }

    @Override
    public String toString() {
        return "ConsultarDescargaListaWsDTO{" +
                "anexos=" + anexos +
                ", apoderado=" + apoderado +
                ", codOrganismo='" + codOrganismo + '\'' +
                ", estado=" + estado +
                ", fechaFin=" + fechaFin +
                ", fechaInicio=" + fechaInicio +
                ", listaCategoriasPorOrganismo=" + listaCategoriasPorOrganismo +
                ", listaTramitesPorOrganismo=" + listaTramitesPorOrganismo +
                ", numRegistro='" + numRegistro + '\'' +
                ", poderdante=" + poderdante +
                ", representante=" + representante +
                '}';
    }
}
