package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import javax.xml.bind.annotation.XmlElement;

public class ConsultarApoderamientoRequestDTO {

	@XmlElement (name = "url")
	private String url;
	@XmlElement (name = "idOrganismo")
	private String idOrganismo;
	@XmlElement(name = "CodApoderamiento")
	private int CodApoderamiento;
	@XmlElement(name = "NIFPoderdante")
	private String NIFPoderdante;
	@XmlElement(name = "CIFPoderdante")
	private String CIFPoderdante;
	@XmlElement(name = "CIFApoderado")
	private String CIFApoderado;
	@XmlElement(name = "idTramiteREA")
	private int idTramiteREA;
	@XmlElement(name = "idCategoriaREA")
	private Integer  idCategoriaREA;
	@XmlElement(name = "Estado")
	private Integer  Estado;
	@XmlElement(name = "fechaInicio")
	private String fechaInicio;
	@XmlElement(name = "fechaFin")
	private String fechaFin;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}



	public int getCodApoderamiento() {
		return CodApoderamiento;
	}

	public void setCodApoderamiento(int codApoderamiento) {
		CodApoderamiento = codApoderamiento;
	}

	public String getNIFPoderdante() {
		return NIFPoderdante;
	}

	public void setNIFPoderdante(String NIFPoderdante) {
		this.NIFPoderdante = NIFPoderdante;
	}

	public String getCIFPoderdante() {
		return CIFPoderdante;
	}

	public void setCIFPoderdante(String CIFPoderdante) {
		this.CIFPoderdante = CIFPoderdante;
	}

	public String getCIFApoderado() {
		return CIFApoderado;
	}

	public void setCIFApoderado(String CIFApoderado) {
		this.CIFApoderado = CIFApoderado;
	}

	public int getIdTramiteREA() {
		return idTramiteREA;
	}

	public void setIdTramiteREA(int idTramiteREA) {
		this.idTramiteREA = idTramiteREA;
	}

	public Integer getIdCategoriaREA() {
		return idCategoriaREA;
	}

	public void setIdCategoriaREA(Integer idCategoriaREA) {
		this.idCategoriaREA = idCategoriaREA;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getIdOrganismo() {
		return idOrganismo;
	}

	public void setIdOrganismo(String idOrganismo) {
		this.idOrganismo = idOrganismo;
	}

	public Integer getEstado() {
		return Estado;
	}

	public void setEstado(Integer estado) {
		Estado = estado;
	}

	@Override
	public String toString() {
		return "ConsultarApoderamientoRequestDTO{" +
				"idOrganismo='" + idOrganismo + '\'' +
				", CodApoderamiento='" + CodApoderamiento + '\'' +
				", NIFPoderdante='" + NIFPoderdante + '\'' +
				", CIFPoderdante='" + CIFPoderdante + '\'' +
				", CIFApoderado='" + CIFApoderado + '\'' +
				", idTramiteREA='" + idTramiteREA + '\'' +
				", idCategoriaREA='" + idCategoriaREA + '\'' +
				", Estado='" + Estado + '\'' +
				", fechaInicio='" + fechaInicio + '\'' +
				", fechaFin='" + fechaFin + '\'' +
				'}';
	}
}
