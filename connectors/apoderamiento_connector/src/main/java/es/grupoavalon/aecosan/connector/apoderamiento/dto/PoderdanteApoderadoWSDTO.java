package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import javax.xml.bind.annotation.XmlElement;

public class PoderdanteApoderadoWSDTO {

    @XmlElement(name="CIF")
    private String CIF;
    @XmlElement(name="email")
    private String email;
    @XmlElement(name="NIF_NIE")
    private String NIF_NIE;
    @XmlElement(name="nombre")
    private String nombre;
    @XmlElement(name="primerApellido")
    private String primerApellido;
    @XmlElement(name="razonSocial")
    private String razonSocial;
    @XmlElement(name="segundoApellido")
    private String segundoApellido;
    @XmlElement(name="telefono")
    private  int telefono;

    public String getCIF() {
        return CIF;
    }

    public void setCIF(String CIF) {
        this.CIF = CIF;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNIF_NIE() {
        return NIF_NIE;
    }

    public void setNIF_NIE(String NIF_NIE) {
        this.NIF_NIE = NIF_NIE;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "PoderdanteApoderadoWSDTO{" +
                "CIF='" + CIF + '\'' +
                ", email='" + email + '\'' +
                ", NIF_NIE='" + NIF_NIE + '\'' +
                ", nombre='" + nombre + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", razonSocial='" + razonSocial + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", telefono=" + telefono +
                '}';
    }
}
