package es.grupoavalon.aecosan.connector.apoderamiento.dto;


import java.util.List;

public class ConsultarApoderamientoWSResponseSalidaDTO {

    private ErrorWSDTO error;

    public ErrorWSDTO getError() {
        return error;
    }

    private List<ConsultarDescargaListaWsDTO> consultarDescargaListaWsDTOList;

    public List<ConsultarDescargaListaWsDTO> getConsultarDescargaListaWsDTOList() {
        return consultarDescargaListaWsDTOList;
    }

    public void setError(ErrorWSDTO error) {
        this.error = error;
    }

    public void setConsultarDescargaListaWsDTOList(List<ConsultarDescargaListaWsDTO> consultarDescargaListaWsDTOList) {
        this.consultarDescargaListaWsDTOList = consultarDescargaListaWsDTOList;
    }
}
