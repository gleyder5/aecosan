package es.grupoavalon.aecosan.connector.apoderamiento.impl;

import es.grupoavalon.aecosan.connector.apoderamiento.ApoderamientoConnectorApi;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.*;
import es.grupoavalon.aecosan.connector.apoderamiento.exception.ApoderamientoConnectorOperationException;
import es.grupoavalon.aecosan.connector.apoderamiento.util.Errores;
import es.map.funcionario.ws.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.ArrayList;


public class ApoderamientoConnectorApiImpl extends AbstractApoderamientoConnector implements ApoderamientoConnectorApi {

	protected static final Logger logger = LoggerFactory.getLogger(ApoderamientoConnectorApiImpl.class);


	@Override
	public ConsultarApoderamientoResponseDTO consultarApoderamiento(ConsultarApoderamientoRequestDTO apoderamientoRequestDTO) {
		URL url = instanciateUrl(apoderamientoRequestDTO.getUrl());
		WSREACXFService port = instanciateServicePort(url);
		ConsultarApoderamientoResponseDTO responseDTO = null;
		try {
			ConsultarApoderamiento request = getConsultApoderamientoFromDTO(apoderamientoRequestDTO);
			ConsultarApoderamientoWSResponseSalida response =  executeConsultaApoderamiento(port, request);
			responseDTO = instanciateConsultaApoderamientoResponseFromWSResponse(response);
		} catch (Exception e) {
			handleException(e);
		}
		return responseDTO;
	}


	private ConsultarApoderamiento getConsultApoderamientoFromDTO(ConsultarApoderamientoRequestDTO consultaApoderamientoDTO)
	{
		ObjectFactory factory = new ObjectFactory();
		ConsultarApoderamiento consultaApoderamiento = factory.createConsultarApoderamiento();
		consultaApoderamiento.setCIFApoderado(consultaApoderamientoDTO.getCIFApoderado());
		consultaApoderamiento.setCIFPoderdante(consultaApoderamientoDTO.getCIFPoderdante());
		consultaApoderamiento.setNIFPoderdante(consultaApoderamientoDTO.getNIFPoderdante());
		consultaApoderamiento.setCodApoderamiento(consultaApoderamientoDTO.getCodApoderamiento());
		consultaApoderamiento.setEstado(consultaApoderamientoDTO.getEstado());
		if (consultaApoderamientoDTO.getFechaFin() != null) {
			consultaApoderamiento.setFechaFin(consultaApoderamientoDTO.getFechaFin());
		}
		consultaApoderamiento.setFechaInicio(consultaApoderamientoDTO.getFechaInicio());

		consultaApoderamiento.setIdCategoriaREA(consultaApoderamientoDTO.getIdCategoriaREA());
		consultaApoderamiento.setIdTramiteREA(consultaApoderamientoDTO.getIdTramiteREA());
		consultaApoderamiento.setIdOrganismo(consultaApoderamientoDTO.getIdOrganismo());
		return  consultaApoderamiento;
	}


	private ConsultarApoderamientoWSResponseSalida executeConsultaApoderamiento(WSREACXFService port, ConsultarApoderamiento request) throws ApoderamientoConnectorOperationException {

		Object[] responseRaw = new Object[0];
		ConsultarApoderamientoWSResponseSalida response= null;
		try {
			responseRaw = invokeClientProxy(port, "consultarApoderamiento", request.getCodApoderamiento(), request.getNIFPoderdante(), request.getCIFPoderdante(), request.getNIFApoderado(), request.getCIFApoderado(), request.getNIFRepresentante(), request.getIdOrganismo(), request.getIdTramiteREA(), request.getIdTramiteOrganismo(), request.getIdCategoriaREA(), request.getEstado(), request.getFechaInicio(), request.getFechaFin());
			response = getRespuestaFromRaw(responseRaw);
			processConsultaApoderamientoResponse(response);
		} catch (Exception e) {
			handleException(e);
		}

		return response;

	}


	private ConsultarApoderamientoResponseDTO processConsultaApoderamientoResponse(ConsultarApoderamientoWSResponseSalida response) {
		ConsultarApoderamientoResponseDTO responseDTO ;

		if (response.getError()!=null) {
			logger.debug(" error response received: {0}", response);
			switch(response.getError().getCodError()) {
				case 0000:
					logger.error( Errores.ERR_SISTEMA.getDescription());
					break;
				case 0001:
					logger.error( Errores.ERR_IDENTIFICADOR_INVALIDO.getDescription());
					break;
				case 0002:
					logger.error( Errores.ERR_CAMPO_INCORRECTO.getDescription());
					break;
				case 0100:
					logger.error( Errores.ERR_PARAMETROS_VACIOS.getDescription());
					break;
				case 0101:
					logger.error( Errores.ERR_ESTADO_INVALIDO.getDescription());
					break;
				case 0102:
					logger.error( Errores.ERR_APODERAMIENTO_NO_ENCONTRADO.getDescription());
					break;
				case 0103:
					logger.error( Errores.ERR_FECHA_FIN.getDescription());
					break;
				case 0104:
					logger.error( Errores.ERR_FECHA_INCIO_FIN.getDescription());
					break;
				case 0105:
					logger.error( Errores.ERR_EXCESO_REGISTROS.getDescription());
					break;
				default:
					logger.error(response.getError().getDesError());
				break;
			}
			throw new ApoderamientoConnectorOperationException(Integer.toString(response.getError().getCodError()),response.getError().getDesError());
		}
		else {
			logger.debug("response ok received: {0}", response);
			responseDTO = instanciateConsultaApoderamientoResponseFromWSResponse(response);
		}
		return responseDTO;
	}


	private ConsultarApoderamientoResponseDTO instanciateConsultaApoderamientoResponseFromWSResponse(ConsultarApoderamientoWSResponseSalida consultaApoderamientoWSResponseSalida){
		ConsultarApoderamientoResponseDTO apoderamientoResponseDTO = new ConsultarApoderamientoResponseDTO();
		apoderamientoResponseDTO.setConsultaApoderamientoWSResponseSalida(instanciateConsultaApoderamientoResponseSalidaDTOFromWSResponse(consultaApoderamientoWSResponseSalida));
		return apoderamientoResponseDTO;
	}
	private ConsultarApoderamientoWSResponseSalidaDTO instanciateConsultaApoderamientoResponseSalidaDTOFromWSResponse(ConsultarApoderamientoWSResponseSalida  response) {

		ConsultarApoderamientoWSResponseSalidaDTO consultarApoderamientoWSResponseSalidaDTO = new ConsultarApoderamientoWSResponseSalidaDTO();
		consultarApoderamientoWSResponseSalidaDTO.setConsultarDescargaListaWsDTOList(new ArrayList<ConsultarDescargaListaWsDTO>());
		for(ConsultaDescargaListaWS consultaDescargaListaWS: response.getLista()){
			ConsultarDescargaListaWsDTO consultarDescargaListaWSDTO = new ConsultarDescargaListaWsDTO();
			PoderdanteApoderadoWSDTO poderdanteApoderadoWSDTO  = new PoderdanteApoderadoWSDTO();
			if(consultaDescargaListaWS.getPoderdante()!=null) {
				poderdanteApoderadoWSDTO.setCIF(consultaDescargaListaWS.getPoderdante().getCIF());
				poderdanteApoderadoWSDTO.setEmail(consultaDescargaListaWS.getPoderdante().getEmail());
				poderdanteApoderadoWSDTO.setNIF_NIE(consultaDescargaListaWS.getPoderdante().getNIFNIE());
				poderdanteApoderadoWSDTO.setNombre(consultaDescargaListaWS.getPoderdante().getNombre());
				poderdanteApoderadoWSDTO.setPrimerApellido(consultaDescargaListaWS.getPoderdante().getPrimerApellido());
				poderdanteApoderadoWSDTO.setSegundoApellido(consultaDescargaListaWS.getPoderdante().getSegundoApellido());
				poderdanteApoderadoWSDTO.setRazonSocial(consultaDescargaListaWS.getPoderdante().getRazonSocial());
				poderdanteApoderadoWSDTO.setTelefono(consultaDescargaListaWS.getPoderdante().getTelefono());
			}
			consultarDescargaListaWSDTO.setApoderado(poderdanteApoderadoWSDTO);
			consultarApoderamientoWSResponseSalidaDTO.getConsultarDescargaListaWsDTOList().add(consultarDescargaListaWSDTO);
		}
		return consultarApoderamientoWSResponseSalidaDTO;
	}

	private WSREACXFService instanciateServicePort(URL url) {
		ReaCXFWS service = new ReaCXFWS(url);
		return service.getWSREACXFServiceImplPort();
	}
}
