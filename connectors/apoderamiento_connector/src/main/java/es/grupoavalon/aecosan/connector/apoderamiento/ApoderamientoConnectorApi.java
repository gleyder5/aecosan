package es.grupoavalon.aecosan.connector.apoderamiento;


import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoRequestDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;

public interface ApoderamientoConnectorApi {


	ConsultarApoderamientoResponseDTO consultarApoderamiento(ConsultarApoderamientoRequestDTO consultarApoderamientoRequestDTO);


}

