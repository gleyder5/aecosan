package es.grupoavalon.aecosan.connector.apoderamiento.dto;
import javax.xml.bind.annotation.XmlElement;

public class AnexoDTO {


    @XmlElement(required = true)
    private String nombre;
    @XmlElement(required = true)
    private String contenido;
    @XmlElement(required = true)
    private String firma;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }
}
