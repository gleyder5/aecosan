package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

public class CategoriaWsDTO {


    @XmlElement(name = "codOrganismo")
    private String codOrganismo;

    @XmlElement(name = "codCategoria")
    private String codCategoria;

    @XmlElement(name = "nombreCategoria")
    private String nombreCategoria;

    @XmlElement(name = "descripcionCategoria")
    private String descripcionCategoria;

    @XmlElement(name = "listaTramites")
    private List<TramiteWsDTO> listaTramites;

    public String getCodOrganismo() {
        return codOrganismo;
    }

    public void setCodOrganismo(String codOrganismo) {
        this.codOrganismo = codOrganismo;
    }

    public String getCodCategoria() {
        return codCategoria;
    }

    public void setCodCategoria(String codCategoria) {
        this.codCategoria = codCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }

    public List<TramiteWsDTO> getListaTramites() {
        return listaTramites;
    }

    public void setListaTramites(List<TramiteWsDTO> listaTramites) {
        this.listaTramites = listaTramites;
    }
}
