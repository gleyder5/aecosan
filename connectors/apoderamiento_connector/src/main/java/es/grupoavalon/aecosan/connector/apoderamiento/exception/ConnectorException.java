package es.grupoavalon.aecosan.connector.apoderamiento.exception;

public class ConnectorException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public static final String GENERAL_ERROR = "Can not execute the apodaremiento request due an error:";

	public static final String RESPONSE_UNEXPECTED = "Can not process the response from the server was unexpected";

	public ConnectorException(String message, Throwable cause) {
		super(message, cause);

	}

	public ConnectorException(String message) {
		super(message);
	}
	
	public ConnectorException(Throwable t) {
		super(t);
	}
}
