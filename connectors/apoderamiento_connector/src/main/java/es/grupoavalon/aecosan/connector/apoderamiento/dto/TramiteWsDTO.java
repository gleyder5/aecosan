package es.grupoavalon.aecosan.connector.apoderamiento.dto;

import javax.xml.bind.annotation.XmlElement;

public class TramiteWsDTO {


    @XmlElement(name = "codOrganismo")
    private  String codOrganismo;


    @XmlElement(name = "idTramiteREA")
    private  int    idTramiteREA;

    public String getCodOrganismo() {
        return codOrganismo;
    }

    public void setCodOrganismo(String codOrganismo) {
        this.codOrganismo = codOrganismo;
    }

    public int getIdTramiteREA() {
        return idTramiteREA;
    }

    public void setIdTramiteREA(int idTramiteREA) {
        this.idTramiteREA = idTramiteREA;
    }
}
