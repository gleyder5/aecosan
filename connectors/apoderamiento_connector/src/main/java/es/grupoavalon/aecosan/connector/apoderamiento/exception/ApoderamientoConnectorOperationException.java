package es.grupoavalon.aecosan.connector.apoderamiento.exception;

public class ApoderamientoConnectorOperationException extends ConnectorException {
	private static final long serialVersionUID = 1L;
	
	private String errorCode;
	private String errorDescription;

	public ApoderamientoConnectorOperationException(String errorCode, String errorDescription) {
		super(errorDescription);
		this.errorCode = errorCode;
		this.errorDescription = errorDescription;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
}
