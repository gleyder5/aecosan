LIBRERIAS REQUERIDAS POR MAVEN QUE HAY QUE INSTALAR
---------------------------------------------------
Ejecutar los siguientes comandos en maven ( boton derecho sobre el pom principal de proyecto, run as--> maven build...)

install:install-file -Dfile=jarsMaven/jaxb-api-2.2.11.jar -DgroupId=javax.xml.bind -DartifactId=jaxb-api -Dversion=2.2.11 -Dpackaging=jar
install:install-file -Dfile=jarsMaven/cinstall:install-file -Dfile=jarsMaven/ojdbc6.jar -DgroupId=com.oracle -DartifactId=ojdbc6 -Dversion=11.2.0.3.0 -Dpackaging=jar
laveclient-2.2.0.jar -DgroupId=es.msssi.claveclient -DartifactId=claveclient -Dversion=2.2.0 -Dpackaging=jar
install:install-file -Dfile=jarsMaven/Commons-2.0.0.jar -DgroupId=eu.stork -DartifactId=Commons -Dversion=2.0.0 -Dpackaging=jar
install:install-file -Dfile=jarsMaven/SamlEngine-2.0.0.jar -DgroupId=es.clave -DartifactId=SamlEngine -Dversion=2.0.0 -Dpackaging=jar
Estas librerias no se encuentran en repositorios publicos por lo que es requerido instalarlas para que el proyecto compile

COMO CONFIGURAR EL PROYECTO EN ECLIPSE
---------------------------------------
Para poder tener proyectos separados en el workspace ( una carpeta para la version generica, otra para _weblogic )
es necesario hacer lo siguiente:

1) Hacer checkout del proyecto completo en el workspace
2) Buscar la opcion File-->Import-->Existing Maven Projects y seleccionar los proyectos con los que se desea trabajar

Las carpetas creadas son un link simbolico a las carpetas y ficheros existentes en el root del proyecto, por lo que cualquier cambio
se refleja directamente en las carpetas y ficheros contenidos en la carpeta root

De esta forma es posible desplegar los proyectos web en tomcat como un proyecto web normal de eclipse

COMO CONFIGURAR TOMCAT EN ECLIPSE PARA ESTE PROYECTO
-----------------------------------------------------
Es necesario agregar algunos jars al contexto de tomcat, estas librerias normalmente estan 
disponibles en weblogic, es por ello que se deben agregar manualmente a tomcat y no al WEB-INF/lib

Se asume que previamente se debe haber agregado un servidor tomcat al workspace

1) ir a la carpeta servers ( vista navigator)
2) buscar el fichero catalina.properties del servidor
3) buscar la propiedad common.loader=
4) agregar al final, RUTA_DEL_PROYECTO/*.jar donde RUTA_DEL_PROYECTO es la ruta fisica del projecto ejemplo...
common.loader=${catalina.base}/lib,${catalina.base}/lib/*.jar,${catalina.home}/lib,${catalina.home}/lib/*.jar,/Users/ottoabreu/Documents/workspaceAvalon/PROYECTO_ROOT/jarsMaven/*.jar
5) sobre cada proyecto web, boton derecho, configuracion, build assambly y verificar que la carpeta de pruebas test no esta incluida entre los paquetes que generan el war, ya que puede
generar problemas de classpath. Si se encuentra presente, retirarla

CREAR CARPETA REPO
---------------------------------------------------------
En el ambiente de produccion y pre se utiliza una carpeta externa.
para simular esto es necesario crear una carpeta como ejemplo:
C:/appsRepo/repositorio/wld-pre-06/apps/geda-web-jee-r01a-iq/

Si se desea modificar esta ruta, debe cambiarse en todos los web.xml de geda_tomcat y
geda_weblogic_test, asi como en geda_tomcat/src/test/resources/pathrepo.properties y el weblogic.xml de geda_weblogic_test

CONFIGURAR BOUNCY CASTLE COMO PROVEEDOR
------------------------------------------

Ir a la carpeta donde se encuentra la instalacion de java(JAVA_HOME) que ejecuta el servidor y buscar el siguiente fichero: 
${JAVA_HOME}\jre\lib\security\java.security

agregar: security.provider.<n>=org.bouncycastle.jce.provider.BouncyCastleProvider

donde <n> es reemplazado por un numero.

Asegurarse que la libreria bcprov-jdk15on-1.52.jar se encuentra en el classpath del servidor ( no de la aplicacion)
