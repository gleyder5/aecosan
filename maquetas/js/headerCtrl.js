var app = angular.module('app', ['ui-router']);

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider
	.state("formulario",{	
		url: "/formulario",
		templateUrl:"formulario.html",
		controller: "headerCtrl"		
	})
	
	$urlRouterProvider.otherwise("/");
	
});


app.controller('headerCtrl', function(){}