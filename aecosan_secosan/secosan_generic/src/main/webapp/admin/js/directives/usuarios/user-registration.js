angular.module('secosanAdmin').directive('userRegistration', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "user-registration.html",
		controller:"GestionUsuariosController"
	};
});
