angular.module('secosanAdmin').directive('containerBienvenidaAdmin', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_TRAMITADOR + "container-bienvenida-admin.html"
	};
});
