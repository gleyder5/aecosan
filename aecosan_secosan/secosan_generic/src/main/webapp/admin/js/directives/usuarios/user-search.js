angular.module('secosanAdmin').directive('userSearch', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "user-search.html",
		controller:"GestionUsuariosController"
	};
});
