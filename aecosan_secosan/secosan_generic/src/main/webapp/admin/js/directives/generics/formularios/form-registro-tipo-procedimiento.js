angular.module('secosanAdmin').directive('formRegistroTipoProcedimiento', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS_ADMIN	 + "form-registro-tipo-procedimiento.html"
	};
});
