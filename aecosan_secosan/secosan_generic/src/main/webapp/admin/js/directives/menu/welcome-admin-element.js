angular.module('secosanAdmin').directive('welcomeAdminElement', function(WelcomeMenu, TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_TRAMITADOR + "welcome-admin-element.html",
		link : function(scope) {
			scope.$watch(function() {
				return WelcomeMenu.getWelcomeMenuItem()
			}, function(newVal) {

				switch (newVal) {
				case "solicitudes":
					scope.solicitudes = true;
					scope.gestinstrucciones = false;
					scope.altausuarios = false;
					scope.altausuarios = false;
					scope.gestionusuarios = false;
					scope.monitoring = false;
					scope.manejotasa = false;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "gestinstrucciones":
					scope.gestinstrucciones = true;
					scope.solicitudes = false;
					scope.altausuarios = false;
					scope.gestionusuarios = false;
					scope.monitoring = false;
					scope.manejotasa = false;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "altausuarios":
					scope.altausuarios = true;
					scope.solicitudes = false;
					scope.gestinstrucciones = false;
					scope.gestionusuarios = false;
					scope.monitoring = false;
					scope.manejotasa = false;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "gestionusuarios":
					scope.gestionusuarios=true;
					scope.altausuarios = false;
					scope.solicitudes = false;
					scope.gestinstrucciones = false;
					scope.monitoring = false;
					scope.manejotasa = false;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "manejotasa":
					scope.gestionusuarios=false;
					scope.altausuarios = false;
					scope.solicitudes = false;
					scope.gestinstrucciones = false;
					scope.monitoring = false;
					scope.manejotasa = true;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "monitoring":
					scope.gestionusuarios = false;
					scope.altausuarios = false;
					scope.solicitudes = false;
					scope.gestinstrucciones = false;
					scope.manejotasa = false;
					scope.monitoring = true;
					scope.logging = false;
                    scope.gesttiposprocedimiento = false;
					break;
				case "logging":
					scope.gestionusuarios = false;
					scope.altausuarios = false;
					scope.solicitudes = false;
					scope.gestinstrucciones = false;
					scope.manejotasa = false;
					scope.monitoring = false;
					scope.logging = true;
					scope.gesttiposprocedimiento = false;
					break;

                // case "gesttiposprocedimiento":
                //    scope.gestionusuarios = false;
                //    scope.altausuarios = false;
                //    scope.solicitudes = false;
                //    scope.gestinstrucciones = false;
                //    scope.manejotasa = false;
                //    scope.monitoring = false;
                //    scope.logging = false;
                //    scope.gesttiposprocedimiento = true;
                //  break;
				default:
					break;
				}

			});
		}
	};
});
