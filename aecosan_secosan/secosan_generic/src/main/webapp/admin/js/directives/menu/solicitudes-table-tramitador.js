angular.module('secosanAdmin').directive('solicitudesTableTramitador', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_TRAMITADOR + "solicitudes-table-tramitador.html"
	};
});
