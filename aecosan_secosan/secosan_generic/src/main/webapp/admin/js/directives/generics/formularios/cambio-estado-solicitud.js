angular.module('secosanAdmin').directive('cambioEstadoSolicitud', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS_ADMIN + "cambio-estado-solicitud.html",
		scope : {
			areaCode : '=',
			id : '=',
			prevStatus : '=',
			formId:'=',
			solicitaCopiaAutentica:'=',
			solicitaCopiaAutenticaInfo:'='
		}
	};
});