angular.module('secosanAdmin').directive('monitoringFilter', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_MONITORING + "monitoring-filter.html"
	};
});
