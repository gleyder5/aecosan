angular.module('secosanAdmin').directive('monitoringFilteredTable', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_MONITORING + "monitoring-filtered-table.html"
	};
});
