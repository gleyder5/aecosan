angular.module('secosanAdmin').directive('formUserRegistration', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "form-user-registration.html"
	};
});
