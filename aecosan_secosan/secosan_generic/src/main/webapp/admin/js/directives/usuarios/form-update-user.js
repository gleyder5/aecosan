angular.module('secosanAdmin').directive('formUpdateUser', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "form-update-user.html"
	};
});
