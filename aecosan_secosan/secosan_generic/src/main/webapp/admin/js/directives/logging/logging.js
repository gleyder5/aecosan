angular.module('secosanAdmin').directive('logging', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_LOGGING + "logging.html",
		controller : "LoggingController",
		scope:{}
	};
});
