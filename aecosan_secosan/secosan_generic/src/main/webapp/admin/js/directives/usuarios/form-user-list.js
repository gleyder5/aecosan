angular.module('secosanAdmin').directive('formUserList', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "form-user-list.html"
	};
});
