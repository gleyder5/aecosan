angular.module('secosanAdmin').directive('updateUserPopup', function($log,UpdateUserService, TEMPLATE_PATHS) {

	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "popup-update-user.html",
		transclude : true,
		replace : true,

		link : function(scope, element) {
			scope.$watch(function() {
				return UpdateUserService.getUserArgs();
			}, function(newVal) {
				if (angular.equals(newVal, {})) {
					UpdateUserService.clearUserArgs();
				} else {
					if (newVal.visible === true) {
						angular.copy(newVal.userData, scope.userData);
						$log.debug("newVal:");
						$log.debug(newVal);
						scope.sameUser = newVal.sameUser;
						scope.update = newVal.update;
						scope.auxUpdate.area.catalogId = scope.userData.area;
						scope.auxUpdate.userPermission.catalogId = scope.userData.role;
						scope.auxUpdate.status = !scope.userData.userBlocked;
						scope.userData.password = "";
						scope.auxUpdate.retypePassword = "";

						$(element).modal('show');
					} else {
						$(element).modal('hide');
					}
					UpdateUserService.clearUserArgs();
				}
			}, true);
			scope.$on('$destroy', function() {
				UpdateUserService.clearUserArgs();
			});
		}
	};
});
