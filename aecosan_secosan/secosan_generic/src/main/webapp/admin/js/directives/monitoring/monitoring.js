angular.module('secosanAdmin').directive('monitoring', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_MONITORING + "monitoring.html",
		controller : "MonitoringController"
	};
});
