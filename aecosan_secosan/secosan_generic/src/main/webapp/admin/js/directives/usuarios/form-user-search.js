angular.module('secosanAdmin').directive('formUserSearch', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ADMIN_USUARIOS + "form-user-search.html"
	};
});
