angular.module('secosanAdmin').directive('validateInternalUserDni', function(UserManagementService) {
	
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, elem, attr, ctrl) {
			elem.on('blur', function() {
				var dni = elem.val();
				if(dni !==undefined && dni !==""){
					UserManagementService.checkIfInternalUserExists(dni,function(data){
						var result = data.value;
						ctrl.$setValidity('userexists', !result);
					});
				}else{
					ctrl.$setValidity('userexists', true);
				}
			});
			
		}
	};
});