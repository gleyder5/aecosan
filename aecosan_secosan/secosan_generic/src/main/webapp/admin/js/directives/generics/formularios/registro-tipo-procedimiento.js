angular.module('secosanAdmin').directive('registroTipoProcedimiento', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS_ADMIN + "registro-tipo-procedimiento.html",
		controller:"GestionTipoProcedimientoController"
	};
});
