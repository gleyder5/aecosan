angular.module('secosanAdmin').config([ '$routeProvider', 'TEMPLATE_PATHS', function($routeProvider, TEMPLATE_PATHS) {
	$routeProvider.when('/', {
		redirectTo : '/login'
	}).when('/bienvenida', {
		templateUrl : TEMPLATE_PATHS.ADMIN_PAGES + 'bienvenidaAdmin.html',
		controller : 'BienvenidaAdminController',
		controllerAs : 'bienvenidaAdminController'
	}).when('/login', {
		templateUrl : TEMPLATE_PATHS.ADMIN_PAGES + 'login.html',
		controller : 'LoginAdminController',
		controllerAs : 'loginAdminController'
	}).when('/comunicacionSugerencia/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'comunicacionSugerencia.html',
		controller : 'SugerenciaControllerAdmin',
		controllerAs : 'sugerenciaControllerAdmin'
	}).when('/comunicacionQueja/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'comunicacionQueja.html',
		controller : 'QuejaControllerAdmin',
		controllerAs : 'quejaControllerAdmin'
	}).when('/evaluacionRiesgos/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'evaluacionRiesgos.html',
		controller : 'EvaluacionRiesgosControllerAdmin',
		controllerAs : 'evaluacionRiesgosControllerAdmin'
	}).when('/lmrFitosanitarios/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'lmrFitosanitarios.html',
		controller : 'LmrFitosanitariosControllerAdmin',
		controllerAs : 'lmrFitosanitariosControllerAdmin'
	}).when('/solicitudLogos/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'solicitudLogos.html',
		controller : 'SolicitudLogosControllerAdmin',
		controllerAs : 'solicitudLogosControllerAdmin'
	}).when('/financiacionAlimentosAlteracion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'financiacionAlimentosAlteracion.html',
		controller : 'FinanciacionAlimentosAlteracionControllerAdmin',
		controllerAs : 'financiacionAlimentosAlteracionControllerAdmin'
	}).when('/financiacionAlimentosInclusion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'financiacionAlimentosInclusion.html',
		controller : 'FinanciacionAlimentosInclusionControllerAdmin',
		controllerAs : 'financiacionAlimentosInclusionControllerAdmin'
	}).when('/financiacionAlimentosSuspension/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'financiacionAlimentosSuspension.html',
		controller : 'FinanciacionAlimentosSuspensionControllerAdmin',
		controllerAs : 'financiacionAlimentosSuspensionControllerAdmin'
	}).when('/alimentosGruposCeseComercializacion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'alimentosGruposCeseComercializacion.html',
		controller : 'AlimentosGruposCeseControllerAdmin',
		controllerAs : 'alimentosGruposCeseControllerAdmin'
	}).when('/alimentosGruposModificacion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'alimentosGruposModificacion.html',
		controller : 'AlimentosGruposModificacionControllerAdmin',
		controllerAs : 'alimentosGruposModificacionControllerAdmin'
	}).when('/alimentosGruposPuestaEnMercado/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'alimentosGruposPuestaEnMercado.html',
		controller : 'AlimentosGruposPuestaMercadoControllerAdmin',
		controllerAs : 'alimentosGruposPuestaMercadoControllerAdmin'
	}).when('/complementosAlimenticiosCeseComercializacion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'complementosAlimenticiosCeseComercializacion.html',
		controller : 'ComplementosAlimenticiosCeseControllerAdmin',
		controllerAs : 'complementosAlimenticiosCeseControllerAdmin'
	}).when('/complementosAlimenticiosModificacion/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'complementosAlimenticiosModificacion.html',
		controller : 'ComplementosAlimenticiosModificacionControllerAdmin',
		controllerAs : 'complementosAlimenticiosModificacionControllerAdmin'
	}).when('/complementosAlimenticiosPuestaEnMercado/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'complementosAlimenticiosPuestaEnMercado.html',
		controller : 'ComplementosAlimenticiosPuestaMercadoControllerAdmin',
		controllerAs : 'complementosAlimenticiosPuestaMercadoControllerAdmin'
	}).when('/registroAguas/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'registroAguas.html',
		controller : 'RegistroAguasControllerAdmin',
		controllerAs : 'registroAguasControllerAdmin'
	}).when('/registroIndustrias/:id?/:v?', {
		templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'registroIndustrias.html',
		controller : 'RegistroIndustriasControllerAdmin',
		controllerAs : 'registroIndustriasControllerAdmin'
	}).when('/registroEstatal/:id?/:v?', {
        templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'registroEstatal.html',
        controller : 'RegistroEstatalControllerAdmin',
        controllerAs : 'registroEstatalControllerAdmin'
    }).when('/subvencionAsocuae/:id?/:v?', {
        templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'subvencionAsocuae.html',
        controller : 'SubvencionAsocuaeControllerAdmin',
        controllerAs : 'subvencionAsocuaeControllerAdmin'
    }).when('/subvencionJuntasConsumo/:id?/:v?', {
        templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'subvencionJuntas.html',
        controller : 'SubvencionJuntasControllerAdmin',
        controllerAs : 'subvencionJuntasControllerAdmin'
    }).when('/becasConcejoConsumidores/:id?/:v?', {
            templateUrl : TEMPLATE_PATHS.GENERIC_PAGES + 'becasConcejoConsumidores.html',
            controller : 'BecasConcejoConsumidoresControllerAdmin',
            controllerAs : 'becasConcejoConsumidoresControllerAdmin'
        }).
	otherwise({
		redirectTo : '/'
	});
} ])

.run([ '$rootScope', '$cookieStore', '$location', 'AuthenticationUser', '$log', 'REDIRECT_COOKIES', function($rootScope, $cookieStore, $location, AuthenticationUser, $log, REDIRECT_COOKIES) {
	$rootScope.$on('$routeChangeStart', function(event, next) {
		var prevLocation = $location.url();
		AuthenticationUser.checkLoggedIn(function() {

			// event.preventDefault();
			if (prevLocation === "/login" || prevLocation === "/") {
				$location.path('/bienvenida');
			} else {
				$cookieStore.put(REDIRECT_COOKIES, prevLocation);
			}
		}, function() {

			if (prevLocation !== "/login" && prevLocation !== "/" && prevLocation !== "") {
				event.preventDefault();
				$location.path('/login');
				$log.info("user not logged: " + prevLocation);
			}
		});

	});
} ]);
