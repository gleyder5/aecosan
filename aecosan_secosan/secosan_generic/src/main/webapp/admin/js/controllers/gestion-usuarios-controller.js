angular.module('secosanAdmin').controller(
		'GestionUsuariosController',
		function($scope, $log, $location, Messages, AdminFormModelService, UtilService, AdminSetDataFromCatalogService, NgTableParams, ModalService, AdminModalParamsService, UserManagementService,
				UpdateUserService,AdminCatalogueHandler,Spinner, ERR_MSG, REDIRECT_URL,IMAGES_URL,CATALOGUE_NAMES,VALIDATION_PATTERN) {
			var $this = this;

			$scope.formularioATratar = AdminFormModelService.getInternalUserRegistryModel();

			AdminSetDataFromCatalogService.getGruposUsuarioCatalogue($scope);
			AdminSetDataFromCatalogService.getRolesUsuarioCatalogue($scope);

			$scope.aux = {
				area : "",
				userPermission : ""
			}

			$scope.statusFilter = [ {
				'id' : 'false',
				'title' : 'Activo'
			}, {
				'id' : 'true',
				'title' : 'Bloqueado'
			} ];

			$scope.configTableParamsGestUsers = new NgTableParams({
				count : 10,
				sorting : {
					login : "asc"
				},
			}, {
				// page size buttons (right set of buttons in demo)
				counts : [],
				// determines the pager buttons (left set of buttons in demo)
				paginationMaxBlocks : 13,
				paginationMinBlocks : 2,
				getData : function($defer, params) {
					UserManagementService.getInternalUserList(params).success(function(data) {
						var total = data.recordsTotal;
						if(total!==data.recordsFiltered){
							total = data.recordsFiltered;
						}
						params.total(total);
						$defer.resolve(data.data);
					}).error(function(data, status, headers, config) {
						$log.debug(data);
						$log.debug(status);
						$log.debug(headers);
						$log.debug(config);
						Messages.setErrorMessageFromResponse(data, status);
					});
				}
			});

			$this.showHideUserFilter = function() {
				if ($scope.showTableUserFilters) {
					$scope.showTableUserFilters = false;
					$scope.configTableParamsGestUsers.filter({});
				} else {
					$scope.showTableUserFilters = true;
				}
			};
			
			$this.loadAreaTableFilterSelect = function($column){
				$log.info("loadAreaTableFilterSelect");
				AdminCatalogueHandler.getCatalogue(CATALOGUE_NAMES.USER_AREA).success(function(data) {
					$column.data = AdminCatalogueHandler.generateCatalogueDataForShow(data);
					$log.info("data:"+data);
				}).error(function() {
					AdminCatalogueHandler.generateDefaultErrorMessage();
				});
			}

			$this.deleteUser = function(index, idToDelete) {
				ModalService.showModalWindow(AdminModalParamsService.deleteInternalUser(index, idToDelete, $scope.configTableParamsGestUsers));
			}

			$this.updateUser = function(userData) {
				UpdateUserService.showUpdateUserWindow({
					'userData' : userData
				},$scope.configTableParamsGestUsers);
			};

			var errorMessage = "";

			$this.checkValidForm = function() {
				var valid = true;
				if ($scope.formularioATratar.password !== $scope.aux.retypePassword) {
					errorMessage = ERR_MSG.PASSWORD_MATCH;
					// Messages.setMessageAndType(ERR_MSG.PASSWORD_MATCH,
					// Messages.error);
					valid = false;
				}
				return valid;
			};

			$this.resetFields = function() {
				$scope.formularioATratar = AdminFormModelService.getInternalUserRegistryModel();
				$scope.aux.area = "";
				$scope.aux.userPermission = "";
				$scope.aux.status = undefined;
				$scope.aux.retypePassword = "";
			}

			$scope.sendForm = function() {
				Spinner.showSpinner();
				
				if ($this.checkValidForm()) {
					$scope.formularioATratar.area = $scope.aux.area.catalogId;
					$scope.formularioATratar.role = $scope.aux.userPermission.catalogId;
					UserManagementService.addInternalUser($scope.formularioATratar).success(function() {
						$this.resetFields();
						$scope.configTableParamsGestUsers.reload();
						Spinner.hideSpinner();
						$scope.addUserMessages = {
								success:true,
								msg:"USER_ADD_SUCCESS"
						};
					}).error(function(data, status, headers, config) {
						$scope.hasdata = false;
						Spinner.hideSpinner();
						$scope.addUserMessages = {
								success:false,
								msg:"USER_ADD_ERROR"
						};
					});
				} else {
					Messages.setMessageAndType(errorMessage, Messages.error);
					Spinner.hideSpinner();
				}

			};
			$scope.isSameUserLogged = function(id){
				return UserManagementService.isSameUser(id);
		
			}

			$scope.showTableUserFilters = false;
			$scope.showHideUserFilter = $this.showHideUserFilter;
			$scope.deleteUser = $this.deleteUser;
			$scope.updateUser = $this.updateUser;
			$scope.images = IMAGES_URL;
			$scope.loadAreaTableFilterSelect = $this.loadAreaTableFilterSelect;
			$scope.form ={};
			$scope.addUserMessages={};
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
		});