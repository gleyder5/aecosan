angular.module('secosanAdmin').controller(
    'SubvencionJuntasControllerAdmin',
    function($timeout, $scope, $http, $location, $log, $window, $sce, $filter, $routeParams, Spinner,HeaderTitle, Messages, NgTableParams, Becas, Instrucciones, IdentificadorPeticion, DatetimeService,
             SolicitudeAdmin, FileDtoDownload, UbicacionService, FormModelService, AdminCatalogueLocationHandler, AdminLocationCombosService, AdminSetDataFromCatalogService, ValidationUtilsService, CATALOGUE_NAMES,
             SOLICITUDE_CODES, RESPONSIBILITIES_MSG, ERR_MSG, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {
        var $this = this;

        var idToEdit = $routeParams.id;
        var viewMode = $routeParams.v;

        $this.modelo = FormModelService.getSubvencionesJuntasModel();

        AdminSetDataFromCatalogService.getCountryCatalogue($scope);

        $this.loadFormToEdit = function(data) {

            $scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
            $scope.instruccionesLeidas = true;
            $scope.viewMode = true;

            $scope.checkboxModel = {};

            $scope.auxSolicitante = {
                pais : {},
                provincia : {},
                municipio : {}
            };

            $scope.auxRepresentante = {
                pais : {},
                provincia : {},
                municipio : {}
            };

            $scope.auxContacto = {
                pais : {},
                provincia : {},
                municipio : {}
            };

            $scope.auxLugarOMedio = {
                pais : {},
                provincia : {},
                municipio : {}
            };
            AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisSolicitante);
            AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisRepresentante);
            AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisContacto);
            AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisLugarOMedio);

            var solicitudeDate = new Date(data.solicitude.fechaBoe);
            $scope.datePickerParams = DatetimeService.setDateTime(solicitudeDate);
        };

        if (idToEdit !== undefined) {
            Spinner.showSpinner();
            SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
                $this.loadFormToEdit(data);
                $scope.id_solicitude = idToEdit;
                $scope.$broadcast('sendPrevStatus', data.solicitude.status);
                $log.debug("OK getting solicitude " + idToEdit);
                Spinner.hideSpinner();
            }).error(function(data, status) {
                $log.error("Error getting solicitude " + idToEdit + ": " + status);
                Messages.setErrorMessageFromResponse(data, status);
                Spinner.hideSpinner();
            });
        }

        $this.loadProvinciaOnChange = function(comboPais) {
            AdminCatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
        };

        $this.loadMunicipioOnChange = function(comboProvincia) {
            AdminCatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);

        };


        $this.gotoWelcome = function() {
            $location.path(REDIRECT_URL.WELCOME);
        };

        $scope.identificadorSolicitud = {
            identificationRequest : ""
        };

        $scope.auxFecha = {
            fechaIncidencia : "",
            horaIncidencia : ""
        };
        $scope.tabCtrl = angular.copy(TABS_CONFIGURATION.ASOCIACION_CONSUMIDORES);

        $scope.loadProvincia = $this.loadProvinciaOnChange;
        $scope.loadMunicipio = $this.loadMunicipioOnChange;

        $scope.tipoSolicitudId = SOLICITUDE_CODES.SUBVENCIONES_JUNTAS;
        $scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
        $scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
        $scope.area_codes = AREA_CODES;

        $scope.gotoWelcome = $this.gotoWelcome;

        $scope.dtp = {
            test : ""
        };
        $scope.open = DatetimeService.openDatepicker();

        $scope.formID = FORM_ID_CODES.SUBVENCIONES_JUNTAS;

    });