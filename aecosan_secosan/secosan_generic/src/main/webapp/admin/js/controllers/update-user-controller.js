angular.module('secosanAdmin').controller('updateUserController', function($scope, $http, $log, $location, Messages,Spinner, UserManagementService, UtilService, UpdateUserService, AdminFormModelService, ERR_MSG) {

	var $this = this;

	$scope.editMode = false;

	$scope.userData = AdminFormModelService.getInternalUserRegistryModel();

	$scope.auxUpdate = {
		area : {},
		userPermission : {},
	}

	$this.changeEditMode = function() {
		$scope.editMode = !$scope.editMode;
	};

	$this.disableEditMode = function() {
		$scope.editMode = false;
	}

	$this.checkValidForm = function() {
		var valid = true;

		if ($scope.userData.password !=="" && $scope.userData.password !== $scope.auxUpdate.retypePassword) {
			Messages.setMessageAndType(ERR_MSG.PASSWORD_MATCH, Messages.error);
			valid = false;
		}
		return valid;
	};

	$this.updateUserData = function() {
		if ($this.checkValidForm()) {
			Spinner.showSpinner();
			$scope.userData.area = $scope.auxUpdate.area.catalogId;
			$scope.userData.role = $scope.auxUpdate.userPermission.catalogId;
			$scope.userData.userBlocked = !$scope.auxUpdate.status;

			UserManagementService.updateInternalUser($scope.userData).success(function() {
				$log.debug("updateUser OK");
				$scope.configTableParamsGestUsers.reload();
				$this.disableEditMode();
				UpdateUserService.setVisible('hide');
				UpdateUserService.reloadTableAfterUpload();
				Spinner.hideSpinner();
			}).error(function(data, status, headers, config) {
				$log.debug("updateUser KO");
				$scope.hasdata = false;
				Messages.setErrorMessageFromResponse(data, status);
				Spinner.hideSpinner();
			});
		}
	}

	$scope.updateUserData = $this.updateUserData;
	$scope.disableEditMode = $this.disableEditMode;
	$scope.changeEditMode = $this.changeEditMode;
	$scope.update = true;
	$scope.form ={};
});
