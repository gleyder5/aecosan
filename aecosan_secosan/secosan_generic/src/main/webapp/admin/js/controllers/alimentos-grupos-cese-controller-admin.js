angular.module('secosanAdmin').controller(
		'AlimentosGruposCeseControllerAdmin',
		function($scope, $http, $log, $window, $location, $sce, $routeParams, HeaderTitle, Messages, NgTableParams, AlimentosGrupos,Spinner, FormModelService, Instrucciones, IdentificadorPeticion,
				FileDtoDownload, CatalogueLocationHandler, ValidationUtilsService, AdminSetDataFromCatalogService, SolicitudeAdmin, LocationCombosService, CATALOGUE_NAMES, SOLICITUDE_CODES,
				RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getAlimentosGruposCeseModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);

			$this.loadFormToEdit = function(data) {
				
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				$log.debug("verInstrucciones");
//				Spinner.showSpinner();
//				$scope.instruccionesVistas = true;
//				Instrucciones.getInstrucciones(FORM_ID_CODES.GE_CESE).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//					Spinner.hideSpinner();
//				}).error(function(data) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//					Spinner.hideSpinner();
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.ALIMENTOS_GRUPOS_CESE);

			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.ALIM_GRUPOS_CESE;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.GE_CESE;
		});