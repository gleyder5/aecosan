angular.module('secosanAdmin').controller('LoginAdminController',
		function($scope, $log, $location, Messages, AuthenticationUser, FormModelService, CambioContraseniaService, REDIRECT_URL, REST_URL, IMAGES_URL, CLAVE_PATH_ADMIN) {

			var $this = this;

			this.setupController = function() {
				this.validatePresentCookie();
				Messages.clearMessagesAndForceRemove();
			}

			this.validatePresentCookie = function() {
				var cookie = AuthenticationUser.isCookiePresent();
				if (cookie) {
					AuthenticationUser.checkToken(function() {
						$log.info("user have valid token");
						$location.path($this.getForwardLocationIfValid());
					}, function(data, status, headers, config) {
						$log.info("user have invalid token" + data);
						Messages.clearMessages();
						if (status === 500) {
							Messages.setErrorMessageFromResponse(data, status);
						}
					}, REST_URL.AUTHENTICATION_CHECK_ADMIN)
				}
			};

			this.getForwardLocationIfValid = function() {
				var prevLocation = AuthenticationUser.getPrevUrl();
				if (angular.equals(prevLocation, undefined)) {
					prevLocation = REDIRECT_URL.WELCOME;
				}
				return prevLocation;
			}

			$this.setupController();

			this.redirectSuccessfullLogin = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			this.doLoginAdmin = function() {
				$scope.showloading = true;
				var form = {};
				form =angular.copy($scope.formularioATratar,form);
				AuthenticationUser.loginAdminUser(form).success(function(data) {
					$this.doLoginAdminSuccess(data);
					var firstLogin = AuthenticationUser.getUserPayloadData("firstLogin");
					if (firstLogin) {
						var cambioContraseniaParams = {
							'oldPassword' : "",
							'newPassword' : "",
							'retypeNewPassword' : ""
						}
						CambioContraseniaService.showCambioContraseniaWindow(cambioContraseniaParams);
					}
				}).error(function(data, status, headers, config) {
					$this.doLoginAdminError(data, status, headers, config);
				});
			};

			this.doLoginAdminSuccess = function(data) {
				$log.debug("loginAdmin OK");
				$scope.showloading = false;
				AuthenticationUser.handleAdminLogin(data, $this.redirectSuccessfullLogin());
			};

			this.doLoginAdminError = function(data, status, headers, config) {
				$log.debug("loginAdmin KO");
				$scope.showloading = false;
				$scope.hasdata = false;
				$scope.formularioATratar.password="";
				if (status === 500) {
					Messages.setErrorMessageFromResponse(data, status);
				} else if (status === 401) {
					$log.debug("loginUser 401");
					$scope.loginErrorMessage = AuthenticationUser.getAuthenticationErrorFromRemoteCodeErrors(data.code);
				}
			};

			this.redirectAfterClaveAdminError = function() {
				$log.debug("redirectAfterClaveAdminError");
				AuthenticationUser.handleClaveLoginAdminError(function(code) {
					$log.debug("code");
					$log.debug(code);
					$scope.loginClaveErrorMessage = AuthenticationUser.getAuthenticationErrorFromRemoteCodeErrors(code)
				});
			}

			$scope.formularioATratar = FormModelService.getUserLoginModel();
			$scope.sendForm = $this.doLoginAdmin;
			$scope.redirectAfterClaveAdminSuccess = $this.redirectSuccessfullLogin;
			$scope.redirectAfterClaveAdminError = $this.redirectAfterClaveAdminError;
			$scope.showloading = false;
			$scope.images = IMAGES_URL;
			$scope.clavePath = CLAVE_PATH_ADMIN;
		});
