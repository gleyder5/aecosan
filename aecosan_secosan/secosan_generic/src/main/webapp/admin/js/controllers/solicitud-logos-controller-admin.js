angular.module('secosanAdmin').controller(
		'SolicitudLogosControllerAdmin',
		function($scope, $http, $location, $log, $window, $sce, $routeParams, HeaderTitle,Spinner, Messages, NgTableParams, SolicitudLogos, SolicitudeAdmin, FormModelService, Instrucciones, IdentificadorPeticion,
				FileDtoDownload, CatalogueLocationHandler, ValidationUtilsService, AdminSetDataFromCatalogService, LocationCombosService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG,
				SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getSolicitudLogosModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);

			$this.loadFormToEdit = function(data) {
			
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				$log.debug("verInstrucciones");
//				$scope.instruccionesVistas = true;
//				Instrucciones.getInstrucciones(SOLICITUDE_CODES.SOLICITUD_LOGOS).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//				}).error(function(data, status) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};
			
			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.SOLICITUD_LOGOS);
		

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.SOLICITUD_LOGOS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.LOGOS;
		});