angular.module('secosanAdmin').controller('MonitoringController', function($scope, $log, NgTableParams, MonitoringService, Messages) {

	var $this = this;

	$scope.statusFilter = [ {
		'id' : 'false',
		'title' : 'Error'
	}, {
		'id' : 'true',
		'title' : 'Ok'
	} ];

	$scope.connectorNameFilter = [ {
		'id' : 'CLAVE',
		'title' : 'CLAVE'
	}, {
		'id' : 'EPAGO',
		'title' : 'EPAGO'
	}, {
		'id' : 'SIGM',
		'title' : 'SIGM'
	}, {
		'id' : 'SNEC',
		'title' : 'SNEC'
	} ];

	$scope.configTableParamsMonitoring = new NgTableParams({
		count : 10,
		sorting : {
			registerDate : "desc"
		},
	}, {
		// page size buttons (right set of buttons in demo)
		counts : [],
		// determines the pager buttons (left set of buttons in demo)
		paginationMaxBlocks : 13,
		paginationMinBlocks : 2,
		getData : function($defer, params) {
			MonitoringService.getMonitoringList(params).success(function(data) {
				var total = data.recordsTotal;
				if (total !== data.recordsFiltered) {
					total = data.recordsFiltered;
				}
				$scope.hasdata = $this.hasData(total);
				params.total(total);
				$defer.resolve(data.data);
			}).error(function(data, status, headers, config) {
				Messages.setErrorMessageFromResponse(data, status);
				$scope.hasdata = false;
			});
		}
	});
	$this.hasData = function(recordsTotal) {
		return recordsTotal > 0
	}
	
	$this.showHideMonitoringFilter = function() {
		if ($scope.showTableMonitoringFilters) {
			$scope.showTableMonitoringFilters = false;
			$scope.configTableParamsMonitoring.filter({});
		} else {
			$scope.showTableMonitoringFilters = true;
		}
	};

	$scope.showTableMonitoringFilters = false;
	$scope.showHideMonitoringFilter = $this.showHideMonitoringFilter;

	$scope.options = {
		chart : {
			type : 'multiBarHorizontalChart',
			height : 450,
			x : function(d) {
				return d.label;
			},
			y : function(d) {
				return d.value;
			},
			showControls : false,
			stacked : true,
			showValues : true,
			duration : 500,
			xAxis : {
				showMaxMin : false
			},
			yAxis : {
				axisLabel : 'Values',
				tickFormat : function(d) {
					return d3.format('')(d);
				}
			}
		}
	};
	$scope.data = {};

	MonitoringService.getStatistics().success(function(data) {
		// $scope.dataRaw = data;
		$scope.data = data.statisticsNVD3;
	}).error(function(data, status, headers, config) {
		$log.debug("data: " + data);
		$log.debug("status: " + status);
		$log.debug("headers: " + headers);
		$log.debug("config: " + config);
		Messages.setErrorMessageFromResponse(data, status);
	});
	$scope.hasdata = false;
});
