angular.module('secosanAdmin').controller(
		'ComplementosAlimenticiosCeseControllerAdmin',
		function($scope, $http, $log, $window, $location, $sce, $routeParams, HeaderTitle, Messages, Spinner, NgTableParams, ComplementosAlimenticios, SolicitudeAdmin, FormModelService, AdminCatalogueHandler,
				Instrucciones, IdentificadorPeticion, Spinner,ValidationUtilsService, FileDtoDownload, CatalogueLocationHandler, AdminSetDataFromCatalogService, LocationCombosService, CATALOGUE_NAMES, ERR_MSG,
				SOLICITUDE_CODES, RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getComplementosAlimenticiosCeseModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);

			$this.loadFormToEdit = function(data) {
				
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					provincia : {},
					pais : {},
					municipio : {}
				};
				$scope.auxRepresentante = {
					provincia : {},
					pais : {},
					municipio : {}
				};
				$scope.auxContacto = {
					provincia : {},
					pais : {},
					municipio : {}
				}

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				Spinner.showSpinner();
//				$log.debug("verInstrucciones");
//				Instrucciones.getInstrucciones(SOLICITUDE_CODES.COMP_ALIM_CESE).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//					Spinner.hideSpinner();
//				}).error(function(data) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//					Spinner.hideSpinner();
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};
			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.COMPLEMENTOS_ALIMENTICIOS_CESE_CONTROLLER);
			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.COMP_ALIM_CESE;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.CA_CESE;
		});