angular.module('secosanAdmin').controller(
		'FinanciacionAlimentosInclusionControllerAdmin',
		function($scope, $http, $log, $window, $location, $sce, $routeParams,Spinner, HeaderTitle, Messages, NgTableParams, FinanciacionAlimentos, FormModelService, Instrucciones, IdentificadorPeticion,
				FileDtoDownload, CatalogueLocationHandler, SolicitudeAdmin, ValidationUtilsService, LocationCombosService, AdminSetDataFromCatalogService, RegistryNumberFormatService, CATALOGUE_NAMES,
				SOLICITUDE_CODES, RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION,DatetimeService) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getFinanciacionAlimentosInclusionModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);
			AdminSetDataFromCatalogService.getAdminModeCatalogue($scope);

			$this.loadFormToEdit = function(data) {

				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				var fiber = $scope.formularioATratar.fiber.fiber;
				if (fiber) {
					$scope.aux.fiber = "true";
				} else if (fiber === "") {
					$scope.aux.fiber = "";
				} else {
					$scope.aux.fiber = "false";
				}


                var administrationMode= $scope.formularioATratar.administrationMode;
                if (administrationMode) {
                    $scope.aux.administrationMode = {catalogId:administrationMode.catalogId,catalogValue:administrationMode.catalogValue};

                    $scope.catalogAdminMode.forEach(function (it,k,obj) {
                        if($scope.formularioATratar.administrationMode.id == it.catalogId ){
                            $scope.aux.administrationMode  = it;
                            return false;
                        }
                    });
                }


                var solicitudeDate = new Date(data.solicitude.inscriptionDate);
                $scope.datePickerParams = DatetimeService.setDate(solicitudeDate);
                RegistryNumberFormatService.putCompanyRegisterNumberInView($scope, $scope.formularioATratar.companyRegisterNumber);
				RegistryNumberFormatService.putProductReferenceNumberInView($scope, $scope.formularioATratar.productReferenceNumber);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				$log.debug("verInstrucciones");
//				$scope.instruccionesVistas = true;
//				Instrucciones.getInstrucciones(SOLICITUDE_CODES.FINA_ALIM_INCLUSION).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//				}).error(function(data) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.resetFiberValues = function() {
				if ($scope.aux.fiber !== 'true') {
					$scope.formularioATratar.fiber.fiberType = "";
					$scope.formularioATratar.fiber.fiberQuantity = "";
				}
			}

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.companyRegisterNumberAux = {
				numRegEmp1 : "",
				numRegEmp2 : "",
				numRegEmp3 : ""
			}
			$scope.productReferenceNumber = {
				numRegPro1 : "",
				numRegPro2 : "",
				numRegPro3 : "",
				numRegPro4 : ""
			}

			$scope.aux = {
				administrationMode : {}
			}

			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.FINA_ALIM_INCLUSION);
			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.FINA_ALIM_INCLUSION;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.INCLUSION_UME;
			$scope.showFormAnexoII = true;
		});