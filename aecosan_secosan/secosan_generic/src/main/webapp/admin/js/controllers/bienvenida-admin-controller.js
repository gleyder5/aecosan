angular.module('secosanAdmin').controller(
		'BienvenidaAdminController',
		function(NgTableParams, $modal, $scope, $filter, $log, $location, Messages, AdminCatalogueHandler, FormModelService, UtilService, AdminInstrucciones, AuthenticationUser, FileDtoDownload,
				ModalService, ModalParamsService, WelcomeMenu,AdminDocTypeService,SolicitudeAdmin, DatePickerPopup, EntityUrlService, AdminSetDataFromCatalogService, ERR_MSG, FILES_CONSTANTS, USER_ROLE_CODE,
				CATALOGUE_NAMES, IMAGES_URL,STATUS_ENVIADA_REPLACE,MENU_ADMIN,MENU_TRAMITADOR) {

			var $this = this;
			this.configParams = undefined;
			this.setupController = function() {
				if ($this.configParams !== undefined) {
					$this.configParams.reload();
				}
			};

			$this.getSolicitudesList = function() {
				$this.configParams = new NgTableParams({
					count : 5,
					sorting : {
						id : "desc"
					},
				}, {
					counts : [],
					paginationMaxBlocks : 3,
					paginationMinBlocks : 2,
					getData : function($defer, params) {
						SolicitudeAdmin.getSolicitudeListTramitador(params).success(function(data) {
							var total = data.recordsTotal;
							if (total !== data.recordsFiltered) {
								total = data.recordsFiltered;
							}
							params.total(total);
							$scope.hasdata = $this.hasData(total);
							$defer.resolve(data.data);

						}).error(function(data, status, headers, config) {
							$scope.hasdata = false;
							Messages.setErrorMessageFromResponse(data, status);
						});
					}
				});

				return $this.configParams;
			}
			$this.setupController();

			$this.hasData = function(recordsTotal) {
				return recordsTotal > 0
			}

			$this.getInstrucciones = function() {

				AdminInstrucciones.getInstruccionesPagoList().success(function(data) {
					$scope.instrucciones = data;
				}).error(function(data, status) {
					Messages.setErrorMessageFromResponse(data, status);
				});
			}

			var role = AuthenticationUser.getUserPayloadData("roleId");
			if (role === USER_ROLE_CODE.ADMINISTRADOR) {
				$scope.menuOptions = MENU_ADMIN;
				WelcomeMenu.setWelcomeMenuItem("gestionusuarios");
				$scope.titleWelcome ="WELCOME_TITLE_ADMIN";
				$scope.titleClass="bienvenidaAdmin"
			} else {
				$scope.menuOptions = MENU_TRAMITADOR;

				WelcomeMenu.setWelcomeMenuItem("solicitudes");
				$scope.titleWelcome ="WELCOME_TITLE_TRAMITADOR";
				$scope.titleClass="bienvenidaTramitador";
				$scope.configTableParams = $this.getSolicitudesList();
				$this.getInstrucciones();
			}

			$this.verInstruccionesSolicitud = function(solicitudeCode) {
				
				$scope.instruccionesVistas = true;
				AdminInstrucciones.getInstrucciones(solicitudeCode,function(data) {
					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
				},function(data, status) {
					Messages.setErrorMessageFromResponse(data, status);
				});
			};

			$this.verInstruccionesPago = function(idFormulario) {
				
				$scope.instruccionesVistas = true;
				AdminInstrucciones.getInstruccionesPago(idFormulario).success(function(data) {
					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
				}).error(function(data, status) {
					Messages.setErrorMessageFromResponse(data, status);
				});
			};

			$this.showHideFilter = function() {
				if ($scope.showTableFilters) {
					$scope.showTableFilters = false;
					if ($this.configParams !== undefined) {
						$this.configParams.filter({});
					}

				} else {
					$scope.showTableFilters = true;
				}
			};

			$this.prepareFileToUpload = function(id, file) {
				return {
					id : id,
					catalogValue : file.filename,
					base64 : file.base64,
				}
			};

			$this.uploadInstruccionesPago = function(idFormulario, file) {
			
				if (file !== undefined) {
					var fileUpload = $this.prepareFileToUpload(idFormulario, file);
					AdminInstrucciones.uploadInstructionsToPay(fileUpload,function() {
						ModalService.showModalWindow(ModalParamsService.uploadFileSuccess());
						$this.getInstrucciones();
					},function() {
						ModalService.showModalWindow(ModalParamsService.uploadFileError());
						
					});
				}
			};

			$this.uploadInstruccionesSolicitud = function(idSolicitud, file) {
				
				if (file !== undefined) {
					var fileUpload = $this.prepareFileToUpload(idSolicitud, file);
					AdminInstrucciones.uploadInstructions(fileUpload,function() {
						ModalService.showModalWindow(ModalParamsService.uploadFileSuccess());
						$this.getInstrucciones();
					},function() {
						ModalService.showModalWindow(ModalParamsService.uploadFileError());
					});
				}
			};

			$this.getURLFromEntity = function(entityName) {
				return (EntityUrlService.getEntityURL(entityName));
			};

			$scope.solicitudeTypes = function($column) {
			
				AdminCatalogueHandler.getCatalogue(CATALOGUE_NAMES.SOLICITUDE_TYPE).success(function(data) {
					$column.data = AdminCatalogueHandler.generateCatalogueDataForShow(data);

				}).error(function() {
					
					AdminCatalogueHandler.generateDefaultErrorMessage();
				});
			}
			
			$this.fixStatusForTramitador = function (catalogItem){
				var catalogName = catalogItem.catalogValue;
				if(catalogItem.id ==STATUS_ENVIADA_REPLACE.id){
					 catalogName = STATUS_ENVIADA_REPLACE.catalogValue;
				}
				return catalogName;
			}
			
			$scope.solicitudStatusFilter = function ($column){
				AdminSetDataFromCatalogService.getSolicitudeStatusCatalogByArea(SolicitudeAdmin.getUserLoggedAreaId(),function(data) {
					var rowdata = {
							catalog :data
					};
					$column.data = AdminCatalogueHandler.generateCatalogueDataForShow(rowdata);
					
				},function(data, status) {
				
					AdminCatalogueHandler.generateDefaultErrorMessage();
				});
			}
			$scope.open = function(size, dateValues, params, paramName) {
				DatePickerPopup.datePickerConfig($scope, $modal, $filter, size, dateValues, params, paramName);
			}

			/* añadir filtro tabla */
			$scope.verInstruccionesSolicitud = $this.verInstruccionesSolicitud;
			$scope.verInstruccionesPago = $this.verInstruccionesPago;
			$scope.uploadInstruccionesPago = $this.uploadInstruccionesPago;
			$scope.uploadInstruccionesSolicitud = $this.uploadInstruccionesSolicitud;
			$scope.getURLFromEntity = this.getURLFromEntity;

			$scope.maxfilesize = FILES_CONSTANTS.MAXSIZE;
			$scope.minfilesize = FILES_CONSTANTS.MINSIZE;
			$scope.fileType = FILES_CONSTANTS.FILE_TYPE;
			$scope.showHideFilter = $this.showHideFilter;
			$scope.fixStatusForTramitador =  $this.fixStatusForTramitador;
			$scope.showTableFilters = false;
			$scope.images=IMAGES_URL;
		});
