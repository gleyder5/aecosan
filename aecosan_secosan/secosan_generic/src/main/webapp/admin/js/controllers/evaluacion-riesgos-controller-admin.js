angular.module('secosanAdmin').controller(
		'EvaluacionRiesgosControllerAdmin',
		function($scope, $http, $location, $log, $window, $sce, $routeParams, HeaderTitle, Spinner, Messages, NgTableParams, EvaluacionRiesgos, FormModelService, ServiciosPagoService, Instrucciones,
				IdentificadorPeticion, SolicitudeAdmin, FileDtoDownload, ValidationUtilsService, LocationCombosService, UtilService, CatalogueLocationHandler, AdminSetDataFromCatalogService,
				PagoService, ERR_MSG, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getEvaluacionriesgosModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);
			AdminSetDataFromCatalogService.getPaymentTypeCatalogue($scope);

			$this.loadFormToEdit = function(data) {

				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxPago = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				$scope.auxServicio = {
					sujetoTasa : {},
					servicioSolicitado : {}
				};

				if ($scope.formularioATratar.payTypeService.idPayType !== "") {
					$scope.auxServicio.sujetoTasa.catalogId = $scope.formularioATratar.payTypeService.idPayType;
					$this.loadService($scope.auxServicio.sujetoTasa.catalogId, true);
				}
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			// $this.verInstrucciones = function() {
			// $log.debug("verInstrucciones");
			// Instrucciones.getInstrucciones(SOLICITUDE_CODES.EVALUACION_RIESGOS).success(function(data) {
			// FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
			// $log.debug("Download intrucciones success");
			// }).error(function(data, status) {
			// $log.error("Download intrucciones error");
			// Messages.setErrorMessageFromResponse(data, status);
			// });
			// };

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

		
			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$this.loadService = function(id, positioning) {
				if (id !== undefined) {
					ServiciosPagoService.getServiciosByPago(id).success(function(data) {
						$scope.catalogTipoServicio = {};
						$scope.catalogTipoServicio = data;

						if (positioning) {
							$scope.auxServicio.servicioSolicitado.id = $scope.formularioATratar.payTypeService.idService;
						} else {
							$scope.auxServicio.servicioSolicitado = $scope.catalogTipoServicio[-1];
						}

						$log.debug("OK getting catalog from ServiciosPagoService");
					}).error(function(data, status) {
						$log.error("Error getting catalog from ServiciosPagoService" + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				} else {
					$scope.catalogTipoServicio = {};
				}
			};
			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.EVALUACION_RIESGOS);
			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

			// $scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

			// $scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.EVALUACION_RIESGOS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.payment_tasa_code = CATALOGUE_NAMES.PAYMENT_TASA_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.loadService = $this.loadService;
			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.RIESGOS;

		});