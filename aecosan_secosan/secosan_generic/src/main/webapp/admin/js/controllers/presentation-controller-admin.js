angular.module('secosanAdmin').controller(
		'presentationController',
		function($scope, $log, $location, Messages, ModalService, ModalParamsService) {

			var $this = this;

			$this.composeObject = function() {
				return {
					containerType : $scope.auxPresentation.tipoEnvase,
					contents : $scope.auxPresentation.contenidoEnvase,
					numberOfContainer : $scope.auxPresentation.numeroEnvases,
					flavour : $scope.auxPresentation.saborProducto,
					companyPriceSale : $scope.auxPresentation.precioVenta
				}
			};

			$this.emptyFields = function() {
				$scope.auxPresentation = {};
				$scope.auxPresentation.tipoEnvase = "";
				$scope.auxPresentation.contenidoEnvase = "";
				$scope.auxPresentation.numeroEnvases = "";
				$scope.auxPresentation.saborProducto = "";
				$scope.auxPresentation.precioVenta = "";
			};

			$this.emptyFields();

			$this.checkFields = function() {
				return ($scope.auxPresentation.tipoEnvase.length !== 0 && $scope.auxPresentation.contenidoEnvase.length !== 0 && $scope.auxPresentation.numeroEnvases.length !== 0
						&& $scope.auxPresentation.saborProducto.length !== 0 && $scope.auxPresentation.precioVenta.length !== 0);
			};

			$this.addPresentation = function() {
				if ($scope.formularioATratar.presentations.length < 4) {
					if ($this.checkFields()) {
						var newPresentation = $this.composeObject();
						$scope.formularioATratar.presentations.push(newPresentation);
						$this.emptyFields();
					} else {
						ModalService.showModalWindow(ModalParamsService.financialProductError());
					}
				} else {
					ModalService.showModalWindow(ModalParamsService.maxItemsError());
					$this.emptyFields();
				}
			};

			$this.deletePresentation = function(index) {
				$scope.formularioATratar.presentations.splice(index, 1);
			};

			$scope.addPresentation = $this.addPresentation;
			$scope.deletePresentation = $this.deletePresentation;
		});
