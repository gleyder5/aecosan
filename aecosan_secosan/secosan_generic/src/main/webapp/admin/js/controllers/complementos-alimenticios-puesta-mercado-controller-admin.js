angular.module('secosanAdmin').controller(
		'ComplementosAlimenticiosPuestaMercadoControllerAdmin',
		function($scope, $http, $log, $window, $location, $sce, $routeParams, HeaderTitle, NgTableParams, ComplementosAlimenticios, Instrucciones, IdentificadorPeticion, Messages, ModalService,
				UtilService, SolicitudeAdmin, ModalParamsService, FileDtoDownload, Spinner,ValidationUtilsService, UbicacionService, FormModelService, CatalogueLocationHandler, AdminSetDataFromCatalogService,
				LocationCombosService, RegistryNumberFormatService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getComplementosAlimenticiosPuestaMercadoModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);
			AdminSetDataFromCatalogService.getFormatsCatalogue($scope);

			$this.loadFormToEdit = function(data) {
				
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.aux = {
					paisProcedencia : {},
					paisComercializacionPrevia : {},
					idIsProductMaker : ""
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				// Datos identificativos adicionales
				var isProductMaker = $scope.formularioATratar.datosIdentificativosAdicionales.isProductMaker;
				if (isProductMaker) {
					$scope.aux.idIsProductMaker = "true";
				} else if (isProductMaker === "") {
					$scope.aux.idIsProductMaker = "";
				} else {
					$scope.aux.idIsProductMaker = "false";
				}

				RegistryNumberFormatService.putRGSEAAInView($scope, $scope.formularioATratar.datosIdentificativosAdicionales.rgseaa);

				$scope.formasToShow = $scope.formularioATratar.presentaciones;
				$scope.aux.paisProcedencia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisProcedencia;
				$scope.aux.paisComercializacionPrevia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisComercializacionPrevia;
			};

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				$log.debug("verInstrucciones");
//				Instrucciones.getInstrucciones(SOLICITUDE_CODES.COMP_ALIM_PUESTA_MERCADO).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//				}).error(function(data, status) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.resetIngredientValues = function() {
				$scope.formularioATratar.ingredientes.incluyeVitaminas = "";
				$scope.formularioATratar.ingredientes.incluyeNuevosIngredientes = "";
				$scope.formularioATratar.ingredientes.nuevoIngrediente = "";
				$scope.formularioATratar.ingredientes.otrasSustancias = "";
			}

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxNumRGSEAA = {
				"auxNumRGSEAA1" : "",
				"auxNumRGSEAA2" : "",
				"auxNumRGSEAA3" : ""
			};

			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO);
			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.COMP_ALIM_PUESTA_MERCADO;
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.otherForm = CATALOGUE_NAMES.OTHER_FORM_CODE;
			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.CA_PUESTA_MERCADO;
		});