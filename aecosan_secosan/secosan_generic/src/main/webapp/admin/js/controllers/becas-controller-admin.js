angular.module('secosanAdmin').controller(
		'BecasConcejoConsumidoresControllerAdmin',
		function($timeout, $scope, $http, $location, $log, $window, $sce, $filter, $routeParams, Spinner,HeaderTitle, Messages, NgTableParams, Becas, Instrucciones, IdentificadorPeticion, DatetimeService,
				SolicitudeAdmin, FileDtoDownload, UbicacionService, FormModelService, AdminCatalogueLocationHandler, AdminLocationCombosService, AdminSetDataFromCatalogService, ValidationUtilsService, CATALOGUE_NAMES,
				SOLICITUDE_CODES, RESPONSIBILITIES_MSG, ERR_MSG, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getBecasModel();
			$scope.becaId= idToEdit;
			$scope.checkBecaId= function (solicitud) {
				return parseInt($scope.becaId)==solicitud.id;
            };

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);

			$this.loadFormToEdit = function(data) {
			
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
                Becas.getIdiomasBecasList(idToEdit).success(function (resp) {
                    $scope.langs.forEach(function (t) {
                        resp.forEach(function (v) {
                            if(v.nombre==t.nombre){
                                t.puntos = v.puntos;
                            }
                        });
                    });
                    if(resp.length>3){
                        $scope.otherLangs = resp.slice(3,resp.length);
                        $scope.formularioATratar.languages  = $scope.langs ;
                    }
                });



                $scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

                $scope.auxLugarOMedio = {
                    pais : {},
                    provincia : {},
                    municipio : {}
                };
				AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisSolicitante);
				AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisRepresentante);
				AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisContacto);
                AdminLocationCombosService.loadCombo($scope, data, AdminCatalogueLocationHandler.domObjectName.paisLugarOMedio);

				var solicitudeDate = new Date(data.solicitude.solicitante.fechaNac);
				$scope.datePickerParams = DatetimeService.setDateTime(solicitudeDate);
			};

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				AdminCatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				AdminCatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);

			};

//			$this.verInstrucciones = function() {
//				$log.debug("verInstrucciones");
//				Instrucciones.getInstrucciones(SOLICITUDE_CODES.QUEJA).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//				}).error(function(data, status) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxFecha = {
				fechaIncidencia : "",
				horaIncidencia : ""
			};

            $this.langs = [{
                nombre: 'Ingles',
                puntos: 0},
                {
                    nombre: 'Aleman',
                    puntos: 0},
                {
                    nombre: 'Frances',
                    puntos:  0
                }];

            $scope.tabCtrl = angular.copy(TABS_CONFIGURATION.BECAS_CONSUMIDORES);

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.BECAS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.dtp = {
				test : ""
			};
			$scope.open = DatetimeService.openDatepicker();

			$scope.formID = FORM_ID_CODES.BECAS;

			$scope.langs = $this.langs;
		});