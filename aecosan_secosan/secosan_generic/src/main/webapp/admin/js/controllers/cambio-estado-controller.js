angular.module('secosanAdmin').controller('CambioEstadoController',
		function($scope,$routeParams, $log, Messages, AdminSetDataFromCatalogService, SolicitudeAdmin, FILES_CONSTANTS, STATUS_CODES, REDIRECT_URL,IMAGES_URL,ModalService,ModalParamsService,LANGUAGES_LIST,AREA_CODES,Becas,FormModelService,HistorialSolicitudesService,PROCEDIMIENTOS_ALTERNOS,VALIDATION_PATTERN) {
			var $this = this;

			$scope.procedimentos_portafirma = PROCEDIMIENTOS_ALTERNOS.PORTAFIRMA;
			console.log($scope.solicitaCopiaAutentica);
			// TODO COMPROBAR TIPO DE USUARIO
			$scope.aux = {
				status : {
					id : "",
					catalogValue : ""
				}
			};
            $scope.idSolicitude =  $routeParams.id;
            $scope.info = "";

            $this.modelo = FormModelService.getBecasModel();
            $this.langs = [{
                nombre: 'Ingles',
                puntos: 0},
                {
                    nombre: 'Aleman',
                    puntos: 0},
                {
                    nombre: 'Frances',
                    puntos:  0
                }];


			AdminSetDataFromCatalogService.getSolicitudeStatusCatalog($scope);

			$scope.$on('sendPrevStatus', function(event, data) {
				$scope.aux.status.id = data;
			});

			$this.checkValidSizeAndFormat = function() {
				if(!$scope.file){
					return false;
				}
				return $scope.file.filesize < (FILES_CONSTANTS.MAXSIZE * 1000) && $scope.file.filetype === FILES_CONSTANTS.FILE_TYPE;
			}
            $this.setSelectedOption  = function (arr,val,fieldScoped) {
                var found = -1;
                arr.forEach(function (v,k) {
                    if(v.numero==val){
                        found  = k ;
                        return;
                    };
                });
                if(found!=-1){
                    $scope.formularioATratar[fieldScoped] = arr[found];
                }
            };

            $this.sendChangeStatusSolicitud = function (typeSubmit) {

                var newStatus = {
                    solicitudeId : $scope.id,
                    statusId : $scope.aux.status.id,
                    statusText : $scope.info,
                    typeSubmit : typeSubmit,
                    score    :$scope.score,
                    otraTitulacion :$scope.formularioATratar.otraTitulacion,
                    otraTitulacionPuntos :$scope.formularioATratar.otraTitulacionPuntos != undefined     ? $scope.formularioATratar.otraTitulacionPuntos.numero:0,
                    puntosExpediente :   $scope.formularioATratar.puntosExpediente != undefined     ? $scope.formularioATratar.puntosExpediente.numero:0,
                    puntosOfimatica  :   $scope.formularioATratar.puntosOfimatica != undefined      ? $scope.formularioATratar.puntosOfimatica.numero:0,
                    puntosVoluntariado : $scope.formularioATratar.puntosVoluntariado != undefined   ? $scope.formularioATratar.puntosVoluntariado.numero:0,
                    puntosEntrevista  :  $scope.formularioATratar.puntosEntrevista != undefined     ? $scope.formularioATratar.puntosEntrevista.numero:0,
                    area: $scope.areaCode,
                    onlySendEmail:false,
                    emailRemision :$scope.emailRemision
                };
                if($scope.areaCode == AREA_CODES.BECAS){
                	newStatus['languages']	=	$scope.langs.concat($scope.otherLangs);
                }
                if($scope.file!==undefined){
                	newStatus['fileName']  = $scope.file.filename;
					newStatus['fileB64']  = $scope.file.base64;
				}
                if($scope.fileCartera!==undefined){
                    newStatus['fileCarteraName']  = $scope.fileCartera.filename;
                    newStatus['fileCarteraB64']  = $scope.fileCartera.base64;
                }
                if($scope.fileCartera!==undefined){
                    newStatus['fileCarteraName']  = $scope.fileCartera.filename;
                    newStatus['fileCarteraB64']  = $scope.fileCartera.base64;
                }
                if($scope.fileCopiaAutentica!==undefined){
                    newStatus['fileCopiaAutenticaName']  = $scope.fileCopiaAutentica.filename;
                    newStatus['fileCopiaAutenticaB64']  = $scope.fileCopiaAutentica.base64;
                }
                SolicitudeAdmin.updateSolicitudeStatus(newStatus,REDIRECT_URL.WELCOME,$this.hideSpinner,$this.hideSpinner);
            };
			$this.changeSolicitudeStatus = function(typeSubmit) {
				$scope.showNotificationSpinner = true;
				$log.debug($scope.info);
				if ($scope.id !== undefined) {
					if($scope.areaCode==AREA_CODES.BECAS){
                        $this.sendChangeStatusSolicitud(typeSubmit);
					}
					else if(!$this.checkValidSizeAndFormat()){
						ModalService.showModalWindow(ModalParamsService.formatFileError());
						$scope.showNotificationSpinner = false;
						return false;
					}else {
						$this.sendChangeStatusSolicitud(typeSubmit);
					}
				} else {
					Messages.setMessageAndType("No se ha indicado el identificador de la petición a modificar", Messages.error);
				}
			};
			
			$this.hideSpinner = function(){
				$scope.showNotificationSpinner = false;
			};
			$this.getAggregation = function () {
			    $this.puntosFields = [
			        "puntosOfimatica",
			        // "puntosEntrevista",
                    "puntosExpediente",
                    "puntosVoluntariado",
                    "otraTitulacionPuntos"
                ];

			    var acum  = 0 ;
			    $this.puntosFields.forEach(function (t) {
                    if($scope.formularioATratar!=undefined) {
                        if ($scope.formularioATratar[t] != undefined) {
                            if ($scope.formularioATratar[t].hasOwnProperty('numero')) {
                                acum += parseFloat($scope.formularioATratar[t].numero);
                            } else if ($scope.formularioATratar[t] > 0) {
                                acum += parseFloat($scope.formularioATratar[t]);
                            }

                        }
                    }
                });

			    if($scope.formularioATratar!=undefined) {
			        var result = 0 ;
                    if( $scope.totalPuntosIdiomas !=undefined){
                            result += $scope.totalPuntosIdiomas;
                    }
                    return  result+acum

                }
                return  0 ;

            };

			$scope.statusChanged = function () {
                $scope.info  = "";
                SolicitudeAdmin.getSolicitudeById($routeParams.id).success(function(data) {
                    $scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
                    $scope.beca =$scope.formularioATratar;
                    $scope.totalScore = $scope.beca.score;
                    $this.setSelectedOption($scope.numberListExpediente,$scope.formularioATratar.puntosExpediente,'puntosExpediente');
                    $this.setSelectedOption($scope.numberListOfimatica,$scope.formularioATratar.puntosOfimatica,'puntosOfimatica');
                    $this.setSelectedOption($scope.numberListOfimatica,$scope.formularioATratar.puntosVoluntariado,'puntosVoluntariado');
                    $this.setSelectedOption($scope.numberListEntrevista,$scope.formularioATratar.puntosEntrevista,'puntosEntrevista');
                    $this.setSelectedOption($scope.numberListOtraTitulacion,$scope.formularioATratar.otraTitulacionPuntos,'otraTitulacionPuntos');
                    // $scope.aux.status.id = data.solicitude.status;
                    // if($scope.areaCode==AREA_CODES.BECAS ) {
                        HistorialSolicitudesService.getHistorialBySolicitudAndStatus($scope.idSolicitude, $scope.aux.status.id).success(function (resp) {
                            if (resp) {
                                if (resp.descripcion && parseInt(resp.status.id) != 2) {
                                    $scope.info = resp.descripcion;
                                }
                            }
                        });
                    // }
                });

            };

            $scope.range = function(min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = min; i <= max; i += step) {
                    input.push({numero:i});
                }
                return input;
            };
			
            $scope.addLanguage = function ($evt) {
            	$evt.preventDefault();
				if($scope.otherLangs.length<2) {
                    $scope.otherLangs.push({
                        nombre: '',
                        puntos: 0
                    });
                }
            };

            $scope.getReportResolucionProvisional  = function ($event) {
                $event.preventDefault();
                $scope.showNotificationSpinner = true;
                Becas.getReporteResolucionProvisional($routeParams.id,$this.hideSpinner);
            };

            $scope.removeLang = function ($evt,idx) {
            	$evt.preventDefault();
            	$scope.otherLangs.splice(idx,1);
                $scope.langPointsChange()
            };

            $scope.langPointsChange = function () {
					$scope.totalPuntosIdiomas = 0;

					$scope.langs.forEach(function (t) {
                        if(t.puntos>0 ||t.puntos.length>0){
                            $scope.totalPuntosIdiomas  += parseFloat(t.puntos);
                        }
					});
					if($scope.otherLangs.length>0){
                        $scope.otherLangs.forEach(function (t) {
                            if(t.puntos>0 ||t.puntos.length>0){
                                $scope.totalPuntosIdiomas  += parseFloat(t.puntos);
                            }

                        });
					}

            };

            if($scope.areaCode==AREA_CODES.BECAS ){
                SolicitudeAdmin.getSolicitudeById($routeParams.id).success(function(data) {
                    $scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
                    $this.setSelectedOption($scope.numberListExpediente,$scope.formularioATratar.puntosExpediente,'puntosExpediente');
                    $this.setSelectedOption($scope.numberListOfimatica,$scope.formularioATratar.puntosOfimatica,'puntosOfimatica');
                    $this.setSelectedOption($scope.numberListOfimatica,$scope.formularioATratar.puntosVoluntariado,'puntosVoluntariado');
                    $this.setSelectedOption($scope.numberListOtraTitulacion,$scope.formularioATratar.otraTitulacionPuntos,'otraTitulacionPuntos');
                    $this.setSelectedOption($scope.numberListEntrevista,$scope.formularioATratar.puntosEntrevista,'puntosEntrevista');

                    Becas.getIdiomasBecasList($routeParams.id).success(function (resp) {
                        $scope.langs   = resp;
                        $scope.totalPuntosIdiomas = 0;
                        $scope.formularioATratar.languages  = $scope.langs;
                        if($scope.langs.length>3){
                            $scope.otherLangs = $scope.langs.slice(3,$scope.langs.length);
                            $scope.langs =$scope.langs.slice(0,3) ;
                            $scope.formularioATratar.languages  = $scope.langs;
                        }
                        $scope.langPointsChange();
                        HistorialSolicitudesService.getHistorialBySolicitudAndStatus($scope.idSolicitude, $scope.aux.status.id).success(function (resp) {
                            if (resp) {
                                if(resp.descripcion && parseInt(resp.status.id)!=2){
                                    $scope.info = resp.descripcion;
                                }
                            }
                        });
                    });
                    // Spinner.hideSpinner();
                }).error(function(data, status) {
                    $log.error("Error getting solicitude " + $scope.idSolicitude + ": " + status);
                    Messages.setErrorMessageFromResponse(data, status);
                    // Spinner.hideSpinner();
                });
            }

            $scope.showWorkFlowForProcedure = function(workFlow){
                if(PROCEDIMIENTOS_ALTERNOS[workFlow]!=undefined){
                    if(PROCEDIMIENTOS_ALTERNOS[workFlow].indexOf(parseInt($scope.formId))!=-1){
                        return true;
                    }
                    return false;
                }
                return false;
            };


            $this.numberListOfimatica = $scope.range(1,10,1);
            $this.numberListEntrevista= $scope.range(1,10,1);
            $this.numberListOtraTitulacion = $scope.range(10,20,1);
            $this.numberListExpediente = $scope.range(15,30,1);
            $scope.enviarCopiaAutenticaEmail = function($event){
                $event.preventDefault();
                var newStatus = {
                    solicitudeId : $scope.id,
                    statusId : $scope.aux.status.id,
                    statusText : $scope.info,
                    score    :$scope.score,
                    area: $scope.areaCode,
                };

                if($scope.fileCopiaAutentica!==undefined){
                    newStatus['fileCopiaAutenticaName']  = $scope.fileCopiaAutentica.filename;
                    newStatus['fileCopiaAutenticaB64']  = $scope.fileCopiaAutentica.base64;
                    newStatus['onlySendEmail']  = true;
                }
                SolicitudeAdmin.sendCopiaAutentica(newStatus,$this.hideSpinner,$this.hideSpinner);
            };

            $scope.getAggregation =  $this.getAggregation;
            $scope.changeSolicitudeStatus = $this.changeSolicitudeStatus;
			$scope.maxfilesize = FILES_CONSTANTS.MAXSIZE;
			$scope.minfilesize = FILES_CONSTANTS.MINSIZE;
			$scope.fileType = FILES_CONSTANTS.FILE_TYPE;
			$scope.env_id = STATUS_CODES.ENVIADA;
			$scope.sg_id = STATUS_CODES.ENVIADO_SG_CARTERA_BASICA;
			$scope.images = IMAGES_URL;
			$scope.langs = $this.langs;
            $scope.otherLangs  = [];
            $scope.anotherDegreePoints = 0 ;
            $scope.totalPuntosIdiomas  = 0 ;
            $scope.numberListOfimatica = $this.numberListOfimatica;
            $scope.numberListOtraTitulacion = $this.numberListOtraTitulacion;
            $scope.numberListExpediente = $this.numberListExpediente;
            $scope.numberListEntrevista= $this.numberListEntrevista;
            $scope.email_pattern = VALIDATION_PATTERN.EMAIL;
            $scope.beca = {};
            $scope.statusChanged();
            $scope.emailChange = function () {
                console.log($scope.formularioPrincial);
                $scope.formularioPrincial.emailRemision.$validate();
            };

			// $scope.availableLangs = LANGUAGES_LIST.ES;
		});
