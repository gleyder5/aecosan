angular.module('secosanAdmin').controller(
		'AlimentosGruposPuestaMercadoControllerAdmin',
		function($scope, $http, $log, $window, $routeParams, $location, $sce, HeaderTitle, NgTableParams, Spinner,AlimentosGrupos, FormModelService, Instrucciones, IdentificadorPeticion, Messages,
				UtilService, ModalService, ModalParamsService, FileDtoDownload, ValidationUtilsService, CatalogueLocationHandler, AdminSetDataFromCatalogService, SolicitudeAdmin, LocationCombosService,
				RegistryNumberFormatService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getAlimentosGruposPuestaMercadoModel();

			AdminSetDataFromCatalogService.getCountryCatalogue($scope);
			AdminSetDataFromCatalogService.getFormatsCatalogue($scope);
			AdminSetDataFromCatalogService.getProductTypeCatalogue($scope);

			$this.loadFormToEdit = function(data) {
				
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				$scope.viewMode = true;

				$scope.checkboxModel = {};

				$scope.auxSolicitante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxRepresentante = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxContacto = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.aux = {
					paisProcedencia : {},
					paisComercializacionPrevia : {},
					idIsProductMaker : "",
					tipoProducto : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				// Datos identificativos adicionales
				var isProductMaker = $scope.formularioATratar.datosIdentificativosAdicionales.isProductMaker;
				if (isProductMaker) {
					$scope.aux.idIsProductMaker = "true";
				} else if (isProductMaker === "") {
					$scope.aux.idIsProductMaker = "";
				} else {
					$scope.aux.idIsProductMaker = "false";
				}

				RegistryNumberFormatService.putRGSEAAInView($scope, $scope.formularioATratar.datosIdentificativosAdicionales.rgseaa);

				$scope.aux.tipoProducto.catalogId = $scope.formularioATratar.productType.id;
				$scope.formasToShow = $scope.formularioATratar.presentaciones;
				$scope.aux.paisProcedencia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisProcedencia;
				$scope.aux.paisComercializacionPrevia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisComercializacionPrevia;
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				SolicitudeAdmin.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$scope.id_solicitude = idToEdit;
					$scope.$broadcast('sendPrevStatus', data.solicitude.status);
					$log.debug("OK getting solicitude " + idToEdit);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

//			$this.verInstrucciones = function() {
//				Spinner.showSpinner();
//				$log.debug("verInstrucciones");
//				Instrucciones.getInstrucciones(FORM_ID_CODES.GE_PUESTA_MERCADO).success(function(data) {
//					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
//					$log.debug("Download intrucciones success");
//					Spinner.showSpinner();
//				}).error(function(data, status) {
//					$log.error("Download intrucciones error");
//					Messages.setErrorMessageFromResponse(data, status);
//					Spinner.showSpinner();
//				});
//			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxNumRGSEAA = {
				"auxNumRGSEAA1" : "",
				"auxNumRGSEAA2" : "",
				"auxNumRGSEAA3" : ""
			};

			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.ALMIENTOS_GRUPOS_PUESTA_MERCADO);
			$scope.accordionCtrl = {
				accordionUno : true,
				accordionDos : true,
				accordionTres : true,
				accordionCuatro : true,
				accordionTodos : false
			}

//			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;

//			$scope.instruccionesVistas = false;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.ALIM_GRUPOS_PUESTA_MERCADO;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.GE_PUESTA_MERCADO;
		});