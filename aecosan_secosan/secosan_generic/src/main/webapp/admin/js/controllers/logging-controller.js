angular.module('secosanAdmin').controller('LoggingController', function($scope, $log,LoggingService,IMAGES_URL) {

	var $this = this;
	
	$this.getLogsList = function(){
		
		if($scope.logsList.length === 0){
			$scope.disabledDownloadButton = false;
			LoggingService.getLogList().success(function(data) {
				$scope.logsList = data;
			}).error(function(data, status) {
				$scope.dowloadErrorMsg = data;
			});
		}
		
	};
	
	$this.getLogFromService = function(selectedLog){
		$log.debug("$scope.selectedLog--->"+selectedLog);
		if(selectedLog !== undefined && selectedLog !==""){
			$scope.showSpiner=true;
			LoggingService.getLog(selectedLog,function(){
				$scope.showSpiner=false;
			},function(data, status) {
				$scope.dowloadErrorMsg = data;
				$scope.showSpiner=false;
			});
		}	
	};
	
	$scope.logsList=[];
	$scope.showSpiner=false;
	$scope.getLogsList = $this.getLogsList
	$scope.getLogFromService =$this.getLogFromService;
	$scope.images = IMAGES_URL;
	
});
