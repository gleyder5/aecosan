angular.module('secosanAdmin').controller(
		'GestionTipoProcedimientoController',
		function($scope, $log, $location, Messages, AdminFormModelService, UtilService, AdminSetDataFromCatalogService, NgTableParams, ModalService, AdminModalParamsService, TipoProcedimientoManagementService,
				UpdateUserService,AdminCatalogueHandler,Spinner, ERR_MSG, REDIRECT_URL,IMAGES_URL,CATALOGUE_NAMES,VALIDATION_PATTERN) {
			var $this = this;

			$scope.formularioATratar = AdminFormModelService.getTipoProcedimientoModel();
			$scope.tipoProcedimientosList = [];

			$scope.aux = {
				area : "",
				userPermission : ""
			};
			$scope.refresh  =  function () {
                TipoProcedimientoManagementService.getTipoProcedimientoList().success(function(data) {
                	$scope.tipoProcedimientosList = data;
                });

            };

			$scope.statusFilter = [ {
				'id' : 'false',
				'title' : 'Activo'
			}, {
				'id' : 'true',
				'title' : 'Bloqueado'
			} ];

			$scope.configTableParamsTipos = new NgTableParams({
				count : 10,
				sorting : {
					login : "asc"
				},
			}, {
				// page size buttons (right set of buttons in demo)
				counts : [],
				// determines the pager buttons (left set of buttons in demo)
				paginationMaxBlocks : 13,
				paginationMinBlocks : 2,
				getData : function($defer, params) {
					$scope.refresh();
					//TODO añadir params
					TipoProcedimientoManagementService.getTipoProcedimientoList().success(function(data) {
						var total = data.recordsTotal;
						if(total!==data.recordsFiltered){
							total = data.recordsFiltered;
						}
						params.total(total);
						$defer.resolve(data.data);
					}).error(function(data, status, headers, config) {
						$log.debug(data);
						$log.debug(status);
						$log.debug(headers);
						$log.debug(config);
						Messages.setErrorMessageFromResponse(data, status);
					});
				}
			});

			$this.showHideUserFilter = function() {
				if ($scope.showTableUserFilters) {
					$scope.showTableUserFilters = false;
					$scope.configTableParamsTipos.filter({});
				} else {
					$scope.showTableUserFilters = true;
				}
			};
			
			$this.loadAreaTableFilterSelect = function($column){
				$log.info("loadAreaTableFilterSelect");
				AdminCatalogueHandler.getCatalogue(CATALOGUE_NAMES.USER_AREA).success(function(data) {
					$column.data = AdminCatalogueHandler.generateCatalogueDataForShow(data);
					$log.info("data:"+data);
				}).error(function() {
					AdminCatalogueHandler.generateDefaultErrorMessage();
				});
			}

			$this.deleteTipoProcedimiento  = function(index, idToDelete) {
				ModalService.showModalWindow(AdminModalParamsService.deleteTipoProcedimiento(index,idToDelete,$scope.configTableParamsTipos));
			};

			$this.updateUser = function(userData) {
				UpdateUserService.showUpdateUserWindow({
					'userData' : userData
				},$scope.configTableParamsTipos);
			};

			var errorMessage = "";

			$this.checkValidForm = function() {
				var valid = true;

				return valid;
			};

			$this.resetFields = function() {
				$scope.formularioATratar = AdminFormModelService.getTipoProcedimientoModel();
				$scope.aux.area = "";
				$scope.aux.userPermission = "";
				$scope.aux.status = undefined;
				$scope.aux.retypePassword = "";
			}

			$scope.sendForm = function() {
				Spinner.showSpinner();
				
				if ($this.checkValidForm()) {
					TipoProcedimientoManagementService.addTipoProcedimiento($scope.formularioATratar).success(function() {
						$this.resetFields();
						$scope.configTableParamsTipos.reload();
						Spinner.hideSpinner();
						$scope.addTipoProcMessages = {
								success:true,
								msg:"TIPO_PROC_ADD_SUCCESS"
						};
						$scope.refresh();
					}).error(function(data, status, headers, config) {
						$scope.hasdata = false;
						Spinner.hideSpinner();
						$scope.addTipoProcMessages = {
								success:false,
								msg:"TIPO_PROC_ADD_ERROR"
						};
					});
				} else {
					Messages.setMessageAndType(errorMessage, Messages.error);
					Spinner.hideSpinner();
				}

			};
			$scope.isSameUserLogged = function(id){
				return TipoProcedimientoManagementService.isSameUser(id);
		
			}

			$scope.showTableUserFilters = false;
			$scope.showHideUserFilter = $this.showHideUserFilter;
			$scope.deleteTipoProcedimiento = $this.deleteTipoProcedimiento;
			$scope.updateUser = $this.updateUser;
			$scope.images = IMAGES_URL;
			$scope.loadAreaTableFilterSelect = $this.loadAreaTableFilterSelect;
			$scope.form ={};
			$scope.addTipoProcMessages={};
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.refresh();
		});