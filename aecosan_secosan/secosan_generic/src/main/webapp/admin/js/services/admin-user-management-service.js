angular.module("secosanAdmin").factory("UserManagementService", function($http, $log, AuthenticationUser, PaginationFilter,base64, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	};

	this.isSameUserLogged = function(id) {
		var userId = AuthenticationUser.getUserPayloadData('id');
		var isSame = false;

		if (userId === id) {
			isSame = true;
		}
		return isSame;
	}
	
	this.encriptPassword=function(form){
		var passClear= form.password;
		if(passClear !== undefined && passClear !==""){
			form.password =  base64.encode(passClear);
		}
		return form;
	}

	return {
		addInternalUser : function(form) {
			var url = REST_URL.USER_MANAGEMENT_ADD_INTERNAL;
			form = $this.encriptPassword(form);
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateInternalUser : function(form) {
			var url = REST_URL.USER_MANAGEMENT_UPDATE_INTERNAL + $this.getUserLoggedId();
			form = $this.encriptPassword(form);
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		getInternalUserList : function(filterParams) {
			var url = REST_URL.USER_MANAGEMENT_LIST;
			url = PaginationFilter.getPaginationUrl(url, filterParams);
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		deleteInternalUser : function(idToDelete) {
			var url = REST_URL.USER_MANAGEMENT_DELETE_INTERNAL + $this.getUserLoggedId() + "/" + idToDelete;
			return $http({
				method : 'DELETE',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		isSameUser : function(id) {
			return $this.isSameUserLogged(id);
		},
		changePasswordAdmin : function(form) {
			var url = REST_URL.CHANGE_PASSWORD_ADMIN;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		checkIfInternalUserExists: function(dni,validationFuction,errorFunction){
			var url = REST_URL.CHECK_USER_EXISTS+dni;
			$http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			}).success(function(data){
				validationFuction(data);
			}).error(function(data,status){
				if(errorFunction !==undefined){
					errorFunction(data,status);
				}
			});
		}
	}
});