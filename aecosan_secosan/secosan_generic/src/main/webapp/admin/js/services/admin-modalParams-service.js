angular.module("secosanAdmin").factory(
		"AdminModalParamsService",
		function($log,$location,$rootScope,UserManagementService, Messages,TipoProcedimientoManagementService) {

			return {
				deleteTipoProcedimiento: function (indx,idToDelete,tableConfig) {
                    $log.debug("delete tipo procedure modal params");
                    return {
                        title : "Borrado de tipo de procedimiento",
                        okButtonStyle : "btn-danger",
                        description : "\u00BFEst\u00E1 seguro de que desea eliminar permanentemente este registro? Esta acci\u00F3n no se puede deshacer.",
                        type : "question",
                        okButton : "Eliminar",
                        okFunction : function() {
                            TipoProcedimientoManagementService.deleteTipoProcedimiento(idToDelete).success(function () {
                                $log.debug("deleteTipoProc OK");
                                tableConfig.reload();
                            }).error(function(data, status) {
                                $log.debug("deleteTipoProc KO");
                                Messages.setErrorMessageFromResponse(data, status);
                            });

                        }
                    }
                },
				deleteInternalUser : function(id, idToDelete, tableConfig) {
					$log.debug("delete internal user modal params");
					return {
						title : "Borrado de usuarios",
						okButtonStyle : "btn-danger",
						description : "\u00BFEst\u00E1 seguro de que desea eliminar permanentemente este usuario? Esta acci\u00F3n no se puede deshacer.",
						type : "question",
						okButton : "Eliminar",
						okFunction : function() {
							UserManagementService.deleteInternalUser(idToDelete).success(function(data) {
								$log.debug("deleteInternalUser OK");
								tableConfig.reload();
							}).error(function(data, status) {
								$log.debug("deleteInternalUser KO");
								Messages.setErrorMessageFromResponse(data, status);
							});
						}
					}
				},
				notifyNewStatusSuccess:function(redirect){
					return {
						title : "Notificaci\u00F3n de cambio de Estado",
						description : "Se ha notificado con exito el cambio de estado de la solicitud",
						type : "ok",
						okFunction : function() {
							$location.path(redirect);
							$rootScope.$apply();
						}
					}
				},
                emailSentSuccess:function(){
                    return {
                        title : "Correo electrónico enviado",
                        description : "Se ha enviado con exito el fichero",
                        type : "ok",
                        okFunction : function() {
                            $rootScope.$apply();
                        }
                    }
                },
				notifyNewStatusError:function(solicitudeId,statusText){
					return {
						title : "Notificaci\u00F3n de cambio de Estado",
						description : "No ha sido posible realizar la notificaci\u00F3n de cambio de estado  de la solicitud",
						type : "error"
					}
				}
			}
		});