angular.module("secosanAdmin").factory("AdminCatalogueHandler", function($log,$http,CatalogueHandler,REST_URL) {
	var $this = this;
		this.setAdminDefaultUrl=function(){
			var adminDefaultUrl = REST_URL.ADMIN_CATALOGUE;
			CatalogueHandler.setRemoteBaseUrl(adminDefaultUrl);
		}
	
	return {
		getCatalogue : function(catalogueName) {
			$this.setAdminDefaultUrl()
			return CatalogueHandler.getCatalogue(catalogueName);
		},

		getRelatedCatalogue : function(catalogueName, parentCatalogue, parentId) {
			$this.setAdminDefaultUrl()
			return CatalogueHandler.getRelatedCatalogue(catalogueName, parentCatalogue, parentId);
		},
		getTaggedCatalogue : function(catalogueName, tag) {
			$this.setAdminDefaultUrl()
			return CatalogueHandler.getTaggedCatalogue(catalogueName, tag);
		},

		generateCatalogueDataForShow : function(rawData) {
			$this.setAdminDefaultUrl()
			return CatalogueHandler.generateCatalogueDataForShow(rawData);
		},
		generateDefaultErrorMessage : function(rawData) {
			$this.setAdminDefaultUrl()
			return CatalogueHandler.generateDefaultErrorMessage(rawData);
		},
		generateCatalogueDataSolicitudStatusByArea:function(areaId){
			var url =REST_URL.ADMIN_CATALOGUE_STATUS_BY_AREA+areaId;
			return $http({
				method : 'GET',
				url : url
			});
		}

	}

});