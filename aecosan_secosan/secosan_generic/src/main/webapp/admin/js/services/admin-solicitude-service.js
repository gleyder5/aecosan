angular.module("secosanAdmin").factory("SolicitudeAdmin", function($http, REST_URL, HEADERS, PaginationFilter, Solicitude,AuthenticationUser,ModalService,AdminModalParamsService) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}
	
	this.getUserLoggedAreaId = function() {
		return AuthenticationUser.getUserPayloadData("areaId");
	}


	return {
		getSolicitudeListTramitador : function(filterParams) {
			var url = REST_URL.SOLICITUDE_LIST_TRAMITADOR + $this.getUserLoggedAreaId();
			url = PaginationFilter.getPaginationUrl(url, filterParams);
			return $http({
				method : 'GET',
				url : url
			})
		},
		getSolicitudeById : function(id) {
			Solicitude.setSolicitudeRestUrl(REST_URL.SOLICITUDE);
			return Solicitude.getSolicitudeById(id);
		},
		updateSolicitudeStatus : function(datos,redirectOk,callbackOk,callbackError) {
			var url = REST_URL.SOLICITUDE + $this.getUserLoggedId();
			 $http({
				method : 'POST',
				url : url,
				data : datos,
				headers : HEADERS.JSON
			}).success(function(data) {
				if(redirectOk) {
                    ModalService.showModalWindow(AdminModalParamsService.notifyNewStatusSuccess(redirectOk));
                }
                if (callbackOk && typeof callbackOk==='function') {
					callbackOk(data);
				}

			}).error(function(data, status, headers, config){
				ModalService.showModalWindow(AdminModalParamsService.notifyNewStatusError());
				if(callbackError){
					callbackError(data, status, headers, config);
				}
			 });
		},getUserLoggedAreaId:function(){
			return $this.getUserLoggedAreaId();
		},
        sendCopiaAutentica : function(datos,callbackOk,callbackError) {
            var url = REST_URL.SOLICITUDE + $this.getUserLoggedId();
            $http({
                method : 'POST',
                url : url,
                data : datos,
                headers : HEADERS.JSON
            }).success(function(data) {

				ModalService.showModalWindow(AdminModalParamsService.emailSentSuccess());

                if (callbackOk && typeof callbackOk==='function') {
                    callbackOk(data);
                }

            }).error(function(data, status, headers, config){
                ModalService.showModalWindow(AdminModalParamsService.notifyNewStatusError());
                if(callbackError){
                    callbackError(data, status, headers, config);
                }
            });
        },getUserLoggedAreaId:function(){
            return $this.getUserLoggedAreaId();
        }
	}
});