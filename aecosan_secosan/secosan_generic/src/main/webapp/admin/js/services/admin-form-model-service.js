angular.module("secosanGeneric").factory("AdminFormModelService", function() {
	return {
		
		getInternalUserRegistryModel : function() {
			return {
				"id":null,
				"login" : "",
				"password" : "",
				"nombre" : "",
				"apellido" : "",
				"area" : "",
				"userBlocked" : "false",
				"email":"",
				"identificationNumber":""
			}
		},
        getTipoProcedimientoModel : function() {
            return {
                // "id": '',
				"descripcion":"",
            }
        },
	}
});