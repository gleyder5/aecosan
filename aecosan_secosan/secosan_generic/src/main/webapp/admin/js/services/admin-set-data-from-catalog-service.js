angular.module("secosanAdmin").factory("AdminSetDataFromCatalogService",
		function($log, $filter, AdminCatalogueHandler, Messages, CatalogueLocationHandler, AdminDocTypeService, CATALOGUE_NAMES, STATUS_SUBSANADA,STATUS_ENVIADA_REPLACE,AREA_CODES,STATUS_CODES) {
			var $this = this;

			$this.catalogToFilter = function(catalog) {
				var filter = [];
				filter.push({
					'id' : "",
					'title' : ""
				});
				angular.forEach(catalog, function(item) {
					filter.push({
						'id' : item.catalogValue,
						'title' : item.catalogValue
					});
				});
				return filter;
			};
			$this.changeSendStatusValueToShow = function(catalog){
				angular.forEach(catalog, function(value,key) {
					if(STATUS_ENVIADA_REPLACE.id== value.id){
						catalog[key] = STATUS_ENVIADA_REPLACE;
					}
				});
			};
			

			return {
				getCountryCatalogue : function($scope) {
					AdminCatalogueHandler.getCatalogue(CATALOGUE_NAMES.COUNTRIES).success(function(data) {
						$log.info("OK getting catalog " + CATALOGUE_NAMES.COUNTRIES);
						CatalogueLocationHandler.countryCatalogSuccessHandler($scope, data);
					}).error(function(data, status) {
						CatalogueLocationHandler.countryCatalogErrorHandler(data, status);
					});
				},
				getFormatsCatalogue : function($scope) {
					var catalogFormas = CATALOGUE_NAMES.FORMATS;
					AdminCatalogueHandler.getCatalogue(catalogFormas).success(function(data) {
						$log.info("OK getting catalog " + catalogFormas);
						$scope.catalogoFormas = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogFormas + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getEUCountriesCatalogue : function($scope) {
					var catalogPaisUE = CATALOGUE_NAMES.COUNTRIES;
					AdminCatalogueHandler.getTaggedCatalogue(catalogPaisUE, "paisesEU").success(function(data) {
						$log.info("OK getting catalog " + catalogPaisUE);
						$scope.catalogoPaisesUE = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogPaisUE + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getNoEUCountriesCatalogue : function($scope) {
					var catalogPaisNoUE = CATALOGUE_NAMES.COUNTRIES;
					AdminCatalogueHandler.getTaggedCatalogue(catalogPaisNoUE, "paisesNoEU").success(function(data) {
						$log.info("OK getting catalog " + catalogPaisNoUE);
						$scope.catalogoPaisesNoUE = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogPaisNoUE + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getProductTypeCatalogue : function($scope) {
					var catalogProductTypeName = CATALOGUE_NAMES.PRODUCT_TYPE;
					AdminCatalogueHandler.getCatalogue(catalogProductTypeName).success(function(data) {
						$log.info("OK getting catalog " + catalogProductTypeName);
						$scope.catalogProductTypeAll = data.catalog;
						$scope.catalogProductTypeAlimEsp = [];
						for (var i = 0, len = data.catalog.length; i < len; i++) {
							if (data.catalog[i].catalogId === "1") {
								$scope.catalogProductTypeAlimEsp.push(data.catalog[i]);
							}
						}
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogProductTypeName + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getAdminModeCatalogue : function($scope) {
					var catalogAdminModeName = CATALOGUE_NAMES.ADMINISTRATION_MODE;
					AdminCatalogueHandler.getCatalogue(catalogAdminModeName).success(function(data) {
						$log.info("OK getting catalog " + catalogAdminModeName);
						$scope.catalogAdminMode = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogAdminModeName + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getSuspensionTypeCatalogue : function($scope) {
					var catalogTipos = CATALOGUE_NAMES.SUSPENSION_TYPES;
					AdminCatalogueHandler.getCatalogue(catalogTipos).success(function(data) {
						$log.info("OK getting catalog " + catalogTipos);
						$scope.catalogTiposSuspension = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogTiposSuspension + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getPaymentTypeCatalogue : function($scope) {
					var catalogTipoPagoName = CATALOGUE_NAMES.PAYMENT_TYPE;
					AdminCatalogueHandler.getCatalogue(catalogTipoPagoName).success(function(data) {
						$log.info("OK getting catalog " + catalogTipoPagoName);
						$scope.catalogTipoPago = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogTipoPagoName + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getPaymentCatalogue : function($scope) {
					var catalogTipoPago = CATALOGUE_NAMES.PAYMENT;
					AdminCatalogueHandler.getCatalogue(catalogTipoPago).success(function(data) {
						$log.info("OK getting catalog " + catalogTipoPago);
						$scope.catalogFormaPago = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogTipoPago + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getGruposUsuarioCatalogue : function($scope) {
					var catalogGruposUsuario = CATALOGUE_NAMES.USER_AREA;
					AdminCatalogueHandler.getCatalogue(catalogGruposUsuario).success(function(data) {
						$log.info("OK getting catalog " + catalogGruposUsuario);
						$scope.catalogoGrupos = data.catalog;
						$scope.areaFilter = $this.catalogToFilter($scope.catalogoGrupos);
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogGruposUsuario + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getRolesUsuarioCatalogue : function($scope) {
					var catalogRolesUsuario = CATALOGUE_NAMES.USER_ROLE;
					AdminCatalogueHandler.getTaggedCatalogue(catalogRolesUsuario, "internalRoles").success(function(data) {
						$log.info("OK getting catalog " + catalogRolesUsuario);
						$scope.catalogoRoles = data.catalog;
					}).error(function(data, status) {
						$log.error("Error getting catalog " + catalogRolesUsuario + " " + status);
						Messages.setErrorMessageFromResponse(data, status);
					});
				},
				getDocumentTypeCatalog : function($scope, formID) {
					if(formID!=undefined) {
                        AdminDocTypeService.getDocTypesByFormID(formID).success(function (data) {
                            $log.info("OK getting catalog DocumentTypeCatalog");
                            $scope.dataOption.availableOptions = $scope.dataOption.availableOptions.concat(data);
                        }).error(function (data, status) {
                            Messages.setErrorMessageFromResponse(data, status);
                            $log.error("Error getting catalog DocumentTypeCatalog " + status);
                        });
                    }
				},
				getSolicitudeStatusCatalog : function($scope) {

					AdminDocTypeService.getSolicitudeStatusByAreaID($scope.areaCode).success(function(data) {
						$log.info("OK getting catalog SolicitudeStatusCatalog");
						$this.changeSendStatusValueToShow(data);
						if($scope.areaCode==AREA_CODES.BECAS){
							data.forEach(function (itm) {
                                if (itm.id == STATUS_CODES.REQ_SUBSANAR_10_DIAS) {
                                    data.splice(data.indexOf(itm), 1);
                                }
                                if (itm.id == STATUS_CODES.PRESENTAR_EN_SEDE) {
                                    data.splice(data.indexOf(itm), 1);
                                }
                            })

						}
						$scope.catalogSolicitudeStatus = data;

					}).error(function(data, status) {
						Messages.setErrorMessageFromResponse(data, status);
						$log.error("Error getting catalog SolicitudeStatusCatalog " + status);
					});
				},
				getSolicitudeStatusCatalogByArea : function(areaCode, success, error) {
					AdminDocTypeService.getSolicitudeStatusByAreaID(areaCode).success(function(data) {
						data.push(STATUS_SUBSANADA);
						$this.changeSendStatusValueToShow(data);
						data = $filter('orderBy')(data, "catalogValue");
						if (!angular.equals(success, undefined)) {
							success(data);
						}
					}).error(function(data, status) {
						if (!angular.equals(error, undefined)) {
							error(data, status);
						}
					});
				}
			}
		});