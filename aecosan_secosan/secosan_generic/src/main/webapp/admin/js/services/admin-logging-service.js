angular.module("secosanAdmin").factory("LoggingService", function($http, $log, REST_URL, HEADERS,FileDtoDownload) {
	$this = this;

	return {
		getLogList : function() {
			var url = REST_URL.LOGGING_LIST;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		getLog : function(logName,successFunction,errorFunction) {
			var url = REST_URL.LOGGING_GET_LOG+logName;
			$http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			}).success(function(data) {
				FileDtoDownload.decryptAndDownloadFile(data.logBase64, data.name);
				if(successFunction!==undefined){
					successFunction(data);
				}
			}).error(function(data, status) {
				if(errorFunction!==undefined){
					errorFunction(data,status);
				}
			});
		}
	}
});