angular.module('secosanAdmin').factory("AdminDocTypeService", function($http, $log,DocTypeService, REST_URL, HEADERS) {
	return {
	
		getSolicitudeStatusByAreaID : function(areadID) {
			var url = REST_URL.SOLICITUDE_STATUS + areadID;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		getDocTypesByFormID : function(formID) {
			DocTypeService.setBaseUrl(REST_URL.SOLICITUDE_STATUS);
			return DocTypeService.getDocTypesByFormID(formID);
		},
	}
});