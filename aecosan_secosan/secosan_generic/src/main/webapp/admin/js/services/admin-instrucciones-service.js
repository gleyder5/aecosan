angular.module("secosanAdmin").factory("AdminInstrucciones", function($http, $log, AuthenticationUser,Instrucciones,InstruccionesPago, Spinner,REST_URL) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		getInstruccionesPagoList : function() {
			var url = REST_URL.INSTRUCCIONES_PAGO_LIST + $this.getUserLoggedId();
			return $http({
				method : 'GET',
				url : url
			})
		},
		uploadInstructionsToPay : function(ficheroInstrucciones, callbackSuccess, callbackError) {
			Spinner.showSpinner();
			var url = REST_URL.INSTRUCCIONES_PAGO_UPLOAD;
			$http({
				method : 'POST',
				data : ficheroInstrucciones,
				url : url
			}).success(function(data) {
				if(!angular.equals(callbackSuccess,undefined)){
					callbackSuccess(data);
				}
				Spinner.hideSpinner();
			}).error(function(data, status) {
				if(!angular.equals(callbackError,undefined)){
					callbackError(data, status);
				}
				Spinner.hideSpinner()
			});
		},
		uploadInstructions :  function(ficheroInstrucciones, callbackSuccess, callbackError) {
			Spinner.showSpinner();
			var url = REST_URL.INSTRUCCIONES;
			 $http({
				method : 'POST',
				data : ficheroInstrucciones,
				url : url
			}).success(function(data) {
				if(!angular.equals(callbackSuccess,undefined)){
					callbackSuccess(data);
				}
				Spinner.hideSpinner();
			}).error(function(data, status) {
				if(!angular.equals(callbackError,undefined)){
					callbackError(data, status);
				}
				Spinner.hideSpinner()
			});
		},
		getInstruccionesPago : function(idFormulario) {
			InstruccionesPago.setBaseUrl(REST_URL.INSTRUCCIONES_PAGO_DOWNLOAD);
			return InstruccionesPago.getInstruccionesPago(idFormulario);
		},
		getInstrucciones : function(idFormulario) {
			var form ={
				formularioEspecifico:{
					id:idFormulario
				}
			};
			Instrucciones.setBaseUrl(REST_URL.INSTRUCCIONES);
			$log.debug("form");
			$log.debug(form);
			return Instrucciones.getInstrucciones(form);
		}

	}
});