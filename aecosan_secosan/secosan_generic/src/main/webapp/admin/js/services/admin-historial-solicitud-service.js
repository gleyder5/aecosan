angular.module("secosanAdmin").factory("HistorialSolicitudesService", function(AuthenticationUser,$log, $http, $translate,HEADERS, Messages, REST_URL,PaginationFilter) {
    var $this = this;
    $this.getUserLoggedId = function() {
        return AuthenticationUser.getUserPayloadData("id");
    }
    this.baseUrl = REST_URL.ADMIN_HISTORIAL_SOLICITUDES;
    return {
        getHistorialBySolicitudAndStatus :function(solicitudId,statusId){
            let url = $this.baseUrl +solicitudId+"/"+statusId;
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        },
        getHistorialListBySolicitud : function(solicitudId,params){
            let url = $this.baseUrl +solicitudId;
            url = PaginationFilter.getPaginationUrl(url, params);
            return $http({
                method : 'GET',
                url : url
            })
        }
    }

});