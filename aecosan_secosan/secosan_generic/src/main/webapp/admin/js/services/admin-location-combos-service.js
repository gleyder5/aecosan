angular.module("secosanAdmin").factory("AdminLocationCombosService", function($log, AdminCatalogueHandler, AdminCatalogueLocationHandler,LocationCombosService) {
	var $this = this;
	 this.setHandlers=function(){
		 LocationCombosService.setCatalogueHandler(AdminCatalogueHandler);
		 LocationCombosService.setCatalogueLocationHandler(AdminCatalogueLocationHandler);
	 };
	return {
		loadCombo : function($scope, data, combo) {
			$this.setHandlers();
			 LocationCombosService.loadCombo($scope, data, combo);
		},
		setLocationDataToScope : function setLocationDataToScope($scope) {
			$this.setHandlers();
			 LocationCombosService.setLocationDataToScope($scope);
		},
		setPaymentLocationToScope : function($scope) {
			$this.setHandlers();
			 LocationCombosService.setPaymentLocationToScope($scope);
		},
		loadPaymentCombo : function($scope, data) {
			$this.setHandlers();
			 LocationCombosService.loadPaymentCombo($scope,data);
		}
	}
});
