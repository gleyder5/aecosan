angular.module("secosanAdmin").factory("AdminCatalogueLocationHandler", function($log, AdminCatalogueHandler, CatalogueLocationHandler) {
	var $this = this;
	

	return {

		loadMunicipio : function($scope, idProvincia, combo, idMunicipio) {
			CatalogueLocationHandler.setcatalogueHandler(AdminCatalogueHandler);
			return CatalogueLocationHandler.loadMunicipio($scope, idProvincia, combo, idMunicipio);
		},

		loadProvince : function($scope, idPais, combo, idProvincia, callback, callbackArguments) {
			CatalogueLocationHandler.setcatalogueHandler(AdminCatalogueHandler);
			return CatalogueLocationHandler.loadProvince($scope, idPais, combo, idProvincia, callback, callbackArguments);
		},

		loadProvinciaOnChange : function($scope, comboPais) {
			CatalogueLocationHandler.setcatalogueHandler(AdminCatalogueHandler);
			return CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
		},

		loadMunicipioOnChange : function($scope, comboProvincia) {
			CatalogueLocationHandler.setcatalogueHandler(AdminCatalogueHandler);
			return CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
		},
		countryCatalogSuccessHandler : function($scope, data, callback, callbackArguments) {
			CatalogueLocationHandler.setcatalogueHandler(AdminCatalogueHandler);
			return CatalogueLocationHandler.countryCatalogSuccessHandler($scope, data, callback, callbackArguments);
			
		},
		domObjectName : CatalogueLocationHandler.domObjectName,

	}

});