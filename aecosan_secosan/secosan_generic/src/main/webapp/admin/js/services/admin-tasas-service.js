angular.module("secosanAdmin").factory("AdminTasas", function($http, $log, PaginationFilter,REST_URL, HEADERS) {
	var $this = this;
	
	return {
		
		getTasasList : function(filterParams) {
			var url = REST_URL.TASA_LIST;
			url = PaginationFilter.getPaginationUrl(url, filterParams);
			$log.debug("tasa url:"+url);
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		updateTasa : function(tasa) {
			var url = REST_URL.TASA;
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : tasa
			})
		},
		
	}
});