angular.module("secosanAdmin").factory("MonitoringService", function($http, $log, AuthenticationUser, PaginationFilter, REST_URL, HEADERS) {
	$this = this;

	return {
		getMonitoringList : function(filterParams) {
			var url = REST_URL.MONITORING_LIST;
			url = PaginationFilter.getPaginationUrl(url, filterParams);
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		getStatistics : function() {
			var url = REST_URL.MONITORING_STATISTICS;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		}
	}
});