angular.module("secosanAdmin").factory("UpdateUserService", function($log,UserManagementService) {
	var $this = this;
	var userArgs = {};
	this.tableParams = undefined;
	return {
		// setters
		setUserData : function(userData) {
			userArgs['userData'] = userData;
		},

		setVisible : function(visible) {
			userArgs['visible'] = visible;
		},

		// getters
		getUserArgs : function() {
			return userArgs;
		},

		// methods
		clearUserArgs : function() {
			userArgs = {};
		},
		reloadTableAfterUpload:function(){
			if(!angular.equals($this.tableParams,undefined)){
				tableParams.reload();
			}
		},

		showUpdateUserWindow : function(userParams,tableConfig) {
			this.setUserData(userParams.userData);
			userArgs['sameUser'] = UserManagementService.isSameUser(userParams.userData.id);
			userArgs['update'] = true;
			
			$log.debug("isSameUsuer"+$this.sameUser);
			this.setVisible(true);
		}
	}
});