<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../commonHeader.jsp" />
</head>
<body>
<div class="container headerForwardAlert">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 alert alert-danger text-center">
			<img src="/secosan/images/info.png"><span>Ha ocurrido un error inesperado en el servidor&nbsp;<button class="btn btn-danger" id="home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> ir al inicio</button></span>
		</div>
	</div>
</div>
<script src="/secosan/js/vendor/jquery/jquery-1.12.0.js"></script>
<script>
$( document ).ready(function() {
	$("#home").click(function() {
		$(location).attr('href','${originalRequest}');
	});
});
</script>
</body>
</html>