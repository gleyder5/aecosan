<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="commonHeader.jsp" />
</head>
<body>
<div class="container headerForwardAlert">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 alert alert-warning ">
			<img src="images/warning.png"><span>Se han detectado valores incorrectos en la petici&oacute;n de la p&aacute;gina de inicio. Verifique que invoc&oacute; esta p&aacute;gina desde una direcci&oacute;n confiable o que se encuentra detr&aacute;s de un proxy</span>
		</div>
		<div class="col-md-6 col-md-offset-3 alert alert-info">
			<img src="/secosan/images/info.png"><span>Para ir a la zona de acceso presione el siguiente bot&oacute;n: <button class="btn btn-primary" id="home"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> ir al inicio</button> o copie la siguiente direccion en la barra del navegador: ${originalRequest}</span>
		</div>
	</div>
</div>
<script src="/secosan/js/vendor/jquery/jquery-1.12.0.js"></script>
<script>
$( document ).ready(function() {
	$("#home").click(function() {
		$(location).attr('href','${originalRequest}');
	});
});

</script>
</body>
</html>