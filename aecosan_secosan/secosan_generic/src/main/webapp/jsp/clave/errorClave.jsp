<%@page import="es.msssi.claveclient.Respuesta"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="eu.stork.peps.auth.commons.PersonalAttribute"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../commonHeader.jsp" />
</head>
<body>
<div class="container">
	<div class="row alert alert-danger">
		<div class="col-md-6 ">
			<h2>Ha ocurrido un error durante la comunicaci&oacute;n con el sistema </h2>
		</div>
		<div class="col-md-6">
				<img src="/secosan/images/logoCLAVE.png">
		</div>
	</div>
</div>
	<script src="/secosan//js/vendor/jquery/jquery-1.12.0.js"></script>
</body>
</html>