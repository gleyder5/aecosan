<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="../commonHeader.jsp" />
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h2>Finalizado proceso de Autenticaci&oacute;n </h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<h4>Esta p&aacute;gina se cerrar&aacute; autom&aacute;ticamente </h4>
		</div>
	</div>
</div>

	<script src="/secosan/js/vendor/jquery/jquery-1.12.0.js"></script>
	<script>
		$(document).ready(function() {
			 window.close();
		});
	</script>
</body>
</html>