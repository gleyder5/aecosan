angular.module("secosanGeneric").factory("AuthenticationUser", function($http, $log, $cookieStore,$location, $rootScope, $document,base64, jwtHelper, Idle,REST_URL, HEADERS, CREDENTIALS_COOKIES,CREDENTIALS_COOKIES_ERROR, REDIRECT_COOKIES,IDLE_TIME_SECONDS) {

	var $this = this;

	this.getCookies = function(cookieName){
		return $cookieStore.get(cookieName);
	}
	
	this.getCookie = function() {
		return $this.getCookies(CREDENTIALS_COOKIES);
	};
	
	this.getCookieErrors = function() {
		return $this.getCookies(CREDENTIALS_COOKIES_ERROR);
	};

	this.clientCertCookiePresent = function() {
		var isPresent = false;
		var cookie = this.getCookie();
		if (cookie !== undefined) {
			isPresent = true;
		}
		return isPresent;
	};
	
	this.clientCertErrorCookiePresent = function() {
		var isPresent = false;
		var cookie = this.getCookieErrors();
		if (cookie !== undefined) {
			isPresent = true;
		}
		return isPresent;
	};
	
	this.getclientCertErrorCookieErrorCode = function (){
		var cookie = this.getCookieErrors();
		$log.debug(cookie);
		var cookieTranslatedToJson = angular.fromJson(cookie)
		var code = cookieTranslatedToJson.code;
		$cookieStore.remove(CREDENTIALS_COOKIES_ERROR);
		return code;
	};
	
	

	this.clientCredentialsPresentInRootScope = function() {
		var clientCredentialsPresents = false;
		if (angular.equals($rootScope.userCredentials, undefined)) {
			clientCredentialsPresents = true;
		}
		return clientCredentialsPresents;
	}

	this.setCookieInRootScope = function() {
		var cookie = this.getCookie();
		$this.setUserCredentialsInRootScope(cookie);

	};

	this.setUserCredentialsInRootScope = function(cookie) {
		var cookieValueJson = angular.fromJson(cookie)
		var userPayload = $this.setPayloadInRootScope(cookieValueJson.token);
		$log.debug(userPayload);
		$rootScope.userCredentials = {
			token : cookieValueJson.token,
			userPayload : userPayload
		}
	};

	this.setPayloadInRootScope = function(token, userCredentials) {
		return jwtHelper.decodeToken(token);
	};

	this.createCookieFromRestData = function(data) {
		$cookieStore.put(CREDENTIALS_COOKIES, data);
	};

	this.checkLoggedIn = function(loggedInAction, notLoggedInAction) {
		var userCredentials = $rootScope.userCredentials;
		if (!angular.equals(userCredentials, {}) && (!angular.equals(userCredentials, undefined))) {
			loggedInAction();
		} else {
			notLoggedInAction();
		}
	};

	this.clearLoginData = function() {
		$cookieStore.remove(CREDENTIALS_COOKIES);
		$cookieStore.remove(REDIRECT_COOKIES);
		$cookieStore.remove(CREDENTIALS_COOKIES_ERROR);
		$rootScope.userCredentials = undefined;
		Idle.unwatch();
	};
	
	this.forceExpireCookie = function(name){
		$log.debug(name);
		$document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}

	this.handleLogin = function(data, callback) {
		$this.createCookieFromRestData(data);
		$this.setCookieInRootScope();
		$this.setupIdle();
		if (!angular.equals(undefined, callback)) {
			callback(data);
		}
		
	};
	
	this.setupIdle = function(){
		Idle.watch();
		Idle.setIdle(IDLE_TIME_SECONDS);
	};
	
	this.getRemindingTokenTimeInSeconds= function(){
		var token = $rootScope.userCredentials.token;
		var dateExpiration = new Date(jwtHelper.decodeToken(token).expiration);
		$log.debug("expiration token"+dateExpiration);
		var dateNow = new Date();
		var timeDiff = Math.abs(dateNow.getTime() - dateExpiration.getTime());
		return Math.floor((timeDiff/1000));
	}
	
	this.encriptLoginPassword=function(form){
		var passClear= form.password;
		form.password =  base64.encode(passClear);
		return form;
	}
	

	return {
		loginUser : function(form) {
			var url = REST_URL.AUTHENTICATION;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : $this.encriptLoginPassword(form)
			})
		},
		checkToken : function(handleValidToken, handleInvalidToken, urlLogin) {
			var url = REST_URL.AUTHENTICATION_CHECK;
			if (!angular.equals(urlLogin, undefined)) {
				url = urlLogin;
			}
			var tokenData = $this.getCookie();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : tokenData
			}).success(function(data, status, headers, config) {
				$this.setCookieInRootScope();
				$this.setupIdle();
				handleValidToken(data, status, headers, config);
			}).error(function(data, status, headers, config) {
				if (angular.equals(handleInvalidToken, undefined)) {
					handleInvalidToken(data, status, headers, config);
				}
			});
		},
		isCookiePresent : function() {
			return $this.clientCertCookiePresent();
		},
		isCredentialsInRootScope : function() {
			return $this.clientCredentialsPresentInRootScope();
		},

		handleClaveLogin : function() {
			var isCookieSet = $this.clientCertCookiePresent();
			if (isCookieSet) {
				$this.setCookieInRootScope();
			}
			return isCookieSet;
		},
		handleNotEULogin : function(data, callback) {
			$this.handleLogin(data, callback);
		},
		handleAdminLogin : function(data, callback) {
			$this.handleLogin(data, callback);
		},
		getUserPayload : function() {
			var payloadValue = undefined;
			if(!angular.equals($rootScope.userCredentials,undefined)){
				payloadValue =$rootScope.userCredentials.userPayload;
			}
			return  payloadValue;
		},
		getUserPayloadData : function(dataKey) {
			var payload = this.getUserPayload();
			if(!angular.equals(payload,undefined)){
				return payload[dataKey];
			}
		},
		getUserToken : function() {
			return $rootScope.userCredentials.token;
		},
		cleanUserData : function() {
			$this.clearLoginData();
		},
		checkLoggedIn : function(loggedInAction, notLoggedInAction) {
			$this.checkLoggedIn(loggedInAction, notLoggedInAction);
		},
		getPrevUrl : function() {
			var redirect = $cookieStore.get(REDIRECT_COOKIES);
			if (!angular.equals(redirect, undefined)) {
				$cookieStore.remove(REDIRECT_COOKIES);
			}
			return redirect;
		},
		loginAdminUser : function(form) {
			var url = REST_URL.ADMIN_AUTHENTICATION;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : $this.encriptLoginPassword(form)
			})
		},
		getRemindingTokenTimeInSeconds:function(){
			return $this.getRemindingTokenTimeInSeconds();
		},
		defaultUserLoggedAction:function(prevLocation){
			if (prevLocation === "/login" || prevLocation === "/") {
				$location.path('/bienvenida');
			} else {
				$cookieStore.put(REDIRECT_COOKIES, prevLocation);
			}
		},
		defaultUserNotLoggedAction:function(prevLocation,event){
			if (prevLocation !== "/login" && prevLocation !== "/" && prevLocation !== "") {
				event.preventDefault();
				$location.path('/login');
				$log.info("user not logged: " + prevLocation);
				$cookieStore.put(REDIRECT_COOKIES, prevLocation);
		 }
	   },
	   handleClaveLoginAdminError:function(callbackError){
		
		   if($this.clientCertErrorCookiePresent()){
			   $log.debug("clientCertErrorCookiePresent");
			   var code = $this.getclientCertErrorCookieErrorCode();
			   if(callbackError !==undefined){
				   $log.debug("calling callback"+code);
				   callbackError(code);
			   }
		   }
	   },
	   getAuthenticationErrorFromRemoteCodeErrors:function(code){
		
		   var authenticationMessage ="";
		   if (code === -1) {
			   authenticationMessage = "WRONGCREDENTIALS";
			} else if (code === -2) {
				authenticationMessage = "USERBLOCKED";
			} else if(code === -3){
				authenticationMessage = "PWDMUSTCHANGE";
			}else if(code === -4){
				authenticationMessage = "PWDEXPIRED";
			}else if(code === -5){
				authenticationMessage = "TOKENINVALID";
			}else if(code === -6){
				authenticationMessage = "USERNOTREGISTER";
			}else if(code === -7){
				authenticationMessage = "USERLACKSPRIVILEGES";
			}
		 
		   return authenticationMessage;
	   }
	  
	}
});