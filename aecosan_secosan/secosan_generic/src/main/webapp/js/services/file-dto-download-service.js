angular.module("secosanGeneric").factory("FileDtoDownload", function() {

	var $this = this;

	$this.b64toBlob = function(b64Data, contentType, sliceSize) {
		contentType = contentType || '';
		sliceSize = sliceSize || 512;

		var byteCharacters = atob(b64Data);
		var byteArrays = [];

		for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
			var slice = byteCharacters.slice(offset, offset + sliceSize);

			var byteNumbers = [];
			byteNumbers.size = slice.length;

			for (var i = 0; i < slice.length; i++) {
				byteNumbers[i] = slice.charCodeAt(i);
			}

			var byteArray = new Uint8Array(byteNumbers);

			byteArrays.push(byteArray);
		}

		var blob = new Blob(byteArrays, {
			type : contentType
		});
		return blob;
	};

	return {
		decryptAndDownloadFile : function(base64File, fileName) {
			var contentType = "application/pdf";
			var blob = $this.b64toBlob(base64File, contentType);
			saveAs(blob, fileName);
		}
	}
});