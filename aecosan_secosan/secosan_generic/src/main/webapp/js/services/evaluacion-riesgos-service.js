angular.module("secosanGeneric").factory("EvaluacionRiesgos", function($http, $log, AuthenticationUser,RemovePaymentData, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addRiesgo : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.EVALUACION_RIESGOS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateRiesgo : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.EVALUACION_RIESGOS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});