angular.module("secosanGeneric").factory("RegistryNumberFormatService", function() {

	_that = this;

	_that.checkSizeAndFill = function(num, size) {
		var s = "000000" + num;
		return s.substr(s.length - size);
	};

	_that.checkUndefined = function(num) {
		var res = "";
		if (num !== undefined) {
			res = num;
		}

		return res;
	}

	return {
		composeRGSEAA : function(numRGSEAA1, numRGSEAA2, numRGSEAA3) {
			var result = "";

			numRGSEAA1 = _that.checkUndefined(numRGSEAA1);
			numRGSEAA2 = _that.checkUndefined(numRGSEAA2);
			numRGSEAA3 = _that.checkUndefined(numRGSEAA3);

			if (numRGSEAA1 !== "" || numRGSEAA2 !== "" || numRGSEAA3 !== "") {
				if (numRGSEAA1.length > 0 && numRGSEAA1.length < 2) {
					numRGSEAA1 = _that.checkSizeAndFill(numRGSEAA1, 2);
				}
				if (numRGSEAA2.length > 0 && numRGSEAA2.length < 6) {
					numRGSEAA2 = _that.checkSizeAndFill(numRGSEAA2, 6);
				}
				if (numRGSEAA3.length > 0 ) {
					numRGSEAA3 = numRGSEAA3.toUpperCase();
				}

				result = numRGSEAA1 + "-" + numRGSEAA2 + "/" + numRGSEAA3;
			}
			return result;
		},
		putRGSEAAInView : function($scope, rgseaa) {
			if (rgseaa !== "") {
				var parts = rgseaa.split('-');
				$scope.auxNumRGSEAA.auxNumRGSEAA1 = parts[0];
				parts = parts[1].split('/');
				$scope.auxNumRGSEAA.auxNumRGSEAA2 = parts[0];
				$scope.auxNumRGSEAA.auxNumRGSEAA3 = parts[1];
			}
		},
		composeCompanyRegisterNumber : function(numRegComp1, numRegComp2, numRegComp3) {
			var result = "";

			numRegComp1 = _that.checkUndefined(numRegComp1);
			numRegComp2 = _that.checkUndefined(numRegComp2);
			numRegComp3 = _that.checkUndefined(numRegComp3);

			if (numRegComp1 !== "" || numRegComp2 !== "" || numRegComp3 !== "") {


				if (numRegComp1.length > 0 && numRegComp1.length < 2) {
					numRegComp1 = _that.checkSizeAndFill(numRegComp1, 2);
				}
				if (numRegComp2.length > 0 && numRegComp2.length < 6) {
					numRegComp2 = _that.checkSizeAndFill(numRegComp2, 6);
				}
				if (numRegComp3.length > 0 ) {
					numRegComp3 = numRegComp3.toUpperCase();
				}
				result = numRegComp1 + "." + numRegComp2 + "/" + numRegComp3;
			}
			return result;
		},
		putCompanyRegisterNumberInView : function($scope, companyRegisterNumber) {
			if (companyRegisterNumber !== "") {
				var parts = companyRegisterNumber.split('.');
				$scope.companyRegisterNumberAux.numRegEmp1 = parts[0];
				parts = parts[1].split('/');
				$scope.companyRegisterNumberAux.numRegEmp2 = parts[0];
				$scope.companyRegisterNumberAux.numRegEmp3 = parts[1];
			}
		},
		composeProductReferenceNumber : function(numRegPro1, numRegPro2, numRegPro3, numRegPro4) {
			var result = "";

			numRegPro1 = _that.checkUndefined(numRegPro1);
			numRegPro2 = _that.checkUndefined(numRegPro2);
			numRegPro3 = _that.checkUndefined(numRegPro3);
			numRegPro4 = _that.checkUndefined(numRegPro4);

			if (numRegPro1 !== "" || numRegPro2 !== "" || numRegPro3 !== "" || numRegPro4 !== "") {

				if (numRegPro1.length > 0 && numRegPro1.length < 2) {
					numRegPro1 = _that.checkSizeAndFill(numRegPro1, 2);
				}
				if (numRegPro2.length > 0 && numRegPro2.length < 6) {
					numRegPro2 = _that.checkSizeAndFill(numRegPro2, 6);
				}
				
				if (numRegPro4.length > 0 && numRegPro4.length < 2) {
					numRegPro4 = _that.checkSizeAndFill(numRegPro4, 2);
				}
				result = numRegPro1 + " " + numRegPro2 + "/" + numRegPro3 + "-" + numRegPro4;
			}
			return result;
		},
		putProductReferenceNumberInView : function($scope, productReferenceNumber) {
			if (productReferenceNumber !== "") {
				var parts = productReferenceNumber.split(' ');
				$scope.productReferenceNumber.numRegPro1 = parts[0];
				parts = parts[1].split('/');
				$scope.productReferenceNumber.numRegPro2 = parts[0];
				parts = parts[1].split('-');
				$scope.productReferenceNumber.numRegPro3 = parts[0];
				$scope.productReferenceNumber.numRegPro4 = parts[1];
			}
		},
	}
});