angular.module("secosanGeneric").factory(
		"ModalParamsService",
		function($log, $location, $rootScope, Solicitude, DocumentacionService, UserManagementService, Messages, REDIRECT_URL, SENDER_MODE) {

			return {
				genericFilesError : function() {
					return {
						title : "Gesti\u00F3n de documentos",
						description : "Indicar tipo y adjuntar documento.",
						type : "exclamation"
					};
				},
				formatFileError : function() {
					return {
						title : "Gesti\u00F3n de documentos",
						description : "El formato del documento debe ser PDF y su tama\u00F1o menor de 20MB.",
						type : "exclamation"
					};
				},
				formatFileErrorMagicNumber: function(){
					return {
						title : "Gesti\u00F3n de documentos",
						description : "La firma del documento no concuerda con el tipo de fichero esperado",
						type : "exclamation"
					};
				},
				RGSEAAError : function() {
					return {
						title : "Modificaci\u00F3n de datos",
						description : "¡¡ error en Nº RGSEAA !!",
						type : "exclamation"
					};
				},
				financialProductError : function() {
					return {
						title : "Gesti\u00F3n de productos",
						description : "Por favor rellene todos los campos.",
						type : "exclamation"
					};
				},
				presentationError : function() {
					return {
						title : "Gesti\u00F3n de presentaciones",
						description : "Por favor rellene todos los campos.",
						type : "exclamation"
					};
				},
				maxItemsError : function() {
					return {
						title : "Gesti\u00F3n de presentaciones",
						description : "Ha excedido el número máximo de presentaciones que pueden añadirse.",
						type : "exclamation"
					};
				},
				confirmDeleteDocument : function(idBBDD, index, documentacion, $scope) {
					$log.debug("ConfirmDeleteDocument: Llamada a okFunction en modal");
					return {
						title : "Gesti\u00F3n de documentaci\u00F3n",
						description : "Est\u00E1  seguro que quiere borrar el documento. Esta acci\u00F3n no se puede deshacer",
						type : "question",
						okFunction : function() {
							if (idBBDD !== undefined) {
								DocumentacionService.deleteDocumentacionById(idBBDD).success(function(data) {
									documentacion.splice(index, 1);
								}).error(function(data, status) {
									$log.error("Download documentacion error");
									Messages.setErrorMessageFromResponse(data, status);
								});
							} else {
								documentacion.splice(index, 1);
							}
							$scope.$apply();
						},
						okButtonStyle : "btn-danger"
					};
				},
				deleteSolicitude : function(id, idNumber, tableConfig) {
					$log.debug("delete solicitude modal params");
					return {
						title : "Borrado de solicitudes",
						okButtonStyle : "btn-danger",
						description : "\u00BFEst\u00E1 seguro de que desea eliminar permanentemente  la solicitud con numero de identifica\u00F3n " + idNumber
								+ "? Una vez realizada esta acci\u00F3n la solicitud no podr\u00E1 ser recuperada.",
						type : "question",
						okButton : "Eliminar",
						okFunction : function() {
							Solicitude.deleteSolicitud(id).success(function() {
								tableConfig.reload();
							}).error(function() {
								Messages.setMessageAndType("No fue posible eliminar la solicitud:" + idNumber + " debido a un error interno");
							});
						}

					}
				},

				uploadFileSuccess : function() {
					return {
						title : "Gesti\u00F3n de instrucciones.",
						description : "Las instrucciones han sido actualizadas correctamente.",
						type : "ok",
					}
				},
				uploadFileError : function() {
					return {
						title : "Gesti\u00F3n de instrucciones.",
						description : "Ha ocurrido un error con la actualización de las instrucciones.",
						type : "error",
					}
				},
				sessionExpired : function() {
					return {
						title : 'Session Cerrada',
						description : "Sesion cerrada por inactividad",
						type : "exclamation",
						okFunction : function() {
							$location.path(REDIRECT_URL.LOGIN);
							$rootScope.$apply();
						}
					}
				},
				addedSolicitudSuccess : function(identificationNumber, isDraft) {
					$log.debug("isDraft--->" + isDraft);
					var firstPartOfTitle = "Enviar";
					var addedAction = "enviada";
					if (isDraft === SENDER_MODE.DRAFT) {
						firstPartOfTitle = "Crear Borrador";
						addedAction = "creada en modo borrador, puede continuar la edición luego";
					}
					return {
						title : firstPartOfTitle + ' Solicitud',
						description : "La solicitud con identificador:" + identificationNumber + " ha sido " + addedAction,
						type : "ok",
						okFunction : function() {
							$location.path(REDIRECT_URL.WELCOME);
							$rootScope.$apply();
						}
					}
				},
				addedSolicitudError : function(identificationNumber, isDraft) {
					var firstPartOfTitle = "Enviar";
					var addedAction = "enviada";
					if (isDraft === SENDER_MODE.DRAFT) {
						firstPartOfTitle = "Crear Borrador";
						addedAction = "creada en modo borrador";
					}
					return {
						title : firstPartOfTitle + ' Solicitud',
						description : "La solicitud con identificador:" + identificationNumber + " no pudo ser " + addedAction,
						type : "error"
					}
				},
				genericErrorMessage : function(message) {
					return {
						title : 'Error en envio',
						description : message,
						type : "error"
					}
				},deleteProductError : function() {
					return {
						title : "Eliminaci\u00F3n de Productos.",
						description : "Ha ocurrido un error al eliminar el producto del listado.",
						type : "error",
					}
				},
			}

		});