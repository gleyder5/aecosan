angular.module("secosan").factory("ProcedureDescriptionService", function($http, $log, AuthenticationUser, REST_URL, HEADERS) {
	let $this = this;
	$this.data = {};
	$this.tipoProcedimiento = "";
	return {
		getDataIdx : function(idx) {
			if($this.data[idx]!=undefined){
				return $this.data[idx];
			}
		},

        getData : function() {
			return $this.data;
        },
		setData : function (data) {
            Object.keys(data).forEach(function (v) {
            	$this.data[v]= data[v];
			});
        }

	}
});