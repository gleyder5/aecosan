angular.module("secosan").factory("UserManagementService", function($http, $log, AuthenticationUser,base64, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}
	
	this.encodePassword = function(form,formPasswordKey){
		var plainPassword = form[formPasswordKey];
		form[formPasswordKey] = base64.encode(plainPassword);
		return form;
	}

	return {
		addUserNoUe : function(form) {
			var url = REST_URL.REGISTRY_USER_NOUE;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : $this.encodePassword(form,"password")
			})
		},
		changePassword : function(form) {
			var url = REST_URL.CHANGE_PASSWORD;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});