angular.module("secosanGeneric").factory("RegistroAguasService", function($http, $log, AuthenticationUser, RemovePaymentData, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addAguas : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.REGISTRO_AGUAS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateAguas : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.REGISTRO_AGUAS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});