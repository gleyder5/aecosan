angular.module("secosanGeneric").factory("SetDataFromCatalogService", function($log, CatalogueHandler, Messages, CatalogueLocationHandler, DocTypeService, CATALOGUE_NAMES) {
	var $this = this;

	$this.catalogToFilter = function(catalog) {
		var filter = [];
		filter.push({
			'id' : "",
			'title' : ""
		});
		angular.forEach(catalog, function(item) {
			filter.push({
				'id' : item.catalogValue,
				'title' : item.catalogValue
			});
		});
		return filter;
	};
	
	this.setDataIntoScopeVariable = function($scope,scopeVariable,data){
		if(scopeVariable !== undefined){
			$scope[scopeVariable] = data.catalog;
		}else{
			CatalogueLocationHandler.countryCatalogSuccessHandler($scope, data);
		}
	}

	return {
		getCountryCatalogue : function($scope,scopeVariable) {
			CatalogueHandler.getCatalogue(CATALOGUE_NAMES.COUNTRIES).success(function(data) {
				$log.info("OK getting catalog " + CATALOGUE_NAMES.COUNTRIES);
				if(scopeVariable!==undefined){
					$this.setDataIntoScopeVariable($scope,scopeVariable,data);
				}else{
					CatalogueLocationHandler.countryCatalogSuccessHandler($scope, data);
				}
			}).error(function(data, status) {
				CatalogueLocationHandler.countryCatalogErrorHandler(data, status);
			});
		},
		getRelatedProvinceCatalogueFromCountry: function ($scope,$countryId,scopedVariable) {
                CatalogueHandler.getRelatedCatalogue(CATALOGUE_NAMES.PROVINCIES, CATALOGUE_NAMES.MUNICIPALITY_PARENT_COUNTRY, $countryId).success(function(data) {
                    $scope[scopedVariable] = [];
                    $scope[scopedVariable]= data.catalog;
                });
        },
        getRelatedMunicipioCatalogueFromProvince: function ($scope,$provinceId,scopedVariable) {
            CatalogueHandler.getRelatedCatalogue(CATALOGUE_NAMES.MUNICIPALITY, CATALOGUE_NAMES.MUNICIPALITY_PARENT_PROVINCIE, $provinceId).success(function(data) {
                $scope[scopedVariable] = [];
                $scope[scopedVariable]= data.catalog;
            });
        },
		getFormatsCatalogue : function($scope) {
			var catalogFormas = CATALOGUE_NAMES.FORMATS;
			CatalogueHandler.getCatalogue(catalogFormas).success(function(data) {
				$log.info("OK getting catalog " + catalogFormas);
				$scope.catalogoFormas = data.catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogFormas + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getEUCountriesCatalogue : function($scope,scopeVariable) {
			var catalogPaisUE = CATALOGUE_NAMES.COUNTRIES;
			CatalogueHandler.getTaggedCatalogue(catalogPaisUE, "paisesEU").success(function(data) {
				$log.info("OK getting catalog " + catalogPaisUE);
				$this.setDataIntoScopeVariable($scope,scopeVariable,data);
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogPaisUE + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getNoEUCountriesCatalogue : function($scope,scopeVariable) {
			var catalogPaisNoUE = CATALOGUE_NAMES.COUNTRIES;
			CatalogueHandler.getTaggedCatalogue(catalogPaisNoUE, "paisesNoEU").success(function(data) {
				$log.info("OK getting catalog " + catalogPaisNoUE);
				$this.setDataIntoScopeVariable($scope,scopeVariable,data);
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogPaisNoUE + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getProductTypeCatalogue : function($scope) {
			var catalogProductTypeName = CATALOGUE_NAMES.PRODUCT_TYPE;
			CatalogueHandler.getCatalogue(catalogProductTypeName).success(function(data) {
				$log.info("OK getting catalog " + catalogProductTypeName);
				$scope.catalogProductTypeAll = data.catalog;
				$scope.catalogProductTypeAlimEsp = [];
				for (var i = 0, len = data.catalog.length; i < len; i++) {
					if (data.catalog[i].catalogId === "1") {
						$scope.catalogProductTypeAlimEsp.push(data.catalog[i]);
					}
				}
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogProductTypeName + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getAdminModeCatalogue : function($scope) {
			var catalogAdminModeName = CATALOGUE_NAMES.ADMINISTRATION_MODE;
			CatalogueHandler.getCatalogue(catalogAdminModeName).success(function(data) {
				console.debug(data);
				$log.info("OK getting catalog " + catalogAdminModeName);
				$scope.catalogAdminMode = data.catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogAdminModeName + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getSuspensionTypeCatalogue : function($scope) {
			var catalogTipos = CATALOGUE_NAMES.SUSPENSION_TYPES;
			CatalogueHandler.getCatalogue(catalogTipos).success(function(data) {
				$log.info("OK getting catalog " + catalogTipos);
				$scope.catalogTiposSuspension = data.catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogTiposSuspension + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getPaymentTypeCatalogue : function($scope, filter) {
			var catalogTipoPagoName = CATALOGUE_NAMES.PAYMENT_TYPE;
			CatalogueHandler.getCatalogue(catalogTipoPagoName).success(function(data) {
				var catalog = data.catalog
				if (filter != undefined) {
					catalog = filter(catalog);
				}
				$scope.catalogTipoPago = catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogTipoPagoName + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getPaymentCatalogue : function($scope, filter) {
			var catalogTipoPago = CATALOGUE_NAMES.PAYMENT;
			CatalogueHandler.getCatalogue(catalogTipoPago).success(function(data) {
				$log.info("OK getting catalog " + catalogTipoPago);
				var catalog = data.catalog
				if (filter != undefined) {
					catalog = filter(catalog);
				}
				$scope.catalogFormaPago = catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogTipoPago + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getGruposUsuarioCatalogue : function($scope) {
			var catalogGruposUsuario = CATALOGUE_NAMES.USER_AREA;
			CatalogueHandler.getCatalogue(catalogGruposUsuario).success(function(data) {
				$log.info("OK getting catalog " + catalogGruposUsuario);
				$scope.catalogoGrupos = data.catalog;
				$scope.areaFilter = $this.catalogToFilter($scope.catalogoGrupos);
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogGruposUsuario + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getRolesUsuarioCatalogue : function($scope) {
			var catalogRolesUsuario = CATALOGUE_NAMES.USER_ROLE;
			CatalogueHandler.getCatalogue(catalogRolesUsuario).success(function(data) {
				$log.info("OK getting catalog " + catalogRolesUsuario);
				$scope.catalogoRoles = data.catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogRolesUsuario + " " + status);
				Messages.setErrorMessageFromResponse(data, status);
			});
		},
		getDocumentTypeCatalog : function($scope, formID) {
			if(formID!=undefined) {
                DocTypeService.getDocTypesByFormID(formID).success(function (data) {
                    $log.info("OK getting catalog DocumentTypeCatalog");
                    $scope.dataOption.availableOptions = $scope.dataOption.availableOptions.concat(data);
                }).error(function (data, status) {
                    Messages.setErrorMessageFromResponse(data, status);
                    $log.error("Error getting catalog DocumentTypeCatalog " + status);
                });
            }
		},
		getSolicitudeStatusCatalog : function($scope) {
			DocTypeService.getSolicitudeStatusByAreaID($scope.areaCode).success(function(data) {
				$log.info("OK getting catalog SolicitudeStatusCatalog");
				$scope.catalogSolicitudeStatus = data;
			}).error(function(data, status) {
				Messages.setErrorMessageFromResponse(data, status);
				$log.error("Error getting catalog SolicitudeStatusCatalog " + status);
			});
		},
		getShippingTypeCatalogue : function($scope) {
			CatalogueHandler.getCatalogue(CATALOGUE_NAMES.SHIPPING_TYPE).success(function(data) {
				$log.info("OK getting catalog " + CATALOGUE_NAMES.SHIPPING_TYPE);
				$scope.catalogoTipoEnvio = data.catalog;
			}).error(function(data, status) {
				$log.error("Error getting catalog ShippingTypeCatalogue " + status);
			});
		}
	}
});