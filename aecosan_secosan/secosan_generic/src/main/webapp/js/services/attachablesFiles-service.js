angular.module("secosanGeneric").factory("AttachablesFilesService", function($http, $log,FileDtoDownload,Spinner, REST_URL) {
	var $this = this;
	$this.baseUrl = REST_URL.ATTACHABLE_FILE;
	return {
		getFile : function(fileName,callbackSuccess,callbackError) {
			Spinner.showSpinner();
			var url = $this.baseUrl + fileName;
			$http({
				method : 'GET',
				url : url
			}).success(function(data) {
				FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
				if(!angular.equals(callbackSuccess,undefined)){
					callbackSuccess(data);
				}
				Spinner.hideSpinner();
			}).error(function(data) {
				Messages.setErrorMessageFromResponse(data, status);
				if(!angular.equals(callbackError,undefined)){
					callbackError(data)
				}
				Spinner.hideSpinner();
			});
		}
	}
});