angular.module("secosanGeneric").factory("UtilService", function() {
	var $this = this;

	$this.checkNotNullNorUndefined = function(value) {
		return (value !== undefined && value !== null && value!=="")
	};

	return {
		checkNotNullNorUndefined : function(value) {
			return $this.checkNotNullNorUndefined(value);
		}
	};

});
