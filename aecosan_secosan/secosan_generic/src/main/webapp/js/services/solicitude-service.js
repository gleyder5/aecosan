angular.module("secosanGeneric").factory("Solicitude", function($http, REST_URL, HEADERS, PaginationFilter, AuthenticationUser) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}
	
	this.solicitudeRestUrl = REST_URL.SOLICITUDE;
	this.solicitudeRestListUrl = REST_URL.SOLICITUDE;

	return {
		getSolicitudeByAuthor : function(filterParams) {
			if(!angular.equals($this.getUserLoggedId(),undefined)){
				var url = REST_URL.SOLICITUDE_LIST + $this.getUserLoggedId();
				url = PaginationFilter.getPaginationUrl(url, filterParams);
				return $http({
					method : 'GET',
					url : url
				})
			}
		},
		getSolicitudeById : function(id) {
			var url = REST_URL.SOLICITUDE + $this.getUserLoggedId() + "/" + id;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			})
		},
		deleteSolicitud : function(id) {
			var url = REST_URL.SOLICITUDE + $this.getUserLoggedId() + "/" + id;
			return $http({
				method : 'DELETE',
				url : url,
				headers : HEADERS.JSON
			})
		},
		setSolicitudeRestUrl:function(baseUrl){
			$this.solicitudeRestUrl = baseUrl;
		},
		setSolicitudeRestListUrl:function(baseUrl){
			$this.solicitudeRestListUrl = baseUrl;
		}
	}
});