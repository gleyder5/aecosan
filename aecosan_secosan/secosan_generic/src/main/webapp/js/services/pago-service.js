angular.module("secosanGeneric").factory("PagoService", function($log, $translate) {
	var pagoArgs = {};
	pagoArgs['isPaid']   =  false;
	return {
		// setters
		setIdSolicitude : function(idSolicitude) {
			pagoArgs['idSolicitude'] = idSolicitude;
		},

		setIdSolicitudeType : function(idSolicitudeType) {
			pagoArgs['idSolicitudeType'] = idSolicitudeType;
		},

		setPaymentData : function(paymentData) {
			pagoArgs['paymentData'] = paymentData;
		},

		setServiceId : function(serviceId) {
			pagoArgs['serviceId'] = serviceId;
		},
		setWindowPagoOfflineVisible : function(visible) {
			pagoArgs['windowPagoOfflineVisible'] = visible;
		},

		setVisible : function(visible) {
			pagoArgs['visible'] = visible;
		},

		setServicioSolicitado : function(servicio){
			pagoArgs["servicioSolicitado"] = servicio;

		},
		// getters
		getPagoArgs : function() {
			return pagoArgs;
		},

		// methods
		clearPagoArgs : function() {
			pagoArgs = {};
		},

		setPaid : function (isPaid) {
            pagoArgs['isPaid'] = isPaid;
		},

        isPaid : function (isPaid) {
            return pagoArgs['isPaid'];
        },

		showPagoPanel : function(pagoParams) {
			this.setIdSolicitude(pagoParams.idSolicitude);
			this.setIdSolicitudeType(pagoParams.idSolicitudeType);
			this.setPaymentData(pagoParams.paymentData);
			this.setServiceId(pagoParams.serviceId);
			this.setVisible(true);
		},
		hidePagoPanel:function(){
			this.setVisible(false);
		}
	}
});