angular.module("secosanGeneric").factory("HeaderTitle", function() {
	var $this = this;
	this.titles = {
		title : "",
		subtitle : ""
	};

	return {
		getTitles : function() {
			return $this.titles;
		},
		setTitles : function(title, subtitle) {
			$this.titles.title = title;
			if (subtitle) {
				$this.titles.subtitle = subtitle;
			}
		},
		clearTitles : function() {
			$this.titles.title = "";
			$this.titles.subtitle = "";
		}
	}

});