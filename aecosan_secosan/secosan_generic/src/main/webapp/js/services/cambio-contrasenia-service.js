angular.module("secosanGeneric").factory("CambioContraseniaService", function($log, $translate) {
	var cambioContraseniaArgs = {};

	return {
		// setters
		setVisible : function(visible) {
			cambioContraseniaArgs['visible'] = visible;
		},

		// getters
		getCambioContraseniaArgs : function() {
			return cambioContraseniaArgs;
		},

		// methods
		clearCambioContraseniaArgs : function() {
			cambioContraseniaArgs = {};
		},

		showCambioContraseniaWindow : function(cambioContraseniaParams) {
			this.setVisible(true);
		},
		hideCambioContraseniaWindow : function() {
			this.setVisible(false);
		}
	}
});