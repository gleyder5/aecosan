angular.module('secosanGeneric').factory("DocTypeService", function($http, $log, REST_URL, HEADERS) {
	var $this = this;
	this.baseUrl = REST_URL.DOC_TYPES;
	return {
		getDocTypesByFormID : function(formID) {
			var url = $this.baseUrl + formID;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		setBaseUrl:function(baseUrl){
			$this.baseUrl = baseUrl;
		}
	}
});