angular.module("secosan").factory("PagoOnline", function($log,$http,base64,REST_URL,HEADERS,PAYMENT_TYPE) {

	var $this =this;
	
	$this.requestJustifier = function(formData,paymentType,serviceId){

		var requestData = $this.extractPaymentDataFromFormForJustifier(formData,paymentType,serviceId);	
		return $http({
			method : 'POST',
			url : REST_URL.GET_JUSTIFICANTE,
			headers : HEADERS.JSON,
			data : requestData
		})
	};
	
	$this.extractPaymentDataFromFormForJustifier = function(formData,paymentType,serviceId){
		var requestData = {};
		// var amount = $this.parseAmmount(formData.solicitudePayment.amount, formData.solicitudePayment.amountDecimal);
		let amount =  formData.solicitudePayment.monto;
		var justifierParams ={
			 dataPayment : formData.solicitudePayment.dataPayment,
			 idPayment : formData.solicitudePayment.idPayment,
			 solicitudData : formData.formulario,	
			 bankCode : formData.solicitudePayment.bankCode.catalogId,
			 ammount :amount,
			 quantity: formData.solicitudePayment.payedTasas,
			 formType:formData.formularioEspecifico.id,
			 serviceType:serviceId,
	
		};
		
		if(paymentType == PAYMENT_TYPE.ONLINE_TARJETA){
			requestData = $this.getRequestDataForCreditCard(justifierParams);
		}else if (paymentType == PAYMENT_TYPE.ONLINE_CUENTA){
		
			requestData = $this.getRequestForAccount(justifierParams);
		}
	
		return requestData;
	};
	
	$this.getRequestDataForCreditCard = function(justifierParams) {
		
		var mainRequestData = $this.getMainRequestData(justifierParams);
		var expDate = $this.getCardExpirationDate(justifierParams.dataPayment);
		var creditCardtData = {
				expirationDate:expDate,
				cardNumber: base64.encode(justifierParams.dataPayment.cardNumber),
				cardIssuerCode:justifierParams.bankCode
			};
		mainRequestData.creditCardData = creditCardtData;
		return mainRequestData;
	};
	
	$this.getRequestForAccount = function (justifierParams){
		var mainRequestData = $this.getMainRequestData(justifierParams);
		var account = $this.parseAccountNumber(justifierParams.dataPayment,justifierParams.bankCode);
		var accountData = {
					bankCode:justifierParams.bankCode,
					accountNumber: base64.encode(account)
				}
		
		mainRequestData.accountData = accountData;
		return mainRequestData;
	};
	
	$this.parseAccountNumber = function(dataPayment,bankCode){
		return dataPayment.countryCode+dataPayment.controlDigit+bankCode+dataPayment.office+dataPayment.dc+dataPayment.account
	}
	
	$this.getMainRequestData = function(justifierParams){
		var mainRequestData =   {
			ammount:justifierParams.ammount,
			personIdentificationNumber:justifierParams.idPayment.identificationNumber,
			formType:justifierParams.formType,
			quantity : justifierParams.quantity
		};
	
		if(!angular.equals(justifierParams.serviceType,undefined)){
			mainRequestData.serviceType =justifierParams.serviceType;
		}
		return mainRequestData;
	}
	
	$this.makePayment = function(formData,paymentType,justifierData,serviceId){
		
		var requestData = this.extractPaymentDataFromForm(formData,paymentType,justifierData,serviceId);
		return $http({
			method : 'POST',
			url : REST_URL.MAKE_PAYMENT,
			headers : HEADERS.JSON,
			data : requestData
		})
	};
	
	$this.extractPaymentDataFromForm = function(formData,paymentType,justifierData,serviceId){
		// var amount = $this.parseAmmount(formData.solicitudePayment.amount, formData.solicitudePayment.amountDecimal);
		let amount =  formData.solicitudePayment.monto;

		var paymentParams={
			 dataPayment : formData.solicitudePayment.dataPayment,
			 idPayment : formData.solicitudePayment.idPayment,
			 solicitudData : formData.formularioEspecifico.id,
			 bankCode : formData.solicitudePayment.bankCode.catalogId,
			 ammount  : 	amount,
			 quantity : formData.solicitudePayment.payedTasas,
			 formType : formData.formularioEspecifico.id,
			 identificadorPeticion: formData.identificadorPeticion,
			 justifierData:justifierData,
		};
		var  requestData = {};
		if(paymentType ==PAYMENT_TYPE.ONLINE_TARJETA){
			requestData= $this.getRequestDataForCreditCardPayment(paymentParams,serviceId);
			
		}else if (paymentType ==PAYMENT_TYPE.ONLINE_CUENTA){
			
			requestData= $this.requestForAccountPayment(paymentParams,serviceId);
		}
		$log.debug(requestData);
		return requestData;
	}
	
	$this.getRequestDataForCreditCardPayment = function(paymentParams,serviceId) {
		var mainPaymentData = $this.requestPaymentMainData(paymentParams);
		var expDate = $this.getCardExpirationDate(paymentParams.dataPayment);
		var creditCardData ={
				expirationDate:expDate,
				creditCard:base64.encode(paymentParams.dataPayment.cardNumber),
				bankCode:paymentParams.bankCode,
			}

		mainPaymentData.creditCardPaymentInfo = creditCardData;
		return mainPaymentData;
	};
	
	$this.requestForAccountPayment = function (paymentParams,serviceId){
		var accountNum = $this.parseAccountNumber(paymentParams.dataPayment,paymentParams.bankCode);
		var mainPaymentData = $this.requestPaymentMainData(paymentParams,serviceId);
		var accountData = {
					accountNumber:base64.encode(accountNum),
					bankCode:paymentParams.bankCode,
				};
		mainPaymentData.accountPaymentInfo = accountData;
		return mainPaymentData;
	};
	
	$this.requestPaymentMainData = function(paymentParams,serviceId){
		return {
			name:paymentParams.idPayment.name,
			lastName:paymentParams.idPayment.lastName,
			secondlastName:paymentParams.idPayment.secondLastName,
			telephone:paymentParams.idPayment.telephone,
			ammount:paymentParams.ammount,
			quantity:paymentParams.quantity,
			personIdentificationType:paymentParams.idPayment.identificationType,
			personIdentificationNumber:paymentParams.idPayment.identificationNumber,
			signedJustifier:paymentParams.justifierData.signed,
			signedJustifierRaw:paymentParams.justifierData.signedRaw,
			justifier:paymentParams.justifierData.justifier,
			petitionIdentificator:paymentParams.identificadorPeticion,
			formType:paymentParams.formType,
			serviceType:serviceId,
			address:{
				streetTypeId:paymentParams.idPayment.address.tipoVia,
				streetName:paymentParams.idPayment.address.nombreCalle,
				number:paymentParams.idPayment.address.numero,
				stair:paymentParams.idPayment.address.escalera,
				story:paymentParams.idPayment.address.piso,
				door:paymentParams.idPayment.address.puerta
			},
			location:{
				countryId:paymentParams.idPayment.location.pais.catalogId,
				province:paymentParams.idPayment.location.provincia.catalogId,
				municipio:paymentParams.idPayment.location.municipio.catalogId,
				postalCode:paymentParams.idPayment.location.codigoPostal
			}
		}
	}
	
	$this.getCardExpirationDate = function(dataPayment){
		let firstDayOfMonth = 1;
		var expDate =  new Date(dataPayment.expirationYear.catalogId,dataPayment.expirationMonth.catalogId,firstDayOfMonth);
		return expDate.getTime();
	};
	
	$this.parseAmmount = function(ammount, amountDecimal){
	
		return ammount+"."+amountDecimal;
	};
	
	$this.isPaid = function(identificadorPeticion){
		var url = REST_URL.IS_PAID +identificadorPeticion;
		return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			})
	}
	
	return {
		requestJustifier: function(formData,paymentType,serviceId){
			return $this.requestJustifier(formData,paymentType,serviceId)
		},
		makePayment: function(formData,paymentType,justifierData,serviceId){
			return $this.makePayment(formData,paymentType,justifierData,serviceId)
		},
		isPaid: function(identificadorPeticion){
			return $this.isPaid(identificadorPeticion);
		}
	}
});