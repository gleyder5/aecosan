angular.module("secosanGeneric").factory("ServiciosPagoService", function($http, REST_URL, HEADERS) {

	return {
		getServiciosByPago : function(pago) {
			var url = REST_URL.SERVICIOSPAGO + pago;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			})
		},
	}
});