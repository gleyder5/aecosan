angular.module("secosanGeneric").factory("HeaderSolicitudNumber", function() {
	var $this = this;
	this.numAndTitle = {
		num : "",
		title : ""
	};
	return {

		clear : function() {
			$this.numAndTitle.num = "";
			$this.numAndTitle.title = "";
		},
		setNumberAndTitle : function(title, solicitudNumber) {
			$this.numAndTitle.num = solicitudNumber;
			$this.numAndTitle.title = title;
		},
		getNumberAndTitle : function() {
			return $this.numAndTitle;
		}

	}
});