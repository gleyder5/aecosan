angular.module("secosanGeneric").factory("ComplementosAlimenticios", function($http, $log, AuthenticationUser, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addCeseComercializacion : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_CESE + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateCeseComercializacion : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_CESE + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},

		addPuestaMercado : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},

		updatePuestaMercado : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},

		addModificacionDatos : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_MODIFICACION + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateModificacionDatos : function(form) {
			var url = REST_URL.COMPLEMENTOS_ALIMENTICIOS_MODIFICACION + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});