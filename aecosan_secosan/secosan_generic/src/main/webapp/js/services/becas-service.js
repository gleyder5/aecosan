angular.module("secosanGeneric").factory("Becas", function($http, $log, AuthenticationUser, REST_URL, HEADERS,FileDtoDownload,PaginationFilter) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
        getBecaById : function (becaId) {
            var url = REST_URL.BECAS +becaId+"/"+ $this.getUserLoggedId()+"/";
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        },
        getIdiomasBecasList : function(becaId) {
            var url = REST_URL.BECAS +becaId+"/idiomas/"+ $this.getUserLoggedId()+"/";
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        },
        getBecasSolicitantes : function(params) {
            var url = REST_URL.BECAS_SOLICITANTES + $this.getUserLoggedId();
            url = PaginationFilter.getPaginationUrl(url, params);
            return $http({
                method : 'GET',
                url : url
            })
        },
        getBecasList : function() {
            var url = REST_URL.BECAS_SOLICITANTES + $this.getUserLoggedId();
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        },
        getReporteResolucionProvisional : function (becaId,callbackOk,callbackError) {
            var url = REST_URL.RESOLUCION_PROV_REPORT + $this.getUserLoggedId()+"/"+becaId;
            $http({
                method : 'POST',
                url : url,
                headers : HEADERS.JSON,
            }).success(function (data) {

                FileDtoDownload.decryptAndDownloadFile(data.pdfB64, data.reportName);
                callbackOk();
            }) .error(function () {
                callbackError();
            });
        },
        getTotalScore : function(solicitudId) {
            var url = REST_URL.BECAS_SOLICITANTES + $this.getUserLoggedId()+"/"+solicitudId +"/score/";
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        },
		addRegistro : function(form) {
			var url = REST_URL.BECAS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateRegistro : function(form) {
			var url = REST_URL.BECAS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			});
		}
	}
});