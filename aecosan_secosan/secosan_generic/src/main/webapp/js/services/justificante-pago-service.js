angular.module("secosanGeneric").factory("JustificantePagoService", function($log, $http, FileDtoDownload, AuthenticationUser, REST_URL, HEADERS) {
	var $this = this;

	$this.getPdfReport = function(solicitudeId,serviceId) {

		let url = REST_URL.JUSTIFICANTE_PAGO + solicitudeId;
		if(serviceId!=undefined){
            url += "/" + serviceId;
		}
		return $http({
			method : 'GET',
			url : url,
			headers : HEADERS.JSON
		})

	};

	$this.isPaid = function(solicitudeId) {
		var url = REST_URL.IS_PAID + solicitudeId;

		return $http({
			method : 'GET',
			url : url,
			headers : HEADERS.JSON
		})
	};

	return {
		getJustificantePago : function(solicitudeId, callbackSuccess, callbackFail,serviceId) {
			$log.debug("getJustificantePago || solicitudeId:" + solicitudeId);
			$this.getPdfReport(solicitudeId,serviceId).success(function(data) {
				FileDtoDownload.decryptAndDownloadFile(data.pdfB64, data.reportName);
				if(!angular.equals(callbackSuccess,undefined)){
					callbackSuccess(data);
				}
			}).error(function(data, status) {
				if(!angular.equals(callbackFail(data, status))){
					callbackFail(data, status)
				}
			});
		},
		isPaid : function(solicitudeId, callbackSuccess, callbackFail) {
			$log.debug("isPaid || solicitudeId:" + solicitudeId);
			$this.isPaid(solicitudeId).success(function(data) {
				if (!angular.equals(callbackSuccess, undefined)) {
					callbackSuccess(data);
				}
			}).error(function(data, status) {
				if (!angular.equals(callbackFail(data, status))) {
					callbackFail(data, status)
				}
			});
		}
	}
});