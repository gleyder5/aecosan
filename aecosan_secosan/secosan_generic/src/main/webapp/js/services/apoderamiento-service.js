angular.module("secosanGeneric").factory("ApoderamientoService", function(AuthenticationUser,$log, $http, $translate,HEADERS, Messages, REST_URL) {
    var $this = this;


    $this.getUserLoggedId = function() {
        return AuthenticationUser.getUserPayloadData("id");
    }
    $this.baseUrl = REST_URL.APODERAMIENTO;

    return {
        consultarApoderamieto : function(nif){
            let url = $this.baseUrl +"consultar/"+nif;
            return $http({
                method : 'GET',
                url : url,
                headers : HEADERS.JSON,
            })
        }
    }

});