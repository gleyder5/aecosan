angular.module("secosanGeneric").factory("SubvencionAsocuae", function($http, $log, AuthenticationUser, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addRegistro : function(form) {
			var url = REST_URL.SUBVENCIONES_ASOCUAE + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateRegistro : function(form) {
			var url = REST_URL.SUBVENCIONES_ASOCUAE + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			});
		}
	}
});