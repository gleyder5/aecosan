angular.module("secosanGeneric").factory("SolicitudLogos", function($http, AuthenticationUser, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addSolicitudLogos : function(form) {
			var url = REST_URL.SOLICITUD_LOGOS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateSolicitudLogos : function(form) {
			var url = REST_URL.SOLICITUD_LOGOS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});