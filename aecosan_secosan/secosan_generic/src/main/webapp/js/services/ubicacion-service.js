angular.module("secosanGeneric").factory("UbicacionService", function($http, $log, REST_URL) {
	return {

		getProvinciaByIdMunicipio : function(idMunicipio) {
			var url = REST_URL.GET_PROVINCIA + idMunicipio;
			return $http({
				method : 'GET',
				url : url
			})
		},

		cargaMunicipio : function(idProvincia) {
			var catalogMuni = CATALOGUE_NAMES.MUNICIPALITY;
			var idMunicipaly_fieldName = CATALOGUE_NAMES.MUNICIPALITY_PARENT_PROVINCIE;
			$log.debug("Getting catalog " + catalogMuni + " para " + idProvincia);
			return CatalogueHandler.getRelatedCatalogue(catalogMuni, idMunicipaly_fieldName, idProvincia);
		}
	}
});