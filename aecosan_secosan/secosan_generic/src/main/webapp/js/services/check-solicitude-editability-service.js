angular.module("secosanGeneric").factory("CheckSolicitudeEditabilityService", function($log,Messages, URL_PARAMETER, ERR_MSG, SOLICITUDE_EDIT_MODE_STATUS_CODE) {
	return {
		isEditable : function($scope, status, viewMode) {
			
			var editMode = !(SOLICITUDE_EDIT_MODE_STATUS_CODE.indexOf(status) === -1);

			if (viewMode === undefined && !editMode) {
				$scope.viewMode = true;
				Messages.setMessageAndType(ERR_MSG.VIEW_MODE, Messages.error);
			} else {
				if (viewMode === URL_PARAMETER.VIEW_MODE) {
					$scope.viewMode = true;
				} else {
					$scope.viewMode = false;
				}
			}
		}
	}
});