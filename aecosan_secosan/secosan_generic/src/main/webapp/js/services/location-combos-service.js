angular.module("secosanGeneric").factory("LocationCombosService", function($log, CatalogueHandler, CatalogueLocationHandler, CATALOGUE_NAMES,SetDataFromCatalogService) {
	var $this = this;
	this.catalogueHandler = CatalogueHandler;
	this.catalogueLocationHandler = CatalogueLocationHandler;

	$this.loadSolicitante = function($scope, data) {
        var idPaisEdSolicitante = data.solicitude.solicitante.location.pais.id;
        $scope.auxSolicitante.pais =  {};
        $scope.auxSolicitante.pais.catalogId  = idPaisEdSolicitante;


        SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisEdSolicitante,'catalogProvinciasSolicitante');

        if (data.solicitude.solicitante.location.provincia !== undefined) {
			var idProvinciaEdSolicitante = data.solicitude.solicitante.location.provincia.id;
			var valueProvinciaEdSolicitante = data.solicitude.solicitante.location.provincia.catalogValue;

			$scope.auxSolicitante.provincia = {};
            $scope.auxSolicitante.provincia.catalogId  = idProvinciaEdSolicitante;
			$scope.catalogProvinciasSolicitante = [ {
				catalogId : idProvinciaEdSolicitante,
				catalogValue : valueProvinciaEdSolicitante
			} ];
            SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaEdSolicitante,'catalogMunicipiosSolicitante');
		}
        $scope.auxSolicitante.municipio = {};
        if (data.solicitude.solicitante.location.municipio !== undefined) {
			var idMunicipioEdSolicitante = data.solicitude.solicitante.location.municipio.id;
            var valueMunicipioEdSolicitante = data.solicitude.solicitante.location.municipio.catalogValue;
            $scope.auxSolicitante.municipio = {};
            $scope.auxSolicitante.municipio.catalogId = idMunicipioEdSolicitante;
            $scope.catalogMunicipiosSolicitante = [ {
				catalogId : idMunicipioEdSolicitante,
				catalogValue : valueMunicipioEdSolicitante
			} ];
		}
	};

	$this.loadRepresentante = function($scope, data) {
		$scope.checkboxModel.representante = true;


		if (data.solicitude.representante.location !== undefined) {
            var idPaisEdRepresentante = data.solicitude.representante.location.pais.id;
            SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisEdRepresentante,'catalogProvinciasRepresentante');
            $scope.auxRepresentante.pais = {};
            $scope.auxRepresentante.pais.catalogId = idPaisEdRepresentante;


			if (data.solicitude.representante.location.provincia !== undefined) {
                var idProvinciaEdRepresentante = data.solicitude.representante.location.provincia.id;
				var valueProvinciaEdRepresentante = data.solicitude.representante.location.provincia.catalogValue;
				$scope.catalogProvinciasRepresentante = [ {
					catalogId : idProvinciaEdRepresentante,
					catalogValue : valueProvinciaEdRepresentante
				} ];
				$scope.auxRepresentante.provincia  = {};
                $scope.auxRepresentante.provincia.catalogId = idProvinciaEdRepresentante;
                SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaEdRepresentante,'catalogMunicipiosRepresentante');
            }

            if (data.solicitude.representante.location.municipio !== undefined) {
                var idMunicipioEdRepresentante = data.solicitude.representante.location.municipio.id;
                var valueMunicipioEdRepresentante = data.solicitude.representante.location.municipio.catalogValue;
                $scope.auxRepresentante.municipio = {};
                $scope.auxRepresentante.municipio.catalogId = idMunicipioEdRepresentante;
                $scope.catalogMunicipiosRepresentante = [ {
                    catalogId : idMunicipioEdRepresentante,
                    catalogValue : valueMunicipioEdRepresentante
                } ];


            }
		}
	};

	$this.loadContacto = function($scope, data) {
		$scope.checkboxModel.adicionales = true;

		if (data.solicitude.contactData.location !== undefined) {
			var idPaisEdContacto = data.solicitude.contactData.location.pais.id;
            $scope.auxContacto.pais =   {};
			$scope.auxContacto.pais.catalogId = idPaisEdContacto;
            SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisEdContacto,'catalogProvinciasContacto');


            if (data.solicitude.contactData.location.provincia !== undefined) {
				var idProvinciaEdContacto = data.solicitude.contactData.location.provincia.id;
				var valueProvinciaEdContacto = data.solicitude.contactData.location.provincia.catalogValue;
				$scope.catalogProvinciasContacto = [ {
					catalogId : idProvinciaEdContacto,
					catalogValue : valueProvinciaEdContacto
				} ];
                $scope.auxContacto.provincia  = {};
				$scope.auxContacto.provincia.catalogId = idProvinciaEdContacto;
                SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaEdContacto,'catalogMunicipiosRepresentante');
			}
			if (data.solicitude.contactData.location.municipio !== undefined) {
				var idMunicipioEdContacto = data.solicitude.contactData.location.municipio.id;
                var valueMunicipioEdContacto = data.solicitude.contactData.location.municipio.catalogValue;
                $scope.auxContacto.municipio = {};
                $scope.catalogMunicipiosContacto = [ {
					catalogId : idMunicipioEdContacto,
					catalogValue : valueMunicipioEdContacto
				} ];
				$scope.auxContacto.municipio.catalogId = idMunicipioEdContacto;
			}
		}
	};
	$this.loadPaisOrigen = function($scope, data) {
		if (data.solicitude.paisOrigen !== undefined) {
			var idPaisEdContacto = data.solicitude.paisOrigen.id;
			$scope.auxAltaPais.paisOrigen.catalogId = idPaisEdContacto;
		}
	};
    $this.loadPaisLugarOMedio = function($scope, data) {

        if (data.solicitude.lugarOMedio.location !== undefined) {
            if (data.solicitude.lugarOMedio.location.pais) {
                var idPaisLugarOMedio = data.solicitude.lugarOMedio.location.pais.id;
                $scope.auxLugarOMedio.pais =  {};
                $scope.auxLugarOMedio.pais.catalogId = idPaisLugarOMedio;
                SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisLugarOMedio,'catalogProvincias');
                if (data.solicitude.lugarOMedio.location.provincia !== undefined) {
                    var idProvinciaLugarOMedio = data.solicitude.lugarOMedio.location.provincia.id;
                    var valueProvinciaLugarOMedio = data.solicitude.lugarOMedio.location.provincia.catalogValue;
                    $scope.auxLugarOMedio.provincia = {};
                    $scope.auxLugarOMedio.provincia.catalogId = idProvinciaLugarOMedio;
                    $scope.catalogProvincia = [{
                        catalogId: idProvinciaLugarOMedio,
                        catalogValue: valueProvinciaLugarOMedio
                    }];
                    SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaLugarOMedio,'catalogMunicipiosContacto');
                }
                $scope.auxLugarOMedio.municipio = {};
                if (data.solicitude.lugarOMedio.location.municipio !== undefined) {
                    var idMunicipioLugarOMedio = data.solicitude.lugarOMedio.location.municipio.id;
                    var valueMunicipioLugarOMedio = data.solicitude.lugarOMedio.location.municipio.catalogValue;
                    $scope.auxLugarOMedio.municipio = {};
                    $scope.auxLugarOMedio.municipio.catalogId = idMunicipioLugarOMedio;
                    $scope.catalogMunicipio = [{
                        catalogId: idMunicipioLugarOMedio,
                        catalogValue: valueMunicipioLugarOMedio
                    }];

                }
            }
        }
    };
	$this.loadShipping = function($scope, data) {
		if (data.solicitude.shippingData.location !== undefined) {
			var idPaisEnvio = data.solicitude.shippingData.location.pais.id;
			$scope.auxEnvio.pais.catalogId = idPaisEnvio;
            SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisEnvio,'catalogProvinciasEnvio');

            if (data.solicitude.shippingData.location.provincia !== undefined) {
				var idProvinciaEnvio = data.solicitude.shippingData.location.provincia.id;
				var valueProvinciaEnvio = data.solicitude.shippingData.location.provincia.catalogValue;
				$scope.catalogProvinciasEnvio = [ {
					catalogId : idProvinciaEnvio,
					catalogValue : valueProvinciaEnvio
				} ];
				$scope.catalogProvinciaEnvio = [ {
					catalogId : idProvinciaEnvio,
					catalogValue : valueProvinciaEnvio
				} ];
                $scope.auxEnvio.provincia  = {};
				$scope.auxEnvio.provincia.catalogId = idProvinciaEnvio;
                SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaEnvio,'catalogMunicipiosEnvio');
			}
            $scope.auxEnvio.municipio = {};
			if (data.solicitude.shippingData.location.municipio !== undefined) {
				var idMunicipioEnvio = data.solicitude.shippingData.location.municipio.id;
				var valueMunicipioEnvio = data.solicitude.shippingData.location.municipio.catalogValue;
                $scope.auxEnvio.municipio = {};
				$scope.catalogMunicipiosEnvio = [ {
					catalogId : idMunicipioEnvio,
					catalogValue : valueMunicipioEnvio
				} ];
				$scope.auxEnvio.municipio.catalogId = idMunicipioEnvio;
			}
		}
	};

	$this.loadPayment = function($scope, data) {

		if (data.idPayment.location.pais.id !== "") {
			var idPaisPayment = data.idPayment.location.pais.id;
			$scope.auxPago.pais.catalogId = idPaisPayment;
            SetDataFromCatalogService.getRelatedProvinceCatalogueFromCountry($scope,idPaisPayment,'catalogProvinciasPago');

			if (data.idPayment.location.provincia.id !== "") {
				var idProvinciaPayment = data.idPayment.location.provincia.id;
				var valueProvinciaPayment = data.idPayment.location.provincia.catalogValue;
				$scope.catalogProvinciasPago = [ {
					catalogId : idProvinciaPayment,
					catalogValue : valueProvinciaPayment
				} ];
				$scope.auxPago.provincia.catalogId = idProvinciaPayment;
                SetDataFromCatalogService.getRelatedMunicipioCatalogueFromProvince($scope,idProvinciaPayment,'catalogMunicipiosPago');

            }
            $scope.auxPago.municipio = {};
			if (data.idPayment.location.municipio.id !== "") {
				var idMunicipioPago = data.idPayment.location.municipio.id;
				var valueMunicipioPago = data.idPayment.location.municipio.catalogValue;
                $scope.auxPago.municipio = {};
				$scope.catalogMunicipiosPago = [ {
					catalogId : idMunicipioPago,
					catalogValue : valueMunicipioPago
				} ];
				$scope.auxPago.municipio.catalogId = idMunicipioPago;
			}
		}
	};

	// $this.loadIndustria = function($scope, data) {
	// $scope.checkboxModel.adicionales = true;
	//
	// if (data.solicitude.industriaData.location !== undefined) {
	// var idPaisEdContacto = data.solicitude.industriaData.location.pais.id;
	// $scope.auxIndustria.pais.catalogId = idPaisEdContacto;
	//
	// if (data.solicitude.industriaData.location.provincia !== undefined) {
	// var idProvinciaEdContacto = data.solicitude.industriaData.location.provincia.id;
	// var valueProvinciaEdContacto = data.solicitude.industriaData.location.provincia.catalogValue;
	// $scope.catalogProvinciasContacto = [ {
	// catalogId : idProvinciaEdContacto,
	// catalogValue : valueProvinciaEdContacto
	// } ];
	// $scope.auxIndustria.provincia.catalogId = idProvinciaEdContacto;
	// }
	//
	// if (data.solicitude.industriaData.location.municipio !== undefined) {
	// var idMunicipioEdContacto = data.solicitude.industriaData.location.municipio.id;
	// var valueMunicipioEdContacto = data.solicitude.industriaData.location.municipio.catalogValue;
	// $scope.catalogMunicipiosContacto = [ {
	// catalogId : idMunicipioEdContacto,
	// catalogValue : valueMunicipioEdContacto
	// } ];
	// $scope.auxIndustria.municipio.catalogId = idMunicipioEdContacto;
	// }
	// }
	// };
	$this.clearPais = function(location) {
		location.pais.id = "";
		location.pais.catalogValue = "";
	};

	$this.clearProvincia = function(location) {
		location.provincia.id = "";
		location.provincia.catalogValue = "";
	};

	$this.clearMunicipio = function(location) {
		location.municipio.id = "";
		location.municipio.catalogValue = "";
		location.municipio.provincia = "";
	};

	$this.setPaisOrigen = function(auxData, pais) {
		if (auxData.paisOrigen !== null && auxData.paisOrigen !== undefined) {
			pais.id = auxData.paisOrigen.catalogId;
			pais.catalogValue = auxData.paisOrigen.catalogValue;
		} else {
			pais.id = "";
			pais.catalogValue = "";
		}
	};

	$this.setLocationData = function(auxData, location) {
		if (auxData.pais !== null && auxData.pais !== undefined) {
			var paisSolicitante = auxData.pais.catalogId;
			location.pais.id = paisSolicitante;
			location.pais.catalogValue = auxData.pais.catalogValue;
			if (paisSolicitante === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
				if (auxData.provincia !== null && auxData.provincia !== undefined && auxData.provincia.catalogId !== undefined) {
					location.provincia.id = auxData.provincia.catalogId;
					location.provincia.catalogValue = auxData.provincia.catalogValue;
				} else {
					$this.clearProvincia(location);
				}
				if (auxData.municipio !== null && auxData.municipio !== undefined && auxData.municipio.catalogId !== undefined) {
					location.municipio.id = auxData.municipio.catalogId;
						location.municipio.catalogValue = auxData.municipio.catalogValue;
						location.municipio.provincia = auxData.provincia.catalogId;
				} else {
					$this.clearMunicipio(location);
				}
			} else {
				$this.clearProvincia(location);
				$this.clearMunicipio(location);
			}
		} else {
			$this.clearPais(location);
			$this.clearProvincia(location);
			$this.clearMunicipio(location);
		}
	};

	$this.setShippingData = function(auxIndustria, location) {
		if (auxIndustria.pais !== null && auxIndustria.pais !== undefined) {
			var paisSolicitante = auxIndustria.pais.catalogId;
			location.pais.id = paisSolicitante;
			location.pais.catalogValue = auxIndustria.pais.catalogValue;
			if (paisSolicitante === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
				if (auxIndustria.provincia !== null && auxIndustria.provincia !== undefined && auxIndustria.provincia.catalogId !== undefined) {
					location.provincia.id = auxIndustria.provincia.catalogId;
					location.provincia.catalogValue = auxIndustria.provincia.catalogValue;
				} else {
					$this.clearProvincia(location);
				}
				if (auxIndustria.municipio !== null && auxIndustria.municipio !== undefined && auxIndustria.municipio.catalogId !== undefined) {
					location.municipio.id = auxIndustria.municipio.catalogId;
					location.municipio.catalogValue = auxIndustria.municipio.catalogValue;
					location.municipio.provincia = auxIndustria.provincia.catalogId;
				} else {
					$this.clearMunicipio(location);
				}
			} else {
				$this.clearProvincia(location);
				$this.clearMunicipio(location);
			}
		} else {
			$this.clearPais(location);
			$this.clearProvincia(location);
			$this.clearMunicipio(location);
		}
	};

	return {
		loadCombo : function($scope, data, combo) {
			switch (combo) {
			case $this.catalogueLocationHandler.domObjectName.paisSolicitante:
				if (data.solicitude.solicitante !== undefined && Object.keys(data.solicitude.solicitante).length > 1 && data.solicitude.solicitante.location !== undefined) {
					$this.loadSolicitante($scope, data);
				}
				break;
			case $this.catalogueLocationHandler.domObjectName.paisRepresentante:
				if (data.solicitude.representante !== undefined && Object.keys(data.solicitude.representante).length > 1) {
					$this.loadRepresentante($scope, data);
				}
				break;
			case $this.catalogueLocationHandler.domObjectName.paisContacto:
				if (data.solicitude.contactData !== undefined && Object.keys(data.solicitude.contactData).length > 1 ) {
					$this.loadContacto($scope, data);
				}
				break;
			case $this.catalogueLocationHandler.domObjectName.paisOrigen:
				if (data.solicitude.paisOrigen !== undefined) {
					$this.loadPaisOrigen($scope, data);
				}
				break;
			case $this.catalogueLocationHandler.domObjectName.paisEnvio:
				if (data.solicitude.shippingData !== undefined) {
					$this.loadShipping($scope, data);
				}
				break;
			case $this.catalogueLocationHandler.domObjectName.paisLugarOMedio:
                if (data.solicitude.lugarOMedio !== undefined && Object.keys(data.solicitude.lugarOMedio).length > 1 ) {
                    $this.loadPaisLugarOMedio($scope, data);
                }
				break;
			default:

				break;
			}
		},
		setLocationDataToScope : function setLocationDataToScope($scope) {
			$this.setLocationData($scope.auxSolicitante, $scope.formularioATratar.solicitante.location);
			if ($scope.checkboxModel.representante) {
				$this.setLocationData($scope.auxRepresentante, $scope.formularioATratar.representante.location);
			}
			if ($scope.checkboxModel.adicionales) {
				$this.setLocationData($scope.auxContacto, $scope.formularioATratar.contactData.location);
			}
			if($scope.auxLugarOMedio ){
				if($scope.auxLugarOMedio.pais) {
                    $this.setLocationData($scope.auxLugarOMedio, $scope.formularioATratar.lugarOMedio.location);
                }
			}
		},
		setPaymentLocationToScope : function($scope) {
			$this.setLocationData($scope.auxPago, $scope.formularioATratar.solicitudePayment.idPayment.location);
		},
		loadPaymentCombo : function($scope, data) {
			$this.loadPayment($scope, data);	
		},
		setCatalogueHandler : function(catalogueHandler) {
			$this.catalogueHandler = catalogueHandler;
		},
		setCatalogueLocationHandler : function(catalogueLocationHandler) {
			$this.catalogueLocationHandler = catalogueLocationHandler;
		},
		setPaisOrigenToScope : function setPaisOrigenToScope($scope) {
			$this.setPaisOrigen($scope.auxAltaPais, $scope.formularioATratar.paisOrigen);
		},
		setLocationShippingDataToScope : function setLocationDataToScope($scope) {
			$this.setShippingData($scope.auxEnvio, $scope.formularioATratar.shippingData.location);
		}
	}
});
