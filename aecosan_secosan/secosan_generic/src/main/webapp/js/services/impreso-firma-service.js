angular.module("secosan").factory("ImpresoFirmaService", function($log, $http, Messages, FileDtoDownload, AuthenticationUser, Spinner, REST_URL, HEADERS, FORM_ID_CODES, CATALOGUE_NAMES,RemovePaymentData) {

	var $this = this;

	$this.composeAddress = function(solicitante) {
		var address = '';
		address += solicitante.address + ', ';
		if (solicitante.location.pais.id == CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
			address += +solicitante.location.codigoPostal + ', ' + solicitante.location.municipio.catalogValue + ', ' + solicitante.location.provincia.catalogValue + ', '
		}

		address += solicitante.location.pais.catalogValue;

		return address;
	}

	$this.prepareReportData = function(formData, formID) {
		if (formID === FORM_ID_CODES.INCLUSION_UME) {
			return {
				id : formData.identificadorPeticion,
				formId : formData.formulario.id,
				formType : formID,
				date : new Date().getTime(),
				denomination : formData.productName,
				referenceNumber : formData.productReferenceNumber,
				company : formData.companyName,
				inscriptionDate : formData.inscriptionDate,
				signer : formData.solicitante.name,
				pdfB64 : ''
			};
		} else if (formID === FORM_ID_CODES.ALTERACION_UME) {
			return {
				id : formData.identificadorPeticion,
				formId : formData.formulario.id,
				formType : formID,
				date : new Date().getTime(),
				denomination : formData.productName,
				referenceNumber : formData.productReferenceNumber,
				company : formData.companyName,
				signer : formData.solicitante.name,
				alterMotive : formData.alterMotive,
				pdfB64 : ''
			};
		} else if (formID === FORM_ID_CODES.SUSPENSION_UME) {
			return {
				id : formData.identificadorPeticion,
				formId : formData.formulario.id,
				formType : formID,
				date : new Date().getTime(),
				company : formData.companyName,
				signer : formData.solicitante.name,
				address : $this.composeAddress(formData.solicitante),
				companyRegNumber : formData.companyRegisterNumber,
				personInCharge : formData.solicitante.name,
				phone : formData.solicitante.telephoneNumber,
				mail : formData.solicitante.email,
				comunicationDate : formData.communicationDate,
				comunicantionType : formData.cancelOrSuspension.id,
				motive : formData.suspensionReasonDuration,
				products : formData.products

			};
		} else if (formID === FORM_ID_CODES.LOGOS) {
			return {
				id : formData.identificadorPeticion,
				formType : formID,
				date : new Date().getTime(),

				nombreSolicitante : formData.solicitante.name,
				direccionSolicitante : $this.composeAddress(formData.solicitante),
				nifSolicitante : formData.solicitante.identificationNumber,
				telefonoSolicitante : formData.solicitante.telephoneNumber,
				mailSolicitante : formData.solicitante.email,
				paisSolicitante : formData.solicitante.location.pais.catalogValue,

				nombreRepresentante : formData.representante.name,
				direccionRepresentante : $this.composeAddress(formData.representante),
				nifRepresentante : formData.representante.identificationNumber,
				telefonoRepresentante : formData.representante.telephoneNumber,
				mailRepresentante : formData.representante.email,
				paisRepresentante : formData.representante.location.pais.catalogValue,

				material : formData.requestedMaterial,
				finalidad : formData.purposeAndUse

			};
		} else {
			return {
				id : formData.identificadorPeticion,
				formId : formData.formulario.id,
				formType : formID,
				date : new Date().getTime(),

				pdfB64 : ''
			};
		}
	};
    $this.getPdfReportFichaTecnica = function(idUser, datos) {
        var url = REST_URL.PDF_REPORT_ANEXOII_FICHA_TECNICA + idUser;
        RemovePaymentData.removePaymentNodeFromForm(datos);
    	return $http({
            method : 'POST',
            url : url,
            data : datos,
            headers : HEADERS.JSON
        })
    };
    
    $this.getPdfReportAnexoII = function(idUser, datos) {
        var url = REST_URL.PDF_REPORT_ANEXOII+ idUser;
        RemovePaymentData.removePaymentNodeFromForm(datos);
    	return $http({
            method : 'POST',
            url : url,
            data : datos,
            headers : HEADERS.JSON
        })
    };
	$this.getPdfReport = function(idUser, datos, formID) {
		var url = REST_URL.PDF_REPORT + idUser;
		if (formID === FORM_ID_CODES.INCLUSION_UME) {
			url = REST_URL.PDF_REPORT_ANEXOII + idUser;
		} else if (formID === FORM_ID_CODES.ALTERACION_UME) {
			url = REST_URL.PDF_REPORT_ANEXOIII + idUser;
		} else if (formID === FORM_ID_CODES.SUSPENSION_UME) {
			url = REST_URL.PDF_REPORT_ANEXOIV + idUser;
		} else if (formID === FORM_ID_CODES.LOGOS) {
			url = REST_URL.PDF_REPORT_LOGOS + idUser;
		}
		return $http({
			method : 'POST',
			url : url,
			data : datos,
			headers : HEADERS.JSON
		})

	};

	return {
        getSignaturePDF_FICHA_TECNICA : function(data) {
            Spinner.showSpinner();
            $log.debug("data:");
            $log.debug(data);
            $this.getPdfReportFichaTecnica(AuthenticationUser.getUserPayloadData("id"), data).success(function(data) {
                FileDtoDownload.decryptAndDownloadFile(data.pdfB64, data.reportName);
                $log.debug("getPdfReportFichaTecnica success");
                Spinner.hideSpinner();

            }).error(function(data, status) {
                $log.error("getPdfReportFichaTecnica error");
                Messages.setErrorMessageFromResponse(data, status);
                Spinner.hideSpinner();
            });
        },
         getSignaturePDFAnexoII : function(data) {
            Spinner.showSpinner();
            $log.debug("data:");
            $log.debug(data);
            $this.getPdfReportAnexoII(AuthenticationUser.getUserPayloadData("id"), data).success(function(data) {
                FileDtoDownload.decryptAndDownloadFile(data.pdfB64, data.reportName);
                $log.debug("getPdfReportAnexoII success");
                Spinner.hideSpinner();

            }).error(function(data, status) {
                $log.error("getPdfReportAnexoII error");
                Messages.setErrorMessageFromResponse(data, status);
                Spinner.hideSpinner();
            });
        },
		getSignaturePDF : function(formData, formID) {
			Spinner.showSpinner();
			var data = $this.prepareReportData(formData, formID);
			$log.debug("data:");
			$log.debug(data);
			$this.getPdfReport(AuthenticationUser.getUserPayloadData("id"), data, formID).success(function(data) {
				FileDtoDownload.decryptAndDownloadFile(data.pdfB64, data.reportName);
				$log.debug("getPdfReport success");
				Spinner.hideSpinner();

			}).error(function(data, status) {
				$log.error("getPdfReport error");
				Messages.setErrorMessageFromResponse(data, status);
				Spinner.hideSpinner();
			});
		}
	}
});