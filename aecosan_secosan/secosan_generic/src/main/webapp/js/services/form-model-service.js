angular.module("secosanGeneric").factory("FormModelService", function(SOLICITUDE_CODES, FORM_ID_CODES) {
	return {
		getSolicitudQuejaModel : function() {
			return {// SolicitudQuejaDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.QUEJA,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.QUEJA
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : "1"
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : "1"
					},
					"personType" : { // TipoPersonaDTO
						"id" : "1"
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"unity" : "",
				"dateTimeIncidence" : "",
				"motive" : "",
				"comunicationType" : {
					"id" : "1"
				},
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getSugerenciaModel : function() {
			return {// SolicitudSugerenciaDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.SUGERENCIA,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.SUGERENCIA
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : "1"
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : "1"
					},
					"personType" : { // TipoPersonaDTO
						"id" : "1"
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"unity" : "",
				"dateTimeIncidence" : "",
				"motive" : "",
				"comunicationType" : {
					"id" : "1"
				},
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getAlimentosGruposCeseModel : function() {
			return {// CeseComercializacionAlimentosGruposDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.ALIM_GRUPOS_CESE,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.GE_CESE
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				// "comercialProductName" : "",
				"nombreComercialProducto" : "",
				"observaciones" : "",
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getAlimentosGruposModificacionModel : function() {
			return {// ModificacionDatosAlimentosGruposDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.ALIM_GRUPOS_MODIFICACION,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.GE_MODIFICACION
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				// "comercialProductName" : "",
				"nombreComercialProducto" : "",
				"rgseaa" : "",
				"nombreRazonSocial" : "",
				"nif" : "",
				"nuevoNombreComercialProducto" : "",
				"modComposicionCualCuan" : false,
				"modDiseno" : false,
				"modAmplPresentaciones" : false,
				"otrasModificaciones" : false,
				"descripcionModificaciones" : "",
				"status" : "",
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getAlimentosGruposPuestaMercadoModel : function() {
			return {// PuestaMercadoAlimentosGruposDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.ALIM_GRUPOS_PUESTA_MERCADO,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.GE_PUESTA_MERCADO
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				// Puesta en el Mercado
				"productType" : {
					"id" : ""
				},
				"presentaciones" : [],
				"nombreComercialProducto" : "",
				"ingredientes" : "",
				"datosIdentificativosAdicionales" : {
					"isProductMaker" : "",
					"rgseaa" : "",
					"solicitudeInclude" : ""
				},
				"antecedentesProcedencia" : {
					"primeraComercializacionUE" : "",
					"idPaisProcedencia" : "",
					"idPaisComercializacionPrevia" : ""
				},"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				// FIN Puesta en el Mercado
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getComplementosAlimenticiosCeseModel : function() {
			return {// CeseComercializacionDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.COMP_ALIM_CESE,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.CA_CESE
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				
				// "comercialProductName" : "",
				"nombreComercialProducto" : "",
				"observaciones" : "",
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getComplementosAlimenticiosModificacionModel : function() {
			return {// ModificacionDatosDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.COMP_ALIM_MODIFICACION,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.CA_MODIFICACION
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				// "comercialProductName" : "",
				"nombreComercialProducto" : "",
				"rgseaa" : "",
				"nombreRazonSocial" : "",
				"nif" : "",
				"nuevoNombreComercialProducto" : "",
				"modComposicionCualCuan" : false,
				"modDiseno" : false,
				"modAmplPresentaciones" : false,
				"otrasModificaciones" : false,
				"descripcionModificaciones" : "",
				"status" : "",
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getComplementosAlimenticiosPuestaMercadoModel : function() {
			return {// PuestaMercadoDTO
				"fechaCreacion" : "",
				"identificadorPeticion" : "",
				"status" : "",
				"formulario" : {
					"id" : SOLICITUDE_CODES.COMP_ALIM_PUESTA_MERCADO,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.CA_PUESTA_MERCADO
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				},
				// Puesta en el Mercado
				"presentaciones" : [],
				"nombreComercialProducto" : "",
				"ingredientes" : {
					"situacion" : {
						"id" : "",
						"catalogValue" : ""
					},
					"incluyeVitaminas" : "",
					"incluyeNuevosIngredientes" : "",
					"nuevoIngrediente" : "",
					"otrasSustancias" : ""
				},
				"datosIdentificativosAdicionales" : {
					"isProductMaker" : "",
					"rgseaa" : ""
				},
				"antecedentesProcedencia" : {
					"primeraComercializacionUE" : "",
					"idPaisProcedencia" : "",
					"idPaisComercializacionPrevia" : ""
				},
				// FIN Puesta en el Mercado
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getEvaluacionriesgosModel : function() {
			return {// EvaluacionRiesgosDTO
				"fechaCreacion" : "",
				"identificadorPeticion" : "",
				"formulario" : {
					"id" : SOLICITUDE_CODES.EVALUACION_RIESGOS,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.RIESGOS
				},
				"solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : "1"
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : "1"
					},
					"personType" : { // TipoPersonaDTO
						"id" : "1"
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"status" : "",
				"commercialProductName" : "",
				"solicitudeSubject" : "",
				"payTypeService" : {
					"idPayType" : "",
					"idService" : ""
				},
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
		getFinanciacionAlimentosAlteracionModel : function() {
			return {// AlterOferAlimUMEDTO
				"fechaCreacion" : "",
				"alterMotive" : "",
				"identificadorPeticion" : "",
				"status" : "",
				"formulario" : {
					"id" : SOLICITUDE_CODES.FINA_ALIM_ALTERACION,
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.ALTERACION_UME
				},
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"companyRegisterNumber" : "",
				"companyName" : "",
				"productName" : "",
				"productReferenceNumber" : "",
				"age" : "",
				"caloricDensity" : "",
				"osmolarity" : "",
				"osmolality" : "",
				"molecularWeight" : "",
				"instruction" : "",
				"groupOfPatient" : {
					"breastfed" : false,
					"child" : false,
					"adult" : false
				},
				"physicalState" : {
					"dust" : false,
					"liquid" : false,
					"standardSolution" : ""
				},
				"caloricDistribution" : {
					"protein" : "",
					"lipids" : "",
					"carbohydrates" : ""
				},
				"fiber" : {
					"fiber" : "",
					"fiberType" : "",
					"fiberQuantity" : ""
				},

				"proposal" : {
					"typeCode" : "",
					"subtypeCode" : ""
				},

				"administrationMode" : {
					"id" : ""
				},
				"presentations" : [],
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
		getFinanciacionAlimentosInclusionModel : function() {
			return {// IncOferAlimUMEDTO
				"fechaCreacion" : "",
				"identificadorPeticion" : "",
				"status" : "",
				"formulario" : {
					"id" : SOLICITUDE_CODES.FINA_ALIM_INCLUSION,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.INCLUSION_UME
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"companyRegisterNumber" : "",
				"companyName" : "",
				"inscriptionDate" : "",
				"productName" : "",
				"productReferenceNumber" : "",
				"age" : "",
				"caloricDensity" : "",
				"osmolarity" : "",
				"osmolality" : "",
				"molecularWeight" : "",
				"instruction" : "",
				"groupOfPatient" : {
					"breastfed" : false,
					"child" : false,
					"adult" : false
				},
				"physicalState" : {
					"dust" : false,
					"liquid" : false,
					"standardSolution" : ""
				},
				"caloricDistribution" : {
					"protein" : "",
					"lipids" : "",
					"carbohydrates" : ""
				},
				"fiber" : {
					"fiber" : "",
					"fiberType" : "",
					"fiberQuantity" : ""
				},

				"proposal" : {
					"typeCode" : "",
					"subtypeCode" : ""
				},

				"administrationMode" : {
					"id" : ""
				},
				"presentations" : [],
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
		getFinanciacionAlimentosSuspensionModel : function() {
			return {// BajaOferAlimUMEDTO
				"fechaCreacion" : "",
				"identificadorPeticion" : "",
				"status" : "",
				"formulario" : {
					"id" : SOLICITUDE_CODES.FINA_ALIM_SUSPENSION,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.SUSPENSION_UME
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"companyRegisterNumber" : "",
				"companyName" : "",
				"communicationDate" : "",
				"suspensionReasonDuration" : "",
				"cancelOrSuspension" : {
					"id" : ""
				},
				"products" : [],
				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
		getLmrFitosanitariosModel : function() {
			return {// SolicitudLmrFitosanitariosDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.LMR_FITOSANITARIOS,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.LMR
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"activeSubstance" : "",
				"originCountry" : "",
				"foods" : "",
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
		getSolicitudLogosModel : function() {
			return {// SolicitudLogoDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.SOLICITUD_LOGOS,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.LOGOS
				},
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : "1"
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : "1"
					},
					"personType" : { // TipoPersonaDTO
						"id" : "1"
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"requestedMaterial" : "",
				"purposeAndUse" : "",

				"documentacion" : [ // DocumentacionDTO
				]
			};
		},
 		getRegistroAguasModel : function() {
			return {// ModificacionDatosDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.REGISTRO_AGUAS,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.REGISTRO_AGUAS
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"commercialProductName" : "",
				"rgseaa" : "",
				"nombreManantial" : "",
				"lugarExplotacion" : "",
				"nombreAguaPaisOrigen" : "",
				"paisOrigen" : {
					"id" : "",
					"catalogValue" : ""
				},
				"tratamientoAgua" : "",
				"datosOficiales" : "",
				"status" : "",
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
		getRegistroIndustriasModel : function() {
			return {// ModificacionDatosDTO
				"formulario" : {
					"id" : SOLICITUDE_CODES.REGISTRO_INDUSTRIAS,
				},
				"formularioEspecifico" : {
					"id" : FORM_ID_CODES.REGISTRO_INDUSTRIAS
				},
                "solicitaCopiaAutentica": 0,
                "solicitaCopiaAutenticaInfo":"",
				"identificadorPeticion" : "",
				"solicitante" : { // UsuarioSolicitanteDTO
					"name" : "",
					"identificationNumber" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					},
					"telephoneNumber" : "",
					"email" : "",
					"role" : { // UsuarioRolDTO
						"id" : ""
					},
					"tipo" : { // TipoUsuarioDTO
						"id" : ""
					},
					"personType" : { // TipoPersonaDTO
						"id" : ""
					}
				},
				"representante" : { // RepresentanteDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"identificationNumber" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"contactData" : { // DatosContactoDTO -->
					// AbstractPersonDataDTO
					"name" : "",
					"telephoneNumber" : "",
					"email" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"paisOrigen" : {
					"id" : "",
					"catalogValue" : ""
				},
				"rgseaa" : "",
				"tipoEnvio" : {
					"id" : "",
					"catalogValue" : ""
				},
				"shippingData" : {
					"name" : "",
					"address" : "",
					"location" : { // UbicacionGeograficaDTO
						"codigoPostal" : "",
						"pais" : {
							"id" : "",
							"catalogValue" : ""
						},
						"provincia" : {
							"id" : "",
							"catalogValue" : ""
						},
						"municipio" : {
							"id" : "",
							"catalogValue" : "",
							"provincia" : ""
						}
					}
				},
				"status" : "",
				"documentacion" : [ // DocumentacionDTO
				],
				"solicitudePayment" : {
					"rateAmount" : "",
					"amount" : "",
					"payment" : {
						"id" : ""
					},
					"idPayment" : {
						"name" : "",
						"identificationNumber" : "",
						"address" : "",
						"location" : {
							"codigoPostal" : "",
							"pais" : {
								"id" : "",
								"catalogValue" : ""
							},
							"provincia" : {
								"id" : "",
								"catalogValue" : ""
							},
							"municipio" : {
								"id" : "",
								"catalogValue" : "",
								"provincia" : ""
							}
						}
					},
					"dataPayment" : {
						"countryCode" : "",
						"controlDigit" : "",
						"entity" : "",
						"office" : "",
						"dc" : "",
						"nrc" : "",
						"account" : "",
						"cardNumber" : "",
						"expirationMonth" : "",
						"expirationYear" : ""
					}
				}
			};
		},
        getRegistroReacuModel : function() {
            return {// RegistroReacuDTO
                "fechaCreacion" : "",
                "identificadorPeticion" : "",
                "formulario" : {
                    "id" : SOLICITUDE_CODES.REGISTRO_REACU,
                },

                "lugarOMedio":{
                	"address":"",
                    "fax":"",
                    "email":"",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal": "",
                        "pais": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "provincia": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "municipio": {
                            "id": "",
                            "catalogValue": "",
                            "provincia": ""
                        }
                    }
				},
				"formularioEspecifico" : {
                    "id" : FORM_ID_CODES.REGISTRO_REACU,
                },

                "solicitante" : { // UsuarioSolicitanteDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    },
                    "telephoneNumber" : "",
                    "email" : "",
                    "role" : { // UsuarioRolDTO
                        "id" : "1"
                    },
                    "tipo" : { // TipoUsuarioDTO
                        "id" : "1"
                    },
                    "personType" : { // TipoPersonaDTO
                        "id" : "1"
                    }
                },
                "representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },
                "contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
                    "name" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },

                "documentacion" : [ // DocumentacionDTO
                ],
            };
        },
        getBecasModel: function() {
            return {//BecasDTO
                "fechaCreacion" : "",
                "identificadorPeticion" : "",
                "formulario" : {
                    "id" : SOLICITUDE_CODES.BECAS,
                },
                "formularioEspecifico" : {
                    "id" : FORM_ID_CODES.BECAS,
                },

                "solicitante" : { // UsuarioSolicitanteDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "address" : "",
					"fechaNac" : null,
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    },
                    "telephoneNumber" : "",
                    "email" : "",
                    "role" : { // UsuarioRolDTO
                        "id" : "1"
                    },
                    "tipo" : { // TipoUsuarioDTO
                        "id" : "1"
                    },
                    "personType" : { // TipoPersonaDTO
                        "id" : "1"
                    }
                },
                "representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },
                "contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
                    "name" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },

                "documentacion" : [ // DocumentacionDTO
                ],
                "otraTitulacion": "",
                "otraTitulacionPuntos":0,
                "puntosEntrevista":0,
                "puntosExpediente": 0,
                "puntosOfimatica": 0,
				"puntosVoluntariado":0,


            };
        },
        getSubvencionesAsocuaeModel: function() {
            return {//SubvencionesAsocuaeDTO
                "fechaCreacion" : "",
                "identificadorPeticion" : "",
                "formulario" : {
                    "id" : SOLICITUDE_CODES.SUBVENCIONES_ASOCUAE,
                },
                "lugarOMedio":{
                    "address":"",
                    "fax":"",
                    "email":"",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal": "",
                        "pais": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "provincia": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "municipio": {
                            "id": "",
                            "catalogValue": "",
                            "provincia": ""
                        }
                    }
                },
                "formularioEspecifico" : {
                    "id" : FORM_ID_CODES.SUBVENCIONES_ASOCUAE,
                },

                "solicitante" : { // UsuarioSolicitanteDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    },
                    "telephoneNumber" : "",
                    "email" : "",
                    "role" : { // UsuarioRolDTO
                        "id" : "1"
                    },
                    "tipo" : { // TipoUsuarioDTO
                        "id" : "1"
                    },
                    "personType" : { // TipoPersonaDTO
                        "id" : "1"
                    }
                },
                "representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },
                "contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
                    "name" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },

                "documentacion" : [ // DocumentacionDTO
                ],
            };
        },
        getSubvencionesJuntasModel : function() {
            return {//SubvencionesJuntasDTO
                "fechaCreacion" : "",
                "identificadorPeticion" : "",
                "formulario" : {
                    "id" : SOLICITUDE_CODES.SUBVENCIONES_JUNTAS,
                },
                "lugarOMedio":{
                    "address":"",
                    "fax":"",
                    "email":"",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal": "",
                        "pais": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "provincia": {
                            "id": "",
                            "catalogValue": ""
                        },
                        "municipio": {
                            "id": "",
                            "catalogValue": "",
                            "provincia": ""
                        }
                    }
                },
                "formularioEspecifico" : {
                    "id" : FORM_ID_CODES.SUBVENCIONES_JUNTAS,
                },

                "solicitante" : { // UsuarioSolicitanteDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    },
                    "telephoneNumber" : "",
                    "email" : "",
                    "role" : { // UsuarioRolDTO
                        "id" : "1"
                    },
                    "tipo" : { // TipoUsuarioDTO
                        "id" : "1"
                    },
                    "personType" : { // TipoPersonaDTO
                        "id" : "1"
                    }
                },
                "representante" : { // RepresentanteDTO --> // AbstractPersonDataDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },
                "contactData" : { // DatosContactoDTO -->// AbstractPersonDataDTO
                    "name" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },

                "documentacion" : [ // DocumentacionDTO
                ],
            };
        },
        getProcedimientoGeneralModel : function() {
            return {// ProcedimientoGeneralDTO
                "fechaCreacion" : "",
                "identificadorPeticion" : "",
                "formulario" : {
                    "id" : SOLICITUDE_CODES.PROCEDIMIENTO_GENERAL,
                },
                "status" : "",
                "asunto" : "",
                "expone" : "",
                "solicita" : "",
                "formularioEspecifico" : {
                    "id" : FORM_ID_CODES.PROCEDIMIENTO_GENERAL,
                },
                "solicitante" : { // UsuarioSolicitanteDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    },
                    "telephoneNumber" : "",
                    "email" : "",
                    "role" : { // UsuarioRolDTO
                        "id" : "1"
                    },
                    "tipo" : { // TipoUsuarioDTO
                        "id" : "1"
                    },
                    "personType" : { // TipoPersonaDTO
                        "id" : "1"
                    }
                },
                "representante" : { // RepresentanteDTO -->
                    // AbstractPersonDataDTO
                    "name" : "",
                    "identificationNumber" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },
                "contactData" : { // DatosContactoDTO -->
                    // AbstractPersonDataDTO
                    "name" : "",
                    "telephoneNumber" : "",
                    "email" : "",
                    "address" : "",
                    "location" : { // UbicacionGeograficaDTO
                        "codigoPostal" : "",
                        "pais" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "provincia" : {
                            "id" : "",
                            "catalogValue" : ""
                        },
                        "municipio" : {
                            "id" : "",
                            "catalogValue" : "",
                            "provincia" : ""
                        }
                    }
                },

                "documentacion" : [ // DocumentacionDTO
                ],
            };
        },
		getUserNoUERegisterModel : function() {
			return {
				"login" : "",
				"nombre" : "",
				"apellido" : "",
				"password" : "",
				"email" : "",
				"identificationNumber" : ""
			}
		},
		getUserLoginModel : function() {
			return {
				"login" : "",
				"password" : ""
			}
		}
	}
});