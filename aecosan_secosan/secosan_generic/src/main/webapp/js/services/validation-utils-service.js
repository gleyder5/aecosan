angular.module("secosanGeneric").factory(
		"ValidationUtilsService",
		function(ModalService, ModalParamsService, UtilService, CATALOGUE_NAMES, DOCUMENT_TYPE, ERR_MSG,SENDER_MODE,$log) {

			var $this = this;
			var validationListeners = [];
			var currentValidations = [];

			$this.checkValidDataSolicitante = function(nif, email, phone, postalCode, countryCode,section,$scope) {
				var valid = true;
				var errorMessage = "";

				if (!UtilService.checkNotNullNorUndefined(nif)) {
					errorMessage = ERR_MSG.NIF_INVALID_SOLICITANTE;
					valid = false;
				} else if (countryCode === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE && !UtilService.checkNotNullNorUndefined(postalCode)) {
					errorMessage = ERR_MSG.POSTAL_CODE_INVALID_SOLICITANTE;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(phone)) {
					errorMessage = ERR_MSG.PHONE_INVALID_SOLICITANTE;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(email)) {
					errorMessage = ERR_MSG.EMAIL_INVALID_SOLICITANTE;
					valid = false;
				}

				if (!valid) {
					$this.addValidMessage(errorMessage,section,$scope);
					//ModalService.showModalWindow(ModalParamsService.genericErrorMessage(errorMessage));
				}

				return valid;
			};

			$this.checkValidDataRepresentante = function(nif, email, phone, postalCode, countryCode,section,$scope) {
				var valid = true;
				var errorMessage = "";

				if (!UtilService.checkNotNullNorUndefined(nif)) {
					errorMessage = ERR_MSG.NIF_INVALID_REPRESENTANTE;
					valid = false;
				} else if (countryCode === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE && !UtilService.checkNotNullNorUndefined(postalCode)) {
					errorMessage = ERR_MSG.POSTAL_CODE_INVALID_REPRESENTANTE;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(phone)) {
					errorMessage = ERR_MSG.PHONE_INVALID_REPRESENTANTE;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(email)) {
					errorMessage = ERR_MSG.EMAIL_INVALID_REPRESENTANTE;
					valid = false;
				}

				if (!valid) {
					$this.addValidMessage(errorMessage,section,$scope);
					//ModalService.showModalWindow(ModalParamsService.genericErrorMessage(errorMessage));
				}

				return valid;
			};

			$this.checkValidDataDatosAdicionales = function(email, phone, postalCode, countryCode,section,$scope) {
				var valid = true;
				var errorMessage = "";

				if (countryCode === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE && !UtilService.checkNotNullNorUndefined(postalCode)) {
					errorMessage = ERR_MSG.POSTAL_CODE_INVALID_ADICIONALES;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(phone)) {
					errorMessage = ERR_MSG.PHONE_INVALID_ADICIONALES;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined(email)) {
					errorMessage = ERR_MSG.EMAIL_INVALID_ADICIONALES;
					valid = false;
				}

				if (!valid) {
					$this.addValidMessage(errorMessage,section,$scope);
					//ModalService.showModalWindow(ModalParamsService.genericErrorMessage(errorMessage));
				}
				return valid;
			};

			$this.checkDocumentAttached = function(documentArray, documentType) {
				var documentAttached = false;
				for (var i = 0; i < documentArray.length; i++) {
					if (documentArray[i].docType == documentType) {
						documentAttached = true;
					}
				}
				return documentAttached;
			};

			$this.checkValidDataIndustria = function(postalCode, countryCode,section,$scope) {
				var valid = true;
				var errorMessage = "";

				if (countryCode === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE && !UtilService.checkNotNullNorUndefined(postalCode)) {
					errorMessage = ERR_MSG.POSTAL_CODE_INVALID_INDUSTRIA;
					valid = false;
				}
				if (!valid) {
					$this.addValidMessage(errorMessage,section,$scope);
					//ModalService.showModalWindow(ModalParamsService.genericErrorMessage(errorMessage));
				}
				return valid;
			};

			$this.checkFormValid = function($scope,isSubmitMode){

				if(isSubmitMode == true && $scope.formularioATratar.status == SENDER_MODE.DRAFT){
					return true;
				}

				var isValid = true;
				for (var formIdentifier in validationListeners) {

					var formElement = validationListeners[formIdentifier].formDomElement;
					var formCtrl = validationListeners[formIdentifier].formCtrl;

					var resultado = $this.validateSection(formElement, formCtrl, $scope );
					var tab = $scope.tabCtrl.getTabByID($scope.tabCtrl, formIdentifier);
					tab.hasError = resultado.hasError;
					if(resultado.hasError){ isValid = false}
					//currentValidations[formIdentifier] = resultado.validaciones;
					currentValidations[formIdentifier]=	$.extend(currentValidations[formIdentifier],resultado.validaciones )
					validationListeners[formIdentifier].validation(currentValidations[formIdentifier])
				}


	            return isValid;
			};

			$this.validateSection = function(formElement, formCtrl, $scope ){
				var validaciones = {};
	        	var flag = false;
	        	if(formElement!=undefined) {
                    var inputEl = formElement.querySelectorAll("[name]");

                    var hiddenContainerList = [];
                    angular.forEach(formElement.querySelectorAll("[ng-show]"), function (hiddenContainer) {
                        var model = $(hiddenContainer).attr("ng-show");
                        if (UtilService.checkNotNullNorUndefined($scope.$eval(model)) && !$scope.$eval(model)) {//TODO verificar que el model con puntes funcione
                            hiddenContainerList.push(hiddenContainer);
                        }
                    });
                    angular.forEach(formCtrl, function (obj) {
                        if (angular.isObject(obj) && angular.isDefined(obj.$setDirty)) {
                            obj.$setDirty();
                            if (angular.isDefined(obj.$setTouched)) {
                                obj.$setTouched();

                            }
                        }
                    })

                    angular.forEach(inputEl, function (element) {
                        var aElment = angular.element(element);
                        var inputName = aElment.attr("name");
                        var found = false;
                        hiddenContainerList.forEach(function (hiddencont) {
                            if ($(hiddencont).find("[name=" + inputName + "]").length >= 1) {
                                found = true;
                            }
                        })

                        //no se valida, ya que se encuentra dentro de un elemento con ng-show escondido
                        if (found) {
                            return
                        };

                        var id = aElment.attr('id');
                        var labele = formElement.querySelector("[for='" + id + "']");
                        if (labele != null) {
                            labele = labele.innerHTML
                        } else if (aElment.data("label") != null) {
                            labele = aElment.data("label");
                        } else {
                            labele = aElment.attr("name");
                        }

                        //if($('#elementId').is(':visible'))

                        var inputInfo = {label: labele}
                        var errores = [];
                        if (formCtrl[inputName] != undefined) {
                            if (formCtrl[inputName].$error) {
                                angular.forEach(formCtrl[inputName].$error, function (value, key) {
                                    errores.push(ERR_MSG[key])
                                });
                            }
                        }
                        if (errores.length > 0) {
                            flag = true
                        };
                        inputInfo.errors = errores;
                        validaciones[inputName] = inputInfo;

                    });
                }
	        	return {hasError : flag, validaciones : validaciones};
			}

			$this.addValidMessage = function(message, section,$scope,customName){
				if(!UtilService.checkNotNullNorUndefined(validationListeners[section])){
					$log.debug("no se encontro validador");
					return;
				}
				if(UtilService.checkNotNullNorUndefined(currentValidations[section])){
					currentValidations[section][customName] = {errors : [message]};
				}else{
					var errors = [];
					errors.push(message);
					currentValidations[section] = {};
					currentValidations[section][customName] = {errors : errors};
				}
				var tab = $scope.tabCtrl.getTabByID($scope.tabCtrl, section);
				tab.hasError = true;
				validationListeners[section].validation(currentValidations[section]);
			}

            $this.register = function (key,listener,scope){
                // scope.$emit('validation-listener-registered',{'set' :true,'key':scope.formIdentifier});
                listener.validation = function(validaciones) {
                    scope.inputs = validaciones;
                    var flagHasError = false;
                    angular.forEach(validaciones, function(item){
                        if(item.errors.length > 0){
                            flagHasError = true;
                        }
                    });
                    scope.hasError = flagHasError;
                    if(flagHasError){
                        scope.$emit('check-resume-validity',{'set' :false,'key':scope.formIdentifier});
                    }else{
                        scope.$emit('check-resume-validity',{'set' :true,'key':scope.formIdentifier});
                    }
                };
                // callBack(scope,key);
                validationListeners[key] = listener;
            };

		    $this.unregister = function (key) {
				validationListeners[key] = null;
				delete  validationListeners[key];
		    };
		    
		    $this.clearCurrent = function (){
		    	currentValidations = [];
		    };

		    $this.validationListeners = validationListeners;
            $this.currentValidations = currentValidations;
		    
			return {
				isSignedDocumentAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SIGNED_DOCUMENT);
				},
				isLmrSolicitudeAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.LMR_SOLICITUDE) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SIGNED_DOCUMENT);
				},
				isSignedPaymentAndLabelDocumentAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SIGNED_DOCUMENT) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.PAGO_TASAS_RECEIPT)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.LABEL);
				},
                isCollegeDegreeDocumentAttached : function(documentArray) {
                    return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.COLLEGE_DEGREE);
                },
                isCVDocumentAttached : function(documentArray) {
                    return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.CV);
                },
                isDNIDocumentAttached : function(documentArray) {
                    return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.DNI);
                },

                isAnexoIIDocumentsAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ANEXOII) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TAX_PAGO_TASAS_RECEIPT)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.QUALITY_ABD_QUALITATIVE_COMPOSITION)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TECHNICAL_DATA_FORM) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TAX_PAGO_TASAS_RECEIPT)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ORIGINAL_LABEL) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SKETCH);
				},
				isAnexoIIIDocumentsAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ANEXOIII) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TAX_PAGO_TASAS_RECEIPT)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TECHNICAL_DATA_FORM)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.TAX_PAGO_TASAS_RECEIPT) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ORIGINAL_LABEL)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SKETCH);
				},
				isAnexoIVDocumentsAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ANEXOIV);
				},
				isPersonalDataCorrect : function($scope,section) {
					var valid = $this.checkValidDataSolicitante($scope.formularioATratar.solicitante.identificationNumber, $scope.formularioATratar.solicitante.email,
							$scope.formularioATratar.solicitante.telephoneNumber, $scope.formularioATratar.solicitante.location.codigoPostal,
							($scope.auxSolicitante.pais !== undefined ? $scope.auxSolicitante.pais.catalogId : ""),section,$scope);

					if (valid && $scope.checkboxModel.representante && $scope.formularioATratar.status !== SENDER_MODE.DRAFT) {
						valid = $this.checkValidDataRepresentante($scope.formularioATratar.representante.identificationNumber, $scope.formularioATratar.representante.email,
								$scope.formularioATratar.representante.telephoneNumber, $scope.formularioATratar.representante.location.codigoPostal,
								($scope.auxRepresentante.pais.catalogId !== undefined ? $scope.auxRepresentante.pais.catalogId : ""),section,$scope);
					}

					if (valid && $scope.checkboxModel.adicionales && $scope.formularioATratar.status !== SENDER_MODE.DRAFT) {
						valid = $this.checkValidDataDatosAdicionales($scope.formularioATratar.contactData.email, $scope.formularioATratar.contactData.telephoneNumber,
								$scope.formularioATratar.contactData.location.codigoPostal, ($scope.auxContacto.pais !== undefined ? $scope.auxContacto.pais.catalogId : ""),section,$scope);
					}

					return valid;
				},
				isIndustryDataCorrect : function($scope,section) {
					var valid = $this.checkValidDataIndustria($scope.formularioATratar.shippingData.location.codigoPostal, ($scope.auxEnvio.pais !== undefined ? $scope.auxEnvio.pais.catalogId : ""),section,$scope);
					return valid;
				},
				isSignedPaymentAndOriginalCountryDocumentAttached : function(documentArray) {
					return $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.SIGNED_DOCUMENT) && $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.PAGO_TASAS_RECEIPT)
							&& $this.checkDocumentAttached(documentArray, DOCUMENT_TYPE.ORIGINAL_COUNTRY_DOCUMENT);
				},
				isPaid : function ($scope) {
                    var valid = $scope.isPaid;
                    return valid;
                },
				register : $this.register,
				unregister : $this.unregister,
				checkFormValid : $this.checkFormValid,
				addValidMessage : $this.addValidMessage,
				clearCurrent : $this.clearCurrent,
                validationListeners : $this.validationListeners,
                currentValidations: $this.currentValidations,
			}});