angular.module("secosanGeneric").factory("CatalogueHandler", function($log, $http, $translate, Messages, GENERIC_REST_PATHS,EPAGO_BANKS,PAYMENT_TYPE) {
	var $this = this;
	this.baseUrl = GENERIC_REST_PATHS.CATALOG_REST_PATH;
	var defaultErrorMessage = "CATALOGUE_DEFAULT_ERROR_MESSAGE";

	this.executeRemoteCatalogCall = function(params) {
		var url = $this.createUrl(params);
		return $http({
			method : 'GET',
			url : url
		});
	}

	this.createUrl = function(params) {
		var catalogueName = params.catalogueName;
		var url = $this.baseUrl + catalogueName;

		if (params.relatedParams !== undefined && !angular.equals(params.relatedParams, {})) {

			url += "/" + params.relatedParams.parentCatalogue + "/" + params.relatedParams.parentId;

		} else if (params.tag !== undefined && !angular.equals(params.tag, "")) {
			url += "/" + params.tag;
		}

		return url;
	}

	this.createCatalogueElement = function(id, value) {
		return {
			"id" : id,
			"title" : value
		};
	}

	return {
		getCatalogue : function(catalogueName) {
			var params = {
				"catalogueName" : catalogueName
			};
			return $this.executeRemoteCatalogCall(params);
		},

		getRelatedCatalogue : function(catalogueName, parentCatalogue, parentId) {
			$log.debug(parentId);
			var params = {
				"catalogueName" : catalogueName,
				"relatedParams" : {
					"parentCatalogue" : parentCatalogue,
					"parentId" : parentId
				}

			};

			return $this.executeRemoteCatalogCall(params);
		},
		getTaggedCatalogue : function(catalogueName, tag) {
			var params = {
				"catalogueName" : catalogueName,
				"tag" : tag
			};
			return $this.executeRemoteCatalogCall(params);
		},

		generateCatalogueDataForShow : function(rawData) {
			var catalogueData = [];
			var rawArrayElement = rawData.catalog;
			if (rawArrayElement !== undefined && rawArrayElement.length > 0) {
				angular.forEach(rawArrayElement, function(value) {
					var catalogId = value.catalogId;
					if(angular.equals(catalogId,undefined)){
						 catalogId = value.id;
					}
					var element = $this.createCatalogueElement(catalogId, value.catalogValue);
					catalogueData.push(element);
				});
			}
			$log.debug("catalogueData");
			$log.debug(catalogueData);
			return catalogueData;
		},
		generateDefaultErrorMessage : function() {
			var message = $translate.instant(defaultErrorMessage);
			Messages.setMessageAndType(message);
			
		},
		setRemoteBaseUrl:function(remoteBaseUrl){
			$this.baseUrl = remoteBaseUrl;
		},
		
		getBanksListForOnlinePayment:function(paymentType){
		
			if(paymentType ==PAYMENT_TYPE.ONLINE_TARJETA){
				return EPAGO_BANKS.CREDITCARD;
			}else if(paymentType ==PAYMENT_TYPE.ONLINE_CUENTA){
				$log.debug("banks for account");
				return EPAGO_BANKS.ACCOUNT;
			}else{
				return [];
			}
		}

	}

});