angular.module("secosanGeneric").factory("IdentificadorPeticion", function($http, $log, REST_URL) {
	return {
		getIdentificador : function(idArea) {
			var url = REST_URL.IDENTIFICADOR + idArea;
			return $http({
				method : 'GET',
				url : url
			})
		}
	}
});