angular.module("secosanGeneric").factory("ModalService", function($log, $translate) {
	var modalArgs = {};

	return {
		// setters
		setTitle : function(title) {
			modalArgs['title'] = title;
		},
		setDescription : function(Description) {
			modalArgs['description'] = Description;
		},
		setType : function(type) {
			modalArgs['type'] = type;
		},
		setVisible : function(visible) {
			modalArgs['visible'] = visible;
		},
		setOkFunction : function(okFunction) {
			modalArgs['okFunction'] = okFunction;
		},
		setOkButton : function(okButton) {
			modalArgs['okButton'] = okButton;
		},
		setCancelButton : function(cancelButton) {
			modalArgs['cancelButton'] = cancelButton;
		},
		setOkButtonStyle : function(okButtonStyle) {
			modalArgs['okButtonStyle'] = okButtonStyle;
		},
		setCancelButtonStyle : function(cancelButtonStyle) {
			modalArgs['cancelButtonStyle'] = cancelButtonStyle;
		},

		// getters
		getModalArgs : function() {
			return modalArgs;
		},

		// methods
		clearModalArgs : function() {
			modalArgs = {};
		},

		showModalWindow : function(modalParams) {
			this.clearModalArgs();
			this.setTitle(modalParams.title);
			this.setDescription(modalParams.description);
			if (modalParams.type !== undefined) {
				this.setType(modalParams.type);
			} else {
				this.setType("exclamation");
			}

			if (modalParams.okButton !== undefined) {
				this.setOkButton(modalParams.okButton);
			} else {
				this.setOkButton($translate.instant("ACCEPT"));
			}

			if (modalParams.cancelButton !== undefined) {
				this.setCancelButton(modalParams.cancelButton);
			} else {
				this.setCancelButton($translate.instant("CANCEL"));
			}

			if (modalParams.okFunction !== undefined) {
				this.setOkFunction(modalParams.okFunction);
			}
			this.setOkButtonStyle(modalParams.okButtonStyle);
			this.setCancelButtonStyle(modalParams.cancelButtonStyle);
			this.setVisible(true);
		}

	}

});