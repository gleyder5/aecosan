angular.module("secosanGeneric").factory(
		"DatePickerPopup",
		function($log) {
			return {
				datePickerConfig:function($scope,$modal,$filter,size, dateValues, params, paramName) {
					$log.debug(paramName);
					$log.debug("hola :)!s");
					var modalInstance = $modal.open({
						templateUrl : 'dateFiltersModal.html',
						size : size,
						controller : function($scope, $modalInstance) {
							var openDatepicker = function(opened) {
								if (!opened) {
									opened = true;
								} else {
									opened = false;
								}
								return opened;
							}
							var openDatePicker = function($event, datepicker) {
								$event.preventDefault();
								$event.stopPropagation();
								switch (datepicker) {
								case 'to':
									$scope.openedTo = openDatepicker($scope.openedTo);
									$scope.openedFrom = false;
									break;

								case 'from':
									$scope.openedFrom = openDatepicker($scope.openedFrom);
									$scope.openedTo = false;
									break;
								default:
									break;
								}
							}
							var extractDateTime = function(date) {
								if (date && (Object.prototype.toString.call(date) === '[object Date]')) {
									return date.getTime();
								} else if (!isNaN(date)) {
									return new Date().setTime(date);
								} else {
									return undefined;
								}
							}
							var setupDates = function($scope) {
								if (dateValues) {
									var dateTo = dateValues.to;
									var dateFrom = dateValues.from;
									if (!dateTo && !dateFrom) {
										$scope.dtTo = new Date().getTime();
									} else {
										$scope.dtFrom = dateFrom;
										$scope.dtTo = dateTo;
									}
								} else {
									$scope.dtTo = new Date().getTime();
								}
							}
							var setupInitDate = function(date){
								var initDate = undefined;
								if(date !== undefined){
								  initDate = new Date(date);
								   initDate.setHours(0);
								  initDate.setMinutes(0);
								  initDate.setSeconds(0);
								 return initDate.getTime();
								}else{
									return initDate;
								}
							}
							var setupEndDate = function(date){
								 var endDate = undefined ;
								 if(date!== undefined){
									  endDate = new Date(date);
									  endDate.setHours(23);
									  endDate.setMinutes(59);
									  endDate.setSeconds(59);
									return endDate.getTime(); 
								 }else{
									return  endDate;
								 }
							}
							setupDates($scope);
							$scope.today = new Date().getTime();
							$scope.clear = function() {
								$scope.dtTo = undefined;
								$scope.dtFrom = undefined;
								$scope.dateValues = {
									to : undefined,
									from : undefined
								}
								$modalInstance.close($scope.dateValues);
							};
							$scope.openDatapickerTo = function($event) {
								openDatePicker($event, 'to');
							};

							$scope.openDatapickerFrom = function($event) {
								openDatePicker($event, 'from');
							};

							$scope.cancel = function() {
								$modalInstance.dismiss('cancel');
							};

							$scope.ok = function() {
								var dateTo = extractDateTime($scope.dtTo);
								var dateFrom = extractDateTime($scope.dtFrom);
								$scope.dateValues = {
									to : setupEndDate(dateTo),
									from : setupInitDate(dateFrom)
								}
								$modalInstance.close($scope.dateValues);
							};
						},
						resolve : {
							values : function() {
								return $scope.values;
							}
						}
					});
					modalInstance.result.then(function(values) {
						$scope.values = values;
						$filter('dateRangeFilter')(params, values, paramName);
					}, function() {

					});
				}
			}
		});