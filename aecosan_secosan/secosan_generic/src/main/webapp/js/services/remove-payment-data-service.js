angular.module("secosanGeneric").factory("RemovePaymentData", function($log) {
	/**
	 * debido a que no se estan registrando los datos de pago, salvo los online, se elimina el nodo de "SolicitudPayment" para evitar reescribir datos de pago
	 *
	 */
	return{
		removePaymentNodeFromForm:function(form){
			if(!angular.equals(form.solicitudePayment,undefined)){
				delete form.solicitudePayment;
			}
		}
	}
	
	
});