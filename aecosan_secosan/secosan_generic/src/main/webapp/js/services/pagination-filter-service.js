angular.module("secosanGeneric").factory("PaginationFilter", function() {
	var $this = this;
	this.generateQueryParams = function(paginationFilterParams) {
		var paginationParams = $this.generatePaginationQueryParams(paginationFilterParams.pagination);
		var orderByParams = $this.generateOrderByQueryParams(paginationFilterParams.orderBy);
		var filters = $this.generateFilters(paginationFilterParams.filters);
		var encodedParams = $this.generateEncodedParam(paginationParams, []);
		encodedParams = $this.generateEncodedParam(orderByParams, encodedParams);
		encodedParams = $this.generateEncodedParam(filters, encodedParams);
		return encodedParams.join("&");
	}
	this.generatePaginationQueryParams = function(pagination) {
		var currentPage = pagination.currentPage;

		var totalToShow = pagination.count;
		var paramsTemplate = "start={startNum}&length={lenghtNum}";
		var startNum = 0;
		var lengthNum;
		if (currentPage <= 1) {
			lengthNum = totalToShow;
		} else {
			startNum = ((currentPage - 1) * totalToShow + 1)-1;
			lengthNum = totalToShow;
		}
		paramsTemplate = paramsTemplate.replace("{startNum}", startNum);
		paramsTemplate = paramsTemplate.replace("{lenghtNum}", lengthNum);
		return paramsTemplate;
	}
	this.generateOrderByQueryParams = function(orderBy) {
		var columnId = "";
		var order = "";
		angular.forEach(orderBy, function(value, key) {
			columnId = key;
			order = value;
		});
		return "order=" + order + "&columnorder=" + columnId
	}
	this.generateFilters = function(filters) {
		var template = "filter[{pos}][{filterName}]={value}";
		var count = 0;
		var queryParams = "";
		angular.forEach(filters, function(value, key) {
			if (Object.prototype.toString.call(value) === '[object Object]') {
				var filtersObjects = $this.generateFilterParamObjectValue(key, value, template, count);
				count = count + filtersObjects.length;
				queryParams = $this.generateFilterQueryParamsStringFromArray(queryParams, filtersObjects);
			} else if (value !== "") {
				var partial = $this.generateFilterParamSinglePairValue(key, value, template, count++);
				queryParams = $this.generateFilterQueryParamsString(queryParams, partial);

			}
		});
		return queryParams;
	}
	this.generateFilterQueryParamsStringFromArray = function(queryParams, partialArray) {
		for (var i = 0; i < partialArray.length; i++) {
			var partialFilter = partialArray[i];
			queryParams = $this.generateFilterQueryParamsString(queryParams, partialFilter);
		}
		return queryParams;
	}
	this.generateFilterQueryParamsString = function(queryParams, partial) {
		if (queryParams === "") {
			queryParams = partial;
		} else {
			queryParams = queryParams + "&" + partial;
		}
		return queryParams;
	}, this.generateFilterParamObjectValue = function(parentKey, objectValue, template, count) {
		var filters = [];
		angular.forEach(objectValue, function(value, key) {
			if (value !== undefined) {
				var keyCapitilize = key.charAt(0).toUpperCase() + key.slice(1);
				var filter = $this.generateFilterParamSinglePairValue(parentKey + keyCapitilize, value, template, count++);
				filters.push(filter);
			}
		})
		return filters;
	}
	this.generateFilterParamSinglePairValue = function(key, value, template, count) {
		template = template.replace("{pos}", count);
		template = template.replace("{filterName}", key);
		template = template.replace("{value}", value);
		return template;
	}
	this.generateEncodedParam = function(param, encodedParams) {
		if (param !== undefined && param.length > 0) {
			encodedParams.push(encodeURI(param))
		}
		return encodedParams;
	}
	this.extractNgTableParams = function(ngTableParams) {
		var filters = undefined;
		if (ngTableParams.hasFilter()) {
			filters = ngTableParams.filter();
		}
		var paginationParams = {
			pagination : {
				currentPage : ngTableParams.page(),
				total : ngTableParams.total(),
				count : ngTableParams.count()
			},
			orderBy : ngTableParams.sorting(),
			filters : filters
		}
		return paginationParams;
	}
	return {
		getPaginationUrl : function(baseUrl, ngParams) {
			var paginationParams = $this.extractNgTableParams(ngParams);
			var queryParams = $this.generateQueryParams(paginationParams);
			return baseUrl + "?" + queryParams;
		}
	}
});