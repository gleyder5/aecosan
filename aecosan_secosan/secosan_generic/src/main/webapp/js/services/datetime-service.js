angular.module("secosanGeneric").factory("DatetimeService", function($filter) {
	return {
		transformDateTime2Long : function(fechaIncidencia, horaIncidencia) {
			var composedData;

			// Si fechaIncidencia es del tipo string significa que el
			// usuario no ha modificado la fecha inicial, por lo tanto se
			// corresponde con la de horaIncidencia
			if (typeof fechaIncidencia === "string") {
				composedData = horaIncidencia.getTime();
			} else {
				fechaIncidencia.setHours(horaIncidencia.getHours());
				fechaIncidencia.setMinutes(horaIncidencia.getMinutes());
				composedData = fechaIncidencia.getTime();
			}

			return composedData;
		},
		transformDate2Long : function(date) {
			var composedData;

			if (typeof date === "string") {
				var parts = date.split('/');
				composedData = new Date(parts[2], parts[1] - 1, parts[0]).getTime();
			} else {
				composedData = date.getTime();
			}

			return composedData;
		},
		setDateTime : function(date) {
			return {
				maxDate : new Date(),
				auxFecha : {
					fechaIncidencia : $filter("date")(date, 'dd/MM/yyyy'),
					horaIncidencia : date
				},
				dateOptions : {
					startingDay : 1
				}
			}
		},
		setDate : function(date) {
			return {
				maxDate : new Date(),
				auxFecha : {
					fechaComunicacion : $filter("date")(date, 'dd/MM/yyyy')
				},
				dateOptions : {
					startingDay : 1
				}
			}
		},
		setInitialValues : function() {
			var initialDate = new Date();

			return {
				maxDate : initialDate,
				auxFecha : {
					fechaIncidencia : $filter("date")(initialDate, 'dd/MM/yyyy'),
					horaIncidencia : initialDate
				},
				dateOptions : {
					startingDay : 1
				}
			}
		},
		setInitialValuesFinanciacionAlimentos : function() {
			var initialDate = new Date();

			return {
				auxFecha : {
					fechaComunicacion : $filter("date")(initialDate, 'dd/MM/yyyy'),
				},
				dateOptions : {
					startingDay : 1
				}
			}
		},
		openDatepicker : function(dtp) {
			return function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				this.dtp = {
					opened : true
				};
			};
		}
	}
});