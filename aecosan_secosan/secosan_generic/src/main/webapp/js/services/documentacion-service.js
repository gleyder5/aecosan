angular.module("secosanGeneric").factory("DocumentacionService", function($http, REST_URL, HEADERS) {

	return {
		getDocumentacionById : function(id) {
			var url = REST_URL.DOCUMENTACION + id;
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			})
		},
		deleteDocumentacionById : function(id) {
			var url = REST_URL.DOCUMENTACION + 1 + "/" + id;
			return $http({
				method : 'DELETE',
				url : url,
				headers : HEADERS.JSON
			})
		},
	}
});