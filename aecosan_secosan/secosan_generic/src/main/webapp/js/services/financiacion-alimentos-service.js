angular.module("secosanGeneric").factory("FinanciacionAlimentos", function($http, $log, AuthenticationUser,RemovePaymentData, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addAlteracionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_ALTERACION + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateAlteracionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_ALTERACION + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		addInclusionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_INCLUSION + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateInclusionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_INCLUSION + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		addSuspensionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_SUSPENSION + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateSuspensionFinanciacionAlimentos : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.FINANCIACION_ALIMENTOS_SUSPENSION + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		deleteSuspendedProduct : function(id) {
		
			var url = REST_URL.DELETE_SUSPENDED_PRODUCT + id;
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
			})
		},
	}
});