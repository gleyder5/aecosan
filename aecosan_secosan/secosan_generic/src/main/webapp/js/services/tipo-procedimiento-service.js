angular.module("secosanGeneric").factory("TipoProcedimientoManagementService", function($http, $log, AuthenticationUser, PaginationFilter,base64, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	};

	this.isSameUserLogged = function(id) {
		var userId = AuthenticationUser.getUserPayloadData('id')
		var isSame = false;

		if (userId === id) {
			isSame = true;
		}
		return isSame;
	}
	
	this.encriptPassword=function(form){
		var passClear= form.password;
		if(passClear !== undefined && passClear !==""){
			form.password =  base64.encode(passClear);
		}
		return form;
	}

	return {
		addTipoProcedimiento : function(form) {
			var url = REST_URL.TIPO_PROCEDIMIENTO_MANAGEMENT_ADD;
			form = $this.encriptPassword(form);
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateTipoProcedimiento : function(form) {
			var url = REST_URL.TIPO_PROCEDIMIENTO_MANAGEMENT_UPDATE + $this.getUserLoggedId();
			form = $this.encriptPassword(form);
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		getTipoProcedimientoList : function(filterParams) {
			var url = REST_URL.TIPO_PROCEDIMIENTO_MANAGEMENT_LIST;
			// url = PaginationFilter.getPaginationUrl(url, filterParams);
			return $http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON,
			})
		},
		deleteTipoProcedimiento : function(idToDelete) {
                var url = REST_URL.TIPO_PROCEDIMIENTO_MANAGEMENT_DELETE + $this.getUserLoggedId() + "/" + idToDelete;
                return $http({
                    method : 'DELETE',
                    url : url,
                    headers : HEADERS.JSON,
				});
		},
		isSameUser : function(id) {
			return $this.isSameUserLogged(id);
		},
		changePasswordAdmin : function(form) {
			var url = REST_URL.CHANGE_PASSWORD_ADMIN;
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		checkIfTipoProcedimientoExists: function(dni,validationFuction,errorFunction){
			var url = REST_URL.CHECK_USER_EXISTS+dni;
			$http({
				method : 'GET',
				url : url,
				headers : HEADERS.JSON
			}).success(function(data){
				validationFuction(data);
			}).error(function(data,status){
				if(errorFunction !==undefined){
					errorFunction(data,status);
				}
			});
		}
	}
});