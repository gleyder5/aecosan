angular.module("secosanGeneric").factory("CatalogueLocationHandler", function($log, Messages, CatalogueHandler, UtilService, CATALOGUE_NAMES) {
	var $this = this;
	this.catalogueHandler = CatalogueHandler;
	$this.executeCallback = function(callback) {
		if (callback !== undefined && typeof callback === 'function') {
			if (arguments[2] === 'loadProvince') {
				callback(arguments[1][0], arguments[1][1], arguments[1][2], arguments[1][3], arguments[1][4], arguments[1][5]);
			} else if (arguments[2] === 'loadMunicipio') {
				callback(arguments[1][0], arguments[1][1], arguments[1][2], arguments[1][3]);
			}
		}
	}

	$this.municipioCatalogSuccessHandler = function($scope, data, combo, idMunicpio) {
		switch (combo) {
		case $this.domObjectName.provinciaSolicitante:
			$scope.catalogMunicipiosSolicitante = {};
			$scope.catalogMunicipiosSolicitante = data.catalog;
			// $scope.auxSolicitante.municipio.catalogId = idMunicpio;
			break;
		case $this.domObjectName.provinciaRepresentante:
			$scope.catalogMunicipiosRepresentante = {};
			$scope.catalogMunicipiosRepresentante = data.catalog;
			// $scope.auxRepresentante.municipio.catalogId = idMunicpio;
			break;
		case $this.domObjectName.provinciaContacto:
			$scope.catalogMunicipiosContacto = {};
			$scope.catalogMunicipiosContacto = data.catalog;
			// $scope.auxContacto.municipio.catalogId = idMunicpio;
			break;
		case $this.domObjectName.provinciaPago:
			$scope.catalogMunicipiosPago = {};
			$scope.catalogMunicipiosPago = data.catalog;
			break;
		case $this.domObjectName.provinciaEnvio:
			$scope.catalogMunicipiosEnvio = {};
			$scope.catalogMunicipiosEnvio = data.catalog;
			break;
         case $this.domObjectName.provinciaLugarOMedio:
         	$scope.catalogMunicipios = {};
         	$scope.catalogMunicipios = data.catalog;
         	break;
		default:
			break;
		}
	};

	$this.municipioCatalogErrorHandler = function(data, status) {
		Messages.setErrorMessageFromResponse(data, status);
	};

	$this.countryCatalogErrorHandler = function(data, status) {
		$log.error("Error getting catalog " + catalogPais + " " + status);
		Messages.setErrorMessageFromResponse(data, status);
	};

	$this.provinceCatalogSuccessHandler = function($scope, data, combo, idProvincia, callback) {
		$log.debug("province data set into scope");
		switch (combo) {
		case $this.domObjectName.paisSolicitante:
			$scope.catalogProvinciasSolicitante = {};
			// $scope.catalogMunicipiosSolicitante = {};
			$scope.catalogProvinciasSolicitante = data.catalog;
			// $scope.auxSolicitante.provincia.catalogId = idProvincia;
			break;
		case $this.domObjectName.paisRepresentante:
			$scope.catalogProvinciasRepresentante = {};
			// $scope.catalogMunicipiosRepresentante = {};
			$scope.catalogProvinciasRepresentante = data.catalog;
			// $scope.auxRepresentante.provincia.catalogId = idProvincia;
			break;
		case $this.domObjectName.paisContacto:
			$scope.catalogProvinciasContacto = {};
			// $scope.catalogMunicipiosContacto = {};
			$scope.catalogProvinciasContacto = data.catalog;
			// $scope.auxContacto.provincia.catalogId = idProvincia;
			break;
		case $this.domObjectName.paisPago:
			$scope.catalogProvinciasPago = {};
			$scope.catalogProvinciasPago = data.catalog;
			break;
		case $this.domObjectName.paisEnvio:
			$scope.catalogProvinciasEnvio = {};
			$scope.catalogProvinciasEnvio = data.catalog;
			break;
		case $this.domObjectName.paisLugarOMedio:
            $scope.catalogProvincias = {};
            $scope.catalogProvincias = data.catalog;
            break;
		default:
			break;
		}
	};

	$this.provinceCatalogErrorHandler = function(data, status) {
		Messages.setErrorMessageFromResponse(data, status);
	};

	$this.domObjectName = {
		paisSolicitante : "paisSolicitante",
		paisRepresentante : "paisRepresentante",
		paisContacto : "paisContacto",
		paisEnvio : "paisEnvio",
		paisPago : "paisPago",
		paisLugarOMedio: "paisLugarOMedio",
		provinciaSolicitante : "provinciaSolicitante",
		provinciaRepresentante : "provinciaRepresentante",
		provinciaContacto : "provinciaContacto",
		provinciaEnvio : "provinciaEnvio",
		provinciaPago : "provinciapago",
        provinciaLugarOMedio: "provinciaLugarOMedio",
		municipioSolicitante : "municipioSolicitante",
		municipioRepresentante : "municipioRepresentante",
		municipioContacto : "municipioContacto",
		municipioEnvio : "municipioEnvio",
		municipioPago : "municipioPago",
		municipioLugarOMedio: "municipioLugarOMedio"
	};

	$this.isLoaded = function(catalog) {
		var loaded = false;
		if (catalog !== undefined) {
			loaded = catalog.length > 1;
		}
		return loaded;
	}

	return {

		loadMunicipio : function($scope, idProvincia, combo, idMunicipio) {
			var catalogMuni = CATALOGUE_NAMES.MUNICIPALITY;
			var idMunicipaly_fieldName = CATALOGUE_NAMES.MUNICIPALITY_PARENT_PROVINCIE;

			$this.catalogueHandler.getRelatedCatalogue(catalogMuni, idMunicipaly_fieldName, idProvincia).success(function(data) {
				$log.info("OK getting catalog " + catalogMuni + " para " + idProvincia);
				$this.municipioCatalogSuccessHandler($scope, data, combo, idMunicipio);
			}).error(function(data, status) {
				$this.provinceCatalogErrorHandler(data, status);
			});
		},

		loadProvince : function($scope, idPais, combo, idProvincia, callback, callbackArguments) {
			var catalogProv = CATALOGUE_NAMES.PROVINCIES;
			var idPais_fieldName = CATALOGUE_NAMES.MUNICIPALITY_PARENT_COUNTRY;

			$this.catalogueHandler.getRelatedCatalogue(catalogProv, idPais_fieldName, idPais).success(function(data) {
				$log.info("OK getting catalog " + catalogProv + " para " + idPais);
				$this.provinceCatalogSuccessHandler($scope, data, combo, idProvincia);
			}).error(function(data, status) {
				$log.error("Error getting catalog " + catalogProv + " " + status);
				$this.provinceCatalogErrorHandler(data, status);
			});
		},

		loadProvinciaOnChange : function($scope, comboPais) {
			switch (comboPais) {
			case $this.domObjectName.paisSolicitante:
				// if ($scope.auxSolicitante.pais!== undefined &&
				// $scope.auxSolicitante.pais !== null &&
				// $scope.auxSolicitante.pais.catalogId ===
				// CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
				if (UtilService.checkNotNullNorUndefined($scope.auxSolicitante.pais) && $scope.auxSolicitante.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
					this.loadProvince($scope, $scope.auxSolicitante.pais.catalogId, comboPais);
				} else {
					$scope.catalogProvinciaSolicitante = {};
					$scope.auxSolicitante.provincia = null;
					$scope.catalogMunicipioSolicitante = {};
					$scope.auxSolicitante.municipio = null;
				}
				break;
			case $this.domObjectName.paisRepresentante:
				if (UtilService.checkNotNullNorUndefined($scope.auxRepresentante.pais) && $scope.auxRepresentante.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
					this.loadProvince($scope, $scope.auxRepresentante.pais.catalogId, comboPais);
				} else {
					$scope.catalogProvinciaRepresentante = {};
					$scope.auxRepresentante.provincia = null;
					$scope.catalogMunicipioRepresentante = {};
					$scope.auxRepresentante.municipio = null;
				}
				break;
			case $this.domObjectName.paisContacto:
				if (UtilService.checkNotNullNorUndefined($scope.auxContacto.pais) && $scope.auxContacto.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
					this.loadProvince($scope, $scope.auxContacto.pais.catalogId, comboPais);
				} else {
					$scope.catalogProvinciaContacto = {};
					$scope.auxContacto.provincia = null;
					$scope.catalogMunicipioContacto = {};
					$scope.auxContacto.municipio = null;
				}
				break;
			case $this.domObjectName.paisPago:
				if (UtilService.checkNotNullNorUndefined($scope.auxPago.pais) && $scope.auxPago.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
					this.loadProvince($scope, $scope.auxPago.pais.catalogId, comboPais);
				} else {
					$scope.catalogProvinciaPago = {};
					$scope.auxPago.provincia = null;
					$scope.catalogMunicipioPago = {};
					$scope.auxPago.municipio = null;
				}
				break;
			case $this.domObjectName.paisEnvio:
				if (UtilService.checkNotNullNorUndefined($scope.auxEnvio.pais) && $scope.auxEnvio.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
					this.loadProvince($scope, $scope.auxEnvio.pais.catalogId, comboPais);
				} else {
					$scope.catalogProvinciaEnvio = {};
					$scope.auxEnvio.provincia = null;
					$scope.catalogMunicipioEnvio = {};
					$scope.auxEnvio.municipio = null;
				}
				break;
                case $this.domObjectName.paisLugarOMedio:
                    if (UtilService.checkNotNullNorUndefined($scope.auxLugarOMedio.pais) && $scope.auxLugarOMedio.pais.catalogId === CATALOGUE_NAMES.SPAIN_COUNTRY_CODE) {
                        this.loadProvince($scope, $scope.auxLugarOMedio.pais.catalogId, comboPais);
                    } else {
                        $scope.catalogProvincias = {};
                        $scope.auxLugarOMedio.provincia = null;
                        $scope.catalogMunicipios= {};
                        $scope.auxLugarOMedio.municipio = null;
                    }
                    break;
			default:
				break;
			}
		},

		loadMunicipioOnChange : function($scope, comboProvincia) {
			switch (comboProvincia) {
			case $this.domObjectName.provinciaSolicitante:
				if (UtilService.checkNotNullNorUndefined($scope.auxSolicitante.provincia)) {
					this.loadMunicipio($scope, $scope.auxSolicitante.provincia.catalogId, comboProvincia);
				} else {
					$scope.catalogMunicipiosSolicitante = {};
					$scope.auxSolicitante.municipio = null;
				}
				break;
			case $this.domObjectName.provinciaRepresentante:
				if (UtilService.checkNotNullNorUndefined($scope.auxRepresentante.provincia)) {
					this.loadMunicipio($scope, $scope.auxRepresentante.provincia.catalogId, comboProvincia);
				} else {
					$scope.catalogMunicipiosRepresentante = {};
					$scope.auxRepresentante.municipio = null;
				}
				break;
			case $this.domObjectName.provinciaContacto:
				if (UtilService.checkNotNullNorUndefined($scope.auxContacto.provincia)) {
					this.loadMunicipio($scope, $scope.auxContacto.provincia.catalogId, comboProvincia);
				} else {
					$scope.catalogMunicipiosContacto = {};
					$scope.auxContacto.municipio = null;
				}
				break;
			case $this.domObjectName.provinciaPago:
				if (UtilService.checkNotNullNorUndefined($scope.auxPago.provincia)) {
					this.loadMunicipio($scope, $scope.auxPago.provincia.catalogId, comboProvincia);
				} else {
					$scope.catalogMunicipiosPago = {};
					$scope.auxPago.municipio = null;
				}
				break;
			case $this.domObjectName.provinciaEnvio:
				if (UtilService.checkNotNullNorUndefined($scope.auxEnvio.provincia)) {
					this.loadMunicipio($scope, $scope.auxEnvio.provincia.catalogId, comboProvincia);
				} else {
					$scope.catalogMunicipiosEnvio = {};
					$scope.auxEnvio.municipio = null;
				}
				break;
			case $this.domObjectName.provinciaLugarOMedio:
                    if (UtilService.checkNotNullNorUndefined($scope.auxLugarOMedio.provincia)) {
                        this.loadMunicipio($scope, $scope.auxLugarOMedio.provincia.catalogId, comboProvincia);
                    } else {
                        $scope.catalogMunicipios = {};
                        $scope.auxLugarOMedio.municipio = null;
                    }
                    break;

			default:
				break;
			}
		},
		loadProvinciaOnClick : function($scope, comboPais) {
			switch (comboPais) {
			case $this.domObjectName.paisSolicitante:
				if (!$this.isLoaded($scope.catalogProvinciasSolicitante)) {
				}
				break;
			case $this.domObjectName.paisRepresentante:
				if (!$this.isLoaded($scope.catalogProvinciasRepresentante)) {
					this.loadProvince($scope, $scope.auxRepresentante.pais.catalogId, comboPais);
				}
				break;
			case $this.domObjectName.paisContacto:
				if (!$this.isLoaded($scope.catalogProvinciasContacto)) {
					this.loadProvince($scope, $scope.auxContacto.pais.catalogId, comboPais);
				}
				break;
			case $this.domObjectName.paisPago:
				if (!$this.isLoaded($scope.catalogProvinciasPago)) {
					this.loadProvince($scope, $scope.auxPago.pais.catalogId, comboPais);
				}
				break;
			case $this.domObjectName.paisEnvio:
				if (!$this.isLoaded($scope.catalogProvinciasEnvio)) {
					this.loadProvince($scope, $scope.auxEnvio.pais.catalogId, comboPais);
				}
				break;
                case $this.domObjectName.paisLugarOMedio:
                    if (!$this.isLoaded($scope.catalogProvinciasLugarOMedio)) {
                        this.loadProvince($scope, $scope.auxLugarOMedio.pais.catalogId, comboPais);
                    }
            break;
			default:
				break;
			}
		},
		loadMunicipioOnClick : function($scope, comboProvincia) {
			switch (comboProvincia) {
			case $this.domObjectName.provinciaSolicitante:
				if ($this.isLoaded($scope.catalogMunicipiosSolicitante)) {
					this.loadMunicipio($scope, $scope.auxSolicitante.provincia.catalogId, comboProvincia);
				}
				break;
			case $this.domObjectName.provinciaRepresentante:
				if ($this.isLoaded($scope.catalogMunicipiosRepresentante)) {
					this.loadMunicipio($scope, $scope.auxRepresentante.provincia.catalogId, comboProvincia);
				}
				break;
			case $this.domObjectName.provinciaContacto:
				if ($this.isLoaded($scope.catalogMunicipiosContacto)) {
					this.loadMunicipio($scope, $scope.auxContacto.provincia.catalogId, comboProvincia);
				}
				break;
			case $this.domObjectName.provinciaPago:
				if ($this.isLoaded($scope.catalogMunicipiosPago)) {
					this.loadMunicipio($scope, $scope.auxPago.provincia.catalogId, comboProvincia);
				}
				break;
			case $this.domObjectName.provinciaEnvio:
				if ($this.isLoaded($scope.catalogMunicipiosEnvio)) {
					this.loadMunicipio($scope, $scope.auxEnvio.provincia.catalogId, comboProvincia);
				}
				break;
			case $this.domObjectName.provinciaLugarOMedio:
                    if ($this.isLoaded($scope.catalogMunicipioLugarOMedio)) {
                        this.loadMunicipio($scope, $scope.auxLugarOMedio.provincia.catalogId, comboProvincia);
                    }
             break;
				default:
				break;
			}
		},
		countryCatalogSuccessHandler : function($scope, data, callback, callbackArguments) {
			$scope.catalogoPaises = data.catalog;
			$log.debug("Country data set into scope");
			$this.executeCallback(callback, callbackArguments, 'loadProvince');
		},

		domObjectName : $this.domObjectName,
		setCatalogueHandler : function(catalogueHandler) {
			$this.catalogueHandler = catalogueHandler;
		}
	// countryCatalogErrorHandler : $this.countryCatalogErrorHandler

	}

});