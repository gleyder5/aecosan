angular.module("secosanGeneric").factory("Instrucciones", function($http,FileDtoDownload,Messages, Spinner,$log, REST_URL) {
	var $this = this;
	this.baseUrl =  REST_URL.INSTRUCCIONES;
	return {
		getInstrucciones : function(form, successCallback,errorCallback) {
			Spinner.showSpinner();
			var formType =form.formularioEspecifico.id;
			var url = $this.baseUrl + formType;
			 $http({
				method : 'GET',
				url : url
			}).success(function(data) {
				FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);
				if(!angular.equals(successCallback,undefined)){
					successCallback(data);
				}
				Spinner.hideSpinner();
			}).error(function(data,status) {
				Messages.setErrorMessageFromResponse(data, status);
				if(!angular.equals(errorCallback,undefined)){
					errorCallback(data,status);
				}
				Spinner.hideSpinner();
			});
		},
		setBaseUrl:function(baseUrl){
			$this.baseUrl = baseUrl;
		}
	}
});