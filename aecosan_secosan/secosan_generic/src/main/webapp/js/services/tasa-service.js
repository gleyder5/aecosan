angular.module("secosan").factory("TasaService", function($log,$http,REST_URL,HEADERS)  {
	
var $this =this;

	$this.requestInfo= function(url,successCallback,failCallback){
		 $http({
			method : 'GET',
			url : url,
			headers : HEADERS.JSON,
		}).success(function(data) {
			if(successCallback !== undefined){
				successCallback(data);
			}

		}).error(function(data, status) {
			if(failCallback !== undefined){
				failCallback(data, status);
			}
			
		});
	}
	
	$this.requestTasaInfoByFormId = function(identificador,successCallback,failCallback){
		var url = REST_URL.TASA_BY_FORM+identificador
		$this.requestInfo(url,successCallback,failCallback);
	};
	
	$this.requestTasaInfoByServiceId = function(serviceId,successCallback,failCallback){
		var url = REST_URL.TASA_BY_SERVICE+serviceId
		 $this.requestInfo(url,successCallback,failCallback);
	};
	
	return {
		getTasaInfo:function(identificador,successCallback,failCallback){
			$this.requestTasaInfoByFormId(identificador,successCallback,failCallback);
		},
		getTasaInfoByService:function(serviceId,successCallback,failCallback){
			$this.requestTasaInfoByServiceId(serviceId,successCallback,failCallback);
		}
	}
});