angular.module("secosanGeneric").factory("Spinner", function SpinnnerFactory($log) {
	var showSpiner = false;
	return {
		showSpinner:function(){
			showSpiner=true;
		},
		hideSpinner:function(){
			showSpiner=false;
		},
		loadSpinner:function(){
			return showSpiner;
		},
		getSpinnerDivId:function(){
			return "#loadingContainer";
		}
		
	}
});