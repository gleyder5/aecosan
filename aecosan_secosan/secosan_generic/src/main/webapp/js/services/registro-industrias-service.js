angular.module("secosanGeneric").factory("RegistroIndustriasService", function($http, $log, AuthenticationUser, RemovePaymentData, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addIndustrias : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.REGISTRO_INDUSTRIAS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateIndustrias : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.REGISTRO_INDUSTRIAS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});