angular.module("secosanGeneric").factory("Queja", function($http, $log, AuthenticationUser, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addQueja : function(form) {
			var url = REST_URL.QUEJA + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateQueja : function(form) {
			var url = REST_URL.QUEJA + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});