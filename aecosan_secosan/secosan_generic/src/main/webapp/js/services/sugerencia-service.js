angular.module("secosanGeneric").factory("Sugerencia", function($http, AuthenticationUser, REST_URL, HEADERS) {
	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addSugerencia : function(form) {
			var url = REST_URL.SUGERENCIA + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateSugerencia : function(form) {
			var url = REST_URL.SUGERENCIA + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});