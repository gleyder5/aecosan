angular.module("secosanGeneric").factory("WelcomeMenu", function($log) {
	var $this = this;
	this.welcomeMenuItemActive = "";
	this.textUrlArray = {};
	return {
		setTextUrlArray : function(textUrlArray) {
			$this.welcomeMenuItemActive ="";
			$this.textUrlArray = textUrlArray;
		},
		getWelcomeMenuItem : function() {
			return $this.welcomeMenuItemActive;
		},
		setWelcomeMenuItem : function(welcomeMenuItemActive) {
			$this.welcomeMenuItemActive = welcomeMenuItemActive;
		},
		getMessagesObjectByActive : function() {
			if ($this.welcomeMenuItemActive !== "") {
				return $this.textUrlArray[$this.welcomeMenuItemActive];
			} else {
				this.getDefaultMessage();
			}
		},
		getDefaultMessage : function() {
			return $this.textUrlArray["defaultMessage"];
		}
		
	}
});