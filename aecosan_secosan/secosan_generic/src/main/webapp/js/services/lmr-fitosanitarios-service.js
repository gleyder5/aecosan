angular.module("secosanGeneric").factory("LmrFitosanitarios", function($http, $log, AuthenticationUser,RemovePaymentData, REST_URL, HEADERS) {

	var $this = this;

	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		addLmrFitosanitarios : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			RemovePaymentData
			var url = REST_URL.LMR_FITOSANITARIOS + $this.getUserLoggedId();
			return $http({
				method : 'POST',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		},
		updateLmrFitosanitarios : function(form) {
			RemovePaymentData.removePaymentNodeFromForm(form);
			var url = REST_URL.LMR_FITOSANITARIOS + $this.getUserLoggedId();
			return $http({
				method : 'PUT',
				url : url,
				headers : HEADERS.JSON,
				data : form
			})
		}
	}
});