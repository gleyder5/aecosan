angular.module("secosanGeneric").factory("InstruccionesPago", function($http, $log, AuthenticationUser, REST_URL) {

	var $this = this;
	this.baseUrl = REST_URL.INSTRUCCIONES_PAGO_DOWNLOAD
	this.getUserLoggedId = function() {
		return AuthenticationUser.getUserPayloadData("id");
	}

	return {
		getInstruccionesPago : function(idFormulario) {
			var url =  $this.baseUrl+ idFormulario;
			return $http({
				method : 'GET',
				url : url
			})
		},
		setBaseUrl:function(baseUrl){
			$this.baseUrl = baseUrl;
		}
	}
});