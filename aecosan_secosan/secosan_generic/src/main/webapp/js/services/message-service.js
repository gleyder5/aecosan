angular.module("secosanGeneric").factory("Messages", function() {

	var messageArgs = {};
	var elementId = "msg";
	var anchorId = "msgAnchor";
	var forceRemove = false;

	return {
		warning : "warn",
		success : "success",
		error : "error",
		info : "info",
		setMessage : function(message) {

			messageArgs["message"] = message;
		},
		setMessageType : function(type) {
			messageArgs["messageType"] = type;
		},
		setMessageAndType : function(message, type) {
			if (type === undefined) {
				type = this.error;
			}
			this.setMessageType(type);
			this.setMessage(message);

		},
		clearMessages : function() {
			messageArgs = {};
		},
		clearMessagesAndForceRemove:function(){
			messageArgs = {};
			forceRemove = true;
		},
		getMessageArgs : function() {
			return messageArgs;
		},
		getElementId : function() {
			return elementId;
		},
		getAnchorId : function() {
			return anchorId;
		},
		setErrorMessageFromResponse : function(responseData, status) {
			var msg = "";

			if (status === 500 && responseData.error !== undefined) {
				msg = responseData.description;
			} else {
				// TODO MANEJAR ERRORES GENERICOS
			}
			this.setMessageAndType(msg);
		},
		getForceRemove:function(){
			return forceRemove;
		}

	}
});