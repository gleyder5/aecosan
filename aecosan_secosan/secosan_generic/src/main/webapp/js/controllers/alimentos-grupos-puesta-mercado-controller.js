angular.module('secosan').controller(
		'AlimentosGruposPuestaMercadoController',
		function($scope, $http, $log, $window, $routeParams, $location, $sce, HeaderTitle, NgTableParams, ModalService, ModalParamsService, AlimentosGrupos, Spinner, FormModelService, Instrucciones,
				IdentificadorPeticion, Messages, UtilService, FileDtoDownload, ValidationUtilsService, CatalogueLocationHandler, SetDataFromCatalogService, ImpresoFirmaService, Solicitude,
				LocationCombosService, CheckSolicitudeEditabilityService, RegistryNumberFormatService, CATALOGUE_NAMES, FORM_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, VALIDATION_PATTERN,
				SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL, TASA, STATUS_CODES,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getAlimentosGruposPuestaMercadoModel();
 
			SetDataFromCatalogService.getEUCountriesCatalogue($scope);
			SetDataFromCatalogService.getFormatsCatalogue($scope);
			SetDataFromCatalogService.getProductTypeCatalogue($scope);
			SetDataFromCatalogService.getNoEUCountriesCatalogue($scope,"catalogoPaisesNoUE");

			$this.initializeNewSolicitude = function() {

                Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				};

				$scope.aux = {
					idIsProductMaker : "",
					tipoProducto : {}
				};

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};

				$scope.formularioATratar.antecedentesProcedencia.primeraComercializacionUE = true;
				$scope.formasToShow = [];

				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.ALIM_GRUPOS_PUESTA_MERCADO).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadFormToEdit = function(data) {
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};
                $scope.checkboxModel = {};


                $scope.aux = {
					paisProcedencia : {},
					paisComercializacionPrevia : {},
					idIsProductMaker : "",
					tipoProducto : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				// Datos identificativos adicionales
				var isProductMaker = $scope.formularioATratar.datosIdentificativosAdicionales.isProductMaker;
				if (isProductMaker) {
					$scope.aux.idIsProductMaker = "true";
				} else if (isProductMaker === "") {
					$scope.aux.idIsProductMaker = "";
				} else {
					$scope.aux.idIsProductMaker = "false";
				}

				RegistryNumberFormatService.putRGSEAAInView($scope, $scope.formularioATratar.datosIdentificativosAdicionales.rgseaa);

				$scope.aux.tipoProducto.catalogId = $scope.formularioATratar.productType.id;
				$scope.formasToShow = $scope.formularioATratar.presentaciones;
				$scope.aux.paisProcedencia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisProcedencia;
				$scope.aux.paisComercializacionPrevia.catalogId = $scope.formularioATratar.antecedentesProcedencia.idPaisComercializacionPrevia;
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxNumRGSEAA = {
				"auxNumRGSEAA1" : "",
				"auxNumRGSEAA2" : "",
				"auxNumRGSEAA3" : ""
			};

			// 	control de tabs
			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.ALIMENTOS_GRUPOS_PUESTA_MERCADO);
			$scope.$watch("instruccionesLeidas",function(newV,oldV){
				if(newV===oldV){
					return false; 
				}
				$scope.tabCtrl.setTabFocusByIndex($scope.tabCtrl,0);
			});
			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope);
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedPaymentAndLabelDocumentAttached($scope.formularioATratar.documentacion)) {
					console.log($scope.formularioATratar.documentacion)
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_PAYMENT_LABEL_DOCUMENT, "tab-documentacion",$scope,'SIGNED_PAYMENT_LABEL_DOCUMENT');
				}
				
			}
			


			$scope.saveForm = function() {
				$scope.editMode = false;
			}

			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;
				delete $scope.formularioATratar['solicitudePayment'];
				LocationCombosService.setLocationDataToScope($scope);

				$scope.formularioATratar.presentaciones = $scope.formasToShow;
				$scope.formularioATratar.datosIdentificativosAdicionales.isProductMaker = $scope.aux.idIsProductMaker;
				$scope.formularioATratar.productType.id = $scope.aux.tipoProducto.catalogId;
				$scope.formularioATratar.datosIdentificativosAdicionales.rgseaa = RegistryNumberFormatService.composeRGSEAA($scope.auxNumRGSEAA.auxNumRGSEAA1, $scope.auxNumRGSEAA.auxNumRGSEAA2,
						$scope.auxNumRGSEAA.auxNumRGSEAA3);

				if (UtilService.checkNotNullNorUndefined($scope.aux.paisProcedencia)) {
					$scope.formularioATratar.antecedentesProcedencia.idPaisProcedencia = $scope.aux.paisProcedencia.catalogId;
				}

				if (!$scope.formularioATratar.antecedentesProcedencia.primeraComercializacionUE) {
					if (UtilService.checkNotNullNorUndefined($scope.aux.paisComercializacionPrevia)) {
						$scope.formularioATratar.antecedentesProcedencia.idPaisComercializacionPrevia = $scope.aux.paisComercializacionPrevia.catalogId;
					}
				}

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					requiredDocumentAttached = ValidationUtilsService.isSignedPaymentAndLabelDocumentAttached($scope.formularioATratar.documentacion);
				} else if (senderMode === SENDER_MODE.DRAFT) {
					// TODO: check default behaviour
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				$log.info("enviar:> " + $scope.formularioATratar.solicitante.name);
				if (ValidationUtilsService.checkFormValid($scope,true)) {
					if ((requiredDocumentAttached && senderMode === SENDER_MODE.SUBMIT) || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
							AlimentosGrupos.updatePuestaMercado($scope.formularioATratar).success(function() {
								$log.debug("updatePuestaMercado OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("updatePuestaMercado KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						} else {
							AlimentosGrupos.addPuestaMercado($scope.formularioATratar).success(function() {
								$log.debug("addPuestaMercado OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addPuestaMercado KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						Spinner.hideSpinner();
						ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_PAYMENT_LABEL_DOCUMENT, "tab-documentacion",$scope,'SIGNED_PAYMENT_LABEL_DOCUMENT');
					}
				} else {
					Spinner.hideSpinner();
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_PAYMENT_LABEL_DOCUMENT, "tab-documentacion",$scope,'SIGNED_PAYMENT_LABEL_DOCUMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
				}
			};

			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.formularios = FORM_NAMES;
			$scope.formularioSeleccionado = FORM_NAMES.CESECOMERCIALIZACION;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.tipoSolicitudId = SOLICITUDE_CODES.ALIM_GRUPOS_PUESTA_MERCADO;
			$scope.eliminarDocumento = $this.eliminarDocumento;
			$scope.changeMunicipioSolicitante = $this.changeMunicipioSolicitante;
			$scope.showModal = $this.showModal;

			$scope.gotoWelcome = $this.gotoWelcome;
			$scope.showPagoPanel = $this.showPagoPanel;
			$scope.formID = FORM_ID_CODES.GE_PUESTA_MERCADO;
			$scope.tasatipo = TASA.GE_PUESTA_MERCADO;
		});