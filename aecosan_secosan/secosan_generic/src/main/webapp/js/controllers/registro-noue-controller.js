angular.module('secosan').controller('RegistroNoUeController',
		function($scope, $log, $location, Messages, FormModelService, UtilService, UserManagementService,Spinner, ERR_MSG, VALIDATION_PATTERN, REDIRECT_URL) {

			var $this = this;

			$scope.formularioATratar = FormModelService.getUserNoUERegisterModel();

			$scope.aux = {
				retypePassword : "",
				group : {},
				rol : {}
			}

			var errorMessage = "";

			$this.checkValidForm = function() {
				var valid = true;
				if ($scope.formularioATratar.password !== $scope.aux.retypePassword) {
					errorMessage = ERR_MSG.PASSWORD_MATCH;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined($scope.formularioATratar.email)) {
					errorMessage = ERR_MSG.EMAIL_INVALID;
					valid = false;
				} else if (!UtilService.checkNotNullNorUndefined($scope.formularioATratar.identificationNumber)) {
					errorMessage = ERR_MSG.NIF_INVALID;
					valid = false;
				}
				return valid;
			};

			$this.gotoLogin = function() {
				$location.path(REDIRECT_URL.LOGIN);
			};

			$scope.sendForm = function() {
				Spinner.showSpinner();
				if ($this.checkValidForm()) {
					var form = {};
					form = angular.copy($scope.formularioATratar,form);
					UserManagementService.addUserNoUe(form).success(function() {
						$log.debug("addUserNoUe OK");
						Spinner.hideSpinner();
						$location.path(REDIRECT_URL.LOGIN);
					}).error(function(data, status, headers, config) {
						$log.debug("addUserNoUe KO");
						$log.debug(data);
						$log.debug(status);
						$log.debug(headers);
						$log.debug(config);
						$scope.hasdata = false;
						Spinner.hideSpinner();
						Messages.setErrorMessageFromResponse(data, status);
					});
				} else {
					Spinner.hideSpinner();
					Messages.setMessageAndType(errorMessage, Messages.error);
				}
			}

			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.gotoLogin = $this.gotoLogin;
		});