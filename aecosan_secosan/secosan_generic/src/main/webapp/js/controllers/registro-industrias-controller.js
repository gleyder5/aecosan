angular.module('secosan').controller(
		'RegistroIndustriasController',
		function($scope, $http, $location, $log, $window, $sce, $routeParams, HeaderTitle, Messages, NgTableParams, ModalService, ModalParamsService, Spinner, FormModelService, ServiciosPagoService,
				Instrucciones, IdentificadorPeticion, Solicitude, FileDtoDownload, ValidationUtilsService, LocationCombosService, UtilService, CatalogueLocationHandler, ImpresoFirmaService,
				SetDataFromCatalogService, RegistryNumberFormatService, CheckSolicitudeEditabilityService, RegistroIndustriasService, PagoService, ERR_MSG, CATALOGUE_NAMES, SOLICITUDE_CODES,
				RESPONSIBILITIES_MSG, VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL, TASA, STATUS_CODES,TABS_CONFIGURATION) {

			var $this = this;
			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;
			$this.modelo = FormModelService.getRegistroIndustriasModel();
			SetDataFromCatalogService.getCountryCatalogue($scope);
			SetDataFromCatalogService.getNoEUCountriesCatalogue($scope,"catalogoPaisesNoUE");
			SetDataFromCatalogService.getPaymentTypeCatalogue($scope);
			SetDataFromCatalogService.getShippingTypeCatalogue($scope);

			$this.initializeNewSolicitude = function() {
				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				}

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};
				$scope.auxEnvio = {};

				$scope.auxTipoEnvio = {
					tipoEnvio : {},
				};
				$scope.auxAltaPais = {
					paisOrigen : {},
				};
				$scope.auxPago = {};

				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.REGISTRO_INDUSTRIAS).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};
			$scope.auxNumRGSEAA = {
				"auxNumRGSEAA1" : "",
				"auxNumRGSEAA2" : "",
				"auxNumRGSEAA3" : ""
			};
			$this.loadFormToEdit = function(data) {
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;
				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};

				$scope.auxEnvio = {
					pais : {},
					provincia : {},
					municipio : {}
				};
				$scope.auxPago = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				$scope.auxAltaPais = {
					paisOrigen : {}
				};

				$scope.auxTipoEnvio = {
					tipoEnvio : {},
				};
                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};

				$scope.auxTipoEnvio.tipoEnvio.catalogId = $scope.formularioATratar.tipoEnvio.id;

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisOrigen);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisEnvio);

				RegistryNumberFormatService.putRGSEAAInView($scope, $scope.formularioATratar.rgseaa);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$this.isSubsanation = function() {
				return $scope.formularioATratar === STATUS_CODES.PENDIENTE;
			};

		
			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};
			// control de tab
			$scope.tabCtrl= angular.copy(TABS_CONFIGURATION.REGISTRO_INDUSTRIAS);
			$scope.$watch("instruccionesLeidas",function(newV,oldV){
				if(newV===oldV){
					return false; 
				}
				$scope.tabCtrl.setTabFocusByIndex($scope.tabCtrl,0);
			});



			$scope.saveForm = function() {
				$scope.editMode = false;
			}

			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;

				// $scope.formularioATratar.paisOrigen.id = $scope.auxAltaPais.paisOrigen.catalogId;
				LocationCombosService.setLocationDataToScope($scope);
				LocationCombosService.setPaisOrigenToScope($scope);
				LocationCombosService.setLocationShippingDataToScope($scope);

				$scope.formularioATratar.tipoEnvio.id = $scope.auxTipoEnvio.tipoEnvio.catalogId;
				$scope.formularioATratar.rgseaa = RegistryNumberFormatService.composeRGSEAA($scope.auxNumRGSEAA.auxNumRGSEAA1, $scope.auxNumRGSEAA.auxNumRGSEAA2, $scope.auxNumRGSEAA.auxNumRGSEAA3);

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					requiredDocumentAttached = ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion);
				} else if (senderMode === SENDER_MODE.DRAFT) {
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				$log.info("enviar:> " + $scope.formularioATratar.solicitante.name);
				if((ValidationUtilsService.checkFormValid($scope,true) && ValidationUtilsService.isIndustryDataCorrect($scope,"tab-registroindustriasdocumentacion"))){
					if ((requiredDocumentAttached && senderMode === SENDER_MODE.SUBMIT) || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
							RegistroIndustriasService.updateIndustrias($scope.formularioATratar).success(function() {
								$log.debug("updateIndustrias OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("updateIndustrias KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						} else {
							RegistroIndustriasService.addIndustrias($scope.formularioATratar).success(function() {
								$log.debug("addIndustrias OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addIndustrias KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENT_SIGNED_PAYMENT, "tab-registroindustriasdocumentacion",$scope,'DOCUMENT_SIGNED_PAYMENT');
						Spinner.hideSpinner();
					}
				} else {
					Spinner.hideSpinner();
					ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENT_SIGNED_PAYMENT, "tab-registroindustriasdocumentacion",$scope,'DOCUMENT_SIGNED_PAYMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
				}
			};
			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope); 
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENT_SIGNED_PAYMENT, "tab-registroindustriasdocumentacion",$scope,'DOCUMENT_SIGNED_PAYMENT');
				}
			
			}

			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.postal_code_pattern = VALIDATION_PATTERN.POSTAL_CODE;

			$scope.tipoSolicitudId = SOLICITUDE_CODES.REGISTRO_INDUSTRIAS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.payment_tasa_code = CATALOGUE_NAMES.PAYMENT_TASA_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.paisEnvio = CatalogueLocationHandler.domObjectName.paisEnvio;
			$scope.paisPago = CatalogueLocationHandler.domObjectName.paisPago;
			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.provinciaEnvio = CatalogueLocationHandler.domObjectName.provinciaEnvio;
			$scope.provinciaPago = CatalogueLocationHandler.domObjectName.provinciaPago;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;

			$scope.loadService = $this.loadService;
			$scope.gotoWelcome = $this.gotoWelcome;
			$scope.showPagoPanel = $this.showPagoPanel;
			$scope.formID = FORM_ID_CODES.REGISTRO_INDUSTRIAS;
		});