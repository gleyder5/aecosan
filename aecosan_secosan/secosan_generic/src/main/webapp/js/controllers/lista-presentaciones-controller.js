angular.module('secosanGeneric').controller('ListaPresentacionesController', function($scope, $log, $location, Messages, ModalService, ModalParamsService) {

	var $this = this;

	$this.composeFormat = function(idForma, envase, otras, sabor) {
		return {
			forma : {
				id : idForma
			},
			otrasCaracteristicas : otras,
			envase : envase,
			sabores : sabor
		}
	};

	$this.resetFormatFrom = function() {
		$scope.aux.forma = undefined;
		$scope.aux.otrasCaracteristicas = "";
		$scope.aux.tipoTamEnvase = "";
		$scope.aux.sabores = "";
	}

	$this.addPresentation = function() {
		var newForma;
		var idForma;
		var envase;
		var otras;
		var sabor;

		var formaForm = $scope.aux.forma;

		if (formaForm !== undefined && $scope.aux.tipoTamEnvase.length > 0) {
			idForma = formaForm.catalogId;
			otras = $scope.aux.otrasCaracteristicas;
			envase = $scope.aux.tipoTamEnvase;
			sabor = $scope.aux.sabores;
			newForma = $this.composeFormat(idForma, envase, otras, sabor);
			$scope.formasToShow.push(newForma);
			$this.resetFormatFrom();
		} else {
			ModalService.showModalWindow(ModalParamsService.presentationError());
		}
	}

	$this.eliminarForma = function(index) {
		$scope.formasToShow.splice(index, 1);
	};

	$scope.addPresentation = $this.addPresentation;
	$scope.eliminarForma = $this.eliminarForma;
});
