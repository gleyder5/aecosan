angular.module('secosanGeneric').controller(
		'productsController',
		function($scope, $log, $location, Messages, ModalService, ModalParamsService, RegistryNumberFormatService,FinanciacionAlimentos) {

			var $this = this;

			$this.composeObject = function(newRegNumber) {
				return {
					productRegistrationNumber : newRegNumber,
					identificationCode : $scope.auxProduct.codIdProd,
					description : $scope.auxProduct.descpresentacion
				}
			}

			$this.emptyFields = function() {
				$scope.auxProduct = {};
				$scope.auxProduct.numRegPro1 = "";
				$scope.auxProduct.numRegPro2 = "";
				$scope.auxProduct.numRegPro3 = "";
				$scope.auxProduct.numRegPro4 = "";
				$scope.auxProduct.codIdProd = "";
				$scope.auxProduct.descpresentacion = "";
			};

			$this.emptyFields();

			$this.checkFields = function() {
				return ($scope.auxProduct.numRegPro1.length !== 0 && $scope.auxProduct.numRegPro2.length !== 0 && $scope.auxProduct.numRegPro3.length !== 0
						&& $scope.auxProduct.numRegPro4.length !== 0 && $scope.auxProduct.codIdProd.length !== 0 && $scope.auxProduct.descpresentacion.length !== 0);
			};

			$this.addProduct = function() {
				if ($this.checkFields()) {
					var newRegNumber = RegistryNumberFormatService.composeProductReferenceNumber($scope.auxProduct.numRegPro1, $scope.auxProduct.numRegPro2, $scope.auxProduct.numRegPro3,
							$scope.auxProduct.numRegPro4);
					var newProduct = $this.composeObject(newRegNumber);
					$scope.formularioATratar.products.push(newProduct);
					$this.emptyFields();
				} else {
					ModalService.showModalWindow(ModalParamsService.financialProductError());
				}
			};

			$this.deleteProduct = function(index) {
				var product =$scope.formularioATratar.products[index];
				if(product.id !== undefined && product.id !==""){
					FinanciacionAlimentos.deleteSuspendedProduct(product.id).success(function(){
						
						$scope.formularioATratar.products.splice(index, 1);
					
					}).error(function(){
						ModalService.showModalWindow(ModalParamsService.deleteProductError());
					});
					
				}else{
					$scope.formularioATratar.products.splice(index, 1);
				}
			}

			$scope.addProduct = $this.addProduct;
			$scope.deleteProduct = $this.deleteProduct;
		});
