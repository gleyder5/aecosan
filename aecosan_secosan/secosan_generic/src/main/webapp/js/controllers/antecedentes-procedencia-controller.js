angular.module('secosanGeneric').controller('AntecedentesProcedenciaController', function($scope, $http, $log, SetDataFromCatalogService) {

	SetDataFromCatalogService.getEUCountriesCatalogue($scope,"catalogoPaisesUE");
	SetDataFromCatalogService.getNoEUCountriesCatalogue($scope);

});
