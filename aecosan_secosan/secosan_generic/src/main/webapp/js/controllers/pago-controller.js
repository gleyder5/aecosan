angular.module('secosan').controller(
		'pagoController',
		function($scope, $http, $log, $location, Messages, CatalogueHandler, ModalService, ModalParamsService, InstruccionesPago, FormModelService, LocationCombosService, DocumentacionService,
				CatalogueLocationHandler, FileDtoDownload, PagoOnline, base64, JustificantePagoService, PagoService, PAYMENT_TYPE, IMAGES_URL, MINIAPPLET, POPUP_IMAGES, MONTHS_TWO_DIGITS_SELECT,
				YEARS_TWO_DIGITS_SELECT, CATALOGUE_NAMES,DISABLE_PAYMENTONLINE) {

			var $this = this;
			$scope.aux = {};
			$scope.showPagoOnline = true;

			$this.servicioSolicitado  = PagoService.getPagoArgs(); 
			/*
				Both of these watchers are to manage Show/hide of online payme (panel), and offline Payment(window popup)
			*/
			$scope.$watchCollection(function(){
					let param =  PagoService.getPagoArgs();
					return param;
				},function(newV,oldV){
					if(newV.visible ===oldV){
						return;
					}
					if(newV.visible){
						$scope.showPagoOnline = true;
					}			
			});	
			$scope.$watch(function(){
					let param =  PagoService.getPagoArgs();
					return param.windowPagoOfflineVisible;
				},function(newV,oldV){
					if(newV===oldV){
						return false;
					}else{
						if(!angular.isUndefined(newV)){
							if(newV){//Window is open  so we hide pagoOnline
								$scope.showPagoOnline = false;		
							}else{
								$scope.showPagoOnline = true;		
							}
						}
					}
			});

			$this.loadProvinciaPagoOnClick = function() {
				if($scope.solicitudePayment.catalogProvinciasPago === undefined || $scope.solicitudePayment.catalogProvinciasPago.lenght === 0){
					CatalogueHandler.getRelatedCatalogue(CATALOGUE_NAMES.PROVINCIES, CATALOGUE_NAMES.COUNTRIES + "_id", CATALOGUE_NAMES.SPAIN_COUNTRY_CODE).success(function(data) {
						$log.info(data);
						$scope.solicitudePayment.catalogProvinciasPago = data.catalog;
					}).error(function(data, status) {
						$log.error(data);
					});
				}
			};

			$this.getJustifier = function() {
				try {
					$scope.paymentProcessing = true;
					$scope.formularioATratar.solicitudePayment = $scope.solicitudePayment;
					$log.debug("serviceId-->");
					$log.debug($scope.serviceId);
					var serviceId = $scope.serviceId;
					PagoOnline.requestJustifier($scope.formularioATratar, $scope.aux.formaPago.catalogId,serviceId).success(function(data) {
						$scope.justifierNumber = data.justifierNumber;
						$scope.signSignature = data.signData;
						$scope.showPaymenButton = true;
						$scope.paymentProcessing = false;
					}).error(function(data, status) {
						$scope.paymentProcessing = false;
						$this.processJustifierError();
					});
				} catch (err) {
					$scope.paymentProcessing = false;
					$this.processJustifierError();
					$log.error(err);
				}

			};

			$this.processJustifierError = function() {
				$scope.resultImage = POPUP_IMAGES.error;
				$scope.processMessage = "PAYMENT_JUSTIFIER_ERROR";
				$scope.showProcessResult = true;
			}

			$this.makePayment = function() {
				$this.initMiniApplet();
				$this.signAndPay();
			}

			$this.initMiniApplet = function() {
				MiniApplet.cargarAppAfirma(MINIAPPLET);
			}
			$this.taxQuantityChange = function($event){
				if($scope.solicitudePayment.payedTasas==0 || angular.isUndefined($scope.solicitudePayment.payedTasas) ){
					$scope.solicitudePayment.payedTasas = 1; 
				}
				$scope.solicitudePayment.monto = $scope.solicitudePayment.payedTasas * $scope.tasa.amount;				
				$scope.solicitudePayment.monto = $scope.solicitudePayment.monto.toFixed(2);
				$this.getAmountParsed($scope.solicitudePayment.monto.toString());
			};
			$this.getAmountParsed	 = function(amount){
				let amountValue = "";
				let amountDecimal = "00";
				if(amount.indexOf(".") > -1){
					var amountSplited = amount.split(".");
					amountValue = amountSplited[0];
					amountDecimal = amountSplited[1];
				}else{
					amountValue = amount;
				}
				$scope.solicitudePayment.amount = amountValue;
				$scope.solicitudePayment.amountDecimal = amountDecimal;
			}

			$this.signAndPay = function() {
				try {
					$scope.paymentProcessing = true;
					MiniApplet.sign(base64.encode($scope.signSignature), "SHA256withRSA", "CADES", $this.signExtraParams(), function(signatureB64) {
						$scope.paymentProcessing = false;
						$this.executeOnlinePayment(signatureB64);
					}, function(type, message) {
						$scope.paymentProcessing = false;
						$this.processSignatureError(message);
					});
				} catch (err) {
					$scope.paymentProcessing = false;
					$this.processSignatureError(err);
					$log.error(err);
				}
			}

			$this.processSignatureError = function(message) {
				$scope.resultImage = POPUP_IMAGES.error;
				$scope.processMessage = message;
				$scope.showProcessResult = true;
			}

			$this.signExtraParams = function() {
				var params = "";
				var paramsObject = {
					mode : "explicit",
					policyIdentifier : "urn:oid:2.16.724.1.3.1.1.2.1.8",
					expPolicy : "FirmaAGE",
					policyQualifier : "http://administracionelectronica.gob.es/es/ctt/politicafirma/politica_firma_AGE_v1_8.pdf",
					policyIdentifierHash : "7SxX3erFuH31TvAw9LZ70N7p1vA="
				};
				var first = true;
				for ( var p in paramsObject) {
					var nextLine = "";
					if (!first) {
						nextLine = "\n";

					} else {
						first = false;
					}
					if (paramsObject.hasOwnProperty(p)) {
						params += nextLine + p + "=" + paramsObject[p];
					}

				}
				$log.debug("params:" + params);
				return params;

			};

			$this.executeOnlinePayment = function(signed) {
				$scope.paymentProcessing = true;
				$log.debug("serviceId en payment-->");
				$log.debug($scope.serviceId);
				var serviceId = $scope.serviceId;
				var justifierData = {
					justifier : $scope.justifierNumber,
					signedRaw : $scope.signSignature,
					signed : signed
				};
				PagoOnline.makePayment($scope.formularioATratar, $scope.aux.formaPago.catalogId, justifierData,serviceId).success(function(data) {
					$scope.nrc = data.nrc;
					$scope.paymentProcessing = false;
					$scope.pagOnlineDone = true
					$this.processPaymentSuccess();
				}).error(function(data, status) {
					$scope.paymentProcessing = false;
					$scope.pagOnlineDone = false
					$this.processPaymentError(data);
				});
			};

			$this.processPaymentSuccess = function() {
				$scope.resultImage = POPUP_IMAGES.ok;
				$scope.processMessage = "PAYMENT_SUCCESS";
				$scope.showProcessResult = true;
			}

			$this.processPaymentError = function(data) {
				$scope.resultImage = POPUP_IMAGES.error;
				if (data.description !== undefined) {
					$scope.processMessage = data.description;
				} else {
					$scope.processMessage = "PAYMENT_GENERIC_ERROR";
				}
				$scope.processMessage = data.description;
				$scope.showProcessResult = true;
			}

			$this.generatePaymentPDF = function() {
				$scope.paymentProcessing = true;
				JustificantePagoService.getJustificantePago($scope.formularioATratar.identificadorPeticion, $this.processPaymentPDFSuccess, $this.processPaymentPDFError,$scope.serviceId);
				$log.debug($scope);
			};

			$this.processPaymentPDFSuccess = function() {
				$scope.paymentProcessing = false;
			}

			$this.processPaymentPDFError = function() {
				$scope.resultImage = POPUP_IMAGES.error;
				$scope.processMessage = "PAYMENT_PDF_ERROR";
				$scope.paymentProcessing = false;
				$scope.showProcessResult = true;
			}

			$this.verObservaciones = function() {
				$scope.observacionesLeidas = true;
			};

			$this.resetInstructions = function() {
				$scope.observacionesLeidas = false;
				$scope.observacionesAceptadas = false;
			};

			$this.loadMunicipioOnChangePago = function() {
				if($scope.solicitudePayment.idPayment.location.provincia !== undefined){
					var provincia = $scope.solicitudePayment.idPayment.location.provincia.catalogId;
					if (provincia !== undefined) {
						CatalogueHandler.getRelatedCatalogue(CATALOGUE_NAMES.MUNICIPALITY, CATALOGUE_NAMES.PROVINCIES + "_id", provincia).success(function(data) {
							$log.info(data);
							$scope.solicitudePayment.catalogMunicipiosPago = data.catalog;
						}).error(function(data, status) {
							$log.error(data);
						});
					}
				}
			};

			$this.verInstruccionesPago = function() {
				$log.debug("instrucciones pago");
				$log.debug($scope);
				$log.debug($scope.popupParams);
				$this.verObservaciones();
				InstruccionesPago.getInstruccionesPago($scope.popupParams.idSolicitudeType).success(function(data) {
					FileDtoDownload.decryptAndDownloadFile(data.base64, data.catalogValue);

				}).error(function(data, status) {

					Messages.setErrorMessageFromResponse(data, status);
				});
			};

			$this.clearOtherPaymentData = function() {
				$scope.solicitudePayment.payedTasas =  1 ; 
				$scope.solicitudePayment.dataPayment.countryCode = "";
				$scope.solicitudePayment.dataPayment.controlDigit = "";
				$scope.solicitudePayment.dataPayment.entity = "";
				$scope.solicitudePayment.dataPayment.office = "";
				$scope.solicitudePayment.dataPayment.dc = "";
				$scope.solicitudePayment.dataPayment.account = "";
				$scope.solicitudePayment.dataPayment.nrc = "";
				$scope.solicitudePayment.dataPayment.cardNumber = "";
				$scope.solicitudePayment.dataPayment.expirationMonth = "";
				$scope.solicitudePayment.dataPayment.expirationYear = "";
				$scope.showProcessResult = false;
				if ($scope.aux.formaPago !== undefined) {
					$this.loadBanksForEpago();
				} else {
					$this.clearBanksForEpago();
				}

			};

			$this.clearBanksForEpago = function() {
				$scope.bankCatalogFormaPago = [];
				$scope.solicitudePayment.bankCode = "";
			}

			$this.loadBanksForEpago = function() {
				$this.clearBanksForEpago();
				$scope.bankCatalogFormaPago = CatalogueHandler.getBanksListForOnlinePayment($scope.aux.formaPago.catalogId);
				$scope.solicitudePayment.bankCode = "";
			}

//			$this.clearPopupWindow = function() {
//				$this.clearOtherPaymentData();
//				$scope.paymentProcessing = false;
//				$scope.bankCatalogFormaPago = [];
//				$scope.pagOnlineDone = false;
//				$scope.showProcessResult = false;
//				$scope.showPaymenButton = false
//				$scope.justifierNumber = "";
//				$scope.nrc = "";
//				$scope.solicitudePayment.idPayment.name = "";
//				$scope.solicitudePayment.idPayment.lastName = "";
//				$scope.solicitudePayment.idPayment.secondLastName = "";
//				$scope.solicitudePayment.idPayment.identificationNumber = "";
//				$scope.solicitudePayment.idPayment.address = {};
//				$scope.signSignature = "";
//				$scope.observacionesLeidas = false;
//				$scope.observacionesAceptadas = false;
//				PagoService.hidePagoPanel();
//				$log.debug("clear");
//
//			}

			$this.validatePaymentIdentification = function() {
				var paymentIdentification = $scope.identification.identificationPaymentForm;

				$this.activateAcordeonAutoLiqOnValid(paymentIdentification.$invalid);
			}

			$this.activateAcordeonAutoLiqOnValid = function(isValidPaymentIdentification) {
				$scope.accordionPaymentCtrl.disableAutoliquidacion = isValidPaymentIdentification;
			}
			
			$this.closeIdentificationOpenLiquidacion = function(){
				$scope.accordionPaymentCtrl.accordionIdentificacion = true;
				$scope.accordionPaymentCtrl.accordionAutoliquidacion = true;
			}
			
			$scope.accordionPaymentCtrl = {
				accordionIdentificacion : true,
				accordionAutoliquidacion : true,
				accordionTodos : true,
				disableAutoliquidacion : false
			};

			$scope.resetFields = $this.resetFields;
			$scope.generatePaymentPDF = $this.generatePaymentPDF;
			$scope.clearOtherPaymentData = $this.clearOtherPaymentData;
			$scope.verInstruccionesPago = $this.verInstruccionesPago;
			$scope.onClickProvinciaPago = $this.onClickProvinciaPago;
			$scope.onClickProvinciaPago = $this.onClickProvinciaPago;
			$scope.loadMunicipioPago = $this.loadMunicipioOnChangePago;
			$scope.taxQuantityChange = $this.taxQuantityChange;

			$scope.verObservaciones = $this.verObservaciones;

			$scope.efectivo = PAYMENT_TYPE.EFECTIVO;
			$scope.adeudo_cuenta = PAYMENT_TYPE.ADEUDO_CUENTA;
			$scope.online_tarjeta = PAYMENT_TYPE.ONLINE_TARJETA;
			$scope.online_cuenta = PAYMENT_TYPE.ONLINE_CUENTA;
			$scope.images = IMAGES_URL;
			$scope.makePayment = $this.makePayment;
			$scope.getJustifier = $this.getJustifier;

			$scope.paymentProcessing = false;
			$scope.bankCatalogFormaPago = [];
			$scope.pagOnlineDone = false;
			$scope.showProcessResult = false;
			$scope.showPaymenButton = false
			$scope.justifierNumber = "";
			$scope.nrc = "";
			$scope.signSignature = "";
			$scope.creditCardYears = YEARS_TWO_DIGITS_SELECT;
			$scope.creditCardMonths = MONTHS_TWO_DIGITS_SELECT;

			$scope.isPaid = $this.isPaid;
			$scope.validatePaymentIdentification = $this.validatePaymentIdentification;
			$scope.observacionesLeidas = false;
			$scope.observacionesAceptadas = false;
			$scope.onClickProvinciaPagoPayment = $this.onClickProvinciaPago;
			$scope.loadProvinciaPagoOnClick = $this.loadProvinciaPagoOnClick;
			$scope.identification = {};
			$scope.autoliq = {};
//			$scope.clearPopupWindow = $this.clearPopupWindow;
			$scope.disableOnlinePayment =DISABLE_PAYMENTONLINE;
			$scope.accordionPaymentCtrl.disableAutoliquidacion = false;
		});
