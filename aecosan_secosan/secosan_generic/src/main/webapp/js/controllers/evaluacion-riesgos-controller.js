angular.module('secosan').controller(
		'EvaluacionRiesgosController',
		function($scope, $http, $location, $log, $window, $sce, $routeParams,$timeout, HeaderTitle, Messages, NgTableParams, ModalService, ModalParamsService, EvaluacionRiesgos, Spinner, FormModelService,
				ServiciosPagoService, Instrucciones, IdentificadorPeticion, Solicitude, FileDtoDownload, ValidationUtilsService, LocationCombosService, UtilService, CatalogueLocationHandler,
				ImpresoFirmaService, SetDataFromCatalogService, CheckSolicitudeEditabilityService, PagoService, ERR_MSG, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, VALIDATION_PATTERN,
				SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL, TASA, STATUS_CODES,TABS_CONFIGURATION,TEMPLATE_PATHS) {

			var $this = this;

			var idToEdit = $routeParams.id;		

			var viewMode = $routeParams.v;

			$scope.idToEdit   =  idToEdit;

			$this.modelo = FormModelService.getEvaluacionriesgosModel();
 
			SetDataFromCatalogService.getCountryCatalogue($scope);
			SetDataFromCatalogService.getPaymentTypeCatalogue($scope);

            $this.initializeTabs = function(){
                // $scope.tabCtrl = angular.copy(TABS_CONFIGURATION.EVALUACION_RIESGOS);
                $timeout(function(){
                    let tab = $scope.tabCtrl.getTabByID($scope.tabCtrl,"tab-pagotasas");
                    if(tab){
                        $scope.tabCtrl.closeTab(tab);
                    }
                },1000)

            };

			$this.initializeNewSolicitude = function() {  $scope.auxSolicitante = {}; $scope.auxRepresentante = {};  $scope.auxContacto = {};                $scope.auxLugarOMedio = {};
                $this.initializeTabs();
				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				}

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};
				$scope.auxPago = {};
				$scope.auxServicio = {};

				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.EVALUACION_RIESGOS).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};

			$this.loadFormToEdit = function(data) {
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};

                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};
				$scope.auxPago = {};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				$scope.auxServicio = {
					sujetoTasa : {},
					servicioSolicitado : {}
				};

				if ($scope.formularioATratar.payTypeService.idPayType !== "") {
					$scope.auxServicio.sujetoTasa.catalogId = $scope.formularioATratar.payTypeService.idPayType;
					$this.loadService($scope.auxServicio.sujetoTasa.catalogId, true);
				}
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};
			

			
			$this.openTabPagos  = function (){				 
				$scope.tabCtrl.addTabAfterTabID({
					id:"tab-pagotasas",
					autoload:true,
					onlyEdit : true,
					title : 'PAGO_TASAS',
					resolve: {
						servicioSolicitado : $scope.auxServicio.servicioSolicitado
					},
					head: {
						icon: 'icon-note',
						text: 'PAGO_TASAS',
						title : "PAGO_TASAS"
					},
					templateUrl : TEMPLATE_PATHS.DIRECTIVES_RIESGOS+"pago-tasa-en-linea.html",
				},"tab-evaluacionriesgosservice");
				$scope.triggerValidations();
			};

			
			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$this.loadService = function(id, positioning) {
				Spinner.showSpinner();
				if (id !== undefined) {
					ServiciosPagoService.getServiciosByPago(id).success(function(data) {
						$scope.catalogTipoServicio = {};
						$scope.catalogTipoServicio = data;

						if (positioning) {
							$scope.auxServicio.servicioSolicitado.id = $scope.formularioATratar.payTypeService.idService;
						} else {
							$scope.auxServicio.servicioSolicitado = $scope.catalogTipoServicio[-1];
						}

						$log.debug("OK getting catalog from ServiciosPagoService");
						Spinner.hideSpinner();
						let tab = $scope.tabCtrl.getTabByID($scope.tabCtrl,"tab-pagotasas");
						if($scope.auxServicio.servicioSolicitado != undefined && $scope.auxServicio.sujetoTasa.catalogId == $scope.payment_tasa_code && $scope.auxServicio.servicioSolicitado.id > 0){					
								PagoService.setServicioSolicitado($scope.auxServicio.servicioSolicitado);
								// PagoService.setIdSolicitudeType($scope.tipoSolicitudId);
								if(!tab){
									$scope.openTabPagos();
								}
								$timeout(function () {
                    					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl, "tab-pagotasas");
                    			}, 300);

						}else{
							if(tab) {
                                $scope.tabCtrl.closeTab(tab);
                            }
							
						}
					}).error(function(data, status) {
						$log.error("Error getting catalog from ServiciosPagoService" + status);
						Messages.setErrorMessageFromResponse(data, status);
						Spinner.hideSpinner();
					});
				} else {
					$scope.catalogTipoServicio = {};
					Spinner.hideSpinner();
					//$scope.checkAndShowPagoTab();
				}
			};

            // control de tabs
            $scope.tabCtrl = angular.copy(TABS_CONFIGURATION.EVALUACION_RIESGOS);

			$scope.saveForm = function() {
				$scope.editMode = false;
				$log.debug($scope);
			}

			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;

				LocationCombosService.setLocationDataToScope($scope);

				if (UtilService.checkNotNullNorUndefined($scope.auxServicio.sujetoTasa)) {
					$scope.formularioATratar.payTypeService.idPayType = $scope.auxServicio.sujetoTasa.catalogId;
				} else {
					$scope.formularioATratar.payTypeService.idPayType = "";
				}

				if (UtilService.checkNotNullNorUndefined($scope.auxServicio.servicioSolicitado)) {
					$scope.formularioATratar.payTypeService.idService = $scope.auxServicio.servicioSolicitado.id;
				} else {
					$scope.formularioATratar.payTypeService.idService = "";
				}

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					requiredDocumentAttached = ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion);
				} else if (senderMode === SENDER_MODE.DRAFT) {
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				$log.info("enviar:> " + $scope.formularioATratar.solicitante.name);

				if (ValidationUtilsService.checkFormValid($scope,true) ) {
					if ((requiredDocumentAttached && senderMode === SENDER_MODE.SUBMIT) || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
							EvaluacionRiesgos.updateRiesgo($scope.formularioATratar).success(function() {
								$log.debug("updateRiesgo OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("updateRiesgo KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						} else {
							EvaluacionRiesgos.addRiesgo($scope.formularioATratar).success(function() {
								$log.debug("addRiesgo OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addRiesgo KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-evaluacionriesgosdocumentacion",$scope,'SIGNED_DOCUMENT');
						Spinner.hideSpinner();
					}
				} else {
					Spinner.hideSpinner();
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-evaluacionriesgosdocumentacion",$scope,'SIGNED_DOCUMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
				}
			};
			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope); 
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-evaluacionriesgosdocumentacion",$scope,'SIGNED_DOCUMENT');
				}
			
			}

			$scope.checkAndShowPagoTab = function (){
				//if($scope.auxServicio.servicioSolicitado != undefined){return;}
				// CHECK AND ADD TAB IN DRAFT MODE
				let tab = $scope.tabCtrl.getTabByID($scope.tabCtrl,"tab-pagotasas");
				if(!viewMode && $scope.auxServicio.servicioSolicitado != undefined && $scope.auxServicio.sujetoTasa.catalogId == $scope.payment_tasa_code && $scope.auxServicio.servicioSolicitado.id.length > 0){					
					PagoService.setServicioSolicitado($scope.auxServicio.servicioSolicitado);
					PagoService.setIdSolicitudeType($scope.tipoSolicitudId);
					if(!tab){
						$scope.openTabPagos();
					}
					$timeout(function () {
                    	$scope.tabCtrl.setTabFocusByID($scope.tabCtrl, "tab-pagotasas");
                    }, 300);

				}				
			}
			
			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.EVALUACION_RIESGOS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.payment_tasa_code = CATALOGUE_NAMES.PAYMENT_TASA_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.paisPago = CatalogueLocationHandler.domObjectName.paisPago;
			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.provinciaPago = CatalogueLocationHandler.domObjectName.provinciaPago;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;

			$scope.loadService = $this.loadService;
			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.RIESGOS;
			$scope.showPagoPanel = $this.showPagoPanel;
			$scope.tasatipo = TASA.RIESGOS;

			$scope.openTabPagos = $this.openTabPagos;
		});