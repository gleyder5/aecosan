angular.module('secosanGeneric').controller(
		'documentationManagerProcedimientoGenericoController',
		function($scope, $http, $log, $location, Messages, CatalogueHandler, ModalService, Spinner, ModalParamsService, DocumentacionService, FileDtoDownload, SetDataFromCatalogService,base64,
				CATALOGUE_NAMES, FILES_CONSTANTS,DOCUMENT_UPLOAD_MAGICNUMBER_REGEX) {
			var $this = this;

			$this.resetDocumentos = function() {
				$scope.dataOption.selectedOption.id = "0";
				$scope.file = {};
			};

			$this.dataOption = {
				repeatSelect : null,
				availableOptions : [ {
					id : '0',
					catalogValue : 'Elija una opción'
				} ],
				// Opcion por defecto para el desplegable.
				selectedOption : {
					id : '0',
					catalogValue : 'Elija una opción'
				}
			};

			$scope.showModal = false;

			$this.composeObjectForSend = function() {
				this.obj = {
                    docType: $this.dataOption.selectedOption.id,
                    fileName: $scope.file.filename,
                    docTypeDesc: $scope.tipoDocumento,
                    base64: $scope.file.base64
				};
				if($scope.tipoDocumento!="") {
					this.obj.docType=null;
                    this.obj.docTypeDesc  =  $scope.tipoDocumento;
                }
                return this.obj;
			};

			$this.checkValidSizeAndFormat = function(file) {
				if(file!==undefined){
                    return file.size < (FILES_CONSTANTS.MAXSIZE * 1000) && file.type === FILES_CONSTANTS.FILE_TYPE;
				}
				return $scope.file.filesize < (FILES_CONSTANTS.MAXSIZE * 1000) && $scope.file.filetype === FILES_CONSTANTS.FILE_TYPE;
			}

			$scope.maxfilesize = FILES_CONSTANTS.MAXSIZE;
			$scope.minfilesize = FILES_CONSTANTS.MINSIZE;
			$scope.fileType = FILES_CONSTANTS.FILE_TYPE;

			$this.checkRequiredFieldsFilled = function() {
                return $scope.dataOption.selectedOption.id != "0" && $scope.dataOption.selectedOption.catalogId != "0" && $scope.file !== undefined && $scope.file.length !== 0 && $scope.file.filename !== undefined;
			};
			
			$this.validateMagicNumber = function(){
				var fileDecoded = base64.decode($scope.file.base64);
				return fileDecoded.match(DOCUMENT_UPLOAD_MAGICNUMBER_REGEX.expected);
			};

			$this.onLoad  = function (e, reader, file, fileList, fileOjects, fileObj){
                if (!$this.checkValidSizeAndFormat(file)) {
                    ModalService.showModalWindow(ModalParamsService.formatFileError());
                }
            };

			$this.addFile = function() {
				if ($this.checkRequiredFieldsFilled()) {
					if ($this.checkValidSizeAndFormat() && $this.validateMagicNumber()) {
						var newFileSend = $this.composeObjectForSend();
						$scope.formularioATratar.documentacion.push(newFileSend);
					} else if(!$this.checkValidSizeAndFormat()) {
						ModalService.showModalWindow(ModalParamsService.formatFileError());
					}else if(!$this.validateMagicNumber()){
						ModalService.showModalWindow(ModalParamsService.formatFileErrorMagicNumber());
					}
				} else {
					ModalService.showModalWindow(ModalParamsService.genericFilesError());
				}
				$this.resetDocumentos();
			};

			SetDataFromCatalogService.getDocumentTypeCatalog($scope, $scope.formID);

			$this.eliminarDocumento = function(index, idBBDD) {
				ModalService.showModalWindow(ModalParamsService.confirmDeleteDocument(idBBDD, index, $scope.formularioATratar.documentacion, $scope));
			};

			$this.downloadDocument = function(index) {
				Spinner.showSpinner();
				var document = $scope.formularioATratar.documentacion[index];

				DocumentacionService.getDocumentacionById(document.id).success(function(data) {
					FileDtoDownload.decryptAndDownloadFile(data.base64, data.fileName);
					$log.debug("Download documentacion success");
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Download documentacion error");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};

			$scope.addFile = $this.addFile;
			$scope.onLoad = $this.onLoad;

			$scope.dataOption = $this.dataOption;
			$scope.eliminarDocumento = $this.eliminarDocumento;
			$scope.downloadDocument = $this.downloadDocument;
		});
