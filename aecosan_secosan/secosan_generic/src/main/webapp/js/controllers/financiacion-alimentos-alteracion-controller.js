angular.module('secosan').controller(
		'FinanciacionAlimentosAlteracionController',
		function($scope, $http, $log, $window, $location, $sce, $routeParams, HeaderTitle, Messages, NgTableParams, ModalService, ModalParamsService, FinanciacionAlimentos, Spinner, FormModelService,
				Instrucciones, IdentificadorPeticion, FileDtoDownload, CatalogueLocationHandler, Solicitude, ValidationUtilsService, LocationCombosService, PagoService, SetDataFromCatalogService,
				ImpresoFirmaService, RegistryNumberFormatService, CheckSolicitudeEditabilityService, AttachablesFilesService, CATALOGUE_NAMES, FORM_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG,
				VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES, REDIRECT_URL, ATTACHABLE_FILE_NAME, TASA, STATUS_CODES,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getFinanciacionAlimentosAlteracionModel();

			SetDataFromCatalogService.getCountryCatalogue($scope);
			SetDataFromCatalogService.getAdminModeCatalogue($scope);			

			$this.initializeNewSolicitude = function() {
				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				}

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};
				$scope.auxPago = {};

				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.FINA_ALIM_ALTERACION).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};

			$this.loadFormToEdit = function(data) {
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				$scope.instruccionesLeidas = true;

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};

                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};
                // $scope.auxPago = {};


				$scope.auxPago = {
					pais : {},
					provincia : {},
					municipio : {}
				};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);

				var fiber = $scope.formularioATratar.fiber.fiber;
				if (fiber) {
					$scope.aux.fiber = "true";
				} else if (fiber === "") {
					$scope.aux.fiber = "";
				} else {
					$scope.aux.fiber = "false";
				}

				

				RegistryNumberFormatService.putCompanyRegisterNumberInView($scope, $scope.formularioATratar.companyRegisterNumber);
				RegistryNumberFormatService.putProductReferenceNumberInView($scope, $scope.formularioATratar.productReferenceNumber);
			};

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.getAttachedFile = function() {
				var fileToGet = ATTACHABLE_FILE_NAME.FICHA_TECNICA_ALTERACION;
				Spinner.showSpinner();
				$log.debug("getAttachedFile: " + fileToGet);
				AttachablesFilesService.getFile(fileToGet);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.resetFiberValues = function() {
				if ($scope.aux.fiber !== 'true') {
					$scope.formularioATratar.fiber.fiberType = "";
					$scope.formularioATratar.fiber.fiberQuantity = "";
				}
			}

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.companyRegisterNumberAux = {
				numRegEmp1 : "",
				numRegEmp2 : "",
				numRegEmp3 : ""
			}
			$scope.productReferenceNumber = {
				numRegPro1 : "",
				numRegPro2 : "",
				numRegPro3 : "",
				numRegPro4 : ""
			}

			$scope.aux = {
				administrationMode : {}
			}

			

			// control de tabs
			$scope.tabCtrl  = angular.copy(TABS_CONFIGURATION.FINA_ALIM_ALTERACION);
			$scope.$watch("instruccionesLeidas",function(newV,oldV){
				if(newV===oldV){
					return false; 
				}
				$scope.tabCtrl.setTabFocusByIndex($scope.tabCtrl,0);
			});
			

			$scope.saveForm = function() {
				$scope.editMode = false;			
			}

			$scope.getPDF = function() {
				$scope.formularioATratar.productReferenceNumber = RegistryNumberFormatService.composeProductReferenceNumber($scope.productReferenceNumber.numRegPro1,
						$scope.productReferenceNumber.numRegPro2, $scope.productReferenceNumber.numRegPro3, $scope.productReferenceNumber.numRegPro4);
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;

				LocationCombosService.setLocationDataToScope($scope);

				$scope.formularioATratar.fiber.fiber = $scope.aux.fiber;
				$scope.formularioATratar.companyRegisterNumber = RegistryNumberFormatService.composeCompanyRegisterNumber($scope.companyRegisterNumberAux.numRegEmp1,
						$scope.companyRegisterNumberAux.numRegEmp2, $scope.companyRegisterNumberAux.numRegEmp3);
				$scope.formularioATratar.productReferenceNumber = RegistryNumberFormatService.composeProductReferenceNumber($scope.productReferenceNumber.numRegPro1,
						$scope.productReferenceNumber.numRegPro2, $scope.productReferenceNumber.numRegPro3, $scope.productReferenceNumber.numRegPro4);

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					requiredDocumentAttached = ValidationUtilsService.isAnexoIIIDocumentsAttached($scope.formularioATratar.documentacion);
				} else if (senderMode === SENDER_MODE.DRAFT) {
					// TODO: check default behaviour
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				$log.info("enviar:> " + $scope.formularioATratar.solicitante.name);

				if (ValidationUtilsService.checkFormValid($scope,true) ) {
					if ((requiredDocumentAttached && senderMode === SENDER_MODE.SUBMIT) || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
							FinanciacionAlimentos.updateAlteracionFinanciacionAlimentos($scope.formularioATratar).success(function() {
								$log.debug("updateAlteracionFinanciacionAlimentos OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("updateAlteracionFinanciacionAlimentos KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						} else {
							FinanciacionAlimentos.addAlteracionFinanciacionAlimentos($scope.formularioATratar).success(function() {
								$log.debug("addAlteracionFinanciacionAlimentos OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addAlteracionFinanciacionAlimentos KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENTS_FOR_ANEXO_III, "tab-finaalimalteraciondocumentacion",$scope,'DOCUMENTS_FOR_ANEXO_III');
						Spinner.hideSpinner();
					}
				} else {
					Spinner.hideSpinner();
					ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENTS_FOR_ANEXO_III, "tab-finaalimalteraciondocumentacion",$scope,'DOCUMENTS_FOR_ANEXO_III');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
				}
			};
			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope); 
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isAnexoIIIDocumentsAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.DOCUMENTS_FOR_ANEXO_III, "tab-finaalimalteraciondocumentacion",$scope,'DOCUMENTS_FOR_ANEXO_III');
				}
			
			}

			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.formularios = FORM_NAMES;
			$scope.formularioSeleccionado = FORM_NAMES.CESECOMERCIALIZACION;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.PAY =$sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.FINA_ALIM_ALTERACION;
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.ALTERACION_UME;

//			$scope.showPagoPanel = $this.showPagoPanel;
			$scope.getAttachedFile = $this.getAttachedFile;
			$scope.tasatipo = TASA.ALTERACION_UME;
		});