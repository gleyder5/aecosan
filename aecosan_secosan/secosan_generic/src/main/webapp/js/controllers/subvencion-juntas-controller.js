angular.module('secosan').controller(
		'SubvencionJuntasController',
		function($timeout, $scope,  $http, $location, $log, $window, $sce, $filter, $routeParams, HeaderTitle, Messages, NgTableParams, Spinner, SubvencionJuntas, Instrucciones, IdentificadorPeticion,
				DatetimeService, Solicitude, FileDtoDownload, UbicacionService, FormModelService, ModalService, ModalParamsService, CatalogueLocationHandler, LocationCombosService,
				SetDataFromCatalogService, CheckSolicitudeEditabilityService, ImpresoFirmaService, ValidationUtilsService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, ERR_MSG,
				VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL, STATUS_CODES,TABS_CONFIGURATION) {
			
			var $this = this;

			var idToEdit = $routeParams.id; 
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getSubvencionesJuntasModel();
            //Eliminar esto cuando se habilite el formulario, actualmente redirecciona a redsara
            // $this.URL = "https://registroelectronico.msssi.es/formulariosINC/IncFormSubvAACC.jsp";
            // $scope.$watch("instruccionesLeidas",function (newVal,oldVal) {
            //     if(newVal===oldVal){
            //         return;
            //     }
            //     if(newVal){
            //         $window.open($this.URL );
            //     }
            //
            // });

            SetDataFromCatalogService.getCountryCatalogue($scope);

			$scope.tabCtrl= angular.copy(TABS_CONFIGURATION.ASOCIACION_CONSUMIDORES);
		
			$this.initializeNewSolicitude = function() {
				
				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				};

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};
                $scope.auxLugarOMedio = {};

                // $scope.datePickerParams = DatetimeService.setInitialValues();
				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.SUBVENCIONES_JUNTAS).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};

			
			$this.loadFormToEdit = function(data) {

				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				// $scope.editMode = true;
				$scope.instruccionesLeidas = true;

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};
                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};
                $scope.auxLugarOMedio = {};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
                LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisLugarOMedio);

				// var solicitudeDate = new Date(data.solicitude.fechaBoe);
				// $scope.datePickerParams = DatetimeService.setDateTime(solicitudeDate);
				$log.debug("$scope.formularioATratar");
				$log.debug($scope.formularioATratar);
			};

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);

			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxFecha = {
				fechaIncidencia : "",
				horaIncidencia : ""
			};

		
 			
 			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope) 
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
				}
			
			}
			
			$scope.saveForm = function() {
				$scope.editMode = false;
			}
			
			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};
			
			
			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;

				LocationCombosService.setLocationDataToScope($scope);

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					// requiredDocumentAttached = ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion);

				} else if (senderMode === SENDER_MODE.DRAFT) {
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				
				if ( ValidationUtilsService.checkFormValid($scope,true) ) {
					
					if (senderMode === SENDER_MODE.SUBMIT || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {

							SubvencionJuntas.updateRegistro($scope.formularioATratar).success(function() {
								$log.debug("updateProcedimiento OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
							} else {
							SubvencionJuntas.addRegistro($scope.formularioATratar).success(function() {
								$log.debug("addRegistro OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addRegistro KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						Spinner.hideSpinner();
						ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
					}
				} else {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
					Spinner.hideSpinner();
				}
			};
			
			

			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.postal_code_pattern = VALIDATION_PATTERN.POSTAL_CODE;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.SUBVENCIONES_JUNTAS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.paisLugarOMedio = CatalogueLocationHandler.domObjectName.paisLugarOMedio;

			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.provinciaLugarOMedio = CatalogueLocationHandler.domObjectName.provinciaLugarOMedio;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;
			$scope.formID = FORM_ID_CODES.SUBVENCIONES_JUNTAS;

			$scope.gotoWelcome = $this.gotoWelcome;


		});