angular.module('secosanGeneric').controller(
		'documentationManagerBecasController',
		function($scope, $http, $log, $location, Messages, CatalogueHandler, ModalService, Spinner, ModalParamsService, DocumentacionService, FileDtoDownload, SetDataFromCatalogService,base64,
				CATALOGUE_NAMES, FILES_CONSTANTS,DOCUMENT_UPLOAD_MAGICNUMBER_REGEX) {
			var $this = this;

			$this.resetDocumentos = function() {
				$scope.dataOption.selectedOption.id = "0";
				$scope.file = {};
			};

			$this.fileTypeChanged = function(val){
				if(val.id==35){ ///Courses , validation  to allow zip files attached when courses is selected
					$scope.fileType  = FILES_CONSTANTS.ACCEPTS_ZIPS;
				}else{
					$scope.fileType =FILES_CONSTANTS.FILE_TYPE;
				}
			};

			$this.dataOption = {
				repeatSelect : null,
				availableOptions : [ {
					id : '0',
					catalogValue : 'Elija una opción'
				} ],
				// Opcion por defecto para el desplegable.
				selectedOption : {
					id : '0',
					catalogValue : 'Elija una opción'
				}
			};

			$scope.showModal = false;
            $this.composeObjectForSend = function() {
                return {
                    docType : $this.dataOption.selectedOption.id,
                    fileName : $scope.file.filename,
                    base64 : $scope.file.base64
                }
            }

            $this.checkValidSizeAndFormat = function(file) {
                if(file!==undefined){
                    return file.size < (FILES_CONSTANTS.MAXSIZE * 1000) && (file.type === FILES_CONSTANTS.FILE_TYPE || FILES_CONSTANTS.ACCEPTS_ZIPS.indexOf(file.type)!=-1);
                }
                return $scope.file.filesize < (FILES_CONSTANTS.MAXSIZE * 1000) && ($scope.file.filetype === FILES_CONSTANTS.FILE_TYPE || FILES_CONSTANTS.ACCEPTS_ZIPS.indexOf($scope.file.filetype)!=-1);
            };

			$scope.maxfilesize = FILES_CONSTANTS.MAXSIZE;
			$scope.minfilesize = FILES_CONSTANTS.MINSIZE;
			$scope.fileType = FILES_CONSTANTS.FILE_TYPE;
            $scope.accepts  = FILES_CONSTANTS.ACCEPTS_PDF;

            $this.checkRequiredFieldsFilled = function() {
                return $scope.dataOption.selectedOption.id != "0" && $scope.dataOption.selectedOption.catalogId != "0" && $scope.file !== undefined && $scope.file.length !== 0 && $scope.file.filename !== undefined;
			};
			
			$this.validateMagicNumber = function(){
				var fileDecoded = base64.decode($scope.file.base64);
				return fileDecoded.match(DOCUMENT_UPLOAD_MAGICNUMBER_REGEX.expected);
			};

			$this.onLoad  = function (e, reader, file, fileList, fileOjects, fileObj){
                if (!$this.checkValidSizeAndFormat(file)) {
                    ModalService.showModalWindow(ModalParamsService.formatFileError());
                }
            };

			$this.addFile = function() {
				if ($this.checkRequiredFieldsFilled()) {
					if ($this.checkValidSizeAndFormat()) {
						var newFileSend = $this.composeObjectForSend();
						$scope.formularioATratar.documentacion.push(newFileSend);
					} else if(!$this.checkValidSizeAndFormat()) {
						ModalService.showModalWindow(ModalParamsService.formatFileError());
					}else if(!$this.validateMagicNumber()){
						ModalService.showModalWindow(ModalParamsService.formatFileErrorMagicNumber());
					}
				} else {
					ModalService.showModalWindow(ModalParamsService.genericFilesError());
				}
				$this.resetDocumentos();
			};

			SetDataFromCatalogService.getDocumentTypeCatalog($scope, $scope.formID);

			$this.eliminarDocumento = function(index, idBBDD) {
				ModalService.showModalWindow(ModalParamsService.confirmDeleteDocument(idBBDD, index, $scope.formularioATratar.documentacion, $scope));
			};

			$this.downloadDocument = function(index) {
				Spinner.showSpinner();
				var document = $scope.formularioATratar.documentacion[index];

				DocumentacionService.getDocumentacionById(document.id).success(function(data) {
					FileDtoDownload.decryptAndDownloadFile(data.base64, data.fileName);
					$log.debug("Download documentacion success");
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Download documentacion error");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};
			$scope.getFileTypeString   = function () {
				if(Array.isArray($scope.fileType)){
					return $scope.fileType.join(",");
				}
                return $scope.fileType;
            };

			$scope.addFile = $this.addFile;
			$scope.onLoad = $this.onLoad;
			$scope.fileTypeChanged = $this.fileTypeChanged;

			$scope.dataOption = $this.dataOption;
			$scope.eliminarDocumento = $this.eliminarDocumento;
			$scope.downloadDocument = $this.downloadDocument;
		});
