angular.module('secosanGeneric').controller(
		'cambioContraseniaController',
		function($scope, $http, $log, $location, Messages, CatalogueHandler, ModalService, ModalParamsService, FormModelService, UserManagementService, AuthenticationUser, base64,
				CambioContraseniaService, IMAGES_URL, ERR_MSG, POPUP_IMAGES, CATALOGUE_NAMES) {

			var $this = this;
			

			$this.clearPopupWindow = function() {
				$scope.form.errroPassword = "";
				CambioContraseniaService.hideCambioContraseniaWindow();
			};
			$this.encryptData = function() {
				$scope.auxBase64.oldPassword = Base64.encode($scope.aux.oldPassword);
				$scope.auxBase64.newPassword = Base64.encode($scope.aux.newPassword);
			},
			
			$this.handleServerError = function(data){
				var codeErr = data.code;
				switch (codeErr) {
				case -1:
					$scope.form.errroPassword = ERR_MSG.OLD_EQUAL_NEW_ERROR;
					break;
				case -2:
					$scope.form.errroPassword = ERR_MSG.NEW_PASSWORD_FORMAT_ERROR;
					break;
				case -3:
					$scope.form.errroPassword = ERR_MSG.USER_NOT_FOUND_ERROR;
					break;
				case -4:
					$scope.form.errroPassword = ERR_MSG.OLD_PASSWORD_ERROR;
					break;
				case -10:
					$scope.form.errroPassword = ERR_MSG.GENERAL_ERROR;
					break;
				default:
					$scope.form.errroPassword = ERR_MSG.GENERAL_ERROR;
					break;
				}
			}
			
			$this.handlePromise = function(promise){
				promise.success(function() {
					$scope.aux = {};
					$scope.clearPopupWindow();

				}).error(function(data, status, headers, config) {
					$this.handleServerError(data);
				});
			}

			$this.changePassword = function() {
				$scope.form.errroPassword = "";
				$this.encryptData();
				var promise;
				var roleId = AuthenticationUser.getUserPayloadData("roleId");
				if (roleId == 2 || roleId ==3) {
					promise = UserManagementService.changePasswordAdmin($scope.auxBase64);

				} else {
					promise = UserManagementService.changePassword($scope.auxBase64)
				}
				$this.handlePromise(promise);
			}
			
			$scope.aux = {};
			$scope.auxBase64 = {};
			$scope.form = {};
			$scope.firstLogin = AuthenticationUser.getUserPayloadData("firstLogin");
			$scope.roleId = AuthenticationUser.getUserPayloadData("roleId");
			$scope.validateOldPassword = $this.validateOldPassword;
			$scope.changePassword = $this.changePassword;
			$scope.clearPopupWindow = $this.clearPopupWindow;
		});
