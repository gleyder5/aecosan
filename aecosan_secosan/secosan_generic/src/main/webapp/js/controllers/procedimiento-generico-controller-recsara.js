angular.module('secosan').controller(
		'ProcedimientoGenericoControllerRecSara',
		function($timeout, $scope,  $http, $location, $log, $window, $sce, $filter, $routeParams, HeaderTitle, Messages, NgTableParams, Spinner, ProcedimientoGeneral, Instrucciones, IdentificadorPeticion,
				DatetimeService, Solicitude, FileDtoDownload, UbicacionService, FormModelService, ModalService, ModalParamsService, CatalogueLocationHandler, LocationCombosService,
				SetDataFromCatalogService, CheckSolicitudeEditabilityService, ImpresoFirmaService, ValidationUtilsService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, ERR_MSG,
				VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL, STATUS_CODES,TABS_CONFIGURATION,ProcedureDescriptionService) {
			
			var $this = this;
			$this.OFICINA_REGISTRAL  = "https://rec.redsara.es/registro/action/are/acceso.do";
			$this.modelo = FormModelService.getProcedimientoGeneralModel();
			$scope.tabCtrl= angular.copy(TABS_CONFIGURATION.PROCEDIMIENTO_GENERICO);
            $scope.formularioATratar = $this.modelo;

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.$watch("instruccionesLeidas",function (newVal,oldVal) {
				if(newVal===oldVal){
					return;
				}
				if(newVal){
					$window.open($this.OFICINA_REGISTRAL );
				}

			});
            IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.PROCEDIMIENTO_GENERAL).success(function(data) {
                $log.debug("Identificador: " + data.identificationRequest);
                $scope.formularioATratar.identificadorPeticion = data.identificationRequest;
                Spinner.hideSpinner();
            }).error(function(data, status) {
                $log.error("Error getIdentificador");
                Messages.setErrorMessageFromResponse(data, status);
                Spinner.hideSpinner();
            });


			$scope.verInstrucciones = $this.verInstrucciones;


			$scope.instruccionesVistas = false;

			$scope.tipoSolicitudId = SOLICITUDE_CODES.PROCEDIMIENTO_GENERAL;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.PROCEDIMIENTO_GENERAL;

		});