angular.module('secosan').controller(
		'BecasConcejoConsumidoresController',
		function($timeout, $scope,  $http, $location, $log, $window, $sce, $filter, $routeParams, HeaderTitle, Messages, NgTableParams, Spinner, Becas, Instrucciones, IdentificadorPeticion,
				DatetimeService, Solicitude, FileDtoDownload, UbicacionService, FormModelService, ModalService, ModalParamsService, CatalogueLocationHandler, LocationCombosService,
				SetDataFromCatalogService, CheckSolicitudeEditabilityService, ImpresoFirmaService, ValidationUtilsService, CATALOGUE_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, ERR_MSG,
				VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, AREA_CODES, REDIRECT_URL, STATUS_CODES,TABS_CONFIGURATION) {

			var $this = this;
			// Eliminar esto cuando se habilite el formulario, actualmente redirecciona a redsara
            // $this.URL  = "https://rec.redsara.es/registro/action/are/acceso.do";
            // $scope.$watch("instruccionesLeidas",function (newVal,oldVal) {
             //    if(newVal===oldVal){
             //        return;
             //    }
             //    if(newVal){
             //        $window.open($this.URL );
             //    }
            // });
            var idToEdit = $routeParams.id;
            var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getBecasModel();
            $scope.auxSolicitante = {};
            $scope.auxRepresentante = {};
            $scope.auxContacto = {};

			SetDataFromCatalogService.getCountryCatalogue($scope);

			$scope.tabCtrl= angular.copy(TABS_CONFIGURATION.BECAS_CONSUMIDORES);
		
			$this.initializeNewSolicitude = function() {
				
				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				};



                $scope.datePickerParams = DatetimeService.setInitialValues();
                // var solicitudeDate = new Date();
                $scope.datePickerParams = DatetimeService.setDateTime(null);
				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.BECAS).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			};
			$this.loadFormToEdit = function(data) {

				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				// $scope.editMode = true;

				$scope.instruccionesLeidas = true;
                Becas.getIdiomasBecasList(idToEdit).success(function (resp) {
                    $scope.langs.forEach(function (t) {
                        resp.forEach(function (v) {
                            if(v.nombre==t.nombre){
                                t.puntos = v.puntos;
                            }
                        });
                    });
                    if(resp.length>3){
                        $scope.otherLangs = resp.slice(3,resp.length);
                        $scope.formularioATratar.languages  = $scope.langs ;
                    }
                });

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};

                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);


				if(data.solicitude.solicitante.fechaNac) {
                    var solicitudeDate = new Date(data.solicitude.solicitante.fechaNac);
                    $scope.datePickerParams = DatetimeService.setDateTime(solicitudeDate);
                }else{
                    $scope.datePickerParams = DatetimeService.setDateTime(null);
				}
				$log.debug("$scope.formularioATratar");
				$log.debug($scope.formularioATratar);
			};

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE) ||($scope.formularioATratar.status === STATUS_CODES.REQ_SUBSANAR_10_DIAS);

					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);

			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			$scope.auxFecha = {
				fechaIncidencia : "",
				horaIncidencia : ""
			};

		
			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope);
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion-becas",$scope,'SIGNED_DOCUMENT');
				}

                if ($scope.formularioATratar != undefined && !ValidationUtilsService.isCVDocumentAttached($scope.formularioATratar.documentacion)) {
                    ValidationUtilsService.addValidMessage(ERR_MSG.CV, "tab-documentacion-becas",$scope,'CV');
                }
                if ($scope.formularioATratar != undefined && !ValidationUtilsService.isCollegeDegreeDocumentAttached($scope.formularioATratar.documentacion)) {
                    ValidationUtilsService.addValidMessage(ERR_MSG.COLLEGE_DEGREE, "tab-documentacion-becas",$scope,'COLLEGE_DEGREE');
                }
                if ($scope.formularioATratar != undefined && !ValidationUtilsService.isDNIDocumentAttached($scope.formularioATratar.documentacion)) {
                    ValidationUtilsService.addValidMessage(ERR_MSG.DNI, "tab-documentacion-becas",$scope,'DNI');
                }
			
			};
			
			$scope.saveForm = function() {
				$scope.editMode = false;
			};
			
			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};
			
			
			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();

				LocationCombosService.setLocationDataToScope($scope);
                if($scope.datePickerParams.auxFecha.fechaIncidencia!=null){
                    $scope.auxSolicitante.fechaNac = DatetimeService.transformDate2Long($scope.datePickerParams.auxFecha.fechaIncidencia);
                }
                if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;


				} else if (senderMode === SENDER_MODE.DRAFT) {
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}
                var i = $scope.otherLangs.length;
                while (i--) {
                    if($scope.otherLangs[i].nombre==undefined ||$scope.otherLangs[i].puntos == 0){
                        $scope.otherLangs.splice(i,1);
                 	   }else {
                        if ($scope.otherLangs[i].nombre.length < 2) {
                            $scope.otherLangs.splice(i, 1);
                        }
                    }

                }
				
				if ( ValidationUtilsService.checkFormValid($scope,true) ) {
					
					if (senderMode === SENDER_MODE.SUBMIT || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
                            if($scope.auxSolicitante.fechaNac) {
                                $scope.formularioATratar.solicitante.fechaNac = $scope.auxSolicitante.fechaNac;
                            }

                            $scope.formularioATratar.languages  =  $scope.langs.concat($scope.otherLangs);
                            Becas.updateRegistro($scope.formularioATratar).success(function() {
                                $log.debug("updateRegistro  OK");
                                Spinner.hideSpinner();
                                ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
                            }).error(function(data, status, headers, config) {
                                $scope.hasdata = false;
                                Spinner.hideSpinner();
                                ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
                            });

						} else {

                            if($scope.auxSolicitante.fechaNac) {
                                $scope.formularioATratar.solicitante.fechaNac = $scope.auxSolicitante.fechaNac;
                            }
                            $scope.formularioATratar.languages  =  $scope.langs.concat($scope.otherLangs);
                            Becas.addRegistro($scope.formularioATratar).success(function() {
								$log.debug("addRegistro OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addRegistro KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						Spinner.hideSpinner();
						ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
					}
				} else {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
					Spinner.hideSpinner();
				}
			};

            $this.langs = [{
                nombre: 'Ingles',
                puntos: 0},
                {
                    nombre: 'Aleman',
                    puntos: 0},
                {
                    nombre: 'Frances',
                    puntos:  0
                }];


			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;
			$scope.langs = $this.langs;

			$scope.instruccionesVistas = false;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.postal_code_pattern = VALIDATION_PATTERN.POSTAL_CODE;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.BECAS;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;

			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;

			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;
			$scope.formID = FORM_ID_CODES.BECAS;
            $scope.open = DatetimeService.openDatepicker();

            $scope.triggerValidations();
            $scope.gotoWelcome = $this.gotoWelcome;
            $scope.otherLangs = [];

            $scope.addLanguage = function ($evt) {
                $evt.preventDefault();
				if($scope.otherLangs.length){

				}
                if($scope.otherLangs.length<2) {
                    $scope.otherLangs.push({
                        nombre: '',
                        puntos: 0
                    });
                }
            };
            $scope.removeLang = function ($evt,idx) {
                $evt.preventDefault();
                $scope.otherLangs.splice(idx,1);
            };


		});