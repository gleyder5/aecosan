angular.module('secosan').controller('LoginController', function($scope, $log, $location, Messages, AuthenticationUser, FormModelService, REDIRECT_URL) {

	var $this = this;
	
	this.setupController = function(){
		Messages.clearMessagesAndForceRemove();
		$this.validatePresentCookie();
	}

	this.validatePresentCookie = function() {
		var cookie = AuthenticationUser.isCookiePresent();
		if (cookie) {
			AuthenticationUser.checkToken(function() {
				$log.info("user have valid token");
				$location.path($this.getForwardLocationIfValid());
			}, function(data, status, headers, config) {
				$log.info("user have invalid token" + data);
				Messages.clearMessages();
				if (status === 500) {
					Messages.setErrorMessageFromResponse(data, status);
				}
			})
		}
	};

	this.getForwardLocationIfValid = function() {
		var prevLocation = AuthenticationUser.getPrevUrl();
		if (angular.equals(prevLocation, undefined)) {
			prevLocation = REDIRECT_URL.WELCOME;
		}
		return prevLocation;
	}

	$this.setupController();

	this.redirectAfterClaveLogin = function() {
		$this.redirectSuccessfullLogin();
	};

	this.redirectSuccessfullLogin = function() {
		var prevUrl =AuthenticationUser.getPrevUrl();
		if(angular.equals(prevUrl,undefined)){
			prevUrl = REDIRECT_URL.WELCOME;
		}
		$location.path(prevUrl);
	};

	this.doLoginNoEu = function() {
		$scope.showloading = true;
		var form ={};
		form =angular.copy($scope.formularioATratar, form);
		AuthenticationUser.loginUser(form).success(function(data) {
			$this.doLoginNoEuSuccess(data);
		}).error(function(data, status, headers, config) {
			$this.doLoginNoEuError(data, status, headers, config);
		});
	}

	this.doLoginNoEuSuccess = function(data) {
		$log.debug("loginUser OK");
		$scope.showloading = false;
		AuthenticationUser.handleNotEULogin(data, $this.redirectSuccessfullLogin());

	};

	this.doLoginNoEuError = function(data, status, headers, config) {
		$log.debug("loginUser KO");
		$log.debug(data);
		$log.debug(status);
		$log.debug(headers);
		$log.debug(config);
		$scope.showloading = false;
		$scope.hasdata = false;
		if (status === 500) {
			Messages.setErrorMessageFromResponse(data, status);
		} else if (status === 401) {
			$log.debug("loginUser 401");
			$log.debug(data);
			if (data.code === -1) {
				$scope.loginErrorMessage = "WRONGCREDENTIALS";
			} else if (data.code === -2) {
				$scope.loginErrorMessage = "USERBLOCKED";
			}
		}
	};
	$scope.redirectAfterClave = $this.redirectAfterClaveLogin;
	$scope.formularioATratar = FormModelService.getUserLoginModel();
	$scope.sendForm = $this.doLoginNoEu;
	$scope.showloading = false;
});