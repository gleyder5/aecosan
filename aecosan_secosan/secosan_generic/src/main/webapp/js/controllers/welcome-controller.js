angular.module('secosan').controller('WelcomeController', function($scope, $location, $log, AuthenticationUser, HeaderTitle, Messages, NgTableParams, Solicitude, WelcomeMenu, PROCEDURES_TEXT,WELCOME_MENU_ITEMS) {
	var $this = this;

	this.setupController = function() {
		Messages.clearMessagesAndForceRemove();
		WelcomeMenu.setTextUrlArray(PROCEDURES_TEXT);
		if ($scope.tableOptions !== undefined) {
			$scope.tableOptions.filter({});
			$scope.tableOptions.reload();
		}
	};

	this.showHideFilter = function() {
		if ($scope.showTableFilters) {
			$scope.showTableFilters = false;
			$scope.tableOptions.filter({});
		} else {
			$scope.showTableFilters = true;
		}
	}
	this.createTableOptions = new NgTableParams({
		count : 5,
		sorting : {
			id : "desc"
		},
	}, {
		counts : [],
		paginationMaxBlocks : 3,
		paginationMinBlocks : 2,
		getData : function($defer, params) {

			Solicitude.getSolicitudeByAuthor(params).success(function(data) {
				var total = data.recordsTotal;
				if (total !== data.recordsFiltered) {
					total = data.recordsFiltered;
				}
				params.total(total);
				$scope.hasdata = $this.hasData(total);
				$defer.resolve(data.data);

			}).error(function(data, status, headers, config) {
				$scope.hasdata = false;
				Messages.setErrorMessageFromResponse(data, status);
			});
		}
	});
	this.hasData = function(recordsTotal) {
		return recordsTotal > 0
	}
	$scope.menuOptions = WELCOME_MENU_ITEMS;
	this.setupController();
	$scope.tableOptions = $this.createTableOptions;
	$scope.showTableFilters = false;
	$scope.showHideFilter = $this.showHideFilter;
	$scope.hasdata = true;
});