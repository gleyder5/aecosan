angular.module('secosan').controller('WelcomeControllerSia', function($scope, $location, $log,$routeParams,SIA_MENUS) {
	var $this = this;
	var welcomeURL = "/bienvenida";
	
	$this.siaNumber = $routeParams.sia;
	
	$this.redirect=function(url){
		$location.path(url);
	}
	
	
	$this.redirectIfSiaEmpty = function(){
		if($this.siaNumber === undefined && $this.siaNumber===''){
			$this.redirect(welcomeURL);
		}
	};
	
	$this.redirectIfSiaNotDefined = function(){
		var siaMenuElements = SIA_MENUS[$this.siaNumber];
		if(siaMenuElements === undefined || siaMenuElements.length === 0){
			$this.redirect(welcomeURL);
		}
	};
	
	$this.validateRoutParams = function(){
		$this.redirectIfSiaEmpty();
		$this.redirectIfSiaNotDefined();
	}
	
	
	$this.redirectToProcedure = function(url){
		$this.redirect(url);
	}
	
	$this.getTituloSia= function(){
		var siaMenuElements = SIA_MENUS[$this.siaNumber];
		return siaMenuElements.title;
	}
	
	$this.getMenuOptions = function(){
		var siaMenuElements = SIA_MENUS[$this.siaNumber];
		return  siaMenuElements.menuElements;
	}
	
	
	$this.validateRoutParams();
	$scope.redirectToProcedure = $this.redirectToProcedure;
	$scope.tituloSia = $this.getTituloSia();
	$scope.options = $this.getMenuOptions();
});