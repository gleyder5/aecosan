angular.module('secosan').controller(
		'AlimentosGruposCeseController',
		function($scope, $http, $log, $window, $location, $sce, $routeParams, HeaderTitle, Messages, ModalService, ModalParamsService, NgTableParams, AlimentosGrupos, FormModelService, Instrucciones,
				IdentificadorPeticion, FileDtoDownload, CatalogueLocationHandler, ValidationUtilsService, SetDataFromCatalogService, Spinner, Solicitude, LocationCombosService, ImpresoFirmaService,
				CheckSolicitudeEditabilityService, CATALOGUE_NAMES, FORM_NAMES, SOLICITUDE_CODES, RESPONSIBILITIES_MSG, VALIDATION_PATTERN, SENDER_MODE, FORM_ID_CODES, ERR_MSG, AREA_CODES,
				REDIRECT_URL, TASA, STATUS_CODES,TABS_CONFIGURATION) {

			var $this = this;

			var idToEdit = $routeParams.id;
			var viewMode = $routeParams.v;

			$this.modelo = FormModelService.getAlimentosGruposCeseModel();
 
			SetDataFromCatalogService.getEUCountriesCatalogue($scope);

			$this.initializeNewSolicitude = function() {

				Spinner.showSpinner();
				$scope.formularioATratar = $this.modelo;
				// $scope.editMode = false;
				$scope.instruccionesLeidas = false;

				$scope.checkboxModel = {
					representante : false,
					adicionales : false
				}

				$scope.auxSolicitante = {};
				$scope.auxRepresentante = {};
				$scope.auxContacto = {};

				IdentificadorPeticion.getIdentificador(SOLICITUDE_CODES.ALIM_GRUPOS_CESE).success(function(data) {
					$log.debug("Identificador: " + data.identificationRequest);
					$scope.formularioATratar.identificadorPeticion = data.identificationRequest;
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getIdentificador");
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			}

			$this.loadFormToEdit = function(data) {
				$scope.formularioATratar = angular.merge($this.modelo, data.solicitude);
				// $scope.editMode = true;
				$scope.instruccionesLeidas = true;

				CheckSolicitudeEditabilityService.isEditable($scope, $scope.formularioATratar.status, viewMode);

				$scope.checkboxModel = {};

                $scope.auxSolicitante = {};
                $scope.auxRepresentante = {};
                $scope.auxContacto = {};

				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisSolicitante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisRepresentante);
				LocationCombosService.loadCombo($scope, data, CatalogueLocationHandler.domObjectName.paisContacto);
			}

			if (idToEdit !== undefined) {
				Spinner.showSpinner();
				Solicitude.getSolicitudeById(idToEdit).success(function(data) {
					$this.loadFormToEdit(data);
					$log.debug("OK getting solicitude " + idToEdit);
					$scope.enSubsanacion = ($scope.formularioATratar.status === STATUS_CODES.PENDIENTE);
					Spinner.hideSpinner();
				}).error(function(data, status) {
					$log.error("Error getting solicitude " + idToEdit + ": " + status);
					Messages.setErrorMessageFromResponse(data, status);
					Spinner.hideSpinner();
				});
			} else {
				$this.initializeNewSolicitude();
			}

			$this.loadProvinciaOnChange = function(comboPais) {
				CatalogueLocationHandler.loadProvinciaOnChange($scope, comboPais);
			};

			$this.loadMunicipioOnChange = function(comboProvincia) {
				CatalogueLocationHandler.loadMunicipioOnChange($scope, comboProvincia);
			};

			$this.onClickProvincia = function(comboPais) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadProvinciaOnClick($scope, comboPais);
				}
			};

			$this.onClickMunicipio = function(comboProvincia) {
				if (idToEdit !== undefined) {
					CatalogueLocationHandler.loadMunicipioOnClick($scope, comboProvincia);
				}
			};

			$this.verInstrucciones = function() {
				Spinner.showSpinner();
				$log.debug("verInstrucciones");
				$scope.instruccionesVistas = true;
				Instrucciones.getInstrucciones($scope.formularioATratar);
			};

			$this.gotoWelcome = function() {
				$location.path(REDIRECT_URL.WELCOME);
			};

			$scope.identificadorSolicitud = {
				identificationRequest : ""
			};

			
			// control de tabs
			$scope.tabCtrl = angular.copy(TABS_CONFIGURATION.ALIMENTOS_GRUPOS_CESE);
			$scope.$watch("instruccionesLeidas",function(newV,oldV){
				if(newV===oldV){
					return false; 
				}
				$scope.tabCtrl.setTabFocusByIndex($scope.tabCtrl,0);
			});

			
			$scope.triggerValidations = function(){
				ValidationUtilsService.clearCurrent();
				ValidationUtilsService.checkFormValid($scope);
				if ($scope.formularioATratar != undefined && !ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion)) {
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
				}
				
			
			}

			

			$scope.saveForm = function() {
				$scope.editMode = false;
			}

			$scope.getPDF = function() {
				ImpresoFirmaService.getSignaturePDF($scope.formularioATratar, $scope.formID);
			};

			$scope.sendForm = function(senderMode) {
				Spinner.showSpinner();
				var requiredDocumentAttached = false;
				delete $scope.formularioATratar['solicitudePayment'];
				LocationCombosService.setLocationDataToScope($scope);

				if (senderMode === SENDER_MODE.SUBMIT) {
					$scope.formularioATratar.status = SENDER_MODE.SUBMIT;
					$scope.editMode = false;
					requiredDocumentAttached = ValidationUtilsService.isSignedDocumentAttached($scope.formularioATratar.documentacion);
				} else if (senderMode === SENDER_MODE.DRAFT) {
					// TODO: check default behaviour
					$scope.formularioATratar.status = SENDER_MODE.DRAFT;
					$scope.editMode = true;
				}

				$log.info("enviar:> " + $scope.formularioATratar.solicitante.name);

				if (ValidationUtilsService.checkFormValid($scope,true)) {
					if ((requiredDocumentAttached && senderMode === SENDER_MODE.SUBMIT) || senderMode === SENDER_MODE.DRAFT) {
						if (idToEdit !== undefined) {
							AlimentosGrupos.updateCeseComercializacion($scope.formularioATratar).success(function() {
								$log.debug("updateCeseComercializacion OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
								;
							}).error(function(data, status, headers, config) {
								$log.debug("updateCeseComercializacion KO");
								$scope.hasdata = false;
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));

							});
						} else {
							AlimentosGrupos.addCeseComercializacion($scope.formularioATratar).success(function() {
								$log.debug("addCeseComercializacion OK");
								Spinner.hideSpinner();
								ModalService.showModalWindow(ModalParamsService.addedSolicitudSuccess($scope.formularioATratar.identificadorPeticion, senderMode));
							}).error(function(data, status, headers, config) {
								$log.debug("addCeseComercializacion KO");
								Spinner.hideSpinner();
								$scope.hasdata = false;
								ModalService.showModalWindow(ModalParamsService.addedSolicitudError($scope.formularioATratar.identificadorPeticion, senderMode));
							});
						}
					} else {
						ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
						Spinner.hideSpinner();
					}
				} else {
					Spinner.hideSpinner();
					ValidationUtilsService.addValidMessage(ERR_MSG.SIGNED_DOCUMENT, "tab-documentacion",$scope,'SIGNED_DOCUMENT');
					$scope.tabCtrl.setTabFocusByID($scope.tabCtrl  , "VALIDTAB"); 
				}
			};

			$scope.verInstrucciones = $this.verInstrucciones;
			$scope.loadProvincia = $this.loadProvinciaOnChange;
			$scope.loadMunicipio = $this.loadMunicipioOnChange;
			$scope.onClickProvincia = $this.onClickProvincia;
			$scope.onClickMunicipio = $this.onClickMunicipio;

			$scope.instruccionesVistas = false;
			$scope.nif_pattern = VALIDATION_PATTERN.NIF;
			$scope.phone_pattern = VALIDATION_PATTERN.PHONE;
			$scope.email_pattern = VALIDATION_PATTERN.EMAIL;
			$scope.tipoSolicitudId = SOLICITUDE_CODES.ALIM_GRUPOS_CESE;
			$scope.RESPONSIBILITIE_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.GENERAL);
			$scope.spain_county_code = CATALOGUE_NAMES.SPAIN_COUNTRY_CODE;
			$scope.paisSolicitante = CatalogueLocationHandler.domObjectName.paisSolicitante;
			$scope.paisRepresentante = CatalogueLocationHandler.domObjectName.paisRepresentante;
			$scope.paisContacto = CatalogueLocationHandler.domObjectName.paisContacto;
			$scope.provinciaSolicitante = CatalogueLocationHandler.domObjectName.provinciaSolicitante;
			$scope.provinciaRepresentante = CatalogueLocationHandler.domObjectName.provinciaRepresentante;
			$scope.provinciaContacto = CatalogueLocationHandler.domObjectName.provinciaContacto;
			$scope.senderMode = SENDER_MODE;
			$scope.area_codes = AREA_CODES;
			$scope.PAY_MSG = $sce.trustAsHtml(RESPONSIBILITIES_MSG.PAY);
			$scope.formularios = FORM_NAMES;
			$scope.formularioSeleccionado = FORM_NAMES.CESECOMERCIALIZACION;

			$scope.gotoWelcome = $this.gotoWelcome;

			$scope.formID = FORM_ID_CODES.GE_CESE;
			$scope.tasatipo = TASA.GE_CESE;
		});