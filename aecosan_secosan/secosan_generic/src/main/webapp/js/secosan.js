'use strict';
angular.module('secosan', ['ngRoute', 'ngResource', 'ngCookies', 'ngTable', 'ui.bootstrap', 'ngDialog', 'naif.base64', 'utf8-base64', 'secosanGeneric', 'ngPasswordMeter']).constant(
    'TEMPLATE_PATHS', {
        PAGES: '/secosan/partials/pages/',
        DIRECTIVES: '/secosan/partials/directives/',
        DIRECTIVES_ALIMENTOS_GRUPOS: '/secosan/partials/directives/alimentosGrupos/',
        DIRECTIVES_APODERAMIENTO : '/secosan/partials/directives/apoderamiento/',
        DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS: '/secosan/partials/directives/complementosAlimenticios/',
        DIRECTIVES_FORMULARIOS: "/secosan/partials/directives/generics/formularios/",
        DIRECTIVES_COMUNICACION: "/secosan/partials/directives/comunicacion/",
        DIRECTIVES_PROCEDIMIENTO_GENERICO: "/secosan/partials/directives/procedimientoGenerico/",
        DIRECTIVES_SOLICITUD_LOGOS: "/secosan/partials/directives/solicitudLogos/",
        DIRECTIVES_RIESGOS: "/secosan/partials/directives/evaluacionRiesgos/",
        DIRECTIVES_LMR: "/secosan/partials/directives/lmrFitosanitarios/",
        DIRECTIVES_USUARIOS: "/secosan/partials/directives/usuarios/",
        DIRECTIVES_PAGO: "/secosan/partials/directives/generics/formularios/pago/",
        DIRECTIVES_FINANCIACION: "/secosan/partials/directives/financiacionAlimentos/",
        DIRECTIVES_AGUAS: "/secosan/partials/directives/registroAguas/",
        DIRECTIVES_INDUSTRIAS: "/secosan/partials/directives/registroIndustrias/",
        DIRECTIVES_TRAMITADOR: "/secosan/partials/directives/menuTramitador/",
        DIRECTIVES_REGISTRO_ESTATAL: "/secosan/partials/directives/registroEstatal/",
        DIRECTIVES_ASOCIACION_CONSUMIDORES: "/secosan/partials/directives/subvencionAsocuae/",
        DIRECTIVES_ASOCIACION_JUNTAS: "/secosan/partials/directives/subvencionJuntas/",
        DIRECTIVES_BECAS: "/secosan/partials/directives/becas/",
        // DIRECTIVES_GENERICS : "/secosan/partials/directives/generics/",
        // DIRECTIVES_WRAPPERS : "/secosan/partials/directives/wrappers/"
    }).constant('AUTH_EVENTS', {
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
}).constant('FORM_URL', {
    FORM_ALIMENTOS_GRUPOS_CESE_URL: '#/alimentosGruposCeseComercializacion',
    FORM_ALIMENTOS_GRUPOS_PUESTA_MERCADO_URL: '#/alimentosGruposPuestaEnMercado',
    FORM_ALIMENTOS_GRUPOS_MODIFICACION_URL: '#/alimentosGruposModificacion',
    FORM_COMPLEMENTOS_ALIMENTICIOS_CESE_URL: '#/complementosAlimenticiosCeseComercializacion',
    FORM_COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO_URL: '#/complementosAlimenticiosPuestaEnMercado',
    FORM_COMPLEMENTOS_ALIMENTICIOS_MODIFICACION_URL: '#/complementosAlimenticiosModificacion',
    FORM_QUEJA_URL: '#/comunicacionQueja',
    FORM_SUGERENCIA_URL: '#/comunicacionSugerencia',
    FORM_FINANCIACION_ALIMENTOS_INCLUSION_URL: '#/financiacionAlimentosInclusion',
    FORM_FINANCIACION_ALIMENTOS_ALTERACION_URL: '#/financiacionAlimentosAlteracion',
    FORM_FINANCIACION_ALIMENTOS_SUSPENSION_URL: '#/financiacionAlimentosSuspension',
    FORM_LMR_FITOSANITARIOS_URL: '#/lmrFitosanitarios',
    FORM_SOLICITUD_LOGOS_URL: '#/solicitudLogos',
    FORM_EVALUACION_RIESGOS_URL: '#/evaluacionRiesgos',
    FORM_REGISTRO_AGUAS_URL: '#/registroAguas',
    FORM_REGISTRO_INDUSTRIAS_URL: '#/registroIndustrias',
    FORM_PROCEDIMIENTO_GENERAL_URL: '#/procedimientoGenerico',
    FORM_REGISTRO_REACU_URL: '#/registroEstatal',
    FORM_SUBVENCIONES_ASOCUAE_URL: '#/subvencionAsocuae',
    FORM_SUBVENCIONES_JUNTAS_URL: '#/subvencionJuntasConsumo',
    FORM_BECAS_URL: '#/becasConcejoConsumidores',
}).constant('ENTITY_NAME', {
    ENTITY_ALIMENTOS_GRUPOS_CESE: 'CeseComercializacionAGEntity',
    ENTITY_ALIMENTOS_GRUPOS_PUESTA_MERCADO: 'PuestaMercadoAGEntity',
    ENTITY_ALIMENTOS_GRUPOS_MODIFICACION: 'ModificacionDatosAGEntity',
    ENTITY_COMPLEMENTOS_ALIMENTICIOS_CESE: 'CeseComercializacionCAEntity',
    ENTITY_COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO: 'PuestaMercadoCAEntity',
    ENTITY_COMPLEMENTOS_ALIMENTICIOS_MODIFICACION: 'ModificacionDatosCAEntity',
    ENTITY_QUEJA: 'SolicitudQuejaEntity',
    ENTITY_SUGERENCIA: 'SolicitudSugerenciaEntity',
    ENTITY_FINANCIACION_ALIMENTOS_INCLUSION: 'IncOferAlimUMEEntity',
    ENTITY_FINANCIACION_ALIMENTOS_ALTERACION: 'AlterOferAlimUMEEntity',
    ENTITY_FINANCIACION_ALIMENTOS_SUSPENSION: 'BajaOferAlimUMEEntity',
    ENTITY_LMR_FITOSANITARIOS: 'SolicitudLmrFitosanitariosEntity',
    ENTITY_SOLICITUD_LOGOS: 'SolicitudLogoEntity',
    ENTITY_EVALUACION_RIESGOS: 'EvaluacionRiesgosEntity',
    ENTITY_REGISTRO_AGUAS: 'RegistroAguasEntity',
    ENTITY_REGISTRO_INDUSTRIAS: 'RegistroIndustriasEntity',
    ENTITY_PROCEDIMIENTO_GENERAL: 'ProcedimientoGeneralEntity',
    ENTITY_REGISTRO_REACU: 'RegistroReacuEntity',
    ENTITY_SUBVENCIONES_ASOCUAE: 'SubvencionesAsocuaeEntity',
    ENTITY_SUBVENCIONES_JUNTAS: 'SubvencionesJuntasConsumoEntity',
    ENTITY_BECAS: 'BecasEntity'
}).constant('REST_URL', {
    SOLICITUDE_LIST: "/secosan/rest/secure/solicitudList/",
    ALIMENTOS_GRUPOS_CESE: "/secosan/rest/secure/alimentosGrupos/ceseComercializacion/",
    ALIMENTOS_GRUPOS_PUESTA_MERCADO: "/secosan/rest/secure/alimentosGrupos/puestaMercado/",
    ALIMENTOS_GRUPOS_MODIFICACION: "/secosan/rest/secure/alimentosGrupos/modificacionDatos/",
    APODERAMIENTO: "/secosan/rest/secure/apoderamiento/",
    COMPLEMENTOS_ALIMENTICIOS_CESE: "/secosan/rest/secure/complementosAlimenticios/ceseComercializacion/",
    COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO: "/secosan/rest/secure/complementosAlimenticios/puestaMercado/",
    COMPLEMENTOS_ALIMENTICIOS_MODIFICACION: "/secosan/rest/secure/complementosAlimenticios/modificacionDatos/",
    PROCEDIMIENTO_GENERICO: "/secosan/rest/secure/procedimientoGeneral/",
    REGISTRO_REACU: "/secosan/rest/secure/registroReacu/",
    SUBVENCIONES_ASOCUAE: "/secosan/rest/secure/subvencionesAsocuae/",
    SUBVENCIONES_JUNTAS: "/secosan/rest/secure/subvencionesJuntas/",
    QUEJA: "/secosan/rest/secure/solicitudQuejaSugerencia/queja/",
    SUGERENCIA: "/secosan/rest/secure/solicitudQuejaSugerencia/sugerencia/",
    FINANCIACION_ALIMENTOS_INCLUSION: "/secosan/rest/secure/financiacionAlimentos/incOferAlimUME/",
    FINANCIACION_ALIMENTOS_ALTERACION: "/secosan/rest/secure/financiacionAlimentos/alterOferAlimUME/",
    FINANCIACION_ALIMENTOS_SUSPENSION: "/secosan/rest/secure/financiacionAlimentos/bajaOferAlimUME/",
    LMR_FITOSANITARIOS: "/secosan/rest/secure/solicitudLmrFitosanitarios/lmrFitosanitarios/",
    SOLICITUD_LOGOS: "/secosan/rest/secure/solicitudLogo/logo/",
    REGISTRO_AGUAS: "/secosan/rest/secure/registroAguas/",
    BECAS: "/secosan/rest/secure/becas/",

    REGISTRO_INDUSTRIAS: "/secosan/rest/secure/registroIndustrias/",
    INSTRUCCIONES: "/secosan/rest/secure/instructions/",
    INSTRUCCIONES_PAGO_DOWNLOAD: "/secosan/rest/secure/instructions/payment/",
    IDENTIFICADOR: "/secosan/rest/secure/nuevoIdentificacionPeticion/",
    SERVICIOSPAGO: "/secosan/rest/secure/servicios/",
    DOCUMENTACION: "/secosan/rest/secure/documentacion/",
    SOLICITUDE: "/secosan/rest/secure/solicitud/",
    REGISTRO: "/secosan/rest/secure/userNoUEMembership/",
    REGISTRY_USER_NOUE: "/secosan/rest/userNoUEMembership/",
    USER_MANAGEMENT: "/secosan/rest/admin/addUserInternal",
    AUTHENTICATION: "/secosan/rest/login/",
    AUTHENTICATION_CHECK: "/secosan/rest/login/checkToken",
    GET_PROVINCIA: "/secosan/rest/secure/provincias/",
    EVALUACION_RIESGOS: "/secosan/rest/secure/evaluacionRiesgos/",
    PDF_REPORT: "/secosan/rest/secure/impresoFirma/",
    PDF_REPORT_ANEXOII: "/secosan/rest/secure/impresoFirmaANEXOII/",
    PDF_REPORT_ANEXOII_FICHA_TECNICA :"/secosan/rest/secure/impresoFirmaANEXOIIFichaTecnicaDetail/",
    PDF_REPORT_ANEXOIII: "/secosan/rest/secure/impresoFirmaANEXOIII/",
    PDF_REPORT_ANEXOIV: "/secosan/rest/secure/impresoFirmaANEXOIV/",
    PDF_REPORT_LOGOS: "/secosan/rest/s  ecure/impresoFirmaLogos/",
    SOLICITUDE_STATUS: "/secosan/rest/secure/status/",
    DOC_TYPES: "/secosan/rest/secure/documentacion/form/",
    GET_JUSTIFICANTE: "/secosan/rest/secure/payment/justificante",
    JUSTIFICANTE_PAGO: "/secosan/rest/secure/payment/justificantePdf/",
    MAKE_PAYMENT: "/secosan/rest/secure/payment",
    IS_PAID: "/secosan/rest/secure/payment/isPaid/",
    TASA_BY_FORM: "/secosan/rest/secure/tasa/formulario/",
    TASA_BY_SERVICE: "/secosan/rest/secure/tasa/service/",
    ATTACHABLE_FILE: "/secosan/rest/secure/attachables/",
    CHANGE_PASSWORD: "/secosan/rest/secure/changePassword/",
    DELETE_SUSPENDED_PRODUCT: "/secosan/rest/secure/financiacionAlimentos/bajaOferAlimUME/deleteProduct/",
    //Tipo Procedimiento

    TIPO_PROCEDIMIENTO_MANAGEMENT_LIST: "/secosan/rest/secure/tipoProcedimiento/list",

}).constant('REDIRECT_URL', {
    WELCOME: "/bienvenida",
    LOGIN: "/login",
    WELCOME_TRAMITADOR: "/bienvenidaTramitador",
    LOGIN_TRAMITADOR: "/loginTramitador"
}).constant('FORM_ID_CODES', {
    CA_PUESTA_MERCADO: 1,
    CA_MODIFICACION: 2,
    CA_CESE: 3,
    GE_PUESTA_MERCADO: 4,
    GE_MODIFICACION: 5,
    GE_CESE: 6,
    RIESGOS: 7,
    QUEJA: 8,
    SUGERENCIA: 9,
    LOGOS: 10,
    INCLUSION_UME: 11,
    ALTERACION_UME: 12,
    SUSPENSION_UME: 13,
    LMR: 14,
    REGISTRO_AGUAS: 15,
    REGISTRO_INDUSTRIAS: 16,
    PROCEDIMIENTO_GENERAL : 17,
    REGISTRO_REACU:18,
    SUBVENCIONES_ASOCUAE : 19 ,
    SUBVENCIONES_JUNTAS: 21 ,
    BECAS: 20 ,

})
    .constant('SOLICITUDE_DELETE_STATUS', ['Borrador'])
    .constant('SOLICITUDE_EDIT_MODE_STATUS', ['Borrador', 'Pendiente de subsanación'])
    .constant('SOLICITUDE_EDIT_MODE_STATUS_CODE', [1, 3,18])
    .constant('PROCEDURES_TEXT', {
        defaultMessage: {
            message: "WELCOME_PROC_DES_DEFAULT",
        },
        comqueja: {
            message: "WELCOME_PROC_DES_COMPLAINS",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_COMPLAINS",
                url: "#/comunicacionQueja"
            }, {
                    buttonMessage: "WELCOME_PROC_BTN_SUGESTIONS",
                    url: "#/comunicacionSugerencia"
                }
            ]
        },
        evalriesgo: {
            message: "WELCOME_PROC_DES_RISK",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_RISK",
                url: "#/evaluacionRiesgos"
            }]

        },
        solfinmed: {
            message: "WELCOME_PROC_DES_SOLFINMD",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_SOLFINMD",
                url: "#/financiacionAlimentosInclusion"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_SOLFIALO",
                url: "#/financiacionAlimentosAlteracion"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_SOLFICOMSUS",
                url: "#/financiacionAlimentosSuspension"
            },

            ]
        },
        notcomp: {
            message: "WELCOME_PROC_DES_COMPAL",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_COMPAL_PM",
                url: "#/complementosAlimenticiosPuestaEnMercado"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_COMPAL_CC",
                url: "#/complementosAlimenticiosCeseComercializacion"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_COMPAL_MC",
                url: "#/complementosAlimenticiosModificacion"
            },]
        },
        notaliesp: {
            message: "WELCOME_PROC_DES_ALES",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_ALESP_PM",
                url: "#/alimentosGruposPuestaEnMercado"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_ALESP_CC",
                url: "#/alimentosGruposCeseComercializacion"
            }, {
                buttonMessage: "WELCOME_PROC_BTN_ALESP_MC",
                url: "#/alimentosGruposModificacion"
            }]
        },
        sollogo: {
            message: "WELCOME_PROC_DES_LOGIMGDOC",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_LOGIMGDOC",
                url: "#/solicitudLogos"
            }]
        },
        lmrfitosanitarios: {
            message: "WELCOME_PROC_DES_LMRFITOSANITARIOS",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_LMRFITOSANITARIO",
                url: "#/lmrFitosanitarios"
            }]
        },
        registroAguas: {
            message: "WELCOME_MENU_REGISTRO_AGUAS",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_REGISTRO_AGUAS",
                url: "#/registroAguas"
            }]
        },
        registroIndustrias: {
            message: "WELCOME_MENU_REGISTRO_INDUSTRIAS",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_REGISTRO_INDUSTRIAS",
                url: "#/registroIndustrias"
            }]
        },
        plag: {
            message: "WELCOME_PROC_DES_PLAG",
            buttonsSetup: [{
                buttonMessage: "WELCOME_PROC_BTN_PLAG",
                url: "#/comunicacionQueja"
            }]
        },
        // consuExtInscReg: {
        //     message: "WELCOME_PROC_DES_CONSUMO_EXT_INSC_REG",
        //     buttonsSetup: [{
        //         buttonMessage: "WELCOME_PROC_BTN_CONSUMO_EXT_INSC_REG",
        //         url:"#/registroEstatal"
        //         // url:"https://registroelectronico.msssi.es/formulariosINC/IncFormInscrREACU.jsp"
        //     }]
        // },
        // consuExtProgAct: {
        //     message: "WELCOME_PROC_DES_CONSUMO_EXT_PROG_ACT",
        //     buttonsSetup: [{
        //         buttonMessage: "WELCOME_PROC_BTN_CONSUMO_EXT_PROG_ACT",
        //         // url: "https://registroelectronico.msssi.es/formulariosINC/IncFormSubvAACC.jsp",
        //         url:"#/subvencionAsocuae"
        //     }]
        // },
        // consuExtBecas: {
        //     message: "WELCOME_PROC_DES_CONSUMO_EXT_BECAS",
        //     buttonsSetup: [{
        //         buttonMessage: "WELCOME_PROC_BTN_CONSUMO_EXT_BECAS",
        //         // url:  "https://rec.redsara.es/registro/action/are/acceso.do";
        //         url:"#/becasConcejoConsumidores"
        //     }]
        // },
        // consuExtSubvenciones: {
        //     message: "WELCOME_PROC_DES_CONSUMO_EXT_SUBVENCIONES",
        //     buttonsSetup: [{
        //         buttonMessage: "WELCOME_PROC_BTN_CONSUMO_EXT_SUBVENCIONES",
        //         url:"#/subvencionJuntasConsumo"
        //
        //     }]
        // },
        genprocess:{
            message: "WELCOME_PROC_DES_GENERIC_PROCESS",
            // template: "/secosan/partials/directives/procedimientoGenerico/ingresar-procedimiento-recsara.html",
            buttonsSetup: [{
                buttonMessage: "LABEL_INGRESAR_OFICINA_REGISTRAL",
                url: "#/procedimientoGenerico"
            }]
        },
    }).constant('WELCOME_MENU_ITEMS',
    [
        {
            elementid: "genprocess",
            title: "WELCOME_MENU_SIA_CODES_OFICINA_REGISTRAL",
            // siaCodes: "WELCOME_MENU_SIA_CODES_GENERIC",
            siaCodes:null,
        },
        {
            elementid: "comqueja",
            title: "WELCOME_MENU_SIA_CODES_COMPLAINS_SUGERENCIAS",
            siaCodes: "WELCOME_MENU_SIA_CODES_COMPLAINS",
        },
        {
        elementid: "evalriesgo",
        title: "WELCOME_MENU_RISKS",
        siaCodes: "WELCOME_MENU_SIA_CODES_RISKS",
    },
        {
        elementid: "solfinmed",
        title: "WELCOME_MENU_SOLFINMD",
        siaCodes: "WELCOME_MENU_SIA_CODES_SOLFINMD",
    },
            {
        elementid: "notcomp",
        title: "WELCOME_MENU_COMPAL",
        siaCodes: "WELCOME_MENU_SIA_CODES_COMPAL",
    }, {
        elementid: "notaliesp",
        title: "WELCOME_MENU_ALES",
        siaCodes: "WELCOME_MENU_SIA_CODES_ALES",
    }, {
        elementid: "sollogo",
        title: "WELCOME_MENU_LOGIMGDOC",
        siaCodes: "WELCOME_MENU_SIA_CODES_LOGIMGDOC",
    },
        {
            elementid: "lmrfitosanitarios",
            title: "WELCOME_MENU_LMRFITOSANITARIOS",
            siaCodes: "WELCOME_MENU_SIA_CODES_LMRFITOSANITARIOS",
        }, {
        elementid: "registroAguas",
        title: "WELCOME_MENU_REGISTRO_AGUAS",
        siaCodes: "WELCOME_MENU_SIA_CODES_REGISTRO_AGUAS",
    }, {
        elementid: "registroIndustrias",
        title: "WELCOME_MENU_REGISTRO_INDUSTRIAS",
        siaCodes: "WELCOME_MENU_SIA_CODES_REGISTRO_INDUSTRIAS",
    },
        // {
        //     elementid: "consuExtInscReg",
        //     title: "WELCOME_MENU_CONSUMO_EXT_INSC_REG",
        //     siaCodes: "WELCOME_MENU_SIA_CODES_CONSUMO_EXT_INSC_REG",
        // },
        // {
        //     elementid: "consuExtProgAct",
        //     title: "WELCOME_MENU_CONSUMO_EXT_PROG_ACT",
        //     siaCodes: "WELCOME_MENU_SIA_CODES_CONSUMO_EXT_PROG_ACT",
        // },
        // {
        //     elementid: "consuExtBecas",
        //     title: "WELCOME_MENU_CONSUMO_EXT_BECAS",
        //     siaCodes: "WELCOME_MENU_SIA_CODES_CONSUMO_EXT_BECAS",
        // },
        // {
        //     elementid: "consuExtSubvenciones",
        //     title: "WELCOME_MENU_CONSUMO_EXT_SUBVENCIONES",
        //     siaCodes: "WELCOME_MENU_SIA_CODES_CONSUMO_EXT_SUBVENCIONES",
        // },

    ]
).constant('SIA_MENUS', {
    "000000": {
        title: "SIA_WELCOME_TITLE_000000",
        menuElements: [
            {
                elementid: "genprocess",
                title: "WELCOME_MENU_PROCEDIMIENTO_GENERAL",
                url: "/procedimientoGenerico"
            }
        ]
    },
    "998404": {
        title: "SIA_WELCOME_TITLE_998404",
        menuElements: [
            {
                elementid: "comqueja",
                title: "WELCOME_MENU_COMPLAINS",
                url: "/comunicacionQueja"
            },
            {
                elementid: "comsur",
                title: "WELCOME_MENU_SUGESTIONS",
                url: "/comunicacionSugerencia"
            }
        ]
    },
    "202098": {
        title: "SIA_WELCOME_TITLE_202098",
        menuElements: [
            {
                elementid: "incl",
                title: "WELCOME_MENU_FINANCIACION_MEDICOS_INCLUSION",
                url: "/financiacionAlimentosInclusion"
            },
            {
                elementid: "alter",
                title: "WELCOME_MENU_FINANCIACION_MEDICOS_ALTERACION",
                url: "/financiacionAlimentosAlteracion"
            },
            {
                elementid: "susp",
                title: "WELCOME_MENU_FINANCIACION_MEDICOS_SUSPENCION",
                url: "/financiacionAlimentosSuspension"
            }
        ]


    },
    "998510": {
        title: "SIA_WELCOME_TITLE_998510",
        menuElements: [
            {
                elementid: "puesta",
                title: "WELCOME_MENU_COMP_ALIM_PUESTA",
                url: "/complementosAlimenticiosPuestaEnMercado"
            },
            {
                elementid: "cese",
                title: "WELCOME_MENU_COMP_ALIM_CESE",
                url: "/complementosAlimenticiosCeseComercializacion"
            },
            {
                elementid: "susp",
                title: "WELCOME_MENU_COMP_ALIM_MODIFICACION",
                url: "/complementosAlimenticiosModificacion"
            }
        ]


    },
    "085044": {
        title: "SIA_WELCOME_TITLE_085044",
        menuElements: [
            {
                elementid: "puesta",
                title: "WELCOME_MENU_GRUPOSESPC_PUESTA",
                url: "/alimentosGruposPuestaEnMercado"
            },
            {
                elementid: "cese",
                title: "WELCOME_MENU_GRUPOSESPC_CESE",
                url: "/alimentosGruposCeseComercializacion"
            },
            {
                elementid: "susp",
                title: "WELCOME_MENU_GRUPOSESPC_MODIFICACION",
                url: "/alimentosGruposModificacion"
            }
        ]
    },
}).constant('FORM_NAMES', {
    CESECOMERCIALIZACION: "ceseComercializacion",
    PUESTAENMERCADO: "puestaEnMercado",
    MODIFICACION: "modificacion",
    QUEJA: "queja"
}).constant('SOLICITUDE_CODES', {
    QUEJA: "1",
    SUGERENCIA: "2",
    EVALUACION_RIESGOS: "3",
    FINA_ALIM_ALTERACION: "4",
    FINA_ALIM_INCLUSION: "4",
    FINA_ALIM_SUSPENSION: "4",
    COMP_ALIM_CESE: "5",
    COMP_ALIM_MODIFICACION: "5",
    COMP_ALIM_PUESTA_MERCADO: "5",
    ALIM_GRUPOS_CESE: "6",
    ALIM_GRUPOS_MODIFICACION: "6",
    ALIM_GRUPOS_PUESTA_MERCADO: "6",
    SOLICITUD_LOGOS: "7",
    LMR_FITOSANITARIOS: "8",
    REGISTRO_AGUAS: "9",
    REGISTRO_INDUSTRIAS: "10",
    PROCEDIMIENTO_GENERAL :"11",
    REGISTRO_REACU :"12",
    SUBVENCIONES_ASOCUAE:"13",
    SUBVENCIONES_JUNTAS:"15",
    BECAS:"14",
}).constant('HEADERS', {
    JSON: "{'Content-Type': 'application/json'}"
}).constant('STATUS_CODES', {
    BORRADOR: 1,
    ENVIADA: 2,
    PENDIENTE: 3,
    TRAMITADA: 4,
    DENEGADA: 5,
    ENVIADO_SG_CARTERA_BASICA: 6,
    ESTUDIO_AECOSAN: 7,
    PENDIENTE_COMPROBACIÓN: 8,
    ENVIADO_MAGRAMA: 9,
    ENVIADO_EFSAICON: 10,
    PARADA_RELOJ_EFSA: 11,
    VALORACION :19,
    REQ_SUBSANAR_10_DIAS :18
}).constant('AREA_CODES', {
    RGSEAA: 1,
    EVALUACION_RIESGOS: 2,
    COMUNICACION: 3,
    GEST_RIESGOS_NUTRICIONALES: 4,
    GEST_RIESGOS_QUIMICOS: 5,
    SECRETARIA_GENERAL: 6,
    BECAS: 8,
    REGESTATAL: 9,
    SUBVENCIONES:10,

}).constant('DOCUMENT_TYPE', {
    SIGNED_DOCUMENT: "1",
    PAGO_TASAS_RECEIPT: "2",
    LABEL: "3",
    QUALITY_ABD_QUALITATIVE_COMPOSITION: "12",
    ORIGINAL_LABEL: "13",
    SKETCH: "14",
    TAX_PAGO_TASAS_RECEIPT: "15",
    CHANGE_EXPLANATION: "16",
    LMR_SOLICITUDE: "17",
    TECHNICAL_DATA_FORM: "20",
    ANEXOII: "21",
    ANEXOIII: "23",
    ANEXOIV: "24",
    ORIGINAL_COUNTRY_DOCUMENT: "27",
    CV:'33',
    COLLEGE_DEGREE:'34',
    DNI:'38'
}).constant('URL_PARAMETER', {
    VIEW_MODE: "v"
}).constant('MINIAPPLET', "https://127.0.0.1:56161/afirma").constant('TASA', {
    TASA_PATH: 'https://oficina-virtual-tasasv2.msssi.es/aesantasa/gestionTasa.do',
    CA_PUESTA_MERCADO: "1",
    CA_MODIFICACION: "1",
    CA_CESE: "1",
    GE_PUESTA_MERCADO: "1",
    GE_MODIFICACION: "1",
    GE_CESE: "1",
    RIESGOS: "6",
    INCLUSION_UME: "3",
    ALTERACION_UME: "3",
    SUSPENSION_UME: "3",
    LMR: "6"

}).constant('DISABLE_PAYMENTONLINE', false).constant('IS_ADMIN', false);