'use strict';
angular
		.module('secosanGeneric', [ 'ngCookies', 'angular-jwt', 'pascalprecht.translate', 'ngIdle', 'angular-cookie-law', 'utf8-base64','ui.select' ])
		.config([ "$translateProvider", function($translateProvider) {

			$translateProvider.useStaticFilesLoader({
				prefix : "/secosan/js/languages/",
				suffix : ".json"
			});

			$translateProvider.preferredLanguage("es_ES");
			$translateProvider.forceAsyncReload(true);
			$translateProvider.useSanitizeValueStrategy("sanitizeParameters");

		} ])
		.constant('GENERIC_TEMPLATE_PATHS', {
			DIRECTIVES_GENERICS : "/secosan/partials/directives/generics/",
			DIRECTIVES_WRAPPERS : "/secosan/partials/directives/wrappers/",
			DIRECTIVES_LOGIN : "/secosan/partials/directives/login/",
		})
		.constant('CLAVE_PATH', '/secosan/clavelogin.do')
		.constant('GENERIC_REST_PATHS', {
			CATALOG_REST_PATH : "/secosan/rest/secure/catalog/",
			HISTORIAL_SOLICITUDES_REST_PATH : "/secosan/rest/secure/historial/",
		})
		.constant('FILES_CONSTANTS', {
			MAXSIZE : 20480,
			MINSIZE : 0.001,
			FILE_TYPE : "application/pdf",
            ACCEPTS_PDF  : ".pdf",
            ACCEPTS_ZIPS : ["application/zip","application/rar","application/7z","application/x-zip-compressed"]
		})
		.constant('SENDER_MODE', {
			SUBMIT : 2,
			DRAFT : 1
		})
		.constant(
				'ERR_MSG',
				{
					SIGNED_DOCUMENT : "Para poder enviar una solicitud debe adjuntar un documento del tipo 'Impreso Firmado'.",
					CV:"Para poder enviar una solicitud debe adjuntar un documento del tipo 'Curriculum Vitae'.",
                    DNI:"Para poder enviar una solicitud debe adjuntar el DNI.",
                    COLLEGE_DEGREE:"Para poder enviar una solicitud debe adjuntar un documento del tipo 'Titulo Universitario'.",
					SIGNED_PAYMENT_LABEL_DOCUMENT : "Para poder enviar una solicitud debe adjuntar un documento del tipo 'Impreso Firmado', 'Justificante de pago' y 'Etiqueta'.",
					MISSED_PAYMENT : "Para poder continuar, debe realizar el pago de la tasa correspondiente al tipo de servicio",
					LMR_SOLICITUDE : "Para poder enviar una solicitud debe adjuntar un documento del tipo 'Impreso Firmado' y 'Formulario UE solicitud LMR'.",
					PASSWORD_MATCH : "Las contrase\u00F1as debe coincidir.",
					NIF_INVALID : "El Nif no tiene el formato adecuado.",
					EMAIL_INVALID : "El email no tiene el formato adecuado.",
					VIEW_MODE : "Este documento no es editable, se muestra en modo visualizaci\u00F3n.",
					NIF_INVALID_SOLICITANTE : "El Nif del solicitante no tiene el formato adecuado.",
					POSTAL_CODE_INVALID_SOLICITANTE : "El c\u00F3digo postal del solicitante no tiene el formato adecuado.",
					PHONE_INVALID_SOLICITANTE : "El tel\u00E9fono del solicitante no tiene el formato adecuado.",
					EMAIL_INVALID_SOLICITANTE : "El email del solicitante no tiene el formato adecuado.",
					NIF_INVALID_REPRESENTANTE : "El Nif del representante no tiene el formato adecuado.",
					POSTAL_CODE_INVALID_REPRESENTANTE : "El c\u00F3digo postal del representante no tiene el formato adecuado.",
					PHONE_INVALID_REPRESENTANTE : "El tel\u00E9fono del representante no tiene el formato adecuado.",
					EMAIL_INVALID_REPRESENTANTE : "El email del representante no tiene el formato adecuado.",
					POSTAL_CODE_INVALID_ADICIONALES : "El c\u00F3digo postal de los datos de contacto adicionales no tiene el formato adecuado.",
					PHONE_INVALID_ADICIONALES : "El tel\u00E9fono de los datos de contacto adicionales no tiene el formato adecuado.",
					EMAIL_INVALID_ADICIONALES : "El email de los datos de contacto adicionales no tiene el formato adecuado.",
					DOCUMENTS_FOR_ANEXO_II : "Para poder enviar la solicitud debe adjuntar los siguientes documentos: 'Anexo II firmado', 'Ficha de datos t\u00E9cnicos', 'Composici\u00F3n cualitativa y cuantitativa del producto', 'Etiqueta original','Boceto de precinto identificativo' y 'Justificante de pago de tasas'.",
					DOCUMENTS_FOR_ANEXO_III : "Para poder enviar la solicitud debe adjuntar los siguientes documentos: 'Anexo III firmado', 'Etiqueta original de cada presentaci\u00F3n', 'Ficha de datos t\u00E9cnicos', 'Boceto de precinto identificativo' y 'Justificante de pago de tasas'.",
					DOCUMENTS_FOR_ANEXO_IV : "Para poder enviar la solicitud debe adjuntar el siguiente documento: 'Anexo IV'.",
					POSTAL_CODE_INVALID_INDUSTRIA : "El c\u00F3digo postal de la industria o establecimiento no tiene el formato adecuado.",
					DOCUMENT_SIGNED_PAYMENT_ORIGINAL_COUNTRY : "Para poder enviar una solicitud debe adjuntar un documento del tipo 'Impreso Firmado', 'Justificante de pago' y 'Certificaci\u00F3n del pa\u00EDs origen'.",
					DOCUMENT_SIGNED_PAYMENT : "Para poder enviar una solicitud debe adjuntar un documento del tipo 'Impreso Firmado' y 'Justificante de pago' ",
					OLD_EQUAL_NEW_ERROR: "La nueva contraseña no puede coincidir con la contraseña actual.",
					NEW_PASSWORD_FORMAT_ERROR: "La nueva contraseña debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula, al menos una mayúscula y al menos un carácter no alfanumérico.",
					USER_NOT_FOUND_ERROR: "Usuario no válido.",
					OLD_PASSWORD_ERROR : "La contraseña actual no es correcta",
					GENERAL_ERROR : "Se ha producido un error al cambiar la contraseña",
					email : "El email no tiene el formato adecuado.",
					max : "Valor demasiado grande",
					maxlength : "Texto muy largo",
					min : "Valor muy pequeño",
					minlength : "Texto muy corto",
					number : "El valor no es un numero",
					pattern : "Este valor no encaja con el patron especificado",
					required : "Campo requerido",
					url : "Url mal formado",
					date : "Formato de fecha incorrecta",
					datetimelocal : "Formato de hora y fecha incorrecta",
					time : "Formato de hora incorrecta",
					week : "Tiempo especificado no existe",
					month : "Tiempo especificado no existe"
						

				})
		.constant(
				'RESPONSIBILITIES_MSG',
				{
					GENERAL : "DECLARACI\u00D3N\ DE RESPONSABILIDAD:"
							+ "El firmante, con capacidad jur\u00EDdica y de obrar suficiente para este acto, en su propio nombre o en representaci\u00F3n "
							+ "de la empresa, y con relaci\u00F3n a este procedimiento, declara bajo su responsabilidad:\n"
							+ "1. Haber le\u00EDdo y comprendido las instrucciones que acompa\u00F1an al presente formulario.\n"
							+ "2. Que son ciertos los datos consignados en este formulario y declaraci\u00F3n.\n"
							+ "3. Que el producto objeto de este procedimiento cumple con los requisitos establecidos conforme a la legislaci\u00F3n que le es de aplicaci\u00F3n.\n"
							+ "4. Que se compromete a aportar la documentaci\u00F3n necesaria cuando se le requiera por parte de la autoridad competente para que subsane una falta o acompa&nacute;e los documentos preceptivos.",
					PAY : "For the online platform, you MUST have a bank account or credit card from the List of associated banking institutions of the Spanish Tax Agency (AEAT). Otherwise you can pay by bank transfer to AECOSAN. Follow the instructions at the top.",
					COMP_ALIM_PUESTA: "DECLARACI\u00D3N\ DE RESPONSABILIDAD:"
							+ "El firmante, con capacidad jur\u00EDdica y de obrar suficiente para este acto, en su propio nombre o en representaci\u00F3n "
							+ "de la empresa, y con relaci\u00F3n a la comunicaci\u00F3n de puesta en el mercado de este complemento alimenticio, declara bajo su responsabilidad:\n"
							+ "1. Haber le\u00EDdo y comprendido las instrucciones que acompa\u00F1an al presente formulario.\n"
							+ "2. Que son ciertos los datos consignados en este formulario y declaraci\u00F3n.\n"
							+ "3. Que el producto objeto de esta comunicaci\u00F3n de puesta en el mercado cumple con los requisitos establecidos conforme a la legislaci\u00F3n que le es de aplicaci\u00F3n.\n"
							+ "4. Que se compromete a aportar la documentaci\u00F3n necesaria cuando se le requiera por parte de la autoridad competente para que subsane una falta o acompa&nacute;e los documentos preceptivos.",
					COMP_ALIM_MOD: "DECLARACI\u00D3N\ DE RESPONSABILIDAD:"
							+ "El firmante, con capacidad jur\u00EDdica y de obrar suficiente para este acto, en su propio nombre o en representaci\u00F3n "
							+ "de la empresa, y con relaci\u00F3n a la comunicaci\u00F3n de la modificaci\u00F3n  de este complemento alimenticio, declara bajo su responsabilidad:\n"
							+ "1. Haber le\u00EDdo y comprendido las instrucciones que acompa\u00F1an al presente formulario.\n"
							+ "2. Que son ciertos los datos consignados en este formulario y declaraci\u00F3n.\n"
							+ "3. Que el producto objeto de esta comunicaci\u00F3n de modificaci\u00F3n cumple con los requisitos establecidos conforme a la legislaci\u00F3n que le es de aplicaci\u00F3n.\n"
							+ "4. Que se compromete a aportar la documentaci\u00F3n necesaria cuando se le requiera por parte de la autoridad competente para que subsane una falta o acompa&nacute;e los documentos preceptivos.",
					COMP_ALIM_CESE: "DECLARACI\u00D3N\ DE RESPONSABILIDAD:"
							+ "El firmante, con capacidad jur\u00EDdica y de obrar suficiente para este acto, en su propio nombre o en representaci\u00F3n "
							+ "de la empresa, y con relaci\u00F3n a la comunicaci\u00F3n del cese de comercializaci\u00F3n de este complemento alimenticio, declara bajo su responsabilidad:\n"
							+ "1. Haber le\u00EDdo y comprendido las instrucciones que acompa\u00F1an al presente formulario.\n"
							+ "2. Que son ciertos los datos consignados en este formulario y declaraci\u00F3n.\n"
							+ "3. Que el producto objeto de esta comunicaci\u00F3n de cese de comercializaci\u00F3n cumple con los requisitos establecidos conforme a la legislaci\u00F3n que le es de aplicaci\u00F3n.\n"
							+ "4. Que se compromete a aportar la documentaci\u00F3n necesaria cuando se le requiera por parte de la autoridad competente para que subsane una falta o acompa&nacute;e los documentos preceptivos.",
				}).constant('CREDENTIALS_COOKIES', 'CLIENTCERT').constant('CREDENTIALS_COOKIES_ERROR', 'CLIENTCERTERROR').constant('REDIRECT_COOKIES', 'REDIRECT').constant('IMAGES_URL',
				'/secosan/images').constant('POPUP_IMAGES', {
			info : "info.png",
			exclamation : "warning.png",
			question : "help.png",
			ok : "ok.png",
			error : "error.png"
		}).constant('IDLE_TIME_SECONDS', 30).constant('ATTACHABLE_FILE_NAME', {
			FICHA_TECNICA_INCLUSION : 'Ficha_Tecnica_Inclusion.pdf',
			FICHA_TECNICA_ALTERACION : 'Ficha_Tecnica_Alteracion.pdf'
		}).constant('CATALOGUE_NAMES', {
			COUNTRIES : "Pais",
			PROVINCIES : "Provincia",
			MUNICIPALITY : "Municipio",
			SOLICITUDE_STATUS : "EstadoSolicitud",
			SOLICITUDE_TYPE : "TipoFormulario",
			DOCUMENT_TYPE : "DocumentType",
			FORMATS : "Forma",
			USER_AREA : "Areas",
			USER_ROLE : "PermisoUsuarios",
			SUSPENSION_TYPES : "TipoSuspensionFinanciacion",
			ADMINISTRATION_MODE : "ModoAdministracion",
			PRODUCT_TYPE : "TipoProducto",
			PAYMENT_TYPE : "TipoPago",
			PAYMENT : "FormaPago",
			MUNICIPALITY_PARENT_COUNTRY : "Pais_id",
			MUNICIPALITY_PARENT_PROVINCIE : "Provincia_id",
			SPAIN_COUNTRY_CODE : "6",
			SUSPENSION_CODE : "1",
			PAYMENT_TASA_CODE : "1",
			OTHER_FORM_CODE : "8",
			SHIPPING_TYPE : "TipoEnvio"
		}).constant('PAYMENT_TYPE', {
			EFECTIVO : 1,
			ADEUDO_CUENTA : 2,
			ONLINE_TARJETA : 3,
			ONLINE_CUENTA : 4
		}).constant('MONTHS_TWO_DIGITS_SELECT', [ {
			"catalogId" : "01",
			"catalogValue" : "01"
		}, {
			"catalogId" : "02",
			"catalogValue" : "02"
		}, {
			"catalogId" : "03",
			"catalogValue" : "03"
		}, {
			"catalogId" : "04",
			"catalogValue" : "04"
		}, {
			"catalogId" : "05",
			"catalogValue" : "05"
		}, {
			"catalogId" : "06",
			"catalogValue" : "06"
		}, {
			"catalogId" : "07",
			"catalogValue" : "07"
		}, {
			"catalogId" : "08",
			"catalogValue" : "08"
		}, {
			"catalogId" : "09",
			"catalogValue" : "09"
		}, {
			"catalogId" : "10",
			"catalogValue" : "10"
		}, {
			"catalogId" : "11",
			"catalogValue" : "11"
		}, {
			"catalogId" : "12",
			"catalogValue" : "12"
		}, ]).constant('YEARS_TWO_DIGITS_SELECT', [ {
			"catalogId" : "16",
			"catalogValue" : "16"
		}, {
			"catalogId" : "17",
			"catalogValue" : "17"
		}, {
			"catalogId" : "18",
			"catalogValue" : "18"
		}, {
			"catalogId" : "19",
			"catalogValue" : "19"
		}, {
			"catalogId" : "20",
			"catalogValue" : "20"
		}, {
			"catalogId" : "21",
			"catalogValue" : "21"
		}, {
			"catalogId" : "22",
			"catalogValue" : "22"
		}, {
			"catalogId" : "23",
			"catalogValue" : "23"
		}, {
			"catalogId" : "24",
			"catalogValue" : "24"
		}, {
			"catalogId" : "25",
			"catalogValue" : "25"
		}, {
			"catalogId" : "26",
			"catalogValue" : "26"
		}, {
			"catalogId" : "27",
			"catalogValue" : "27"
		}, ]).constant('VALIDATION_PATTERN', {
			NIF : /^\d{8}[a-zA-Z]$/,
			PHONE : /(00\d{2})*\d{9,12}/,
			EMAIL : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
			POSTAL_CODE : /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/
		}).constant('DOCUMENT_UPLOAD_MAGICNUMBER_REGEX',
				{
					expected:/%(PDF|pdf)-[0-9\.]+/
				}
		).constant('PASSWORD_REGEX',{regex:/(?=.*\d)(?=.*[\u0021-\u002E\u003c-\u0040])(?=.*[A-Z])(?=.*[a-z])\S{8,16}/g})
		.factory('httpResponseInterceptor',
				[ '$q', '$location', '$rootScope', '$cookieStore', 'CREDENTIALS_COOKIES', '$log', function($q, $location, $rootScope, $cookieStore, CREDENTIALS_COOKIES, $log) {

					return {
						'request' : function(config) {
							var overriddenMethods = new RegExp('patch|put|delete', 'i');
							config.headers = config.headers || {};

							if (!angular.equals($rootScope.userCredentials, undefined) && !angular.equals($rootScope.userCredentials.token, undefined)) {
								config.headers.Authorization = 'Bearer ' + $rootScope.userCredentials.token;
							}

							if (overriddenMethods.test(config.method)) {
								config.headers['X-HTTP-Method-Override'] = config.method;
								config.method = 'POST';
							}
							return config;
						},
						'response' : function(response) {
							if (!(angular.equals(response, undefined)) && !(angular.equals(response.headers, undefined))) {
								var newToken = response.headers('AuthorizationRenewal');
								if (!(angular.equals(newToken, undefined)) && !(angular.equals(newToken, null)) && !(angular.equals(newToken, "")) && !(angular.equals(newToken, {}))) {

									newToken = {
										token : newToken
									};
									$rootScope.userCredentials.token = newToken.token;
									$cookieStore.put(CREDENTIALS_COOKIES, newToken);
									$log.info("token renewed:" + newToken);
								}

							}
							return response;
						},
						'responseError' : function(response) {
							if (response.status === 401 || response.status === 403) {
								$location.path('/login');
								$rootScope.userCredentials = undefined;
								$cookieStore.remove(CREDENTIALS_COOKIES);

							}
							return $q.reject(response);
						}
					};
				} ]).config([ '$httpProvider', function($httpProvider) {
			$httpProvider.interceptors.push('httpResponseInterceptor');
			if (!$httpProvider.defaults.headers.get) {
				$httpProvider.defaults.headers.get = {};
			}
			$httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
			$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
			$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
			
		} ]).config([ '$cookiesProvider', function($cookiesProvider) {
			 $cookiesProvider.defaults.path = '/';
		} ])
	.constant('TABS_CONFIGURATION', {
	ALIMENTOS_GRUPOS_CESE: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/cese/solicitud.html",
            },
            {
                id: "tab-cese-comer",
                autoload: true,
                title: "CESE_COMER",
                head: {
                    icon: 'icon-note',
                    text: 'CESE_COMER',
                    title: 'CESE_COMER'
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/cese/form-ag-cese.html",
            },
            {
                id: "tab-documentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/cese/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",
                isSent: true,
                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    ALIMENTOS_GRUPOS_MODIFICACION: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/modificacion/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-modificacion",
                title: "MODIFICATIONS",
                head: {
                    icon: 'icon-note',
                    text: 'MODIFICATIONS',
                    title: 'MODIFICATIONS'
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/modificacion/form-ag-modificacion.html",
            },
            {
                autoload: true,
                id: "tab-documentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/modificacion/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",
                isSent: true,
                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    ALIMENTOS_GRUPOS_PUESTA_MERCADO: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/puestaMercado/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-aditionaldata",
                title: "ADDITIONAL_DATA",
                head: {
                    icon: 'icon-note',
                    text: 'ADDITIONAL_DATA',
                    title: 'ADDITIONAL_DATA'
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/puestaMercado/form-ag-identificativos-adicionales.html",
            },
            {
                autoload: true,
                id: "tab-puestamercado",
                title: "PUESTA_MERCADO",
                head: {
                    icon: 'icon-note',
                    text: 'PUESTA_MERCADO',
                    title: 'PUESTA_MERCADO'
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/puestaMercado/form-ag-puesta-mercado.html",
            },
            {
                autoload: true,
                onlyEdit: true,
                id: "tab-pagotasas",
                title: "PAGO_TASAS",
                head: {
                    icon: 'icon-note',
                    text: 'PAGO_TASAS',
                    title: 'PAGO_TASAS'
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/puestaMercado/pago-tasa-en-linea.html",
            },
            {
                autoload: true,
                id: "tab-documentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/alimentosGrupos/puestaMercado/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",
                isSent: true,
                onlyEdit: true,
                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    COMPLEMENTOS_ALIMENTICIOS_CESE: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/cese/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-cese-comer",
                title: "CESE_COMER",
                head: {
                    icon: 'icon-note',
                    text: 'CESE_COMER',
                    title: 'CESE_COMER'
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/cese/form-ca-cese.html",
            },
            {
                autoload: true,
                id: "tab-documentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/cese/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    COMPLEMENTOS_ALIMENTICIOS_MODIFICACION: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/modificacion/solicitud.html",
            },
            {
                id: "tab-modificacion",
                autoload: true,
                title: "MODIFICATIONS",
                head: {
                    icon: 'icon-note',
                    text: 'MODIFICATIONS',
                    title: 'MODIFICATIONS'
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/modificacion/form-ca-modificacion.html",
            },
            {
                id: "tab-documentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/modificacion/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },

        ]
    },
    COMPLEMENTOS_ALIMENTICIOS_PUESTA_MERCADO: {
        tabList: [
            {
                id: "tab-solicitud",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/puestaMercado/solicitud.html",
            },
            {
                id: "tab-aditionaldata",
                autoload: true,
                title: "ADDITIONAL_DATA",
                head: {
                    icon: 'icon-note',
                    text: 'ADDITIONAL_DATA',
                    title: 'ADDITIONAL_DATA'
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/puestaMercado/form-ca-identificativos-adicionales.html",
            },
            {
                id: "tab-puestamercado",
                autoload: true,
                title: "PUESTA_MERCADO",
                head: {
                    icon: 'icon-note',
                    text: 'PUESTA_MERCADO',
                    title: 'PUESTA_MERCADO'
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/puestaMercado/form-ca-puesta-mercado.html",
            },
            {
                id: "tab-pagotasas",
                autoload: true,
                // onlyEdit: true,
                title: "PAGO_TASAS",
                head: {
                    icon: 'icon-note',
                    text: 'PAGO_TASAS',
                    title: 'PAGO_TASAS'
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/puestaMercado/pago-linea-tasas.html",
            },
            {
                id: "tab-documentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/complementosAlimenticios/puestaMercado/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    COMUNICACION_QUEJAS: {
        tabList: [

            {
                id: "tab-solicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/comunicacion/queja/solicitud.html",
            },
            {
                id: "tab-quejaunidad",
                autoload: true,
                title: "DATOS_UNIDAD_1",
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_UNIDAD_1',
                    title: 'DATOS_UNIDAD_1'
                },
                templateUrl: "/secosan/partials/directives/comunicacion/queja/queja.html",
            },
            {
                autoload: true,
                id: "tab-quejadocumentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/comunicacion/queja/validacion-queja.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    COMUNICACION_SUGERENCIAS: {
        tabList: [
            {
                id: "tab-sujerenciasolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/comunicacion/sugerencia/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-sujerenciaunidad",
                title: "DATOS_UNIDAD_1",
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_UNIDAD_1',
                    title: 'DATOS_UNIDAD_1'

                },
                templateUrl: "/secosan/partials/directives/comunicacion/sugerencia/sugerencia.html",

            },
            {
                autoload: true,
                id: "tab-sujerenciadocumentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'

                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",

            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/comunicacion/sugerencia/validacion-sugerencia.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    EVALUACION_RIESGOS: {
        tabList: [
            {
                id: "tab-evaluacionriesgossolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/evaluacionRiesgos/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-evaluacionriesgosservice",
                title: "DATA_SERVICE",
                head: {
                    icon: 'icon-note',
                    text: 'DATA_SERVICE',
                    title: 'DATA_SERVICE'

                },
                templateUrl: "/secosan/partials/directives/evaluacionRiesgos/evaluacion-riesgos.html",

            },

            {
                autoload: true,
                id: "tab-evaluacionriesgosdocumentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'

                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",

            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/evaluacionRiesgos/validacion-evaluacionriesgos.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    FINA_ALIM_ALTERACION: {
        tabList: [
            {
                id: "tab-finaalimalteracionsolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/alteracion/solicitud.html",
            },
            {
                id: "tab-finaalimalteracionanexo",
                autoload: true,
                title: "ANEXO_III",
                head: {
                    icon: 'icon-note',
                    text: 'ANEXO_III',
                    title: 'ANEXO_III'

                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/alteracion/form-alteracion.html",
            },
            {
                id: "tab-finaalimalteraciondocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'

                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/alteracion/validacion-finaalimalteracion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    FINA_ALIM_INCLUSION: {
        tabList: [
            {
                id: "tab-finaaliminclusionsolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/inclusion/solicitud.html",
            },
            {
                id: "tab-finaaliminclusionanexo",
                autoload: true,
                title: "ANEXO_II",
                head: {
                    icon: 'icon-note',
                    text: 'ANEXO_II',
                    title: 'ANEXO_II'

                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/inclusion/anexoii.html",

            },
            {
                id: "tab-pagotasas",
                autoload: true,
                onlyEdit: true,
                title: "PAGO_TASAS",
                head: {
                    icon: 'icon-note',
                    text: 'PAGO_TASAS',
                    title: 'PAGO_TASAS'

                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/inclusion/pago-tasa-en-linea.html",

            },
            {
                id: "tab-finaaliminclusiondocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'

                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/inclusion/validacion-finaaliminclusion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    FINA_ALIM_SUSPENSION: {
        tabList: [
            {
                id: "tab-finaalimsuspensionsolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/suspension/solicitud.html",
            },
            {
                id: "tab-finaalimsuspensionanexo",
                autoload: true,
                title: "ANEXO_IV",
                head: {
                    icon: 'icon-note',
                    text: 'ANEXO IV',
                    title: 'ANEXO IV'

                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/suspension/form-suspension.html",

            },
            {
                id: "tab-finaalimsuspensiondocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'

                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/financiacionAlimentos/suspension/validacion-finaalimsuspension.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    LMR_FITOSANITARIOS: {
        tabList: [
            {
                id: "tab-lmrfitosanitariossolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/lmrFitosanitarios/solicitud.html",
            },
            {
                id: "tab-lmrfitosanitariossolicitude",
                autoload: true,
                title: "DATA_SOLICITUDE",
                head: {
                    icon: 'icon-note',
                    text: 'DATA_SOLICITUDE',
                    title: 'DATA_SOLICITUDE'
                },
                templateUrl: "/secosan/partials/directives/lmrFitosanitarios/form-lmr-fitosanitarios.html",
            },
            {
                id: "tab-pagotasas",
                autoload: true,
                onlyEdit: true,
                title: "PAGO_TASAS",
                head: {
                    icon: 'icon-note',
                    text: 'PAGO_TASAS',
                    title: 'PAGO_TASAS'

                },
                templateUrl: "/secosan/partials/directives/lmrFitosanitarios/pago-tasa-en-linea.html",

            },
            {
                id: "tab-lmrfitosanitariosdocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/lmrFitosanitarios/validacion-lmrfitosanitarios.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    REGISTRO_AGUAS: {
        tabList: [
            {
                id: "tab-registroaguassolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/registroAguas/solicitud.html",
            },
            {
                id: "tab-registroaguasdatosagua",
                autoload: true,
                title: "DATOS_AGUA",
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_AGUA',
                    title: 'DATOS_AGUA'
                },
                templateUrl: "/secosan/partials/directives/registroAguas/form-registro-aguas.html",
            },
            {
                id: "tab-registroaguasdocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/registroAguas/validacion-registroaguas.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    REGISTRO_INDUSTRIAS: {
        tabList: [
            {
                id: "tab-registroindustriassolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/registroIndustrias/solicitud.html",
            },
            {
                id: "tab-registroindustriasdatos",
                autoload: true,
                title: "DATOS_INDUSTRIA",
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_INDUSTRIA',
                    title: 'DATOS_INDUSTRIA'
                },
                templateUrl: "/secosan/partials/directives/registroIndustrias/form-registro-industrias.html",
            },
            {
                id: "tab-pagotasas",
                autoload: true,
                onlyEdit: true,
                title: "PAGO_TASAS",
                head: {
                    icon: 'icon-note',
                    text: 'PAGO_TASAS',
                    title: 'PAGO_TASAS'
                },
                templateUrl: "/secosan/partials/directives/registroIndustrias/pago-tasa-en-linea.html",
            },
            {
                id: "tab-registroindustriasdocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/registroIndustrias/validacion-registroindustrias.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    SOLICITUD_LOGOS: {
        tabList: [
            {
                id: "tab-solicitudlogossolicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"

                },
                templateUrl: "/secosan/partials/directives/solicitudLogos/solicitud.html",
            },
            {
                id: "tab-solicitudlogosfinalidad",
                autoload: true,
                title: "Finalidad",
                head: {
                    icon: 'icon-note',
                    text: 'Finalidad',
                    title: 'Finalidad'
                },
                templateUrl: "/secosan/partials/directives/solicitudLogos/form-solicitud-logos.html",
            },
            {
                id: "tab-solicitudlogosdocumentacion",
                autoload: true,
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/solicitudLogos/validacion-solicitudlogos.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ]
    },
    PROCEDIMIENTO_GENERICO: {
        tabList: [

            {
                id: "tab-procedimiento-general-solicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/procedimientoGenerico/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-procedimiento-general-solicitud",
                title: "DATOS_SOLICITUD",
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLICITUD',
                    title: 'DATOS_SOLICITUD'
                },
                templateUrl: "/secosan/partials/directives/procedimientoGenerico/datos-solicitud.html",
            },
            {
                autoload: true,
                id: "tab-documentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/procedimientoGenerico/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    REGISTRO_REACU: {
        tabList: [

            {
                id: "tab-registro-estatal-solicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/registroEstatal/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-registro-estatal-solicitud",
                title: "LUGAR_O_MEDIO",
                head: {
                    icon: 'icon-note',
                    text: 'LUGAR_O_MEDIO',
                    title: 'LUGAR_O_MEDIO'
                },
                templateUrl: "/secosan/partials/directives/registroEstatal/datos-solicitud.html",
            },
            {
                autoload: true,
                id: "tab-documentacion-reacu",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/registroEstatal/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
        ],
    },
    ASOCIACION_CONSUMIDORES: {
        tabList: [
            {
                id: "tab-asociacion-consumidores-solicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/subvencionAsocuae/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-asociacion-consumidores-solicitud",
                title: "LUGAR_O_MEDIO",
                head: {
                    icon: 'icon-note',
                    text: 'LUGAR_O_MEDIO',
                    title: 'LUGAR_O_MEDIO'
                },
                templateUrl: "/secosan/partials/directives/subvencionAsocuae/datos-solicitud.html",
            },
            {
                autoload: true,
                id: "tab-documentacion",
                title: "DOCUMENTACION_ASOC",
                head: {
                    icon: 'icon-note',
                    text: 'DOCUMENTACION_ASOC',
                    title: 'DOCUMENTACION_ASOC'
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/subvencionAsocuae/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            }
        ],
    },
    BECAS_CONSUMIDORES: {
        tabList: [
            {
                id: "tab-becas-solicitante",
                autoload: true,
                title: 'DATOS_SOLI',
                head: {
                    icon: 'icon-note',
                    text: 'DATOS_SOLI',
                    title: "DATOS_SOLI"
                },
                templateUrl: "/secosan/partials/directives/becas/solicitud.html",
            },
            {
                autoload: true,
                id: "tab-documentacion-becas",
                title: "ACADEMIC_DATA",
                head: {
                    icon: 'icon-note',
                    text: 'ACADEMIC_DATA',
                    title: 'ACADEMIC_DATA'
                },
                templateUrl: "/secosan/partials/directives/becas/documentacion.html",
            },
            {
                autoload: true,
                id: "VALIDTAB",
                onlyEdit: true,
                title: "VALIDATION_TITLE",
                head: {
                    text: 'VALIDATION_TITLE',
                },
                templateUrl: "/secosan/partials/directives/becas/validacion.html",
            },
            {
                autoload: true,
                id: "tab-historial-solictud",

                title: "HISTORY_TITLE",
                head: {
                    text: 'HISTORY_TITLE',
                },
                templateUrl: "/secosan/partials/directives/generics/formularios/historial-solicitud-container.html",
            },
            {
                autoload: true,
                id: "tab-resolucion",
                isSent: true,
				onlyAdmin:true,
                title: "RESOLUTION",
                head: {
                    text: 'RESOLUTION',
                },
                templateUrl: "/secosan/partials/directives/becas/resolucion-container.html",
            }
        ],
    }
}).constant('PROCEDIMIENTOS_ALTERNOS',{
    //Utilizado para ocultar o mostrar diversos flujos dentro del area de cambio de estado, se define por el nombre y la lista de
    //ids de los tipos de  formularios para los cuales se mostrará
    // 1 Puesta en el Mercado de Complementos Alimenticios
    // 2 Modificación Datos de Complementos Alimenticios
    // 3 Cese de Comercialización de Complementos Alimenticios
    // 4 Puesta en el Mercado de Alimentos  para Grupos Específicos
    // 5 Modificación Datos de Alimentos para Grupos Específicos
    // 6 Cese Comercialización de Alimentos para Grupos Específicos
    // 7 Evaluación de Riesgos
    // 8 Quejas
    // 9 Sugerencia
    // 10 Logos, Imágenes y documentación
    // 11 Inclusión Usos Médicos Especiales
    // 12 Alteración Usos Médicos Especiales
    // 13 Suspensión o Baja Usos Médicos Especiales
    // 14 LRM de Productos Fitosanitarios
    // 15 Solicitud para el reconocimiento de aguas minerales naturales de paises no pertenecientes a la U. E.
    // 16 Solicitud de certificado de empresa
    // 17 Procedimiento General
    // 21 Subvenciones a las juntas arbitrales de consumo
    // 19 Subvenciones a asociaciones de consumidores y usuarios de ámbito estatal
    // 20 Becas de formación para el programa de trabajo del consejo de consumidores y usuarios
    // 18 Inscripción en el Registro Estatal de Asociaciones de Consumidores y Usuarios

    'PORTAFIRMA':[1,4,7,11,12,13,14,15,16],
    'REMISION' : [11,12,13],
    'VALORACION_WORKFLOW' : [20]
});