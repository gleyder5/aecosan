angular.module('secosanGeneric').config(setConfigPhaseSettings);
setConfigPhaseSettings.$inject = [ "ngTableFilterConfigProvider" ];
function setConfigPhaseSettings(ngTableFilterConfigProvider) {
	var filterAliasUrls = {
		"date-range-filter" : "/secosan/partials/ngtable/date-range-filters.html",
	};
	ngTableFilterConfigProvider.setConfig({
		aliasUrls : filterAliasUrls
	});
};
angular.module('secosanGeneric').config(function(datepickerConfig, datepickerPopupConfig) {
	datepickerConfig.showWeeks = false;
	datepickerPopupConfig.showButtonBar = true;
	datepickerPopupConfig.currentText = "Hoy";
	datepickerPopupConfig.clearText = "Borrar";
	datepickerPopupConfig.closeText = "Cerrar";
});
angular.module('secosanGeneric').filter("dateRangeFilter", function() {
	return function(params, dateValues, parameterName) {
		var filterParamName = parameterName;
		if (filterParamName === undefined || filterParamName === "") {
			filterParamName = 'dateRangeFilter';
		}
		if (dateValues) {
			if (dateValues.to !== undefined || dateValues.from !== undefined) {
				params.filter()[filterParamName] = dateValues;
			} else if (params.filter()[filterParamName] !== undefined && (dateValues.to === undefined && dateValues.from === undefined)) {
				delete params.filter()[filterParamName];
			}
		}
		return params;
	};
});