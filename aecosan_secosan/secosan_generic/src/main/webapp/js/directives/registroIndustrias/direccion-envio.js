angular.module('secosanGeneric').directive('direccionEnvio', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_INDUSTRIAS + "direccion-envio.html"
	};
});
