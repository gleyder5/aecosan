angular.module('secosanGeneric').directive('containerRegistroIndustrias', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_INDUSTRIAS + "container-registro-industrias.html"
	};
});
