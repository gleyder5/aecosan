angular.module('secosanGeneric').directive('formRegistroIndustrias', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_INDUSTRIAS + "form-registro-industrias.html"
	};
});
