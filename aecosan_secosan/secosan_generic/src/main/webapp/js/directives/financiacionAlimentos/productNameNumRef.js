angular.module('secosanGeneric').directive('productNameNumRef', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		scope:{
			compClassNumRef:"@",
			compClassProductName:"@",
			productReferenceNumber:"=",
			formularioATratar:"=",
			viewmode :'='
		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "productNameNumRef.html"
	};
});
