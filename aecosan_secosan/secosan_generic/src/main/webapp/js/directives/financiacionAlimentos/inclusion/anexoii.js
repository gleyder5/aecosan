angular.module('secosanGeneric').directive('formAnexoii', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "inclusion/anexoii.html"
	};
});
