angular.module('secosanGeneric').directive('formSuspension', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "suspension/form-suspension.html"
	};
});
