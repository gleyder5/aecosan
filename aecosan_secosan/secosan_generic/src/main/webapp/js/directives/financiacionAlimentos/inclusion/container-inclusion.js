angular.module('secosanGeneric').directive('containerInclusion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "inclusion/container-inclusion.html"
	};
});
