angular.module('secosanGeneric').directive('productos', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "/suspension/productos.html"
	};
});