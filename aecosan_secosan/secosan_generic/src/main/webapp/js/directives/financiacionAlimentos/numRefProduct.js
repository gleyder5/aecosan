angular.module('secosanGeneric').directive('numRefProduct', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		scope:{
			compClass:"@",
			viewmode:'=',
			productReferenceNumber:"="
		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "numRefProduct.html"
	};
});
