angular.module('secosanGeneric').directive('formAlteracion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "alteracion/form-alteracion.html"
	};
});
