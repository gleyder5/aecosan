angular.module('secosanGeneric').directive('formInclusion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "inclusion/form-inclusion.html"
	};
});
