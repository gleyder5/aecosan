angular.module('secosanGeneric').directive('containerAlteracion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "alteracion/container-alteracion.html"
	};
});
