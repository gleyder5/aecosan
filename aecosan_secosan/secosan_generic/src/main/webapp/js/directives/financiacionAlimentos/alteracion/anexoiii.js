angular.module('secosanGeneric').directive('formAnexoiii', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "alteracion/anexoiii.html"
	};
});
