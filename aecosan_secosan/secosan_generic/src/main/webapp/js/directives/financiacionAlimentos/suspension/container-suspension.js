angular.module('secosanGeneric').directive('containerSuspension', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FINANCIACION + "suspension/container-suspension.html"
	};
});