angular.module('secosanGeneric').directive('containerBecas', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_BECAS + "container-becas.html"
		// templateUrl : TEMPLATE_PATHS.DIRECTIVES_BECAS + "container-becas-recsara.html"
	};
});
