angular.module('secosanAdmin').directive(
    'resolucion',
    function ($log, $modal, ngDialog, NgTableParams,DatePickerPopup, Becas, TEMPLATE_PATHS, $routeParams) {
        return {
            restrict: 'E',
            templateUrl: TEMPLATE_PATHS.DIRECTIVES_BECAS + 'resolucion.html',
            controller: function ($scope, $modal, $filter) {
                var $this = this;
                this.setupController = function() {
                    if ($this.configTableParams !== undefined) {
                        $this.configTableParams.filter();
                        $this.configTableParams.reload();
                    }
                }

                $this.becasList = [];


                    $this.configParams = new NgTableParams({
                        count : 5,
                    }, {
                        counts : [],
                        paginationMaxBlocks : 3,
                        paginationMinBlocks : 2,
                        getData : function($defer, params) {
                            Becas.getBecasSolicitantes(params).success(function (data) {
                                $scope.becasList = data.data;
                                var total = data.recordsTotal;
                                params.total(total);
                                $scope.hasdata = $this.hasData(total);
                                $defer.resolve(data.data);
                            }).error(function(data, status, headers, config) {
                                $scope.hasdata = false;
                            });
                        }
                    });


                $this.setupController();
                $this.hasData = function(recordsTotal) {
                    return recordsTotal > 0
                };

                $scope.configTableParams = $this.configParams;
                $scope.becasList = $this.becasList;
            },
        }
    });