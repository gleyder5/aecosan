angular.module('secosanGeneric').directive('containerSubvencionAsocuae', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ASOCIACION_CONSUMIDORES + "container-subvencion-asocuae.html"
		// templateUrl : TEMPLATE_PATHS.DIRECTIVES_ASOCIACION_CONSUMIDORES + "container-subvencion-asocuae-recsara.html"
	};
});
