angular.module('secosanGeneric').directive('containerSubvencionJuntas', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ASOCIACION_JUNTAS + "container-subvencion-juntas.html"
		// templateUrl : TEMPLATE_PATHS.DIRECTIVES_ASOCIACION_CONSUMIDORES + "container-subvencion-asocuae-recsara.html"
	};
});
