angular.module('secosanGeneric').directive('containerRegistroEstatal', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_REGISTRO_ESTATAL + "container-registro-estatal.html"
		// templateUrl : TEMPLATE_PATHS.DIRECTIVES_REGISTRO_ESTATAL + "container-registro-estatal-recsara.html"
	};
});