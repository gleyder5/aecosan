angular.module("secosanGeneric").filter('decimalToComma', [  
        function() {
            return function() {
                var ret=(input)?input.toString().replace(".",","):null;
                if(ret){
                    var decArr=ret.split(",");
                    if(decArr.length>1){
                        var dec=decArr[1].length;
                        if(dec===1){ret+="0";} //esto es para mostrar precios  asi 10,20 en lugar de 10,2
                    }
                }
                return ret;
            };
        }
    ]
);