angular.module('secosanGeneric').directive('spinnerLoading', function($log, Spinner,$timeout,IMAGES_URL) {
	var $this = this;
	var elementId ="spinner";
	var divIdLoadingBackground = "loading-div-background";
	var divIdLoadingDiv ="loading-div";
	
	var loadDiv = "<div id='"+divIdLoadingBackground+"'> <div id='"+divIdLoadingDiv+"' ><img src='"+IMAGES_URL+"/loading.gif' alt='Cargando...' /></div></div>";
	this.showSpinner = function(element){
	 $timeout(function () {
		   element.append(loadDiv);
		  $("#loading-div-background").show();
	  }, 1, false);
	 
	};
	this.hideSpinner = function(element){
		 $timeout(function () {
			 $("#"+divIdLoadingBackground).hide();
			 $('#'+divIdLoadingBackground).remove();
		 }, 1, false);
	}
	return {
		restrict : 'E',
		template : "<div id='"+elementId+"'></div>",
		scope : {
			
		},
		link : function(scope, element, $timeout) {

			scope.$watch(function() {
				return Spinner.loadSpinner();
			}, function(newVal, oldVal) {
				if(newVal){
					$this.showSpinner(element);
					
				}else{
					$this.hideSpinner(element);
				}
			}, true);

			scope.$on('$destroy', function(element) {
				$this.hideSpinner(element);
			});
			
		}
	};
});