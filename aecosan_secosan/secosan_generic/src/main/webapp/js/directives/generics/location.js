angular.module('secosanGeneric').directive(
		'location',
		function($window,$log, $interval,SetDataFromCatalogService) {
			var $this = this;
			return{
				restrict : 'E',
				templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS
						+ "form-location.html",
				scope : {
					paisesEU : '@',
					soloEspana : "@"
				},
			}
		});