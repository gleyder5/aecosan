angular.module('secosan').directive('pagoBotones', function($log,PagoService,TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "pago-botones.html",
		scope:{
			fomularioATratar:"=",
			solicitudeType:"=",
			serviceId:"=",
			tasatipo:"="
		},
		controller: function($scope){
			$scope.showPago = function(){
				
				var formularioATratar = $scope.fomularioATratar;
				var idSolicitude =  formularioATratar.identificadorPeticion;
				var solicitudeType = $scope.solicitudeType;
				var solicitudePayment =  formularioATratar.solicitudePayment;
				var serviceId =  $scope.serviceId;
				var popupParams = {
						'idSolicitude' : idSolicitude,
						'idSolicitudeType': solicitudeType,
						'paymentData' :solicitudePayment,
						'serviceId':serviceId
					}
				$log.debug(popupParams);
				PagoService.showPagoPanel(popupParams);
			}
		
			$scope.showPagoPanel = function(){
				
				var formularioATratar = $scope.fomularioATratar;
				var idSolicitude =  formularioATratar.identificadorPeticion;
				var solicitudeType = $scope.solicitudeType;
				var solicitudePayment  =  formularioATratar.solicitudePayment;
				var serviceId =  $scope.serviceId;
				var popupParams = {
						'idSolicitude' : idSolicitude,
						'idSolicitudeType': solicitudeType,
						'paymentData' :solicitudePayment,
						'serviceId':serviceId
					}
				$log.debug(popupParams);
				PagoService.showPagoPanel(popupParams);
			}
		}
	}
});