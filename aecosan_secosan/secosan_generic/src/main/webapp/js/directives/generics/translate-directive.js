angular.module('secosanGeneric').directive('secosanTranslate', function($log, $translate, GENERIC_TEMPLATE_PATHS) {
	var $this = this;

	this.spanish = {
		"value" : "es_ES",
		"name" : "spanish"
	};
	this.britishEnglish = {
		"value" : "en_GB",
		"name" : "britishEnglish"
	};
	this.setLanguage = function(langKey) {
		$translate.use(langKey);
	};
	return {
		restrict : 'E',
		scope : {

		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + 'secosan-translate.html',
		controller : function($scope) {
			$scope.selectedLanguaje = $this.spanish.name;

			$scope.setSpanish = function() {
				$this.setLanguage($this.spanish.value);
				$scope.selectedLanguaje = $this.spanish.name;
			}, $scope.setBritishEnglish = function() {
				$this.setLanguage($this.britishEnglish.value);
				$scope.selectedLanguaje = $this.britishEnglish.name;
			}
			$scope.isActive = function(lang) {
				return lang === $scope.selectedLanguaje;
			}
		}
	}
});