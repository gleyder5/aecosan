angular.module('secosanGeneric').directive('botonesTabs', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "botones-tabs.html",
	
		link : function(scope, elem, attr, ctrl) {	
			scope.back = function(event,currentIndex){
					event.preventDefault();
					scope.$parent.triggerValidations();
					if(currentIndex>0)
						scope.workspaceTabs.backTab(currentIndex);
					
			};
			scope.next = function(event,currentIndex){
					event.preventDefault();
					scope.$parent.triggerValidations();
					if(currentIndex<scope.workspaceTabs.tabList.length)
						scope.workspaceTabs.nextTab(currentIndex);
			};
		
		},
	};
});