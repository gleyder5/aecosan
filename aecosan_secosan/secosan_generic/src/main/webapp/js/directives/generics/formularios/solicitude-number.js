angular.module('secosanGeneric').directive('solicitudeNumber', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "solicitude-number.html",
		scope : {
			number : '=',
			title : "@"
		}
	};
});