angular.module('secosanGeneric').directive('cambioContrasenia', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "cambio-contrasenia.html"
	};
});