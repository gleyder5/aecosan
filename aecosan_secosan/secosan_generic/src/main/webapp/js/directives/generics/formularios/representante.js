angular.module('secosanGeneric').directive('representante', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "representante.html",
		controller:function($scope,IS_ADMIN){
			$scope.IS_ADMIN  = IS_ADMIN;
		}
	};
});