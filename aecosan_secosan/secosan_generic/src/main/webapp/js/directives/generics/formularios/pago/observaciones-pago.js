angular.module('secosan').directive('observacionesPago', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "observaciones-pago.html"
	};
});