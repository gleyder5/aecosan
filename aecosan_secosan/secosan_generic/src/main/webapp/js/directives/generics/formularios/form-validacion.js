angular.module('secosanGeneric').directive(
		'formValidacion',
		function($timeout, ERR_MSG, TEMPLATE_PATHS, ValidationUtilsService) {
			return {
				restrict : 'E',
				templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS
						+ "form-validacion.html",
				require : '^form',
				replace : false,
				scope : {
					formIdentifier : '@',
					heading : "@",
					allValid : "="
				},
				link : function(scope, el, attrs, formCtrl) {
					scope.hasError = false;

					var scopedVars = {};
					
					scopedVars.formCtrl = formCtrl;
				
					scopedVars.formDomElement = $('#'+scope.formIdentifier)[0];
					

					var keyId = scope.formIdentifier;
					ValidationUtilsService.register(keyId , scopedVars,scope);
                    scope.$on('validation-listener-registered', function(event,action) {
                        if(action.set) {
                            $compile()(scope);
                        }
                    });



                    scope.$on('$destroy', function() {
						ValidationUtilsService.unregister(scope.formIdentifier);
					});
				}
			}
		});