angular.module('secosanGeneric').directive('secosanHeader', function($log, GENERIC_TEMPLATE_PATHS,IMAGES_URL, HeaderTitle, HeaderSolicitudNumber) {
	var $this = this;

	this.watchTitle = function(scope) {
		scope.$watch(function() {
			return HeaderTitle.getTitles();
		}, function(newVal) {
			$this.setupTitle(newVal, scope);
		}, true);
	};

	this.setupTitle = function(titleValue, scope) {
		if (!angular.isUndefined(titleValue)) {
			scope.title = titleValue.title;
			scope.subtitle = titleValue.subtitle;
			if (!angular.isUndefined(titleValue.title) && !angular.equals(titleValue.title, "")) {
				scope.showTitle = true;
			}
		} else {
			scope.showTitle = false;
			HeaderTitle.clearTitles();
		}

	};
	this.watchSolicitudNumber = function(scope) {
		scope.$watch(function() {
			return HeaderSolicitudNumber.getNumberAndTitle();
		}, function(newVal) {
			$this.setupSolicitudNumber(newVal, scope);
		}, true);
	};

	this.setupSolicitudNumber = function(numberAndTitle, scope) {
		if (!angular.isUndefined(numberAndTitle)) {
			scope.solicitudNumber = numberAndTitle.num;
			scope.solicitudeNumberTitle = numberAndTitle.title;
			if (!angular.isUndefined(numberAndTitle.num) && !angular.equals(numberAndTitle.num, "")) {
				scope.showpNumber = true;
			}
		} else {
			scope.showpNumber = false;
			HeaderSolicitudNumber.clear();
		}
	};

	return {
		restrict : 'E',
		scope : {
			title:"@"
		},
		controller:function($scope){
			$scope.logo = IMAGES_URL;
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + 'secosan-header.html',
		link : function(scope) {

			$this.watchTitle(scope);
			$this.watchSolicitudNumber(scope);
			scope.$on('$destroy', function() {
				HeaderTitle.clearTitles();
				HeaderSolicitudNumber.clear();
				scope.showTitle = false;
				scope.showpNumber = false;

			});

		}
	}
});
