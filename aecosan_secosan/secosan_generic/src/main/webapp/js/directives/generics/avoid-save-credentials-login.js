angular.module('secosanGeneric').directive('avoidSaveCredentials', function($log) {
	var $this = this;
	this.fakeusernamerememberedId = 'fakeusernameremembered';
	this.fakepasswordrememberedId = 'fakepasswordremembered';
	this.avoidAutoComplete = function(scope) {
		var randomValueGen = function(length) {
			var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ;'
			var result = '';
			for (var i = length; i > 0; --i)
				result += chars[Math.floor(Math.random() * chars.length)];
			return result;
		}
		var emptyFieldValue = "           ";
		var randomVal = randomValueGen(10);
		$("#" + scope.loginformid)[0].reset();
		$("#" + $this.fakeusernamerememberedId)[0].value = emptyFieldValue;
		$("#" + $this.fakeusernamerememberedId).val(emptyFieldValue);
		$("#" + $this.fakepasswordrememberedId).val();
		$("#" + $this.fakepasswordrememberedId)[0].value=randomVal;
	};
	return {
		restrict : 'E',
		scope : {
			loginformid : '@'
		},
		template : "<input style=\"display: none\" type=\"text\" name=\"" + $this.fakeusernamerememberedId + "\"" + " id=\"" + $this.fakeusernamerememberedId + "\" /> " +
		"<input style=\"display:none\" type=\"password\" name=\""+$this.fakepasswordrememberedId+"\"  id=\""+$this.fakepasswordrememberedId+"\"/>",
		link : function(scope) {
			$this.avoidAutoComplete(scope);

		}
	}
});