angular.module('secosanGeneric').directive('direccionContacto', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + "/formularios/direccion-contacto.html"
	};
});