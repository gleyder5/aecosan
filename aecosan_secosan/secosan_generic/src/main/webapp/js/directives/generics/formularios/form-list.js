angular.module('secosanGeneric').directive('formList', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		replace : true,
		scope : {
			bootstrapSize : "@",
			element : "@",
			titulo : "@",
			modelo : "@",
			ttip : "@",
			options : "@",
			loadChange : "@",
			selectedValue : "@"
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + "/formularios/form-list.html"
	};
});