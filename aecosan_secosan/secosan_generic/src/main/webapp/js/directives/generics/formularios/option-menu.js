angular.module('secosan').directive('optionMenu', function(TEMPLATE_PATHS) {
  return {
    restrict: 'E',
    replacce: true,
    scope:{
    	options:'=',
    },
    templateUrl: TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS+"option-menu.html"
  };
});