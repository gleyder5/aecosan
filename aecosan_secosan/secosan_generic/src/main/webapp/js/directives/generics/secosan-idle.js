angular.module('secosanGeneric').directive('secosanIdle', function($log,$location,$rootScope,$translate,Idle,Title,AuthenticationUser,ModalService,ModalParamsService) {

	return {
		restrict : 'E',
		scope : {
			
		},
		controller:function($scope){
			$rootScope.$on('IdleStart', function() {
			    	var timeOut = AuthenticationUser.getRemindingTokenTimeInSeconds();			
			    	Idle.setTimeout(timeOut);
			    });
				$rootScope.$on('IdleTimeout', function() {
			    	AuthenticationUser.cleanUserData();
			    	var modalParams = ModalParamsService.sessionExpired();
					ModalService.showModalWindow(modalParams);
					var expiredSessionTitle = $translate.instant("SESSION_EXPIRED_TITLE");
					Title.value(expiredSessionTitle);
					$rootScope.$digest();
			    });
				
		}
		
	}
	
});