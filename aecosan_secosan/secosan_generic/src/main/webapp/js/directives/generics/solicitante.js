angular.module('secosanGeneric').directive('solicitante', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "solicitante.html",
	};
});
