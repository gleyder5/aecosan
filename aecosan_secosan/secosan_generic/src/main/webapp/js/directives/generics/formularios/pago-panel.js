angular.module('secosan').directive('pagoPopup', function($log,$filter, $window,PagoService,SetDataFromCatalogService, PagoOnline,LocationCombosService,TasaService, TEMPLATE_PATHS,CATALOGUE_NAMES) {
	var $this = this;
	
	$this.checkIfCanUseEPayment = function() {
		var canUseEPayment = true;
//		var userAgent = $window.navigator.userAgent;
//		var browsers = {
//			chrome : /chrome/i,
//			safari : /safari/i,
//			firefox : /firefox/i,
//			ie : /internet explorer/i
//		};
//
//		if (browsers['chrome'].test(userAgent)) {
//			canUseEPayment = false;
//		}

		return canUseEPayment;
	}
	
	$this.checkIfPayed = function($scope,identificadorPeticion){
		PagoOnline.isPaid(identificadorPeticion).success(function(data) {
			$scope.isPayed = data.value;
		}).error(function(data, status) {
			$log.error(data);
		});
	};
	
	
	$this.setTasaIntoScope = function($scope,data){
		$scope.tasa.title=data.description;
		$scope.tasa.model=data.model;
		$scope.tasa.tasaCode=data.tasaCode;
		$scope.tasa.amount = data.amount;
		$scope.solicitudePayment.monto= data.amount;
		$this.parseAmount(data.amount,$scope);
	}
	
	$this.parseAmount = function(amount,$scope){
		var amountValue = "00";
		var amountDecimal = "00";
		if(amount!=undefined) {
            if (amount.indexOf(".") > -1) {
                var amountSplited = amount.split(".");
                amountValue = amountSplited[0];
                amountDecimal = amountSplited[1];
            } else {
                amountValue = amount;
            }
        }
		$scope.solicitudePayment.amount = amountValue;
		$scope.solicitudePayment.amountDecimal = amountDecimal;
	}
	
	$this.getTasaInfo = function(params,$scope){
		$scope.tasa = {};
		if(params.serviceId !== undefined){
			TasaService.getTasaInfoByService(params.serviceId,function(data){
				$this.setTasaIntoScope($scope,data);
				$scope.serviceId = params.serviceId;
			},function(data,status){
				$log.error(data);
			});
		}else{
			TasaService.getTasaInfo(params.idSolicitudeType,function(data){
				$this.setTasaIntoScope($scope,data);
			},function(data,status){
				$log.error(data);
			});
		}
	}
	
	

	$this.initializePaymentCatalog = function($scope) {
	
		SetDataFromCatalogService.getPaymentCatalogue($scope, function(catalog) {
			if (!$this.checkIfCanUseEPayment()) {
				angular.forEach(catalog, function(value, key) {

					if (value.catalogValue.toLowerCase().indexOf("online") !== -1) {
						$log.debug("removing catalog:" + value.catalogValue);
						catalog.splice(key);
					}
				});
			}
			return catalog;
		});
	}
	

	$this.initializePopUp = function($scope) {
		$scope.solicitudePayment = {};
		$scope.solicitudePayment.quantity = 1;
		$scope.solicitudePayment.catalogoPaisesPayment;
		angular.copy($scope.formularioATratar.solicitudePayment, $scope.solicitudePayment);
		var paises = angular.copy($scope.catalogoPaises);
		var countryArray =  $filter('filter')(paises, {
			catalogId : CATALOGUE_NAMES.SPAIN_COUNTRY_CODE
		}, true);
		$scope.solicitudePayment.idPayment.location.pais = countryArray[0];
		$scope.solicitudePayment.idPayment.identificationType="NIF";
		$this.initializePaymentCatalog($scope);
		$this.checkIfPayed($scope,$scope.formularioATratar.identificadorPeticion);
		$scope.checkIfCanUseEPayment = $this.checkIfCanUseEPayment();
		$this.getTasaInfo($scope.popupParams,$scope);
	}

	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "pago-panel.html",
		transclude : true,
		replace : true,
		controller:"pagoController",
		link : function(scope, element,attrs,controller) {
			scope.$watchCollection(function() {
				return PagoService.getPagoArgs();
			}, function(newVal) {
				if (angular.equals(newVal, {})) {
					// PagoService.clearPagoArgs();
				} else {
				    	if (newVal.visible === true ){ 
						      scope.popupParams = {
						        idSolicitude:newVal.idSolicitude,
						        idSolicitudeType:newVal.idSolicitudeType,
						        paymentData:newVal.paymentData,
						        serviceId:undefined
						      };
				    	if(newVal.hasOwnProperty("servicioSolicitado")) {
				    	   	scope.popupParams.serviceId = newVal.servicioSolicitado.id
				   		}
				    	$log.debug("initializing");
				    	$log.debug(scope);
				    	$this.initializePopUp(scope);
				    }
				}
			}, true);
			scope.$on('$destroy', function() {
				PagoService.clearPagoArgs();
			});
		}
	};
});
