angular
		.module('secosanGeneric')
		.directive(
				'resumeValidacion',
				function($compile) {
					return {
						restrict : 'E',
						link : function(scope, el, attrs) {
							scope.allValid = [];
							scope.invalidCount = true;
							var template = "<div><span ng-if='!invalidCount' class='text-center'><h1><span class='glyphicon glyphicon-remove-sign text-danger' aria-hidden='true'></span><small><strong> {{'ALL_FORM_INVALID' | translate}}</strong></small></h1></span>"
									+ "<span ng-if='invalidCount' class='text-center'><h1><span class='glyphicon glyphicon-ok-sign text-success' aria-hidden='true'></span><small><strong>  {{'ALL_FORM_VALID' | translate}}</strong></small></h1></span></div>";
							
							scope.$on('check-resume-validity', function(event,action) {
								if(!action.set){
									scope.allValid[action.key] = false;
								}else{
									scope.allValid[action.key] = true;
								}
								scope.invalidCount = true;
								for (var key in scope.allValid) {
									if(!scope.allValid[key]){
										scope.invalidCount = false;
									}
								}
								
							});
							el.prepend($compile(template)(scope));

						}
					}
				})