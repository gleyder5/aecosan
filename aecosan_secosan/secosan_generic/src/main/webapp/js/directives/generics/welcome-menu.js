angular.module('secosanGeneric').directive('welcomeMenu', function($log,$anchorScroll, $location, GENERIC_TEMPLATE_PATHS, WelcomeMenu) {

	return {
		restrict : 'E',
		transclude : true,
		scope : {
			options : "="
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + 'menu/welcome-menu.html',
		controller : function($scope) {
			$scope.click = function(elementId) {
				WelcomeMenu.setWelcomeMenuItem(elementId);
			}

		}

	}

});
angular.module('secosanGeneric').directive('welcomeMenuElement', function($log, GENERIC_TEMPLATE_PATHS, WelcomeMenu) {

	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + 'menu/welcome-menu-element.html',
		scope : {
			title : "=",
			sia : "=",
		},
		link : function(scope, element) {
			scope.$watch(function() {
				return WelcomeMenu.getWelcomeMenuItem()
			}, function(newVal) {
				var liMenuElement = element.parent();
				var elementChildrenH3 = element.children();
				var linkElement = elementChildrenH3.children();
				var activeClass = "active";
				if (newVal === liMenuElement.attr("id")) {
					liMenuElement.addClass(activeClass);
					elementChildrenH3.addClass(activeClass);
					linkElement.addClass(activeClass);
				} else {
					liMenuElement.removeClass(activeClass);
					elementChildrenH3.removeClass(activeClass);
					linkElement.removeClass(activeClass);
				}
			});
		}

	}
});