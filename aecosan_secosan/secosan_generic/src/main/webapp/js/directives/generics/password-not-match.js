angular.module('secosanGeneric').directive('pwNotCheck', [ function() {
	return {
		require : 'ngModel',
		link : function(scope, elem, attrs, ctrl) {
			var firstPassword = '#' + attrs.pwNotCheck;
			elem.add(firstPassword).on('keyup', function() {
				scope.$apply(function() {
					var firstPwValue = $(firstPassword).val();
					if (firstPwValue !== "") {
						var v = elem.val() === $(firstPassword).val();
						ctrl.$setValidity('pwnotmatch', !v);
					}
				});
			});
		}
	}
} ]);