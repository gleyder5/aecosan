angular.module('secosan').directive('pagoBotones', function($log,PagoService,TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "pago-botones.html",
		scope:{
			fomularioATratar:"=", // fomulario almacena los datos de formulario  (en el scope padre)
			solicitudeType:"=",
			serviceId:"=",
			tasatipo:"="
		},
		controller: function($scope, PagoOnline){
            PagoOnline.isPaid($scope.fomularioATratar.identificadorPeticion).success(function (resp) {
                let formularioATratar = $scope.fomularioATratar;
                let idSolicitude =  formularioATratar.identificadorPeticion;
                let solicitudeType = $scope.solicitudeType;
                let solicitudePayment =  formularioATratar.solicitudePayment;
                let serviceId =  $scope.serviceId;
                let pagoParams = {
                    'idSolicitude' : idSolicitude,
                    'idSolicitudeType': solicitudeType,
                    'paymentData' :solicitudePayment,
                    'serviceId':serviceId
                };

                PagoService.showPagoPanel(pagoParams);
                $scope.ocultarPagoBotones   = true ;
            })
			$scope.showPago = function(){
				
				var formularioATratar = $scope.fomularioATratar;
				var idSolicitude =  formularioATratar.identificadorPeticion;
				var solicitudeType = $scope.solicitudeType;
				var solicitudePayment =  formularioATratar.solicitudePayment;
				var serviceId =  $scope.serviceId;
				var popupParams = {
						'idSolicitude' : idSolicitude,
						'idSolicitudeType': solicitudeType,
						'paymentData' :solicitudePayment,
						'serviceId':serviceId
					}
				$log.debug(popupParams);
                PagoService.showPagoPanel(popupParams);
			}
		
			$scope.showPagoPanel = function(){
				
				var formularioATratar = $scope.fomularioATratar;
				var idSolicitude =  formularioATratar.identificadorPeticion;
				var solicitudeType = $scope.solicitudeType;
				var solicitudePayment  =  formularioATratar.solicitudePayment;
				var serviceId =  $scope.serviceId;
				var popupParams = {
						'idSolicitude' : idSolicitude,
						'idSolicitudeType': solicitudeType,
						'paymentData' :solicitudePayment,
						'serviceId':serviceId
					}
				$log.debug(popupParams);
				PagoService.showPagoPanel(popupParams);
			}
		}
	}
});