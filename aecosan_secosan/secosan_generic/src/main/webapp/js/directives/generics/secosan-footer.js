angular.module('secosanGeneric')
.directive('secosanFooter', function(GENERIC_TEMPLATE_PATHS) {
    return {
        restrict: 'E',
        scope: {
        	showprotecciondatos:"@"
        },
        templateUrl: GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS+'secosan-footer.html'
    }
});