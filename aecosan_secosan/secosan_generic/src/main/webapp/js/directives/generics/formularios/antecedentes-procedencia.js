angular.module('secosanGeneric').directive('antecedentesProcedencia', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "antecedentes-procedencia.html"
	};
});