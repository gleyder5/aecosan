angular.module('secosanGeneric').directive(
    'historialSolicitud',
    function ($log, $modal, ngDialog, NgTableParams,DatePickerPopup, HistorialSolicitudesService, TEMPLATE_PATHS, $routeParams,PaginationFilter,DocumentacionService,FileDtoDownload) {
        return {
            restrict: 'E',
            templateUrl: TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + 'historial-solicitud.html',
            controller: function ($scope, $modal, $filter) {
                var $this = this;
				this.setupController = function() {
				
					if ($this.configTableParams !== undefined) {
                        $this.configTableParams.filter();
                        $this.configTableParams.reload();
					}
				}

                $scope.historialList = [];

				$this.descargarAdjunto = function (idDocumento) {
					if(idDocumento!=undefined) {
                        DocumentacionService.getDocumentacionById(idDocumento).success(function (resp) {
                            FileDtoDownload.decryptAndDownloadFile(resp.base64, resp.fileName);
                        }).error(function (erro) {

                        });
                    }

                };
				$this.configParams = new NgTableParams({
						count : 5,
						}, {
						counts : [],
						paginationMaxBlocks : 3,
						paginationMinBlocks : 2,
						getData : function($defer, params) {
							HistorialSolicitudesService.getHistorialListBySolicitud($routeParams.id,params).success(function (data) {
                    	    	var total = data.recordsTotal;
                    	    	params.total(total);
                    	    	$scope.hasdata = $this.hasData(total);
                    	    	$defer.resolve(data.data);
                    		}).error(function(data, status, headers, config) {
								$scope.hasdata = false;
							});
						}
					});

				$this.setupController();
				$this.hasData = function(recordsTotal) {
					return recordsTotal > 0
				};

				$scope.configTableParams = $this.configParams;
				$scope.descargarAdjunto = $this.descargarAdjunto;
            },
        }
    });