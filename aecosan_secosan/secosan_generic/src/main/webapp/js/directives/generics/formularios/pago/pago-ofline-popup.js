angular.module("secosan").directive(
		'pagoOfflinePopUp',
		function($window,$log, $interval,TASA,TEMPLATE_PATHS,PagoService) {
			var $this = this;
			this.tasaTipo="?tipoTasa=1";
			
			this.windowTasaPopup;
			this.isOpenWindow = false;
			this.backgroundDivId = "blockBackgroudPayment"
			
			this.openTasaPopupWindow = function(){
				if($this.windowTasaPopup !==undefined){
					$this.windowTasaPopup = undefined;
				}
				$this.windowTasaPopup = $window.open(TASA.TASA_PATH+$this.tasaTipo, 'TASA', 'height=1600,width=1250,location=0, status=0, resizable=1, scrollbars=1');
				$this.windowTasaPopup.focus();
				$this.isOpenWindow = true;
				$log.debug("pagoOffline");
				$log.debug($this.windowTasaPopup);
			};
			
			this.addTasaTipoToUrl = function(scope){
			
				if(!angular.equals(scope.aesantasatipo,undefined)){
					$this.tasaTipo = "?tipoTasa="+scope.aesantasatipo;
				}
			}
			
			this.openTasaPopup = function(){
				$log.debug("en openTasaPopup");
				$this.openTasaPopupWindow();
				var pollTimer = $interval(function() {
				    if (!angular.equals($this.windowTasaPopup,undefined) && $this.windowTasaPopup.closed !== false) { 
				    	$interval.cancel(pollTimer);
				        $this.setTasaWindowClosed();   
				    }
				}, 20);
			};
			
			$this.setTasaWindowClosed= function(){
				$this.windowTasaPopup.close();
				$this.windowTasaPopup = undefined;
				$this.isOpenWindow = false;
				
			};
			
			this.disableTasaBackground = function(){
				var div ="<div class=\"parentDisable\" id=\""+$this.blockBackgroud+"\"></div>";
				$(div).appendTo('body');
			};
			
			this.afterWindowIsClosed=function(redirectFn){
				this.enableTasaBackground();
			};
			
			this.enableTasaBackground= function(){
			
				 $("body").children("#"+$this.blockBackgroud).remove();
			};
			
			
			return {
				restrict : 'E',
				scope : {
					aesantasatipo:"="
				},
				templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "pago-offline-popup.html",
				controller:function($scope){
			
					$scope.openTasaPopup = $this.openTasaPopup;
					
					
				},link : function(scope, element, attrs) {
					$this.addTasaTipoToUrl(scope);
					scope.$watch(function() {
						return $this.isOpenWindow;
					}, function(newValue, oldValue, scope) {
						
						if(!newValue && oldValue){
							$this.afterWindowIsClosed();
							PagoService.setWindowPagoOfflineVisible(false);
						}else if(newValue){
							PagoService.setWindowPagoOfflineVisible(true);
							$this.disableTasaBackground();
						}
					});
					
					element.on('$destroy', function() {
						if($this.windowTasaPopup != undefined){
							$this.windowTasaPopup.close();
						}
						$this.windowTasaPopup = undefined;
						$this.enableTasaBackground();
				    });
				}
			}
		});