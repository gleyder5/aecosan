angular.module('secosanGeneric').directive('paises', function(GENERIC_TEMPLATE_PATHS,SetDataFromCatalogService) {
	$this = this;
	
	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + "/formularios/paises.html",
		controller : function($scope){

		},
		scope : {
			type  :'=',// 'EU' | 'noEU' | ('all' | null )			
			modelList  : '=',
			variablename :'=',
			modelObj : '='
		},		
		link: function(scope,element,attrs,ctrl){
			let _that  = this ;
			this.lookFunctionInParents = function(scope,fn){
				let fnFound =null; 
				if(scope.$parent!=null && scope.$id!=1){
					if(scope.hasOwnProperty(fn)){
						fnFound = scope[fn];
					}else{
						fnFound = _that.lookFunctionInParents(scope.$parent,fn);
					}
				}
				return fnFound;
			};
			scope.cargarProvincias  = function(pais){
				// loadProvinciaOnChange("paisSolicitante");
				// scope.$parent.loadProvinciaOnChange("paisSolicitante");
				let provChangeFn = _that.lookFunctionInParents(scope,"loadProvincia");
				provChangeFn("paisSolicitante");
				if(pais.catalogValue==='España'){
					console.log(pais);
				}
			};
			scope[scope.variablename] = {};
			if(scope.type==="all" ||scope.type==="" || scope.type===undefined){
				SetDataFromCatalogService.getCountryCatalogue(scope,scope.variablename);
			}
			if(scope.type==="EU"){
				SetDataFromCatalogService.getEUCountriesCatalogue(scope,scope.variablename);
			}
			if(scope.type==="noEU"){
				SetDataFromCatalogService.getNoEUCountriesCatalogue(scope,scope.variablename);
			}
			scope.$watchCollection(function(){return scope[scope.variablename]},function(nV,oV){
				if(nV===oV){
					return false;
				}
				scope.modelList = scope[scope.variablename];
			});
			scope.modelList = scope[scope.variablename];
		},
	}
});		