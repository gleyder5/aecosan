angular.module('secosanGeneric').directive('messagesZone', function($log, $timeout, $anchorScroll, $location, Messages) {
	var messagesDirective = this;
	var divAlert = "<div class='alert alert-{type}' role='alert'><span>{message}<span></div>";
	var hidenClass = "hidden";

	this.showMessages = function(newVal) {
		var message = newVal.message;
		var classType = "";
		switch (newVal.messageType) {
		case Messages.warning:
			classType = "warning";
			break;
		case Messages.error:
			classType = "danger";
			break;
		case Messages.success:
			classType = "success";
			break;
		case Messages.info:
			classType = "info";
			break;
		default:
			break;
		}
		$timeout(function() {
			messagesDirective.generateMessage(classType, message);
			messagesDirective.goToMessageArea();
		}, 1, false);
	}

	this.generateMessage = function(type, message) {
		if(!angular.equals(message,"") && (message.length > 0)){
			var divMessage = divAlert.replace("{type}", type);
			divMessage = divMessage.replace("{message}", message);
			var messageElement = $("#" + Messages.getElementId());
			messageElement.empty().append(divMessage).removeClass(hidenClass);
		}
	}

	this.goToMessageArea = function() {
		var old = $location.hash();
		$location.hash(Messages.getElementId());
		$anchorScroll();
		$location.hash(old);
		Messages.clearMessages();
	}

	this.cleanMessageArea = function() {

		$("#" + Messages.getElementId()).empty().addClass(hidenClass);
	}

	return {
		restrict : 'E',
		template : "<div><a id='" + Messages.getAnchorId() + "'></a></div>" + "<div id='" + Messages.getElementId() + "' class='" + hidenClass + "'></div>",
		scope : {

		},
		link : function(scope) {
			scope.$watch(function() {
				return Messages.getMessageArgs();
			}, function(newVal) {
				if (angular.equals(newVal, {})) {
					Messages.clearMessages();
					$log.debug("force remove:"+Messages.getForceRemove());
					if(Messages.getForceRemove()){
						messagesDirective.cleanMessageArea();
					}
				} else {
					messagesDirective.cleanMessageArea();
					messagesDirective.showMessages(newVal);
				}
			}, true);
			scope.$on('$destroy', function() {
				messagesDirective.cleanMessageArea();
			});
		}
	};
});