angular.module('secosan').directive('identificacionPago', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "identificacion-pago.html"
	};
});