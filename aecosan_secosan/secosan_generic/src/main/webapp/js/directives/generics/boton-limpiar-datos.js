
angular.module('secosan').directive('botonLimpiarDatos', function (TEMPLATE_PATHS, AuthenticationUser, $filter, BotonCopiarDatosService) {
    var $this = this;
    var $translate = $filter('translate');

    $this.cleanData= function (_scope, type,aux) {
        _scope[aux]   = {};
        this.keys = Object.keys(_scope.form[type]);
        this.keys.forEach(function (key) {
            // if(typeof(_scope.form)==='string'){
            //     _scope.form[type][key] = '' ;
            // }else if(typeof(_scope.form[type]==='object')){
            //     _scope.form[type][key] = "";
            // }
            _scope.form[type].name  ="";
            _scope.form[type].lastName = "";
            _scope.form[type].identificationNumber = "";
            _scope.form[type].email = "";
            _scope.form[type].addressType = "";
            _scope.form[type].address = "";
            _scope.form[type].addressNumber = "";
            _scope.form[type].stair= "";
            _scope.form[type].floor= "";
            _scope.form[type].door= "";
            _scope.form[type].telephoneNumber= "";

        });
    };
    return {
        restrict: 'E',
        templateUrl: TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "boton-limpiar-datos.html",
        scope: {
            type: '@',
            form: '=',
            show: '=',
            aux  : '@'
        },
        link: function (scope, elem, attr, ctrl) {
            $this.clicked = false;
            scope.buttonText = "Limpiar Datos"
            scope.clean = function (event, type,aux ) {
                event.preventDefault();
                $this.cleanData(scope, type,aux);
                event.preventDefault();
            };
        },
    };
});

