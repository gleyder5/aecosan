angular.module('secosanGeneric').directive('appDisclamer', function(GENERIC_TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + "app-disclamer.html",
		scope : {
			disclamer:'='
		},
		controller:function($scope){
			$scope.images = IMAGES_URL;
		}
	};
});