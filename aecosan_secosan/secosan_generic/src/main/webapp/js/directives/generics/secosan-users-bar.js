angular.module('secosanGeneric').directive('secosanUsersBar', function($location, GENERIC_TEMPLATE_PATHS, AuthenticationUser, CambioContraseniaService) {
	return {
		restrict : 'E',
		scope : {
			showgoback : '@'
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS + 'secosan-users-bar.html',
		controller : function($scope) {
			$scope.loggedUser = AuthenticationUser.getUserPayloadData("name") + " " + AuthenticationUser.getUserPayloadData("userLastName");
			$scope.lastAccess = AuthenticationUser.getUserPayloadData("lastLogin");
			var area = AuthenticationUser.getUserPayloadData("areaDescription");
			var role = AuthenticationUser.getUserPayloadData("roleDescription");
			var loginType = AuthenticationUser.getUserPayloadData("loginType");
			var userUE = true;
			if (loginType === "userpw") {
				userUE = false;
			}
			$scope.userRole = role;
			$scope.userArea = area;
			$scope.userUE = userUE;
			$scope.logout = function() {
				AuthenticationUser.cleanUserData();
				$location.path("/login");
			}, $scope.showChangePasswordPopup = function() {
				var cambioContraseniaParams = {
					'oldPassword' : oldPassword,
					'newPassword' : newPassword,
					'retypeNewPassword' : retypeNewPassword
				}
				CambioContraseniaService.showCambioContraseniaWindow(cambioContraseniaParams);
			}
		}
	}
});