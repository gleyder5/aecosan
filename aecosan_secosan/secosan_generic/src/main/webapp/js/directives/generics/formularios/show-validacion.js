angular.module('secosanGeneric').directive('showValidations', function($timeout) {
    return {
    	restrict : 'A',
    	require: '^form',		
		replace : true,
		scope : {
			type : '@',
			value : '@',
			btnStyle : '@',
			btnSize : '@',
			glyphicon:'@'
		},
      link: function (scope, el, attrs, formCtrl) {
        // find the text box element, which has the 'name' attribute
        var inputEl   = el[0].querySelector("[name]");
        // convert the native text box element to an angular element
        var inputNgEl = angular.element(inputEl);
        // get the name on the text box
        var inputName = inputNgEl.attr('name');

        scope.$watch(function() {
            return formCtrl[inputName].$value
          }, function(dirty) {
            // we only want to toggle the has-error class after the blur
            // event or if the control becomes valid
        	  console.log(dirty)
            if (!blurred && invalid) { return }
            el.toggleClass('has-error', invalid);
          });

        scope.$on('show-errors-check-validity', function() {
           	

	        
        	
	        if(formCtrl[inputName].$error.required){
	        	//mensaje si es requerido
	        	var myEl = '<p class="ng-invalid" id="'+inputName+'" >Campo requerido</p>',
	            ngEl = angular.element(myEl);
	        	el.append(ngEl); //Append to element
	        }
	
	        
	        
        });
	
	        scope.$on('show-errors-reset', function() {
	          $timeout(function() {
	        	  var lightBoxEl = document.getElementById(inputName),
	                ngLightBoxEl = angular.element(lightBoxEl);
	        	  ngLightBoxEl.remove();
	          }, 0, false);
	        });
      }
    }
  });