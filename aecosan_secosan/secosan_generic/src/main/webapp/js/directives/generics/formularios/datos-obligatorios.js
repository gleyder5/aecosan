angular.module('secosanGeneric').directive('datosObligatorios', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "datos-obligatorios.html"
	};
});