angular.module('secosan').directive('pagoOnline', function($log,TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "pago-online.html",
		scope : {
			paymentfnc : "&",
			justifierfnc : "&",
			justifier : "=",
			tosign : "=",
			showpaymentbutton:"=",
			paymenttype:"=",
			paymentprocessing:"=",
			pagonlinedone:"=",
			nrc:"=",
			showprocessresult:"=",
			processmessage:"=",
			resultimage:"=",
			generatepdf:"&"
			
		},
		controller : function($scope) {
			$scope.getJustificante = function() {
				$scope.justifierfnc();
			}

			$scope.makePayment = function() {
				$scope.paymentfnc();
			}
			$scope.images= IMAGES_URL;
			
		}
	};
});