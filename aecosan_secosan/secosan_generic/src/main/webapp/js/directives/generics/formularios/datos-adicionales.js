angular.module('secosanGeneric').directive('datosAdicionales', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "datos-adicionales.html"
	};
});