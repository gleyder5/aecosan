angular.module('secosanGeneric').directive(
		'claveLoginImg',
		function($window,$log, $interval,AuthenticationUser,CLAVE_PATH,IMAGES_URL) {
			var $this = this;
			
			this.windowClavePopup;
			this.isOpenWindow = false;
			this.backgroundDivId = "blockBackgroud"
			
			this.openClavePopupWindow = function($scope){
				
				if($this.windowClavePopup !==undefined){
					$this.windowClavePopup = undefined;
				}
				$this.windowClavePopup = $window.open($this.getClavePath($scope), 'Cl@ve', 'width=800,height=600');
				$this.windowClavePopup.focus();
				$this.isOpenWindow = true;
			};
			
			this.getClavePath = function($scope){
				if($scope.clavePath !==undefined){		
					return $scope.clavePath;
				}else{	
					return CLAVE_PATH;
				}
			}
			
			this.openClavePopup = function($scope){
			    $this.openClavePopupWindow($scope);
				var pollTimer = $interval(function() {
				    if (!angular.equals($this.windowClavePopup,undefined) && $this.windowClavePopup.closed !== false) { 
				    	$interval.cancel(pollTimer);
				        $this.setClaveWindowClosed();   
				    }
				}, 20);
			};
			
			this.setClaveWindowClosed= function(){
				this.windowClavePopup.close();
				this.windowClavePopup = undefined;
				this.isOpenWindow = false;
				
			};
			
			this.disableClaveBackground = function(){
				var div ="<div class=\"parentDisable\" id=\""+$this.blockBackgroud+"\"></div>";
				$(div).appendTo('body');
			};
			
			this.checkAfterClaveValidation=function(redirectFn,redirectError){
				this.enableClaveBackground();
				if(AuthenticationUser.handleClaveLogin()){
					if(!angular.equals(redirectFn,undefined)){
						redirectFn();
					}
				
				}else if(!angular.equals(redirectError,undefined)){
					$log.debug("redirectError");
					$log.debug(redirectError);
					redirectError();
					$log.debug("executed?");
				}
			};	
			this.enableClaveBackground= function(){
			
				 $("body").children("#"+$this.blockBackgroud).remove();
			};
			return {
				restrict : 'E',
				scope : {
					
					redirect:'&',
					clavePath:"@",
					redirectError:'&'
				},
				template : "<img src=\""+IMAGES_URL+"/logoCLAVE.png\" ng-click=\"openClavePopup()\" id=\"logoClaveImg\"></img>",
				
				controller:function($scope){
			
					$scope.openClavePopup = function(){
						$this.openClavePopup($scope);
					};
					
				},link : function(scope, element, attrs) {
					scope.$watch(function() {
						return $this.isOpenWindow;
					}, function(newValue, oldValue, scope) {
						
						if(!newValue && oldValue){
							$this.checkAfterClaveValidation(scope.redirect,scope.redirectError);
						}else if(newValue){
							$this.disableClaveBackground();
						}
					});
					
					element.on('$destroy', function() {
						if($this.windowClavePopup != undefined){
							$this.windowClavePopup.close();
						}
						$this.windowClavePopup = undefined;
						$this.enableClaveBackground();
				    });
				}
			}
		});