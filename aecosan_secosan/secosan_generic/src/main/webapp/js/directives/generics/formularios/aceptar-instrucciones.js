angular.module('secosanGeneric').directive('aceptarInstrucciones', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "aceptar-instrucciones.html"
	};
});