angular.module('secosan').service('BotonCopiarDatosService', function () {
    var _this = this;
    _this.buttonSolicitanteDisabled = false;
    _this.buttonRepresentanteDisabled = false;
    _this.isButtonRepresentanteDisabled = function () {
        return _this.buttonRepresentanteDisabled;
    };
    _this.isButtonSolicitanteDisabled = function () {
        return _this.buttonSolicitanteDisabled;
    };
});
angular.module('secosan').directive('botonCopiarDatos', function (TEMPLATE_PATHS, AuthenticationUser, $filter, BotonCopiarDatosService) {
    var $this = this;
    var $translate = $filter('translate');
    $this.clicked = false;

    $this.cleanUserData = function (_scope, type) {
        _scope.form[type].name = "";
        _scope.form[type].lastName = "";

        _scope.form[type].identificationNumber = "";
        _scope.form[type].email = "";
    };
    $this.copy = function (_scope, type, userData) {
        _scope.form[type].name = userData.name;
        _scope.form[type].lastName = userData.lastName;

        _scope.form[type].identificationNumber = userData.identificationNumber;
        _scope.form[type].email = userData.email;
    };

    return {
        restrict: 'E',
        templateUrl: TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "boton-copiar-datos.html",
        scope: {
            type: '@',
            form: '=',
            show: '='
        },
        link: function (scope, elem, attr, ctrl) {
            $this.clicked = false;
            scope.buttonText = $translate("COPY_DATA_FROM_USER");
            scope.copyTo = function (event, type) {
                event.preventDefault();
                this.userData = {
                    name: AuthenticationUser.getUserPayloadData("name"),
                    lastName: AuthenticationUser.getUserPayloadData("userLastName"),
                    secondLastName: AuthenticationUser.getUserPayloadData("userSecondLastName"),
                    identificationNumber: AuthenticationUser.getUserPayloadData("identificationNumber"),
                    email: AuthenticationUser.getUserPayloadData("email"),
                };
                this.userData.lastName += ' ' +this.userData.secondLastName;
                if (type == "representante") {

                    if (!$this.clicked) {
                        $this.copy(scope, type, this.userData);
                        BotonCopiarDatosService.buttonSolicitanteDisabled = true;
                        $this.clicked = true;
                    } else {
                        $this.cleanUserData(scope, type);
                        BotonCopiarDatosService.buttonSolicitanteDisabled = false;
                        $this.clicked = false;
                    }
                } else if (type === "solicitante") {
                    if (!$this.clicked) {
                        $this.copy(scope, type, this.userData);
                        BotonCopiarDatosService.buttonRepresentanteDisabled = true;
                        $this.clicked = true;

                    } else {
                        $this.cleanUserData(scope, type);
                        BotonCopiarDatosService.buttonRepresentanteDisabled = false;
                        $this.clicked = false;
                    }
                }
                if (!$this.clicked) {
                    scope.buttonText = $translate("COPY_DATA_FROM_USER");
                } else {
                    scope.buttonText = $translate("DELETE_DATA_FROM_USER");
                }
            }
            scope.checkDisabled = function (type) {
                if (type == "representante") {
                    return BotonCopiarDatosService.isButtonRepresentanteDisabled();
                } else if (type == "solicitante") {
                    return BotonCopiarDatosService.isButtonSolicitanteDisabled();
                }
            };
        },
    };
});

