angular.module('secosanGeneric').directive('checkboxSubformularios', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		// require : 'ngModel',
		replace : true,
		scope : {
			title : '@',
			id : '@',
			bar : '=ngModel',
			view : '=ngDisabled',
			form : '=',
			typeModel :'@',
			aux  : '='

		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "checkbox-subformularios.html",
		link : function (scope) {
				scope.checkboxChange = function (form,model,aux) {

					if(!scope.bar){
                        aux = {};
                        form[model].location = {
                            codigoPostal: "",
                        	municipio: {},
                        	pais: {},
                        	provincia:{}
                        };
						form[model].name  ="";
                        form[model].lastName = "";
                        form[model].identificationNumber = "";
                        form[model].email = "";
                        form[model].addressType = "";
                        form[model].address = "";
                        form[model].addressNumber = "";
                        form[model].stair= "";
                        form[model].floor= "";
                        form[model].door= "";
                        form[model].telephoneNumber= "";
					}


                };
        }


	};
});