angular.module('secosanGeneric').directive('pieformulario', function(TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		transclude : true,
		scope : {
			msg : "@"
		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "pieformulario.html",
		controller:function($scope){
			$scope.images =IMAGES_URL;
		}
	};
});