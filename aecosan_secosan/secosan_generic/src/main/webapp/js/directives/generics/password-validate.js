angular.module('secosanGeneric').directive('pwValidate', function(PASSWORD_REGEX) {
	return {
		restrict : 'A',
		require : 'ngModel',
		link : function(scope, elem, attr, ctrl) {
			elem.on('blur', function() {
				var passwordValue = elem.val();
				if(passwordValue !==undefined && passwordValue !=="" && passwordValue.length > 3){
					var valid =PASSWORD_REGEX.regex.test(passwordValue);
					ctrl.$setValidity('pwvalidate', valid);
				}
			});
			
		}
	};
});