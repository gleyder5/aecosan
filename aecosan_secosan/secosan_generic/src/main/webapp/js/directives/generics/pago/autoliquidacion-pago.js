angular.module('secosan').directive('autoliquidacionPago', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "autoliquidacion-pago.html"
	};
});