angular.module('secosanGeneric').directive('botonAccionFormulario', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		require : 'onclick',
		replace : true,
		scope : {
			type : '@',
			value : '@',
			btnStyle : '@',
			btnSize : '@',
			glyphicon:'@',
			myId:'@'
		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "boton-accion-formulario.html"
	};
});