angular.module('secosanGeneric').directive('instrucciones', function(TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "instrucciones.html",
		scope : {
			eventHandler : '&ngClick',
			viewmode:'=',
			form:"="
		},
		controller:function($scope){
			$scope.images=IMAGES_URL;	
		}
	};
});