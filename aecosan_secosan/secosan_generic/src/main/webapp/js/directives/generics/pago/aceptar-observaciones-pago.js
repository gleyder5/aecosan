angular.module('secosan').directive('aceptarObservacionesPago', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PAGO + "aceptar-observaciones-pago.html"
	};
});