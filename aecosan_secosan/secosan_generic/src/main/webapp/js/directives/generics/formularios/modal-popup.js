angular.module('secosanGeneric').directive('modalPopup', function(ModalService, GENERIC_TEMPLATE_PATHS,$log,IMAGES_URL,POPUP_IMAGES) {
    var $this = this;
    this.setupOkFunction = function(modalparams,element){
    	$(element).find("#okAction").off( "click" ).click(function(){
			if(!angular.equals(modalparams.okFunction,undefined)){
				modalparams.okFunction();
    		}
		});
    };
    this.setupModalTexts = function(modalparams,scope){
    	scope.title = modalparams.title;
		scope.description = modalparams.description;
		scope.type = modalparams.type;
		scope.okButton = modalparams.okButton;
		scope.cancelButton = modalparams.cancelButton;
		
    };
    this.setupModalStyles = function(modalparams,scope){
    	var defaultStyle ="btn-primary";
    	if(!angular.equals(modalparams.okButtonStyle,undefined) && !angular.equals(modalparams.okButtonStyle,"")){
    		scope.okButtonStyle = modalparams.okButtonStyle;
    	}else{
    		scope.okButtonStyle =defaultStyle
    	}
    	if(!angular.equals(modalparams.cancelButtonStyle,undefined) && !angular.equals(modalparams.cancelButtonStyle,"")){
    		scope.cancelButtonStyle = modalparams.cancelButton;
    	}else{
    		scope.cancelButtonStyle = defaultStyle;
    	}
    	
    }
    
    this. setupModalImages = function(type,scope){
    	var imageBase = IMAGES_URL+"/"
    	var imageIcon ="";
    	switch(type){
	    	case 'info':
	    		imageIcon= imageBase+POPUP_IMAGES.info;
	    	break;
	    	case 'exclamation':
	    		imageIcon = imageBase+POPUP_IMAGES.exclamation;
		    break;
	    	case 'question':
	    		imageIcon = imageBase+POPUP_IMAGES.question;
		    break;
	    	case 'error':
	    		imageIcon = imageBase+POPUP_IMAGES.error;
		    break;
	    	case 'ok':
	    		imageIcon = imageBase+POPUP_IMAGES.ok;
		    break;
    	}
    	scope.imageIcon = imageIcon;
    }
    
	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS+"formularios/" + "modal-popup.html",
		transclude : true,
		replace : true,
		scope : {},
		link : function(scope, element) {
			scope.$watch(function() {
				return ModalService.getModalArgs();
			}, function(newVal) {
				if (angular.equals(newVal, {})) {
					ModalService.clearModalArgs();
				} else {
					if (newVal.visible === true) {
						$this.setupModalTexts(newVal,scope);
					    $this.setupOkFunction(newVal,element);
					    $this.setupModalStyles(newVal,scope);
					    $this.setupModalImages(newVal.type,scope);
					    $(element).modal({
							  keyboard: false,
							  show:true  
						});
					} else {
						$(element).modal('hide');
					}
				}
				ModalService.clearModalArgs();
			}, true);
			scope.$on('$destroy', function() {
				ModalService.clearModalArgs();
			});
		}
	};
});
