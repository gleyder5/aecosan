angular.module('secosanGeneric').directive(
		'imgTitle',
		function(GENERIC_TEMPLATE_PATHS) {
			return {
				restrict : 'E',
				scope : {
					title : "@",
					class : "@"
				},
				templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_GENERICS
						+ 'img-title.html'
			}
		});