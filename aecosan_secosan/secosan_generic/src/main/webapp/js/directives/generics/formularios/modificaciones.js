angular.module('secosan').directive('modificaciones', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "modificaciones.html"
	};
});
