angular.module('secosanGeneric').directive('presentacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_FORMULARIOS + "presentacion.html"
	};
});