angular.module('secosan').directive('validarApoderamiento', function (TEMPLATE_PATHS, AuthenticationUser, $filter,ApoderamientoService,POPUP_IMAGES) {
    let $this = this;
    $this.consultarApoderamieto = function(nif) {
        return ApoderamientoService.consultarApoderamieto(nif);
    };
    return{
        restrict : 'E',
        templateUrl : TEMPLATE_PATHS.DIRECTIVES_APODERAMIENTO  + "validar-apoderamiento.html",
        controller: function($scope,IMAGES_URL){
            $scope.images= IMAGES_URL;
        },
        link:function($scope,event){
            $this.processConsultaApoderamientoSuccess = function() {
                $scope.resultImage = POPUP_IMAGES.ok;
                $scope.showProcessResult = true
            };
            $this.processConsultaApoderamientoError = function(resp) {
                $scope.resultImage = POPUP_IMAGES.error;
                $scope.showProcessResult = true
                $scope.processMessage = resp.description;

            };

            $scope.consultarApoderamiento = function (event,nif) {
                event.preventDefault();
                $scope.processMessage = "";
                $scope.showProcessResult = false
                $scope.consultarApoderamientoProcessing= true ;
                $this.consultarApoderamieto(nif).success(function (resp) {
                    $this.processConsultaApoderamientoSuccess();
                    $scope.consultarApoderamientoProcessing= false ;
                    $scope.processMessage  = "Se ha verificado el apoderamiento correctamente";
                }).error(function (resp) {
                    $this.processConsultaApoderamientoError(resp);
                    $scope.consultarApoderamientoProcessing = false;
                });
            }
        }
    }
});