angular.module('secosan').directive(
		'solicitudeTableWelcome',
		function($log, $modal, ngDialog, CatalogueHandler,DatePickerPopup, ModalService, ModalParamsService, Solicitude, WelcomeMenu, EntityUrlService, CATALOGUE_NAMES, SOLICITUDE_DELETE_STATUS,
				SOLICITUDE_EDIT_MODE_STATUS_CODE, TEMPLATE_PATHS,IMAGES_URL) {
			return {
				restrict : 'E',
				scope : {
					configparams : "=",
					showfilter : "=",
					showfilterfunction : "=",
					formulariodata : "=",
					hasdata : "="
				},
				templateUrl : TEMPLATE_PATHS.DIRECTIVES + 'solicitude-table-welcome.html',
				controller : function($scope, $modal, $filter) {
					var $this = this;
					$this.getSolicitudeTypes = function($column) {
						CatalogueHandler.getCatalogue(CATALOGUE_NAMES.SOLICITUDE_TYPE).success(function(data) {
							$column.data = CatalogueHandler.generateCatalogueDataForShow(data);

						}).error(function() {
							CatalogueHandler.generateDefaultErrorMessage();
						});
					}
					$this.getSolicitudeStatus = function($column) {

						CatalogueHandler.getCatalogue(CATALOGUE_NAMES.SOLICITUDE_STATUS).success(function(data) {
							$column.data = CatalogueHandler.generateCatalogueDataForShow(data);

						}).error(function() {
							CatalogueHandler.generateDefaultErrorMessage();
						});
					}

					$this.deleteSolicitude = function(id, idnumber) {
						ModalService.showModalWindow(ModalParamsService.deleteSolicitude(id, idnumber, $scope.configparams));
					}
					$this.showDeleteButtonByStatus = function(status) {
						return SOLICITUDE_DELETE_STATUS.indexOf(status.catalogValue) === -1;
					}

					$this.isEditMode = function(status) {
						$log.debug(SOLICITUDE_EDIT_MODE_STATUS_CODE.indexOf(parseInt(status)));
						return (SOLICITUDE_EDIT_MODE_STATUS_CODE.indexOf(parseInt(status)) >= 0);
					}

					$this.getURLFromEntity = function(entityName) {
						return (EntityUrlService.getEntityURL(entityName));
					};
					$scope.getURLFromEntity = this.getURLFromEntity;

					$scope.solicitudeTypes = $this.getSolicitudeTypes;
					$scope.solicitudeStatus = $this.getSolicitudeStatus;
					$scope.close = function() {
						$modalInstance.dismiss('close');
					};
					$scope.values = {};
					$scope.deleteSolicitude = $this.deleteSolicitude;
					$scope.showDeleteButtonByStatus = $this.showDeleteButtonByStatus;
					$scope.isEditMode = $this.isEditMode;
					$scope.images =IMAGES_URL;
					$scope.open = function(size, dateValues, params, paramName) {
						DatePickerPopup.datePickerConfig($scope,$modal,$filter,size, dateValues, params, paramName);
					}
				},
			}
		});