angular.module('secosanGeneric').directive('containerLmrFitosanitarios', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_LMR + "container-lmr-fitosanitarios.html"
	};
});
