angular.module('secosanGeneric').directive('formLmrFitosanitarios', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_LMR + "form-lmr-fitosanitarios.html"
	};
});
