angular.module('secosanGeneric').directive('formSolicitudLogos', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_SOLICITUD_LOGOS + "form-solicitud-logos.html"
	};
});
