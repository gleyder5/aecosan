angular.module('secosanGeneric').directive('containerSolicitudLogos', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_SOLICITUD_LOGOS + "container-solicitud-logos.html"
	};
});
