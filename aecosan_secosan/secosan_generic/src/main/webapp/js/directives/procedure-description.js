angular.module('secosan').directive('procedureDescription', function($log, TEMPLATE_PATHS, WelcomeMenu,ProcedureDescriptionService) {
	var $this = this;

	this.setMessagesIntoScope = function(scope, messagesObject) {
		$log.debug(messagesObject);
		if (messagesObject !== undefined) {
			$this.setupButton(scope, messagesObject);
		}
		$this.setupMessage(scope, messagesObject);
	};
	this.setupButton = function(scope, messagesObject) {
		if (messagesObject.buttonsSetup !== undefined && messagesObject.buttonsSetup.length > 0) {
			scope.buttonsSetup = messagesObject.buttonsSetup;
		}
	};

	this.setupMessage = function(scope, messagesObject) {
		var message = "";
		var showMessageBox = false;
		let template = null;
		if ((messagesObject !== undefined)) {
			message = messagesObject.message;
			template = messagesObject.template;
			showMessageBox = true;
		}
		scope.description = message;
		scope.template = template;
		scope.showMessageBox = showMessageBox;
	};
	return {
		restrict : 'E',
		scope : {
			description : "@",
			template:"@"
		},
		templateUrl : TEMPLATE_PATHS.DIRECTIVES + 'procedure-description.html',
		controller : function($scope) {
			$this.setupMessage($scope);
		},
		link : function(scope) {

			scope.$watch(function() {
				return WelcomeMenu.getWelcomeMenuItem()
			}, function() {
				$this.setMessagesIntoScope(scope, WelcomeMenu.getMessagesObjectByActive())
			});

		}
	}
});