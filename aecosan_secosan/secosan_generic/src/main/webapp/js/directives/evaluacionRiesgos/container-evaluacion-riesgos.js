angular.module('secosanGeneric').directive('containerEvaluacionRiesgos', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_RIESGOS + "container-evaluacion-riesgos.html"
	};
});
