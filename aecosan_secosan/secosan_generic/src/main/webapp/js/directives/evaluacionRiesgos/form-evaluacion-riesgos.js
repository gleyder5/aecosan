angular.module('secosanGeneric').directive('formEvaluacionRiesgos', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_RIESGOS + "form-evaluacion-riesgos.html"
	};
});
