angular.module('secosanGeneric').directive('containerRegistroAguas', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_AGUAS + "container-registro-aguas.html"
	};
});
