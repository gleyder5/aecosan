angular.module('secosanGeneric').directive('formRegistroAguas', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_AGUAS + "form-registro-aguas.html"
	};
});
