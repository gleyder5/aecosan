angular.module('secosanGeneric').directive('containerProcedimientoGenerico', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		// templateUrl : TEMPLATE_PATHS.DIRECTIVES_PROCEDIMIENTO_GENERICO + "container-procedimiento-generico.html"
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_PROCEDIMIENTO_GENERICO + "container-procedimiento-generico-recsara.html"
	};
});
