angular.module('secosanGeneric').directive('seleccionTipoProcedimiento', function(TEMPLATE_PATHS,ProcedureDescriptionService,TipoProcedimientoManagementService,$location) {
    return {
        restrict : 'E',
        templateUrl : TEMPLATE_PATHS.DIRECTIVES_PROCEDIMIENTO_GENERICO + "seleccion-tipo-procedimiento.html",
        controller : function ($scope) {
            $scope.tipoProcedimientosList = []
            $scope.refresh = function () {
                TipoProcedimientoManagementService.getTipoProcedimientoList().success(function (data) {
                    $scope.tipoProcedimientosList = data;
                })  ;
            };
            $scope.data  = {} ;
            $scope.tipoProc = "Contratos menores";
            $scope.showInputTipoProcedimiento = false;
            $scope.irAContratosMenores = function(){
                $scope.tipoProc = "Contratos menores";
                ProcedureDescriptionService.setData({tipoProcedimiento:$scope.tipoProc});
                $location.path("/procedimientoGenerico");
            };
            $scope.changeTipo = function(opt) {
                $scope.tipoProc = opt;
                ProcedureDescriptionService.setData({tipoProcedimiento:$scope.tipoProc});
                if($scope.tipoProc==="") {
                    ProcedureDescriptionService.setData({tipoProcedimiento:$scope.data.tipoProcedimientoCustom});
                    $scope.showInputTipoProcedimiento = true;
                }else{
                    $scope.showInputTipoProcedimiento = false;
                }
            };
            ProcedureDescriptionService.setData({tipoProcedimiento :$scope.tipoProc});
            $scope.refresh();
            $scope.changeTipo("");
        },
        link:function(scope){
            scope.$watchCollection('data',function (newV,oldV) {
                if(newV===oldV){
                    return false;
                }
                ProcedureDescriptionService.setData(newV);
            });
        }
    };
});

