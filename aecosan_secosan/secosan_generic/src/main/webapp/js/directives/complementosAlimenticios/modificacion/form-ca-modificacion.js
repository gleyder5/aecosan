angular.module('secosanGeneric').directive('formCaModificacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "modificacion/form-ca-modificacion.html"
	};
});