angular.module('secosanGeneric').directive('containerCaModificacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "modificacion/container-ca-modificacion.html"
	};
});