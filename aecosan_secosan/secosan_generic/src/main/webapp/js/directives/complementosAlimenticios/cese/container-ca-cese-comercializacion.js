angular.module('secosanGeneric').directive('containerCaCeseComercializacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "cese/container-ca-cese-comercializacion.html"
	};
});