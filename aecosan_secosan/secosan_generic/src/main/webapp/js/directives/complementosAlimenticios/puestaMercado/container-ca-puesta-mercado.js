angular.module('secosanGeneric').directive('containerCaPuestaMercado', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "puestaMercado/container-ca-puesta-mercado.html"
	};
});