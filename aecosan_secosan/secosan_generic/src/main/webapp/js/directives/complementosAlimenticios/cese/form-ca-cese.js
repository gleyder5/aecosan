angular.module('secosanGeneric').directive('formCaCese', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "cese/form-ca-cese.html"
	};
});