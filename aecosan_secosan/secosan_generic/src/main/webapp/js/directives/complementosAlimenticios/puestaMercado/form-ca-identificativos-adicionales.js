angular.module('secosanGeneric').directive('formCaIdentificativosAdicionales', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "puestaMercado/form-ca-identificativos-adicionales.html"
	};
});