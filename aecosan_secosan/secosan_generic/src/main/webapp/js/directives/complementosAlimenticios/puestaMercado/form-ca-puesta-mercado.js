angular.module('secosanGeneric').directive('formCaPuestaMercado', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMPLEMENTOS_ALIMENTICIOS + "puestaMercado/form-ca-puesta-mercado.html"
	};
});
