angular.module('secosanGeneric').directive('formAgModificacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "modificacion/form-ag-modificacion.html"
	};
});