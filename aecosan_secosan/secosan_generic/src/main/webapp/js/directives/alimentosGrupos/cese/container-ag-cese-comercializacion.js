angular.module('secosanGeneric').directive('containerAgCeseComercializacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "cese/container-ag-cese-comercializacion.html"
	};
});