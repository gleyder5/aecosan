angular.module('secosanGeneric').directive('formAgCese', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "cese/form-ag-cese.html"
	};
});