angular.module('secosanGeneric').directive('formAgIdentificativosAdicionales', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "puestaMercado/form-ag-identificativos-adicionales.html"
	};
});