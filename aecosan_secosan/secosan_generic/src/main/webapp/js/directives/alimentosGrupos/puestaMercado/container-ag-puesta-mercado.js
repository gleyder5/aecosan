angular.module('secosanGeneric').directive('containerAgPuestaMercado', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "puestaMercado/container-ag-puesta-mercado.html"
	};
});