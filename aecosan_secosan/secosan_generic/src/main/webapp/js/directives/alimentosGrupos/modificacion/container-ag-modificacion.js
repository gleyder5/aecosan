angular.module('secosanGeneric').directive('containerAgModificacion', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "modificacion/container-ag-modificacion.html"
	};
});