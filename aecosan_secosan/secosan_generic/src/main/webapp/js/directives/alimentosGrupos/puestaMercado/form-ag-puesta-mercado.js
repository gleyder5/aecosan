angular.module('secosanGeneric').directive('formAgPuestaMercado', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_ALIMENTOS_GRUPOS + "puestaMercado/form-ag-puesta-mercado.html"
	};
});
