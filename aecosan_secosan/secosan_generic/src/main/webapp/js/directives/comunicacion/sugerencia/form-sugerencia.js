angular.module('secosanGeneric').directive('formSugerencia', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMUNICACION + "sugerencia/form-sugerencia.html"
	};
});
