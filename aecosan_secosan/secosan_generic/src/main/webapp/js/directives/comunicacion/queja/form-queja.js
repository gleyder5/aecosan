angular.module('secosanGeneric').directive('formQueja', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMUNICACION + "queja/form-queja.html"
	};
});
