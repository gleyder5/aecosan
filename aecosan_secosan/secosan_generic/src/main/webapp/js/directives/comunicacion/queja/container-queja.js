angular.module('secosanGeneric').directive('containerQueja', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMUNICACION + "queja/container-queja.html"
	};
});
