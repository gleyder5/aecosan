angular.module('secosanGeneric').directive('containerSugerencia', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_COMUNICACION + "sugerencia/container-sugerencia.html"
	};
});
