angular.module('secosanGeneric').directive('formLogin', function($log,GENERIC_TEMPLATE_PATHS,IMAGES_URL) {
	return {
		restrict : 'E',
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_LOGIN + "form-login.html",
		scope:{
			sendform:'&',
			showloading:'=',
			loginerrormessage:'=',
			formmodel:'='
		},
		controller:function($scope){
			$scope.images =IMAGES_URL;
			$scope.sendForm = function(){
				$scope.sendform();		
			}
		}
	};
});
