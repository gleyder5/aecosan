angular.module('secosanGeneric').directive('containerLoginTramitador', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_LOGIN + "container-login-tramitador.html"
	};
});
