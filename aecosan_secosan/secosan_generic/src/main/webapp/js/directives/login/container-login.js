angular.module('secosanGeneric').directive('containerLogin', function(TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		templateUrl : TEMPLATE_PATHS.DIRECTIVES_LOGIN + "container-login.html"
	};
});
