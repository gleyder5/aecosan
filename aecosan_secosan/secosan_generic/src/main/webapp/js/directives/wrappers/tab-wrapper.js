'use strict';
angular.module('secosanGeneric')
    .directive('tabWrapper', ['$q', '$log', '$timeout', 'GENERIC_TEMPLATE_PATHS','IMAGES_URL','POPUP_IMAGES','$routeParams','ValidationUtilsService',function ($q,$log, $timeout,GENERIC_TEMPLATE_PATHS,IMAGES_URL,POPUP_IMAGES,$routeParams,ValidationUtilsService) {
        return {
            restrict: "E",
            scope: true,
            controller: function ($scope) {
                console.log("TabsCtrl");
                console.log($scope);
                console.log("TabsCtrl");
                $scope.findWorkspace = function (workspace, coord) {

                    var wst = workspace.tabList[coord];

                    if (!wst) {
                        console.log("Error in coord");
                        return false;
                    }
                    else {
                        return wst;
                    }
                }
            },
            templateUrl: GENERIC_TEMPLATE_PATHS.DIRECTIVES_WRAPPERS + "tab-wrapper.html",
            link: function ($scope, element, attrs) {
                $scope.isReadOnly = $routeParams.v == 'v' ? true : false;
                if (!$scope.workspaceTabs) {
                    $scope.workspaceTabs = new Object();
                }

                $scope.workspaceTabs = $scope.$eval(attrs.workspaceInfo);
                $scope.workspaceTabs.loaded = false;

                if (attrs.id !== undefined && $scope.workspaceTabs && angular.isUndefined($scope.workspaceTabs.id)) {
                    $scope.workspaceTabs.id = attrs.id;
                }
                else if (angular.isUndefined($scope.workspaceTabs.id)) {
                    var hash = parseInt(Date.now() * Math.random()).toString(16); //CREATE HEXADECIMAL HASH
                    element.attr("id", hash);
                    $scope.workspaceTabs.id = hash;
                }
                var loading_stack = [];
                var loaded_stack = [];
                $scope.$on('$includeContentRequested', function (event, url) {
                        loading_stack.push(url);
                });
                $scope.$on('$includeContentLoaded', function (event, url) {
                    loaded_stack.push(url);
                    if(loaded_stack.length==$scope.workspaceTabs.tabList.length){
                        $scope.workspaceTabs.loaded = true;
                        if(typeof $scope.triggerValidations ==='function') {
                            $timeout(function () {
                                $scope.triggerValidations();
                            }, 300);
                        }
                        $scope.workspaceTabs.setTabFocusByIndex($scope.tabCtrl,0);
                    }

                     // console.log(url);
                });




                $scope.workspaceTabs.getImage = function (hasError){
                    if(hasError){
                        return IMAGES_URL+"/"+POPUP_IMAGES.error;
                    }else{
                        return IMAGES_URL+"/"+POPUP_IMAGES.ok;
                    }

                }
                $scope.workspaceTabs.getTabConfig  = function(tabConfig){

                    var globalTabConfig = {
                        autoload : true,
                        temp : false,
                        head: {
                            icon: 'glyphicon glyphicon-refresh glyphicon-refresh-animate',
                            text: ''
                        },
                        content: '',
                        templateUrl: '',
                        resolve: {
                            formData: []
                        },
                        disabled: false,
                        closable: false,
                        callback: function () {
                        },
                        onlyEdit : false
                    };

                    if (tabConfig && typeof tabConfig === 'object') {
                        if (tabConfig.id) {
                            globalTabConfig.id = tabConfig.id;
                        }else{
                            console.error("Please define a tab identifier");
                            return;
                        }

                        if (tabConfig.head && tabConfig.head.text) {
                            globalTabConfig.head.text = tabConfig.head.text;
                        }
                        if (tabConfig.temp) {
                            globalTabConfig.temp = tabConfig.temp;
                        }

                        if (tabConfig.content) {
                            globalTabConfig.content = tabConfig.content;
                        }

                        if (tabConfig.templateUrl) {
                            globalTabConfig.templateUrl = tabConfig.templateUrl;
                        }

                        if (tabConfig.resolve) {
                            globalTabConfig.resolve = tabConfig.resolve;
                        }

                        if (tabConfig.disabled) {
                            globalTabConfig.disabled = tabConfig.disabled;
                        }

                        if (tabConfig.closable) {
                            globalTabConfig.closable = tabConfig.closable;
                        }

                        if (tabConfig.autoload) {
                            globalTabConfig.autoload = tabConfig.autoload;
                        }

                        if (tabConfig.validate) {
                            globalTabConfig.validate = tabConfig.validate;
                        }

                        if (tabConfig.callback && tabConfig.callback === 'function') {
                            globalTabConfig.callback = tabConfig.callback;
                        }

                        if (tabConfig.head.icon) {
                            globalTabConfig.head.icon = tabConfig.head.icon;
                        }

                        if (tabConfig.parameters) {
                            globalTabConfig.parameters = tabConfig.parameters;
                            $scope.parameters = tabConfig.parameters;
                        }
                        else {
                            $scope.parameters = {};
                        }

                    }
                    return globalTabConfig;

                }
                $scope.workspaceTabs.addTabAfterTabID= function(tabConfig,tabId){
                    // var index = $scope.workspaceTabs.tabList.findIndex(function(tab){return tab.id===tabId});
                    var index = 0 ;
                    angular.forEach($scope.workspaceTabs.tabList,function (tab,idx) {
                        if(tab.id===tabId){
                            index = idx;
                            return ;
                        }
                    });
                    var globalTabConfig = $scope.workspaceTabs.getTabConfig(tabConfig);
                    if(globalTabConfig.validate){
                        // ValidationUtilsService.register(globalTabConfig,globalTabConfig.id,scopedVars)
                    }
                    $scope.workspaceTabs.tabList.splice( index + 1 , 0, globalTabConfig);
                    //$scope.workspaceTabs.setTabFocusByID($scope.workspaceTabs,globalTabConfig.id);

                }

                $scope.workspaceTabs.addTab= function (tabConfig, coordinates) {
                    var deferred = $q.defer();

                    if ($scope.workspaceTabs.maxTabs && $scope.workspaceTabs.maxTabs < ($scope.workspaceTabs.tabList.length + 1)) {
                        $log.error("The maximum allowed tabs has been reached", "Error Opening", {closeButton: true})
                        deferred.reject("tabs limit exceeded");
                        return false;
                    }

                    var globalTabConfig = $scope.workspaceTabs.getTabConfig(tabConfig);

                    $scope.workspaceTabs.tabList.push(globalTabConfig);
                    deferred.resolve(globalTabConfig);
                    $scope.workspaceTabs.setTabFocusByID($scope.workspaceTabs,globalTabConfig.id);
                    return deferred.promise;
                };


                $scope.workspaceTabs.addTabWS = function (tabConfig, coordinates) {

                    var deferred = $q.defer();

                    if ($scope.workspaceTabs.maxTabs && $scope.workspaceTabs.maxTabs < ($scope.workspaceTabs.tabList.length + 1)) {
                        $log.error("The maximum allowed tabs has been reached", "Error Opening", {closeButton: true})
                        deferred.reject("tabs limit exceeded");
                        return false;
                    }

                    var globalTabConfig = $scope.workspaceTabs.getTabConfig(tabConfig);

                    if (!coordinates)
                        coordinates = [];

                    // var ws = $scope.workspaceTabs.getWorkspaceTabs(coordinates);

                    // if (!ws) {
                    //     $log.error("There was an error calling the tab!", "Opps", {closeButton: true})
                    //     deferred.reject("error loading tab");
                    //     return false;
                    // }

                    if (coordinates.length === 1) {
                        ws.childWorkspace.tabList.push(globalTabConfig);

                        $timeout(function () {
                            coordinates.push(ws.childWorkspace.tabList.length);
                            $scope.workspaceTabs.setWorkspaceTabFocus(coordinates);
                        });
                    }
                    else if (coordinates.length === 0 || coordinates.length > 1) {
                        ws.tabList.push(globalTabConfig);

                        $timeout(function () {
                            coordinates.push(ws.tabList.length);
                            $scope.workspaceTabs.setWorkspaceTabFocus(coordinates);
                        });
                    }


                    // TODO MANAGE REQUEST TEMPLATES
                    var loading_stack = [];
                    $scope.$on('$includeContentRequested', function (event, url) {
                        loading_stack.push(url)
                    });

                    $scope.$on('$includeContentLoaded', function (event, url) {
                        loading_stack.splice(loading_stack.indexOf(url), 1);

                        if (loading_stack.length === 0) {

                            //REMOVE ICON LOADING
                            /*if (tabConfig.head.icon) {
                             $scope.workspaceTabs.tabList[$scope.workspaceTabs.tabList.length - 1].head.icon = tabConfig.head.icon;
                             }
                             else {
                             $scope.workspaceTabs.tabList[$scope.workspaceTabs.tabList.length - 1].head.icon = '';
                             }*/

                            //RESOLVE SECTION

                            if (globalTabConfig.resolve) {

                                if (globalTabConfig.resolve.formData.length > 0) {

                                    var last_index = $scope.workspaceTabs.tabList.length;
                                    var workspace_tab_container = element.find('div[data-tabid="' + $scope.workspaceTabs.id + last_index + '"][role="tab-content"]');

                                    if (workspace_tab_container.length > 0) {

                                        var formDataCpy = [];

                                        angular.copy(globalTabConfig.resolve.formData, formDataCpy);
                                        angular.forEach(formDataCpy, function (element, index) {

                                            if (element.type == "text") {
                                                workspace_tab_container.find("input#" + element.id).val(element.value);
                                            }
                                            else if (element.type == "select") {
                                                workspace_tab_container.find("input#" + element.id).val(element.value);
                                            }
                                            else if (element.type == "checkbox") {
                                                workspace_tab_container.find("input#" + element.id).prop("checked", element.value);
                                            }
                                            else if (element.type == "html") {
                                                workspace_tab_container.find("#" + element.id).html(element.value);
                                            }

                                            //clean array to set form inputs
                                            globalTabConfig.resolve.formData.splice(index, 1);

                                        });
                                    }

                                }
                            }

                            //CALLBACK
                            if (globalTabConfig.callback && typeof globalTabConfig.callback === 'function') {
                                globalTabConfig.callback();
                            }
                            console.log(globalTabConfig);


                            //RETURN RESOLVE PROMISE
                            deferred.resolve(globalTabConfig);
                        }

                    });

                    return deferred.promise;

                }

                $scope.workspaceTabs.closeTab = function (tab, event) {

                	if(event != undefined){
                		event.preventDefault();
                	}


                	// var index = $scope.workspaceTabs.tabList.findIndex(function(v){return v.id === tab.id});
                    // var index = $scope.workspaceTabs.tabList.findIndex(function(tab){return tab.id===tabId});
                    var index = 0 ;
                    angular.forEach($scope.workspaceTabs.tabList,function (v,idx ){
                        if(v.id===tab.id){
                            index = idx;
                            return ;
                        }
                    });

                    if ($scope.workspaceTabs.tabList.length >= 0) {
                        $scope.workspaceTabs.tabList.splice(index, 1);
                    }

                }

                $scope.workspaceTabs.delAllTabRight = function (tab) {

                    if ($scope.workspaceTabs.tabList.length >= 0) {

                        $scope.workspaceTabs.tabList.splice(tab.$index + 1);

                    }

                }

                $scope.workspaceTabs.delAllTabLeft = function (tab) {

                    if ($scope.workspaceTabs.tabList.length >= 0) {

                        angular.forEach($scope.workspaceTabs.tabList, function (index, tab) {

                            if (index >= tab.$index)
                                return false;

                            $scope.workspaceTabs.tabList.splice(index);

                        });

                    }

                }

                $scope.workspaceTabs.backTab = function (currentIndex) {
                    if(currentIndex>0)
                        $scope.workspaceTabs.setTabFocusByIndex($scope.workspaceTabs,currentIndex-1);
                }

                $scope.workspaceTabs.nextTab = function (currentIndex){
                    if(currentIndex<$scope.workspaceTabs.tabList.length)
                        $scope.workspaceTabs.setTabFocusByIndex($scope.workspaceTabs,currentIndex+1);
                }


                $scope.workspaceTabs.getTabByID = function (workspaceCfg,tabID) {

                    var result;

                    if (workspaceCfg.id === tabID) {
                        result = workspaceCfg;
                    }

                    if (result == undefined) {
                        var index = 0 ;
                        angular.forEach(workspaceCfg.tabList, function (workspace) {
                            if (!angular.isUndefined(result)) {
                                return true;
                            }
                            if (workspace.id === tabID) {
                                result = workspace;
                                result.index = index ;

                            }
                            else {

                                if (!angular.isUndefined(workspace.childWorkspace)) {

                                    var tempWS = $scope.workspaceTabs.getWorkspaceTabByID(workspace.childWorkspace, tabID);

                                    if (tempWS != false) {

                                        result = tempWS;

                                    }

                                }

                            }
                        index++;
                        });
                    }
                    if (!angular.isUndefined(result)) {
                        return result;
                    }
                    else {
                        return false;
                    }
                };


                $scope.workspaceTabs.setTabFocusByID = function (ws,id) {
                    if(ws==undefined){
                        ws =   $scope.workspaceTabs;
                    }

                    var tab  =  $scope.workspaceTabs.getTabByID(ws,id);
                    if(tab == undefined){
                        $log.error("Tab not found!")
                        return;
                    }
                    tab.active = 1 ;
                    tab.autoload  = true;
                    console.debug("tab "+tab.id+" focused");
                };
                $scope.workspaceTabs.setTabFocusByIndex = function (ws,idx) {

                    var tab  =  ws.tabList[idx];
                    if(tab == undefined){
                        $log.error("Tab not found!")
                        return;
                    }
                    tab.active = 1 ;
                    tab.autoload  = true;
                    console.debug("tab "+tab.id+" focused");
                };


                $scope.loadParameters = function (parameters) {

                    $scope.parameters = {};

                    for (var key in parameters) {

                        if (parameters.hasOwnProperty(key)) {

                            if (parameters[key])
                                $scope.parameters[key] = parameters[key];

                        }

                    }

                };

                $scope.workspaceTabs.loadTabContent = function (tab) {

                    tab.autoload = true;

                };


                $scope.workspaceTabs.initialize = function(){

                    // REMOVE TEMPORAL TABS


                    if(typeof $scope.triggerValidations === "function"){
                        console.debug("Executing initial validations on tabs forms");
                        $scope.triggerValidations();
                    }
                    var tabsToRemove = [];
                    if($scope.isReadOnly) {
                        tabsToRemove = $scope.workspaceTabs.tabList.filter(function (v) {
                            return v.onlyEdit;
                        });
                    }else {
                        tabsToRemove = $scope.workspaceTabs.tabList.filter(function (v) {
                            return v.isSent;
                        });
                    }
                    if( tabsToRemove.length){
                        tabsToRemove.forEach(function(tab){
                            $scope.workspaceTabs.closeTab(tab);
                        })
                    }
                }
                $scope.workspaceTabs.initialize();


            } // END LINK FUNCTION
        }

    }]);


