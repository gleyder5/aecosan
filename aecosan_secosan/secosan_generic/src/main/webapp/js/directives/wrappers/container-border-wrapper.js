angular.module('secosanGeneric').directive('containerBorderWrapper', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		replace : true,
		transclude : true,
		scope : {
			extraclass : "@"
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_WRAPPERS + "container-border-wrapper.html"
	};
});