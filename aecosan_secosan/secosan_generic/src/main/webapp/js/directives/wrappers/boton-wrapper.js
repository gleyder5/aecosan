angular.module('secosanGeneric').directive('botonWrapper', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		transclude : true,
		scope : {
			myid : '@',
			extraclass :'@'
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_WRAPPERS + "boton-wrapper.html"
	};
});