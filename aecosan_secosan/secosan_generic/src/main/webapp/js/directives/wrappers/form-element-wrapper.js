angular.module('secosanGeneric').directive('formElementWrapper', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		replace : true,
		transclude : true,
		scope : {
			extraclass : "@",
			separator : "@",
			bootstrapSize : "@",
			modelo : "="
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_WRAPPERS + "form-element-wrapper.html"
	};
});