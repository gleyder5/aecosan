angular.module('secosanGeneric').directive('formBackgroundWrapper', function(GENERIC_TEMPLATE_PATHS) {
	return {
		restrict : 'E',
		replace : true,
		transclude : true,
		scope : {
			extraclass : "@"
		},
		templateUrl : GENERIC_TEMPLATE_PATHS.DIRECTIVES_WRAPPERS + "form-background-wrapper.html"
	};
});