var navpilsHelper = {

	activeNavpillscss : "active",

	navpilsActionIndustrias : function(navpillElementId) {
		var navPillElement = $(navpillElementId);
		$(".nav-pills").children().each(function() {
			$(this).removeClass(navpilsHelper.activeNavpillscss);
		});
		navPillElement.addClass(navpilsHelper.activeNavpillscss);
	}
};