'use strict';
angular.module("secosan").controller(
		"HomeController",
		[ '$scope', '$http', 'HeaderTitle', 'HeaderSolicitudNumber', 'Messages', 'CatalogueHandler', 'CATALOGUE_NAMES',
				function($scope, $http, HeaderTitle, HeaderSolicitudNumber, Messages, CatalogueHandler, CATALOGUE_NAMES) {
					$scope.debug = true;
					$scope.title = '';
					Messages.setMessageAndType("test", Messages.info);
					HeaderSolicitudNumber.setNumberAndTitle("Número de Solicitud", 232323);
					HeaderTitle.setTitles("hello");
					$scope.toggleDebug = function() {
						$scope.debug = !$scope.debug;
					};

					CatalogueHandler.getCatalogue(CATALOGUE_NAMES.COUNTRIES).success(function(data) {

						$scope.paises = CatalogueHandler.generateCatalogueDataForShow(data);
					});
					$scope.selectedCountry = "";
					$scope.selectedProvincia = "";
					$scope.provincias = [];

					$scope.setProvinciasRelated = function() {
						CatalogueHandler.getRelatedCatalogue(CATALOGUE_NAMES.PROVINCIES, CATALOGUE_NAMES.MUNICIPALITY_PARENT_COUNTRY, $scope.selectedCountry.id).success(function(data) {
							$scope.provincias = [];
							$scope.provincias = CatalogueHandler.generateCatalogueDataForShow(data);
						});
					}

				} ]);