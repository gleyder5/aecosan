package es.grupoavalon.aecosan.secosan.util.exception;

public class SecosanException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	public static final String CLASS_NOT_MANAGED = "Class not managed";

	public SecosanException(String message, Throwable cause) {
		super(message, cause);
	
	}

	public SecosanException(String message) {
		super(message);
	}

	public SecosanException(Throwable cause) {
		super(cause);
		
	}

}
