package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroIndustriasRepository;

@Component
public class RegistroIndustriasRepositoryImpl extends AbstractCrudRespositoryImpl<RegistroIndustriasEntity, Long> implements RegistroIndustriasRepository {

	@Override
	protected Class<RegistroIndustriasEntity> getClassType() {
		return RegistroIndustriasEntity.class;
	}

}
