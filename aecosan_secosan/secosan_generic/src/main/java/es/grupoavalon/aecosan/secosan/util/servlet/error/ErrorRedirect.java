package es.grupoavalon.aecosan.secosan.util.servlet.error;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Controller
public class ErrorRedirect {
	
	private static final String INIT_PAGE;
	private static final String INIT_PAGE_ADMIN;
	static {
		INIT_PAGE = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_INIT_PAGE);
		INIT_PAGE_ADMIN = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_INIT_ADMIN_PAGE);
	}
	
	private static final String NOT_FOUND_URL = "/404";
	private static final String SERVER_ERROR_URL = "/500";
	private static final String FORBIDEN_URL = "/403";
	private static final String COMMON_FOLDER = "/errors/";
	private static final String NOT_FOUND_PAGE = COMMON_FOLDER + "404";
	private static final String SERVER_ERROR = COMMON_FOLDER + "500";
	private static final String FORBIDEN_ERROR = COMMON_FOLDER + "403";

	@RequestMapping(value = NOT_FOUND_URL, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView notFound404(HttpServletRequest request) {
		return redirectToErrorPage(request, NOT_FOUND_PAGE);
	}

	@RequestMapping(value = SERVER_ERROR_URL, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView serverError500(HttpServletRequest request) {
		return redirectToErrorPage(request, SERVER_ERROR);
	}

	@RequestMapping(value = FORBIDEN_URL, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView serverError403(HttpServletRequest request) {
		return redirectToErrorPage(request, FORBIDEN_ERROR);
	}

	private ModelAndView redirectToErrorPage(HttpServletRequest request, String pageName) {
		ModelAndView alertJsp = new ModelAndView();
		alertJsp.addObject("originalRequest", initPageBasedOnRequest(request));
		alertJsp.setViewName(pageName);
		return alertJsp;
	}

	private static String initPageBasedOnRequest(HttpServletRequest req) {
		String initPage = INIT_PAGE;
		if (StringUtils.contains(req.getRequestURI(), "/admin/")) {
			initPage = INIT_PAGE_ADMIN;
		}
		return initPage;
	}
}
