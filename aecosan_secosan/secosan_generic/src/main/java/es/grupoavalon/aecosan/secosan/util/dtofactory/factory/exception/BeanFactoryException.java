package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception;

/**
 * Exception that indicate an error during the creation of a DTO
 * 
 * @author ottoabreu
 * 
 */
public class BeanFactoryException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * GENERAL_ERROR =
	 * "An general error occurs and can not continue to the creation of the Object: "
	 * ;
	 */
	public static final String GENERAL_ERROR = "An general error occurs and can not continue to the creation of the Object: ";
	/**
	 *  BAD_MAPPING ="The mapping annotation needs at least to have configured one parameter, found annotation but no configuration";
	 */
	public static final String BAD_MAPPING ="The mapping annotation needs at least to have configured one parameter, found annotation but no configuration in field:";
	/**
	 *  NO_MAPPING ="It is required to have at least one field with the annotation BeanToBeanMapping, found 0";
	 */
	public static final String NO_MAPPING ="It is required to have at least one field with the annotation BeanToBeanMapping, found 0";
 
	/**
	 * INSTANTIATE_TRANSFORMER_ERROR = "Can not instantiate a custom transformer due an error: ";
	 */
	public static final String INSTANTIATE_TRANSFORMER_ERROR = "Can not instantiate a custom transformer due an error: ";
	/**
	 * GET_FIELD_NAME_JPA_ERROR="Can not generate the field name in JPA style due an error:";
	 */
	public static final String GET_FIELD_NAME_JPA_ERROR="Can not generate the field name in JPA style due an error:";
	
	public BeanFactoryException() {

	}

	public BeanFactoryException(String arg0) {
		super(arg0);

	}

	public BeanFactoryException(Throwable arg0) {
		super(arg0);

	}

	public BeanFactoryException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

}
