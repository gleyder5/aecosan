package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.SubvencionesAsocuaeService;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesAsocuaeDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.SubvencionesAsocuaeDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubvencionesAsocuaeServiceImpl extends AbstractSolicitudService implements SubvencionesAsocuaeService {

	@Autowired
	private SubvencionesAsocuaeDAO subvencionesAsocuaeDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO subvencionesAsocuaeDTO) {
		try {
			solicitudPrepareNullDTO(subvencionesAsocuaeDTO);
			prepareSubvencionesAsocuaeNullability((SubvencionesAsocuaeDTO) subvencionesAsocuaeDTO);
			addSubvencionesAsocuaeEntity(user, (SubvencionesAsocuaeDTO) subvencionesAsocuaeDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + subvencionesAsocuaeDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO subvencionesAsocuaeDTO) {
		try {
			solicitudPrepareNullDTO(subvencionesAsocuaeDTO);
			prepareSubvencionesAsocuaeNullability((SubvencionesAsocuaeDTO) subvencionesAsocuaeDTO);
			updateProcediminetoGeneral(user, (SubvencionesAsocuaeDTO) subvencionesAsocuaeDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + subvencionesAsocuaeDTO);
		}
	}

	private void addSubvencionesAsocuaeEntity(String user, SubvencionesAsocuaeDTO subvencionesAsocuaeDTO) {
		UbicacionGeograficaDTO location = null;
		if(null != subvencionesAsocuaeDTO.getLugarOMedio()) {
			location = subvencionesAsocuaeDTO.getLugarOMedio().getLocation();
			if (location != null) {
				location.setProvince(SecosanUtil.checkDtoNullability(location.getProvince()));
				location.setMunicipio(SecosanUtil.checkDtoNullability(location.getMunicipio()));
			}
			subvencionesAsocuaeDTO.getLugarOMedio().setLocation(location);
		}
		SubvencionesAsocuaeEntity subvencionesAsocuaeEntity	=	generateSubvencionesAsocuaeEntityFromDTO(subvencionesAsocuaeDTO);
		setUserCreatorByUserId(user, subvencionesAsocuaeEntity);
		prepareAddSolicitudEntity(subvencionesAsocuaeEntity,subvencionesAsocuaeDTO);
		SubvencionesAsocuaeEntity subvencionesAsocuaeEntityNew	=	subvencionesAsocuaeDAO.addSubvencionesAsocuaeEntity(subvencionesAsocuaeEntity);
		if(subvencionesAsocuaeDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(subvencionesAsocuaeEntityNew);
		}
	}

	private void updateProcediminetoGeneral(String user, SubvencionesAsocuaeDTO subvencionesAsocuaeDTO) {
		SubvencionesAsocuaeEntity subvencionesAsocuaeEntity= transformDTOToEntity(subvencionesAsocuaeDTO, SubvencionesAsocuaeEntity.class);
		setUserCreatorByUserId(user, subvencionesAsocuaeEntity);
		prepareUpdateSolicitudEntity(subvencionesAsocuaeEntity, subvencionesAsocuaeDTO);
		subvencionesAsocuaeDAO.updateSubvencionesAsocuaeEntity(subvencionesAsocuaeEntity);
		if(subvencionesAsocuaeDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(subvencionesAsocuaeEntity);
		}
	}

	private SubvencionesAsocuaeEntity generateSubvencionesAsocuaeEntityFromDTO(SubvencionesAsocuaeDTO subvencionesAsocuae) {
		prepareSolicitudDTO(subvencionesAsocuae);
		return transformDTOToEntity(subvencionesAsocuae,SubvencionesAsocuaeEntity.class);
	}
	private void prepareSubvencionesAsocuaeNullability(SubvencionesAsocuaeDTO subvencionesAsocuaeDTO) {
		if (null != subvencionesAsocuaeDTO.getLugarOMedio()) {
//			subvencionesAsocuaeDTO.getLugarOMedio().setLocation(SecosanUtil.checkDtoNullability(subvencionesAsocuaeDTO.getLugarOMedio().getLocation()));
			if (null != subvencionesAsocuaeDTO.getLugarOMedio().getLocation()) {
				prepareLocationNull(subvencionesAsocuaeDTO.getLugarOMedio().getLocation());
			}
		}

	}
}
