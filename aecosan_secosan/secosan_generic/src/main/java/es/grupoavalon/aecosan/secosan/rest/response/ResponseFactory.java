package es.grupoavalon.aecosan.secosan.rest.response;

import java.util.List;

import javax.ws.rs.core.Response;

import es.grupoavalon.aecosan.secosan.business.exception.ApoderamientoException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.CatalogDTO;
import es.grupoavalon.aecosan.secosan.business.exception.EpaymentException;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.CatalogListWrapper;
import es.grupoavalon.aecosan.secosan.util.property.MessagesPropertiesProvider;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.ErrorWrapper;


/**
 * Generates a Response objects
 * @author otto.abreu
 *
 */
@Component
public class ResponseFactory extends AbstractResponseFactory {
	
	@Autowired
	private MessagesPropertiesProvider errorMessageProvider;
	
	@Override
	protected MessagesPropertiesProvider getErrorMessageProvider() {
		
		return this.errorMessageProvider;
	}
		
	/**
	 * Generates a Response base on the list given
	 * 
	 * @param catalogDto
	 *            List<ValorNombreBean>
	 * @return Response
	 */
	public Response generateCatalogResponse(
			List<CatalogDTO> catalog) {
		Response response;
		if(catalog != null && !catalog.isEmpty()){
			CatalogListWrapper catalogWrapper = new CatalogListWrapper(
					catalog);
			logger.debug("generating OK response: {0}", catalogWrapper);
			response = generateOkGenericResponse(catalogWrapper);
		}else{
			response = generateOkGenericResponseNullAsEmpty(null);
		}
		
		return response;
	}
	
	/**
	 * Generates a Response base on the object given
	 * @param catalog
	 * @return
	 */
	public Response generateCatalogTableResponse(
			CatalogDTO catalog) {
		CatalogListWrapper catalogWrapper = new CatalogListWrapper(
				catalog);
		logger.debug("generating OK response: {0}",catalogWrapper);
		return generateOkGenericResponse(catalogWrapper);
	}
	
	public Response generateErrorResponseForPaymentException(Throwable t) {
		Response response;
		if (t instanceof EpaymentException && StringUtils.isNotBlank(((EpaymentException) t).getPaymentWsRemoteMessage())) {
			EpaymentException ex = (EpaymentException) t;
			ErrorWrapper errorWrapper = new ErrorWrapper();
			errorWrapper.setErrorDescription(ex.getPaymentWsRemoteMessage());
			errorWrapper.setError(EpaymentException.class.getSimpleName());
			response = generateErrorResponseFromErrorWrapper(errorWrapper);
		} else {
			response = super.generateErrorResponse(t);
		}

		return response;
	}
	public Response generateErrorResponseForApoderamientoException(Throwable t) {
		Response response;
		if (t instanceof ApoderamientoException && StringUtils.isNotBlank(((ApoderamientoException) t).getErrCode())) {
			ApoderamientoException ex = (ApoderamientoException) t;
			ErrorWrapper errorWrapper = new ErrorWrapper();
			errorWrapper.setErrorDescription(ex.getDescription());
			errorWrapper.setError(ex.getErrCode());
			response = generateErrorResponseFromErrorWrapper(errorWrapper);
		} else {
			response = super.generateErrorResponse(t);
		}

		return response;
	}

}
