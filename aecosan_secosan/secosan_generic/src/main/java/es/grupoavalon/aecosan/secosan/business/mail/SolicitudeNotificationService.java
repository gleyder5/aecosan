package es.grupoavalon.aecosan.secosan.business.mail;

import es.grupoavalon.aecosan.secosan.business.mail.dto.SolicitudeNotificationDTO;

public interface SolicitudeNotificationService {

	public void notifyChangeSolicitudeStatus(SolicitudeNotificationDTO solicitudeNotification);

}
