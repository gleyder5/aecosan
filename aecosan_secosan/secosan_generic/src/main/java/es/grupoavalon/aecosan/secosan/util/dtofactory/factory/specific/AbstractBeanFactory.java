package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanInstantiationException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.FactoryFillingException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.CustomValueTransformer;

/**
 * 
 * Class that generate a new instance of the given T class
 * 
 * @author ottoabreu
 * 
 */
public abstract class AbstractBeanFactory<T> extends AbstractObjectFactory<T> {

	protected Class<?> toInstatiate;
	// LOGGER
	private static final Logger logger = LoggerFactory
			.getLogger(AbstractBeanFactory.class);

	public enum FilterCollectionFields {
		FILTER_FIELDS_NOCOLLECTIONS("Exclude collection Fields"), FILTER_FIELDS_ONLYCOLLECTIONS(
				"Include only collection Fields");

		private String stringRepresentation;

		private FilterCollectionFields(String stringRepresentation) {
			this.stringRepresentation = stringRepresentation;

		}

		@Override
		public String toString() {
			return this.stringRepresentation;
		}
	}

	/**
	 * Constructor
	 * 
	 * @param from
	 * @param Class
	 *            <?> toInstantiateClass
	 */
	protected AbstractBeanFactory(Class<?> toInstantiateClass) {
		super(toInstantiateClass);
		this.toInstatiate = toInstantiateClass;
	}

	/**
	 * Extracts the values from the fields using the get methods and put them
	 * into the new instance using the get method
	 * 
	 * @param fields
	 *            Field[]
	 * @param newInstance
	 *            Object
	 * @throws FactoryFillingException
	 *             if can not execute the get/set methods
	 * @throws BeanFactoryException
	 *             is can not extract the values
	 */
	protected abstract void moveValuesFromObjectToNewInstance(Field[] fields,
			T newInstance) throws FactoryFillingException, BeanFactoryException;

	/**
	 * Generate a DTO from the values of an entity<br>
	 * The DTO <B>MUST HAVE THE {@link BeanToBeanMapping} annotation </b>
	 * otherwise will not do anything
	 * 
	 * @return T
	 * @throws BeanInstantiationException
	 *             if can not instantiate a new DTO object
	 * @throws BeanFactoryException
	 *             if can not execute the getter or setter methods needed
	 */
	protected T generateObjectFromOtherObject()
			throws  BeanFactoryException {

		return this.generateObjectFromOtherObject(getFields());
	}

	protected T generateObjectFromOtherObject(Field[] fields) {
		T newInstance = null;
		try {

			newInstance = this.instanciateObject();
			if(logger.isDebugEnabled()) {
				logger.debug("Object Created {0} ", newInstance.toString());
				logger.debug("fields: {0}", fields.length);
			}
			this.moveValuesFromObjectToNewInstance(fields, newInstance);
		} catch (BeanFactoryException e) {
				logger.error(BeanFactoryException.GENERAL_ERROR, e);
				throw new BeanFactoryException(
						BeanFactoryException.GENERAL_ERROR, e);
		}catch (RuntimeException e){
			throw (RuntimeException) e;
		}

		return newInstance;
	}

	/**
	 * Filters the field from an object
	 * 
	 * @param filterType
	 * @param ignoreFieldsNames
	 * @return Field[]
	 */
	protected Field[] filterFields(FilterCollectionFields filterType) {

		Field[] dtoFields = getFields(this.toInstatiate);
		return this.filterFields(filterType, dtoFields);
	}

	/**
	 * Method that return the array of declared fields of a class uses
	 * {@link Class#getFields()})
	 * 
	 * @return Field[]
	 */
	protected Field[] getFields() {
		Field[] dtoFields = getFields(this.toInstatiate);
		return dtoFields;
	}

	/**
	 * Method that return the array of declared fields of a class uses
	 * {@link Class#getFields()})
	 * 
	 * @return Field[]
	 */
	protected static Field[] getFields(Class<?> toExtractFields) {
		Field[] dtoFields = toExtractFields.getDeclaredFields();
		logger.debug("Class {0}  fields: {1}", toExtractFields , dtoFields.length);
		Set<Field> fieldsFromSuperclass = getAllFieldsThroughInherance(toExtractFields);
		fieldsFromSuperclass.addAll(Arrays.asList(dtoFields));

		getOnlyAnnotatedFields(fieldsFromSuperclass);
		validateAnnotationPresentInClassFields(fieldsFromSuperclass);

		return fieldsFromSuperclass.toArray(new Field[] {});
	}

	private static void getOnlyAnnotatedFields(Set<Field> allFields) {

		CollectionUtils.filter(allFields, new Predicate() {

			@Override
			public boolean evaluate(Object obj) {
				Field field = (Field) obj;
				if (field.isAnnotationPresent(BeanToBeanMapping.class)) {
					return true;
				} else {
					logger.debug("field:" + field.getName()
							+ " do not have annotation present, excluding");
					return false;
				}

			}
		});

	}

	private static void validateAnnotationPresentInClassFields(
			Set<Field> allFields) {
		if (allFields == null || allFields.isEmpty()) {
			throw new BeanFactoryException(BeanFactoryException.NO_MAPPING);
		}
	}

	/**
	 * Filters the field from an object
	 * 
	 * @param filterType
	 * @param ignoreFieldsNames
	 * @return Field[]
	 */
	@SuppressWarnings("unchecked")
	protected Field[] filterFields(FilterCollectionFields filterType,
			Field[] fields) {

		Field[] filteredDtoField = null;
		List<Field> fieldCollection = Arrays.asList(fields);

		// get only the fields that are not in the "blacklist"
		List<Field> fieldFiltered = null;

		switch (filterType) {

		case FILTER_FIELDS_NOCOLLECTIONS:
			// only use the fields that are not collections
			fieldFiltered = (List<Field>) CollectionUtils
					.select(fieldCollection,
							this.getExcludeCollectionFieldsPredicate());
			break;

		case FILTER_FIELDS_ONLYCOLLECTIONS:
			// only use the fields that are collections
			fieldFiltered = (List<Field>) CollectionUtils.select(
					fieldCollection, this.getOnlyCollectionFieldsPredicate());
			break;

		default:
			logger.error("Filter option not valid, received: {0}", filterType);
			throw new BeanFactoryException("Filter option not valid, received:" + filterType);

		}
		filteredDtoField = fieldFiltered
				.toArray(new Field[fieldFiltered.size()]);
		return filteredDtoField;
	}

	private Predicate getOnlyCollectionFieldsPredicate() {

		return new OnlyCollectionPresentPredicate(
				OnlyCollectionPresentPredicate.INCLUDE_COLLECTIONS);

	}

	private Predicate getExcludeCollectionFieldsPredicate() {

		return new OnlyCollectionPresentPredicate(
				OnlyCollectionPresentPredicate.EXCLUDE_COLLECTIONS);

	}

	/**
	 * Predicate class to exclude or include collections fields
	 * 
	 * @author ottoabreu
	 * 
	 */
	private class OnlyCollectionPresentPredicate implements Predicate {

		public static final boolean INCLUDE_COLLECTIONS = true;
		public static final boolean EXCLUDE_COLLECTIONS = false;
		private boolean includeCollections;

		public OnlyCollectionPresentPredicate(boolean includeColections) {

			this.includeCollections = includeColections;
		}

		@Override
		public boolean evaluate(Object fieldObject) {
			Field actualField = (Field) fieldObject;

			List<Class<?>> interfaces = Arrays.asList(actualField.getType()
					.getInterfaces());

			return this.includeOnlyCollections(interfaces);

		}

		private boolean includeOnlyCollections(List<Class<?>> interfaces) {

			boolean includeField = true;
			if (this.includeCollections
					&& !interfaces.contains(Collection.class)) {
				includeField = false;
			} else if (!this.includeCollections
					&& interfaces.contains(Collection.class)) {
				includeField = false;
			}
			return includeField;
		}

	}

	protected void handlleObjectCreationException(Throwable e)
			throws BeanFactoryException {
		if (e instanceof InstantiationException || e instanceof IllegalAccessException ) {
			logger.error(BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
			throw new BeanInstantiationException(
					BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
		} else {
			logger.error(BeanFactoryException.GENERAL_ERROR, e);
			throw new BeanFactoryException(BeanFactoryException.GENERAL_ERROR,
					e);
		}
	}

	protected void handlleReverseObjectCreationException(Throwable e)
			throws BeanFactoryException {
		if (e instanceof InstantiationException || e instanceof  IllegalAccessException  ||  e instanceof  IllegalArgumentException || e instanceof NoSuchMethodException || e instanceof InvocationTargetException) {
			logger.error(BeanInstantiationException.INSTANTIATE_ENTITY_ERROR, e);
			throw new BeanInstantiationException(
					BeanInstantiationException.INSTANTIATE_ENTITY_ERROR, e);
		} else if (e instanceof BeanInstantiationException) {
			throw (BeanInstantiationException) e;
		} else if (e instanceof BeanFactoryException) {
			throw (BeanFactoryException) e;
		} else if (e instanceof FactoryFillingException) {
			throw (FactoryFillingException) e;

		} else {
			logger.error(BeanFactoryException.GENERAL_ERROR, e);
			throw new BeanFactoryException(BeanFactoryException.GENERAL_ERROR,
					e);
		}
	}

	protected static boolean fieldIsACollectionOfOtherBean(
			BeanToBeanMapping annotation) {
		if (annotation != null
				&& (!annotation.generateListOf().equals(Object.class))) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldIsOneToOneMapping(BeanToBeanMapping annotation) {
		if (annotation != null
				&& (StringUtils.isEmpty(annotation.getSecondValueFrom()) && StringUtils
						.isNotEmpty(annotation.getValueFrom()))) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldIsOneToOtherMapping(
			BeanToBeanMapping annotation) {
		if (annotation != null
				&& (StringUtils.isEmpty(annotation.getSecondValueFrom()) && (annotation
						.generateOther()))) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldValueIsObtainedFromOtherBean(
			BeanToBeanMapping annotation) {
		if (annotation != null
				&& (StringUtils.isNotEmpty(annotation.getSecondValueFrom()) && StringUtils
						.isNotEmpty(annotation.getValueFrom()))) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldHaveAnnotationButMissingConfiguration(
			BeanToBeanMapping annotation) {

		if (annotation != null
				&& (StringUtils.isEmpty(annotation.getSecondValueFrom()) && StringUtils
						.isEmpty(annotation.getValueFrom()))) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldUseTransformer(BeanToBeanMapping annotation) {
		if (!annotation.useCustomTransformer().equals(
				CustomValueTransformer.class)) {
			return true;
		} else {
			return false;
		}
	}

	protected static boolean fieldIsPrimitive(Field field) {

		if (field.getType().isPrimitive()) {
			return true;
		} else {
			return false;
		}
	}

	protected CustomValueTransformer instanciateTransformer(
			Class<? extends CustomValueTransformer> transformerClass)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		Class<?> cls[] = new Class[] {};
		CustomValueTransformer instance = transformerClass.getConstructor(cls)
				.newInstance(new Object[] {});
		return instance;
	}

	protected void handleTransformerInstanciationException(Throwable e)
			throws BeanFactoryException {
		if (e instanceof InstantiationException || e instanceof  IllegalAccessException) {
			logger.error(BeanFactoryException.INSTANTIATE_TRANSFORMER_ERROR, e);
			throw new BeanFactoryException(
					BeanFactoryException.INSTANTIATE_TRANSFORMER_ERROR, e);
		}  else {
			logger.error(BeanFactoryException.GENERAL_ERROR, e);
			throw new BeanFactoryException(BeanFactoryException.GENERAL_ERROR,
					e);
		}
	}

	/**
	 * Generate a new instance with only the fields indicated. THis method
	 * obtains the fields from the object to be generated
	 * 
	 * @param includeExclude
	 * @return
	 * @throws BeanFactoryException
	 */
	protected T includeExcludeCollectionsFromNewInstance(
			FilterCollectionFields includeExclude) {
		logger.debug("Filter type: {0}", includeExclude);
		Field[] filteredDtoField = this.filterFields(includeExclude);
		return this.includeExcludeCollectionsFromNewInstance(includeExclude,
				filteredDtoField);

	}

	/**
	 * Generate a new instance with only the fields indicated. THis method
	 * obtains the fields from the given array
	 * 
	 * @param includeExclude
	 * @return
	 * @throws BeanFactoryException
	 */
	@SuppressWarnings("unchecked")
	protected T includeExcludeCollectionsFromNewInstance(
			FilterCollectionFields includeExclude, Field[] annotatedFields)
			throws BeanFactoryException {
		logger.debug("Filter type: {0} ", includeExclude);
		T newInstance = null;
		try {
			newInstance = (T) this.toInstatiate.newInstance();
			logger.debug("Object Created  {0} ",newInstance);

			// only use the fields that are collections
			Field[] filteredField = this.filterFields(includeExclude,
					annotatedFields);
			logger.debug("fields after the filter: {0}", filteredField.length);
			this.moveValuesFromObjectToNewInstance(filteredField, newInstance);

		} catch (Exception e) {
			this.handlleObjectCreationException(e);
		}

		return newInstance;

	}

	protected static String getFieldNameToGetValueFrom(String fieldName,
			BeanToBeanMapping annotation) {
		String methodValueFrom = "";

		if (StringUtils.isNotBlank(annotation.getValueFrom())) {
			methodValueFrom = annotation.getValueFrom();
		} else {
			methodValueFrom = fieldName;
		}
		return methodValueFrom;
	}

	/**
	 * Using the annotation information from valueFrom and secondValueFrom
	 * generate the name of the fieldname in JPA mode. ´
	 * 
	 * @param annotatedObject
	 * @param fieldName
	 * @return
	 */
	public static String generateFieldNameFromOtherBeanFromAnnotationForJPA(
			Class<?> annotatedObjectClass, String fieldName) {

		String fieldNameForJPA = "";

		try {
			
			if (StringUtils.isNotBlank(fieldName)) {
				Field field = getDeclaredField(fieldName, annotatedObjectClass);

				BeanToBeanMapping annotation = field
						.getAnnotation(BeanToBeanMapping.class);
				fieldNameForJPA = annotation.getValueFrom();
				String otherValueFrom = annotation.getSecondValueFrom();
				if (StringUtils.isBlank(fieldName)) {
					fieldNameForJPA = fieldName;
				}

				if (StringUtils.isNotBlank(otherValueFrom)) {
					fieldNameForJPA += "." + otherValueFrom;
				}
			}

		} catch (Exception e) {

			throw new BeanFactoryException(
					BeanFactoryException.GET_FIELD_NAME_JPA_ERROR, e);
		}
		return fieldNameForJPA;
	}

}
