package es.grupoavalon.aecosan.secosan.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Constants Class
 * 
 * @author otto.abreu
 *
 */
public final class SecosanConstants {
	/**
	 * PATH_REPO_CONTEXT_KEY="PATH_REPO";
	 */
	public static final String PATH_REPO_CONTEXT_KEY = "PATH_REPO";

	public static final String PATH_REPO;

	public static final String CHECKED = "X";

	public static final boolean REGISTER_ALL = true;
	public static final boolean REGISTER_SIMPLE = false;

	public static final String CONNECTOR_EXCEPTION_SNEC = "SnecConnectorException";
	public static final String CONNECTOR_EXCEPTION_SIGM = "SigmConnectorConnectionException";
	public static final String CONNECTOR_EXCEPTION_EPAGO_SEND_PAYMENT = "sendPayment";

	static {
		PATH_REPO = SecosanUtil.getStringFromContext(PATH_REPO_CONTEXT_KEY);
	}
	/**
	 * SPRING_CONTEXT_CONFIG="applicationContext.xml";
	 */
	public static final String SPRING_CONTEXT_CONFIG = "applicationContext.xml";
	/**
	 * SECURE_CONTEXT="/secure/";
	 */
	public static final String SECURE_CONTEXT = "/secure";
	/**
	 * DMIN_SECURE_CONTEXT = "/admin/secure";
	 */
	public static final String ADMIN_SECURE_CONTEXT = "/admin/secure";
	/**
	 * PROPERTY_KEY_TOKEN_MAX_VALIDATION = "token.max.validation.milisec";
	 */
	public static final String PROPERTY_KEY_TOKEN_MAX_VALIDATION = "token.max.validation.milisec";
	/**
	 * PROPERTY_KEY_TOKEN_RENEWAL_PERIOD ="token.renewal.period.milisec";
	 */
	public static final String PROPERTY_KEY_TOKEN_RENEWAL_PERIOD = "token.renewal.period.milisec";

	/**
	 * p SOLICITUDE_TYPE_QUEJA_ID ="1";
	 */
	public static final String SOLICITUDE_TYPE_QUEJA_ID = "1";
	/**
	 * SOLICITUDE_TYPE_SUGERENCIAS_ID ="2";
	 */
	public static final String SOLICITUDE_TYPE_SUGERENCIAS_ID = "2";
	/**
	 * SOLICITUDE_TYPE_RIESGOS_ID ="3";
	 */
	public static final String SOLICITUDE_TYPE_RIESGOS_ID = "3";
	/**
	 * SOLICITUDE_TYPE_SOLICITUD_FINANCIACION_USOS_MEDICOS_ID ="4";
	 */
	public static final String SOLICITUDE_TYPE_SOLICITUD_FINANCIACION_USOS_MEDICOS_ID = "4";
	/**
	 * SOLICITUDE_TYPE_PUESTA_MERCADO_COMP_ALIM_ID ="5";
	 */
	public static final String SOLICITUDE_TYPE_PUESTA_MERCADO_COMP_ALIM_ID = "5";
	/**
	 * SOLICITUDE_TYPE_PUESTA_MERCADO_ALIM_ESPECIAL_ID ="6";
	 */
	public static final String SOLICITUDE_TYPE_PUESTA_MERCADO_ALIM_ESPECIAL_ID = "6";
	/**
	 * SOLICITUDE_TYPE_LOGOS_IMG_DOCS_ID ="7";
	 */
	public static final String SOLICITUDE_TYPE_LOGOS_IMG_DOCS_ID = "7";
	/**
	 * SOLICITUDE_TYPE_PLAGUICIDAS_ID ="8";
	 */
	public static final String SOLICITUDE_TYPE_PLAGUICIDAS_ID = "8";

	public static final String USER_INTERNAL_ID = "1";

	public static final String USER_SOLICITANT_ID = "2";

	public static final String USER_NOEU_ID = "3";

	public static final String PROPERTY_KEY_FILE_INTRUCTIONS_FOLDER = "instructionsfolder";

	public static final String PROPERTY_KEY_FILE_PAYMENT_INTRUCTIONS_FOLDER = "instructionsfolder.payment";

	public static final String ID_AGREGATION_PAIS_UE = "1";

	public static final Long FORMULARIO_CESE_COMERCIALIZACION = 5L;

	public static final String TIPO_PERSONA_NATURAL = "1";
	public static final String ROL_SOLICITANTE = "1";

	public static final String FIELD_AREA = "area";
	public static final String FIELD_STATUS = "status";
	public static final String FIELD_ID = "id";
	public static final String FIELD_IDENTIFICACIONPETICION = "identificadorPeticion";
	public static final String FIELD_USER = "user";
	public static final String FIELD_NAME = "name";
	public static final String FIELD_LAST_NAME = "lastName";
	public static final String FIELD_BLOCK = "block";
	public static final String FIELD_DELETED = "deleted";
	public static final String FIELD_LOGIN = "login";
	public static final String FILTER_NOT_STATUS = "notStatus";
	public static final String FILTER_STATUS = "estado";
	public static final String FILTER_CONNECTOR_NAME = "connectorName";
	public static final String FIELD_CONNECTOR_NAME = "connectorName";
	public static final String FILTER_CONNECTOR_DATE = "date";
	public static final String FIELD_CONNECTOR_DATE = "registerDate";
	public static final String FILTER_CONNECTOR_SUCCESS = "success";
	public static final String FIELD_CONNECTOR_SUCCESS = "success";
	public static final String FILTER_CONNECTOR_DATE_FROM = "registerDateFrom";
	public static final String FILTER_CONNECTOR_DATE_TO = "registerDateTo";
	public static final String FILTER_CONNECTOR_ERRORTYPE = "errorType";
	public static final String FIELD_CONNECTOR_ERRORTYPE = "errorType";


	public static final String PROPERTY_KEY_MAX_NUMBER_OF_PRESENTATIONS = "max.number.of.presentations";

	public static final String REQUEST_HEADER_USER_ID = "userid";

	public static final String PROPERTY_KEY_SOLICITUDE_STATUS_DRAFT = "solicitude.status.draft";

	public static final String PROPERTY_KEY_REPORT_TO_SIGN_STANDARD = "report.estandardReportTosign";
	public static final String PROPERTY_KEY_REPORT_TO_SIGN_PATH = "report.reportTosignPath";
	public static final String PROPERTY_KEY_REPORT_IMAGES_PATH = "report.reportImagesPath";

	public static final String PROPERTY_KEY_STATUS_SENDED = "status.sended";
	public static final String PROPERTY_KEY_STATUS_PENDING_OF_SUBSANATION = "status.pending.of.subsanation";
	public static final String PROPERTY_KEY_STATUS_PROCESSED = "status.processed";
	public static final String PROPERTY_KEY_STATUS_DENIED = "status.denied";
	public static final String PROPERTY_KEY_STATUS_SUBSANADA = "status.subsanada";
	public static final String PROPERTY_KEY_STATUS_RESOLUCION = "status.resolucion";
	public static final String PROPERTY_KEY_STATUS_STATUS_SEND_TO_ANOTHER_ENTITY = "status.send.to.another.entity";
	public static final String PROPERTY_KEY_STATUS_PENDING_FOR_DOCUMENT_SUBSANATION = "status.pending.for.document.subsanation";

	public static final String PROPERTY_KEY_DOCUMENT_NAME_SIGM = "document.name.sigm";
	public static final String PROPERTY_KEY_DOCUMENT_TYPE_SIGM = "document.type.sigm";

	public static final String PROPERTY_KEY_DOCUMENT_TYPE_COPIA_AUTENTICA = "document.type.copia.autentica";
	public static final String PROPERTY_KEY_DOCUMENT_TYPE_PORTAFIRMA = "document.type.portafirma";

	public static final String PROPERTY_KEY_LOGIN_MAX_FAILED_ATTEMPTS = "login.max.failed.attempts";

	public static final String USER_ROLE_SOLICITANT = "1";

	public static final String USER_ROLE_TRAMITADOR = "2";

	public static final String USER_ROLE_ADMIN = "3";

	public static final String PROPERTY_KEY_ADMIN_PERMISSION_ID = "admin.permission.id";
	public static final String PROPERTY_KEY_GENERAL_REPORT_LOCAL_NAME = "general.report.local.name";

	public static final String PROPERTY_KEY_COOKIE_PATH = "cookie.path.clientcert";
	public static final String PROPERTY_KEY_COOKIE_ADMIN_PATH = "cookie.path.clientcert.admin";

	public static final String FILE_TEMPLATE_FOLDER = "templates/";
	public static final String TEMPLATE_WRAPPER = FILE_TEMPLATE_FOLDER + "emailWrapper.html";
	public static final String WRAPPER_TOKEN_CONTENT = "#content#";
	public static final String EMAIL_TOKEN_NO_INFO = "N/A";

	public static final String EMAIL_TOKEN_SUBJECT_SOLICITUDE_NOTIFICATION_CHANGE_STATUS = "email.subject.solicitude.notification.change.status";
	public static final String TEMPLATE_SOLICITUDE_NOTIFICATION_CHANGE_STATUS = FILE_TEMPLATE_FOLDER + "solicitudeNotificationChangeStatus.html";

	public static final String EMAIL_TOKEN_SOLICITUDE_REQUEST_ID = "#idRequest#";
	public static final String EMAIL_TOKEN_SOLICITUDE_DATE = "#date#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_NAME = "#userName#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_LASTNAME = "#userLastName#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_ID = "#userIdentificationNumber#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_ADDRESS = "#userAddress#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_PHONE = "#userPhone#";
	public static final String EMAIL_TOKEN_SOLICITUDE_USER_EMAIL = "#userEmail#";

	public static final String PROPERTY_KEY_EMAIL_TO_RGSEAA = "email.to.rgseaa";
	public static final String PROPERTY_KEY_EMAIL_TO_EVALUACION_RIESGOS = "email.to.evaluacion.riesgos";
	public static final String PROPERTY_KEY_EMAIL_TO_COMUNICACION = "email.to.comunicacion";
	public static final String PROPERTY_KEY_EMAIL_TO_RIESGOS_NUTRICIONALES = "email.to.riesgos.nutricionales";
	public static final String PROPERTY_KEY_EMAIL_TO_RIESGOS_QUIMICOS = "email.to.riesgos.quimicos";
	public static final String PROPERTY_KEY_EMAIL_TO_SECRETARIA_GENERAL = "email.to.secretaria.general";

	public static final String PROPERTY_KEY_CREDITCARD_TOTAL_DIGITS_MASK = "creditcard.total.digits.mask";

	public static final String PROPERTY_KEY_SNEC_SUBJECT = "snec.subject";
	public static final String PROPERTY_KEY_SNEC_URL = "snec.url";
	public static final String PROPERTY_KEY_SNEC_USER_NAME = "snec.user.name";
	public static final String PROPERTY_KEY_SNEC_SIA_FORM = "snec.sia.form.";

	public static final String PROPERTY_KEY_SIGM_URL = "sigm.url";
	public static final String PROPERTY_KEY_SIGM_USER_NAME = "sigm.user.name";
	public static final String PROPERTY_KEY_SIGM_PASSWRD = "sigm.password";
	public static final String PROPERTY_KEY_SIGM_OFFICE = "sigm.office";
	public static final String PROPERTY_KEY_SIGM_DESTINY = "sigm.destiny";

	public static final String PROPERTY_KEY_PASARELA_EPAGOS_URL = "pasarela.epagos.url";
	public static final String PROPERTY_KEY_PASARELA_EPAGOS_AECOSAN_ID = "pasarela.epagos.organismo";

	public static final String PROPERTY_KEY_APODERAMIENTO_URL = "apoderamiento.url";
	public static final String PROPERTY_KEY_APODERAMIENTO_AECOSAN_ID = "apoderamiento.organismo";

	public static final String DATE_FORMAT_HYPHEN = "dd-MM-yyyy";
	public static final DateFormat dateFormatHyphen = new SimpleDateFormat(SecosanConstants.DATE_FORMAT_HYPHEN);

	public static final String CHARACTER_MASK = "*";

	public static final long PAYMENT_TYPE_ONLINE_PAYMENT_CREDITCARD_ID = 3;
	public static final long PAYMENT_TYPE_ONLINE_PAYMENT_ACCOUNT_ID = 4;

	public static final String COMUNICATION_TYPE_SUSPENSION = "1";

	public static final String PROPERTY_KEY_ATTACHABLES_FILE_FOLDER = "attachableFolder";

	public static final String TEMPLATE_MODELO_791 = "templateModelo791";

	public static final String PROPERTY_KEY_AECOSAN_NAME = "report.aecosanName";

	public static final String RIESGOS_NUTRICIONALES_FORMS = "4";

	public static final String PROPERTY_KEY_ADMIN_CLAVE_URL = "clave.response.url.admin";

	public static final String PROPERTY_KEY_MONITORING_OK_LABEL = "monitoringGraph.ok.label";
	public static final String PROPERTY_KEY_MONITORING_OK_COLOR = "monitoringGraph.ok.color";
	public static final String PROPERTY_KEY_MONITORING_ERROR_LABEL = "monitoringGraph.error.label";
	public static final String PROPERTY_KEY_MONITORING_ERROR_COLOR = "monitoringGraph.error.color";

	public static final String PROPERTY_KEY_INIT_PAGE = "init.page";
	public static final String PROPERTY_KEY_INIT_ADMIN_PAGE = "init.admin.page";

	public static final String REQUEST_PARAM_X_FORWARD_ORIGINAL = "xforiginal";

	public static final String LOGIN_TYPE_USER_PW = "userpw";

	public static final String LOGIN_TYPE_USER_CLAVE = "clave";

	public static final String PROPERTY_KEY_PW_REGEX = "password.regex";

	public static final String PROPERTY_KEY_AUTOUNBLOCK_TIME = "autounblock.externaluser.timemilisec";

	public static final String PROPERTY_KEY_EXTRA_TRUSTED_SERVER_REGEX_FILTER = "headerfilter.trustedserver.extra";

	public static final String TEMPLATE_FICHA_TEC_ANEXO_II = "FT_TEMPLATE.html";
	public static final String FICHA_TEC_ANEXO_II_PDF= "FT_report.pdf";
	public static final String ANEXO_II_PDF= "anexo_II.pdf";
	public static final String ANEXO_II_TEMPLATE= "ANEXO_2_TEMPLATE.html";
	public static final String FONT= "arialunicodems.ttf";
	private SecosanConstants(){
	}
}
