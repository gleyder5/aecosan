package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.BajaOferAlimUMERepository;

@Component
public class BajaOferAlimUMERepositoryImpl extends AbstractCrudRespositoryImpl<BajaOferAlimUMEEntity, Long> implements BajaOferAlimUMERepository {

	@Override
	protected Class<BajaOferAlimUMEEntity> getClassType() {
		return BajaOferAlimUMEEntity.class;
	}

}
