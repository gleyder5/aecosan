package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

import java.util.List;

public interface HistorialSolicitudRepository  extends CrudRepository<HistorialSolicitudEntity, Long> {

    List<HistorialSolicitudEntity> getPaginatedList(PaginationParams paginationParams);



}
