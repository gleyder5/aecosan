package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.JWTTokenKeyEntity;

public interface JWTTokenKeyRepository extends CrudRepository<JWTTokenKeyEntity,String> {
	
	List<JWTTokenKeyEntity> getCurrents();
	
	List<JWTTokenKeyEntity> getCurrentsAdmin();

}
