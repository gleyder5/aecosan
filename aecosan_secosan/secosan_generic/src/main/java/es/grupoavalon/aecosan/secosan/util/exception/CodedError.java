package es.grupoavalon.aecosan.secosan.util.exception;

public interface CodedError {
	
	int getCodeError();
	String getDescription();

}
