package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.util.SecosanActivities;

public interface ActivityRegistry {

	void registryLoginBasedActivity(String login, SecosanActivities activity);

	void registryLoginClaveBasedActivity(String comentary, SecosanActivities activity);

}