package es.grupoavalon.aecosan.secosan.dao.repository.impl;


import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.BecasRepository;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class BecasRepositoryImpl extends AbstractCrudRespositoryImpl<BecasEntity, Long> implements BecasRepository {



	private static final String QUERY = "#Query: ";
	private static final String WHEREPART_USER = "userCreator.id =:user ";
	private static final String STATUS_FIELD = "status.id";
	private static final String STATUS_KEY_FIELD = "statusId";
	private static final String STATUS_VALUE_FIELD = "22";
	@Autowired
	private EstadoSolicitudDAO estadoSolicitudDAO;


	@Override
	protected Class<BecasEntity> getClassType() {
		return BecasEntity.class;
	}

	@Override
	public List<BecasEntity> findByStatusAndDate(EstadoSolicitudEntity status, LocalDate date) {
			Map<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put("estadoSolicitudId", status.getId());
		return  this.findByNamedQuery(NamedQueriesLibrary.GET_BECAS_BY_STATUS,queryParams);
	}

	@Override
	public List<BecasEntity> findByStatus(EstadoSolicitudEntity status) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("estadoSolicitudId", status.getId());
		return  this.findByNamedQuery(NamedQueriesLibrary.GET_BECAS_BY_STATUS,queryParams);
	}

	@Override
	public List<BecasEntity> getBecasSolicitantesPaginated(PaginationParams paginationParams) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		String queryWherePart = " WHERE status.id=:statusId";

		EstadoSolicitudEntity estadoSolicitudEntity = estadoSolicitudDAO.findEstadoSolicitud(22L);
		queryParams.put(STATUS_KEY_FIELD, Long.valueOf(estadoSolicitudEntity.getId()));

		logger.debug(QUERY + queryWherePart);

		final String sortField = "score";
		List<BecasEntity> resultList;
		resultList = findAllPaginatedAndSortedDESC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		return resultList;
	}


	@Override
	public int getTotalList(List<FilterParams> filters) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("statusId",22L);
		String queryName = " WHERE status.id=:statusId";
		logger.debug(QUERY + queryName);
		return getTotalRecords(queryName, queryParams);
	}


}
