package es.grupoavalon.aecosan.secosan.business.dto.report;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SignedAnexoIIReportDTO extends SignedReportDTO {
	private String denomination;

	private String referenceNumber;

	private String signer;

	private String inscriptionDate;

	private String company;

	public String getInscriptionDate() {
		return inscriptionDate;
	}

	public void setInscriptionDate(Long inscriptionDate) {
		if (inscriptionDate == null) {
			this.inscriptionDate = ". . . . . . . . . . . . . . . . . ";
		} else {
			Date reportDate = new Date(inscriptionDate);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.inscriptionDate = sdf.format(reportDate);
		}
	}

	public String getSigner() {
		return signer;
	}

	public void setSigner(String signer) {
		this.signer = signer;
	}


	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

}
