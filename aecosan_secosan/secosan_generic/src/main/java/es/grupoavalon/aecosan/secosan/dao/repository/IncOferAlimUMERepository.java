package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;

public interface IncOferAlimUMERepository extends CrudRepository<IncOferAlimUMEEntity, Long> {

}
