package es.grupoavalon.aecosan.secosan.rest.response.wrapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PaginatedListWrapper<T> {

	@XmlElement(name = "recordsTotal")
	private int total;
	@XmlElement(name = "recordsFiltered")
	private int recordsFiltered;
	@XmlElement(name = "data")
	private List<T> data;
	@XmlElement(name = "error")
	private String error;
	
	

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getData() {
		if(this.data == null){
			data = new ArrayList<T>();
		}
		return data;
	}

	public void setData(List<T> rows) {
		this.data = rows;
	}

	public int getRecordsFiltered() {
		
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	
}
