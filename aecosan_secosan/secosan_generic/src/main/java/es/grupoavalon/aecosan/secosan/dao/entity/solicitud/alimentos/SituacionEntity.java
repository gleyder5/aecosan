package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang.StringUtils;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_SITUACIONES)
public class SituacionEntity extends AbstractCatalogEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.SITUACION_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.SITUACION_SEQUENCE, sequenceName = SequenceNames.SITUACION_SEQUENCE, allocationSize = 1)
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		if (StringUtils.isNotBlank(idAsString)) {
			id = Long.valueOf(idAsString);
		} else {
			id = null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SituacionEntity other = (SituacionEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SituacionEntity [id=" + id + "]";
	}

}
