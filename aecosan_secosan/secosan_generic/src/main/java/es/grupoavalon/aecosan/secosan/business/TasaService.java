package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.TasaDTO;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface TasaService {

	TasaDTO getTasaByFormId(long identificadorFormulario);

	TasaDTO getTasaByServiceId(long serviceId);

	List<TasaDTO> getPaginatedTasaList(PaginationParams params);

	int getFilteredListRecords(List<FilterParams> filters);

	int getTotalTasaListRecords(List<FilterParams> filters);

	void updateTasa(TasaDTO tasaDto);

}