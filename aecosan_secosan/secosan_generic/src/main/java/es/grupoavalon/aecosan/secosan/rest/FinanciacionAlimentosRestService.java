package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.AlterOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.BajaOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.IncOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("financiacionAlimentosRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/financiacionAlimentos")
public class FinanciacionAlimentosRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/incOferAlimUME/{user}")
	public Response addIncOferAlimUME(@PathParam("user") String user, IncOferAlimUMEDTO incOferAlimUMEDTO) {
		logger.debug("-- Method addIncOferAlimUME POST Init--");
		Response response = addSolicitud(user, incOferAlimUMEDTO);
		logger.debug("-- Method addIncOferAlimUME POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/incOferAlimUME/{user}")
	public Response updateIncOferAlimUME(@PathParam("user") String user, IncOferAlimUMEDTO incOferAlimUME) {
		logger.debug("-- Method updateIncOferAlimUME PUT Init--");
		Response response = updateSolicitud(user, incOferAlimUME);
		logger.debug("-- Method updateIncOferAlimUME PUT End--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/alterOferAlimUME/{user}")
	public Response addAlterOferAlimUME(@PathParam("user") String user, AlterOferAlimUMEDTO alterOferAlimUMEDTO) {
		logger.debug("-- Method addAlterOferAlimUME POST Init--");
		Response response = addSolicitud(user, alterOferAlimUMEDTO);
		logger.debug("-- Method addAlterOferAlimUME POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/alterOferAlimUME/{user}")
	public Response updateAlterOferAlimUME(@PathParam("user") String user, AlterOferAlimUMEDTO alterOferAlimUME) {
		logger.debug("-- Method updateAlterOferAlimUME PUT Init--");
		Response response = updateSolicitud(user, alterOferAlimUME);
		logger.debug("-- Method updateAlterOferAlimUME PUT End--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/bajaOferAlimUME/{user}")
	public Response addBajaOferAlimUME(@PathParam("user") String user, BajaOferAlimUMEDTO bajaOferAlimUMEDTO) {
		logger.debug("-- Method addBajaOferAlimUME POST Init--");
		Response response = addSolicitud(user, bajaOferAlimUMEDTO);
		logger.debug("-- Method addBajaOferAlimUME POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/bajaOferAlimUME/{user}")
	public Response updateBajaOferAlimUME(@PathParam("user") String user, BajaOferAlimUMEDTO bajaOferAlimUME) {
		logger.debug("-- Method updateBajaOferAlimUME PUT Init--");
		Response response = updateSolicitud(user, bajaOferAlimUME);
		logger.debug("-- Method updateBajaOferAlimUME PUT End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/bajaOferAlimUME/deleteProduct/{id}")
	public Response deleteProduct(@PathParam("id") String id) {
		logger.debug("-- Method deleteProduct PUT --");
		Response response = null;
		try {
			this.bfacade.deleteSuspendedProduct(id);
			response = this.responseFactory.generateOkResponse();
		} catch (Exception e) {
			response = this.responseFactory.generateErrorResponse(e);
		}

		return response;
	}

}
