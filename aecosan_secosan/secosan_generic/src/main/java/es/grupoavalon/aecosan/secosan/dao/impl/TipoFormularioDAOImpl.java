package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.TipoFormularioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoFormularioRepository;

@Repository
public class TipoFormularioDAOImpl extends GenericDao implements TipoFormularioDAO {

	@Autowired
	private TipoFormularioRepository formularioRepository;

	@Override
	public TipoFormularioEntity findFormulario(Long pk) {

		TipoFormularioEntity tipoSolicitud = null;

		try {
			tipoSolicitud = formularioRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " findFormulario | " + pk + " ERROR: " + exception.getMessage());
		}

		return tipoSolicitud;
	}

	@Override
	public String getSignByFormId(Long pk) {

		TipoFormularioEntity form = null;
		String template = null;
		try {
			Map<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put("formId", Long.valueOf(pk));
			form = formularioRepository.findOneByNamedQuery(NamedQueriesLibrary.GET_SIGN_BY_FORMULARIO_ID, queryParams);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findAreaByFormId | " + pk + " ERROR: " + exception.getMessage());
		}
		if (form != null) {
			return form.getSingDocumentTemplate();
		}
		return template;

	}

}
