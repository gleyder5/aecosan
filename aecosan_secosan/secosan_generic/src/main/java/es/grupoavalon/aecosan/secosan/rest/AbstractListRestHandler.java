package es.grupoavalon.aecosan.secosan.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;

/**
 * 
 * @author juan.cabrerizo
 *
 * @param <T>
 *            DTO to serve
 */
public abstract class AbstractListRestHandler<T> {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * Params name and default values
	 */
	protected static final String PARAM_COLUMNORDER = "columnorder";
	protected static final String PARAM_ORDER = "order";
	protected static final String PARAM_LENGTH = "length";
	protected static final String PARAM_START = "start";
	protected static final String PARAM_USER = "user";
	protected static final String PARAM_AREA = "area";
	protected static final String DEFAULT_ORDER = "desc";
	protected static final String DEFAULT_COLUMN_ORDER = "id";
	protected static final String DEFAULT_LENTH = "5";

	private static final String FILTER_PATERM = "filter\\[\\d+\\]\\[[a-zA-z0-9]+\\]";

	/**
	 * Get paginated values
	 * 
	 * @param paginationParams
	 *            page to show filters
	 * @param otherParams
	 *            other filters
	 * @return
	 */
	abstract PaginatedListWrapper<T> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams);

	/**
	 * get getResponseFactory associated to the rest implementation
	 * 
	 * @return
	 */
	abstract AbstractResponseFactory getResponseFactory();


	/**
	 * Exposed method to call to the abstracts Method template pattern:
	 * https://en.wikipedia.org/wiki/Template_method_pattern
	 * 
	 * @param start
	 * @param length
	 * @param columnorder
	 * @param order
	 * @param filtrosRaw
	 * @param otherParams
	 * @return
	 */
	public Response getSimplePaginatedData(int start, int length, String columnorder, String order, MultivaluedMap<String, String> filtrosRaw, String... otherParams) {

		Response response = null;
		try {

			PaginationParams paginationParams = getPaginationParams(start, length, order, columnorder, filtrosRaw);

			PaginatedListWrapper<T> dataWraped = this.getSimplePaginatedData(paginationParams, otherParams);

			dataWraped.setError("");

			response = this.getResponseFactory().generateOkGenericResponse(dataWraped);

		} catch (Exception e) {
			logger.error(e.toString());
			response = this.getResponseFactory().generateErrorResponse(e);
		}
		return response;
	}

	/**
	 * Encapsulates the logic to get an Pagination Params object
	 * 
	 * @param start
	 * @param length
	 * @param order
	 * @param columnorder
	 * @param filtrosRaw
	 * @return
	 */
	private PaginationParams getPaginationParams(int start, int length, String order, String columnorder, MultivaluedMap<String, String> filtrosRaw) {
		PaginationParams paginationParams = new PaginationParams();
		paginationParams.setFilters(generateFilters(filtrosRaw));
		paginationParams.setInterval(start, length);
		paginationParams.setOrder(order);
		paginationParams.setSortField(columnorder);

		return paginationParams;
	}

	/**
	 * Generates a List with the filters according with filter members of the
	 * URI
	 * 
	 * @param filtersRawParameters
	 * @return
	 */
	private List<FilterParams> generateFilters(MultivaluedMap<String, String> filtersRawParameters) {
		List<FilterParams> filters = new ArrayList<FilterParams>();
		if (filtersRawParameters != null && filtersRawParameters.size() > 0) {
			for (Map.Entry<String,List<String>> key : filtersRawParameters.entrySet()) {
				if (key.getKey().matches(FILTER_PATERM)) {
					FilterParams fp = new FilterParams();
					final String k = removeBrackets(key.getKey());
					fp.setFilterName(k);
					final String v = filtersRawParameters.get(key.getKey()).get(0);
					fp.setFilterValue(v);
					filters.add(fp);
				}
			}
		}

		return filters;
	}

	/**
	 * Removes the unnecessary brackets received in the URI
	 * 
	 * @param original
	 * @return
	 */
	private String removeBrackets(String original) {
		final String result =original.substring(original.lastIndexOf('[') + 1, original.lastIndexOf(']'));
		return result;
	}
}
