package es.grupoavalon.aecosan.secosan.business.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.LogService;
import es.grupoavalon.aecosan.secosan.business.dto.LogDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Service
@SuppressWarnings("unchecked")
public class LogServiceImpl extends AbstractManager implements LogService {

	@Override
	public Collection<String> getLogsName() {

		Collection<String> fileNames = new ArrayList<String>();
		try {
			Collection<File> files = FileUtils.listFiles(getLogsDirectory(), getLogFileFilter(), TrueFileFilter.TRUE);
			fileNames = getFileNames(files);
		} catch (Exception e) {
			this.handleException(e, "Can not obtain logfiles due an error");
		}
		return fileNames;
	}


	private static IOFileFilter getLogFileFilter() {
		return new WildcardFileFilter("*.log*");
	}

	private static Collection<String> getFileNames(Collection<File> files) {
		List<String> fileNames = new ArrayList<String>();
		for (File file : files) {
			String fileName = file.getName();
			fileNames.add(fileName);
		}

		return fileNames;
	}

	private File getLogsDirectory() {
		return new File(SecosanConstants.PATH_REPO);
	}

	@Override
	public LogDTO getLogFile(String fileName) {
		LogDTO log = null;
		try {
			InputStream logFile = new FileInputStream(SecosanConstants.PATH_REPO + "/logs/" + fileName);
			log = instanciateLogDTO(fileName, logFile);

		} catch (Exception e) {
			this.handleException(e, "Can not obtain log file " + fileName + " due an error ");
		}
		return log;
	}

	private LogDTO instanciateLogDTO(String fileName, InputStream logFile) throws IOException {
		LogDTO logDto = new LogDTO();
		logDto.setName(fileName);
		logDto.setLogBase64(getBase64FromInputStream(logFile));
		return logDto;
	}

	private static String getBase64FromInputStream(InputStream logFile) throws IOException {
		String base64;
		if (logFile != null) {
			byte[] logBytes = IOUtils.toByteArray(logFile);
			base64 = Base64.encodeBase64String(logBytes);
		} else {
			throw new ServiceException("Can not obtain log because inputstream was null");
		}

		return base64;
	}

}
