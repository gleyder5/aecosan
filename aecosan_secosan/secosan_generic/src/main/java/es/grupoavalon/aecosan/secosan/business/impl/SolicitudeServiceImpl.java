package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.dao.*;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.*;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.connector.snec.SnecConnectorApi;
import es.grupoavalon.aecosan.connector.snec.dto.AttachmentSnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecNotificationDTO;
import es.grupoavalon.aecosan.connector.snec.dto.SnecResponseDTO;
import es.grupoavalon.aecosan.connector.snec.exception.SnecConnectorException;
import es.grupoavalon.aecosan.connector.snec.impl.SnecConnectorApiImpl;
import es.grupoavalon.aecosan.secosan.business.SolicitudeService;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;
import es.grupoavalon.aecosan.secosan.business.dto.factory.SolicitudeDTOForGetFactory;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudPaymentDataDTO;
import es.grupoavalon.aecosan.secosan.business.exception.DeleteException;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.business.mail.SolicitudeNotificationService;
import es.grupoavalon.aecosan.secosan.business.mail.dto.SolicitudeNotificationDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.DatosPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.AbstractSolicitudesQuejaSugerencia;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;
import es.grupoavalon.aecosan.secosan.util.exception.MailException;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class SolicitudeServiceImpl extends AbstractSolicitudService implements SolicitudeService {

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private BecasDAO becasDAO;

	@Autowired
	private IdiomasBecasDAO idiomasBecasDAO;

	@Autowired
	private EstadoSolicitudDAO estadoSolicitudDAO;

	@Autowired
	private SolicitudeNotificationService solicitudeNotificationService;

	@Autowired
	private PaymentDAO paymentDao;

	@Autowired
	private ConnectorMonitoringService monitoringService;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Autowired
	private DocumentacionDAO documentacionDAO;

	@Autowired
	private TipoDocumentosDAO tipoDocumentosDAO;

	private static final String STATUS_SENDED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_SENDED);
	private static final String STATUS_PENDING_OF_SUBSANATION = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PENDING_OF_SUBSANATION);
	private static final String STATUS_PENDING_FOR_DOCUMENT_SUBSANATION = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PENDING_FOR_DOCUMENT_SUBSANATION);
	private static final String STATUS_PROCESSED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PROCESSED);
	private static final String STATUS_DENIED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_DENIED);
	private static final String STATUS_SEND_TO_ANOTHER_ENTITY= Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_STATUS_SEND_TO_ANOTHER_ENTITY);

	private static final Long TIPO_DOCUMENTACION_PORTAFIRMA= Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_DOCUMENT_TYPE_PORTAFIRMA);
	private static final Long TIPO_DOCUMENTACION_COPIA_AUTENTICA= Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_DOCUMENT_TYPE_COPIA_AUTENTICA);


	@Override
	@Transactional
	public SolicitudDTO findSolicitudeById(String user, String solicitudeId) {
		SolicitudDTO solicitudDTO = null;
		SolicitudEntity solicitudEntity = null;
		try {
			Long solicitudId = Long.valueOf(solicitudeId);
			solicitudEntity = solicitudDAO.findSolicitudById(solicitudId);
			solicitudDTO = SolicitudeDTOForGetFactory.instanciateDTOFromEntity(solicitudEntity);
			setPaymentData(solicitudDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " findSolicitudeById | id: " + solicitudeId);
		}
		return solicitudDTO;
	}

	@SuppressWarnings("unchecked")
	private void setPaymentData(SolicitudDTO solicitudDTO) {
		PagoSolicitudEntity paymentSolicitude = this.paymentDao.getPagoSolicitudEntityByPetitionNumber(solicitudDTO.getIdentificadorPeticion());
		if (paymentSolicitude != null && paymentSolicitude.getDataPayment() != null) {
			DtoEntityFactory<DatosPagoEntity, SolicitudPaymentDataDTO> dtoDocumentationfactory = DtoEntityFactory.getInstance(DatosPagoEntity.class, SolicitudPaymentDataDTO.class);
			SolicitudPaymentDataDTO paymentData = dtoDocumentationfactory.generateDTOFromEntity(paymentSolicitude.getDataPayment());
			solicitudDTO.setPaymentData(paymentData);
		}
	}

	@Override
	public void deleteSolicitudeById(String user, String solicitudeId) {
		try {
			checkSolicitudeStatusDeletable(solicitudeId);

			solicitudDAO.deleteSolicitudById(solicitudeId);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " deleteSolicitudeById | id: " + solicitudeId);
		}
	}

	private void checkSolicitudeStatusDeletable(String solicitudeId) {
		Long idSolicitude = Long.valueOf(solicitudeId);
		EstadoSolicitudEntity status = estadoSolicitudDAO.getStatusBySolicitudeId(idSolicitude);
		if (!isStatusDeletable(status)) {
			throw new DeleteException(DeleteException.STATUS_MUST_BE_DRAFT);
		}
	}

	private boolean isStatusDeletable(EstadoSolicitudEntity status) {
		Long solicitudeStatusDraft = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SOLICITUDE_STATUS_DRAFT);
		return status.getId().equals(solicitudeStatusDraft);
	}

	@Override
	public void solicitudeUpdateStatusBySolicitudeId(String userId, ChangeStatusDTO statusDTO) {
		SolicitudEntity solicitudEntity = null;
		try {
			if(statusDTO.getArea().equals("8")){
				solicitudEntity = changeStatusAndUpdateBecas( statusDTO.getSolicitudeId(), statusDTO.getStatusId(),statusDTO);

			}else {
				solicitudEntity = changeStatus(statusDTO.getSolicitudeId(), statusDTO.getStatusId(),statusDTO);
			}
			processStatus(statusDTO, solicitudEntity);

			historialSolicitudDAO.addHistorialSolicitud(solicitudEntity,solicitudEntity.getStatus(),statusDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " solicitudeUpdateStatusBySolicitudeId | userId: " + userId + " | statusDTO:" + statusDTO);
		}
	}
	private SolicitudEntity changeStatusAndUpdateBecas(String solicitudeId, String statusId,ChangeStatusDTO changeStatusDTO) {
		Long solicitudId = Long.valueOf(solicitudeId);
		BecasEntity becasEntity = becasDAO.findBecasById(solicitudId);

		double score = 0 ;
		if(changeStatusDTO.getPuntosExpediente()!=null) {
			becasEntity.setPuntosExpediente(changeStatusDTO.getPuntosExpediente());
			score+=becasEntity.getPuntosExpediente();
		}
		if(changeStatusDTO.getOtraTitulacion()!=null) {
			becasEntity.setOtraTitulacion(changeStatusDTO.getOtraTitulacion());
		}
		if(changeStatusDTO.getOtraTitulacionPuntos()!=null){
			becasEntity.setOtraTitulacionPuntos(changeStatusDTO.getOtraTitulacionPuntos());
			score+=becasEntity.getOtraTitulacionPuntos();
		}
		if(changeStatusDTO.getPuntosOfimatica()!=null){
			becasEntity.setPuntosOfimatica(changeStatusDTO.getPuntosOfimatica());
			score+=becasEntity.getPuntosOfimatica();
		}
		if(changeStatusDTO.getPuntosVoluntariado()!=null){
			becasEntity.setPuntosVoluntariado(changeStatusDTO.getPuntosVoluntariado());
			score+=becasEntity.getPuntosVoluntariado();
		}
		if(changeStatusDTO.getPuntosEntrevista()!=null){
			becasEntity.setPuntosEntrevista(changeStatusDTO.getPuntosEntrevista());
			score+=becasEntity.getPuntosEntrevista();
		}
//		idiomasBecasDAO.deleteIdiomasBecasByBeca(becasEntity);
		if(changeStatusDTO.getIdiomas()!=null && changeStatusDTO.getIdiomas().size()>0){

			for(IdiomasBecasDTO idiomasBecasDTO : changeStatusDTO.getIdiomas()){
				IdiomasBecasEntity idiomasBecasEntity = transformDTOToEntity(idiomasBecasDTO,IdiomasBecasEntity.class);
				score+=idiomasBecasEntity.getPuntos();

			}
		}
		becasEntity.setScore(score);
		becasDAO.updateBecasEntity(becasEntity);
		return becasEntity;
	}

	private DocumentacionEntity addDocumentToSolicitudeToSendOnEmailWhenStatusChanged(SolicitudEntity solicitudEntity,DocumentacionDTO documentacionDTO,boolean save){
		documentacionDTO.setSendAsAttachment(true);
		DocumentacionEntity  documentacionEntity = transformDTOToEntity(documentacionDTO,DocumentacionEntity.class);
		Base64TobytesArrayTransformer base64 = new Base64TobytesArrayTransformer();
		documentacionEntity.setSendAsAttachment(true);
		documentacionEntity.setDocument((byte[]) base64.reverseTransform(documentacionDTO.getBase64(),byte[].class));
		if(save) {
			return documentacionDAO.addDocumentacionToSolicitude(solicitudEntity, documentacionEntity);
		}
		return documentacionEntity;
	}
	private SolicitudEntity changeStatus(String solicitudeId, String statusId, ChangeStatusDTO statusDTO) {
		Long solicitudId = Long.valueOf(solicitudeId);
		SolicitudEntity solicitudEntity = solicitudDAO.findSolicitudById(solicitudId);
		solicitudEntity.setStatus(generateEstadoEntityByEstadoId(statusId));
		if(StringUtils.isNotBlank(statusDTO.getEmailRemision())){
			//añadir documento de gestion de estado de solicitud
			DocumentacionDTO doc = new DocumentacionDTO();
			doc.setDocType(9L); //otros documentos
			doc.setBase64(statusDTO.getFileB64());
			doc.setFileName(statusDTO.getFileName());
			addDocumentToSolicitudeToSendOnEmailWhenStatusChanged(solicitudEntity,doc,false);
		}
		if(statusDTO.getFileCopiaAutenticaName()!=null){
			//añadir copia autentica
			DocumentacionDTO docCopiaAutentica = new DocumentacionDTO();
			docCopiaAutentica.setDocType(40L); //Tipo de documento Portafirma
			docCopiaAutentica.setBase64(statusDTO.getFileCopiaAutenticaB64());
			docCopiaAutentica.setFileName(statusDTO.getFileCopiaAutenticaName());
			DocumentacionEntity documentacionEntitySaved  = addDocumentToSolicitudeToSendOnEmailWhenStatusChanged(solicitudEntity,docCopiaAutentica,true);
			solicitudEntity.getDocumentation().add(documentacionEntitySaved);

		}
		if(statusDTO.getFileCarteraName()!=null){
			//añadir portafirma
			DocumentacionDTO docPortaFirma = new DocumentacionDTO();
			docPortaFirma.setDocType(TIPO_DOCUMENTACION_PORTAFIRMA); //Tipo de documento portafirma
			docPortaFirma.setBase64(statusDTO.getFileCarteraB64());
			docPortaFirma.setFileName(statusDTO.getFileCarteraName());
			DocumentacionEntity documentacionEntitySaved  = addDocumentToSolicitudeToSendOnEmailWhenStatusChanged(solicitudEntity,docPortaFirma,true);
			solicitudEntity.getDocumentation().add(documentacionEntitySaved);
		}
		if(statusDTO!=null		){
			if( statusDTO.getOnlySendEmail()) {
				DocumentacionDTO docToSend = new DocumentacionDTO();
				docToSend.setSendAsAttachment(true);
				docToSend.setFileName(statusDTO.getFileCopiaAutenticaName());
				docToSend.setBase64(statusDTO.getFileCopiaAutenticaB64());

				sendCopiaAutenticaToEmail(solicitudEntity, docToSend);
				return solicitudEntity;
			}
		}
		solicitudDAO.updateSolicitudById(solicitudEntity);
		return solicitudEntity;
	}

	private EstadoSolicitudEntity generateEstadoEntityByEstadoId(String statusId) {
		return estadoSolicitudDAO.findEstadoSolicitud(Long.valueOf(statusId));
	}

	private void processStatus(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {

		sendMailIfApplicable(statusDTO.getStatusId(), solicitudEntity,statusDTO.getEmailRemision());
// deshab. temp. envio a SNEC
//		sendSnecIfApplicable(statusDTO, solicitudEntity);

		sendRegistrationIfApplicable(statusDTO, solicitudEntity, true);
	}


	private void sendMailIfApplicable(String statusId, SolicitudEntity solicitudEntity,String anotherUnity) {
		try {
			if ( STATUS_SEND_TO_ANOTHER_ENTITY.equals(statusId) || STATUS_SENDED.equals(statusId)||STATUS_PENDING_FOR_DOCUMENT_SUBSANATION.equals(statusId)) {
				sentToEmail(solicitudEntity,anotherUnity);
			}
		} catch (MailException e) {
			logger.warn("Can not send email to soliciant due status change due an error:" + e.getMessage(), e);
		}
	}

	private void sendCopiaAutenticaToEmail(SolicitudEntity solicitudEntity,DocumentacionDTO documentacionDTO) {
		try {
			SolicitudeNotificationDTO solicitudeNotificationDTO = new SolicitudeNotificationDTO();
			solicitudeNotificationDTO.setTo(solicitudEntity.getSolicitante().getEmail());
			solicitudeNotificationDTO.setIdRequest(solicitudEntity.getIdentificadorPeticion());
			solicitudeNotificationDTO.setDate(solicitudEntity.getFechaCreacionString());
			solicitudeNotificationDTO.setUserName(solicitudEntity.getSolicitante().getName());
			solicitudeNotificationDTO.setUserLastName(solicitudEntity.getSolicitante().getLastName());
			solicitudeNotificationDTO.setUserIdentificationNumber(solicitudEntity.getSolicitante().getIdentificationNumber());
			solicitudeNotificationDTO.setUserAddress(solicitudEntity.getSolicitante().getAddress());
			solicitudeNotificationDTO.setUserEmail(solicitudEntity.getSolicitante().getEmail());
			solicitudeNotificationDTO.setUserPhone(String.valueOf(solicitudEntity.getSolicitante().getTelephoneNumber()));
			solicitudeNotificationDTO.setDocumentacionDTOList(new ArrayList<DocumentacionDTO>());
			solicitudeNotificationDTO.getDocumentacionDTOList().add(documentacionDTO);
			solicitudeNotificationService.notifyChangeSolicitudeStatus(solicitudeNotificationDTO);

		} catch (MailException e) {
			logger.warn("Can not send notification email due an error:" + e.getMessage(), e);
		}
	}

	private void sentToEmail(SolicitudEntity solicitudEntity,String anotherUnity) {
		try {
			SolicitudeNotificationDTO solicitudeNotificationDTO = new SolicitudeNotificationDTO();
			if(StringUtils.isNotBlank(anotherUnity)) {
				solicitudeNotificationDTO.setTo(anotherUnity);
			}else{
				solicitudeNotificationDTO.setTo(generateDestination(solicitudEntity));
			}
			solicitudeNotificationDTO.setIdRequest(solicitudEntity.getIdentificadorPeticion());
			solicitudeNotificationDTO.setDate(solicitudEntity.getFechaCreacionString());
			solicitudeNotificationDTO.setUserName(solicitudEntity.getSolicitante().getName());
			solicitudeNotificationDTO.setUserLastName(solicitudEntity.getSolicitante().getLastName());
			solicitudeNotificationDTO.setUserIdentificationNumber(solicitudEntity.getSolicitante().getIdentificationNumber());
			solicitudeNotificationDTO.setUserAddress(solicitudEntity.getSolicitante().getAddress());
			solicitudeNotificationDTO.setUserEmail(solicitudEntity.getSolicitante().getEmail());
			solicitudeNotificationDTO.setUserPhone(String.valueOf(solicitudEntity.getSolicitante().getTelephoneNumber()));
			solicitudeNotificationDTO.setDocumentacionDTOList(new ArrayList<DocumentacionDTO>());

			Iterator<DocumentacionEntity> it = solicitudEntity.getDocumentation().iterator();
			while (it.hasNext()){
				DocumentacionEntity doc = it.next();
				if(doc.isSendAsAttachment()){
					DocumentacionDTO documentacionDTO = transformEntityToDTO(doc,DocumentacionDTO.class);
					prepareDocumentation(documentacionDTO,doc);
					solicitudeNotificationDTO.getDocumentacionDTOList().add(documentacionDTO);
				}
			}
			solicitudeNotificationService.notifyChangeSolicitudeStatus(solicitudeNotificationDTO);

		} catch (MailException e) {
			logger.warn("Can not send notification email due an error:" + e.getMessage(), e);
		}
	}

	private void prepareDocumentation(DocumentacionDTO doc, DocumentacionEntity docEntity) {
		Base64TobytesArrayTransformer b64transform = new Base64TobytesArrayTransformer();
		String base64 = String.valueOf(b64transform.transform(docEntity.getDocument()));
		doc.setBase64(base64);
	}


	private String generateDestination(SolicitudEntity solicitudEntity) {
		String to = null;
		if (isSecretariaGeneral(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_SECRETARIA_GENERAL);
		} else if (isEvaluacionRiesgos(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_EVALUACION_RIESGOS);
		} else if (isComunicacion(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_COMUNICACION);
		} else if (isRiesgosNutricionales(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_RIESGOS_NUTRICIONALES);
		} else if (isRiesgosQuimicos(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_RIESGOS_QUIMICOS);
		} else if (isRGSEAA(solicitudEntity)) {
			to = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EMAIL_TO_RGSEAA);
		}
		return to;
	}

	private boolean isRGSEAA(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof ComplementosAlimenticios || solicitudEntity instanceof AlimentosGrupos;
	}

	private boolean isEvaluacionRiesgos(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof EvaluacionRiesgosEntity;
	}

	private boolean isComunicacion(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof SolicitudLogoEntity;
	}

	private boolean isRiesgosNutricionales(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof EvaluacionRiesgosEntity;
	}

	private boolean isRiesgosQuimicos(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof SolicitudLmrFitosanitariosEntity;
	}

	private boolean isSecretariaGeneral(SolicitudEntity solicitudEntity) {
		return solicitudEntity instanceof AbstractSolicitudesQuejaSugerencia;
	}

	private SnecResponseDTO sendSnecIfApplicable(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		SnecResponseDTO snecResponseDTO = null;
		try {
			if (isStatusNotificableInSnec(statusDTO.getStatusId())) {
				snecResponseDTO = sendToSNEC(statusDTO, solicitudEntity);
				monitoringService.monitoringSuccess(MonitorizableConnectors.SNEC);
			}
		} catch (SnecConnectorException e) {
			this.monitoringService.monitoringError(MonitorizableConnectors.SNEC, e);
			throw e;
		}
		return snecResponseDTO;
	}

	private boolean isStatusNotificableInSnec(String statusId) {
		boolean isNotificable = false;
		if (STATUS_PENDING_OF_SUBSANATION.equals(statusId) || STATUS_PROCESSED.equals(statusId) || STATUS_DENIED.equals(statusId)) {
			isNotificable = true;
		}
		return isNotificable;
	}

	private SnecResponseDTO sendToSNEC(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		SnecConnectorApi snecConnector = new SnecConnectorApiImpl();
		return snecConnector.sendNotification(generateSnecNotification(statusDTO, solicitudEntity));

	}

	private SnecNotificationDTO generateSnecNotification(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		String subject = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SNEC_SUBJECT) + solicitudEntity.getIdentificadorPeticion();
		String siaCode = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SNEC_SIA_FORM + solicitudEntity.getFormulario().getId());
		SnecNotificationDTO notification = new SnecNotificationDTO();

		notification.setCodigoProce(siaCode);
		notification.setUsername(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SNEC_USER_NAME));
		notification.setAsunto(subject);
		notification.setContenido(statusDTO.getStatusText());
		notification.setUrl(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SNEC_URL));
		notification.setAdjuntos(getAttachedList(statusDTO));
		setContactDataIntoNotification(solicitudEntity, notification);
		logger.debug("Sending to snec: siaCode:" + siaCode + ", subject: " + subject);
		return notification;
	}

	private void setContactDataIntoNotification(SolicitudEntity solicitudEntity, SnecNotificationDTO notification) {
		String name;
		String lastName;
		String secondLastName;
		String email;
		String dni = solicitudEntity.getSolicitante().getIdentificationNumber();
		if (solicitudEntity.getContactData() != null && StringUtils.isNotBlank(solicitudEntity.getContactData().getName())) {
			DatosContactoEntity contactData = solicitudEntity.getContactData();
			name = contactData.getName();
			lastName = contactData.getLastName();
			secondLastName = contactData.getSecondLastName();
			email = contactData.getEmail();
		} else {
			SolicitanteEntity solicitant = solicitudEntity.getSolicitante();
			name = solicitant.getName();
			lastName = solicitant.getLastName();
			secondLastName = solicitant.getSecondLastName();
			email = solicitant.getEmail();
		}
		notification.setNombre(name);
		notification.setApellido1(lastName);
		notification.setApellido2(secondLastName);
		notification.setDni(dni);
		notification.setEmail(email);
		logger.debug("Sending to snec: name:" + name + ",lastName: " + lastName + " secondLastName: " + secondLastName + "dni:" + dni + ",email:" + email);
	}

	private List<AttachmentSnecNotificationDTO> getAttachedList(ChangeStatusDTO statusDTO) {
		List<AttachmentSnecNotificationDTO> attachedList = new ArrayList<AttachmentSnecNotificationDTO>();
		AttachmentSnecNotificationDTO attachmentSnecNotificationDTO = new AttachmentSnecNotificationDTO();

		attachmentSnecNotificationDTO.setEsFirmado("N");
		attachmentSnecNotificationDTO.setNombre(statusDTO.getFileName());
		attachmentSnecNotificationDTO.setContenido(statusDTO.getFileBytes());

		attachedList.add(attachmentSnecNotificationDTO);
		return attachedList;
	}

}