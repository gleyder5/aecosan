package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PAISES)
public class PaisEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.PAISES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.PAISES_SEQUENCE, sequenceName = SequenceNames.PAISES_SEQUENCE, allocationSize = 1)
	private Long idPais;

	@Column(name = ColumnNames.INE_CODE, nullable = false)
	private String ineCode;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "AGREGACION_ID", nullable = false)
	private AgregacionPaisEntity agregation;

	public String getIneCode() {
		return ineCode;
	}

	public void setIneCode(String ineCode) {
		this.ineCode = ineCode;
	}

	public Long getIdPais() {
		return idPais;
	}

	public void setIdPais(Long idPais) {
		this.idPais = idPais;
	}

	public AgregacionPaisEntity getAgregation() {
		return agregation;
	}

	public void setAgregation(AgregacionPaisEntity agregation) {
		this.agregation = agregation;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(idPais);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		if(idAsString.equals("")){
			idPais = null;
		}else{
			idPais = Long.valueOf(idAsString);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((idPais == null) ? 0 : idPais.hashCode());
		result = prime * result + ((ineCode == null) ? 0 : ineCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaisEntity other = (PaisEntity) obj;
		if (idPais == null) {
			if (other.idPais != null)
				return false;
		} else if (!idPais.equals(other.idPais))
			return false;
		if (ineCode == null) {
			if (other.ineCode != null)
				return false;
		} else if (!ineCode.equals(other.ineCode))
			return false;
		return true;
	}

	@Override
	public String toString() {

		return "PaisEntity [idPais=" + idPais + ", ineCode=" + ineCode + "]";
	}

}
