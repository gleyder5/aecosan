package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;

public interface AlimentosGruposDAO {

	CeseComercializacionAGEntity addCeseComercializacion(CeseComercializacionAGEntity ceseComercializacion);

	void updateCeseComercializacion(CeseComercializacionAGEntity ceseComercializacionEntity);

	ModificacionDatosAGEntity addModificacionDatos(ModificacionDatosAGEntity modificacionDatosEntity);

	void updateModificacionDatos(ModificacionDatosAGEntity modificacionDatosEntity);

	PuestaMercadoAGEntity addPuestaMercado(PuestaMercadoAGEntity puestaMercadoEntity);

	void updatePuestaMercado(PuestaMercadoAGEntity puestaMercadoEntity);

}
