package es.grupoavalon.aecosan.secosan.business.mail.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.mail.SolicitudeNotificationService;
import es.grupoavalon.aecosan.secosan.business.mail.dto.SolicitudeNotificationDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.mail.MailService;

@Component
public class SolicitudeNotificationServiceImpl extends MailService implements SolicitudeNotificationService {
	private static final Logger logger = LoggerFactory.getLogger(SolicitudeNotificationServiceImpl.class);

	@Override
	public void notifyChangeSolicitudeStatus(SolicitudeNotificationDTO solicitudeNotification) {
		logger.info("-- Method notifyChangeSolicitudeStatus  Init--");
		logger.info("-- Preparing email to Send --");
		String to = solicitudeNotification.getTo();
		logger.info("-- to: {} -- ",to);
		Map<String, Object> templateData = generateNewParticipantData(solicitudeNotification);
		String subject = SecosanConstants.EMAIL_TOKEN_SUBJECT_SOLICITUDE_NOTIFICATION_CHANGE_STATUS;
		String content = loadDataToTemplate(templateData, SecosanConstants.TEMPLATE_SOLICITUDE_NOTIFICATION_CHANGE_STATUS);
		logger.debug("-- content : {} -- ",content);

		sendHtmlEmail(to, subject, content,solicitudeNotification);
		logger.info("-- Method notifyChangeSolicitudeStatus  end--");
	}

	private Map<String, Object> generateNewParticipantData(SolicitudeNotificationDTO solicitudeNotification) {
		logger.info("-- Method generateNewParticipantData  called--");
		Map<String, Object> data = new HashMap<String, Object>();
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_REQUEST_ID, solicitudeNotification.getIdRequest());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_DATE, solicitudeNotification.getDate());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_NAME, solicitudeNotification.getUserName());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_LASTNAME, solicitudeNotification.getUserLastName());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_ID, solicitudeNotification.getUserIdentificationNumber());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_ADDRESS, solicitudeNotification.getUserAddress());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_PHONE, solicitudeNotification.getUserPhone());
		data.put(SecosanConstants.EMAIL_TOKEN_SOLICITUDE_USER_EMAIL, solicitudeNotification.getUserEmail());
		logger.info("-- Method generateNewParticipantData  end--");
		return data;
		
	}

}
