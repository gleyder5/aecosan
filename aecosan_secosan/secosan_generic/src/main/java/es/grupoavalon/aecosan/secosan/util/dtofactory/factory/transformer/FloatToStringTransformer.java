package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.TransformerException;

public class FloatToStringTransformer extends CustomValueTransformer {

	@Override
	public Object transform(Object input) {
		Float output = null;
		if (input != null) {
			try {
				output = (Float) transformFromString(input, Float.class);
			} catch (Exception e) {
				handleException(e);
			}
		}

		return output;
	}

	@Override
	public Object reverseTransform(Object input, Class<?> outputType) {
		String transformedValue = null;
		if (input != null) {
			try {
				transformedValue = input.toString();
			} catch (Exception e) {
				throw new TransformerException(
						TransformerException.GENERAL_ERROR, e);
			}
		}
		return transformedValue;
	}

}
