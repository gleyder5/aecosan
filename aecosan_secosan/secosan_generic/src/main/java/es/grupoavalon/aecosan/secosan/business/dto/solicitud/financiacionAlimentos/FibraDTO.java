package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class FibraDTO {
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("fiber")
	@XmlElement(name = "fiber")
	@BeanToBeanMapping(getValueFrom = "fiber")
	private Boolean fiber;

	@JsonProperty("fiberType")
	@XmlElement(name = "fiberType")
	@BeanToBeanMapping(getValueFrom = "fiberType")
	private String fiberType;

	@JsonProperty("fiberQuantity")
	@XmlElement(name = "fiberQuantity")
	@BeanToBeanMapping(getValueFrom = "fiberQuantity")
	private String fiberQuantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getFiber() {
		return fiber;
	}

	public void setFiber(Boolean fiber) {
		this.fiber = fiber;
	}

	public String getFiberType() {
		return fiberType;
	}

	public void setFiberType(String fiberType) {
		this.fiberType = fiberType;
	}

	public String getFiberQuantity() {
		return fiberQuantity;
	}

	public void setFiberQuantity(String fiberQuantity) {
		this.fiberQuantity = fiberQuantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fiber == null) ? 0 : fiber.hashCode());
		result = prime * result + ((fiberQuantity == null) ? 0 : fiberQuantity.hashCode());
		result = prime * result + ((fiberType == null) ? 0 : fiberType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FibraDTO other = (FibraDTO) obj;
		if (fiber == null) {
			if (other.fiber != null)
				return false;
		} else if (!fiber.equals(other.fiber))
			return false;
		if (fiberQuantity == null) {
			if (other.fiberQuantity != null)
				return false;
		} else if (!fiberQuantity.equals(other.fiberQuantity))
			return false;
		if (fiberType == null) {
			if (other.fiberType != null)
				return false;
		} else if (!fiberType.equals(other.fiberType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FibraDTO [id=" + id + ", fiber=" + fiber + ", fiberType=" + fiberType + ", fiberQuantity=" + fiberQuantity + "]";
	}

}
