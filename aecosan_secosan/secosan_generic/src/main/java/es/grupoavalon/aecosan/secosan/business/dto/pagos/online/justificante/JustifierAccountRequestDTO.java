package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JustifierAccountRequestDTO {
	@JsonProperty("bankCode")
	@XmlElement(name = "bankCode")
	private String ibanBankIdentifier;

	@JsonProperty("accountNumber")
	@XmlElement(name = "accountNumber")
	private String accountNumber;

	public String getIbanBankIdentifier() {
		return ibanBankIdentifier;
	}

	public void setIbanBankIdentifier(String ibanBankIdentifier) {
		this.ibanBankIdentifier = ibanBankIdentifier;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getAccountNumberDecoded() {
		String accountNumberDecoded = "";
		if (StringUtils.isNotBlank(this.accountNumber)) {
			accountNumberDecoded = new String(Base64.decodeBase64(this.accountNumber));
		}
		return accountNumberDecoded;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((ibanBankIdentifier == null) ? 0 : ibanBankIdentifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustifierAccountRequestDTO other = (JustifierAccountRequestDTO) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null) {
				return false;
			}
		} else if (!accountNumber.equals(other.accountNumber)) {
			return false;
		}
		if (ibanBankIdentifier == null) {
			if (other.ibanBankIdentifier != null) {
				return false;
			}
		} else if (!ibanBankIdentifier.equals(other.ibanBankIdentifier)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "JustifierAccountRequestDTO [ibanBankIdentifier=" + ibanBankIdentifier + ", accountNumber=" + accountNumber + "]";
	}

}
