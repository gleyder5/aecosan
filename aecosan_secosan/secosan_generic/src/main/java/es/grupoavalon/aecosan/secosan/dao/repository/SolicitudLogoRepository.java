package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;

public interface SolicitudLogoRepository extends CrudRepository<SolicitudLogoEntity, Long> {

}
