package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;

public interface TipoSolicitudDAO {
	
	TipoSolicitudEntity findFormulario(Long pk);

}
