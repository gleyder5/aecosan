package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoProcedimientoEntity;

import java.util.List;

public interface TipoProcedimientoDAO {

	TipoProcedimientoEntity addTipoProcedimientoEntity(TipoProcedimientoEntity tipoProcedimientoEntity);
	void updateTipoProcedimientoEntity(TipoProcedimientoEntity tipoProcedimientoEntity);

	List<TipoProcedimientoEntity>	getTipoProcedimientoList();

	void deleteTipoProcedimientoEntity(Long pk);

}
