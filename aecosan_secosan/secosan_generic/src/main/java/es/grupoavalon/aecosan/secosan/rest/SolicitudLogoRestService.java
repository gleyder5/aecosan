package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudLogoDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("solicitudLogoRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/solicitudLogo")
public class SolicitudLogoRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/logo/{user}")
	public Response addSolicitudLogo(@PathParam("user") String user, SolicitudLogoDTO logoRequest) {
		logger.debug("-- Method addSolicitudLogo POST Init--");
		Response response = addSolicitud(user, logoRequest);
		logger.debug("-- Method addSolicitudLogo POST End--");
		return response;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/logo/{user}")
	public Response updateSolicitudLogo(@PathParam("user") String user, SolicitudLogoDTO logoRequest) {
		logger.debug("-- Method updateSolicitudLogo PUT Init--");
		Response response = updateSolicitud(user, logoRequest);
		logger.debug("-- Method updateSolicitudLogo PUT End--");
		return response;
	}

}
