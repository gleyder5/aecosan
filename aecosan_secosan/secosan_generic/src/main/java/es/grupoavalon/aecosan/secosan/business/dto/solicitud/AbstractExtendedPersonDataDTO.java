package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public abstract class AbstractExtendedPersonDataDTO extends AbstractPersonDataDTO {

	@JsonProperty("lastName")
	@XmlElement(name = "lastName")
	@BeanToBeanMapping(getValueFrom = "lastName")
	private String lastName;

	@JsonProperty("secondLastName")
	@XmlElement(name = "secondLastName")
	@BeanToBeanMapping(getValueFrom = "secondLastName")
	private String secondLastName;

	@JsonProperty("addressType")
	@XmlElement(name = "addressType")
	@BeanToBeanMapping(getValueFrom = "addressType")
	private String addressType;

	@JsonProperty("addressNumber")
	@XmlElement(name = "addressNumber")
	@BeanToBeanMapping(getValueFrom = "addressNumber")
	private String addressNumber;

	@JsonProperty("stair")
	@XmlElement(name = "stair")
	@BeanToBeanMapping(getValueFrom = "stair")
	private String stair;

	@JsonProperty("floor")
	@XmlElement(name = "floor")
	@BeanToBeanMapping(getValueFrom = "floor")
	private String floor;

	@JsonProperty("door")
	@XmlElement(name = "door")
	@BeanToBeanMapping(getValueFrom = "door")
	private String door;

	@JsonProperty("location")
	@XmlElement(name = "location")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "location")
	private UbicacionGeograficaDTO location;

	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getSecondLastName() {
		return secondLastName;
	}


	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}


	public String getAddressType() {
		return addressType;
	}


	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}


	public String getAddressNumber() {
		return addressNumber;
	}


	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}


	public String getStair() {
		return stair;
	}


	public void setStair(String stair) {
		this.stair = stair;
	}


	public String getFloor() {
		return floor;
	}


	public void setFloor(String floor) {
		this.floor = floor;
	}


	public String getDoor() {
		return door;
	}


	public void setDoor(String door) {
		this.door = door;
	}

	@Override
	public UbicacionGeograficaDTO getLocation() {
		return location;
	}

	@Override
	public void setLocation(UbicacionGeograficaDTO location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return super.toString() + ",lastName=" + lastName + ", secondLastName=" + secondLastName + ", addressType=" + addressType + ", addressNumber=" + addressNumber + ", stair=" + stair
				+ ", floor=" + floor + ", door=" + door;
	}

}
