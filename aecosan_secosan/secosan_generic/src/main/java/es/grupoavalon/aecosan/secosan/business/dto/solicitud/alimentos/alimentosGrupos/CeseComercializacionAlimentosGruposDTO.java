package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos;

import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.CeseComercializacionDTO;

public class CeseComercializacionAlimentosGruposDTO extends CeseComercializacionDTO implements AlimentosGrupos {

}
