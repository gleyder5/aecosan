package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoDocumentacionRepository;

@Component
public class TipoDocumentacionRepositoryImpl extends CrudCatalogRepositoryImpl<TipoDocumentacionEntity, Long>
		implements TipoDocumentacionRepository, GenericCatalogRepository<TipoDocumentacionEntity> {

	@Override
	public List<TipoDocumentacionEntity> getCatalogList() {
		return findAllOrderByValue();
	}

	@Override
	protected Class<TipoDocumentacionEntity> getClassType() {
		return TipoDocumentacionEntity.class;
	}
}
