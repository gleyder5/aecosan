package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class SolicitudLogoDTO extends SolicitudDTO {

	@JsonProperty("requestedMaterial")
	@XmlElement(name = "requestedMaterial")
	@BeanToBeanMapping(getValueFrom = "requestedMaterial")
	private String requestedMaterial;

	@JsonProperty("purposeAndUse")
	@XmlElement(name = "purposeAndUse")
	@BeanToBeanMapping(getValueFrom = "purposeAndUse")
	private String purposeAndUse;

	public String getRequestedMaterial() {
		return requestedMaterial;
	}

	public void setRequestedMaterial(String requestedMaterial) {
		this.requestedMaterial = requestedMaterial;
	}

	public String getPurposeAndUse() {
		return purposeAndUse;
	}

	public void setPurposeAndUse(String purposeAndUse) {
		this.purposeAndUse = purposeAndUse;
	}

	@Override
	public String toString() {
		return "SolicitudLogoDTO [requestedMaterial=" + requestedMaterial + ", purposeAndUse=" + purposeAndUse + "]";
	}

}
