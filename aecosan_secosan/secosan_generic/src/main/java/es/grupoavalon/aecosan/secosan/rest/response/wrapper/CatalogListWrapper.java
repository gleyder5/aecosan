/**
 * 
 */
package es.grupoavalon.aecosan.secosan.rest.response.wrapper;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import es.grupoavalon.aecosan.secosan.business.dto.CatalogDTO;

/**
 * @author otto.abreu
 *
 */
@XmlRootElement
public class CatalogListWrapper {
	
	@XmlElement(name = "catalog")
	private List<CatalogDTO> catalog;

	
	public CatalogListWrapper() {
		super();
	}

	public CatalogListWrapper(List<CatalogDTO> catalog) {
		super();
		this.catalog = catalog;
	}
	
	public CatalogListWrapper(CatalogDTO element) {
		super();
		this.addOneCatalogElement(element);
	}

	public List<CatalogDTO> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<CatalogDTO> catalog) {
		this.catalog = catalog;
	}
	
	public void addOneCatalogElement(CatalogDTO element){
		if(this.catalog == null){
			this.catalog = new ArrayList<CatalogDTO>(1);
		}
		if(element != null){
			this.catalog.add(element);
		}
		
	}

	@Override
	public String toString() {
		return "catalogTable [catalog=" + catalog + "]";
	}
	
	

}
