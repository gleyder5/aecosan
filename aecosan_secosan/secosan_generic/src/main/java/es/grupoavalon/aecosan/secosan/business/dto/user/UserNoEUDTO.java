package es.grupoavalon.aecosan.secosan.business.dto.user;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class UserNoEUDTO extends AbstractUserEncodedPassword {


	@JsonProperty("login")
	@XmlElement(name = "login")
	@BeanToBeanMapping(getValueFrom = "login")
	private String login;

	@JsonProperty("password")
	@XmlElement(name = "password")
	@BeanToBeanMapping(getValueFrom = "password")
	private String password;

	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	private String identificationNumber;



	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

}
