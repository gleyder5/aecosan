package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;

public interface ProductoSuspendidoRepository extends CrudRepository<ProductoSuspendidoEntity, Long> {

}
