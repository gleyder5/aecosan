package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.SolicitudLmrFitosanitariosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudLmrFitosanitariosRepository;

@Repository
public class SolicitudLmrFitosanitariosDAOImpl extends GenericDao implements SolicitudLmrFitosanitariosDAO {

	@Autowired
	private SolicitudLmrFitosanitariosRepository solicitudLmrFitosanitariosRepository;


	@Override
	public SolicitudLmrFitosanitariosEntity addSolicitudLmrFitosanitarios(SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitarios) {
		SolicitudLmrFitosanitariosEntity solicitudeLmrFitosanitariosEntity = null;
		try {
			solicitudeLmrFitosanitariosEntity = solicitudLmrFitosanitariosRepository.save(solicitudLmrFitosanitarios);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addSolicitudLmrFitosanitarios | solicitudLmrFitosanitarios: " + solicitudLmrFitosanitarios);
		}
		return solicitudeLmrFitosanitariosEntity;
	}

	@Override
	public SolicitudLmrFitosanitariosEntity findSolicitudLmrFitosanitariosById(Long id) {
		SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity = null;
		try {
			solicitudLmrFitosanitariosEntity = solicitudLmrFitosanitariosRepository.findOne(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findSolicitudLmrFitosanitariosById | ID: " + id);
		}

		return solicitudLmrFitosanitariosEntity;
	}
	
	@Override
	public void updateSolicitudLmrFitosanitarios(SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity) {
		try {
			solicitudLmrFitosanitariosRepository.update(solicitudLmrFitosanitariosEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateSolicitudLmrFitosanitarios | ERROR: " + solicitudLmrFitosanitariosEntity);
		}

	}

}
