package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import es.grupoavalon.aecosan.secosan.dao.entity.ProcedimientoGeneralEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.ProcedimientoGeneralRepository;
import org.springframework.stereotype.Component;

@Component
public class ProcedimientoGeneralRepositoryImpl extends AbstractCrudRespositoryImpl<ProcedimientoGeneralEntity, Long> implements ProcedimientoGeneralRepository {

	@Override
	protected Class<ProcedimientoGeneralEntity> getClassType() {
		return ProcedimientoGeneralEntity.class;
	}

}
