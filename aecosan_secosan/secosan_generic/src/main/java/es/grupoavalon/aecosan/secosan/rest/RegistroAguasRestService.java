package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroAguas.RegistroAguasDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("registroAguasRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/registroAguas")
public class RegistroAguasRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addRegistroAguas(@PathParam("user") String user, RegistroAguasDTO registroAguasDTO) {
		logger.debug("-- Method addRegistroAguas POST Init--");
		logger.debug("addRegistroAguas || registroAguasDTO" + registroAguasDTO);

		Response response = addSolicitud(user, registroAguasDTO);
		logger.debug("-- Method addRegistroAguas POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateRegistroAguas(@PathParam("user") String user, RegistroAguasDTO registroAguasDTO) {
		logger.debug("-- Method updateRegistroAguas PUT Init--");
		logger.debug("updateRegistroAguas || registroAguasDTO" + registroAguasDTO);
		Response response = updateSolicitud(user, registroAguasDTO);
		logger.debug("-- Method updateRegistroAguas PUT End--");
		return response;
	}

}
