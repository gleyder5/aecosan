package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IsPaidDTO {
	@JsonProperty("solicitudeId")
	@XmlElement(name = "solicitudeId")
	private String solicitudeId;

	@JsonProperty("isPaid")
	@XmlElement(name = "isPaid")
	private String isPaid;

	public String getSolicitudeId() {
		return solicitudeId;
	}

	public void setSolicitudeId(String solicitudeId) {
		this.solicitudeId = solicitudeId;
	}

	public String getIsPaid() {
		return isPaid;
	}

	public void setIsPaid(String isPaid) {
		this.isPaid = isPaid;
	}

	@Override
	public String toString() {
		return "IsPaidDTO [solicitudeId=" + solicitudeId + ", isPaid=" + isPaid + "]";
	}

}
