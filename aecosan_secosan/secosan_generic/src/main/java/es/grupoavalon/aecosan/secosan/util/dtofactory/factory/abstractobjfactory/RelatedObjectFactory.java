package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.abstractobjfactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.lang.StringUtils;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IsAbstractRelatedObject;

@SuppressWarnings("unchecked")
public abstract class RelatedObjectFactory {

	protected Field field;
	protected Object entity;
	protected Class<?> relatedObjectClass;
	protected IsAbstractRelatedObject annotation;
	protected Object dto;

	public abstract Object instanciateObject() throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException;

	public RelatedObjectFactory(Field field, Object entity) {
		super();
		this.field = field;
		this.entity = entity;
		relatedObjectClass = field.getType();
	}

	public RelatedObjectFactory(Field field, Object entity, Object dto,
			IsAbstractRelatedObject annotation) {
		this(field, entity);
		this.annotation = annotation;
		this.dto = dto;
	}

	public static RelatedObjectFactory instanciateFactory(
			Class<? extends RelatedObjectFactory> factoryClass, Field field,
			Object entity) throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		return newFactoryInstance(factoryClass, field, entity);
	}

	public static RelatedObjectFactory instanciateFactory(
			Class<? extends RelatedObjectFactory> factoryClass, Field field,
			Object entity, Object dto, IsAbstractRelatedObject annotation)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		return newFactoryInstance(factoryClass, field, entity, dto, annotation);
	}

	public Field getField() {
		return field;
	}

	public Object getEntity() {
		return entity;
	}

	public Class<?> getRelatedObjectClass() {
		return relatedObjectClass;
	}

	public Object getDto() {
		return dto;
	}

	private static <T> T newFactoryInstance(Class<?> classToInstanciate,
			Field field, Object entity) throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		return instanciateFromClass(classToInstanciate, new Class<?>[] {
				Field.class, Object.class }, new Object[] { field, entity });
	}

	private static <T> T newFactoryInstance(Class<?> classToInstanciate,
			Field field, Object entity, Object dto,
			IsAbstractRelatedObject annotation)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		return instanciateFromClass(classToInstanciate, new Class<?>[] {
				Field.class, Object.class, Object.class,
				IsAbstractRelatedObject.class }, new Object[] { field, entity,
				dto, annotation });
	}

	protected static <T> T newInstance(Class<?> classToInstanciate)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		return instanciateFromClass(classToInstanciate, new Class<?>[] {},
				new Object[] {});
	}

	private static <T> T instanciateFromClass(Class<?> classToInstanciate,
			Class<?>[] classArray, Object[] paramsArray)
			throws IllegalArgumentException, SecurityException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
		return (T) classToInstanciate.getConstructor(classArray).newInstance(
				paramsArray);
	}

	protected Object executeDiscriminateMethod() throws SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			IllegalAccessException, InvocationTargetException {
		String discriminateMethodName = this.annotation.discriminateMethod();
		return invokeDiscriminateMethod(discriminateMethodName);
	}

	private Object invokeDiscriminateMethod(String discriminateMethodName)
			throws IllegalArgumentException, IllegalAccessException,
			InvocationTargetException, SecurityException, NoSuchMethodException {
		Object result = null;
		if (StringUtils.isNotEmpty(discriminateMethodName)) {
			Method discriminateMethod = this.dto.getClass().getMethod(
					discriminateMethodName, new Class<?>[] {});
			result = discriminateMethod.invoke(dto, new Object[] {
			});
		}
		return result;
	}
}
