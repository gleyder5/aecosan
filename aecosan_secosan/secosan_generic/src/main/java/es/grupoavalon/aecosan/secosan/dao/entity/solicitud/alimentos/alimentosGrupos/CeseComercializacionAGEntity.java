package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos;

import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.CeseComercializacionEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_CESE_COMERCIALIZACION_AG)
public class CeseComercializacionAGEntity extends CeseComercializacionEntity implements AlimentosGrupos {


}
