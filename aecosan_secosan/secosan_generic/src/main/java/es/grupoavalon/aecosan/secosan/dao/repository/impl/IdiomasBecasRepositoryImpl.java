package es.grupoavalon.aecosan.secosan.dao.repository.impl;


import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.BecasRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.IdiomasBecasRepository;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class IdiomasBecasRepositoryImpl extends AbstractCrudRespositoryImpl<IdiomasBecasEntity, Long> implements IdiomasBecasRepository {

	@Override
	protected Class<IdiomasBecasEntity> getClassType() {
		return IdiomasBecasEntity.class;
	}

	@Override
	public List<IdiomasBecasEntity> findByBeca(BecasEntity becasEntity) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("becaId", becasEntity.getId());
		return  findByNamedQuery(NamedQueriesLibrary.GET_IDIOMAS_BECAS_BY_BECA,queryParams);

	}

	@Override
	public IdiomasBecasEntity findByBecaAndIdioma(BecasEntity becasEntity, String name) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("becaId", becasEntity.getId());
		queryParams.put("idioma",name);
		IdiomasBecasEntity result = findOneByNamedQuery(NamedQueriesLibrary.GET_IDIOMAS_BECAS_BY_BECA_AND_IDIOMA,queryParams);
		return  result;
	}
}
