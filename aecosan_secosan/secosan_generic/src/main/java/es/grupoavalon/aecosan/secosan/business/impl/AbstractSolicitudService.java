package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.connector.sigm.SigmConnectorApi;
import es.grupoavalon.aecosan.connector.sigm.dto.AbstractSigmRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.SigmAttachedFileRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleAttachedFileResultDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTORequest;
import es.grupoavalon.aecosan.connector.sigm.dto.attach.SigmMultipleDocAttachDTOResponse;
import es.grupoavalon.aecosan.connector.sigm.dto.register.InteresadoFisicoDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterRequestDTO;
import es.grupoavalon.aecosan.connector.sigm.dto.register.SigmCreateRegisterResponseDTO;
import es.grupoavalon.aecosan.connector.sigm.exception.SigmConnectorConnectionException;
import es.grupoavalon.aecosan.connector.sigm.impl.SigmConnectorApiImpl;
import es.grupoavalon.aecosan.connector.sigm.util.SigmConstants;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitanteEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
@Transactional
public abstract class AbstractSolicitudService extends AbstractManager {

	@Autowired
	private TipoSolicitudDAO tipoSolicitudDAO;

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private AreaDAO areaDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	@Autowired
	private PaymentDAO paymentDao;

	@Autowired
	private EstadoSolicitudDAO estadoSolicitudDAO;

	@Autowired
	private TipoDocumentosDAO tipoDocumentosDAO;

	@Autowired
	private ConnectorMonitoringService monitoringService;
	private static final Long STATUS_SUBSANADA = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_SUBSANADA);
	private static final Long STATUS_SENDED = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_SENDED);

	private static final Long STATUS_PENDING_OF_SUBSANATION = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PENDING_OF_SUBSANATION);
	private static final Long DOCUMENT_TYPE_SIGM = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_DOCUMENT_TYPE_SIGM);
	private static final String DOCUMENT_NAME_SIGM = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_DOCUMENT_NAME_SIGM);

	private static final Predicate EXISTING_DOC = new Predicate() {
		@Override
		public boolean evaluate(Object input) {
			DocumentacionEntity doc = (DocumentacionEntity) input;
			return doc.getId() != null;
		}
	};

	protected void solicitudPrepareNullDTO(SolicitudDTO solicitudDTO) {
		solicitudDTO.setRepresentante(SecosanUtil.checkDtoNullability(solicitudDTO.getRepresentante()));
		solicitudDTO.setContactData(SecosanUtil.checkDtoNullability(solicitudDTO.getContactData()));
		solicitudPrepareNullUbicacionGeograficaDTO(solicitudDTO);
	}

	protected void solicitudPrepareNullUbicacionGeograficaDTO(SolicitudDTO solicitudDTO) {
		if (solicitudDTO.getSolicitante() != null) {
			prepareUbicacionSolicitante(solicitudDTO);
		}
		if (solicitudDTO.getRepresentante() != null) {
			prepareUbicacionRepresentante(solicitudDTO);
		}
		if (solicitudDTO.getContactData() != null) {
			prepareUbicacionContactData(solicitudDTO);
		}
	}

	private void prepareUbicacionSolicitante(SolicitudDTO solicitudDTO) {
		if (solicitudDTO.getSolicitante() != null) {
			solicitudDTO.getSolicitante().setLocation(SecosanUtil.checkDtoNullability(solicitudDTO.getSolicitante().getLocation()));
			prepareLocationNull(solicitudDTO.getSolicitante().getLocation());
		}
	}

	private void prepareUbicacionRepresentante(SolicitudDTO solicitudDTO) {
		if (solicitudDTO.getRepresentante() != null) {
			solicitudDTO.getRepresentante().setLocation(SecosanUtil.checkDtoNullability(solicitudDTO.getRepresentante().getLocation()));
			prepareLocationNull(solicitudDTO.getRepresentante().getLocation());
		}
	}

	private void prepareUbicacionContactData(SolicitudDTO solicitudDTO) {
		if (solicitudDTO.getContactData() != null) {
			solicitudDTO.getContactData().setLocation(SecosanUtil.checkDtoNullability(solicitudDTO.getContactData().getLocation()));
			prepareLocationNull(solicitudDTO.getContactData().getLocation());
		}
	}

	protected void prepareLocationNull(UbicacionGeograficaDTO location) {
		if (location != null) {
			location.setCountry(SecosanUtil.checkDtoNullability(location.getCountry()));
			location.setProvince(SecosanUtil.checkDtoNullability(location.getProvince()));
			location.setMunicipio(SecosanUtil.checkDtoNullability(location.getMunicipio()));
		}
	}

	protected void prepareSolicitudDTO(SolicitudDTO solicitudDTO) {
		solicitudDTO.getSolicitante().getTipo().setId(SecosanConstants.TIPO_PERSONA_NATURAL);
		solicitudDTO.getSolicitante().getPersonType().setId(SecosanConstants.TIPO_PERSONA_NATURAL);
		solicitudDTO.getSolicitante().getRole().setId(SecosanConstants.ROL_SOLICITANTE);
	}

	protected void prepareAddSolicitudEntity(SolicitudEntity solicitudEntity, SolicitudDTO solicitudDTO) {

		Long formId = Long.valueOf(solicitudDTO.getFormulario().getId());

		AreaEntity areaEntity = areaDAO.findAreaByFormId(formId);
		solicitudEntity.setArea(areaEntity);

		Date fechaCreacion = new Date(System.currentTimeMillis());
		solicitudEntity.setFechaCreacion(fechaCreacion);
		solicitudEntity.setSolicitaCopiaAutentica(solicitudDTO.getSolicitaCopiaAutentica());
		solicitudEntity.setSolicitaCopiaAutenticaInfo(solicitudDTO.getSolicitaCopiaAutenticaInfo());

		prepareDocumentacionEntity(solicitudEntity, solicitudDTO);

		if (solicitudEntity.getStatus().getId().equals(STATUS_SENDED)) {
			sendRegistrationAccordingToStatus(solicitudEntity, true);
		}

		setPaymentIfExist(solicitudDTO, solicitudEntity);
	}

	private static ChangeStatusDTO getStatusDTOForSendRegistration(EstadoSolicitudEntity status) {
		ChangeStatusDTO statusDTO = new ChangeStatusDTO();
		statusDTO.setStatusText(status.getCatalogValue());
		return statusDTO;
	}

	protected void prepareUpdateSolicitudEntity(SolicitudEntity solicitudEntity, SolicitudDTO solicitudDTO) {
		Long formularioId = Long.valueOf(solicitudDTO.getFormulario().getId());
		solicitudEntity.setFormulario(tipoSolicitudDAO.findFormulario(formularioId));
		solicitudEntity.setArea(solicitudEntity.getFormulario().getRelatedArea());
		solicitudEntity.setSolicitaCopiaAutentica(solicitudDTO.getSolicitaCopiaAutentica());
		solicitudEntity.setSolicitaCopiaAutenticaInfo(solicitudDTO.getSolicitaCopiaAutenticaInfo());
		prepareDocumentacionEntityForUpdate(solicitudEntity, solicitudDTO);
		setRightStatusFrom(solicitudEntity, solicitudDTO);
		if (solicitudEntity.getStatus().getId().equals(STATUS_SUBSANADA) || solicitudEntity.getStatus().getId().equals(STATUS_SENDED)) {
			sendRegistrationAccordingToStatus(solicitudEntity, false);
		}
		setPaymentIfExist(solicitudDTO, solicitudEntity);
	}

	private void sendRegistrationAccordingToStatus(SolicitudEntity solicitudEntity, Boolean update) {
		ChangeStatusDTO changeStatusDto = getStatusDTOForSendRegistration(solicitudEntity.getStatus());
		sendRegistrationIfApplicable(changeStatusDto, solicitudEntity, update);
	}

	private void setRightStatusFrom(SolicitudEntity solicitudEntity, SolicitudDTO solicitudDTO) {
		EstadoSolicitudEntity status = solicitudDAO.findSolicitudById(solicitudEntity.getId()).getStatus();

		if (STATUS_PENDING_OF_SUBSANATION.equals(status.getId())) {
			solicitudEntity.setStatus(estadoSolicitudDAO.findEstadoSolicitud(STATUS_SUBSANADA));
		} else {
			solicitudEntity.setStatus(estadoSolicitudDAO.findEstadoSolicitud(solicitudDTO.getStatus()));
		}
	}

	protected void prepareDocumentacionEntity(SolicitudEntity solicitudEntity, SolicitudDTO solicitudDTO) {
		prepareDocumentacionEntityToSetInSolicitudEntity(solicitudDTO, solicitudEntity);
		setSolicitudeForDocuments(solicitudEntity);

	}

	protected void prepareDocumentacionEntityForUpdate(SolicitudEntity solicitudEntity, SolicitudDTO solicitudDTO) {
		prepareDocumentacionEntity(solicitudEntity, solicitudDTO);
		discardOldDocuments(solicitudEntity);
	}

	@SuppressWarnings("unchecked")
	private static void discardOldDocuments(SolicitudEntity solicitudEntity) {
		if (solicitudEntity.getDocumentation() != null && !solicitudEntity.getDocumentation().isEmpty()) {
			solicitudEntity.setDocumentation((List<DocumentacionEntity>) CollectionUtils.selectRejected(solicitudEntity.getDocumentation(), EXISTING_DOC));
		}
	}

	private static void setSolicitudeForDocuments(SolicitudEntity solicitudEntity) {
		if (solicitudEntity.getDocumentation() != null && !solicitudEntity.getDocumentation().isEmpty()) {
			for (DocumentacionEntity doc : solicitudEntity.getDocumentation()) {
				doc.setSolicitud(solicitudEntity);
			}
		}
	}

	private void prepareDocumentacionEntityToSetInSolicitudEntity(SolicitudDTO solicitudDTO, SolicitudEntity solicitudEntity) {
		solicitudEntity.setDocumentation(AbstractManager.transformDTOListToEntityList(solicitudDTO.getDocumentacion(), DocumentacionEntity.class, DocumentacionDTO.class));
		generateDocumentationBase64FromString(solicitudDTO, solicitudEntity);
	}

	private static void generateDocumentationBase64FromString(SolicitudDTO solicitudDTO, SolicitudEntity solicitudEntity) {
		if (solicitudDTO.getDocumentacion() != null && solicitudEntity.getDocumentation() != null && solicitudDTO.getDocumentacion().size() == solicitudEntity.getDocumentation().size()) {
			Base64TobytesArrayTransformer base64 = new Base64TobytesArrayTransformer();
			for (int i = 0; i < solicitudEntity.getDocumentation().size(); i++) {
				solicitudEntity.getDocumentation().get(i).setDocument((byte[]) base64.reverseTransform(solicitudDTO.getDocumentacion().get(i).getBase64(), byte[].class));
			}
		}
	}

	private void setPaymentIfExist(SolicitudDTO solicitudDTO, SolicitudEntity solicitudEntity) {
		PagoSolicitudEntity paymentSolicitude = this.paymentDao.getPagoSolicitudEntityByPetitionNumber(solicitudDTO.getIdentificadorPeticion());
		if (paymentSolicitude != null) {
			solicitudEntity.setPayment(paymentSolicitude);
		}
	}

	protected static void exceptionThrowerClassNotSuported(SolicitudDTO solicitudDTO) {
		String exMessage;
		if (solicitudDTO != null) {
			exMessage = "Class not supported: " + solicitudDTO.getClass();
		} else {
			exMessage = "Object solicitudesQuejaSugerenciaDTO is null";
		}
		throw new ServiceException(exMessage);
	}

	protected void setUserCreatorByUserId(String userId, SolicitudEntity solicitud) {
		UsuarioCreadorEntity userCreator = usuarioDAO.findCreatorById(Long.valueOf(userId));
		solicitud.setUserCreator(userCreator);
	}

	protected void sendRegistrationIfApplicable(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity, Boolean update) {

		SigmConnectorApi connector = new SigmConnectorApiImpl();

		if (StringUtils.isBlank(solicitudEntity.getRegistrationNumber())) {
			this.registerAndMonitorizeSendedSolicitude(connector, statusDTO, solicitudEntity, update);

		} else if (solicitudEntity.getStatus().getId().equals(STATUS_SUBSANADA)) {
			monitoringMultipleResponse(connector, solicitudEntity);
		}
	}

	private void monitoringMultipleResponse(SigmConnectorApi connector, SolicitudEntity solicitudEntity) {
		SigmMultipleDocAttachDTOResponse multipleResponse = registerDocumentation(connector, solicitudEntity, solicitudEntity.getRegistrationNumber(), SecosanConstants.REGISTER_SIMPLE);

		if(multipleResponse!=null) {
			monitoringDocumentMultipleResponse(multipleResponse);
		}else{
			logger.error("Error trying to monitor multiple response");
		}
	}

	private void registerAndMonitorizeSendedSolicitude(SigmConnectorApi connector, ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity, Boolean update) {
		SigmCreateRegisterResponseDTO response = null;
		try {
			response = registerSolicitude(connector, statusDTO, solicitudEntity);
			monitoringService.monitoringSuccess(MonitorizableConnectors.SIGM);
			this.saveSolicitudeAndRegisterDocumentation(response, connector, solicitudEntity, update);
		} catch (SigmConnectorConnectionException e) {
			monitoringService.monitoringError(MonitorizableConnectors.SIGM, e);
			throw e;
		}
	}
	
	private void saveSolicitudeAndRegisterDocumentation(SigmCreateRegisterResponseDTO response, SigmConnectorApi connector, SolicitudEntity solicitudEntity, Boolean update) {
		if (response != null) {
			saveRegisterIfApplicable(response, solicitudEntity, update);
			monitoringMultipleResponse(connector, solicitudEntity);
		}
	}
	
	private void monitoringDocumentMultipleResponse(SigmMultipleDocAttachDTOResponse multipleResponse) {
		if (!multipleResponse.isSuccess()) {
			for (SigmMultipleAttachedFileResultDTO resultDTO : multipleResponse.getProccessErrors()) {
				monitoringService.monitoringError(MonitorizableConnectors.SIGM, resultDTO.getProccesException());
			}
		} else {
			monitoringService.monitoringSuccess(MonitorizableConnectors.SIGM);
		}
	}

	private SigmCreateRegisterResponseDTO registerSolicitude(SigmConnectorApi connector, ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		SigmCreateRegisterRequestDTO request = null;
		SigmCreateRegisterResponseDTO response = null;
		try {
			request = generateNotificationToSigm(statusDTO, solicitudEntity);
			response = connector.createRegister(request);
		} catch (Exception e) {
			monitoringService.monitoringError(MonitorizableConnectors.SIGM, e);
			logger.warn("Can not register notification due an error:" + e.getMessage(), e);
		}
		return response;
	}

	private SigmMultipleDocAttachDTOResponse registerDocumentation(SigmConnectorApi connector, SolicitudEntity solicitudEntity, String registrationNumber, boolean registerAll) {
		SigmMultipleDocAttachDTOResponse multipleResponse = null;
		if (solicitudEntity.getDocumentation() != null) {
			multipleResponse = registerMultipleDocumentation(connector, solicitudEntity.getDocumentation(), registrationNumber, registerAll);
		}
		return multipleResponse;
	}

	private SigmMultipleDocAttachDTOResponse registerMultipleDocumentation(SigmConnectorApi connector, List<DocumentacionEntity> documentation, String registrationNumber, boolean registerAll) {
		SigmMultipleDocAttachDTOResponse multipleResponse = null;
		SigmMultipleDocAttachDTORequest request = generateMultiDocRequest(documentation, registrationNumber, registerAll);
		if (request != null) {
			multipleResponse = connector.attachMultipleDocument(request);
		}
		return multipleResponse;
	}

	private SigmMultipleDocAttachDTORequest generateMultiDocRequest(List<DocumentacionEntity> documentation, String registrationNumber, boolean registerAll) {
		SigmMultipleDocAttachDTORequest multipleDocAttachRequest = null;
		List<SigmAttachedFileRequestDTO> listAttachedFiles = generateListAttachedFilesFromDocEntity(documentation, registerAll);
		if (CollectionUtils.isNotEmpty(listAttachedFiles)) {
			multipleDocAttachRequest = new SigmMultipleDocAttachDTORequest();
			setAbstractSigmRequest(multipleDocAttachRequest);
			multipleDocAttachRequest.setRegisterNumber(registrationNumber);
			multipleDocAttachRequest.setDocuments(listAttachedFiles);
		}
		return multipleDocAttachRequest;
	}

	private List<SigmAttachedFileRequestDTO> generateListAttachedFilesFromDocEntity(List<DocumentacionEntity> documentation, boolean registerAll) {
		List<SigmAttachedFileRequestDTO> listAttachedFiles;
		if (registerAll) {
			listAttachedFiles = registerAllDocuments(documentation);
		} else {
			listAttachedFiles = registerNewDocuments(documentation);
		}
		return listAttachedFiles;
	}

	private List<SigmAttachedFileRequestDTO> registerAllDocuments(List<DocumentacionEntity> documentation) {
		List<SigmAttachedFileRequestDTO> listAttachedFiles = null;
		for (DocumentacionEntity docSol : documentation) {
			AddListAttachedFiles(docSol, listAttachedFiles);
		}
		return listAttachedFiles;
	}

	private void AddListAttachedFiles(DocumentacionEntity docSol, List<SigmAttachedFileRequestDTO> listAttachedFiles) {
		SigmAttachedFileRequestDTO docAttached = new SigmAttachedFileRequestDTO();
		docAttached.setName(docSol.getDocumentName());
		docAttached.setFileBytes(docSol.getDocument());
		listAttachedFiles.add(docAttached);
	}

	private SigmCreateRegisterRequestDTO generateNotificationToSigm(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		SigmCreateRegisterRequestDTO sigmCreateRegisterRequestDTO = new SigmCreateRegisterRequestDTO();
		setAbstractSigmRequest(sigmCreateRegisterRequestDTO);
		sigmCreateRegisterRequestDTO.setSolicitud(generateSolicitudeToSigm(statusDTO, solicitudEntity));
		return sigmCreateRegisterRequestDTO;
	}

	private List<SigmAttachedFileRequestDTO> registerNewDocuments(List<DocumentacionEntity> documentation) {
		List<SigmAttachedFileRequestDTO> listAttachedFiles = null;
		if (!documentation.isEmpty()) {
			listAttachedFiles = new ArrayList<SigmAttachedFileRequestDTO>();
		}
		for (DocumentacionEntity docSol : documentation) {
			if (docSol.getId() == null) {
				AddListAttachedFiles(docSol, listAttachedFiles);
			}
		}
		return listAttachedFiles;
	}

	private void setAbstractSigmRequest(AbstractSigmRequestDTO request) {
		request.setUrl(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SIGM_URL));
		request.setUsername(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SIGM_USER_NAME));
		request.setPassword(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SIGM_PASSWRD));
	}

	private es.grupoavalon.aecosan.connector.sigm.dto.register.SolicitudDTO generateSolicitudeToSigm(ChangeStatusDTO statusDTO, SolicitudEntity solicitudEntity) {
		es.grupoavalon.aecosan.connector.sigm.dto.register.SolicitudDTO solicitud = new es.grupoavalon.aecosan.connector.sigm.dto.register.SolicitudDTO();
		solicitud.setDestino(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SIGM_DESTINY));
		solicitud.setTipoRegistro(SigmConstants.TIPO_REGISTRO_ENTRADA);
		solicitud.setFechaRegistroOriginal(SecosanConstants.dateFormatHyphen.format(solicitudEntity.getFechaCreacion()));
		solicitud.setOficina(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SIGM_OFFICE));
		solicitud.setTipoTransporte(SigmConstants.TIPO_TRANSPORTE_CORREO_POSTAL);
		solicitud.setNumeroTransporte(null);
		solicitud.setRefExpediente(null);
		solicitud.setResumen(statusDTO.getStatusText());
		solicitud.setTipoAsunto(SigmConstants.TIPO_ASUNTO_GENERICO);
		solicitud.setTipoRegistro(SigmConstants.TIPO_REGISTRO_ENTRADA);
		solicitud.setInteresadosFisicos(getRightInteresadosFisicosphysicalInterested(solicitudEntity));
		return solicitud;
	}

	private List<InteresadoFisicoDTO> getRightInteresadosFisicosphysicalInterested(SolicitudEntity solicitudEntity) {
		List<InteresadoFisicoDTO> listPhysicalInterested = new ArrayList<InteresadoFisicoDTO>();
		
		InteresadoFisicoDTO physicalInterested = new InteresadoFisicoDTO();
		SolicitanteEntity solicitante = solicitudEntity.getSolicitante();
		physicalInterested.setRepresentanteFisico(null);
		physicalInterested.setRepresentanteJuridico(null);
		
		
		physicalInterested.setNombre(solicitante.getName());
		physicalInterested.setPrimerApellido(this.getLastNameOrNameIfFirstIsNullOrEmpty(solicitante));
//		physicalInterested.setSegundoApellido(this.getSecondLastNameOrNameIfFirstIsNullOrEmpty(solicitante));
		physicalInterested
				.setSegundoApellido((solicitudEntity.getSolicitante().getSecondLastName() != null) ? solicitudEntity.getSolicitante().getSecondLastName() : solicitudEntity.getSolicitante().getName());
		physicalInterested.setNumeroDocumento(solicitante.getIdentificationNumber());
		physicalInterested.setTipoDocumento(SigmConstants.TIPO_DOCUMENTO_NIF);

		listPhysicalInterested.add(physicalInterested);
		return listPhysicalInterested;
	}
	
	private String getLastNameOrNameIfFirstIsNullOrEmpty(SolicitanteEntity solicitante){
		String lastName;
		if(StringUtils.isNotEmpty(solicitante.getLastName())){
			lastName = solicitante.getLastName();
		}else{
			lastName = solicitante.getName();
		}
		return lastName;
	}
	
//	private String getSecondLastNameOrNameIfFirstIsNullOrEmpty(SolicitanteEntity solicitante){
//		String secondLastName;
//		if(StringUtils.isNotEmpty(solicitante.getSecondLastName())){
//			secondLastName = solicitante.getSecondLastName();
//		}else{
//			secondLastName = solicitante.getName();
//		}
//		return secondLastName;
//	}

	private void saveRegisterIfApplicable(SigmCreateRegisterResponseDTO response, SolicitudEntity entity, Boolean update) {
		if (response != null && StringUtils.isNotBlank(response.getNumeroRegistro())) {
			try {
				boolean found = false ;
				entity.setRegistrationNumber(response.getNumeroRegistro());
				DocumentacionEntity registerDoc = generateDocFromResponse(response, entity);
				List<DocumentacionEntity> documentacionEntityList =  entity.getDocumentation();
				for (DocumentacionEntity documentacionEntity: documentacionEntityList){
					if(documentacionEntity.getDocType().getId().equals(DOCUMENT_TYPE_SIGM)){
						found = true;
						break;
					}
				}
				if(!found) {
					documentacionEntityList.add(registerDoc);
				}
				if (entity.getId() != null && update) {
					solicitudDAO.updateSolicitudById(entity);
				}

			} catch (Exception e) {
				logger.warn("Can not updade registrationNumber in solicitude:" + e.getMessage(), e);
			}
		}

	}

	private DocumentacionEntity generateDocFromResponse(SigmCreateRegisterResponseDTO response, SolicitudEntity entity) {
		DocumentacionEntity registerDoc = new DocumentacionEntity();
		registerDoc.setDocument(response.getAcuse().getContenidoAcuse());
		TipoDocumentacionEntity docType = new TipoDocumentacionEntity();
		docType.setId(DOCUMENT_TYPE_SIGM);
		registerDoc.setDocType(docType);
		registerDoc.setDocumentName(DOCUMENT_NAME_SIGM);
		registerDoc.setSolicitud(entity);
		return registerDoc;
	}

}
