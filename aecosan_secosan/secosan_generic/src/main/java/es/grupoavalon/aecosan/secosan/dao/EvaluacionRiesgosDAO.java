package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;

public interface EvaluacionRiesgosDAO {

	EvaluacionRiesgosEntity addEvaluacionRiesgos(EvaluacionRiesgosEntity evaluacionRiesgosEntity);
	void updateEvaluacionRiesgos(EvaluacionRiesgosEntity evaluacionRiesgosEntity);

}
