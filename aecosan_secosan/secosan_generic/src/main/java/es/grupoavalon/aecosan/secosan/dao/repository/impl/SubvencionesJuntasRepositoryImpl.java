package es.grupoavalon.aecosan.secosan.dao.repository.impl;


import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesJuntasConsumoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SubvencionesAsocuaeRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.SubvencionesJuntasRepository;
import org.springframework.stereotype.Component;

@Component
public class SubvencionesJuntasRepositoryImpl extends AbstractCrudRespositoryImpl<SubvencionesJuntasConsumoEntity, Long> implements SubvencionesJuntasRepository{

	@Override
	protected Class<SubvencionesJuntasConsumoEntity> getClassType() {
		return SubvencionesJuntasConsumoEntity.class;
	}

}
