package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_AREAS)
@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_AREA_BY_FORMULARIO_ID, query = "SELECT a FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD
		+ ".AreaEntity AS a WHERE a.id in (SELECT tpe.relatedArea.id FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD + ".TipoSolicitudEntity tpe WHERE tpe.id = :formId)") })
public class AreaEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinTable(name = TableNames.TABLA_ESTADOS_AREAS, joinColumns = { @JoinColumn(name = "AREA_ID", referencedColumnName = ColumnNames.CATALOG_ID) }, inverseJoinColumns = {
			@JoinColumn(name = "ESTADO_ID", referencedColumnName = ColumnNames.CATALOG_ID) })
	private List<EstadoSolicitudEntity> statusByAreas;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "relatedArea", cascade = CascadeType.ALL)
	@JoinColumn(name = "AREA_ID")
	private List<TipoSolicitudEntity> solicitudeTypes;

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TipoSolicitudEntity> getSolicitudeTypes() {
		return solicitudeTypes;
	}

	public void setSolicitudeTypes(List<TipoSolicitudEntity> solicitudeTypes) {
		this.solicitudeTypes = solicitudeTypes;
	}

	public List<EstadoSolicitudEntity> getStatusByAreas() {
		return statusByAreas;
	}

	public void setStatusByAreas(List<EstadoSolicitudEntity> statusByAreas) {
		this.statusByAreas = statusByAreas;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AreaEntity other = (AreaEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AreaEntity [id=" + id + "]";
	}

}
