package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.IngredientesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PUESTA_MERCADO_CA)
public class PuestaMercadoCAEntity extends PuestaMercadoEntity implements ComplementosAlimenticios {

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "INGREDIENTES_ID")
	private IngredientesEntity ingredientes;

	public IngredientesEntity getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(IngredientesEntity ingredientes) {
		this.ingredientes = ingredientes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ingredientes == null) ? 0 : ingredientes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuestaMercadoCAEntity other = (PuestaMercadoCAEntity) obj;
		if (ingredientes == null) {
			if (other.ingredientes != null)
				return false;
		} else if (!ingredientes.equals(other.ingredientes))
			return false;
		return true;
	}

}
