package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;

public interface EstadoSolicitudRepository extends CrudRepository<EstadoSolicitudEntity, Long>{

	public EstadoSolicitudEntity getStatusBySolicitudeId(Long solicitudeId);

	List<EstadoSolicitudEntity> getStatusByAreaId(Long areaId);

}
