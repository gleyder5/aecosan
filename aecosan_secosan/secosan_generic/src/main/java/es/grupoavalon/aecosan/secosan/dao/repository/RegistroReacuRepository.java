package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;

public interface RegistroReacuRepository extends CrudRepository<RegistroReacuEntity, Long> {

}
