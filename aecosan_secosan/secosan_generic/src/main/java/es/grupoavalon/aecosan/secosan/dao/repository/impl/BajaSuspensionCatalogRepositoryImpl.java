package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOSuspensionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("BajaOSuspension")
public class BajaSuspensionCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<BajaOSuspensionEntity, Long> implements GenericCatalogRepository<BajaOSuspensionEntity> {

	@Override
	public List<BajaOSuspensionEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<BajaOSuspensionEntity> getClassType() {

		return BajaOSuspensionEntity.class;
	}

}
