package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Entity
@Table(name = TableNames.TABLA_ROLES_USUARIO)
@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_ROLES_INTERNAL, query = "FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD + ".UsuarioRolEntity AS e WHERE e.id !="
		+ SecosanConstants.ROL_SOLICITANTE) })
public class UsuarioRolEntity extends AbstractCatalogEntity{
	
	@Id
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UsuarioRolEntity other = (UsuarioRolEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioRolEntity [id=" + id + "]";
	}

}
