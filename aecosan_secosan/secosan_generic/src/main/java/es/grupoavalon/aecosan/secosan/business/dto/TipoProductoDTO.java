package es.grupoavalon.aecosan.secosan.business.dto;

public class TipoProductoDTO extends AbstractCatalogDto implements IsNulable<TipoProductoDTO> {

	@Override
	public TipoProductoDTO shouldBeNull() {
		if ("".equals(getId()) || getId() == null) {
			return null;
		} else {
			return this;
		}
	}

}
