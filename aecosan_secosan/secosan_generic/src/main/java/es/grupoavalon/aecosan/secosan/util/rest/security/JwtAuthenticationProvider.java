package es.grupoavalon.aecosan.secosan.util.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider extends AbstractJwtAuthenticationProvider {

	@Autowired
	private JwtUtil jwtUtil;

	@Override
	protected void authenticate(TokenAuth auth) {
		String compactJwt = auth.getJwtOriginalToken();
		if (jwtUtil.isTokenValid(compactJwt)) {
			auth.setAuthenticated(true);
		} else {
			auth.setAuthenticated(false);
		}
	}

}
