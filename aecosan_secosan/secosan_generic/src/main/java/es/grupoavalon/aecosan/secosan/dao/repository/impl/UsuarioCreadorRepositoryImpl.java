package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.UsuarioCreadorRepository;

@Component
public class UsuarioCreadorRepositoryImpl extends AbstractCrudRespositoryImpl<UsuarioCreadorEntity, Long>
		implements UsuarioCreadorRepository {

	@Override
	protected Class<UsuarioCreadorEntity> getClassType() {
		return UsuarioCreadorEntity.class;
	}


}
