package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

public class MonitoringStatisticsDTO {
	@XmlElement(name = "sistema")
	private String sistema;
	@XmlElement(name = "oks")
	private Integer oks = 0;
	@XmlElement(name = "errors")
	private Integer errors = 0;

	public String getSistema() {
		return sistema;
	}

	public void setSistema(String sistema) {
		this.sistema = sistema;
	}

	public Integer getOks() {
		return oks;
	}

	public void setOks(Integer oks) {
		this.oks = oks;
	}

	public Integer getErrors() {
		return errors;
	}

	public void setErrors(Integer errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		return "MonitoringStatisticsDTO [sistema=" + sistema + ", oks=" + oks + ", errors=" + errors + "]";
	}

	public void addOks() {
		this.oks++;
	}

	public void addError() {
		this.errors++;
	}
}
