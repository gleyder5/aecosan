package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudQuejaRepository;

@Component
public class SolicitudQuejaRepositoryImpl extends AbstractCrudRespositoryImpl<SolicitudQuejaEntity, Long> implements SolicitudQuejaRepository {

	@Override
	protected Class<SolicitudQuejaEntity> getClassType() {
		return SolicitudQuejaEntity.class;
	}
}
