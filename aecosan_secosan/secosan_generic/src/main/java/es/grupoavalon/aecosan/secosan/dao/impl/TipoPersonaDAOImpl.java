package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.TipoPersonaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoPersonaRepository;

@Repository
public class TipoPersonaDAOImpl extends GenericDao implements TipoPersonaDAO {

	@Autowired
	private TipoPersonaRepository tipoPersonaRepository;


	@Override
	public TipoPersonaEntity findTipoPersona(Long pk) {

		TipoPersonaEntity tipoPersona = null;

		try {
			tipoPersona = tipoPersonaRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findTipoPersona | " + pk + " ERROR: " + exception.getMessage());
		}

		return tipoPersona;
	}

}
