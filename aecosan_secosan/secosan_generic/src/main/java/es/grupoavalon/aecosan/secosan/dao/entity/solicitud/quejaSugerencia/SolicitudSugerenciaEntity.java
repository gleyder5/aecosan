package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Entity
@DiscriminatorValue(SecosanConstants.SOLICITUDE_TYPE_SUGERENCIAS_ID)
@Table(name = TableNames.TABLA_SOLICITUD_SUGERENCIA)
public class SolicitudSugerenciaEntity extends AbstractSolicitudesQuejaSugerencia {


}
