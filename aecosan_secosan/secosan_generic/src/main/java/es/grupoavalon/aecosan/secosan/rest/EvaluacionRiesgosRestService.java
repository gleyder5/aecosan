package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos.EvaluacionRiesgosDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("evaluacionRiesgosRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/evaluacionRiesgos")
public class EvaluacionRiesgosRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addEvaluacionRiesgos(@PathParam("user") String user, EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		logger.debug("-- Method addEvaluacionRiesgos POST Init--");
		Response response = addSolicitud(user, evaluacionRiesgosDTO);
		logger.debug("-- Method addEvaluacionRiesgos POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateEvaluacionRiesgos(@PathParam("user") String user, EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		logger.debug("-- Method updateEvaluacionRiesgos PUT Init--");
		Response response = updateSolicitud(user, evaluacionRiesgosDTO);
		logger.debug("-- Method updateEvaluacionRiesgos PUT End--");
		return response;
	}

}
