package es.grupoavalon.aecosan.secosan.business.dto.interfaces;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;

public interface ServicePersistenceAddDTO {

	public void addSolicitudDTO(String user, SolicitudDTO solicitud);

}
