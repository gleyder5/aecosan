package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesPagoEntity;

public interface FicheroInstruccionesDAO {
	
	FicheroInstruccionesEntity findInstructionsFileBySolicitudeType(Long solicitudeType);

	FicheroInstruccionesPagoEntity findInstructionsFileByForm(Long formId);

	List<FicheroInstruccionesPagoEntity> findAllPaymentInstructionsByArea(Long area);

	List<FicheroInstruccionesEntity> findAllInstructionsByArea(Long area);

	FicheroInstruccionesPagoEntity addPaymentInstructions(FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity);

	void updatePaymentInstructions(FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity);

	FicheroInstruccionesEntity addInstructions(FicheroInstruccionesEntity generateFicheroInstruccionesEntityFromDTO);

	void updateInstructions(FicheroInstruccionesEntity updateInstruccionesEntityFromDTO);

}
