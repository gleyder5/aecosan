package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class CeseComercializacionDTO extends SolicitudDTO {

	@JsonProperty("nombreComercialProducto")
	@XmlElement(name = "nombreComercialProducto")
	@BeanToBeanMapping(getValueFrom = "nombreComercialProducto")
	private String nombreComercialProducto;

	@JsonProperty("observaciones")
	@XmlElement(name = "observaciones")
	@BeanToBeanMapping(getValueFrom = "observaciones")
	private String observaciones;

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [nombreComercialProducto=" + nombreComercialProducto + ", observaciones=" + observaciones + "]";
	}

}
