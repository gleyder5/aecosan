package es.grupoavalon.aecosan.secosan.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import org.apache.log4j.PropertyConfigurator;
import org.springframework.web.context.ContextLoaderListener;

public class ContextListener extends ContextLoaderListener {

	private Properties log4JConfigProps;

	private static final String APP_NAME_FOR_LOGGING = "[APLICACION SECOSAN]";

	@Override
	public void contextInitialized(ServletContextEvent event) {
		super.contextInitialized(event);
		initLogDir(event.getServletContext());

	}

	protected void initLogDir(ServletContext servletContext) {

		loadLogPropsConfiguration(servletContext);
		if (log4JConfigProps != null) {
			PropertyConfigurator.configure(log4JConfigProps);
			servletContext.log(APP_NAME_FOR_LOGGING
					+ "log4j configurado exitosamente");
		} else {
			servletContext
					.log(APP_NAME_FOR_LOGGING
							+ "WARNING: No fue posible configurar log4j, la aplicacion se instanciar sin log");
		}

	}

	private void loadLogPropsConfiguration(ServletContext servletContext) {
		String log4jFile = servletContext.getInitParameter("log4jFile");
		String fileName= SecosanConstants.PATH_REPO + log4jFile;
		
		servletContext.log(APP_NAME_FOR_LOGGING + " log4jFile: " + fileName);

		File file = new File(fileName);
		InputStream is = null;
		try {
			is = new FileInputStream(file);
			this.log4JConfigProps = new Properties();
			this.log4JConfigProps.load(is);

		} catch (IOException e) {
			servletContext.log(APP_NAME_FOR_LOGGING
					+ "ERROR: no se puede configurar el fichero log4j", e);
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				servletContext
						.log(APP_NAME_FOR_LOGGING
								+ "WARNING: no fue posible cerrar el stream del fichero de configuracion del log");
			}
		}
	}

}
