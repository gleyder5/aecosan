package es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.PaisDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class RegistroIndustriasDTO extends SolicitudDTO {

	@JsonProperty("paisOrigen")
	@XmlElement(name = "paisOrigen")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "paisOrigen")
	private PaisDTO paisOrigen;

	@JsonProperty("rgseaa")
	@XmlElement(name = "rgseaa")
	@BeanToBeanMapping(getValueFrom = "rgseaa")
	private String rgseaa;

	@JsonProperty("tipoEnvio")
	@XmlElement(name = "tipoEnvio")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "tipoEnvio")
	private TipoEnvioDTO tipoEnvio;

	@JsonProperty("shippingData")
	@XmlElement(name = "shippingData")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "shippingData")
	private DatosEnvioDTO shippingData;

	@JsonProperty("solicitudePayment")
	@XmlElement(name = "solicitudePayment")
	@BeanToBeanMapping(getValueFrom = "solicitudePayment", generateOther = true)
	private PagoSolicitudDTO solicitudePayment;

	public PaisDTO getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(PaisDTO paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public TipoEnvioDTO getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(TipoEnvioDTO tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public DatosEnvioDTO getShippingData() {
		return shippingData;
	}

	public void setShippingData(DatosEnvioDTO shippingData) {
		this.shippingData = shippingData;
	}

	public PagoSolicitudDTO getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudDTO solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "RegistroIndustriasDTO [paisOrigen=" + paisOrigen + ", rgseaa=" + rgseaa + ", tipoEnvio=" + tipoEnvio + ", shippingData=" + shippingData + ", solicitudePayment=" + solicitudePayment
				+ "]";
	}
}
