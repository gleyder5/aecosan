package es.grupoavalon.aecosan.secosan.dao.repository;


import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;

import java.util.List;

public interface IdiomasBecasRepository extends CrudRepository<IdiomasBecasEntity, Long> {

    List<IdiomasBecasEntity> findByBeca(BecasEntity becasEntity);
    IdiomasBecasEntity findByBecaAndIdioma(BecasEntity becasEntity,String name);


}
