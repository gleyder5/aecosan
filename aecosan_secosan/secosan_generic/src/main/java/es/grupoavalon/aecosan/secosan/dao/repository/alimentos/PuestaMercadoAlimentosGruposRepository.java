package es.grupoavalon.aecosan.secosan.dao.repository.alimentos;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

public interface PuestaMercadoAlimentosGruposRepository extends CrudRepository<PuestaMercadoAGEntity, Long> {

}
