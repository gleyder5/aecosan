package es.grupoavalon.aecosan.secosan.util.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.mail.dto.SolicitudeNotificationDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.exception.MailException;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

public abstract class MailService {

	private static final Logger logger = LoggerFactory.getLogger(MailService.class);

	private static final String UTF_ENCODING = "UTF-8";

	@Autowired
	private JavaMailSender mailSender;

	protected void sendTxtEmail(String email, String subject, String content) {
		try {
			sendEmail(email, subject, content, false,null);
		} catch (Exception e) {
			throw new MailException(MailException.EXCEPTION_MESSAGE, e);
		}

	}

	protected void sendHtmlEmail(String email, String subject, String content,SolicitudeNotificationDTO solicitudeNotificationDTO) {
		try {
			sendEmail(email, subject, content, true,solicitudeNotificationDTO);
		} catch (Exception e) {
			throw new MailException(MailException.EXCEPTION_MESSAGE, e);
		}
	}

	private void sendEmail(String recipients, String subject, String content, Boolean isHtml,SolicitudeNotificationDTO solicitudeNotificationDTO) {
		try {
			logger.debug("-- Method sendEmail  called--");
			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message,true, UTF_ENCODING);
			InternetAddress to = new InternetAddress(recipients);
			helper.setTo(to);
			helper.setText(applyMessageWrapper(content), isHtml);
			helper.setSubject(getMailSubject(subject));
			if(null!=solicitudeNotificationDTO && solicitudeNotificationDTO.getDocumentacionDTOList().size()>0) {
				Base64TobytesArrayTransformer base64 = new Base64TobytesArrayTransformer();
				for (DocumentacionDTO doc: solicitudeNotificationDTO.getDocumentacionDTOList()) {
					helper.addAttachment(doc.getFileName(), new ByteArrayResource((byte[]) base64.reverseTransform(doc.getBase64(), byte[].class)));
				}

			}
			logger.info("-- Mail is going to send --");
			mailSender.send(message);
		} catch (AddressException e) {
			logger.error("AddressException : Can't sent the email:", e);
			throw new MailException(MailException.MAIL_SEND_ADDRESS_EXCEPTION, e);
		} catch (Exception e) {
			logger.error("Can't sent the email:", e);
			throw new MailException(MailException.MAIL_SEND_MESSAGING_EXCEPTION, e);
		}
		logger.debug("-- Method sendEmail  end--");
	}

	private static String getMailSubject(String subjectKey) {
		return Properties.getString(PropertiesFilesNames.SECOSAN, subjectKey);
	}

	private String applyMessageWrapper(String content) {
		String wrapper = loadTemplate(SecosanConstants.TEMPLATE_WRAPPER);
		return StringUtils.replace(wrapper, SecosanConstants.WRAPPER_TOKEN_CONTENT, content);
	}

	protected String loadTemplate(String template) {
		String content = this.loadTemplateFromPath(template);
		if (StringUtils.isEmpty(content)) {
			logger.info("searching template in classpath: {} ",template);
			content = this.loadTemplateFromClasspath(template);
			if (StringUtils.isBlank(content)) {
				throw new MailException(MailException.TEMPLATE_EMPTY_NULL_ERROR);
			}
		}
		return content;
	}

	private String loadTemplateFromPath(String template) {
		String content = "";
		String templatePath = SecosanConstants.PATH_REPO + "/" + template;
		File file = new File(templatePath);
		if (file.exists()) {
			logger.info("Mail template found in: {}", templatePath);
			content = readTemplateFileInPath(file);
		}

		return content;
	}

	private static String readTemplateFileInPath(File file) {
		String content = "";
		try {
			FileInputStream fileInputStream = new FileInputStream(file);
			content = IOUtils.toString(fileInputStream);
		} catch (Exception e) {
			throw new MailException(MailException.TEMPLATE_READ_ERROR, e);
		}
		return content;
	}

	private String loadTemplateFromClasspath(String template) {
		String content = "";
		InputStream resource = MailService.class.getClassLoader().getResourceAsStream(template);
		try {
			content = IOUtils.toString(resource);
		} catch (Exception e) {
			throw new MailException(MailException.TEMPLATE_READ_ERROR, e);
		}
		return content;
	}

	protected String loadDataToTemplate(Map<String, Object> datos, String template) {
		String content = loadTemplate(template);
		for (Map.Entry<String, Object> token : datos.entrySet()) {
			if (token.getValue() != null) {
				content = content.replaceAll(token.getKey(), StringEscapeUtils.escapeHtml((String) token.getValue()));
			} else {
				content = content.replaceAll(token.getKey(), SecosanConstants.EMAIL_TOKEN_NO_INFO);
			}
		}
		return content;
	}

	protected void handleException(Exception e) {
		if (e instanceof MailException) {
			throw (MailException) e;
		} else {
			throw new MailException(MailException.EXCEPTION_MESSAGE + e.getMessage(), e);
		}
	}

}
