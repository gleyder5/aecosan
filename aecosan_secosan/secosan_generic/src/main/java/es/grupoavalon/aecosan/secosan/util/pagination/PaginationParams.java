package es.grupoavalon.aecosan.secosan.util.pagination;

import java.util.ArrayList;
import java.util.List;

public class PaginationParams {
	private int start;
	private int length;
	private String sortField;
	private QueryOrder order;
	private List<FilterParams> filters;

	public int getStart() {
		return start;
	}

	public int getLength() {
		return length;
	}

	public String getSortField() {
		return sortField;
	}

	public void setSortField(String sortField) {
		this.sortField = sortField;
	}

	public QueryOrder getOrder() {
		if (order == null) {
			this.order = QueryOrder.ASC;
		}
		return order;
	}

	public void setOrder(String order) {
		this.order = QueryOrder.parseFromString(order);
	}
	
	public List<FilterParams> getFilters() {
		return filters;
	}

	public void setFilters(List<FilterParams> filters) {
		this.filters = filters;
	}
	
	public void setFilter(FilterParams filter) {
		if(this.filters == null || this.filters.isEmpty()){
			this.filters = new ArrayList<FilterParams>();
		}
		this.filters.add(filter);
	}

	public void setInterval(int start, int length) {
		this.start = start;
		this.length = length;
	}

	@Override
	public String toString() {
		return "PaginationParams [start=" + start + ", length=" + length
				+ ", sortField=" + sortField + ", order=" + order
				+ ", filters=" + filters + "]";
	}
	
	
}
