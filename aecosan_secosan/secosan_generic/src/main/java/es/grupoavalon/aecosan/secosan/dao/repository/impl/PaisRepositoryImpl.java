package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.PaisEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.AbstractTaggedCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericComplexCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericTaggedCatalog;
import es.grupoavalon.aecosan.secosan.dao.repository.TaggedCatalog;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("Pais")
public class PaisRepositoryImpl extends AbstractTaggedCatalogRepository<PaisEntity> implements GenericComplexCatalogRepository<PaisEntity>, GenericTaggedCatalog<PaisEntity> {

	private static final String QUERY_PAISES_EU = "FROM " + PaisEntity.class.getName() + " WHERE  agregation.id=" + SecosanConstants.ID_AGREGATION_PAIS_UE + " ORDER BY catalogValue";
	private static final String QUERY_PAISES_NO_EU = "FROM " + PaisEntity.class.getName() + " WHERE  agregation.id!=" + SecosanConstants.ID_AGREGATION_PAIS_UE + " ORDER BY catalogValue";

	@Override
	protected Class<PaisEntity> getClassType() {
		return PaisEntity.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PaisEntity> callMethod(Method method) {
		List<PaisEntity> list = new ArrayList<PaisEntity>();
		try {
			list = (List<PaisEntity>) method.invoke(this);
		} catch (Exception e) {
			throw new DaoException(DaoException.TAGGED_CATALOG_INVOKE_ERROR + e.getMessage(), e);
		}

		return list;
	}

	@TaggedCatalog(tagMethod = "paisesEU")
	public List<PaisEntity> paisesEU() {
		return findByQuery(QUERY_PAISES_EU);
	}

	@TaggedCatalog(tagMethod = "paisesNoEU")
	public List<PaisEntity> paisesNoEU() {
		return findByQuery(QUERY_PAISES_NO_EU);
	}
}
