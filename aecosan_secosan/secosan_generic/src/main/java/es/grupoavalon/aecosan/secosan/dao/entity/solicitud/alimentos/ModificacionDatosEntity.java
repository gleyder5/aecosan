package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_MODIFICACION_DATOS)
public class ModificacionDatosEntity extends SolicitudEntity {

	@Column(name = "NOMBRE_COMERCIAL")
	private String nombreComercialProducto;

	@Column(name = "RGSEAA")
	private String rgseaa;

	@Column(name = "NOMBRE_RAZON_SOCIAL")
	private String nombreRazonSocial;

	@Column(name = "NIF")
	private String nif;

	@Column(name = "NUEVO_NOMBRE_COMERCIAL")
	private String nuevoNombreComercialProducto;

	@Column(name = "MOD_COMPOSICION_CUAL_CUAN")
	private Boolean modComposicionCualCuan;

	@Column(name = "MOD_DISENO")
	private Boolean modDiseno;

	@Column(name = "MOD_AMPL_PRESENTACIONES")
	private Boolean modAmplPresentaciones;

	@Column(name = "OTRAS_MODIFICACIONES")
	private Boolean otrasModificaciones;

	@Column(name = "DESCRIPCION_MODIFICACIONES")
	private String descripcionModificaciones;


	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNuevoNombreComercialProducto() {
		return nuevoNombreComercialProducto;
	}

	public void setNuevoNombreComercialProducto(String nuevoNombreComercialProducto) {
		this.nuevoNombreComercialProducto = nuevoNombreComercialProducto;
	}

	public Boolean isModComposicionCualCuan() {
		return modComposicionCualCuan;
	}

	public void setModComposicionCualCuan(Boolean modComposicionCualCuan) {
		this.modComposicionCualCuan = modComposicionCualCuan;
	}

	public Boolean isModDiseno() {
		return modDiseno;
	}

	public void setModDiseno(Boolean modDiseno) {
		this.modDiseno = modDiseno;
	}

	public Boolean isModAmplPresentaciones() {
		return modAmplPresentaciones;
	}

	public void setModAmplPresentaciones(Boolean modAmplPresentaciones) {
		this.modAmplPresentaciones = modAmplPresentaciones;
	}

	public Boolean isOtrasModificaciones() {
		return otrasModificaciones;
	}

	public void setOtrasModificaciones(Boolean otrasModificaciones) {
		this.otrasModificaciones = otrasModificaciones;
	}

	public String getDescripcionModificaciones() {
		return descripcionModificaciones;
	}

	public void setDescripcionModificaciones(String descripcionModificaciones) {
		this.descripcionModificaciones = descripcionModificaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((descripcionModificaciones == null) ? 0 : descripcionModificaciones.hashCode());
		result = prime * result + ((modAmplPresentaciones == null) ? 0 : modAmplPresentaciones.hashCode());
		result = prime * result + ((modComposicionCualCuan == null) ? 0 : modComposicionCualCuan.hashCode());
		result = prime * result + ((modDiseno == null) ? 0 : modDiseno.hashCode());
		result = prime * result + ((nif == null) ? 0 : nif.hashCode());
		result = prime * result + ((nombreComercialProducto == null) ? 0 : nombreComercialProducto.hashCode());
		result = prime * result + ((nombreRazonSocial == null) ? 0 : nombreRazonSocial.hashCode());
		result = prime * result + ((nuevoNombreComercialProducto == null) ? 0 : nuevoNombreComercialProducto.hashCode());
		result = prime * result + ((otrasModificaciones == null) ? 0 : otrasModificaciones.hashCode());
		result = prime * result + ((rgseaa == null) ? 0 : rgseaa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModificacionDatosEntity other = (ModificacionDatosEntity) obj;
		if (descripcionModificaciones == null) {
			if (other.descripcionModificaciones != null)
				return false;
		} else if (!descripcionModificaciones.equals(other.descripcionModificaciones))
			return false;
		if (modAmplPresentaciones == null) {
			if (other.modAmplPresentaciones != null)
				return false;
		} else if (!modAmplPresentaciones.equals(other.modAmplPresentaciones))
			return false;
		if (modComposicionCualCuan == null) {
			if (other.modComposicionCualCuan != null)
				return false;
		} else if (!modComposicionCualCuan.equals(other.modComposicionCualCuan))
			return false;
		if (modDiseno == null) {
			if (other.modDiseno != null)
				return false;
		} else if (!modDiseno.equals(other.modDiseno))
			return false;
		if (nif == null) {
			if (other.nif != null)
				return false;
		} else if (!nif.equals(other.nif))
			return false;
		if (nombreComercialProducto == null) {
			if (other.nombreComercialProducto != null)
				return false;
		} else if (!nombreComercialProducto.equals(other.nombreComercialProducto))
			return false;
		if (nombreRazonSocial == null) {
			if (other.nombreRazonSocial != null)
				return false;
		} else if (!nombreRazonSocial.equals(other.nombreRazonSocial))
			return false;
		if (nuevoNombreComercialProducto == null) {
			if (other.nuevoNombreComercialProducto != null)
				return false;
		} else if (!nuevoNombreComercialProducto.equals(other.nuevoNombreComercialProducto))
			return false;
		if (otrasModificaciones == null) {
			if (other.otrasModificaciones != null)
				return false;
		} else if (!otrasModificaciones.equals(other.otrasModificaciones))
			return false;
		if (rgseaa == null) {
			if (other.rgseaa != null)
				return false;
		} else if (!rgseaa.equals(other.rgseaa))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ModificacionDatosEntity [nombreComercialProducto=" + nombreComercialProducto + ", rgseaa=" + rgseaa + ", nombreRazonSocial=" + nombreRazonSocial + ", nif=" + nif
				+ ", nuevoNombreComercialProducto=" + nuevoNombreComercialProducto + ", modComposicionCualCuan=" + modComposicionCualCuan + ", modDiseno=" + modDiseno + ", modAmplPresentaciones="
				+ modAmplPresentaciones + ", otrasModificaciones=" + otrasModificaciones + ", descripcionModificaciones=" + descripcionModificaciones + "]";
	}

}
