package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;

public interface SubvencionesAsocuaeRepository extends CrudRepository<SubvencionesAsocuaeEntity, Long> {

}
