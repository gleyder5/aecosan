package es.grupoavalon.aecosan.secosan.business.dto.user;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class UserClaveDTO extends UserDTO {

	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	private String identificationNumber;
	private String qaaCitizenLevel;
	private String lastNameInherited;


	public String getQaaCitizenLevel() {
		return qaaCitizenLevel;
	}

	public void setQaaCitizenLevel(String qaaCitizenLevel) {
		this.qaaCitizenLevel = qaaCitizenLevel;
	}

	public String getLastNameInherited() {
		return lastNameInherited;
	}

	public void setLastNameInherited(String lastNameInherited) {
		this.lastNameInherited = lastNameInherited;
	}


	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((identificationNumber == null) ? 0 : identificationNumber.hashCode());
		result = prime * result + ((lastNameInherited == null) ? 0 : lastNameInherited.hashCode());
		result = prime * result + ((qaaCitizenLevel == null) ? 0 : qaaCitizenLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserClaveDTO other = (UserClaveDTO) obj;
		if (identificationNumber == null) {
			if (other.identificationNumber != null) {
				return false;
			}
		} else if (!identificationNumber.equals(other.identificationNumber)) {
			return false;
		}
		if (lastNameInherited == null) {
			if (other.lastNameInherited != null) {
				return false;
			}
		} else if (!lastNameInherited.equals(other.lastNameInherited)) {
			return false;
		}
		if (qaaCitizenLevel == null) {
			if (other.qaaCitizenLevel != null) {
				return false;
			}
		} else if (!qaaCitizenLevel.equals(other.qaaCitizenLevel)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserClaveDTO [" + super.toString() + ", identificationNumber=" + identificationNumber + ", qaaCitizenLevel=" + qaaCitizenLevel + ", lastNameInherited=" + lastNameInherited + "]";
	}


}
