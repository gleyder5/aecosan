package es.grupoavalon.aecosan.secosan.business.impl;

import java.sql.Timestamp;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.ActivityRegistry;
import es.grupoavalon.aecosan.secosan.business.UserService;
import es.grupoavalon.aecosan.secosan.business.dto.user.AbstractUserEncodedPassword;
import es.grupoavalon.aecosan.secosan.business.dto.user.LoginUserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.LoginUserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserNoEUDTO;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.LoginUser;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoUsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioClaveEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioExtraComunitarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioInternoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanActivities;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class UserServiceImpl extends AbstractManager implements UserService {

	private static final String CLAVE_ID_NUMBER_PREFIX_REGEX = "\\w+\\/\\w+\\/";

	private static final long AUTOUNBLOCK_TIME;
	static {
		long autoUnblockTime = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_AUTOUNBLOCK_TIME);
		AUTOUNBLOCK_TIME = autoUnblockTime;
	}

	@Autowired
	private UsuarioDAO usuarioDAO;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private ActivityRegistry registry;

	@Override
	public LoginUserDTO doLogin(String userLogin, String password) throws AutenticateException {
		LoginUserDTO loginUserDto = null;
		LoginUser loginUser = null;
		try {
			loginUser = this.usuarioDAO.findUserByLoginExternal(userLogin);
			loginUserDto = executeLogin(loginUser, password);
			registryLoginActivity(loginUser, SecosanActivities.SUCCESSFULL_LOGIN);
		} catch (Exception e) {
			registryLoginActivity(loginUser, SecosanActivities.UNSUCCESSFULL_LOGIN);
			this.handleAuthenticationException(e);
		}
		return loginUserDto;

	}

	@Override
	public LoginUserDTO doLoginAdmin(String username, String password) throws AutenticateException {
		LoginUserDTO loginUserDto = null;
		LoginUser loginUser = null;
		try {
			loginUser = this.usuarioDAO.findUserByLoginInternal(username);
			loginUserDto = executeLogin(loginUser, password);
			registryLoginActivity(loginUser, SecosanActivities.SUCCESSFULL_ADMIN_LOGIN);
		} catch (Exception e) {
			registryLoginActivity(loginUser, SecosanActivities.UNSUCCESSFULL_ADMIN_LOGIN);
			this.handleAuthenticationException(e);
		}
		return loginUserDto;
	}

	private LoginUserDTO executeLogin(LoginUser loginUser, String password) throws AutenticateException {
		checkLogin(password, loginUser);
		LoginUserDTO loginUserDto = transformLoginUserToLoginUserDTO(loginUser);
		setLoginTypeUserPwInLoginDTO(loginUserDto);
		handleLastLogin((UsuarioEntity) loginUser, loginUserDto);
		return loginUserDto;
	}

	private void registryLoginActivity(LoginUser loginUser, SecosanActivities activity) {
		if (loginUser != null) {
			this.registry.registryLoginBasedActivity(loginUser.getLogin(), activity);
		}
	}

	@Override
	public boolean loginIsTakenInternal(String login) {
		boolean isTaken = false;

		try {
			if (this.usuarioDAO.findUserByLoginInternal(login) != null) {
				isTaken = true;
			}
		} catch (Exception e) {
			this.handleException(e);
		}

		return isTaken;
	}

	private void checkLogin(String plainPw, LoginUser user) throws AutenticateException {

		if (user == null || (!userIsBlocked(user) && passwordDoNotMatch(plainPw, user.getPassword()))) {

			handleUserWrongPassword(plainPw, user);

		} else if (userIsBlocked(user)) {

			handleUnblock(user, plainPw);

		} else if (passwordMatch(plainPw, user.getPassword())) {

			handleSuccessfullLoginNow(user);
		}
	}

	private void handleUserWrongPassword(String plainPw, LoginUser user) throws AutenticateException {
		if (user != null && passwordDoNotMatch(plainPw, user.getPassword())) {
			handleBlockUser(user);
		}
		throw new AutenticateException("wrong credentials", AutenticateException.AutenticateErrorCodes.ERROR_INVALID_CREDENTIALS);
	}

	private boolean passwordDoNotMatch(String plainPw, String pw) {
		return !(this.encoder.matches(plainPw, pw));
	}

	private boolean passwordMatch(String plainPw, String pw) {
		return this.encoder.matches(plainPw, pw);
	}

	private void handleUnblock(LoginUser user, String plainPw) throws AutenticateException {
		if (isAutoUnblockeable(user) && checkbasedOnBlockedTimeIfCanBeUnblock(user) && passwordMatch(plainPw, user.getPassword())) {
			handleAutoUnblock(user);
		} else {
			throw new AutenticateException("User Blocked", AutenticateException.AutenticateErrorCodes.ERROR_LOCKED_USER);
		}
	}

	private static boolean isAutoUnblockeable(LoginUser user) {
		return user instanceof UsuarioExtraComunitarioEntity;
	}

	private static boolean checkbasedOnBlockedTimeIfCanBeUnblock(LoginUser user) {
		Timestamp blockedTime = user.getBlockedTime();
		long actualTimeMinusAutoUnblockTime = System.currentTimeMillis() - AUTOUNBLOCK_TIME;
		boolean autoUnblock = false;
		if (blockedTime.getTime() <= actualTimeMinusAutoUnblockTime) {
			autoUnblock = true;
		}
		return autoUnblock;
	}

	private void handleAutoUnblock(LoginUser user) {
		user.unBlockUser();
		this.usuarioDAO.updateUser((UsuarioEntity) user);
	}

	private void handleBlockUser(LoginUser user) {
		user.increaseBlockAttempt();
		String maxFailedAttempts = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_LOGIN_MAX_FAILED_ATTEMPTS);
		verifyUserFailedAttempts(user, maxFailedAttempts);
		modiffyLoginUserBaseOnAutentication(user);
	}

	private void verifyUserFailedAttempts(LoginUser user, String maxFailedAttempts) {
		int maxAttempts = Integer.parseInt(maxFailedAttempts);
		if (maxAttempts <= user.getFailedLoginAttempts()) {
			user.blockUser();
		}
	}

	private void handleSuccessfullLoginNow(LoginUser user) {
		user.setLastSuccessfullLoginNow();
		user.resetFailedLoginAttemps();
		modiffyLoginUserBaseOnAutentication(user);
	}

	private void modiffyLoginUserBaseOnAutentication(LoginUser user) {
		UsuarioEntity userToMod = (UsuarioEntity) user;
		this.usuarioDAO.updateUser(userToMod);
	}

	private static boolean userIsBlocked(LoginUser user) {
		boolean isBlocked = false;
		if (user.getUserBlocked() != null) {
			isBlocked = user.getUserBlocked().booleanValue();
		}
		return isBlocked;
	}


	@Override
	public LoginUserDTO doClaveLoginAdmin(UserClaveDTO claveUser) throws AutenticateException {
		LoginUserDTO loginInternalUserDTO = null;
		try {
			logger.info("Identification number from certificate " + claveUser.getIdentificationNumber());
			UsuarioInternoEntity internalUser = this.getInternalUserByIdenticationDocumentForClaveLogin(claveUser);
			logger.debug("Internal User:" + internalUser);
			validateUserCredentials(internalUser);
			loginInternalUserDTO = transformLoginUserToLoginUserDTO(internalUser);
			setLoginTypeClaveInLoginDTO(loginInternalUserDTO);
			handleLastLogin(internalUser, loginInternalUserDTO);
			registryClaveLoginActivity(claveUser, SecosanActivities.SUCCESSFULL_ADMIN_CLAVE_LOGIN);
		} catch (Exception e) {
			this.handleAuthenticationException(e);
		}
		return loginInternalUserDTO;
	}

	private UsuarioInternoEntity getInternalUserByIdenticationDocumentForClaveLogin(UserClaveDTO claveUser) {
		String idNumberParsed = removeClavePrefixFromIdentificationNumber(claveUser.getIdentificationNumber());
		return this.usuarioDAO.findInternalUsersByIdentificationDocument(idNumberParsed);
	}

	private static String removeClavePrefixFromIdentificationNumber(String identificationNumber){
		String idNumberParsed = identificationNumber;
		Pattern pattern = Pattern.compile(CLAVE_ID_NUMBER_PREFIX_REGEX);
		Matcher matcher = pattern.matcher(idNumberParsed);
		if (matcher.find()) {
			idNumberParsed = matcher.replaceAll("");
		}
		return idNumberParsed;
	}

	private static void validateUserCredentials(UsuarioInternoEntity internalUser) throws AutenticateException {
		validateUserIsInternalUser(internalUser);
		validateUserNotBlocked(internalUser);
		validateUserHasAdminAccessGrantsd(internalUser);
	}

	private static void validateUserIsInternalUser(UsuarioInternoEntity internalUser) throws AutenticateException {
		if (internalUser == null) {
			throw new AutenticateException(AutenticateException.AutenticateErrorCodes.ERROR_NOT_A_REGISTRED_USER);
		}
	}

	private static void validateUserNotBlocked(UsuarioInternoEntity user) throws AutenticateException {
		if (user.getUserBlocked()) {
			throw new AutenticateException(AutenticateException.AutenticateErrorCodes.ERROR_LOCKED_USER);
		}
	}

	private static void validateUserHasAdminAccessGrantsd(UsuarioInternoEntity user) throws AutenticateException {
		UsuarioRolEntity userRole = user.getRole();
		String roleId = userRole.getIdAsString();
		if (!StringUtils.equals(roleId, SecosanConstants.USER_ROLE_ADMIN) && !StringUtils.equals(roleId, SecosanConstants.USER_ROLE_TRAMITADOR)) {
			throw new AutenticateException(AutenticateException.AutenticateErrorCodes.ERROR_INSUFICIENT_PRIVILEGES);
		}
	}

	private static void setLoginTypeUserPwInLoginDTO(LoginUserDTO userDTO) {
		userDTO.setLoginType(SecosanConstants.LOGIN_TYPE_USER_PW);
	}

	@Override
	public LoginUserDTO doClaveLogin(UserClaveDTO claveUser) {
		LoginUserDTO alreadyRegisteredUser = this.getExternalUserByIdenticationDocument(claveUser.getIdentificationNumber());
		try {
			logger.debug("Clave User:" + claveUser.getIdentificationNumber());
			if (alreadyRegisteredUser == null) {
				logger.debug("Clave User not registered" );
				alreadyRegisteredUser = this.registerNewClaveUser(claveUser);
				alreadyRegisteredUser.setFirstLogin(true);
			} else {
				logger.debug("existing clave user:" + alreadyRegisteredUser);
				updateLastLoginForRegisteredUser(Long.parseLong(alreadyRegisteredUser.getId()));
			}
			setLoginTypeClaveInLoginDTO(alreadyRegisteredUser);
			registryClaveLoginActivity(claveUser, SecosanActivities.SUCCESSFULL_CLAVE_LOGIN);
		} catch (Exception e) {
			this.handleException(e);
		}
		return alreadyRegisteredUser;
	}

	private void registryClaveLoginActivity(UserClaveDTO claveUser, SecosanActivities activity) {
		if (claveUser != null) {
			this.registry.registryLoginClaveBasedActivity(claveUser.getIdentificationNumber(), activity);
		}
	}

	private LoginUserDTO registerNewClaveUser(UserClaveDTO claveUser) {
		UsuarioEntity userAdded = saveNewClaveUser(claveUser);
		LoginUserDTO loginUserDto = new LoginUserDTO();
		loginUserDto.setId(Long.toString(userAdded.getId()));
		loginUserDto.setName(userAdded.getName());
		loginUserDto.setLastName(userAdded.getLastName());
		loginUserDto.setIdentificationNumber(userAdded.getIdentificationNumber());
		loginUserDto.setEmail(userAdded.getEmail());
		Long lastLogin = null;
		if (userAdded.getLastLogin() != null) {
			lastLogin = userAdded.getLastLogin().getTime();
		}
		loginUserDto.setLastLogin(lastLogin);
		return loginUserDto;
	}

	private UsuarioEntity saveNewClaveUser(UserClaveDTO claveUser) {
		logger.info("creating user: " + claveUser);
		UsuarioClaveEntity usuarioClaveEntity = transformDTOToEntity(claveUser, UsuarioClaveEntity.class);
		usuarioClaveEntity.setLastLogin(new Timestamp(System.currentTimeMillis()));
		usuarioClaveEntity.setIdentificationNumber(claveUser.getIdentificationNumber());
		setUserType(usuarioClaveEntity);
		setRoleOnlyForExternalUsers(usuarioClaveEntity);
		logger.debug("usuarioClaveEntity:" + usuarioClaveEntity);
		return this.usuarioDAO.addUser(usuarioClaveEntity);
	}

	private static LoginUserDTO transformLoginUserToLoginUserDTO(LoginUser loginUser) {

		if (loginUser instanceof UsuarioInternoEntity) {
			return transformInternal(loginUser);
		} else {
			return transformExternal(new LoginUserDTO(), loginUser);
		}
	}

	private static LoginUserDTO transformInternal(LoginUser loginUser) {
		UsuarioInternoEntity user = (UsuarioInternoEntity) loginUser;
		LoginUserInternalDTO loginInternalDTO = new LoginUserInternalDTO();
		transformExternal(loginInternalDTO, loginUser);
		loginInternalDTO.setRoleId(user.getRole().getIdAsString());
		loginInternalDTO.setRoleDescription(user.getRole().getCatalogValue());
		loginInternalDTO.setAreaId(user.getArea().getIdAsString());
		loginInternalDTO.setAreaDescription(user.getArea().getCatalogValue());
		return loginInternalDTO;
	}

	private static LoginUserDTO transformExternal(LoginUserDTO loginUserDto, LoginUser loginUser) {
		loginUserDto.setId(Long.toString(loginUser.getId()));
		loginUserDto.setName(loginUser.getName());
		loginUserDto.setLastName(loginUser.getLastName());
		loginUserDto.setEmail(loginUser.getEmail());
		loginUserDto.setSecondLastName(loginUser.getSecondLastName());
		loginUserDto.setIdentificationNumber(loginUser.getIdentificationNumber());
		return loginUserDto;
	}


	private void updateLastLoginForRegisteredUser(long id) {
		UsuarioEntity userClave = this.usuarioDAO.findCreatorById(id);
		userClave.setLastLogin(new Timestamp(System.currentTimeMillis()));
		this.usuarioDAO.updateUser(userClave);
	}

	private static void setLoginTypeClaveInLoginDTO(LoginUserDTO userDTO) {
		userDTO.setLoginType(SecosanConstants.LOGIN_TYPE_USER_CLAVE);
	}
	@Override
	public void doRegistry(UserDTO user) {
		try {
			UsuarioEntity entity = transformUsuarioDTOToUsuarioEntity(user);
			setEncriptedPassword(entity, user);
			setUserType(entity);
			setRoleOnlyForExternalUsers(entity);
			logger.info("adding user:" + entity);
			this.usuarioDAO.addUser(entity);
		} catch (Exception e) {
			this.handleException(e);
		}
	}

	private static UsuarioEntity transformUsuarioDTOToUsuarioEntity(UserDTO user) {
		Class<? extends UsuarioEntity> entityClass = getUserEntityClassBasedOnDTO(user.getClass());
		return transformDTOToEntity(user, entityClass);
	}

	private void setEncriptedPassword(UsuarioEntity entity, UserDTO userDto) {
		if (entity instanceof LoginUser) {
			LoginUser user = (LoginUser) entity;
			String plainPw = getPaswordFromEntityOrDTO(user, userDto);
			user.setPassword(encoder.encode(plainPw));
		}
	}

	private static String getPaswordFromEntityOrDTO(LoginUser entity, UserDTO userDto) {
		String pw = entity.getPassword();
		if (userDto instanceof AbstractUserEncodedPassword) {
			pw = ((AbstractUserEncodedPassword) userDto).getPasswordDecooded();
		}
		return pw;
	}

	private static Class<? extends UsuarioEntity> getUserEntityClassBasedOnDTO(Class<? extends UserDTO> userDtoClass) {
		Class<? extends UsuarioEntity> entityClass = null;
		if (userDtoClass.equals(UserNoEUDTO.class)) {
			entityClass = UsuarioExtraComunitarioEntity.class;
		} else if (userDtoClass.equals(UserClaveDTO.class)) {
			entityClass = UsuarioClaveEntity.class;
		} else if (userDtoClass.equals(UserInternalDTO.class)) {
			entityClass = UsuarioInternoEntity.class;
		}
		return entityClass;
	}

	private static void setUserType(UsuarioEntity newUser) {
		TipoUsuarioEntity userType = new TipoUsuarioEntity();
		String idType = null;
		if (newUser instanceof UsuarioClaveEntity) {
			idType = SecosanConstants.USER_SOLICITANT_ID;
		} else if (newUser instanceof UsuarioExtraComunitarioEntity) {
			idType = SecosanConstants.USER_NOEU_ID;
		} else if (newUser instanceof UsuarioInternoEntity) {
			idType = SecosanConstants.USER_INTERNAL_ID;
		}
		userType.setIdAsString(idType);
		newUser.setTipo(userType);
	}

	private static void setRoleOnlyForExternalUsers(UsuarioEntity newUser) {
		if (newUser instanceof UsuarioClaveEntity || newUser instanceof UsuarioExtraComunitarioEntity) {
			UsuarioRolEntity role = new UsuarioRolEntity();
			role.setIdAsString(SecosanConstants.USER_ROLE_SOLICITANT);
			newUser.setRole(role);
		}

	}

	@Override
	public LoginUserDTO getExternalUserByIdenticationDocument(String identificationNumber) {
		LoginUserDTO user = null;
		try {
			UsuarioCreadorEntity userCreator = this.usuarioDAO.findCreatorUsersByIdentificationDocument(identificationNumber);
			if (userCreator != null) {
				user = new LoginUserDTO();
				user.setId(userCreator.getId().toString());
				user.setEmail(userCreator.getEmail());
				user.setName(userCreator.getName());
				user.setLastName(userCreator.getLastName());
				user.setIdentificationNumber(userCreator.getIdentificationNumber());
				handleLastLogin(userCreator, user);
			}
		} catch (Exception e) {
			this.handleException(e);
		}

		return user;
	}

	private static void handleLastLogin(UsuarioEntity userEntity, LoginUserDTO user) {

		if (userEntity instanceof LoginUser) {

			LoginUser loginUser = (LoginUser) userEntity;
			user.setFirstLogin(loginUser.isFirstLogin());

		}
		if (userEntity.getLastLogin() != null) {

			user.setLastLogin(userEntity.getLastLogin().getTime());
		}

	}


	private void handleAuthenticationException(Exception e) throws AutenticateException {
		if (e instanceof AutenticateException) {
			throw (AutenticateException) e;
		} else {
			super.handleException(e);
		}

	}

	@Override
	public boolean loginIsTaken(String login) {
		boolean isTaken = false;

		try {
			if (this.usuarioDAO.findUserByLoginExternal(login) != null) {
				isTaken = true;
			}
		} catch (Exception e) {
			this.handleException(e);
		}

		return isTaken;
	}


	@Override
	public List<UserInternalDTO> getInternalUserList(PaginationParams paginationParams) {
		List<UserInternalDTO> internalUserDTOList = null;
		List<UsuarioInternoEntity> internalUserList = null;
		try {
			internalUserList = usuarioDAO.getInternalUserList(paginationParams);
			internalUserDTOList = transformEntityListToDTOList(internalUserList, UsuarioInternoEntity.class, UserInternalDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getInternalUserList | ERROR " + e.getMessage());
		}
		return internalUserDTOList;
	}

	@Override
	public void deleteInternalUser(String userId, String userToDeleteId) {
		try {
			usuarioDAO.deleteUserInternalById(Long.valueOf(userToDeleteId));
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getInternalUserList | ERROR " + e.getMessage());
		}

	}

	@Override
	public void updateInternalUser(String userId, UserInternalDTO userToUpdate) {
		try {
			if (areValidFields(userToUpdate)) {
				UsuarioInternoEntity userInternalToUpdate = transformDTOToEntity(userToUpdate, UsuarioInternoEntity.class);
				this.copyActualPassword(userInternalToUpdate);
				usuarioDAO.updateUserInternalById(userInternalToUpdate);
			} else {
				throw new ServiceException("Can not update user because login is already in use");
			}

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " updateInternalUser | ERROR " + e.getMessage());
		}

	}

	private boolean areValidFields(UserInternalDTO userToUpdate) {
		boolean areValidFields = true;
		UsuarioInternoEntity userBeforeUpdate = usuarioDAO.findUserInternalById(Long.valueOf(userToUpdate.getId()));

		if (userBeforeUpdate.getLogin().equalsIgnoreCase(userToUpdate.getLogin())) {
			areValidFields = true;
		} else if (loginIsTakenInternal(userToUpdate.getLogin())) {
			areValidFields = false;
		}
		return areValidFields;
	}

	private void copyActualPassword(UsuarioInternoEntity userInternalToUpdate) {
		UsuarioInternoEntity existingUser = this.usuarioDAO.findUserInternalById(userInternalToUpdate.getId());
		userInternalToUpdate.setPassword(existingUser.getPassword());
	}
	
	@Override
	public int getTotalListRecords(List<FilterParams> filters) {
		int totalRecords = 0;

		try {
			totalRecords = usuarioDAO.getTotalListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getTotalListRecords | PaginationParams: " + filters + " | ERROR: " + exception.getMessage());
		}

		return totalRecords;
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {
		int totalRecords = 0;
		try {
			totalRecords = usuarioDAO.getFilteredListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListRecords | ERROR: " + exception.getMessage());
		}
		return totalRecords;
	}

	@Override
	public UserDTO getInternalUserByIdentificationDocument(String identificationDocument) {
		UserDTO userDto = null;
		try {
			UsuarioEntity userEntity = this.usuarioDAO.findInternalUsersByIdentificationDocument(identificationDocument);
			if (userEntity != null) {
				userDto = transformEntityToDTO(userEntity, UserInternalDTO.class);
			}
		} catch (Exception e) {
			this.handleException(e);
		}
		return userDto;
	}
}
