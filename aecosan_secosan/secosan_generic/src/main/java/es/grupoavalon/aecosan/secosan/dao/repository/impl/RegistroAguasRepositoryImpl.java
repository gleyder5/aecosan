package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroAguasRepository;

@Component
public class RegistroAguasRepositoryImpl extends AbstractCrudRespositoryImpl<RegistroAguasEntity, Long> implements RegistroAguasRepository {

	@Override
	protected Class<RegistroAguasEntity> getClassType() {
		return RegistroAguasEntity.class;
	}

}