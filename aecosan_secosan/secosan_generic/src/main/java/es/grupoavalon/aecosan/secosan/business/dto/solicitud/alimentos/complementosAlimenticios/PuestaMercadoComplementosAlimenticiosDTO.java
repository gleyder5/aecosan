package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.IngredientesDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.PuestaMercadoDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PuestaMercadoComplementosAlimenticiosDTO extends PuestaMercadoDTO implements ComplementosAlimenticiosDTO{

	@JsonProperty("ingredientes")
	@XmlElement(name = "ingredientes")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "ingredientes")
	private IngredientesDTO ingredientes;

	public IngredientesDTO getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(IngredientesDTO ingredientes) {
		this.ingredientes = ingredientes;
	}

}
