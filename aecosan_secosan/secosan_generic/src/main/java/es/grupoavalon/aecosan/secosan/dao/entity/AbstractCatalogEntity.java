package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;



@MappedSuperclass
public abstract class AbstractCatalogEntity {
	
	@Transient
	protected String idAsString;
	
	@Column(name = ColumnNames.CATALOG_NOMBRE, nullable = false)
	protected String catalogValue;
	
	public abstract String getIdAsString();
	
	public abstract void setIdAsString(String idAsString);

	public String getCatalogValue() {
		return catalogValue;
	}

	public void setCatalogValue(String catalogValue) {
		this.catalogValue = catalogValue;
	}

	public void setIdAsString(Long id) {
		if (id != null) {
			this.idAsString = Long.toString(id);
		}
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((catalogValue == null) ? 0 : catalogValue.hashCode());
		result = prime * result
				+ ((idAsString == null) ? 0 : idAsString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractCatalogEntity other = (AbstractCatalogEntity) obj;
		if (catalogValue == null) {
			if (other.catalogValue != null)
				return false;
		} else if (!catalogValue.equals(other.catalogValue))
			return false;
		if (idAsString == null) {
			if (other.idAsString != null)
				return false;
		} else if (!idAsString.equals(other.idAsString))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [idAsString=" + idAsString + ", catalogValue=" + catalogValue + "]";
	}
}
