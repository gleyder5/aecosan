package es.grupoavalon.aecosan.secosan.dao.repository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * @author juan.cabrerizo
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface TaggedCatalog {
	/**
	 * Tag used to identify the method
	 * 
	 * @return
	 */
	String tagMethod();
}
