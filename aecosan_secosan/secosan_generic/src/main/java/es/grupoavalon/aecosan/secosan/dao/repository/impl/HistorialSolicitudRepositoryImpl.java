package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.HistorialSolicitudRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HistorialSolicitudRepositoryImpl  extends AbstractCrudRespositoryImpl<HistorialSolicitudEntity, Long>
        implements HistorialSolicitudRepository {

    @Override
    protected Class<HistorialSolicitudEntity> getClassType() {
        return HistorialSolicitudEntity.class;
    }

    @Override
    public List<HistorialSolicitudEntity> getPaginatedList(PaginationParams paginationParams) {
        Map<String, Object> queryParams = new HashMap<String, Object>();
        List<FilterParams> filters = paginationParams.getFilters();
        final String queryWherePart = "WHERE solicitud.id =:solicitudId";
        for (FilterParams fp :filters) {
            if(fp.getFilterName().equals("solicitudId")){
                queryParams.put("solicitudId", Long.valueOf(fp.getFilterValue()));
            }
        }
        List<HistorialSolicitudEntity> resultList;
        resultList = findAllPaginatedAndSortedDESC("WHERE solicitud.id = :solicitudId" , paginationParams.getStart(), paginationParams.getLength(),"fecha", queryParams);
        return  resultList;
    }
}
