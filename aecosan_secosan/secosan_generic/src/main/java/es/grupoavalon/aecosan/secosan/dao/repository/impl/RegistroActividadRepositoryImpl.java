package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.entity.RegistroActividadEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroActividadRepository;

@Repository
public class RegistroActividadRepositoryImpl extends AbstractCrudRespositoryImpl<RegistroActividadEntity, Long> implements RegistroActividadRepository {

	@Override
	protected Class<RegistroActividadEntity> getClassType() {
		return RegistroActividadEntity.class;
	}

}
