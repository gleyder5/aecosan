package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.IdentificacionPeticionDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("identificacionRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/nuevoIdentificacionPeticion")
public class IdentificacionPeticionRestService {

	private static final Logger logger = LoggerFactory.getLogger(IdentificacionPeticionRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{areaID}")
	public Response getIdentificationByArea(@PathParam("areaID") String areaId) {
		logger.debug("-- Method getIdentificationByArea GET Init --");
		Response response;

		IdentificacionPeticionDTO identificationRequest;

		try {
			logger.debug("-- Method getIdentificationRequestByAreaId || areaId: {} ", areaId);
			identificationRequest = bfacade.findIdentificationRequestByAreaId(areaId);
			response = responseFactory.generateOkGenericResponse(identificationRequest);
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method getIdentificationByArea GET End --");
		return response;
	}
}
