package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.FicheroInstruccionesDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.FicheroInstruccionesPagoRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.FicheroInstruccionesRepository;

@Repository
public class FicheroInstruccionesDAOImpl extends GenericDao implements FicheroInstruccionesDAO {

	@Autowired
	private FicheroInstruccionesRepository ficheroInstruccionesRepository;

	@Autowired
	private FicheroInstruccionesPagoRepository ficheroInstruccionesPagoRepository;

	@Override
	public FicheroInstruccionesEntity findInstructionsFileBySolicitudeType(Long solicitudeType) {
		FicheroInstruccionesEntity instructionsFile = null;
		try {
			instructionsFile = ficheroInstruccionesRepository.findBySolicitudeType(solicitudeType);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findInstructionsFileBySolicitudeType | solicitudeType: " + solicitudeType + " | " + exception.getMessage());
		}
		return instructionsFile;
	}

	@Override
	public FicheroInstruccionesPagoEntity findInstructionsFileByForm(Long formId) {
		FicheroInstruccionesPagoEntity instructionsFile = null;
		try {
			instructionsFile = ficheroInstruccionesPagoRepository.findByForm(formId);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findInstructionsFileByForm | formId: " + formId + " | " + exception.getMessage());
		}
		return instructionsFile;
	}

	@Override
	public List<FicheroInstruccionesPagoEntity> findAllPaymentInstructionsByArea(Long area) {
		List<FicheroInstruccionesPagoEntity> listPaymentInstruction = null;
		try {
			listPaymentInstruction = ficheroInstruccionesPagoRepository.findAllByArea(area);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findAllPaymentInstructionsByArea | area: " + area);
		}
		return listPaymentInstruction;
	}

	@Override
	public List<FicheroInstruccionesEntity> findAllInstructionsByArea(Long area) {
		List<FicheroInstruccionesEntity> listSolicitudeInstruction = null;
		try {
			listSolicitudeInstruction = ficheroInstruccionesRepository.findAllByArea(area);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findAllInstructionsByArea | area: " + area);
		}
		return listSolicitudeInstruction;
	}

	@Override
	public FicheroInstruccionesPagoEntity addPaymentInstructions(FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity) {
		try {
			ficheroInstruccionesPagoEntity = ficheroInstruccionesPagoRepository.save(ficheroInstruccionesPagoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addPaymentInstructions | ficheroInstruccionesPagoEntity: " + ficheroInstruccionesPagoEntity);
		}
		return ficheroInstruccionesPagoEntity;
	}

	@Override
	public void updatePaymentInstructions(FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity) {
		try {
			ficheroInstruccionesPagoRepository.update(ficheroInstruccionesPagoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "updatePaymentInstructions | ficheroInstruccionesPagoEntity: " + ficheroInstruccionesPagoEntity);
		}
	}

	@Override
	public FicheroInstruccionesEntity addInstructions(FicheroInstruccionesEntity ficheroInstruccionesEntity) {
		try {
			ficheroInstruccionesEntity = ficheroInstruccionesRepository.save(ficheroInstruccionesEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addInstructions | ficheroInstruccionesEntity: " + ficheroInstruccionesEntity);
		}
		return ficheroInstruccionesEntity;
	}

	@Override
	public void updateInstructions(FicheroInstruccionesEntity ficheroInstruccionesEntity) {
		try {
			ficheroInstruccionesRepository.update(ficheroInstruccionesEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "updateInstructions | ficheroInstruccionesEntity: " + ficheroInstruccionesEntity);
		}
	}

}
