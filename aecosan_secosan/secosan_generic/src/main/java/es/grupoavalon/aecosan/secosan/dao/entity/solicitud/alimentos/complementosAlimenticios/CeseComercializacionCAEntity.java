package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios;

import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.CeseComercializacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_CESE_COMERCIALIZACION_CA)
public class CeseComercializacionCAEntity extends CeseComercializacionEntity implements ComplementosAlimenticios {


}
