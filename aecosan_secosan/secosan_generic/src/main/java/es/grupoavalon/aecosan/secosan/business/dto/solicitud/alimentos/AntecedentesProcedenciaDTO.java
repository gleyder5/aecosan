package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class AntecedentesProcedenciaDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("primeraComercializacionUE")
	@XmlElement(name = "primeraComercializacionUE")
	@BeanToBeanMapping(getValueFrom = "primeraComercializacionUE")
	private Boolean primeraComercializacionUE;

	@JsonProperty("idPaisProcedencia")
	@XmlElement(name = "idPaisProcedencia")
	@BeanToBeanMapping(getValueFrom = "paisProcedencia", getSecondValueFrom = "idPais")
	private Long idPaisProcedencia;

	@JsonProperty("idPaisComercializacionPrevia")
	@XmlElement(name = "idPaisComercializacionPrevia")
	@BeanToBeanMapping(getValueFrom = "paisComercializacionPrevia", getSecondValueFrom = "idPais")
	private Long idPaisComercializacionPrevia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getPrimeraComercializacionUE() {
		return primeraComercializacionUE;
	}

	public void setPrimeraComercializacionUE(Boolean primeraComercializacionUE) {
		this.primeraComercializacionUE = primeraComercializacionUE;
	}

	public Long getIdPaisProcedencia() {
		return idPaisProcedencia;
	}

	public void setIdPaisProcedencia(Long idPaisProcedencia) {
		this.idPaisProcedencia = idPaisProcedencia;
	}

	public Long getIdPaisComercializacionPrevia() {
		return idPaisComercializacionPrevia;
	}

	public void setIdPaisComercializacionPrevia(Long idPaisComercializacionPrevia) {
		this.idPaisComercializacionPrevia = idPaisComercializacionPrevia;
	}

	@Override
	public String toString() {
		return "AntecedentesProcedenciaDTO [id=" + id + ", primeraComercializacionUE=" + primeraComercializacionUE + ", idPaisProcedencia=" + idPaisProcedencia + ", idPaisComercializacionPrevia="
				+ idPaisComercializacionPrevia + "]";
	}

}
