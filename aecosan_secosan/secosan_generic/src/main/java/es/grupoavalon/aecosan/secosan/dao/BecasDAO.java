package es.grupoavalon.aecosan.secosan.dao;


import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.joda.time.LocalDate;

import java.util.List;

public interface BecasDAO {

	BecasEntity addBecasEntity(BecasEntity becasEntity);
	BecasEntity findBecasById(Long id);
	int getTotal(List<FilterParams> filters);

	void updateBecasEntity(BecasEntity becasEntity);
	List<BecasEntity> getBecasList();
	List<BecasEntity> getBecasListPaginated(PaginationParams paginationParams);
	List<BecasEntity> getBecasByStatusAndDate(Long statusId,LocalDate date);
	List<BecasEntity> getBecasByStatus(Long statusId);


}
