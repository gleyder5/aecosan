package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;

public interface DocumentacionDAO {
	
	DocumentacionEntity findDocumentacionById(Long idDocumentacion);
	DocumentacionEntity addDocumentacionToSolicitude(SolicitudEntity solicitudEntity, DocumentacionEntity documentacionEntity);


	void deleteDocumentacion(Long id);

	public List<TipoDocumentacionEntity> findDocumentationByFormId(String formId);

}
