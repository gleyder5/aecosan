package es.grupoavalon.aecosan.secosan.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLE_JWTOKENKEYS)
@NamedQueries({
	@NamedQuery(name = NamedQueriesLibrary.GET_CURRENT_TOKENKEY, query = "FROM "
			+ TableNames.ENTITY_PACKAGE
 + ".JWTTokenKeyEntity AS e WHERE e.adminkey = :admin"),
})
public class JWTTokenKeyEntity {

	@Id
	@Column(name="key")
	private String key;
	@Column(name="FECHA_CREACION")
	private Timestamp createdAt;
	@Column(name = "ES_ADMIN_KEY")
	private boolean adminkey;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public boolean isAdminkey() {
		return adminkey;
	}

	public void setAdminkey(boolean adminkey) {
		this.adminkey = adminkey;
	}
	@Override
	public String toString() {
		return "JWTTokenKeyEntity [key=" + key + ", createdAt=" + createdAt + ", adminkey=" + adminkey + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (adminkey ? 1231 : 1237);
		result = prime * result + ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JWTTokenKeyEntity other = (JWTTokenKeyEntity) obj;
		if (adminkey != other.adminkey) {
			return false;
		}
		if (createdAt == null) {
			if (other.createdAt != null) {
				return false;
			}
		} else if (!createdAt.equals(other.createdAt)) {
			return false;
		}
		if (key == null) {
			if (other.key != null) {
				return false;
			}
		} else if (!key.equals(other.key)) {
			return false;
		}
		return true;
	}
	
	
}
