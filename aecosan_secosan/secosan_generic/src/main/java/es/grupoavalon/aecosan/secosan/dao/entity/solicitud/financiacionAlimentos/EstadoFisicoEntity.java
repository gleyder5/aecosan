package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_ESTADO_FISICO)
public class EstadoFisicoEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.ESTADO_FISICO)
	@SequenceGenerator(name = SequenceNames.ESTADO_FISICO, sequenceName = SequenceNames.ESTADO_FISICO, allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "POLVO")
	private Boolean dust;

	@Column(name = "LIQUIDO")
	private Boolean liquid;

	@Column(name = "SOLUCION_ESTANDAR")
	private String standardSolution;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getDust() {
		return dust;
	}

	public void setDust(Boolean dust) {
		this.dust = dust;
	}

	public Boolean getLiquid() {
		return liquid;
	}

	public void setLiquid(Boolean liquid) {
		this.liquid = liquid;
	}

	public String getStandardSolution() {
		return standardSolution;
	}

	public void setStandardSolution(String standardSolution) {
		this.standardSolution = standardSolution;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dust == null) ? 0 : dust.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((liquid == null) ? 0 : liquid.hashCode());
		result = prime * result + ((standardSolution == null) ? 0 : standardSolution.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoFisicoEntity other = (EstadoFisicoEntity) obj;
		if (dust == null) {
			if (other.dust != null)
				return false;
		} else if (!dust.equals(other.dust))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (liquid == null) {
			if (other.liquid != null)
				return false;
		} else if (!liquid.equals(other.liquid))
			return false;
		if (standardSolution == null) {
			if (other.standardSolution != null)
				return false;
		} else if (!standardSolution.equals(other.standardSolution))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EstadoFisicoEntity [id=" + id + ", dust=" + dust + ", liquid=" + liquid + ", standardSolution=" + standardSolution + "]";
	}

}
