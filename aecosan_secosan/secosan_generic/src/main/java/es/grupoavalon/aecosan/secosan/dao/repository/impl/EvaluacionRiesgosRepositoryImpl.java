package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.EvaluacionRiesgosRepository;

@Component
public class EvaluacionRiesgosRepositoryImpl extends AbstractCrudRespositoryImpl<EvaluacionRiesgosEntity, Long> implements EvaluacionRiesgosRepository {

	@Override
	protected Class<EvaluacionRiesgosEntity> getClassType() {
		return EvaluacionRiesgosEntity.class;
	}

}
