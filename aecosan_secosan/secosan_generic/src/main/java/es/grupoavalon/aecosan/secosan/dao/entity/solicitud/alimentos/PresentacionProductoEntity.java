package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PRESENTACIONES_PRODUCTOS)
public class PresentacionProductoEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.PRESENTACIONES_PRODUCTOS_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.PRESENTACIONES_PRODUCTOS_SEQUENCE, sequenceName = SequenceNames.PRESENTACIONES_PRODUCTOS_SEQUENCE, allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "PUESTA_MERCADO_ID", referencedColumnName = "ID", nullable = false)
	private PuestaMercadoEntity puestaMercado;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "FORMA_ID")
	private FormaEntity forma;

	@Column(name = "SABORES")
	private String sabores;

	@Column(name = "TIPO_CAPACIDAD_ENVASE")
	private String envase;

	@Column(name = "OTRAS_CARACTERISTICAS")
	private String otrasCaracteristicas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FormaEntity getForma() {
		return forma;
	}

	public void setForma(FormaEntity forma) {
		this.forma = forma;
	}

	public String getSabores() {
		return sabores;
	}

	public void setSabores(String sabores) {
		this.sabores = sabores;
	}

	public String getEnvase() {
		return envase;
	}

	public void setEnvase(String envase) {
		this.envase = envase;
	}

	public String getOtrasCaracteristicas() {
		return otrasCaracteristicas;
	}

	public void setOtrasCaracteristicas(String otrasCaracteristicas) {
		this.otrasCaracteristicas = otrasCaracteristicas;
	}

	public PuestaMercadoEntity getPuestaMercado() {
		return puestaMercado;
	}

	public void setPuestaMercado(PuestaMercadoEntity puestaMercado) {
		this.puestaMercado = puestaMercado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((envase == null) ? 0 : envase.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((otrasCaracteristicas == null) ? 0 : otrasCaracteristicas.hashCode());
		result = prime * result + ((sabores == null) ? 0 : sabores.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PresentacionProductoEntity other = (PresentacionProductoEntity) obj;
		if (envase == null) {
			if (other.envase != null) {
				return false;
			}
		} else if (!envase.equals(other.envase)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (otrasCaracteristicas == null) {
			if (other.otrasCaracteristicas != null) {
				return false;
			}
		} else if (!otrasCaracteristicas.equals(other.otrasCaracteristicas)) {
			return false;
		}
		if (sabores == null) {
			if (other.sabores != null) {
				return false;
			}
		} else if (!sabores.equals(other.sabores)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PresentacionProductoEntity [id=" + id + ", sabores=" + sabores + ", envase=" + envase + ", otrasCaracteristicas="
				+ otrasCaracteristicas + "]";
	}

}
