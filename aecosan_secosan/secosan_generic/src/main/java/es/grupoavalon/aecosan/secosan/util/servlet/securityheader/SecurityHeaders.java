package es.grupoavalon.aecosan.secosan.util.servlet.securityheader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Controller
public class SecurityHeaders {

	private static final Logger logger = LoggerFactory.getLogger(SecurityHeaders.class);
	private static final String INIT_PAGE;
	private static final String INIT_PAGE_ADMIN;
	static {
		INIT_PAGE = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_INIT_PAGE);
		INIT_PAGE_ADMIN = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_INIT_ADMIN_PAGE);
		logger.debug("admin page:" + INIT_PAGE_ADMIN);
		logger.debug("init page:" + INIT_PAGE);
	}

	private static final String LOGIN_URL_ADMIN = "/admin/login";
	private static final String LOGIN_URL = "/login";
	private static final String CHECK_XFORWARD_HEADERS_URL = "/checkSecurityHeaders";

	private static final String SECURITY_HEADERS_CACHE_CONTROL = "Cache-Control";
	private static final String SECURITY_HEADERS_CACHE_CONTROL_VALUE = "no-cache, no-store, max-age=0, must-revalidate";
	private static final String SECURITY_HEADERS_PRAGMA = "Pragma";
	private static final String SECURITY_HEADERS_PRAGMA_VALUE = "no-cache";
	private static final String SECURITY_HEADERS_CONTENT_TYPE_OPTIONS = "X-Content-Type-Options";
	private static final String SECURITY_HEADERS_CONTENT_TYPE_OPTIONS_VALUE = "nosniff";
	private static final String SECURITY_HEADERS_CONTENT_TYPE_FRAME_OPTIONS = "X-Frame-Options";
	private static final String SECURITY_HEADERS_CONTENT_TYPE_FRAME_OPTIONS_VALUE = "SAMEORIGIN";
	private static final String SECURITY_HEADERS_XSS_PROTECTION = "X-XSS-Protection";
	private static final String SECURITY_HEADERS_XSS_PROTECTION_VALUE = "1";

	@RequestMapping(value = LOGIN_URL, method = { RequestMethod.GET, RequestMethod.POST })
	public String redirectToLoginPage(HttpServletResponse response, HttpServletRequest request) {
		String originalRequest = (String) request.getAttribute(SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL);
		addSecurityHeadersToInitialRequest(response);
		return handleRedirect(INIT_PAGE, originalRequest);

	}

	@RequestMapping(value = LOGIN_URL_ADMIN, method = { RequestMethod.GET, RequestMethod.POST })
	public String redirectToAdminLoginPage(HttpServletResponse response, HttpServletRequest request) {
		String originalRequest = (String) request.getAttribute(SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL);
		addSecurityHeadersToInitialRequest(response);
		logger.debug("redirect to admin page:" + INIT_PAGE_ADMIN);
		return handleRedirect(INIT_PAGE_ADMIN, originalRequest);
	}

	private void addSecurityHeadersToInitialRequest(HttpServletResponse response) {
		response.addHeader(SECURITY_HEADERS_CACHE_CONTROL, SECURITY_HEADERS_CACHE_CONTROL_VALUE);
		response.addHeader(SECURITY_HEADERS_PRAGMA, SECURITY_HEADERS_PRAGMA_VALUE);
		response.addHeader(SECURITY_HEADERS_CONTENT_TYPE_OPTIONS, SECURITY_HEADERS_CONTENT_TYPE_OPTIONS_VALUE);
		response.addHeader(SECURITY_HEADERS_CONTENT_TYPE_FRAME_OPTIONS, SECURITY_HEADERS_CONTENT_TYPE_FRAME_OPTIONS_VALUE);
		response.addHeader(SECURITY_HEADERS_XSS_PROTECTION, SECURITY_HEADERS_XSS_PROTECTION_VALUE);
	}

	private String handleRedirect(String initPage, String originalRequest) {
		String redirect;
		if (StringUtils.isBlank(originalRequest)) {
			redirect = returnForwardInitPage(initPage);
		} else {
			redirect = "redirect:" + CHECK_XFORWARD_HEADERS_URL + ".do?" + SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL + "=" + originalRequest;
		}
		return redirect;
	}

	private String returnForwardInitPage(String initPage) {
		Log.debug("redirectingTo:" + initPage);
		return "redirect:" + initPage;
	}

	@RequestMapping(value = CHECK_XFORWARD_HEADERS_URL, method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView checkXforwardHeader(@RequestParam(required = true, value = SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL) String originalRequest) {
		ModelAndView alertJsp = new ModelAndView();
		alertJsp.addObject("originalRequest", transformOriginalRequestBase64ToNormalString(originalRequest));
		alertJsp.setViewName("/errorHeaders");
		return alertJsp;
	}

	private static String transformOriginalRequestBase64ToNormalString(String originalRequest) {
		String originalUrl = originalRequest;
		if (Base64.isBase64(originalRequest.getBytes())) {
			originalUrl = new String(Base64.decodeBase64(originalUrl));
		}
		return originalUrl;
	}
}
