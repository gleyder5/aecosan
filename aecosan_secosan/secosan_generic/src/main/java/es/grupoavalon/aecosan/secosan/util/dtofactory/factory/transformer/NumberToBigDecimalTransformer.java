/**
 * 
 */
package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import java.math.BigDecimal;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.TransformerException;

/**
 * Transforms a Float en BigDecimal
 * 
 * @author otto.abreu
 *
 */
public class NumberToBigDecimalTransformer extends CustomValueTransformer {


	@Override
	public Object transform(Object input) {
		BigDecimal bd = null;
		if (input != null) {

			try {
				if (input instanceof Float) {
					bd = this.fromFloat(input);
				} else if (input instanceof Double) {
					bd = this.fromDouble(input);
				} else if (input instanceof Integer) {
					bd = this.fromInteger(input);
				} else {

					throw new TransformerException(
							TransformerException.NOT_SUPORTED_TYPE
									+ input.getClass());
				}
			} catch (Exception e) {
				handleException(e);
			}

		}

		return bd;
	}

	private BigDecimal fromFloat(Object input) {
		Float floatValue = (Float) input;
		String value = floatValue.toString();
		return BigDecimal.valueOf(Double.parseDouble(value));
	}

	private BigDecimal fromDouble(Object input) {
		Double doubleValue = (Double) input;
		return BigDecimal.valueOf(doubleValue);

	}

	private BigDecimal fromInteger(Object input) {
		Integer integerValue = (Integer) input;
		return BigDecimal.valueOf(integerValue);

	}

	@Override
	public Object reverseTransform(Object input, Class<?> outputType) {
		Object output = null;
		BigDecimal bigDecimalInput = (BigDecimal) input;
		if (input != null) {
			try {
				output = transfromFromBigdecimal(bigDecimalInput, outputType);
			} catch (Exception e) {
				handleException(e);
			}
		}
		return output;
	}

	private static Object transfromFromBigdecimal(BigDecimal input,
			Class<?> outputClass) {
		Object output = null;

		if (isInteger(outputClass)) {

			output = fromBigDecimalToInteger(input);

		} else if (isDouble(outputClass)) {

			output = fromBigDecimalToDouble(input);

		} else if (isFloat(outputClass)) {

			output = fromBigDecimalToFloat(input);
		} else {
			throw new TransformerException(
					TransformerException.NOT_SUPORTED_TYPE + outputClass);
		}

		return output;
	}

	private static Object fromBigDecimalToInteger(BigDecimal input) {
		return input.intValue();
	}

	private static Object fromBigDecimalToDouble(BigDecimal input) {
		return input.doubleValue();
	}

	private static Object fromBigDecimalToFloat(BigDecimal input) {
		return input.floatValue();
	}
}
