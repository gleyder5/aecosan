package es.grupoavalon.aecosan.secosan.util.rest.security;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;


public abstract class AbstractJwtAuthenticationProvider implements AuthenticationProvider {
	
	@Override
	public Authentication authenticate(Authentication auth) {
		if (auth instanceof TokenAuth) {
			this.authenticate((TokenAuth) auth);
		}else if (auth instanceof CorsOptionsAuth){
			
			auth.setAuthenticated(true);
		
		}else {
			throw new JWTAuthenticationException(
					"Not valid autentication object, must be instance of "
							+ TokenAuth.class.getName());
		}
		return auth;
	}

	protected abstract void authenticate(TokenAuth auth);

	@Override
	public boolean supports(Class<?> arg0) {

		return true;
	}

	public static final class TokenAuth implements Authentication {

		
		private boolean authenticated;
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private String jwtOriginalToken;

		public TokenAuth(String jwtOriginalToken) {
			super();
			this.jwtOriginalToken = jwtOriginalToken;
		}

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			// TODO Auto-generated method stub
			return Collections.emptyList();
		}

		@Override
		public Object getCredentials() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getDetails() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getPrincipal() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isAuthenticated() {
			// TODO Auto-generated method stub
			return authenticated;
		}

		@Override
		public void setAuthenticated(boolean arg0)
				throws IllegalArgumentException {
			this.authenticated = arg0;
		}

		public String getJwtOriginalToken() {
			return jwtOriginalToken;
		}

		public void setJwtOriginalToken(String jwtOriginalToken) {
			this.jwtOriginalToken = jwtOriginalToken;
		}

	}
	
	public static final class CorsOptionsAuth implements Authentication {

		private boolean authenticated;
		private static final long serialVersionUID = 1L;

		@Override
		public String getName() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			// TODO Auto-generated method stub
			return Collections.emptyList();
		}

		@Override
		public Object getCredentials() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getDetails() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Object getPrincipal() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isAuthenticated() {
		
			return authenticated;
		}

		@Override
		public void setAuthenticated(boolean isAuthenticated)
				throws IllegalArgumentException {
			this.authenticated = isAuthenticated;
		}
		
	}

	public static class JWTAuthenticationException extends
			AuthenticationException {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public JWTAuthenticationException(String msg) {
			super(msg);

		}

	}

}
