package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import java.util.Arrays;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name=TableNames.TABLA_DOCUMENTO_SOLICITUD)
public class DocumentacionEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.DOCUMENTACION_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.DOCUMENTACION_SEQUENCE, sequenceName = SequenceNames.DOCUMENTACION_SEQUENCE, allocationSize = 1)
	@Column(name="ID")
	private Long id;

	@Column(name="NOMBRE_DOCUMENTO")
	private String documentName;

	@Column(name="ENVIAR_COMO_ADJUNTO")
	private boolean sendAsAttachment = false;

	@Lob
	@Column(name = "DOCUMENTO_BINARIO")
    private byte[] document;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name="TIPO_DOCUMENTACION_ID")
	private TipoDocumentacionEntity docType;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "SOLICITUD_ID", referencedColumnName = "ID")
	private SolicitudEntity solicitud;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDocumentName() {
		return documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public byte[] getDocument() {
		return document;
	}

	public void setDocument(byte[] document) {
		this.document = document;
	}


	public TipoDocumentacionEntity getDocType() {
		return docType;
	}

	public void setDocType(TipoDocumentacionEntity docType) {
		this.docType = docType;
	}

	public SolicitudEntity getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudEntity solicitud) {
		this.solicitud = solicitud;
	}

	public boolean isSendAsAttachment() {
		return sendAsAttachment;
	}

	public void setSendAsAttachment(boolean sendAsAttachment) {
		this.sendAsAttachment = sendAsAttachment;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docType == null) ? 0 : docType.getId().hashCode());
		result = prime * result + Arrays.hashCode(document);
		result = prime * result + ((documentName == null) ? 0 : documentName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DocumentacionEntity other = (DocumentacionEntity) obj;
		if (docType == null) {
			if (other.docType != null)
				return false;
		} else if (!docType.getId().equals(other.docType.getId()))
			return false;
		if (!Arrays.equals(document, other.document))
			return false;
		if (documentName == null) {
			if (other.documentName != null)
				return false;
		} else if (!documentName.equals(other.documentName))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String docTypeId = "";
		if (docType != null) {
			docTypeId = docType.getIdAsString();
		}
		return "DocumentacionEntity [id=" + id + ", documentName=" + documentName + ", docType=" + docTypeId + "]";
	}

}
