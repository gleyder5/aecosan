package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_AGREGACION_PAISES)
public class AgregacionPaisEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "CONTINENTE_ID", nullable = false)
	private ContinenteEntity continent;

	@Column(name = ColumnNames.INE_CODE, nullable = false)
	private String ineCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	public ContinenteEntity getContinent() {
		return continent;
	}

	public void setContinent(ContinenteEntity continent) {
		this.continent = continent;
	}

	public String getIneCode() {
		return ineCode;
	}

	public void setIneCode(String ineCode) {
		this.ineCode = ineCode;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ineCode == null) ? 0 : ineCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AgregacionPaisEntity other = (AgregacionPaisEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ineCode == null) {
			if (other.ineCode != null)
				return false;
		} else if (!ineCode.equals(other.ineCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String continentId = "";
		if (continent != null) {
			continentId = continent.getIdAsString();
		}

		return "AgregacionPaisEntity [id=" + id + ", continent=" + continentId + ", ineCode=" + ineCode + "]";
	}

}
