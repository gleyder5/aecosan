/**
 * 
 */
package es.grupoavalon.aecosan.secosan.business.exception;

import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

public class DeleteException extends DaoException {

	private static final long serialVersionUID = 1L;
	
	public static final String STATUS_MUST_BE_DRAFT = "Solicitude Status must be: draft ";

	public DeleteException(String message) {
		super(message);
	}
}
