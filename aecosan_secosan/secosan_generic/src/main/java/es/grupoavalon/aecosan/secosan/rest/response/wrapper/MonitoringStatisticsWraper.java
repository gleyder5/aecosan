package es.grupoavalon.aecosan.secosan.rest.response.wrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.annotation.XmlElement;

import es.grupoavalon.aecosan.secosan.business.dto.MonitoringStatisticsDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

public class MonitoringStatisticsWraper {
	private Map<String, MonitoringStatisticsDTO> statistics;

	public Map<String, MonitoringStatisticsDTO> getStatistics() {
		return statistics;
	}

	public void setStatistics(Map<String, MonitoringStatisticsDTO> statistics) {
		this.statistics = statistics;
		calcStatisticsForNVD3();
	}

	private void calcStatisticsForNVD3() {
		statisticsNVD3 = new ArrayList<MonitoringStatisticsDTOforNVD3>();

		MonitoringStatisticsDTOforNVD3 oks = new MonitoringStatisticsDTOforNVD3();
		oks.setColor(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_MONITORING_OK_COLOR));
		oks.setKey(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_MONITORING_OK_LABEL));
		oks.values = new ArrayList<MonitoringStatisticsWraper.GroupedResultsDTOforNVD3>();

		MonitoringStatisticsDTOforNVD3 errors = new MonitoringStatisticsDTOforNVD3();
		errors.setColor(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_MONITORING_ERROR_COLOR));
		errors.setKey(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_MONITORING_ERROR_LABEL));
		errors.values = new ArrayList<MonitoringStatisticsWraper.GroupedResultsDTOforNVD3>();

		for (Entry<String, MonitoringStatisticsDTO> entry : statistics.entrySet()) {
			oks.values.add(new MonitoringStatisticsWraper.GroupedResultsDTOforNVD3(entry.getKey(), entry.getValue().getOks()));
			errors.values.add(new MonitoringStatisticsWraper.GroupedResultsDTOforNVD3(entry.getKey(), entry.getValue().getErrors() * -1));
		}
		statisticsNVD3.add(errors);
		statisticsNVD3.add(oks);

	}


	@XmlElement(name = "statisticsNVD3")
	private List<MonitoringStatisticsDTOforNVD3> statisticsNVD3;

	class MonitoringStatisticsDTOforNVD3 {
		@XmlElement(name = "key")
		private String key;
		@XmlElement(name = "color")
		private String color;
		@XmlElement(name = "values")
		private List<GroupedResultsDTOforNVD3> values;

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getColor() {
			return color;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public List<GroupedResultsDTOforNVD3> getValues() {
			return values;
		}

		public void setValues(List<GroupedResultsDTOforNVD3> values) {
			this.values = values;
		}
	}

	class GroupedResultsDTOforNVD3 {
		@XmlElement(name = "label")
		private String label;

		@XmlElement(name = "value")
		private Integer value;

		public GroupedResultsDTOforNVD3(String label, Integer value) {
			this.label = label;
			this.value = value;
		}

		public String getLabel() {
			return label;
		}

		public void setLabel(String label) {
			this.label = label;
		}

		public Integer getValue() {
			return value;
		}

		public void setValue(Integer value) {
			this.value = value;
		}

	}
}
