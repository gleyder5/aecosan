package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_BAJA_OFER_ALIMENTOS)
public class BajaOferAlimUMEEntity extends AbstractFinanciacionAlimentos {

	@Column(name = "FECHA_COMUNICACION")
	private Timestamp communicationDate;

	@Column(name = "MOTIVO_DURACION_SUSPENSION")
	private String suspensionReasonDuration;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "BAJA_O_SUSPENSION_ID")
	private BajaOSuspensionEntity cancelOrSuspension;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "bajaOferAlim", cascade = { CascadeType.MERGE, CascadeType.REMOVE, CascadeType.PERSIST })
	@JoinColumn(name = "BAJA_ALIM_UME_ID")
	private List<ProductoSuspendidoEntity> products;

	@Column(name = "COMPANY_REGISTER_NUMBER")
	private String companyRegisterNumber;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Transient
	private Long communicationDateLong;

	public String getCompanyRegisterNumber() {
		return companyRegisterNumber;
	}

	public void setCompanyRegisterNumber(String companyRegisterNumber) {
		this.companyRegisterNumber = companyRegisterNumber;
	}

	public Long getCommunicationDateLong() {
		return communicationDate.getTime();
	}

	public void setCommunicationDateLong(Long communicationDateLong) {
		this.communicationDateLong = communicationDateLong;
		communicationDate = new Timestamp(communicationDateLong);
	}

	public Timestamp getCommunicationDate() {
		return communicationDate;
	}

	public void setCommunicationDate(Timestamp communicationDate) {
		this.communicationDate = communicationDate;
	}

	public String getSuspensionReasonDuration() {
		return suspensionReasonDuration;
	}

	public void setSuspensionReasonDuration(String suspensionReasonDuration) {
		this.suspensionReasonDuration = suspensionReasonDuration;
	}

	public BajaOSuspensionEntity getCancelOrSuspension() {
		return cancelOrSuspension;
	}

	public void setCancelOrSuspension(BajaOSuspensionEntity cancelOrSuspension) {
		this.cancelOrSuspension = cancelOrSuspension;
	}

	public List<ProductoSuspendidoEntity> getProducts() {
		return products;
	}

	public void setProducts(List<ProductoSuspendidoEntity> products) {
		this.products = products;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((cancelOrSuspension == null) ? 0 : cancelOrSuspension.getId().hashCode());
		result = prime * result + ((communicationDate == null) ? 0 : communicationDate.hashCode());
		result = prime * result + ((suspensionReasonDuration == null) ? 0 : suspensionReasonDuration.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BajaOferAlimUMEEntity other = (BajaOferAlimUMEEntity) obj;
		if (cancelOrSuspension == null) {
			if (other.cancelOrSuspension != null) {
				return false;
			}
		} else if (!cancelOrSuspension.getId().equals(other.cancelOrSuspension.getId())) {
			return false;
		}
		if (communicationDate == null) {
			if (other.communicationDate != null) {
				return false;
			}
		} else if (!communicationDate.equals(other.communicationDate)) {
			return false;
		}
		if (suspensionReasonDuration == null) {
			if (other.suspensionReasonDuration != null) {
				return false;
			}
		} else if (!suspensionReasonDuration.equals(other.suspensionReasonDuration)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String idCancelOrSuspension = null;
		if (cancelOrSuspension != null) {
			idCancelOrSuspension = String.valueOf(cancelOrSuspension.getId());
		} else {
			idCancelOrSuspension = "";
		}
		return "BajaOferAlimUMEEntity [communicationDate=" + communicationDate + ", suspensionReasonDuration=" + suspensionReasonDuration + ", cancelOrSuspension=" + idCancelOrSuspension + "]";
	}

}
