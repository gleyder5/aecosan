package es.grupoavalon.aecosan.secosan.dao.library;

public class NamedQueriesLibrary {

	public static final String GET_INSTRUCTIONS_BY_SOLICITUDETYPE = "instructions.findInstructionsBySolicitudetype";
	public static final String GET_PAYMENT_INSTRUCTIONS_BY_FORM = "paymentInstructions.findInstructionsByForm";
	public static final String GET_INSTRUCTIONS_BY_LIST_AREA = "instructions.findInstructionsListByArea";
	public static final String GET_PAYMENT_INSTRUCTIONS_LIST_BY_AREA = "paymentInstructions.findAllByArea";
	public static final String GET_PAY_TYPE_SERVICE_BY_PAY_TYPE_ID_AND_SERVICE_ID = "payTypeService.findPayTypeServiceByPayTypeIdAndServiceId";
	public static final String GET_SERVICES_BY_PAY_TYPE_ID = "service.getServicesByPayTypeId";
	public static final String GET_CURRENT_TOKENKEY = "JWTTokenKeyEntity.getCurrentTokenKey";
	public static final String GET_STATUS_BY_SOLICITUDE_ID = "status.getStatusBySolicitudeId";
	public static final String GET_HISTORIAL_BY_SOLICITUD_ID = "historialSolicitud.getHistorialSolicitudBySolicitudeId";
	public static final String GET_STATUS_BY_AREA_ID = "status.getStatusByAreaId";
	public static final String GET_USUARIO_BY_LOGIN = "user.getUserbyName";
	public static final String GET_USUARIO_INTERNO_BY_NAME = "userInterno.getUserbyName";
	public static final String GET_USUARIO_BY_IDENTIFICATORNUMBER = "user.getUserbyIdentificationNumber";
	public static final String GET_DOCUMENT_TYPE_BY_FORM_ID = "documentation.getDocumentTypeByFormId";
	public static final String GET_AREA_BY_FORMULARIO_ID = "area.getAreaByFormId";
	public static final String GET_SIGN_BY_FORMULARIO_ID = "solicitudeType.getSignByFormId";
	public static final String GET_PAGO_BY_SOLICITUDE_REFNUMBER = "pago.getPagoBySolicitudeRefNumber";
	public static final String GET_PAGO_BY_SOLICITUDE_ID = "pago.getPagoBySolicitudeId";
	public static final String GET_ROLES_INTERNAL = "roles.getInternalRoles";

	public static final String GET_IDIOMAS_BECAS_BY_BECA = "idiomasBecas.getIdiomasBecasByBeca";
	public static final String GET_IDIOMAS_BECAS_BY_BECA_AND_IDIOMA = "idiomasBecas.getIdiomasBecasByBecaAndIdioma";
	public static final String GET_BECAS_BY_STATUS= "becas.getBecasByStatus";

	public static final String GET_HISTORIAL_BY_SOLICITUD_ID_AND_STATUS_ID = "historial.getBySolicitudAndStatus";
	public static final String GET_PRESENTACION_ENVASE_BY_FINANCIACION_ALIMENTOS_ID = "presentacion.getByFinanciacionId";
	public static final String GET_REGISTRO_ACTIVIDADES_BY_USER= "registroActividad.getByUserId";

	private NamedQueriesLibrary() {
		super();
	}

}
