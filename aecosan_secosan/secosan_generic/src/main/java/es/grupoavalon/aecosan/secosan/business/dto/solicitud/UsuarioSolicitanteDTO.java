package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.TipoPersonaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.TipoUsuarioDTO;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class UsuarioSolicitanteDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("name")
	@XmlElement(name = "name")
	@BeanToBeanMapping(getValueFrom = "name")
	private String name;

	@JsonProperty("lastName")
	@XmlElement(name = "lastName")
	@BeanToBeanMapping(getValueFrom = "lastName")
	private String lastName;

	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	private String identificationNumber;

	@JsonProperty("address")
	@XmlElement(name = "address")
	@BeanToBeanMapping(getValueFrom = "address")
	private String address;
	
	@JsonProperty("addressType")
	@XmlElement(name = "addressType")
	@BeanToBeanMapping(getValueFrom = "addressType")
	private String addressType;
	
	@JsonProperty("addressNumber")
	@XmlElement(name = "addressNumber")
	@BeanToBeanMapping(getValueFrom = "addressNumber")
	private String addressNumber;
	
	@JsonProperty("stair")
	@XmlElement(name = "stair")
	@BeanToBeanMapping(getValueFrom = "stair")
	private String stair;
	
	@JsonProperty("floor")
	@XmlElement(name = "floor")
	@BeanToBeanMapping(getValueFrom = "floor")
	private String floor;
	
	@JsonProperty("door")
	@XmlElement(name = "door")
	@BeanToBeanMapping(getValueFrom = "door")
	private String door;

	@JsonProperty("location")
	@XmlElement(name = "location")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "location")
	private UbicacionGeograficaDTO location;

	@JsonProperty("telephoneNumber")
	@XmlElement(name = "telephoneNumber")
	@BeanToBeanMapping(getValueFrom = "telephoneNumber")
	private String telephoneNumber;

	@JsonProperty("email")
	@XmlElement(name = "email")
	@BeanToBeanMapping(getValueFrom = "email")
	private String email;

	@JsonProperty("role")
	@XmlElement(name = "role")
	// @BeanToBeanMapping(generateOther = true, getValueFrom = "role")
	private UsuarioRolDTO role;

	@JsonProperty("tipo")
	@XmlElement(name = "tipo")
	// @BeanToBeanMapping(generateOther = true, getValueFrom = "tipo")
	private TipoUsuarioDTO tipo;

	@JsonProperty("personType")
	@XmlElement(name = "personType")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "personType")
	private TipoPersonaDTO personType;


	@JsonProperty("fechaNac")
	@XmlElement(name = "fechaNac")
	@BeanToBeanMapping(getValueFrom = "fechaNacString")
	private Long fechaNac;

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UbicacionGeograficaDTO getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaDTO location) {
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UsuarioRolDTO getRole() {
		return role;
	}

	public void setRole(UsuarioRolDTO role) {
		this.role = role;
	}

	public TipoUsuarioDTO getTipo() {
		return tipo;
	}

	public void setTipo(TipoUsuarioDTO tipo) {
		this.tipo = tipo;
	}

	public TipoPersonaDTO getPersonType() {
		return personType;
	}

	public void setPersonType(TipoPersonaDTO personType) {
		this.personType = personType;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStair() {
		return stair;
	}

	public void setStair(String stair) {
		this.stair = stair;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public Long getFechaNac() {
		return fechaNac;
	}

	public void setFechaNac(Long fechaNac) {
		this.fechaNac = fechaNac;
	}

	@Override
	public String toString() {
		return "UsuarioSolicitanteDTO [id=" + id + ", name=" + name + ", lastName=" + lastName + ", identificationNumber=" + identificationNumber + ", address=" + address
				+ ", addressType=" + addressType + ", addressNumber=" + addressNumber + ", stair=" + stair + ", floor=" + floor
				+ ", door=" + door + ", location=" + location + ", telephoneNumber=" + telephoneNumber + ", email=" + email
				+ ", role=" + role + ", tipo=" + tipo + ", personType=" + personType + "]";
	}

}
