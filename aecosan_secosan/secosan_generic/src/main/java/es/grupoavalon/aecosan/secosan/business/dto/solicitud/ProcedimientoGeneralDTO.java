package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos.TipoPagoServicioDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlElement;

public class ProcedimientoGeneralDTO extends SolicitudDTO implements IsNulable<ProcedimientoGeneralDTO> {

	@JsonProperty("asunto")
	@XmlElement(name = "asunto")
	@BeanToBeanMapping(getValueFrom = "asunto")
	private String asunto;

	@JsonProperty("expone")
	@XmlElement(name = "expone")
	@BeanToBeanMapping(getValueFrom = "expone")
	private String expone;

	@JsonProperty("solicita")
	@XmlElement(name = "solicita")
	@BeanToBeanMapping(getValueFrom = "solicita")
	private String solicita;

	@JsonProperty("tipo")
	@XmlElement(name = "tipo")
	@BeanToBeanMapping(getValueFrom = "tipo")
	private String tipo;



	@Override
	public ProcedimientoGeneralDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}

	private boolean checkNullability() {
		boolean nullabillity = true;
		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getAsunto());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getExpone());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getSolicita());
		return nullabillity;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getExpone() {
		return expone;
	}

	public void setExpone(String expone) {
		this.expone = expone;
	}

	public String getSolicita() {
		return solicita;
	}

	public void setSolicita(String solicita) {
		this.solicita = solicita;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
