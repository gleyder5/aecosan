package es.grupoavalon.aecosan.secosan.rest;

import javax.persistence.Id;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentResponseDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.BooleanWrapper;
import org.springframework.web.bind.annotation.RequestParam;

@Component("paymentRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/payment")
public class PaymentRestService {

	private static final Logger logger = LoggerFactory.getLogger(PaymentRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/justificante")
	public Response getJustifier(JustifierRequestDTO justifierRequest) {
		logger.debug("-- Method getJustifier post Init --");
		Response response;
		try {
			JustifierResponseDTO justifierResponse = this.bfacade.getPaymentJustifier(justifierRequest);
			response = this.responseFactory.generateOkGenericResponse(justifierResponse);
		} catch (Exception e) {
			response = this.responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response makePayment(OnlinePaymentRequestDTO paymentRequest) {
		logger.debug("-- Method makePayment post Init --");
		Response response;
		try {
			OnlinePaymentResponseDTO paymentResponse = this.bfacade.makePayment(paymentRequest);
			response = this.responseFactory.generateOkGenericResponse(paymentResponse);
		} catch (Exception e) {
			response = this.responseFactory.generateErrorResponseForPaymentException(e);
		}
		return response;
	}
	private Response generateJustificantePagoPdf(String identificadorPeticion,Long serviceId){
		Response response = null ;
		try {
			logger.debug("-- Method getJustificantePago || identificadorPeticion: {} {}", identificadorPeticion," --");

			JustificantePagoReportDTO justificantePagoDTO;
			justificantePagoDTO = bfacade.getJustificantePago(identificadorPeticion,serviceId);
			response = responseFactory.generateOkGenericResponse(justificantePagoDTO);

		} catch (Exception e) {
			logger.error("Error getJustificantePago || identificadorPeticion: " + identificadorPeticion + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return  response;
	}
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/justificantePdf/{identificadorPeticion}/{serviceId}")
	public Response getJustificantePagoPdfWithServiceId(@PathParam("identificadorPeticion") String identificadorPeticion,@PathParam("serviceId") Long serviceId) {
		return generateJustificantePagoPdf(identificadorPeticion,serviceId);
	}
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/justificantePdf/{identificadorPeticion}")
	public Response getJustificantePagoPdf(@PathParam("identificadorPeticion") String identificadorPeticion) {
		return generateJustificantePagoPdf(identificadorPeticion,null);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/isPaid/{identificadorPeticion}")
	public Response isPaidBySolicitudeID(@PathParam("identificadorPeticion") String identificadorPeticion) {
		Response response = null;
		try {
			if(logger.isDebugEnabled()) {
				logger.debug("-- Method isPaidBySolicitudeID || identificadorPeticion: " + identificadorPeticion + " --");
			}
			BooleanWrapper booleanWrapper = bfacade.isPaidBySolicitudeID(identificadorPeticion);
			response = responseFactory.generateOkGenericResponse(booleanWrapper);

		} catch (Exception e) {
			logger.error("Error isPaidBySolicitudeID || identificadorPeticion: " + identificadorPeticion + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method isPaidBySolicitudeID || response: {}", response);
		return response;

	}

}
