package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.TipoDocumentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoDocumentacionRepository;

@Repository
public class TipoDocumentacionDAOImpl extends GenericDao implements TipoDocumentosDAO {

	@Autowired
	private TipoDocumentacionRepository tipoDocumentacionRepository;

	@Override
	public TipoDocumentacionEntity findTipoDocumentacion(Long pk) {
		TipoDocumentacionEntity tipoDocumentacionEntity = null;

		try {
			tipoDocumentacionEntity = tipoDocumentacionRepository.findOne(pk);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "findTipoDocumentacion | " + pk + " ERROR: " + e.getMessage());
		}

		return tipoDocumentacionEntity;
	}

	@Override
	public void addTipoDocumentacion(TipoDocumentacionEntity tipoDocumentacionEntity) {
		try {
			tipoDocumentacionRepository.save(tipoDocumentacionEntity);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "adddTipoDocumentacion  ERROR: " + e.getMessage());
		}

	}

}
