package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;

public interface RegistroReacuDAO {

	RegistroReacuEntity addRegistroReacuEntity(RegistroReacuEntity registroReacuEntity);
	void updateRegistroReacuEntity(RegistroReacuEntity registroReacuEntity);

}
