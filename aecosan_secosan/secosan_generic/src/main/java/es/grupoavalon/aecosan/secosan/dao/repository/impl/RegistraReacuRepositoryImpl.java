package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroReacuRepository;
import org.springframework.stereotype.Component;

@Component
public class RegistraReacuRepositoryImpl extends AbstractCrudRespositoryImpl<RegistroReacuEntity, Long> implements RegistroReacuRepository {

	@Override
	protected Class<RegistroReacuEntity> getClassType() {
		return RegistroReacuEntity.class;
	}

}
