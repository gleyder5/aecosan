package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_TIPO_SOLICITUD)
public class TipoSolicitudEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "AREA_ID", referencedColumnName = ColumnNames.CATALOG_ID, nullable = true)
	private AreaEntity relatedArea;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinTable(name = TableNames.TABLA_TIPO_DOCUMENTO_TIPO_SOLICITUD, joinColumns = { @JoinColumn(name = "TIPO_SOLICITUD_ID", referencedColumnName = ColumnNames.CATALOG_ID) }, inverseJoinColumns = {
			@JoinColumn(name = "TIPO_DOC_ID", referencedColumnName = ColumnNames.CATALOG_ID) })
	private List<TipoDocumentacionEntity> docTypes;

	public List<TipoDocumentacionEntity> getDocTypes() {
		return docTypes;
	}

	public void setDocTypes(List<TipoDocumentacionEntity> docTypes) {
		this.docTypes = docTypes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	public AreaEntity getRelatedArea() {
		return relatedArea;
	}

	public void setRelatedArea(AreaEntity relatedArea) {
		this.relatedArea = relatedArea;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoSolicitudEntity other = (TipoSolicitudEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TipoSolicitudEntity [id=" + id + "]";
	}

}
