package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SolicitudDTO extends AbstractSolicitudDTO {

	@JsonProperty("documentacion")
	@XmlElement(name = "documentacion")
	private List<DocumentacionDTO> documentationList;

	@JsonProperty("datos_pago")
	@XmlElement(name = "datos_pago")
	private SolicitudPaymentDataDTO paymentData;

	public SolicitudPaymentDataDTO getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(SolicitudPaymentDataDTO paymentData) {
		this.paymentData = paymentData;
	}

	public List<DocumentacionDTO> getDocumentacion() {
		return documentationList;
	}

	public void setDocumentacion(List<DocumentacionDTO> documentationList) {
		this.documentationList = documentationList;
	}

}
