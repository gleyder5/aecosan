package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DocumentacionDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("fileName")
	@XmlElement(name = "fileName")
	@BeanToBeanMapping(getValueFrom = "documentName")
	private String fileName;

	@JsonProperty("docType")
	@XmlElement(name = "docType")
	@BeanToBeanMapping(getValueFrom = "docType", getSecondValueFrom = "id")
	private Long docType;

	@JsonProperty("docTypeDesc")
	@XmlElement(name = "docTypeDesc")
	@IgnoreField(inReverse = true)
	@BeanToBeanMapping(getValueFrom = "docType", getSecondValueFrom = "catalogValue")
	private String docTypeDesc;

	@JsonProperty("solicitud")
	@XmlElement(name = "solicitud")
	@BeanToBeanMapping(getValueFrom = "solicitud", getSecondValueFrom = "id")
	private Long solicitud;

	@JsonProperty("base64")
	@XmlElement(name = "base64")
	private String base64;

	@JsonProperty("sendAsAttachment")
	@XmlElement(name = "sendAsAttachment")
	private boolean sendAsAttachment = false;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getDocType() {
		return docType;
	}

	public void setDocType(Long docType) {
		this.docType = docType;
	}

	public Long getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(Long solicitud) {
		this.solicitud = solicitud;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBase64() {
		return base64;
	}

	public void setBase64(String base64) {
		this.base64 = base64;
	}

	public String getDocTypeDesc() {
		return docTypeDesc;
	}

	public void setDocTypeDesc(String docTypeDesc) {
		this.docTypeDesc = docTypeDesc;
	}

	public boolean isSendAsAttachment() {
		return sendAsAttachment;
	}

	public void setSendAsAttachment(boolean sendAsAttachment) {
		this.sendAsAttachment = sendAsAttachment;
	}
}
