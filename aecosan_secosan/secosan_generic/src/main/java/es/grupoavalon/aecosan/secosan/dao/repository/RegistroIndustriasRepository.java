package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;

public interface RegistroIndustriasRepository extends CrudRepository<RegistroIndustriasEntity, Long> {

}
