package es.grupoavalon.aecosan.secosan.dao.impl;


import es.grupoavalon.aecosan.secosan.dao.ProcedimientoGeneralDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.ProcedimientoGeneralEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.ProcedimientoGeneralRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProcedimientoGeneralDAOImpl extends GenericDao implements ProcedimientoGeneralDAO {

	@Autowired
	private ProcedimientoGeneralRepository procedimientoGeneralRepository;
	@Override
	public ProcedimientoGeneralEntity addProcedimientoGeneralEntity(ProcedimientoGeneralEntity procedimientoGeneral) {
		ProcedimientoGeneralEntity procedimientoGeneralEntity= null;
		try {
			procedimientoGeneralEntity = procedimientoGeneralRepository.save(procedimientoGeneral);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addProcedimientoGeneral | procedimientoGeneral: " + procedimientoGeneral);
		}
		return procedimientoGeneralEntity;
	}

	@Override
	public void updateProcedimientoGeneralEntity(ProcedimientoGeneralEntity procedimientoGeneral) {
		try {
			procedimientoGeneralRepository.update(procedimientoGeneral);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateProcedimientoGeneral | ERROR: " + procedimientoGeneral);
		}

	}
}
