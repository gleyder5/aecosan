package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class CatalogDTO {

	@JsonProperty("catalogId")
	@XmlElement(name = "catalogId")
	@BeanToBeanMapping(getValueFrom="idAsString")
	private String catalogId;

	@JsonProperty("catalogValue")
	@XmlElement(name = "catalogValue")
	@BeanToBeanMapping(getValueFrom="catalogValue")
	private String catalogValue;

	public String getCatalogValue() {
		return catalogValue;
	}

	public void setCatalogValue(String catalogValue) {
		this.catalogValue = catalogValue;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((catalogId == null) ? 0 : catalogId.hashCode());
		result = prime * result + ((catalogValue == null) ? 0 : catalogValue.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CatalogDTO other = (CatalogDTO) obj;
		if (catalogId == null) {
			if (other.catalogId != null)
				return false;
		} else if (!catalogId.equals(other.catalogId))
			return false;
		if (catalogValue == null) {
			if (other.catalogValue != null)
				return false;
		} else if (!catalogValue.equals(other.catalogValue))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CatalogDTO [catalogId=" + catalogId + ", catalogValue=" + catalogValue + "]";
	}

}
