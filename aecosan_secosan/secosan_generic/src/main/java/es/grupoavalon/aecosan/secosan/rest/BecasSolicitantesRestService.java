package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.BecasService;
import es.grupoavalon.aecosan.secosan.business.dto.SolicitudListDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Component("becasSolicitanteRest")
@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/becas/solicitantes/")
public class BecasSolicitantesRestService {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    BusinessFacade bfacade;

    @Autowired
    ResponseFactory responseFactory;

    @Autowired
    BecasService becasService;

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("{user}")
    public Response listItems(@PathParam(AbstractListRestHandler.PARAM_USER) String user, @DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {
        logger.debug("-- Method getBecasSolicitantes GET Init--");
        logger.debug("Entrada petición GET listItems()");

        AbstractListRestHandler<BecasSolicitanteDTO> listHandler = new AbstractListRestHandler<BecasSolicitanteDTO>() {
            @Override
            protected AbstractResponseFactory getResponseFactory() {
                return responseFactory;
            }

            @Override
            PaginatedListWrapper<BecasSolicitanteDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
                return bfacade.getBecasSolicitantesDTOPaginated(otherParams, paginationParams);
            }

        };
        logger.debug("-- Method getBecasSolicitantes GET End--");
        return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters(), user);
    }

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("{user}/{idSolicitud}/score")
    public Response getBecaScore(@PathParam("user") String user,@PathParam("idSolicitud") String idSolicitud){
        logger.debug("-- Method getBecaScore GET Init--");
        Response response = responseFactory.generateOkGenericResponse(becasService.getBecaScore(idSolicitud));
        logger.debug("-- Method getBecaScore GET End--");
        return response;
    }
}
