package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsFileDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsListDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("instruccionesRest")
public class FicheroInstruccionesRestService {

	private static final Logger logger = LoggerFactory.getLogger(FicheroInstruccionesRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/instructions/";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/instructions/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{solicitudeTypeId}")
	public Response downloadInstructionsFileBySolicitudeTypeService(@PathParam("solicitudeTypeId") String solicitudeTypeId) {

		return this.downloadInstructionsFileBySolicitudeType(solicitudeTypeId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "{solicitudeTypeId}")
	public Response downloadInstructionsFileBySolicitudeTypeServiceAdmin(@PathParam("solicitudeTypeId") String solicitudeTypeId) {

		return this.downloadInstructionsFileBySolicitudeType(solicitudeTypeId);
	}

	private Response downloadInstructionsFileBySolicitudeType(String solicitudeTypeId) {
		Response response = null;

		InstructionsFileDTO ficheroInstrucciones;
		logger.debug("-- Method downloadInstructionsFileBySolicitudeType GET Init --");

		try {

			logger.debug("-- Method getFicheroInstruccionesByIdFormulario | solicitudeTypeId:  {} ", solicitudeTypeId);
			ficheroInstrucciones = bfacade.findInstructionsFileBySolicitudeType(solicitudeTypeId);

			response = responseFactory.generateOkGenericResponse(ficheroInstrucciones);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method downloadInstructionsFileBySolicitudeType GET End --");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "payment/{formId}")
	public Response downloadPaymentInstructionsFileByFormIdService(@PathParam("formId") String formId) {

		return this.downloadPaymentInstructionsFileByFormId(formId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "payment/{formId}")
	public Response downloadPaymentInstructionsFileByFormIdServiceAdmin(@PathParam("formId") String formId) {

		return this.downloadPaymentInstructionsFileByFormId(formId);
	}

	private Response downloadPaymentInstructionsFileByFormId(String formId){
		logger.debug("-- Method downloadPaymentInstructionsFileByFormId GET Init --");
		Response response = null;

		InstructionsFileDTO instructionsFile;

		try {

			logger.debug("-- Method downloadPaymentInstructionsFileByFormId | formularioId: {}", formId);
			instructionsFile = bfacade.findInstructionsFileByForm(formId);

			response = responseFactory.generateOkGenericResponse(instructionsFile);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method downloadPaymentInstructionsFileByFormId GET End --");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "instructionList/{userId}")
	public Response findInstructionsLists(@PathParam("userId") String userId) {
		logger.debug("-- Method findInstructionsLists GET Init --");
		Response response = null;

		InstructionsListDTO listInstructions;

		try {

			logger.debug("-- Method findInstructionsLists | userId: {}", userId);
			listInstructions = bfacade.findInstructionsLists(userId);

			response = responseFactory.generateOkGenericResponse(listInstructions);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method findInstructionsLists GET End --");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "instructionsToPay")
	public Response uploadInstructionsToPay(InstructionsFileDTO ficheroInstrucciones) {
		Response response = null;
		try {

			logger.debug("-- Method uploadInstructionsToPay --");
			bfacade.uploadInstructionsToPay(ficheroInstrucciones);

			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method uploadInstructionsToPay POST End --");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL)
	public Response uploadInstructions(InstructionsFileDTO ficheroInstrucciones) {
		Response response = null;
		try {
			logger.debug("-- Method uploadInstructions --");
			bfacade.uploadInstructions(ficheroInstrucciones);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method uploadInstructions POST End --");
		return response;
	}
}
