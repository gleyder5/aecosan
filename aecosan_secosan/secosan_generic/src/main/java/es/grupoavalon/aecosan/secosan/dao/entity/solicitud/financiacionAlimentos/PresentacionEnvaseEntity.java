package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.*;

import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PRESENTACIONES_ENVASES)
@NamedQueries({
				@NamedQuery(name = NamedQueriesLibrary.GET_PRESENTACION_ENVASE_BY_FINANCIACION_ALIMENTOS_ID, query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE + ".solicitud.financiacionAlimentos.PresentacionEnvaseEntity AS p WHERE p.financiacionAlimentos.id = :financiacionId")
		})
public class PresentacionEnvaseEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.PRESENTACIONES_ENVASES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.PRESENTACIONES_ENVASES_SEQUENCE, sequenceName = SequenceNames.PRESENTACIONES_ENVASES_SEQUENCE, allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Column(name = "TIPO_ENVASE")
	private String containerType;

	@Column(name = "NUMERO_ENVASES")
	private String numberOfContainer;

	@Column(name = "PRECIO_VENTA_EMPRESA")
	private String companyPriceSale;

	@Column(name = "SABOR")
	private String flavour;

	@Column(name = "CONTENIDO_ENVASE")
	private String contents;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "OFER_ALIM_UME_ID", referencedColumnName = "ID", nullable = false)
	private FinanciacionAlimentosEntity financiacionAlimentos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getNumberOfContainer() {
		return numberOfContainer;
	}

	public void setNumberOfContainer(String numberOfContainer) {
		this.numberOfContainer = numberOfContainer;
	}

	public String getCompanyPriceSale() {
		return companyPriceSale;
	}

	public void setCompanyPriceSale(String companyPriceSale) {
		this.companyPriceSale = companyPriceSale;
	}

	public String getFlavour() {
		return flavour;
	}

	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public FinanciacionAlimentosEntity getFinanciacionAlimentos() {
		return financiacionAlimentos;
	}

	public void setFinanciacionAlimentos(FinanciacionAlimentosEntity financiacionAlimentos) {
		this.financiacionAlimentos = financiacionAlimentos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((companyPriceSale == null) ? 0 : companyPriceSale.hashCode());
		result = prime * result + ((containerType == null) ? 0 : containerType.hashCode());
		result = prime * result + ((contents == null) ? 0 : contents.hashCode());
		result = prime * result + ((flavour == null) ? 0 : flavour.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((numberOfContainer == null) ? 0 : numberOfContainer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PresentacionEnvaseEntity other = (PresentacionEnvaseEntity) obj;
		if (companyPriceSale == null) {
			if (other.companyPriceSale != null)
				return false;
		} else if (!companyPriceSale.equals(other.companyPriceSale))
			return false;
		if (containerType == null) {
			if (other.containerType != null)
				return false;
		} else if (!containerType.equals(other.containerType))
			return false;
		if (contents == null) {
			if (other.contents != null)
				return false;
		} else if (!contents.equals(other.contents))
			return false;
		if (flavour == null) {
			if (other.flavour != null)
				return false;
		} else if (!flavour.equals(other.flavour))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (numberOfContainer == null) {
			if (other.numberOfContainer != null)
				return false;
		} else if (!numberOfContainer.equals(other.numberOfContainer))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PresentacionEnvaseEntity [id=" + id + ", containerType=" + containerType + ", numberOfContainer=" + numberOfContainer + ", companyPriceSale=" + companyPriceSale + ", flavour="
				+ flavour + ", contents=" + contents + "]";
	}

}
