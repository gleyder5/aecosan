package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import org.apache.commons.lang.StringUtils;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.TransformerException;

public abstract class CustomValueTransformer {
	
	public abstract Object transform (Object input);
	
	public abstract Object reverseTransform(Object input, Class<?> outputType);
	
	protected static boolean objectBelongToClass(Class<?> objectClass,
			Class<?> expectedClass, Class<?> expectedPrimitive) {
	
		if (objectClass.equals(expectedClass)
				|| objectClass.equals(expectedPrimitive)) {
			return true;
		} else {
			return false;
		}
	}
	
	protected static Object transformFromString(Object input, Class<?> outputType) {
		Object output = null;
		String inputString = (String) input;
		if (StringUtils.isNotEmpty(inputString)) {
			if (isInteger(outputType)) {

				output = fromStringToInteger(inputString);

			} else if (isLong(outputType)) {

				output = fromStringToLong(inputString);

			} else if (isFloat(outputType)) {

				output = fromStringToFloat(inputString);

			} else if (isDouble(outputType)) {

				output = fromStringToDouble(inputString);

			} else if (isBoolean(outputType)) {

				output = fromStringToBoolean(inputString);

			} else if (isShort(outputType)) {

				output = fromStringToShort(inputString);

			} else if (isChar(outputType)) {

				output = fromStringToChar(inputString);

			} else if (isByte(outputType)) {

				output = fromStringToByte(inputString);

			} else if (isString(outputType)) {
				output = inputString;
			} else {
				throw new TransformerException(TransformerException.NOT_SUPORTED_TYPE + outputType);
			}
		}

		return output;
	}
	
	protected static Object fromStringToInteger(String input) {
		return Integer.parseInt(input);
	}

	protected static Object fromStringToFloat(String input) {
		return Float.parseFloat(input);
	}

	protected static Object fromStringToDouble(String input) {
		return Double.parseDouble(input);
	}

	protected static Object fromStringToLong(String input) {
		return Long.parseLong(input);
	}

	protected static Object fromStringToChar(String input) {
		Character character = input.charAt(0);
		return character;
	}

	protected static Object fromStringToBoolean(String input) {
		return Boolean.parseBoolean(input);
	}

	protected  static Object fromStringToShort(String input) {
		return Short.parseShort(input);
	}

	protected static Byte fromStringToByte(String input) {
		return Byte.parseByte(input);
	}
	
	protected static boolean isInteger(Class<?> outputType){
		return objectBelongToClass(outputType,Integer.class,Integer.TYPE);
	}
	
	protected static boolean isLong(Class<?> outputType){
		
		return objectBelongToClass(outputType,Long.class,Long.TYPE);
	}
	
	protected static boolean isFloat(Class<?> outputType){
		return objectBelongToClass(outputType,Float.class,Float.TYPE);
	}
	
	protected static boolean isDouble(Class<?> outputType){
		return objectBelongToClass(outputType,Double.class,Double.TYPE);
	}
	
	protected static boolean isBoolean(Class<?> outputType){
		return objectBelongToClass(outputType,Boolean.class,Boolean.TYPE);
	}
	
	protected static boolean isShort(Class<?> outputType){
		return objectBelongToClass(outputType,Short.class,Short.TYPE);
	}
	
	protected static boolean isChar(Class<?> outputType){
		return objectBelongToClass(outputType,Character.class,Character.TYPE);
	}
	
	protected static boolean isByte(Class<?> outputType){
		return objectBelongToClass(outputType,Byte.class,Byte.TYPE);
	}
	
	protected static boolean isString(Class<?> outputType){
		return outputType.equals(String.class);
	}
	
	protected static void handleException(Exception e){
		if(e instanceof TransformerException){
			throw (TransformerException)e;
		}else{
			throw new TransformerException(TransformerException.GENERAL_ERROR,e);
		}
	}
}


