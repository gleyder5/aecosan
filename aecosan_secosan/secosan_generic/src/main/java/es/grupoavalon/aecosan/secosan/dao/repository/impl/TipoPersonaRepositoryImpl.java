package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoPersonaRepository;

@Component
public class TipoPersonaRepositoryImpl extends AbstractCrudRespositoryImpl<TipoPersonaEntity, Long>
		implements TipoPersonaRepository {

	@Override
	protected Class<TipoPersonaEntity> getClassType() {
		return TipoPersonaEntity.class;
	}
}
