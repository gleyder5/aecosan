package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoPagoServicioRepository;

@Component
public class TipoPagoServicioRepositoryImpl extends AbstractCrudRespositoryImpl<TipoPagoServicioEntity, Long> implements TipoPagoServicioRepository {

	private static final String PAY_TYPE = "payType";
	private static final String SERVICE = "service";

	@Override
	protected Class<TipoPagoServicioEntity> getClassType() {
		return TipoPagoServicioEntity.class;
	}

	@Override
	public TipoPagoServicioEntity findByPayTypeIdAndServiceId(Long payTypeId, Long serviceId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(PAY_TYPE, payTypeId);
		queryParams.put(SERVICE, serviceId);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_PAY_TYPE_SERVICE_BY_PAY_TYPE_ID_AND_SERVICE_ID, queryParams);
	}

}
