package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnlinePaymentResponseDTO {

	@JsonProperty("nrc")
	@XmlElement(name = "nrc")
	private String ncr;

	public String getNcr() {
		return ncr;
	}

	public void setNcr(String ncr) {
		this.ncr = ncr;
	}

	@Override
	public String toString() {
		return "OnlinePaymentResponseDTO [ncr=" + ncr + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ncr == null) ? 0 : ncr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OnlinePaymentResponseDTO other = (OnlinePaymentResponseDTO) obj;
		if (ncr == null) {
			if (other.ncr != null) {
				return false;
			}
		} else if (!ncr.equals(other.ncr)) {
			return false;
		}
		return true;
	}

}
