package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.HashMap;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.SolicitudQuejaSugerenciaService;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceAddDTO;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceUpdateDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudSugerenciaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.SolicitudQuejaSugerenciaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;

@Service
public class SolicitudQuejaSugerenciaServiceImpl extends AbstractSolicitudService implements SolicitudQuejaSugerenciaService {

	private Map<String, ServicePersistenceAddDTO> solicitudeAddMap;
	private Map<String, ServicePersistenceUpdateDTO> solicitudeUpdateMap;

	@Autowired
	private SolicitudQuejaSugerenciaDAO solicitudQuejaSugerenciaDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO solicitudesQuejaSugerenciaDTO) {
		try {
			solicitudPrepareNullDTO(solicitudesQuejaSugerenciaDTO);
			solicitudeAddMapInit();
			solicitudeAddMap.get(solicitudesQuejaSugerenciaDTO.getClass().getSimpleName()).addSolicitudDTO(user, solicitudesQuejaSugerenciaDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + solicitudesQuejaSugerenciaDTO);
		}
	}

	private void solicitudeAddMapInit() {
		if (solicitudeAddMap == null) {
			solicitudeAddMap = new HashMap<String, ServicePersistenceAddDTO>();
			solicitudeAddMap.put(EntityDtoNames.SOLICITUD_QUEJA_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addQueja(user, (SolicitudQuejaDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.SOLICITUD_SUGERENCIA_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addSugerencia(user, (SolicitudSugerenciaDTO) solicitud);
				}
			});
		}
	}

	private void addQueja(String user, SolicitudQuejaDTO solicitudQuejaDTO) {
		SolicitudQuejaEntity solicitudQuejaEntity = generateSolicitudQuejaEntityFromDTO(solicitudQuejaDTO);
		setUserCreatorByUserId(user, solicitudQuejaEntity);
		prepareAddSolicitudEntity(solicitudQuejaEntity, solicitudQuejaDTO);
		SolicitudQuejaEntity solicitudQuejaEntityNew = solicitudQuejaSugerenciaDAO.addSolicitudQueja(solicitudQuejaEntity);
		if(solicitudQuejaDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudQuejaEntityNew);
		}
	}

	private void addSugerencia(String user, SolicitudSugerenciaDTO solicitudSugerenciaDTO) {
		SolicitudSugerenciaEntity solicitudSugerenciaEntity = generateSolicitudSugerenciaEntityFromDTO(solicitudSugerenciaDTO);
		setUserCreatorByUserId(user, solicitudSugerenciaEntity);
		prepareAddSolicitudEntity(solicitudSugerenciaEntity, solicitudSugerenciaDTO);
		SolicitudSugerenciaEntity sugerenciaEntityNew  = solicitudQuejaSugerenciaDAO.addSolicitudSugerencia(solicitudSugerenciaEntity);
		if(solicitudSugerenciaDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(sugerenciaEntityNew);
		}
	}

	@Override
	public void update(String user, SolicitudDTO solicitudesQuejaSugerenciaDTO) {
		try {
			solicitudPrepareNullDTO(solicitudesQuejaSugerenciaDTO);
			solicitudeUpdateMapInit();
			solicitudeUpdateMap.get(solicitudesQuejaSugerenciaDTO.getClass().getSimpleName()).updateSolicitudDTO(user, solicitudesQuejaSugerenciaDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + solicitudesQuejaSugerenciaDTO);
		}
	}

	private void solicitudeUpdateMapInit() {
		if (solicitudeUpdateMap == null) {
			solicitudeUpdateMap = new HashMap<String, ServicePersistenceUpdateDTO>();
			solicitudeUpdateMap.put(EntityDtoNames.SOLICITUD_QUEJA_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateSolicitudQueja(user, (SolicitudQuejaDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.SOLICITUD_SUGERENCIA_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateSolicitudSugerencia(user, (SolicitudSugerenciaDTO) solicitud);
				}
			});
		}
	}

	private void updateSolicitudQueja(String user, SolicitudQuejaDTO solicitudesQuejaSugerenciaDTO) {
		SolicitudQuejaEntity solicitudQuejaEntity = transformDTOToEntity(solicitudesQuejaSugerenciaDTO, SolicitudQuejaEntity.class);
		setUserCreatorByUserId(user, solicitudQuejaEntity);
		prepareUpdateSolicitudEntity(solicitudQuejaEntity, solicitudesQuejaSugerenciaDTO);
		if(solicitudesQuejaSugerenciaDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudQuejaEntity);
		}
		solicitudQuejaSugerenciaDAO.updateSolicitudQueja(solicitudQuejaEntity);
	}

	private void updateSolicitudSugerencia(String user, SolicitudSugerenciaDTO solicitudSugerenciaDTO) {
		SolicitudSugerenciaEntity solicitudSugerenciaEntity = transformDTOToEntity(solicitudSugerenciaDTO, SolicitudSugerenciaEntity.class);
		setUserCreatorByUserId(user, solicitudSugerenciaEntity);
		prepareUpdateSolicitudEntity(solicitudSugerenciaEntity, solicitudSugerenciaDTO);
		if(solicitudSugerenciaDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudSugerenciaEntity);
		}
		solicitudQuejaSugerenciaDAO.updateSolicitudSugerencia(solicitudSugerenciaEntity);
	}

	private SolicitudQuejaEntity generateSolicitudQuejaEntityFromDTO(SolicitudQuejaDTO solicitudQuejaDTO) {
		return transformDTOToEntity(solicitudQuejaDTO, SolicitudQuejaEntity.class);

	}

	private SolicitudSugerenciaEntity generateSolicitudSugerenciaEntityFromDTO(SolicitudSugerenciaDTO solicitudSugerenciaDTO) {
		return transformDTOToEntity(solicitudSugerenciaDTO, SolicitudSugerenciaEntity.class);
	}

}
