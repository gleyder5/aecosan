package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.user.LoginDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.TokenJWTWrapper;

@Component("loginRest")
@Path("/login")
public class LoginRestService {

	private static final Logger logger = LoggerFactory.getLogger(LoginRestService.class);

	@Autowired
	private BusinessFacade facade;
	@Autowired
	private ResponseFactory responseFactory;

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response doLogin(LoginDTO user) {
		Response response = null;
		try {

			TokenJWTWrapper token = facade.doLogin(user);
			logger.info("User valid:" + user.getLogin());
			response = this.responseFactory.generateOkGenericResponse(token);

		} catch (Exception e) {

			response = this.handleException(e);
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/admin")
	public Response doLoginAdmin(LoginDTO user) {
		Response response = null;
		try {

			TokenJWTWrapper token = facade.doLoginAdmin(user);
			logger.info("User valid:" + user.getLogin());
			response = this.responseFactory.generateOkGenericResponse(token);

		} catch (Exception e) {

			response = this.handleException(e);
		}
		return response;
	}

	private Response handleException(Exception e) {
		return this.responseFactory.generateErrorResponse(e);
	}

	// private static Response createUnathorizedResponse(AutenticateException e)
	// {
	// String error = "AuthenticationError";
	// int code = e.getErrorCode().getCode();
	// String description = e.getErrorCode().getDescription();
	//
	// LoginErrorWrapper errorWrapper = new LoginErrorWrapper();
	// errorWrapper.setError(error);
	// errorWrapper.setCode(code);
	// errorWrapper.setErrorDescription(description);
	//
	// return
	// Response.status(Response.Status.UNAUTHORIZED).entity(errorWrapper).build();
	// }

	@POST
	@Path("/checkToken")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response checkToken(TokenJWTWrapper token) {
		Response response = null;
		try {
			this.facade.checkToken(token);
			response = this.responseFactory.generateOkResponse();
		} catch (Exception e) {

			response = this.handleException(e);
		}
		return response;

	}

	@POST
	@Path("/admin/checkToken")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response checkAdminToken(TokenJWTWrapper token) {
		Response response = null;
		try {
			this.facade.checkAdminToken(token);
			response = this.responseFactory.generateOkResponse();
		} catch (Exception e) {

			response = this.handleException(e);
		}
		return response;

	}

}
