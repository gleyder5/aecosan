package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoServicioEntity;

public interface TipoPagoServicioRepository extends CrudRepository<TipoPagoServicioEntity, Long> {

	public TipoPagoServicioEntity findByPayTypeIdAndServiceId(Long payTypeId, Long serviceId);

}
