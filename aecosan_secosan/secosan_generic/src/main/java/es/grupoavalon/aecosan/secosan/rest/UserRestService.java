package es.grupoavalon.aecosan.secosan.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.user.ChangePasswordDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserNoEUDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.BooleanWrapper;

@Component("userRest")
public class UserRestService {

	private static final Logger logger = LoggerFactory.getLogger(UserRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@POST
	@Path("/userNoUEMembership")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createUserNoUEMembership(UserNoEUDTO userNoUEDTO) {
		Response response = null;
		try {
			logger.debug("-- Method createUserNoUEMembership || user: " + userNoUEDTO + " --");
			bfacade.addUserNoUeMembership(userNoUEDTO);
			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			logger.error("Error createUserNoUEMembership || user: " + userNoUEDTO + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Path("/userClave")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createUserClave(UserClaveDTO userClaveDTO) {
		Response response = null;
		try {
			logger.debug("-- Method createUserClave || user: " + userClaveDTO + " --");
			bfacade.addUserClave(userClaveDTO);
			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			logger.error("Error createUserClave || user: " + userClaveDTO + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT+"/addUserInternal")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response createInternalUser(UserInternalDTO user) {
		Response response = null;
		try {
			logger.debug("-- Method createInternalUser || user: " + user + " --");
			bfacade.addUserInternal(user);
			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			logger.error("Error createInternalUser || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@GET
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT+"/getInternalUserList")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response getInternalUserList(@QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue("login") @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		AbstractListRestHandler<UserInternalDTO> listHandler = new AbstractListRestHandler<UserInternalDTO>() {
			@Override
			protected AbstractResponseFactory getResponseFactory() {
				return responseFactory;
			}

			@Override
			PaginatedListWrapper<UserInternalDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
				return bfacade.getInternalUserList(paginationParams);
			}

		};
		return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters());
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT+"/deleteInternalUser/{user}/{userToDelete}")
	public Response deleteInternalUser(@PathParam("user") String userId, @PathParam("userToDelete") String userToDeleteId) {
		Response response = null;
		try {
			logger.debug("-- Method deleteInternalUser || user: " + userId + ", userToDeleteId: " + userToDeleteId + " --");
			bfacade.deleteInternalUser(userId, userToDeleteId);
			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			logger.error("Error deleteInternalUserdeleteInternalUser || user: " + userId + ", userToDeleteId: " + userToDeleteId + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT+"/updateInternalUser/{user}")
	public Response updateInternalUser(@PathParam("user") String userId, UserInternalDTO userToUpdate) {
		Response response = null;
		try {
			logger.debug("-- Method updateInternalUser || user: " + userId + ", userToUpdate: " + userToUpdate + " --");
			bfacade.updateInternalUser(userId, userToUpdate);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			logger.error("Error updateInternalUser || user: " + userId + ", UserInternalDTO: " + userToUpdate + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/changePassword")
	public Response changePassword(ChangePasswordDTO changePasswordDto, @Context HttpServletRequest servletRequest) {

		return this.changeUserPassword(changePasswordDto, servletRequest);
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/changePassword")
	public Response changePasswordAdmin(ChangePasswordDTO changePasswordDto, @Context HttpServletRequest servletRequest) {

		return this.changeUserPassword(changePasswordDto, servletRequest);
	}

	private Response changeUserPassword(ChangePasswordDTO changePasswordDto, HttpServletRequest servletRequest) {
		Response response = null;
		try {
			String userId = SecosanUtil.getUserIdHeaderFromServletRequest(servletRequest);
			logger.debug("user id:" + userId);
			changePasswordDto.setUserId(userId);
			this.bfacade.changeUserPassword(changePasswordDto);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {

			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/existsUser/{userIdentificationNumber}")
	public Response checkInternalUserExists(@PathParam("userIdentificationNumber") String identificationNumber) {
		Response response = null;
		try {
			BooleanWrapper booleanWrapper = this.bfacade.checkInternalUserExists(identificationNumber);
			response = this.responseFactory.generateOkGenericResponse(booleanWrapper);

		} catch (Exception e) {

			response = responseFactory.generateErrorResponse(e);
		}

		return response;
	}
}
