package es.grupoavalon.aecosan.secosan.business.dto.user;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class UserInternalDTO extends AbstractUserEncodedPassword {

	@JsonProperty("login")
	@XmlElement(name = "login")
	@BeanToBeanMapping(getValueFrom = "login")
	private String login;

	@JsonProperty("password")
	@XmlElement(name = "password")
	@IgnoreField(inForward=true)
	@BeanToBeanMapping(getValueFrom = "password")
	private String password;

	@BeanToBeanMapping(getValueFrom = "role", getSecondValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	@JsonProperty("role")
	@XmlElement(name = "role")
	private String roleId;

	@BeanToBeanMapping(getValueFrom = "role", getSecondValueFrom = "catalogValue")
	@IgnoreField(inReverse=true)
	@JsonProperty("roleDescription")
	@XmlElement(name = "roleDescription")
	private String role;
	
	@BeanToBeanMapping(getValueFrom = "area", getSecondValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	@JsonProperty("area")
	@XmlElement(name = "area")
	private String areaId;
	
	@BeanToBeanMapping(getValueFrom = "area", getSecondValueFrom = "catalogValue")
	@IgnoreField(inReverse=true)
	@JsonProperty("areaDescription")
	@XmlElement(name = "areaDescription")
	private String area;

	@BeanToBeanMapping(getValueFrom = "userBlocked")
	@JsonProperty("userBlocked")
	@XmlElement(name = "userBlocked")
	private Boolean userBlocked;
	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	private String identificationNumber;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public Boolean getUserBlocked() {
		if(this.userBlocked == null){
			this.userBlocked = false;
		}
		return userBlocked;
	}

	public void setUserBlocked(Boolean userBlocked) {
		if(userBlocked == null){
			userBlocked = false;
		}
		this.userBlocked = userBlocked;
	}
	
	
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((areaId == null) ? 0 : areaId.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		result = prime * result
				+ ((userBlocked == null) ? 0 : userBlocked.hashCode());
		result = prime * result + ((identificationNumber == null) ? 0 : identificationNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserInternalDTO other = (UserInternalDTO) obj;
		if (area == null) {
			if (other.area != null) {
				return false;
			}
		} else if (!area.equals(other.area)) {
			return false;
		}
		if (areaId == null) {
			if (other.areaId != null) {
				return false;
			}
		} else if (!areaId.equals(other.areaId)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		if (roleId == null) {
			if (other.roleId != null) {
				return false;
			}
		} else if (!roleId.equals(other.roleId)) {
			return false;
		}
		if (userBlocked == null) {
			if (other.userBlocked != null) {
				return false;
			}
		} else if (!userBlocked.equals(other.userBlocked)) {
			return false;
		}
		if (identificationNumber == null) {
			if (other.identificationNumber != null) {
				return false;
			}
		} else if (!identificationNumber.equals(other.identificationNumber)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserInternalDTO [" + super.toString() + ", login=" + login + ", password=" + password
				+ ", roleId=" + roleId + ", role=" + role + ", areaId="
				+ areaId + ", area=" + area + ", userBlocked=" + userBlocked
				+ "]";
	}

	
}
