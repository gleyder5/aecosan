package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.LoginUser;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoUsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioInternoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface UsuarioDAO {

	UsuarioRolEntity findRol(long pk);

	TipoUsuarioEntity findTipoUsuario(long pk);

	LoginUser findUserByLoginExternal(String username);

	LoginUser findUserByLoginInternal(String username);

	UsuarioCreadorEntity findCreatorById(long pk);

	UsuarioCreadorEntity findCreatorUsersByIdentificationDocument(String identificationNumber,boolean isCLave);

	UsuarioEntity addUser(UsuarioEntity user);

	void updateUser(UsuarioEntity user);

	List<UsuarioInternoEntity> getInternalUserList(PaginationParams paginationParams);

	void deleteUserInternalById(Long pk);

	void updateUserInternalById(UsuarioInternoEntity userInternalToUpdate);

	UsuarioInternoEntity findUserInternalById(long valueOf);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);

	UsuarioInternoEntity findInternalUsersByIdentificationDocument(String ididentificationNumber,boolean isClave);

	LoginUser findUserByLogin(String username);

	UsuarioEntity getUserById(long userId);

}
