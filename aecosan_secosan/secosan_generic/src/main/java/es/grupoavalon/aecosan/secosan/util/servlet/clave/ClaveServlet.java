package es.grupoavalon.aecosan.secosan.util.servlet.clave;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.CodedErrorWrapper;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.TokenJWTWrapper;
import es.msssi.claveclient.Peticion;
import es.msssi.claveclient.Respuesta;
import eu.stork.peps.auth.commons.PersonalAttribute;

@Controller
public class ClaveServlet {

	private static final Logger logger = LoggerFactory.getLogger(ClaveServlet.class);

	private static final String INITIATE_PETITION_JSP = "/clave/lanzarPeticion";
	private static final String RECEIVE_RESPONSE_JSP = "/clave/paginaRespuesta";
	private static final String ERROR_PAGE = "/clave/errorClave";
	private static final String COOKIE_KEYNAME = "CLIENTCERT";
	private static final String COOKIE_KEYNAME_ERROR = "CLIENTCERTERROR";
	private static final String CLAVE_SERVLET_CONTEXT = "/clavelogin";
	private static final String CLAVE_ADMIN_SERVLET_CONTEXT = "/admin/claveAdminLogin";
	private static final String ADMIN_RESPONSE_URL;
	static {
		ADMIN_RESPONSE_URL = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_ADMIN_CLAVE_URL);
	}

	@Autowired
	private BusinessFacade businessFacade;

	@RequestMapping(value = CLAVE_SERVLET_CONTEXT, method = RequestMethod.GET)
	public ModelAndView doClaveLogin() {

		return executeClaveRequest();
	}

	@RequestMapping(value = CLAVE_ADMIN_SERVLET_CONTEXT, method = RequestMethod.GET)
	public ModelAndView doClaveAdminLogin() {

		return executeClaveRequest(ADMIN_RESPONSE_URL);
	}

	private ModelAndView executeClaveRequest(String... returnUrl) {
		ModelAndView mav = new ModelAndView();
		String view = INITIATE_PETITION_JSP;

		try {

			Peticion peticion = this.instanciateClaveRequest(returnUrl);
			setClaveRequestDataIntoModelView(mav, peticion);

		} catch (Throwable e) {
			view = ERROR_PAGE;
			logger.error("Can not generate clave request due an error:" + e.getMessage(), e);
			this.businessFacade.monitoringError(MonitorizableConnectors.CLAVE, e);
		}
		mav.setViewName(view);
		return mav;
	}

	private Peticion instanciateClaveRequest(String... returnUrl) {
		Peticion peticion = new Peticion();
		peticion.defaultRequest();
		if (returnUrl != null && returnUrl.length > 0 && StringUtils.isNotBlank(returnUrl[0])) {
			peticion.defaultRequest(returnUrl[0]);
		} else {
			peticion.defaultRequest();
		}
		return peticion;
	}

	private void setClaveRequestDataIntoModelView(ModelAndView mav, Peticion peticion) {
		mav.addObject("pepsUrl", peticion.getPepsUrl());
		mav.addObject("SAMLRequest", peticion.getSAMLRequest());
		mav.addObject("excludedIdPList", peticion.getExcludedIdPList());
		mav.addObject("forcedIdP", peticion.getForcedIdP());
	}

	@RequestMapping(value = CLAVE_SERVLET_CONTEXT, method = RequestMethod.POST)
	public ModelAndView claveLoginResponse(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		String view = RECEIVE_RESPONSE_JSP;
		try {

			Respuesta respuesta = this.proccessResponse(request);
			response.addCookie(this.generateCookieFromCLaveResponse(respuesta));
			this.businessFacade.monitoringSuccess(MonitorizableConnectors.CLAVE);
		} catch (Exception e) {
			logger.error("can not generate token for admin due an error:" + e.getMessage(), e);
			view = this.handleClaveResponseError(e);
		}

		mav.setViewName(view);
		return mav;
	}

	private Cookie generateCookieFromCLaveResponse(Respuesta response) throws IOException {
		String path = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_COOKIE_PATH);
		UserClaveDTO loginData = extractInfoFromResponse(response);
		logger.debug("extracted data: {}" , loginData);
		logger.debug("businessFacade: {}" , businessFacade);
		TokenJWTWrapper tokenWrapper = businessFacade.doLoginClave(loginData);
		String tokenJson = transformTokenIntoJson(tokenWrapper);
		logger.debug("json generated: {}", loginData);
		return generateCookie(tokenJson, path);
	}

	@RequestMapping(value = CLAVE_ADMIN_SERVLET_CONTEXT, method = RequestMethod.POST)
	public ModelAndView claveAdminLoginResponse(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView();
		String view = RECEIVE_RESPONSE_JSP;
		try {

			Respuesta respuesta = this.proccessResponse(request);
			response.addCookie(this.handleAdminLogin(respuesta));

		} catch (Throwable e) {
			logger.error("error in clave login admin servlet:" + e, e);
			view = this.handleClaveResponseError(e);
		}

		mav.setViewName(view);
		logger.debug("model and view: {}  view = {}", mav, view);
		return mav;
	}

	private Respuesta proccessResponse(HttpServletRequest request) {
		Respuesta respuesta = new Respuesta();
		respuesta.setSAMLResponse(request.getParameter("SAMLResponse"));
		respuesta.procesarRespuesta(request.getRemoteHost());
		respuesta.procesarRespuesta(request.getRemoteHost());
		return respuesta;
	}

	private Cookie handleAdminLogin(Respuesta response) throws IOException {
		Cookie cookie = null;
		try {
			cookie = generateCookieFromClaveAdminResponse(response);
		} catch (AutenticateException e) {
			cookie = generateCookieFromClaveAdminResponseAuthError(e);
		}
		return cookie;
	}

	private Cookie generateCookieFromClaveAdminResponse(Respuesta response) throws IOException, AutenticateException {
		String path = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_COOKIE_ADMIN_PATH);
		UserClaveDTO loginData = extractInfoFromResponse(response);
		logger.debug("User clave: {}",loginData);
		TokenJWTWrapper tokenWrapper = businessFacade.doLoginAdminClave(loginData);
		logger.debug("tokenWrapper: {}",  tokenWrapper);
		String tokenJson = transformTokenIntoJson(tokenWrapper);
		logger.debug("generating cookie {}  path: {}" , tokenJson ,path);
		return generateCookie(tokenJson, path);
	}

	private Cookie generateCookieFromClaveAdminResponseAuthError(AutenticateException e) throws IOException {
		String path = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_COOKIE_ADMIN_PATH);
		CodedErrorWrapper logginErrorWrapper = new CodedErrorWrapper();
		logginErrorWrapper.setError(AutenticateException.class.getSimpleName());
		logginErrorWrapper.setCode(e.getErrorCode().getCode());
		logginErrorWrapper.setErrorDescription(e.getMessage());
		String tokenJson = transformTokenIntoJson(logginErrorWrapper);
		return generateCookieAdminError(tokenJson, path);
	}

	private static Cookie generateCookie(String tokenJson, String path) {

		return generateCookie(tokenJson, COOKIE_KEYNAME, path);
	}

	private static Cookie generateCookieAdminError(String tokenJson, String path) {

		return generateCookie(tokenJson, COOKIE_KEYNAME_ERROR, path);
	}

	private static Cookie generateCookie(String tokenJson, String cookieName, String path) {
		Cookie cookie = new Cookie(cookieName, tokenJson);
		cookie.setPath(path);
		logger.debug("cookie generated: {} ", cookie);
		return cookie;
	}

	private String transformTokenIntoJson(Object tokenWrapper) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(tokenWrapper);
	}

	private static UserClaveDTO extractInfoFromResponse(Respuesta respuesta) {
		List<PersonalAttribute> attributes = respuesta.getAttrList();
		UserClaveDTO loginDataDTO = new UserClaveDTO();

		for (PersonalAttribute att : attributes) {
			logger.debug("attribute Object from clave: {} ",att);
			setReceivedDataIntoDTO(att, loginDataDTO);
		}
		return loginDataDTO;
	}

	private static void setReceivedDataIntoDTO(PersonalAttribute att, UserClaveDTO loginDataDTO) {
		String attributeName = att.getName();
		String attributeValue = transformAttributeValueIntoString(att.getValue());
		logger.debug("atribbute: {}", att);
		if (StringUtils.equalsIgnoreCase("Identificador electronico", attributeName)) {
			loginDataDTO.setIdentificationNumber(attributeValue);
		} else if (StringUtils.equalsIgnoreCase("Nombre", attributeName)) {
			loginDataDTO.setName(attributeValue);
		} else if (StringUtils.equalsIgnoreCase("Email", attributeName)) {
			loginDataDTO.setEmail(attributeValue);
		} else if (StringUtils.equalsIgnoreCase("Nivel QAA del ciudadano", attributeName)) {
			loginDataDTO.setQaaCitizenLevel(attributeValue);
		} else if (StringUtils.equalsIgnoreCase("Apellidos", attributeName)) {
			loginDataDTO.setLastName(attributeValue);
		} else if (StringUtils.equalsIgnoreCase("Apellidos Heredado", attributeName)) {
			loginDataDTO.setLastNameInherited(attributeValue);
		}else if(StringUtils.equalsIgnoreCase("eIdentifier", attributeName)) {
			loginDataDTO.setIdentificationNumber(attributeValue);
		}
	}

	private static String transformAttributeValueIntoString(List<String> attrvalue) {
		String attributeValue = "";
		if (CollectionUtils.isNotEmpty(attrvalue)) {
			attributeValue = attrvalue.get(0);
		}
		return attributeValue;
	}

	private String handleClaveResponseError(Throwable e) {

		logger.error("Can not process clave response due an error:" + e.getMessage(), e);
		this.businessFacade.monitoringError(MonitorizableConnectors.CLAVE, e);
		return ERROR_PAGE;
	}

}
