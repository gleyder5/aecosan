package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface SolicitudDAO {
	List<SolicitudEntity> getSolicList(PaginationParams paginationParams);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);

	SolicitudEntity findSolicitudById(Long id);

	void deleteSolicitudById(String solicitudeId);

	void updateSolicitudById(SolicitudEntity solicitudEntity);

}
