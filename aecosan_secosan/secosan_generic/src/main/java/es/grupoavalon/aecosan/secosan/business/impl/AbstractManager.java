package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;

@SuppressWarnings("unchecked")
public abstract class AbstractManager {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	protected static <E, D> D transformEntityToDTO(E instance, Class<D> dtoClass) {
		D dto = null;
		if (instance != null) {
			DtoEntityFactory<E, D> factory = getDtoEntityFactory((Class<E>) instance.getClass(), dtoClass);
			dto = factory.generateDTOFromEntity(instance);
		}
		return dto;
	}

	protected static <E, D> List<D> transformEntityListToDTOList(List<E> instances, Class<E> entityclass, Class<D> dtoClass) {

		DtoEntityFactory<E, D> factory = getDtoEntityFactory(entityclass, dtoClass);
		return factory.generateListDTOFromEntity(instances);
	}

	protected static <E, D> List<E> transformDTOListToEntityList(List<D> instances, Class<E> entityclass, Class<D> dtoClass) {

		DtoEntityFactory<E, D> factory = getDtoEntityFactory(entityclass, dtoClass);
		return factory.generateListEntityFromDTO(instances);
	}

	protected static <E, D> E transformDTOToEntity(D instance, Class<E> entityClass) {
		E entity = null;
		if (instance != null) {
			DtoEntityFactory<E, D> factory = getDtoEntityFactory(entityClass, (Class<D>) instance.getClass());
			entity = factory.generateEntityFromDTO(instance);
		}
		return entity;
	}

	private static <E, D> DtoEntityFactory<E, D> getDtoEntityFactory(Class<E> entityclass, Class<D> dtoClass) {

		DtoEntityFactory<E, D> factory = DtoEntityFactory.getInstance(entityclass, dtoClass);
		return factory;
	}

	protected void handleException(Throwable t) {
		if (t instanceof DaoException) {

			throw (DaoException) t;

		} else if (t instanceof ServiceException) {
			throw (ServiceException) t;
		} else {
			String message = ServiceException.SERVICE_ERROR + t;
			logger.error(message, t);
			throw new ServiceException(message, t);
		}
	}

	protected void handleException(Exception e, String logMessage) {
		logger.error(logMessage, e);
		if (!(e instanceof ServiceException)) {
			throw new ServiceException(logMessage, e);
		} else if (e instanceof DaoException) {
			throw (DaoException) e;
		} else {
			throw (ServiceException) e;
		}
	}
}
