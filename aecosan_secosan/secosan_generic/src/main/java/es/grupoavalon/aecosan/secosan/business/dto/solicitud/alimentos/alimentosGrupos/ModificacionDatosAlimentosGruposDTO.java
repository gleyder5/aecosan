package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos;

import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.ModificacionDatosDTO;

public class ModificacionDatosAlimentosGruposDTO extends ModificacionDatosDTO implements AlimentosGrupos {

}
