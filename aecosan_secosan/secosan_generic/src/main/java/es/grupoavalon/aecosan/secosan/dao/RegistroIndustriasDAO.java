package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;

public interface RegistroIndustriasDAO {
	RegistroIndustriasEntity addRegistroIndustrias(RegistroIndustriasEntity registroIndustriasEntity);

	void updateRegistroIndustrias(RegistroIndustriasEntity registroIndustriasEntity);

}
