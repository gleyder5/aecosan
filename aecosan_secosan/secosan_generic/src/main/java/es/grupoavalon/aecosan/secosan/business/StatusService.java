package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;

public interface StatusService {

	List<EstadoSolicitudDTO> getStatusByAreaId(String areaID);

}
