package es.grupoavalon.aecosan.secosan.util;

public class EntityDtoNames {

	public static final String CESE_COMERCIALIZACION_AG_ENTITY = "CeseComercializacionAGEntity";
	public static final String CESE_COMERCIALIZACION_CA_ENTITY = "CeseComercializacionCAEntity";
	public static final String PUESTA_MERCADO_AG_ENTITY = "PuestaMercadoAGEntity";
	public static final String PUESTA_MERCADO_CA_ENTITY = "PuestaMercadoCAEntity";
	public static final String MODIFICACION_DATOS_AG_ENTITY = "ModificacionDatosAGEntity";
	public static final String MODIFICACION_DATOS_CA_ENTITY = "ModificacionDatosCAEntity";
	public static final String SOLICITUD_QUEJA_ENTITY = "SolicitudQuejaEntity";
	public static final String SOLICITUD_SUGERENCIA_ENTITY = "SolicitudSugerenciaEntity";
	public static final String SOLICITUD_LOGO_ENTITY = "SolicitudLogoEntity";
	public static final String EVALUACION_RIESGOS_ENTITY = "EvaluacionRiesgosEntity";
	public static final String SOLICITUD_LMR_FITOSANITARIOS_ENTITY = "SolicitudLmrFitosanitariosEntity";
	public static final String INC_OFER_ALIM_UME_ENTITY = "IncOferAlimUMEEntity";
	public static final String ALTER_OFER_ALIM_UME_ENTITY = "AlterOferAlimUMEEntity";
	public static final String BAJA_OFER_ALIM_UME_ENTITY = "BajaOferAlimUMEEntity";
	public static final String REGISTRO_AGUAS_ENTITY = "RegistroAguasEntity";
	public static final String REGISTRO_INDUSTRIAS_ENTITY = "RegistroIndustriasEntity";
	public static final String FICHERO_INSTRUCCIONES_PAGO_ENTITY = "FicheroInstruccionesPagoEntity";
	public static final String TIPO_SOLICITUD_ENTITY = "TipoSolicitudEntity";
	public static final String TIPO_FORMULARIO_ENTITY = "TipoFormularioEntity";
	public static final String FICHERO_INSTRUCCIONES_ENTITY = "FicheroInstruccionesEntity";
	public static final String PROCEDIMIENTO_GENERAL_ENTITY= "ProcedimientoGeneralEntity";
	public static final String REGISTRO_REACU_ENTITY = "RegistroReacuEntity";
	public static final String SUBVENCIONES_ASOCUAE_ENTITY = "SubvencionesAsocuaeEntity";
	public static final String SUBVENCIONES_JUNTAS_ENTITY= "SubvencionesJuntasConsumoEntity";
	public static final String BECAS_ENTITY = "BecasEntity";

	public static final String COMPLEMENTOS_ALIMENTICIOS_DTO = "ComplementosAlimenticiosDTO";
	public static final String CESE_COMERCIALIZACION_AG_DTO = "CeseComercializacionAlimentosGruposDTO";
	public static final String CESE_COMERCIALIZACION_CA_DTO = "CeseComercializacionComplementosAlimenticiosDTO";
	public static final String MODIFICACION_DATOS_AG_DTO = "ModificacionDatosAlimentosGruposDTO";
	public static final String MODIFICACION_DATOS_CA_DTO = "ModificacionDatosComplementosAlimenticiosDTO";
	public static final String PUESTA_MERCADO_AG_DTO = "PuestaMercadoAlimentosGruposDTO";
	public static final String PUESTA_MERCADO_CA_DTO = "PuestaMercadoComplementosAlimenticiosDTO";
	public static final String INC_OFER_ALIM_UME_DTO = "IncOferAlimUMEDTO";
	public static final String ALTER_OFER_ALIM_UME_DTO = "AlterOferAlimUMEDTO";
	public static final String BAJA_OFER_ALIM_UME_DTO = "BajaOferAlimUMEDTO";
	public static final String ABSTRACT_SOLICITUD_QUEJA_SUGERENCIA_DTO = "AbstractSolicitudesQuejaSugerenciaDTO";
	public static final String SOLICITUD_QUEJA_DTO = "SolicitudQuejaDTO";
	public static final String SOLICITUD_SUGERENCIA_DTO = "SolicitudSugerenciaDTO";
	public static final String EVALUACION_RIESGOS_DTO = "EvaluacionRiesgosDTO";
	public static final String SOLICITUD_LOGO_DTO = "SolicitudLogoDTO";
	public static final String SOLICITUD_LMR_FITOSANITARIOS_DTO = "SolicitudLmrFitosanitariosDTO";
	public static final String FINANCIACION_ALIMENTOS_DTO = "FinanciacionAlimentosDTO";
	public static final String ALIMENTOS_GRUPOS_DTO = "AlimentosGrupos";
	public static final String PROCEDIMIENTO_GENERAL_DTO = "ProcedimientoGeneralDTO";
	public static final String REGISTRO_REACU_DTO = "RegistroReacuDTO";
	public static final String SUBVENCIONES_ASOCUAE_DTO = "SubvencionesAsocuaeDTO";
	public static final String SUBVENCIONES_JUNTAS_DTO  = "SubvencionesJuntasDTO";
	public static final String BECAS = "BecasDTO";


	private EntityDtoNames(){}
}
