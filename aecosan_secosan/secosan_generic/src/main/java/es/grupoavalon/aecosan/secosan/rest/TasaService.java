package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.TasaDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;

@Component("tasaRest")
public class TasaService {

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/tasa/";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/tasa/";

	private static final Logger logger = LoggerFactory.getLogger(TasaService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "formulario/{identificadorFormulario}")
	public Response getTasaByFormId(@PathParam("identificadorFormulario") String identificadorFormulario) {
		Response response = null;
		try {
			logger.debug("-- Method getTasaByFormId || identificadorFormulario: " + identificadorFormulario + " --");

			TasaDTO tasaDTO = bfacade.getTasaByFormId(identificadorFormulario);
			response = responseFactory.generateOkGenericResponse(tasaDTO);

		} catch (Exception e) {
			logger.error("Error getTasaByFormId || identificadorFormulario: " + identificadorFormulario + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method getTasaByFormId || response: " + response);
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "service/{serviceId}")
	public Response getTasaByServiceId(@PathParam("serviceId") String serviceId) {
		Response response = null;
		try {
			logger.debug("-- Method getTasaByServiceId || serviceId: " + serviceId + " --");

			TasaDTO tasaDTO = bfacade.getTasaByServicioId(serviceId);
			response = responseFactory.generateOkGenericResponse(tasaDTO);

		} catch (Exception e) {
			logger.error("Error getTasaByServiceId || serviceId: " + serviceId + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method getTasaByServiceId || response: " + response);
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL)
	public Response updateTasa(TasaDTO tasaDTO) {
		Response response;
		try {
			this.bfacade.updateTasa(tasaDTO);
			response = this.responseFactory.generateOkResponse();
		} catch (Exception e) {
			response = this.responseFactory.generateErrorResponse(e);
		}
		return response;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "list")
	public Response listTasas(@DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		logger.debug("Entrada petición GET listTasas()");

		AbstractListRestHandler<TasaDTO> listHandler = new AbstractListRestHandler<TasaDTO>() {
			@Override
			protected AbstractResponseFactory getResponseFactory() {
				return responseFactory;
			}

			@Override
			PaginatedListWrapper<TasaDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
				return bfacade.getTasaPaginatedList(paginationParams);
			}

		};
		return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters());
	}

}
