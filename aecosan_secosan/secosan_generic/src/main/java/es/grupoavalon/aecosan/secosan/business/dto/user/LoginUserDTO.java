package es.grupoavalon.aecosan.secosan.business.dto.user;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.LongToDateTransformer;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;


public class LoginUserDTO {
	
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;
	@BeanToBeanMapping
	private String name;
	@BeanToBeanMapping
	private String lastName;
	@BeanToBeanMapping
	private String secondLastName;
	@BeanToBeanMapping(getValueFrom="identificationNumber")
	private String identificationNumber;

	@BeanToBeanMapping
	private String email;
	@BeanToBeanMapping(useCustomTransformer = LongToDateTransformer.class)
	private Long lastLogin;
	@BeanToBeanMapping
	@IgnoreField(inReverse = true)
	private boolean firstLogin;

	private String loginType;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isFirstLogin() {
		return firstLogin;
	}

	public void setFirstLogin(boolean firstLogin) {
		this.firstLogin = firstLogin;
	}

	public String getLoginType() {
		return loginType;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName= secondLastName;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (firstLogin ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastLogin == null) ? 0 : lastLogin.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((loginType == null) ? 0 : loginType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LoginUserDTO other = (LoginUserDTO) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (firstLogin != other.firstLogin) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (lastLogin == null) {
			if (other.lastLogin != null) {
				return false;
			}
		} else if (!lastLogin.equals(other.lastLogin)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (loginType == null) {
			if (other.loginType != null) {
				return false;
			}
		} else if (!loginType.equals(other.loginType)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("LoginUserDTO{");
		sb.append("id='").append(id).append('\'');
		sb.append(", name='").append(name).append('\'');
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", secondLastName='").append(secondLastName).append('\'');
		sb.append(", identificationNumber='").append(identificationNumber).append('\'');
		sb.append(", email='").append(email).append('\'');
		sb.append(", lastLogin=").append(lastLogin);
		sb.append(", firstLogin=").append(firstLogin);
		sb.append(", loginType='").append(loginType).append('\'');
		sb.append('}');
		return sb.toString();
	}
}
