package es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoFormularioDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PaymentInstructionsDTO extends AbstractCatalogDto {

	@JsonProperty("formType")
	@XmlElement(name = "formType")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "formType")
	private TipoFormularioDTO formType;

	public TipoFormularioDTO getFormType() {
		return formType;
	}

	public void setFormType(TipoFormularioDTO formType) {
		this.formType = formType;
	}

	@Override
	public String toString() {
		return "FicheroInstruccionesPagoDTO [formType=" + formType + "]";
	}

}
