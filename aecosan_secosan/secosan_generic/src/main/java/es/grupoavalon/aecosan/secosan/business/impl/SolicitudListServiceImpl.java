package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.SolicitudListService;
import es.grupoavalon.aecosan.secosan.business.dto.SolicitudListDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Service
public class SolicitudListServiceImpl extends AbstractManager implements SolicitudListService {

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Override
	public List<SolicitudListDTO> getSolicList(PaginationParams paginationParams) {
		List<SolicitudListDTO> listDTO = null;
		List<SolicitudEntity> listEntity;

		try {
			listEntity = solicitudDAO.getSolicList(paginationParams);
			listDTO = transformEntityListToDTOList(listEntity, SolicitudEntity.class, SolicitudListDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getSolicList | ERROR: " + e.getMessage());
		}

		return listDTO;
	}

	@Override
	public int getTotalListRecords(List<FilterParams> filters) {
		int totalRecords = 0;

		try {
			totalRecords = solicitudDAO.getTotalListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getTotalListRecords | PaginationParams: " + filters + " | ERROR: " + exception.getMessage());
		}

		return totalRecords;
	}


	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {

		int totalRecords = 0;
		try {
			totalRecords = solicitudDAO.getFilteredListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListRecords | ERROR: " + exception.getMessage());
		}
		return totalRecords;
	}

}
