package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.TipoSuspensionFinanciacionEntity;

public interface TipoSuspensionFinanciacionRepository extends CrudRepository<TipoSuspensionFinanciacionEntity, Long> {

}
