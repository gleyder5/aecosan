package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudLmrFitosanitariosDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("solicitudLmrFitosanitariosRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/solicitudLmrFitosanitarios")
public class SolicitudLmrFitosanitariosRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/lmrFitosanitarios/{user}")
	public Response addSolicitudLmrFitosanitarios(@PathParam("user") String user, SolicitudLmrFitosanitariosDTO lmrRequest) {
		logger.debug("-- Method addSolicitudLmrFitosanitarios POST Init--");
		Response response = addSolicitud(user, lmrRequest);
		logger.debug("-- Method addSolicitudLmrFitosanitarios POST End--");
		return response;
	}
	
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/lmrFitosanitarios/{user}")
	public Response updateSolicitudLmrFitosanitarios(@PathParam("user") String user, SolicitudLmrFitosanitariosDTO lmrRequest) {
		logger.debug("-- Method updateSolicitudLmrFitosanitarios PUT Init--");
		Response response = updateSolicitud(user, lmrRequest);
		logger.debug("-- Method updateSolicitudLmrFitosanitarios PUT End--");
		return response;
	}

}
