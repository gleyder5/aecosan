package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.TipoProductoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.PuestaMercadoDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PuestaMercadoAlimentosGruposDTO extends PuestaMercadoDTO implements AlimentosGrupos {

	@JsonProperty("ingredientes")
	@XmlElement(name = "ingredientes")
	@BeanToBeanMapping(getValueFrom = "ingredientes")
	private String ingredientes;

	@JsonProperty("productType")
	@XmlElement(name = "productType")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "productType")
	private TipoProductoDTO productType;

	public String getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}

	public TipoProductoDTO getProductType() {
		return productType;
	}

	public void setProductType(TipoProductoDTO productType) {
		this.productType = productType;
	}

	@Override
	public String toString() {
		return "PuestaMercadoAlimentosGruposDTO [ingredientes=" + ingredientes + ", productType=" + productType + "]";
	}

}
