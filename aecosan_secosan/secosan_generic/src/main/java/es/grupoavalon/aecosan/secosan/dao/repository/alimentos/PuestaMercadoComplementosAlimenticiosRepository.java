package es.grupoavalon.aecosan.secosan.dao.repository.alimentos;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

public interface PuestaMercadoComplementosAlimenticiosRepository extends CrudRepository<PuestaMercadoCAEntity, Long> {

}
