package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class IncOferAlimUMEDTO extends FinanciacionAlimentosDTO {

	@JsonProperty("inscriptionDate")
	@XmlElement(name = "inscriptionDate")
	@BeanToBeanMapping(getValueFrom = "inscriptionDateString")
	private Long inscriptionDate;

	@JsonProperty("email")
	@XmlElement(name = "email")
	@BeanToBeanMapping(getValueFrom = "email")
	private String email;

	@JsonProperty("phone")
	@XmlElement(name = "phone")
	@BeanToBeanMapping(getValueFrom = "phone")
	private String phone;


	@JsonProperty("address")
	@XmlElement(name = "address")
	@BeanToBeanMapping(getValueFrom = "address")
	private String address;

	@JsonProperty("registerNumber")
	@XmlElement(name = "registerNumber")
	@BeanToBeanMapping(getValueFrom = "registerNumber")
	private String registerNumber;


	public Long getInscriptionDate() {
		return inscriptionDate;
	}


	public void setInscriptionDate(Long inscriptionDate) {
		this.inscriptionDate = inscriptionDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
