package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;

public interface EstadoSolicitudDAO {

	EstadoSolicitudEntity addEstadoSolicitud(EstadoSolicitudEntity estadoSolicitudEntity);

	EstadoSolicitudEntity updateEstadoSolicitud(EstadoSolicitudEntity estadoSolicitudEntity);
	
	EstadoSolicitudEntity findEstadoSolicitud(Long pk);

	EstadoSolicitudEntity getStatusBySolicitudeId(Long pk);

	EstadoSolicitudEntity getInitialSolicitudeStatus();

	List<EstadoSolicitudEntity> getStatusByAreaId(Long areaId);
}
