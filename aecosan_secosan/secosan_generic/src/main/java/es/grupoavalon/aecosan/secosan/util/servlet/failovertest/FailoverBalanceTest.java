package es.grupoavalon.aecosan.secosan.util.servlet.failovertest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class FailoverBalanceTest {

	protected static final Logger logger = LoggerFactory
.getLogger(FailoverBalanceTest.class);

	@RequestMapping(value = "test/test.do", method = RequestMethod.GET)
	public ModelAndView showTestPage(HttpServletRequest request,HttpServletResponse response) {
		logger.info("Failover invoked");
		response.setStatus(HttpServletResponse.SC_OK);
		return new ModelAndView("balanceTestPage", "testResult", "SEDE OK");
	}

}
