package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("Areas")
public class AreaCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<AreaEntity, Long> implements GenericCatalogRepository<AreaEntity> {

	@Override
	public List<AreaEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<AreaEntity> getClassType() {

		return AreaEntity.class;
	}

}
