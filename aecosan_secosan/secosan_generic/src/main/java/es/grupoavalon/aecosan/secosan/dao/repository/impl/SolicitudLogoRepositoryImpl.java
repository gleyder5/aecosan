package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudLogoRepository;

@Component
public class SolicitudLogoRepositoryImpl extends AbstractCrudRespositoryImpl<SolicitudLogoEntity, Long> implements SolicitudLogoRepository {

	@Override
	protected Class<SolicitudLogoEntity> getClassType() {
		return SolicitudLogoEntity.class;
	}
}
