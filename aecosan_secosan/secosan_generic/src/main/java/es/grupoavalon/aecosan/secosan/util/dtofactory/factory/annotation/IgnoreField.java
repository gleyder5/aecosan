package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicate that a field must be ignored.<br>
 * Use this annotation if you don't want that the factory read the field and
 * tries to get a value
 * 
 * @author ottoabreu
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface IgnoreField {

	/**
	 * Indicate that the field will not participate in the process of creating
	 * objects in ReverseBeanToBeanFactory
	 * 
	 * @return boolean
	 */
	boolean inReverse() default false;

	/**
	 * Indicate that the field will not participate in the process of creating
	 * objects in BeanToBeanFactory
	 * 
	 * @return boolean
	 */
	boolean inForward() default false;

}
