package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class SolicitudLmrFitosanitariosDTO extends SolicitudDTO {

	@JsonProperty("activeSubstance")
	@XmlElement(name = "activeSubstance")
	@BeanToBeanMapping(getValueFrom = "activeSubstance")
	private String activeSubstance;

	@JsonProperty("originCountry")
	@XmlElement(name = "originCountry")
	@BeanToBeanMapping(getValueFrom = "originCountry")
	private String originCountry;

	@JsonProperty("foods")
	@XmlElement(name = "foods")
	@BeanToBeanMapping(getValueFrom = "foods")
	private String foods;

	@JsonProperty("solicitudePayment")
	@XmlElement(name = "solicitudePayment")
	@BeanToBeanMapping(getValueFrom = "solicitudePayment", generateOther = true)
	private PagoSolicitudDTO solicitudePayment;

	public String getActiveSubstance() {
		return activeSubstance;
	}

	public void setActiveSubstance(String activeSubstance) {
		this.activeSubstance = activeSubstance;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getFoods() {
		return foods;
	}

	public void setFoods(String foods) {
		this.foods = foods;
	}

	public PagoSolicitudDTO getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudDTO solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "SolicitudLmrFitosanitariosDTO [activeSubstance=" + activeSubstance + ", originCountry=" + originCountry + ", foods=" + foods + ", solicitudePayment=" + solicitudePayment
				+ ", getActiveSubstance()=" + getActiveSubstance() + ", getOriginCountry()=" + getOriginCountry() + ", getFoods()=" + getFoods() + ", getSolicitudePayment()=" + getSolicitudePayment()
				+ ", getDocumentacion()=" + getDocumentacion() + ", getId()=" + getId() + ", getFormulario()=" + getFormulario() + ", getFechaCreacion()=" + getFechaCreacion() + ", getEstado()="
				+ ", getSolicitante()=" + getSolicitante() + ", getRepresentante()=" + getRepresentante() + ", getContactData()=" + getContactData() + ", getArea()=" + getArea()
				+ ", getStatus()=" + getStatus() + ", getIdentificadorPeticion()=" + getIdentificadorPeticion() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + "]";
	}

}
