package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias.RegistroIndustriasDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("registroIndustriasRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/registroIndustrias")
public class RegistroIndustriasRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addRegistroIndustrias(@PathParam("user") String user, RegistroIndustriasDTO registroIndustriasDTO) {
		logger.debug("-- Method addRegistroIndustrias POST Init--");
		logger.debug("addRegistroIndustrias || registroIndustriasDTO" + registroIndustriasDTO);

		Response response = addSolicitud(user, registroIndustriasDTO);
		logger.debug("-- Method addRegistroIndustrias POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateRegistroIndustrias(@PathParam("user") String user, RegistroIndustriasDTO registroIndustriasDTO) {
		logger.debug("-- Method updateRegistroIndustrias PUT Init--");
		logger.debug("updateRegistroIndustrias || registroIndustriasDTO" + registroIndustriasDTO);
		Response response = updateSolicitud(user, registroIndustriasDTO);
		logger.debug("-- Method updateRegistroIndustrias PUT End--");
		return response;
	}

}
