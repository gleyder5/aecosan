package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;

public interface ServicioDAO {

	public List<ServicioEntity> findServiceByPayTypeId(Long payTypeId);
}
