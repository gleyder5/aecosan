package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.TipoDocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;

public interface DocumentacionService {

	void deleteDocumentacionById(String user, String id);

	DocumentacionDTO findDocumentationById(String docId);

	List<TipoDocumentacionDTO> findDocumentationByFormId(String formId);

}
