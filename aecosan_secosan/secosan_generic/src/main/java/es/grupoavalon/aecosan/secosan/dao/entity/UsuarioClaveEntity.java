package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Entity
@Table(name = TableNames.TABLA_USUARIO_CLAVE)
@DiscriminatorValue(SecosanConstants.USER_SOLICITANT_ID)
public class UsuarioClaveEntity extends UsuarioCreadorEntity {


	@Column(name = "NIVEL_QQAA")
	private String qaaCitizenLevel;

	public String getQaaCitizenLevel() {
		return qaaCitizenLevel;
	}

	public void setQaaCitizenLevel(String qaaCitizenLevel) {
		this.qaaCitizenLevel = qaaCitizenLevel;
	}

	@Override
	public String toString() {
		return "UsuarioClave [" + super.toString() + ", qaaCitizenLevel=" + qaaCitizenLevel + ", identificationNumber="+this.getIdentificationNumber()+"]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((qaaCitizenLevel == null) ? 0 : qaaCitizenLevel.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UsuarioClaveEntity other = (UsuarioClaveEntity) obj;
		if (qaaCitizenLevel == null) {
			if (other.qaaCitizenLevel != null) {
				return false;
			}
		} else if (!qaaCitizenLevel.equals(other.qaaCitizenLevel)) {
			return false;
		}
		return true;
	}

}
