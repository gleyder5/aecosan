package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.RegistroIndustriasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroIndustriasRepository;

@Repository
public class RegistroIndustriasDAOImpl extends GenericDao implements RegistroIndustriasDAO {

	@Autowired
	private RegistroIndustriasRepository registroIndustriasRepository;

	@Override
	public RegistroIndustriasEntity addRegistroIndustrias(RegistroIndustriasEntity registroIndustrias) {
		RegistroIndustriasEntity registroIndustriasEntity = null;
		try {
			registroIndustriasEntity = registroIndustriasRepository.save(registroIndustrias);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addRegistroIndustrias | registroIndustrias: " + registroIndustrias);
		}
		return registroIndustriasEntity;
	}

	@Override
	public void updateRegistroIndustrias(RegistroIndustriasEntity registroIndustrias) {
		try {
			registroIndustriasRepository.update(registroIndustrias);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateRegistroIndustrias | ERROR: " + registroIndustrias);
		}

	}

}
