package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public abstract class AbstractSolicitudDTO {
	
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("formulario")
	@XmlElement(name = "formulario")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "formulario")
	private TipoSolicitudDTO formulario;

	@JsonProperty("fechaCreacion")
	@XmlElement(name = "fechaCreacion")
	@BeanToBeanMapping(getValueFrom = "fechaCreacionString")
	private Long fechaCreacion;

	@JsonProperty("identificadorPeticion")
	@XmlElement(name = "identificadorPeticion")
	@BeanToBeanMapping(getValueFrom = "identificadorPeticion")
	private String identificadorPeticion;

	@JsonProperty("registrationNumber")
	@XmlElement(name = "registrationNumber")
	@BeanToBeanMapping(getValueFrom = "registrationNumber")
	private String registrationNumber;

	@JsonProperty("status")
	@XmlElement(name = "status")
	@BeanToBeanMapping(getValueFrom = "status", getSecondValueFrom = "id")
	private Long status;

	@JsonProperty("solicitante")
	@XmlElement(name = "solicitante")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "solicitante")
	private UsuarioSolicitanteDTO solicitante;

	@JsonProperty("representante")
	@XmlElement(name = "representante")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "representante")
	private RepresentanteDTO representante;

	@JsonProperty("contactData")
	@XmlElement(name = "contactData")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "contactData")
	private DatosContactoDTO contactData;

	@JsonProperty("area")
	@XmlElement(name = "area")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "area")
	private AreaDTO area;

	@JsonProperty("formularioEspecifico")
	@XmlElement(name = "formularioEspecifico")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "formularioEspecifico")
	private TipoFormularioDTO formularioEspecifico;


	@JsonProperty("solicitaCopiaAutentica")
	@XmlElement(name = "solicitaCopiaAutentica")
	@BeanToBeanMapping(getValueFrom = "solicitaCopiaAutentica")
	private Boolean solicitaCopiaAutentica;

	@JsonProperty("solicitaCopiaAutenticaInfo")
	@XmlElement(name = "solicitaCopiaAutenticaInfo")
	@BeanToBeanMapping(getValueFrom = "solicitaCopiaAutenticaInfo")
	private String solicitaCopiaAutenticaInfo;


	public Boolean getSolicitaCopiaAutentica() {
		return solicitaCopiaAutentica;
	}

	public void setSolicitaCopiaAutentica(Boolean solicitaCopiaAutentica) {
		this.solicitaCopiaAutentica = solicitaCopiaAutentica;
	}

	public String getSolicitaCopiaAutenticaInfo() {
		return solicitaCopiaAutenticaInfo;
	}

	public void setSolicitaCopiaAutenticaInfo(String solicitaCopiaAutenticaInfo) {
		this.solicitaCopiaAutenticaInfo = solicitaCopiaAutenticaInfo;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TipoSolicitudDTO getFormulario() {
		return formulario;
	}

	public void setFormulario(TipoSolicitudDTO formulario) {
		this.formulario = formulario;
	}

	public Long getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Long fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public UsuarioSolicitanteDTO getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(UsuarioSolicitanteDTO solicitante) {
		this.solicitante = solicitante;
	}

	public RepresentanteDTO getRepresentante() {
		return representante;
	}

	public void setRepresentante(RepresentanteDTO representante) {
		this.representante = representante;
	}

	public DatosContactoDTO getContactData() {
		return contactData;
	}

	public void setContactData(DatosContactoDTO contactData) {
		this.contactData = contactData;
	}

	public AreaDTO getArea() {
		return area;
	}

	public void setArea(AreaDTO area) {
		this.area = area;
	}

	public Long getStatus() {
		return status;
	}

	public void setStatus(Long status) {
		this.status = status;
	}

	public String getIdentificadorPeticion() {
		return identificadorPeticion;
	}

	public void setIdentificadorPeticion(String identificadorPeticion) {
		this.identificadorPeticion = identificadorPeticion;
	}

	public TipoFormularioDTO getFormularioEspecifico() {
		return formularioEspecifico;
	}

	public void setFormularioEspecifico(TipoFormularioDTO formularioEspecifico) {
		this.formularioEspecifico = formularioEspecifico;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	@Override
	public String toString() {
		return "AbstractSolicitudDTO [id=" + id + ", formulario=" + formulario + ", fechaCreacion=" + fechaCreacion + ", identificadorPeticion=" + identificadorPeticion + ", status=" + status
				+ ", solicitante=" + solicitante + ", representante=" + representante + ", contactData=" + contactData + ", area=" + area + ", formularioEspecifico=" + formularioEspecifico + "]";
	}
}
