package es.grupoavalon.aecosan.secosan.business.exception;

import es.grupoavalon.aecosan.secosan.util.exception.CodedError;

public class PasswordOperationException extends ServiceException implements CodedError {

	private static final long serialVersionUID = 1L;

	public enum PasswordOperationErrorCodes {
		ERROR_NEW_PASSWORD_EQUAL(-1, "new password equal to previous one"), ERROR_PASSWORD_WEAK(-2, "new password is weak"), ERROR_USER_NOTFOUND(-3, "User not found"), ERROR_USER_AUTH_FAIL(-4,
				"Can't execute password operation, user autentication fail, user or password not match"), GENERAL_PASSWORD_ERROR(-10, "Can't process password, general error");

		private int code;
		private String description;

		private PasswordOperationErrorCodes(int code, String description) {
			this.description = description;
			this.code = code;
		}

		public String getDescription() {
			return description;
		}

		public int getCode() {
			return code;
		}

	}

	private PasswordOperationErrorCodes passwordOperationErrorCodes;

	public PasswordOperationException(String message, PasswordOperationErrorCodes code) {
		super(message);
		this.passwordOperationErrorCodes = code;
	}

	public PasswordOperationException(String message, PasswordOperationErrorCodes code, Exception e) {
		super(message, e);
		this.passwordOperationErrorCodes = code;
	}

	public PasswordOperationException(PasswordOperationErrorCodes code) {
		super(code.description);
		this.passwordOperationErrorCodes = code;
	}

	@Override
	public int getCodeError() {

		return this.passwordOperationErrorCodes.code;
	}

	@Override
	public String getDescription() {
		return this.passwordOperationErrorCodes.getDescription();
	}

}
