package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception;

public class TransformerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 *  NOT_SUPORTED_TYPE="The given object can not be transformed by this transformer because the type is not supported";
	 */
	public static final String NOT_SUPORTED_TYPE="The given object can not be transformed by this transformer because the type is not supported:";
	/**
	 * String GENERAL_ERROR="An error occurs while transforming value: ";
	 */
	public static final String GENERAL_ERROR="An error occurs while transforming value: ";

	public TransformerException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public TransformerException(String arg0) {
		super(arg0);
		
	}

}
