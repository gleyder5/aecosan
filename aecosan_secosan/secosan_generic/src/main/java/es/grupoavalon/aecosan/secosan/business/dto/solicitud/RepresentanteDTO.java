package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class RepresentanteDTO extends AbstractExtendedPersonDataDTO implements IsNulable<RepresentanteDTO> {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "RepresentanteDTO [id=" + id + "]";
	}

	@Override
	public RepresentanteDTO shouldBeNull() {
		
		if (checkNullability()) {
			return null;
		}

		return this;
	}
}
