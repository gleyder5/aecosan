package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.TasaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.ServicioRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TasaModeloRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoFormularioRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Repository
public class TasaDAOImpl extends GenericDao implements TasaDAO {

	@Autowired
	private TipoFormularioRepository tipoFormRepo;

	@Autowired
	private ServicioRepository servicioRepo;

	@Autowired
	private TasaModeloRepository tasaRepo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.aecosan.secosan.dao.impl.TasaDAO#getTasaByFormularioId
	 * (long)
	 */
	@Override
	public TasaModeloEntity getTasaByFormularioId(long formularioId) {
		TasaModeloEntity tasa = null;
		try {
			TipoFormularioEntity tipoFormulario = this.tipoFormRepo.findOne(formularioId);
			if (tipoFormulario != null) {
				tasa = tipoFormulario.getTasaModelo();
			}
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "formularioId |" + formularioId);
		}
		return tasa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.aecosan.secosan.dao.impl.TasaDAO#getTasaBySerivicioId(
	 * long)
	 */
	@Override
	public TasaModeloEntity getTasaBySerivicioId(long servicioId) {
		TasaModeloEntity tasa = null;
		try {
			ServicioEntity serviceEntity = this.servicioRepo.findOne(servicioId);
			if (serviceEntity != null) {
				tasa = serviceEntity.getTasaModelo();
			}
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getTasaBySerivicioId |" + servicioId);
		}
		return tasa;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.aecosan.secosan.dao.impl.TasaDAO#updateTasa(es.grupoavalon
	 * .aecosan.secosan.dao.entity.pago.TasaModeloEntity)
	 */
	@Override
	public void updateTasa(TasaModeloEntity entity) {
		try {
			this.tasaRepo.update(entity);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "updateTasa |" + entity);
		}
	}

	@Override
	public List<TasaModeloEntity> getFilteredTasaList(PaginationParams paginationParams) {
		List<TasaModeloEntity> results = new ArrayList<TasaModeloEntity>();
		try {
			results = tasaRepo.getFilteredTasaList(paginationParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getFilteredTasaList |" + paginationParams + "|ERROR:" + e.getMessage());
		}
		return results;
	}

	@Override
	public int getTotalTasaListRecords(List<FilterParams> filters) {
		int total = 0;
		try {
			// Long userId = Long.parseLong(user);
			total = tasaRepo.getTotalRecords();
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalTasaListRecords | " + filters + "|ERROR:" + e.getMessage());
		}
		return total;
	}

	@Override
	public int getFilteredTasaListRecords(List<FilterParams> filters) {
		int totalFiltered = 0;
		try {
			totalFiltered = tasaRepo.getFilteredTasaListRecords(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getFilteredListRecords |" + filters + "|ERROR:" + e.getMessage());
		}
		return totalFiltered;

	}
}
