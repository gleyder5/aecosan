package es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

import javax.xml.bind.annotation.XmlElement;

public class IdiomasBecasDTO {

    @JsonProperty("nombre")
    @XmlElement(name = "nombre")
    @BeanToBeanMapping(getValueFrom = "nombre")
    private String nombre;


    @JsonProperty("puntos")
    @XmlElement(name = "puntos")
    @BeanToBeanMapping(getValueFrom = "puntos")
    private  Double puntos;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPuntos() {
        return puntos;
    }

    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }
}
