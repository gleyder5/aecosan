package es.grupoavalon.aecosan.secosan.business.dto.interfaces;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;

public interface ServicePersistenceUpdateDTO {

	public void updateSolicitudDTO(String user, SolicitudDTO solicitud);

}
