package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_TIPO_FORMULARIO)
@NamedQueries({
		@NamedQuery(name = NamedQueriesLibrary.GET_SIGN_BY_FORMULARIO_ID, query = "SELECT tp FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD + ".TipoFormularioEntity AS tp WHERE tp.id = :formId") })
public class TipoFormularioEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	@Column(name = "DOC_FIRMADO_TEMPLATE")
	private String singDocumentTemplate;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_SOLICITUD_ID", referencedColumnName = ColumnNames.CATALOG_ID)
	private TipoSolicitudEntity solicitudeType;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "TASA_MODELO_ID", referencedColumnName = "ID", nullable = true)
	private TasaModeloEntity tasaModelo;

	public String getSingDocumentTemplate() {
		return singDocumentTemplate;
	}

	public void setSingDocumentTemplate(String singDocumentTemplate) {
		this.singDocumentTemplate = singDocumentTemplate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	public TipoSolicitudEntity getSolicitudeType() {
		return solicitudeType;
	}

	public void setSolicitudeType(TipoSolicitudEntity solicitudeType) {
		this.solicitudeType = solicitudeType;
	}

	public TasaModeloEntity getTasaModelo() {
		return tasaModelo;
	}

	public void setTasaModelo(TasaModeloEntity tasaModelo) {
		this.tasaModelo = tasaModelo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((solicitudeType == null) ? 0 : solicitudeType.getId().hashCode());
		result = prime * result + ((tasaModelo == null) ? 0 : tasaModelo.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TipoFormularioEntity other = (TipoFormularioEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (solicitudeType == null) {
			if (other.solicitudeType != null) {
				return false;
			}
		} else if (!solicitudeType.getId().equals(other.solicitudeType.getId())) {
			return false;
		}
		if (tasaModelo == null) {
			if (other.tasaModelo != null) {
				return false;
			}
		} else if (!tasaModelo.getId().equals(other.tasaModelo.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "TipoFormularioEntity [id=" + id + ", solicitudeType=" + ((solicitudeType == null) ? "" : solicitudeType.getId()) + "tasaModelo=" + ((tasaModelo == null) ? "" : tasaModelo.getId())
				+ "]";
	}

}
