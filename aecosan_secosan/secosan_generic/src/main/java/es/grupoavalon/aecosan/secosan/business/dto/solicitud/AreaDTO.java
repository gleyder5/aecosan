package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;

public class AreaDTO extends AbstractCatalogDto {

	@Override
	public String toString() {
		return "AreaDTO [id=" + id + "]";
	}

}
