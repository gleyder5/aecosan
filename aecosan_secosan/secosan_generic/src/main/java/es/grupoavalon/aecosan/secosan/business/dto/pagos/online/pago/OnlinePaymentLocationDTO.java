package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class OnlinePaymentLocationDTO {

	@JsonProperty("countryId")
	@XmlElement(name = "countryId")
	@BeanToBeanMapping(getValueFrom = "country", getSecondValueFrom = "idPais")
	private Long countryId;
	@JsonProperty("municipio")
	@XmlElement(name = "municipio")
	@BeanToBeanMapping(getValueFrom = "municipio", getSecondValueFrom = "id")
	private Long municipioId;
	@JsonProperty("province")
	@XmlElement(name = "province")
	@BeanToBeanMapping(getValueFrom = "province", getSecondValueFrom = "id")
	private Long provinceId;
	@JsonProperty("postalCode")
	@XmlElement(name = "postalCode")
	@BeanToBeanMapping
	private String postalCode;

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getMunicipioId() {
		return municipioId;
	}

	public void setMunicipioId(Long municipioId) {
		this.municipioId = municipioId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@Override
	public String toString() {
		return "OnlinePaymentLocationDTO [countryId=" + countryId + ", municipioId=" + municipioId + ", provinceId=" + provinceId + ", postalCode=" + postalCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((municipioId == null) ? 0 : municipioId.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((provinceId == null) ? 0 : provinceId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OnlinePaymentLocationDTO other = (OnlinePaymentLocationDTO) obj;
		if (countryId == null) {
			if (other.countryId != null) {
				return false;
			}
		} else if (!countryId.equals(other.countryId)) {
			return false;
		}
		if (municipioId == null) {
			if (other.municipioId != null) {
				return false;
			}
		} else if (!municipioId.equals(other.municipioId)) {
			return false;
		}
		if (postalCode == null) {
			if (other.postalCode != null) {
				return false;
			}
		} else if (!postalCode.equals(other.postalCode)) {
			return false;
		}
		if (provinceId == null) {
			if (other.provinceId != null) {
				return false;
			}
		} else if (!provinceId.equals(other.provinceId)) {
			return false;
		}
		return true;
	}
}
