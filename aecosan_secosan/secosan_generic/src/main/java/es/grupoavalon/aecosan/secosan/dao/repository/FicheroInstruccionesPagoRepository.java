package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesPagoEntity;

public interface FicheroInstruccionesPagoRepository extends CrudRepository<FicheroInstruccionesPagoEntity, Long> {

	List<FicheroInstruccionesPagoEntity> findAllByArea(Long areaId);

	FicheroInstruccionesPagoEntity findByForm(Long formId);
}
