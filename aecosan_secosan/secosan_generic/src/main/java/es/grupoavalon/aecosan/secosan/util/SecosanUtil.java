package es.grupoavalon.aecosan.secosan.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.util.exception.SecosanException;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

public final class SecosanUtil {
	
	public static final String DATE_FORMAT_YYYY = "yyyy";
	
	public static String sysDate(String format){
		DateFormat dateFormat = new SimpleDateFormat(format);
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	
	public static final String getStringFromContext(String variable){
		String value=null;
		try {
			javax.naming.Context initCtx = new InitialContext();
			javax.naming.Context envCtx = (javax.naming.Context) initCtx.lookup("java:comp/env");
			value = (String) envCtx.lookup(variable);
		} catch (NamingException e) {
			LoggerFactory.getLogger(SecosanUtil.class).warn("La variable  ("+variable+") no puede ser obtenida usando Lookup: "+e.getMessage());
		}
		return value;
	}

	public static <T extends IsNulable<T>> T checkDtoNullability(T dto) {
		if (dto != null) {
			return dto.shouldBeNull();
		} else {
			return null;
		}
	}

	public static String extractTokenFromHeaderValue(String stringToken) {
		String token = "";
		String authorizationSchema = "Bearer";
		if (stringToken != null) {
			if (stringToken.indexOf(authorizationSchema) > -1) {
				token = stringToken.substring(authorizationSchema.length()).trim();
			} else {
				throw new SecosanException("Expected authorization schema not present in header :" + stringToken);
			}
		}
		return token;
	}

	public static String maskCreditCard(String creditCard) {
		int numberOfDigitToMask = Properties.getInt(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_CREDITCARD_TOTAL_DIGITS_MASK);
		return maskString(creditCard, numberOfDigitToMask);
	}

	private static String maskString(String stringToMask, int finalCharacterToMask) {
		StringBuilder maskedString = new StringBuilder(stringToMask);
		if (StringUtils.isNotBlank(stringToMask) && stringToMask.length() > finalCharacterToMask) {
			for (int i = 0; i < finalCharacterToMask; i++) {
				maskedString.replace(i, i + 1, SecosanConstants.CHARACTER_MASK);
			}
		}
		return maskedString.toString();
	}
	
	public static String[] getSeparateAmount(String amount) {
		String[] separateAmount = new String[2];
		int separatorIndex = amount.indexOf(".");
		separateAmount[0] = amount.substring(0, separatorIndex);

		if(amount.substring(separatorIndex+1).length() == 1){
			separateAmount[1] = amount.substring(separatorIndex+1) + "0";
		}else{
			separateAmount[1] = amount.substring(separatorIndex+1);
		}
		
		return separateAmount;
	}

	public static String getUserIdHeaderFromServletRequest(HttpServletRequest req) {
		return req.getHeader(SecosanConstants.REQUEST_HEADER_USER_ID);
	}

	public static String decodeStringIfBase64(String original) {
		String returnSting;
		if (StringUtils.isNotBlank(original) && Base64.isBase64(original) && stringMatchesBase64Pattern(original)) {
			returnSting = new String(Base64.decodeBase64(original));
		} else {
			returnSting = original;
		}
		return returnSting;
	}

	private static boolean stringMatchesBase64Pattern(String string) {
		String base64Pattern = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
		Pattern pat = Pattern.compile(base64Pattern);
		Matcher match = pat.matcher(string);
		return match.matches();
	}

}
