package es.grupoavalon.aecosan.secosan.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.TipoDocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("documentacionRest")
public class DocumentacionRestService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/documentacion";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/documentacion";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "/{user}/{id}")
	public Response deleteDocumentacionById(@PathParam("user") String user, @PathParam("id") String id) {
		logger.debug("-- Method deleteDocumentacionById DELETE Init--");
		Response response = null;
		try {
			logger.debug("-- Method deleteDocumentacionById DELETE deleteDocumentacionById: --");
			bfacade.deleteDocumentacionById(user, id);

			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			logger.error("Error deleteDocumentacionById: | user: " + user + " | id: | " + id + " | " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method deleteDocumentacionById DELETE End--");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "/{docId}")
	public Response downloadDocumentationById(@PathParam("docId") String docId) {

		return this.downloadDocById(docId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "/{docId}")
	public Response downloadDocumentationByIdAdmin(@PathParam("docId") String docId) {

		return this.downloadDocById(docId);
	}

	private Response downloadDocById(String docId) {
		logger.debug("-- Method downloadDocById GET Init --");
		Response response;

		DocumentacionDTO doc;

		try {

			logger.debug("-- Method downloadDocumentationById | docId: {}" , docId);
			doc = bfacade.downloadDocumentationById(docId);

			response = responseFactory.generateOkGenericResponse(doc);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method downloadDocumentationById GET End --");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "/form/{formId}")
	public Response findDocumentationByFormId(@PathParam("formId") String formId) {
		logger.debug("-- Method findDocumentationByFormId GET Init --");
		return findDocByFormId(formId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "/form/{formId}")
	public Response findDocumentationByFormIdAdmin(@PathParam("formId") String formId) {
		logger.debug("-- Method findDocumentationByFormIdAdmin GET Init --");
		return findDocByFormId(formId);
	}


	private Response findDocByFormId(String formId) {
		Response response;

		List<TipoDocumentacionDTO> listDoc = null;

		try {

			logger.debug("-- Method findDocumentationByFormId | formId: {} ",formId);
			listDoc = bfacade.findDocumentationByFormId(formId);

			response = responseFactory.generateOkGenericResponse(listDoc);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method findDocumentationByFormId GET End --");
		return response;
	}


}
