package es.grupoavalon.aecosan.secosan.dao.impl;


import es.grupoavalon.aecosan.secosan.dao.TipoProcedimientoDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoProcedimientoEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoProcedimientoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TipoProcedimientoDAOImpl extends GenericDao implements TipoProcedimientoDAO {

	@Autowired
	private TipoProcedimientoRepository tipoProcedimientoRepository;
	@Override
	public TipoProcedimientoEntity addTipoProcedimientoEntity(TipoProcedimientoEntity TipoProcedimiento) {
		TipoProcedimientoEntity TipoProcedimientoEntity= null;
		try {
			TipoProcedimiento.setActive(true);
			TipoProcedimientoEntity = tipoProcedimientoRepository.save(TipoProcedimiento);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addTipoProcedimiento | TipoProcedimiento: " + TipoProcedimiento);
		}
		return TipoProcedimientoEntity;
	}

	@Override
	public void updateTipoProcedimientoEntity(TipoProcedimientoEntity TipoProcedimiento) {
		try {
			tipoProcedimientoRepository.update(TipoProcedimiento);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateTipoProcedimiento | ERROR: " + TipoProcedimiento);
		}

	}

	@Override
	public List<TipoProcedimientoEntity> getTipoProcedimientoList() {
		List<TipoProcedimientoEntity> tipoProcedimientoEntityList = null;
		try {
			tipoProcedimientoEntityList  = tipoProcedimientoRepository.findAll();
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " ");
		}
		return tipoProcedimientoEntityList;
	}

	@Override
	public void deleteTipoProcedimientoEntity(Long pk) {

		try {
			TipoProcedimientoEntity tipoProcedimientoEntity =  tipoProcedimientoRepository.findOne(pk);
			tipoProcedimientoEntity.setActive(false);
			tipoProcedimientoRepository.update(tipoProcedimientoEntity);

		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " ");
		}
	}
}
