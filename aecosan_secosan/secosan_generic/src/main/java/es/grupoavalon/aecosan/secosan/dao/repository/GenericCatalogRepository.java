package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

public interface GenericCatalogRepository<T> {

	List<T> getCatalogList();

}
