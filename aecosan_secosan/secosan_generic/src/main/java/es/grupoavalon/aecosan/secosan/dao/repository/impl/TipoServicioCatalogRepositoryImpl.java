package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("Servicio")
public class TipoServicioCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<ServicioEntity, Long> implements GenericCatalogRepository<ServicioEntity> {

	@Override
	public List<ServicioEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<ServicioEntity> getClassType() {

		return ServicioEntity.class;
	}

}
