package es.grupoavalon.aecosan.secosan.business.impl;
import es.grupoavalon.aecosan.secosan.business.ResolucionProvisionalService;

import es.grupoavalon.aecosan.secosan.business.dto.ResolucionProvisionalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.*;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.BecasDAO;
import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.TipoFormularioDAO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.report.ReportUtil;
import es.grupoavalon.aecosan.secosan.util.report.Reportable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ResolucionProvisionalServiceImpl extends AbstractManager implements ResolucionProvisionalService {

	private static final String DATE_FORMAT = "yyyyMMddHHmmss";

	private static final String EMPTY_SPACE = ". . . . . . . . . . . . . . . . . ";
	private static final String EMPTY_SPACE_LARGE = ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ";

	@Autowired
	private CatalogDAO catalogueDao;

	@Autowired
	private TipoFormularioDAO tipoFormularioDao;

	@Autowired
	private BusinessFacade bfacade;

	private static final Long STATUS_RESOLUCION = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_RESOLUCION);


	@Override
	public ResolucionProvisionalReportDTO getPDFToSign(String user, String soliciutdId) {
		Map<String, Object> parameters = null;
		ResolucionProvisionalReportDTO impresoFirmadoDTO = new ResolucionProvisionalReportDTO();
		try {
			byte[] pdfReport = null;
			List<Reportable> listOfBeanReport = null;
			Long statusResolucionProvisional =22L;
			List<BecasSolicitanteDTO> becasSolicitanteDTOList =   bfacade.getBecasSolicitantesByStatusDTO(statusResolucionProvisional);
			List<ResolucionProvisionalDTO> resolucionProvisionalDTOList  = new ArrayList<ResolucionProvisionalDTO>();
			for (BecasSolicitanteDTO beca: becasSolicitanteDTOList) {
				ResolucionProvisionalDTO resolucionProvisionalDTO = new ResolucionProvisionalDTO();
				resolucionProvisionalDTO.setApellidoSolicitante(beca.getApellido());
				resolucionProvisionalDTO.setNombreSolicitante(beca.getNombre());
				resolucionProvisionalDTO.setDni(beca.getDNI());
				resolucionProvisionalDTO.setScore(beca.getScore());
				resolucionProvisionalDTOList.add(resolucionProvisionalDTO);
			}
			impresoFirmadoDTO.setResolucionProvisionalDTOList(resolucionProvisionalDTOList);
			listOfBeanReport = generateListOfBeanFromGenericDTO(impresoFirmadoDTO);
			JRBeanCollectionDataSource itemsDatasource = new JRBeanCollectionDataSource(resolucionProvisionalDTOList);
			parameters = new HashMap<String, Object>();
			String imagesPath = getImagePath();
			parameters.put("resolutionList", itemsDatasource);
			parameters.put("imagesPath", imagesPath);
			pdfReport = generatePdfGenericReportFromListOfBean(listOfBeanReport, impresoFirmadoDTO.getFormType(),parameters);

			impresoFirmadoDTO.setPdfB64(getPdfB64ToDTO(pdfReport));
			impresoFirmadoDTO.setReportName("Resolucion Provisional.pdf");
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getPDFToSign | ERROR: " + e.getMessage());
		}
		return impresoFirmadoDTO;
	}


	private String getImagePath() {
		String imagesPath = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_IMAGES_PATH);
		logger.debug("getPDFToSign | imagesPath: {} ", imagesPath);
		return imagesPath;
	}


	private byte[] generatePdfGenericReportFromListOfBean(List<Reportable> listOfBeanReport, Long formType, Map<String, Object> parameters) throws JRException {

		String template = "resolucionProvisional.jrxml";
		String reportToSign = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + template;

		ReportUtil reportUtil = new ReportUtil(reportToSign);
		return reportUtil.generatePdfFromCollectionBean(listOfBeanReport, parameters);
	}

	private List<Reportable> generateListOfBeanFromGenericDTO(ResolucionProvisionalReportDTO report) {
		List<Reportable> lista = new ArrayList<Reportable>();
		SignedReportDTO signedReport = new SignedReportDTO();
		signedReport.setIdentificationNumber(report.getIdentificationNumber());
		signedReport.setFormName("Resolucion Provisional");
		SimpleDateFormat simpleDateFormat   =new SimpleDateFormat("yyyy-mm-dd");

		signedReport.setDate(simpleDateFormat.format(new Date()));
		lista.add(signedReport);
		return lista;
	}

	private String getPdfB64ToDTO(byte[] pdfReport) {
		Base64TobytesArrayTransformer base = new Base64TobytesArrayTransformer();
		return (String) base.transform(pdfReport);
	}

	private static String sysDate() {
		Date sysDate = new Date(System.currentTimeMillis());
		return new SimpleDateFormat(DATE_FORMAT).format(sysDate);
	}

}
