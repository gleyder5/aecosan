package es.grupoavalon.aecosan.secosan.dao.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.BeanNotOfRequiredTypeException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericComplexCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericTaggedCatalog;

@Repository
public class CatalogDAOImpl extends GenericDao implements CatalogDAO {

	@Autowired
	private ApplicationContext ctx;

	@SuppressWarnings("unchecked")
	@Override
	public List<AbstractCatalogEntity> getCatalogItems(String catalogName) {

		GenericCatalogRepository<AbstractCatalogEntity> genericCatalogRepository;

		List<AbstractCatalogEntity> results = new ArrayList<AbstractCatalogEntity>();
		try {
			genericCatalogRepository = ctx.getBean(catalogName, GenericCatalogRepository.class);
			results = genericCatalogRepository.getCatalogList();
		} catch (Exception e) {
			this.handlerException(e, catalogName);
		}

		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AbstractCatalogEntity> getCatalogItems(String catalogName, String parentElement, String parentId) {
		GenericComplexCatalogRepository<AbstractCatalogEntity> genericComplexCatalogRepository;

		List<AbstractCatalogEntity> results = new ArrayList<AbstractCatalogEntity>();
		try {
			genericComplexCatalogRepository = ctx.getBean(catalogName, GenericComplexCatalogRepository.class);
			results = genericComplexCatalogRepository.getCatalogListByParent(parentElement, parentId);
		} catch (Exception e) {
			this.handlerException(e, catalogName);
		}

		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AbstractCatalogEntity> getCatalogItems(String catalogName, String tag) {
		GenericTaggedCatalog<AbstractCatalogEntity> genericTaggedCatalogRepository;

		List<AbstractCatalogEntity> results = new ArrayList<AbstractCatalogEntity>();
		try {
			genericTaggedCatalogRepository = ctx.getBean(catalogName, GenericTaggedCatalog.class);
			
			Map<String, Method> methods=genericTaggedCatalogRepository.getTaggedMethods();
			if (methods.containsKey(tag)) {
				results = genericTaggedCatalogRepository.callMethod(methods.get(tag));
			}
			else {
				throw new DaoException(DaoException.TAGGED_NO_CATALOG_ERROR + catalogName + " | "+tag);
			}

		} catch (Exception e) {
			this.handlerException(e, catalogName);
		}

		return results;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AbstractCatalogEntity getCatalogItem(String catalogName, long catalogId) {

		AbstractCatalogEntity result = null;

		try {
			CrudRepository<AbstractCatalogEntity, Long> genericRepository = ctx.getBean(catalogName, CrudRepository.class);
			result = genericRepository.findOne(catalogId);
		} catch (Exception e) {
			this.handlerException(e, catalogName);
		}

		return result;
	}

	private void handlerException(Exception e, String catalogName){
		String methodNameAndParameter = "getCatalogItems | " + catalogName;
		if (e instanceof NoSuchBeanDefinitionException) {
			handleException(e, DaoException.NOT_EXISTING_CATALOG + methodNameAndParameter);
		}
		else if (e instanceof BeanNotOfRequiredTypeException) {
			handleException(e, DaoException.NOT_COMPLEX_CATALOG + methodNameAndParameter);
		}
		else{
			handleException(e, DaoException.TAGGED_CATALOG_ERROR + methodNameAndParameter + " | " + " ERROR: " + e.getMessage());
		}
	}

}
