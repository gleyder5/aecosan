package es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.PaisEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_REGISTRO_INDUSTRIAS)
public class RegistroIndustriasEntity extends SolicitudEntity {

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PAIS_ID")
	private PaisEntity paisOrigen;

	@Column(name = "RGSEAA")
	private String rgseaa;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_ENVIO")
	private TipoEnvioEntity tipoEnvio;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DATOS_ENVIO_ID")
	private DatosEnvioEntity shippingData;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PAGO_SOLICITUD_ID")
	private PagoSolicitudEntity solicitudePayment;

	public PaisEntity getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(PaisEntity paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public TipoEnvioEntity getTipoEnvio() {
		return tipoEnvio;
	}

	public void setTipoEnvio(TipoEnvioEntity tipoEnvio) {
		this.tipoEnvio = tipoEnvio;
	}

	public DatosEnvioEntity getShippingData() {
		return shippingData;
	}

	public void setShippingData(DatosEnvioEntity shippingData) {
		this.shippingData = shippingData;
	}

	public PagoSolicitudEntity getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudEntity solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {

		return "RegistroIndustriasEntity [paisOrigen=" + paisOrigen + ", rgseaa=" + rgseaa + ", tipoEnvio=" + tipoEnvio + ", shippingData=" + shippingData + ", solicitudePayment=" + solicitudePayment
				+ "]";
	}

}
