package es.grupoavalon.aecosan.secosan.dao.impl;

import es.grupoavalon.aecosan.secosan.dao.BecasDAO;
import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.BecasRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("becasDAO")
public class BecasDAOImpl extends GenericDao implements BecasDAO {
	@Autowired
	private BecasRepository becasRepository;

	@Autowired
	EstadoSolicitudDAO estadoSolicitudDAO;
	@Override
	public BecasEntity addBecasEntity(BecasEntity becas) {
		BecasEntity becasEntity= null;
		try {
			becasEntity = becasRepository.save(becas);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addBecas | becas: " + becas);
		}
		return becasEntity;
	}

	@Override
	public BecasEntity findBecasById(Long Id) {
		BecasEntity becasEntity= null;
		try {
			becasEntity = becasRepository.findOne(Id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " findBeca | id: " + Id);
		}
		return becasEntity;
	}

	@Override
	public int getTotal(List<FilterParams> filters) {
		int total = 0;
		try {
			// Long userId = Long.parseLong(user);
			total = becasRepository.getTotalList(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalListRecords | " + filters + " " + e.getMessage());
		}
		return total;
	}

	@Override
	public void updateBecasEntity(BecasEntity becas) {
		try {
			becasRepository.update(becas);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateBecas | ERROR: " + becas);
		}
	}

	@Override
	public List<BecasEntity> getBecasList() {
		List<BecasEntity> becasEntityList = new ArrayList<BecasEntity>();
		try {
			becasEntityList = becasRepository.findAll();
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateBecas | ERROR: " + becasEntityList);
		}
		return  becasEntityList;
	}

	@Override
	public List<BecasEntity> getBecasListPaginated(PaginationParams paginationParams) {
		/*
		 * Filter example: filter[0][id]=1223 filter[1][formulario]=2
		 * filter[2][fechaCreacionTo]=1461772066276
		 * filter[3][fechaCreacionFrom]=1459893600000 filter[4][estado]=1
		 */
		logger.debug("Paginacion: de " + paginationParams.getStart() + " a " + paginationParams.getLength());
		logger.debug("Filtros: " + paginationParams.getFilters().toString());

		List<BecasEntity> results = new ArrayList<BecasEntity>();
		try {
			results = becasRepository.getBecasSolicitantesPaginated(paginationParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getSolicList |" + paginationParams +" ERROR " + e.getMessage());
		}

		return results;
	}

	@Override
	public List<BecasEntity> getBecasByStatusAndDate(Long statusId, LocalDate date) {
		List<BecasEntity> becasEntityList = new ArrayList<BecasEntity>();
		EstadoSolicitudEntity estadoSolicitudEntity = estadoSolicitudDAO.findEstadoSolicitud(statusId);
		try {
			becasEntityList = becasRepository.findByStatusAndDate(estadoSolicitudEntity,date);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateBecas | ERROR: " + becasEntityList);
		}
		return  becasEntityList;
	}

	@Override
	public List<BecasEntity> getBecasByStatus(Long statusId) {
		List<BecasEntity> becasEntityList = new ArrayList<BecasEntity>();
		EstadoSolicitudEntity estadoSolicitudEntity = estadoSolicitudDAO.findEstadoSolicitud(statusId);
		try {
			becasEntityList = becasRepository.findByStatus(estadoSolicitudEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateBecas | ERROR: " + becasEntityList);
		}
		return  becasEntityList;
	}
}

