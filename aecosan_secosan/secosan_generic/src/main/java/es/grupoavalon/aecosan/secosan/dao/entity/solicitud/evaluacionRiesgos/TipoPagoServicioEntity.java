package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@NamedQueries({
		@NamedQuery(name = NamedQueriesLibrary.GET_PAY_TYPE_SERVICE_BY_PAY_TYPE_ID_AND_SERVICE_ID, query = "FROM " + TableNames.ENTITY_PACKAGE_COMPLEMENTOS_EVALUACION_RIESGOS
				+ ".TipoPagoServicioEntity AS e WHERE e.payType.id = :payType AND e.service.id = :service") })
@Entity
@Table(name = TableNames.TABLA_TIPO_PAGO_SERVICIO)
public class TipoPagoServicioEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.TIPO_PAGO_SERVICIO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.TIPO_PAGO_SERVICIO_SEQUENCE, sequenceName = SequenceNames.TIPO_PAGO_SERVICIO_SEQUENCE, allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "TIPO_PAGO_ID", nullable = false)
	private TipoPagoEntity payType;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "SERVICIO_ID", nullable = false)
	private ServicioEntity service;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPagoEntity getPayType() {
		return payType;
	}

	public void setPayType(TipoPagoEntity payType) {
		this.payType = payType;
	}

	public ServicioEntity getService() {
		return service;
	}

	public void setService(ServicioEntity service) {
		this.service = service;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((payType == null) ? 0 : payType.getId().hashCode());
		result = prime * result + ((service == null) ? 0 : service.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPagoServicioEntity other = (TipoPagoServicioEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (payType == null) {
			if (other.payType.getId() != null)
				return false;
		} else if (!payType.getId().equals(other.payType.getId()))
			return false;
		if (service == null) {
			if (other.service.getId() != null)
				return false;
		} else if (!service.getId().equals(other.service.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {

		String payTypeIdString;
		String serviceIdString;

		if (payType != null && payType.getIdAsString() != null) {
			payTypeIdString = payType.getIdAsString();
		} else {
			payTypeIdString = "";
		}

		if (service != null && service.getIdAsString() != null) {
			serviceIdString = service.getIdAsString();
		} else {
			serviceIdString = "";
		}

		return "TipoPagoServicioEntity [id=" + id + ", payType=" + payTypeIdString + ", service=" + serviceIdString + "]";
	}

}
