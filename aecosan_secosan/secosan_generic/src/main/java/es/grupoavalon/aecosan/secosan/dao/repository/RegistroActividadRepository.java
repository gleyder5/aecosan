package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.RegistroActividadEntity;

public interface RegistroActividadRepository extends CrudRepository<RegistroActividadEntity, Long> {

}
