package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_INGREDIENTES)
public class IngredientesEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.INGREDIENTES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.INGREDIENTES_SEQUENCE, sequenceName = SequenceNames.INGREDIENTES_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "INCLUYE_VITAMINAS")
	private Boolean incluyeVitaminas;

	@Column(name = "INCLUYE_NUEVOS_INGREDIENTES")
	private Boolean incluyeNuevosIngredientes;

	@Column(name = "NUEVO_INGREDIENTE")
	private String nuevoIngrediente;

	@Column(name = "OTRAS_SUSTANCIAS")
	private String otrasSustancias;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SITUACION_ID", nullable = false)
	private SituacionEntity situacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean isIncluyeVitaminas() {
		return incluyeVitaminas;
	}

	public void setIncluyeVitaminas(Boolean incluyeVitaminas) {
		this.incluyeVitaminas = incluyeVitaminas;
	}

	public Boolean isIncluyeNuevosIngredientes() {
		return incluyeNuevosIngredientes;
	}

	public void setIncluyeNuevosIngredientes(Boolean incluyeNuevosIngredientes) {
		this.incluyeNuevosIngredientes = incluyeNuevosIngredientes;
	}

	public String getNuevoIngrediente() {
		return nuevoIngrediente;
	}

	public void setNuevoIngrediente(String nuevoIngrediente) {
		this.nuevoIngrediente = nuevoIngrediente;
	}

	public SituacionEntity getSituacion() {
		return situacion;
	}

	public void setSituacion(SituacionEntity situacion) {
		this.situacion = situacion;
	}

	public String getOtrasSustancias() {
		return otrasSustancias;
	}

	public void setOtrasSustancias(String otrasSustancias) {
		this.otrasSustancias = otrasSustancias;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((incluyeNuevosIngredientes == null) ? 0 : incluyeNuevosIngredientes.hashCode());
		result = prime * result + ((incluyeVitaminas == null) ? 0 : incluyeVitaminas.hashCode());
		result = prime * result + ((nuevoIngrediente == null) ? 0 : nuevoIngrediente.hashCode());
		result = prime * result + ((otrasSustancias == null) ? 0 : otrasSustancias.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IngredientesEntity other = (IngredientesEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (incluyeNuevosIngredientes == null) {
			if (other.incluyeNuevosIngredientes != null)
				return false;
		} else if (!incluyeNuevosIngredientes.equals(other.incluyeNuevosIngredientes))
			return false;
		if (incluyeVitaminas == null) {
			if (other.incluyeVitaminas != null)
				return false;
		} else if (!incluyeVitaminas.equals(other.incluyeVitaminas))
			return false;
		if (nuevoIngrediente == null) {
			if (other.nuevoIngrediente != null)
				return false;
		} else if (!nuevoIngrediente.equals(other.nuevoIngrediente))
			return false;
		if (otrasSustancias == null) {
			if (other.otrasSustancias != null)
				return false;
		} else if (!otrasSustancias.equals(other.otrasSustancias))
			return false;
		return true;
	}

	@Override
	public String toString() {

		String situacionId = "";

		if (situacion != null) {
			situacionId = situacion.getIdAsString();
		}

		return "IngredientesEntity [id=" + id + ", incluyeVitaminas=" + incluyeVitaminas + ", incluyeNuevosIngredientes=" + incluyeNuevosIngredientes + ", nuevoIngrediente=" + nuevoIngrediente
				+ ", otrasSustancias=" + otrasSustancias + ", situacion=" + situacionId + "]";
	}
}
