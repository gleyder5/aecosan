package es.grupoavalon.aecosan.secosan.business.enums;

public interface EnumPrototype {

    String getName();

    String getDescription();

    String getKey();

}
