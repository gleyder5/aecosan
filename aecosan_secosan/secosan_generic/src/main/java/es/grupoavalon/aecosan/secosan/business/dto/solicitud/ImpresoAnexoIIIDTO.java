package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImpresoAnexoIIIDTO extends ImpresoFirmadoDTO {

	@JsonProperty("signer")
	@XmlElement(name = "signer")
	private String signer;

	@JsonProperty("denomination")
	@XmlElement(name = "denomination")
	private String denomination;

	@JsonProperty("referenceNumber")
	@XmlElement(name = "referenceNumber")
	private String referenceNumber;

	@JsonProperty("company")
	@XmlElement(name = "company")
	private String company;

	@JsonProperty("alterMotive")
	@XmlElement(name = "alterMotive")
	private String motivo;

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getReferenceNumber() {
		return referenceNumber;
	}

	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSigner() {
		return signer;
	}

	public void setSigner(String signer) {
		this.signer = signer;
	}

	@Override
	public String toString() {
		return "ImpresoAnexoIIDTO [denomination=" + denomination + ", referenceNumber=" + referenceNumber + ", company=" + company + ", getIdentificationNumber()=" + getIdentificationNumber()
				+ ", getFormId()=" + getFormId() + ", getDate()=" + getDate() + ", getPdfB64()=OMITED" + ", getReportName()=" + getReportName() + ", getFormType()=" + getFormType()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
