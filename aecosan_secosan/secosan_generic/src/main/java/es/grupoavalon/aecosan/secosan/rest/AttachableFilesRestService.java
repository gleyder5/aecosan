package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.AttachableFileDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("attachableFilesRest")
public class AttachableFilesRestService {

	private static final Logger logger = LoggerFactory.getLogger(AttachableFilesRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/attachables/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{fileName}")
	public Response downloadAttachableFileBySolicitudeTypeService(@PathParam("fileName") String fileName) {
		return this.downloadAttachableFileByName(fileName);
	}

	private Response downloadAttachableFileByName(String fileName) {
		Response response = null;

		AttachableFileDTO attachableFileDTO;
		logger.debug("-- Method downloadAttachableFileByName GET Init --");

		try {
			logger.debug("-- Method downloadAttachableFileByName | fileName: {}", fileName);
			attachableFileDTO = bfacade.downloadAttachableFileByName(fileName);
			response = responseFactory.generateOkGenericResponse(attachableFileDTO);
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}

		logger.debug("-- Method downloadAttachableFileByName GET End --");
		return response;
	}

}
