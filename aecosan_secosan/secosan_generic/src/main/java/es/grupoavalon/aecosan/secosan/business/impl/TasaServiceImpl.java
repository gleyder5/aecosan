package es.grupoavalon.aecosan.secosan.business.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.business.TasaService;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.TasaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.TasaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Service
@Transactional
public class TasaServiceImpl extends AbstractManager implements TasaService {

	@Autowired
	private TasaDAO tasaDao;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.aecosan.secosan.business.impl.TasaService#getTasaByFormId
	 * (java.lang.String)
	 */
	@Override
	public TasaDTO getTasaByFormId(long identificadorFormulario) {
		TasaDTO tasaDTO = new TasaDTO();
		try {
			logger.debug("getTasaByFormId || identificadorFormulario:" + identificadorFormulario);
			TasaModeloEntity tasaModeloEntity = this.tasaDao.getTasaByFormularioId(identificadorFormulario);
			logger.debug("getTasaByFormId || tasaModeloEntity:" + tasaModeloEntity);
			tasaDTO = transformEntityToDTO(tasaModeloEntity, TasaDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getTasaByFormId | identificadorFormulario: " + identificadorFormulario + " ERROR: " + e.getMessage());
		}
		return tasaDTO;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.aecosan.secosan.business.impl.TasaService#getTasaByServiceId
	 * (java.lang.String)
	 */
	@Override
	public TasaDTO getTasaByServiceId(long serviceId) {
		TasaDTO tasaDTO = new TasaDTO();
		try {
			logger.debug("getTasaByFormId || serviceId:" + serviceId);
			TasaModeloEntity tasaModeloEntity = this.tasaDao.getTasaBySerivicioId(serviceId);
			logger.debug("getTasaByFormId || tasaModeloEntity:" + tasaModeloEntity);
			tasaDTO = transformEntityToDTO(tasaModeloEntity, TasaDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getTasaByFormId | identificadorFormulario: " + serviceId + " ERROR: " + e.getMessage());
		}
		return tasaDTO;
	}

	@Override
	public List<TasaDTO> getPaginatedTasaList(PaginationParams params){
		 List<TasaDTO> result = new ArrayList<TasaDTO>();
		 try{
			 List<TasaModeloEntity> resultEntities = this.tasaDao.getFilteredTasaList(params);
			result = transformEntityListToDTOList(resultEntities, TasaModeloEntity.class, TasaDTO.class);
		 }catch(Exception e){
			 handleException(e, ServiceException.SERVICE_ERROR + " getPaginatedTasaList | params: " + params + " ERROR: " + e.getMessage());
		 }
		 return result;
	}

	@Override
	public int getTotalTasaListRecords(List<FilterParams> filters) {
		int totalRecords = 0;

		try {
			totalRecords = this.tasaDao.getTotalTasaListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getTotalListRecords | PaginationParams: " + filters + " | ERROR: " + exception.getMessage());
		}

		return totalRecords;
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {

		int totalRecords = 0;
		try {
			totalRecords = this.tasaDao.getFilteredTasaListRecords(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListRecords | ERROR: " + exception.getMessage());
		}
		return totalRecords;
	}

	@Override
	public void updateTasa(TasaDTO tasaDto) {
		try {
			if (tasaDto != null) {
				TasaModeloEntity toUpdate = transformDTOToEntity(tasaDto, TasaModeloEntity.class);
				this.tasaDao.updateTasa(toUpdate);
			}
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "updateTasa | tasaDto: " + tasaDto + " | ERROR: " + e.getMessage());
		}

	}

}
