package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.ProvinciasEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.AbstractTaggedCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericComplexCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericTaggedCatalog;
import es.grupoavalon.aecosan.secosan.dao.repository.ProvinciaRepository;

@Component("Provincia")
public class ProvinciaRepositoryImpl extends AbstractTaggedCatalogRepository<ProvinciasEntity>
		implements GenericComplexCatalogRepository<ProvinciasEntity>, GenericTaggedCatalog<ProvinciasEntity>, ProvinciaRepository {

	@Override
	protected Class<ProvinciasEntity> getClassType() {
		return ProvinciasEntity.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ProvinciasEntity> callMethod(Method method) {

		List<ProvinciasEntity> list = new ArrayList<ProvinciasEntity>();
		try {
			list = (List<ProvinciasEntity>) method.invoke(this);
		} catch (Exception e) {
			throw new DaoException(DaoException.TAGGED_CATALOG_INVOKE_ERROR + e.getMessage(), e);
		}

		return list;
	}

}
