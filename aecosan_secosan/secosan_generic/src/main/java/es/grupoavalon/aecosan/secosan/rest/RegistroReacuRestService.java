package es.grupoavalon.aecosan.secosan.rest;


import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu.RegistroReacuDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("registroReacuRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/registroReacu")
public class RegistroReacuRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addRegistroReacu(@PathParam("user") String user, RegistroReacuDTO registroReacuDTO) {
		logger.debug("-- Method addRegistroReacu POST Init--");
		Response response = addSolicitud(user, registroReacuDTO);
		logger.debug("-- Method addRegistroReacu POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateRegistroReacu(@PathParam("user") String user, RegistroReacuDTO registroReacuDTO) {
		logger.debug("-- Method updateRegistroReacu PUT Init--");
		Response response = updateSolicitud(user, registroReacuDTO);
		logger.debug("-- Method updateRegistroReacu PUT End--");
		return response;
	}

}
