package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import org.apache.commons.lang.StringUtils;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;

public class FormaPagoDTO extends AbstractCatalogDto implements IsNulable<FormaPagoDTO> {

	@Override
	public FormaPagoDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}

	private boolean checkNullability() {
		boolean nullabillity = true;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getId());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getCatalogValue());

		return nullabillity;
	}

}
