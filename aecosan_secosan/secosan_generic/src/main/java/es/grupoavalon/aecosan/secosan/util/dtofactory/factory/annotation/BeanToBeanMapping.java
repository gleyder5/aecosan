package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;
import java.util.List;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.CustomValueTransformer;



/**
 * Annotation that permit make the map between a DTO and an entity<BR>
 * Examples of usage are in the methods<br>
 * <B>For primitives attributes, use Objects as parameters in the setters methods otherwise an exception will rise</B>
 * @author ottoabreu
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
@SuppressWarnings("rawtypes")
public @interface BeanToBeanMapping {
	
	
	/**
	 * Permit to know from what attribute in the entity will be 
	 * extracted the value to be set in the DTO attribute.<BR>
	 * Use attributes name only example:<BR>
	 * Class FooEntity{<BR>
	 * <b>private int myAttribute;</b><BR>
	 * public int getMyAttribute(){}<BR>
	 * public void setMyAttribute(int x){}<BR>
	 * }<BR>
	 * <BR>
	 * <BR>
	 * Class FooDTO{<BR>
	 * <b>DTOMapping(getValueFrom = "myAttribute")</b><BR>
	 * private int myAttributeDTO;<BR>
	 * public int getMyAttribute(){}<BR>
	 * public void setMyAttribute(int x){}<BR>
	 * }
	 * 
	 * @return String
	 */
	String getValueFrom() default "";
	/**
	 * Used when a collection must be generated,<br>
	 * Tells what kind of object must be instantiated inside the collection
	 * Use attributes name only example:<BR>
	 * Class FooEntity{<BR>
	 * <b>private List myAttributeList;</b>// List of instances MyOtherClass.java<BR>
	 * public list getMyAttributeList(){}<BR>
	 * public void setMyAttributeList(List x){}<BR>
	 * }<BR>
	 * <BR>
	 * <BR>
	 * Class FooDTO{<BR>
	 * <b>DTOMapping(getValueFrom = "myAttributeList",generateListOf = MyOtherDTOClass.class )</b><BR>
	 * private List myAttributeDTO;<BR>
	 * public int getMyAttribute(){}<BR>
	 * public void setMyAttribute(int x){}<BR>
	 * }
	 * 
	 * @return Class
	 */
	Class<?> generateListOf() default Object.class;
	/**
	 * When is necessary to obtain a value from another entity
	 * Use attributes name only example:<BR>
	 * Class FooEntity{<BR>
	 * <b>private MyOtherClass myAttribute;</b>//  MyOtherClass.java have a String myOtherAttribute<BR>
	 * public MyOtherClass getMyAttribute(){}<BR>
	 * public void setMyAttribute(MyOtherClass x){}<BR>
	 * }<BR>
	 * <BR>
	 * <BR>
	 * Class FooDTO{<BR>
	 * <b>DTOMapping(getValueFrom = "myAttribute",getSecondValueFrom = "myOtherAttribute" )</b><BR>
	 * private String myOtherAttribute;<BR>
	 * public String getMyOtherAttribute(){}<BR>
	 * public void setMyOtherAttribute(String x){}<BR>
	 * }
	 * @return String
	 */
	String getSecondValueFrom() default "";
	/**
	 * Used to specify the type of the collection param in the DTO setter method<br>
	 * Default {@link java.util.List} 
	 * @return Class<?>
	 */
	Class<? extends Collection> listSetterClass() default List.class;
	
	/**
	 * Used to specify the type of the collection param in the reverse setter method<br>
	 * Default {@link java.util.List} 
	 * @return Class<?>
	 */
	Class<? extends Collection> listReverseSetterClass() default List.class;
	/**
	/**
	 * Allows to use a transformer class to instanciate new objects from others
	 * @return
	 */
	Class<? extends CustomValueTransformer> useCustomTransformer() default CustomValueTransformer.class;
	
	/**
	 * Used when a collection must be generated,<br>
	 * Tells what kind of object must be instantiated inside the collection while executing the reverse method
	 * Use attributes name only example:<BR>
	 * Class FooEntity{<BR>
	 * <b>private List myAttributeList;</b>// List of instances MyOtherClass.java<BR>
	 * public list getMyAttributeList(){}<BR>
	 * public void setMyAttributeList(List x){}<BR>
	 * }<BR>
	 * <BR>
	 * <BR>
	 * Class FooDTO{<BR>
	 * <b>DTOMapping(getValueFrom = "myAttributeList",generateListOf = MyOtherDTOClass.class generateListFrom=MyOtherClass.class )</b><BR>
	 * private List myAttributeDTO;<BR>
	 * public int getMyAttribute(){}<BR>
	 * public void setMyAttribute(int x){}<BR>
	 * }
	 * 
	 * @return Class
	 */
	Class<?> generateListFrom() default Object.class;
	
	
	/**
	 * Used when a another object must be transformed ,<br>
	 * Tells what kind of object must be instantiated and transformed ( will execute the same process of transformation)
	 * Use attributes name only example:<BR>
	 * Class FooEntity{<BR>
	 * <b>private MyOtherClass myotherAttribute;</b>// List of instances MyOtherClass.java<BR>
	 * public list getMyAttributeList(){}<BR>
	 * public void setMyAttributeList(List x){}<BR>
	 * }<BR>
	 * <BR>
	 * <BR>
	 * Class FooDTO{<BR>
	 * <b>DTOMapping(getValueFrom = "myOtherAttribute",generateOther = true )</b><BR>
	 * private MyOtherDTO myAttributeDTO;<BR>
	 * public int getMyAttribute(){}<BR>
	 * public void setMyAttribute(int x){}<BR>
	 * }
	 * 
	 * @return Class
	 */
	boolean generateOther() default false;
	
	
}
