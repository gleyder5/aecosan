package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.HashMap;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.ComplementosAlimenticiosService;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceAddDTO;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceUpdateDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.CeseComercializacionComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.ModificacionDatosComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.PuestaMercadoComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.ComplementosAlimenticiosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class ComplementosAlimenticiosServiceImpl extends AbstractAlimentosService implements ComplementosAlimenticiosService {

	private Map<String, ServicePersistenceAddDTO> solicitudeAddMap;
	private Map<String, ServicePersistenceUpdateDTO> solicitudeUpdateMap;

	@Autowired
	private ComplementosAlimenticiosDAO complementosAlimenticiosDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO complementosAlimenticiosDTO) {
		try {

			solicitudPrepareNullDTO(complementosAlimenticiosDTO);
			solicitudeAddMapInit();
			solicitudeAddMap.get(complementosAlimenticiosDTO.getClass().getSimpleName()).addSolicitudDTO(user, complementosAlimenticiosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + complementosAlimenticiosDTO);
		}
	}

	private void solicitudeAddMapInit() {
		if (solicitudeAddMap == null) {
			solicitudeAddMap = new HashMap<String, ServicePersistenceAddDTO>();
			solicitudeAddMap.put(EntityDtoNames.CESE_COMERCIALIZACION_CA_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addCeseComercializacionComplementosAlimenticiosDTO(user, (CeseComercializacionComplementosAlimenticiosDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.MODIFICACION_DATOS_CA_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addModificacionDatosComplementosAlimenticiosDTO(user, (ModificacionDatosComplementosAlimenticiosDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.PUESTA_MERCADO_CA_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					puestaMercadoPrepareNullDTO(solicitud);
					addPuestaMercadoComplementosAlimenticiosDTO(user, (PuestaMercadoComplementosAlimenticiosDTO) solicitud);
				}
			});
		}
	}

	private void addCeseComercializacionComplementosAlimenticiosDTO(String user, CeseComercializacionComplementosAlimenticiosDTO ceseComercializacionDTO) {
		CeseComercializacionCAEntity ceseComercializacionEntity = generateCeseComercializacionCAEntityFromDTO(ceseComercializacionDTO);
		setUserCreatorByUserId(user, ceseComercializacionEntity);
		prepareAddSolicitudEntity(ceseComercializacionEntity, ceseComercializacionDTO);
		CeseComercializacionCAEntity ceseComercializacionCAEntityNew = complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
		if(ceseComercializacionDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(ceseComercializacionCAEntityNew);
		}

	}

	private void addPuestaMercadoComplementosAlimenticiosDTO(String user, PuestaMercadoComplementosAlimenticiosDTO puestaMercadoDTO) {
		PuestaMercadoCAEntity puestaMercadoEntity = generatePuestaMercadoCAEntityFromDTO(puestaMercadoDTO);
		setUserCreatorByUserId(user, puestaMercadoEntity);
		preparePuestaMercadoEntity(puestaMercadoEntity, puestaMercadoDTO);
		PuestaMercadoEntity puestaMercadoEntityNew = complementosAlimenticiosDAO.addPuestaMercado(puestaMercadoEntity);
		if(puestaMercadoDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(puestaMercadoEntityNew);
		}
	}

	private void addModificacionDatosComplementosAlimenticiosDTO(String user, ModificacionDatosComplementosAlimenticiosDTO modificacionDatosDTO) {
		ModificacionDatosCAEntity modificacionDatosEntity = generateModificacionDatosCAEntityFromDTO(modificacionDatosDTO);
		setUserCreatorByUserId(user, modificacionDatosEntity);
		prepareAddSolicitudEntity(modificacionDatosEntity, modificacionDatosDTO);
		ModificacionDatosCAEntity modificacionDatosCAEntityNew = complementosAlimenticiosDAO.addModificacionDatos(modificacionDatosEntity);
		if(modificacionDatosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(modificacionDatosCAEntityNew);
		}
	}

	@Override
	public void update(String user, SolicitudDTO complementosAlimenticiosDTO) {
		try {
			solicitudPrepareNullDTO(complementosAlimenticiosDTO);
			solicitudeUpdateMapInit();
			solicitudeUpdateMap.get(complementosAlimenticiosDTO.getClass().getSimpleName()).updateSolicitudDTO(user, complementosAlimenticiosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + complementosAlimenticiosDTO);
		}
	}

	private void solicitudeUpdateMapInit() {
		if (solicitudeUpdateMap == null) {
			solicitudeUpdateMap = new HashMap<String, ServicePersistenceUpdateDTO>();
			solicitudeUpdateMap.put(EntityDtoNames.CESE_COMERCIALIZACION_CA_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateCeseComercializacionComplementosAlimenticiosDTO(user, (CeseComercializacionComplementosAlimenticiosDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.MODIFICACION_DATOS_CA_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateModificacionDatosComplementosAlimenticiosDTO(user, (ModificacionDatosComplementosAlimenticiosDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.PUESTA_MERCADO_CA_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					puestaMercadoPrepareNullDTO(solicitud);
					updatePuestaMercadoComplementosAlimenticiosDTO(user, (PuestaMercadoComplementosAlimenticiosDTO) solicitud);
				}
			});
		}
	}

	private void updateCeseComercializacionComplementosAlimenticiosDTO(String user, CeseComercializacionComplementosAlimenticiosDTO ceseComercializacionDTO) {
		CeseComercializacionCAEntity ceseComercializacionEntity = transformDTOToEntity(ceseComercializacionDTO, CeseComercializacionCAEntity.class);
		setUserCreatorByUserId(user, ceseComercializacionEntity);
		prepareUpdateSolicitudEntity(ceseComercializacionEntity, ceseComercializacionDTO);
		complementosAlimenticiosDAO.updateCeseComercializacion(ceseComercializacionEntity);
		if(ceseComercializacionDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(ceseComercializacionEntity);
		}
	}

	private void updateModificacionDatosComplementosAlimenticiosDTO(String user, ModificacionDatosComplementosAlimenticiosDTO modificacionDatosDTO) {
		ModificacionDatosCAEntity modificacionDatosEntity = transformDTOToEntity(modificacionDatosDTO, ModificacionDatosCAEntity.class);
		setUserCreatorByUserId(user, modificacionDatosEntity);
		prepareUpdateSolicitudEntity(modificacionDatosEntity, modificacionDatosDTO);
		complementosAlimenticiosDAO.updateModificacionDatos(modificacionDatosEntity);
		if(modificacionDatosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(modificacionDatosEntity);
		}
	}

	private void updatePuestaMercadoComplementosAlimenticiosDTO(String user, PuestaMercadoComplementosAlimenticiosDTO complementosAlimenticiosDTO) {
		PuestaMercadoCAEntity puestaMercadoEntity = transformDTOToEntity(complementosAlimenticiosDTO, PuestaMercadoCAEntity.class);
		setUserCreatorByUserId(user, puestaMercadoEntity);
		preparePuestaMercadoEntityUpdate(puestaMercadoEntity, complementosAlimenticiosDTO);
		complementosAlimenticiosDAO.updatePuestaMercado(puestaMercadoEntity);
		if(complementosAlimenticiosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(puestaMercadoEntity);
		}
	}

	private CeseComercializacionCAEntity generateCeseComercializacionCAEntityFromDTO(CeseComercializacionComplementosAlimenticiosDTO ceseComercializacionDTO) {
		prepareSolicitudDTO(ceseComercializacionDTO);
		return transformDTOToEntity(ceseComercializacionDTO, CeseComercializacionCAEntity.class);
	}

	private PuestaMercadoCAEntity generatePuestaMercadoCAEntityFromDTO(PuestaMercadoComplementosAlimenticiosDTO puestaMercadoDTO) {
		prepareSolicitudDTO(puestaMercadoDTO);
		return transformDTOToEntity(puestaMercadoDTO, PuestaMercadoCAEntity.class);

	}

	private ModificacionDatosCAEntity generateModificacionDatosCAEntityFromDTO(ModificacionDatosComplementosAlimenticiosDTO modificacionDatosDTO) {
		prepareSolicitudDTO(modificacionDatosDTO);
		return transformDTOToEntity(modificacionDatosDTO, ModificacionDatosCAEntity.class);
	}

	private void puestaMercadoPrepareNullDTO(SolicitudDTO solicitud) {
		PuestaMercadoComplementosAlimenticiosDTO puestaMercadoDTO = (PuestaMercadoComplementosAlimenticiosDTO) solicitud;
		puestaMercadoDTO.setIngredientes(SecosanUtil.checkDtoNullability(puestaMercadoDTO.getIngredientes()));
	}

}
