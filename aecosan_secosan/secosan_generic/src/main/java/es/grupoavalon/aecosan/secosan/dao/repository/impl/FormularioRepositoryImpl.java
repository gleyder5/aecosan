package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.FormularioRepository;

@Component
public class FormularioRepositoryImpl extends AbstractCrudRespositoryImpl<TipoSolicitudEntity, Long>
		implements FormularioRepository {

	@Override
	protected Class<TipoSolicitudEntity> getClassType() {
		return TipoSolicitudEntity.class;
	}
}
