package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionFormularioEntity;

public interface TipoDocumentacionFormularioRepository extends CrudRepository<TipoDocumentacionFormularioEntity, Long> {


}
