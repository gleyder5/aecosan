package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_GRUPO_PACIENTES)
public class GrupoPacientesEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.GRUPO_PACIENTES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.GRUPO_PACIENTES_SEQUENCE, sequenceName = SequenceNames.GRUPO_PACIENTES_SEQUENCE, allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "LACTANTES")
	private Boolean breastfed;

	@Column(name = "NINOS")
	private Boolean child;

	@Column(name = "ADULTOS")
	private Boolean adult;

	public Boolean isBreastfed() {
		return breastfed;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getBreastfed() {
		return breastfed;
	}

	public void setBreastfed(Boolean breastfed) {
		this.breastfed = breastfed;
	}

	public Boolean getChild() {
		return child;
	}

	public void setChild(Boolean child) {
		this.child = child;
	}

	public Boolean getAdult() {
		return adult;
	}

	public void setAdult(Boolean adult) {
		this.adult = adult;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((adult == null) ? 0 : adult.hashCode());
		result = prime * result + ((breastfed == null) ? 0 : breastfed.hashCode());
		result = prime * result + ((child == null) ? 0 : child.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoPacientesEntity other = (GrupoPacientesEntity) obj;
		if (adult == null) {
			if (other.adult != null)
				return false;
		} else if (!adult.equals(other.adult))
			return false;
		if (breastfed == null) {
			if (other.breastfed != null)
				return false;
		} else if (!breastfed.equals(other.breastfed))
			return false;
		if (child == null) {
			if (other.child != null)
				return false;
		} else if (!child.equals(other.child))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GrupoPacientesEntity [id=" + id + ", breastfed=" + breastfed + ", child=" + child + ", adult=" + adult + "]";
	}

}
