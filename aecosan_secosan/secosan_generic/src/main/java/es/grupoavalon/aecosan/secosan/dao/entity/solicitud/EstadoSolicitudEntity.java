package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_ESTADO_SOLICITUD)
@NamedQueries({
		@NamedQuery(name = NamedQueriesLibrary.GET_STATUS_BY_SOLICITUDE_ID, query = "SELECT s.status FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD
				+ ".SolicitudEntity AS s WHERE s.id = :solicitudeId "),
		@NamedQuery(name = NamedQueriesLibrary.GET_STATUS_BY_AREA_ID, query = "SELECT s FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD
				+ ".EstadoSolicitudEntity AS s WHERE s.id in (SELECT ea.status.id FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD
				+ ".EstadosAreasEntity ea WHERE ea.area.id = :area )") })
public class EstadoSolicitudEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.ESTADO_SOLICITUD_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.ESTADO_SOLICITUD_SEQUENCE, sequenceName = SequenceNames.ESTADO_SOLICITUD_SEQUENCE, allocationSize = 1)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoSolicitudEntity other = (EstadoSolicitudEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EstadoSolicitudEntity [id=" + id + "]";
	}

}
