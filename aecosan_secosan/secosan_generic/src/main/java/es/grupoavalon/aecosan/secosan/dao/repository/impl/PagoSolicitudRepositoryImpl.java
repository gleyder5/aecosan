package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.PagoSolicitudRepository;

@Repository
public class PagoSolicitudRepositoryImpl extends AbstractCrudRespositoryImpl<PagoSolicitudEntity, Long> implements PagoSolicitudRepository {

	@Override
	protected Class<PagoSolicitudEntity> getClassType() {

		return PagoSolicitudEntity.class;
	}

	@Override
	public PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeId(long solicitudeId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("solicitudeId", solicitudeId);
		return this.findOneByNamedQuery(NamedQueriesLibrary.GET_PAGO_BY_SOLICITUDE_ID, queryParams);
	}

	@Override
	public PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeRefNum(String refNum) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("refnum", refNum);
		return this.findOneByNamedQuery(NamedQueriesLibrary.GET_PAGO_BY_SOLICITUDE_REFNUMBER, queryParams);
	}

}
