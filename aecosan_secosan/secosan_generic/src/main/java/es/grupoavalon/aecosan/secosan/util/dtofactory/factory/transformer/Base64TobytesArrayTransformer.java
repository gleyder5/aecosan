package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import java.io.File;
import java.io.IOException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.TransformerException;
import es.grupoavalon.aecosan.secosan.util.exception.SecosanException;

public class Base64TobytesArrayTransformer extends CustomValueTransformer {

	@Override
	public Object transform(Object input) {
		String asString = null;
		if (input != null) {

			try {
				if (input instanceof byte[]) {
					asString = fromBytes((byte[]) input);
				} else {

					throw new TransformerException(TransformerException.NOT_SUPORTED_TYPE + input.getClass());
				}
			} catch (Exception e) {
				handleException(e);
			}

		}

		return asString;
	}

	@Override
	public Object reverseTransform(Object input, Class<?> outputType) {
		byte[] output = null;
		String stringInput = (String) input;
		if (input != null) {
			try {
				output = fromString(stringInput);
			} catch (Exception e) {
				handleException(e);
			}
		}
		return output;
	}

	private byte[] fromString(String input) {
		return Base64.decodeBase64(input);
	}

	private String fromBytes(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}

	public static String transformFileToBase64(File file) {
		String base64 = null;
		if (file != null) {
			byte[] fileBytes = transformFileToByte(file);
			base64 = Base64.encodeBase64String(fileBytes);
		}
		return base64;
	}

	public static byte[] transformFileToByte(File file) {
		byte[] fileBytes;
		try {
			// fileBytes =  FileUtils.readFileToString(file).getBytes("ISO-8859-1");
			fileBytes = FileUtils.readFileToByteArray(file);
		} catch (IOException e) {
			throw new SecosanException("Can not create byte array from file.", e);
		}

		return fileBytes;
	}

}
