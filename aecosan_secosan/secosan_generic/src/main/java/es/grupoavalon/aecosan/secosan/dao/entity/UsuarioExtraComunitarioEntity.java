package es.grupoavalon.aecosan.secosan.dao.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_USUARIO_BY_LOGIN, query = "FROM " + TableNames.ENTITY_PACKAGE + ".UsuarioExtraComunitarioEntity AS u WHERE UPPER(u.login) = UPPER(:login) ") })
@Entity
@Table(name = TableNames.TABLA_USUARIO_EXTRACOMUNITARIO)
@DiscriminatorValue(SecosanConstants.USER_NOEU_ID)
public class UsuarioExtraComunitarioEntity extends UsuarioCreadorEntity implements LoginUser {

	@Column(name = ColumnNames.LOGIN_COLUMN, nullable = false, unique = true)
	private String login;
	@Column(name = ColumnNames.PASSWRD_COLUMN, nullable = false)
	private String password;
	@Column(name = ColumnNames.BLOCKED_TIME, nullable = true)
	private Timestamp blockedTime;
	@Column(name = ColumnNames.FAILED_ATTEMPT, nullable = true)
	private Integer failedLoginAttempts;
	@Column(name = ColumnNames.USER_BLOCKED, nullable = true)
	private Boolean userBlocked;
	@Transient
	private boolean firstLogin;


	@Override
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;

	}

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
	}


	public void setFailedLoginAttempts(Integer failedLoginAttempts) {
		this.failedLoginAttempts = failedLoginAttempts;
	}

	@Override
	public Integer getFailedLoginAttempts() {

		return failedLoginAttempts;
	}

	@Override
	public Timestamp getBlockedTime() {

		return this.blockedTime;
	}

	public void setBlockedTime(Timestamp blockedTime) {
		this.blockedTime = blockedTime;
	}

	@Override
	public Boolean getUserBlocked() {
		return userBlocked;
	}

	public void setUserBlocked(Boolean userBlocked) {
		this.userBlocked = userBlocked;
	}

	@Override
	public void increaseBlockAttempt() {
		if (this.failedLoginAttempts == null) {
			this.failedLoginAttempts = 0;
		}
		this.failedLoginAttempts = this.failedLoginAttempts + 1;
	}

	@Override
	public void blockUser() {
		this.userBlocked = true;
		this.blockedTime = new Timestamp(System.currentTimeMillis());
	}

	@Override
	public void setLastSuccessfullLoginNow() {
		if (this.getLastLogin() == null) {
			this.firstLogin = true;
		}
		this.setLastLogin(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	public void resetFailedLoginAttemps() {
		this.failedLoginAttempts = 0;
	}

	@Override
	public void unBlockUser() {
		this.userBlocked = false;
		this.blockedTime = null;
		this.resetFailedLoginAttemps();
	}

	@Override
	public boolean isFirstLogin() {
		return firstLogin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((blockedTime == null) ? 0 : blockedTime.hashCode());
		result = prime * result + ((failedLoginAttempts == null) ? 0 : failedLoginAttempts.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UsuarioExtraComunitarioEntity other = (UsuarioExtraComunitarioEntity) obj;
		if (blockedTime == null) {
			if (other.blockedTime != null) {
				return false;
			}
		} else if (!blockedTime.equals(other.blockedTime)) {
			return false;
		}
		if (failedLoginAttempts == null) {
			if (other.failedLoginAttempts != null) {
				return false;
			}
		} else if (!failedLoginAttempts.equals(other.failedLoginAttempts)) {
			return false;
		}
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		if (password == null) {
			if (other.password != null) {
				return false;
			}
		} else if (!password.equals(other.password)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UsuarioExtraComunitarioEntity [" + super.toString() + ",login=" + login + ", password=" + password + ", blockedTime=" + blockedTime
				+ ", failedLoginAttempts=" + failedLoginAttempts + "]";
	}


}
