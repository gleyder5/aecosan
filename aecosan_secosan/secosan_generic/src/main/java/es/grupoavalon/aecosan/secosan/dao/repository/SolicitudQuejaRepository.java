package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;

public interface SolicitudQuejaRepository extends CrudRepository<SolicitudQuejaEntity, Long> {

}
