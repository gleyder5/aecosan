package es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public abstract class AbstractSolicitudesQuejaSugerenciaDTO extends SolicitudDTO{

	@JsonProperty("unity")
	@XmlElement(name = "unity")
	@BeanToBeanMapping(getValueFrom = "unidad")
	private String unity;

	@JsonProperty("dateTimeIncidence")
	@XmlElement(name = "dateTimeIncidence")
	@BeanToBeanMapping(getValueFrom = "dateTimeIncidenceLong")
	private Long dateTimeIncidence;

	@JsonProperty("motive")
	@XmlElement(name = "motive")
	@BeanToBeanMapping(getValueFrom = "motive")
	private String motive;

	@JsonProperty("comunicationType")
	@XmlElement(name = "comunicationType")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "comunicationType")
	private TipoComunicacionDTO comunicationType;

	public String getUnity() {
		return unity;
	}

	public void setUnity(String unity) {
		this.unity = unity;
	}

	public Long getDateTimeIncidence() {
		return dateTimeIncidence;
	}

	public void setDateTimeIncidence(Long dateTimeIncidence) {
		this.dateTimeIncidence = dateTimeIncidence;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public TipoComunicacionDTO getComunicationType() {
		return comunicationType;
	}

	public void setComunicationType(TipoComunicacionDTO comunicationType) {
		this.comunicationType = comunicationType;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [unity=" + unity + ", dateTimeIncidence=" + dateTimeIncidence + ", motive=" + motive + ", comunicationType=" + comunicationType + "]";
	}

}

