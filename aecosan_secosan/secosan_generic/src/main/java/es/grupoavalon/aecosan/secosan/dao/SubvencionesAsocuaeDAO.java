package es.grupoavalon.aecosan.secosan.dao;


import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;

public interface SubvencionesAsocuaeDAO {

	SubvencionesAsocuaeEntity addSubvencionesAsocuaeEntity(SubvencionesAsocuaeEntity subvencionesAsocuaeEntity);
	void updateSubvencionesAsocuaeEntity(SubvencionesAsocuaeEntity subvencionesAsocuaeEntity);

}
