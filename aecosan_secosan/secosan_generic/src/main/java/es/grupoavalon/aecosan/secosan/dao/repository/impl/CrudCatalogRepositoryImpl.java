package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.dao.repository.CrudCatalogRepository;

@Transactional
public abstract class CrudCatalogRepositoryImpl<T, P extends Serializable> extends AbstractCrudRespositoryImpl<T, Long> implements CrudCatalogRepository<T, Long> {

	private static final String FROM = "FROM ";
	public static final String ORDER_AS = " AS e";
	public static final String ORDER_BY_VALUE = " ORDER BY e.catalogValue";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAllOrderByValue() {
		Query query = this.createQuery(FROM + this.getClassType().getName() + ORDER_AS + ORDER_BY_VALUE);
		return query.getResultList();
	}

	@Override
	public List<T> getCatalogListByParent(String parentElement, String parentId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(parentElement, parentId);

		QueryWhereParts queryParts = new QueryWhereParts();
		queryParts.setPart(parentElement + "=:" + parentElement);

		String query = assambleQuery("FROM " + this.getClassType().getName() + ORDER_AS, queryParts);

		query += ORDER_BY_VALUE;
		return findByQuery(query, queryParams);
	}

}
