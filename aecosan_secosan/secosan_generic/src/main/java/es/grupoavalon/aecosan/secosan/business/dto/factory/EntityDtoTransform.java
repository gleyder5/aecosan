package es.grupoavalon.aecosan.secosan.business.dto.factory;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;

public interface EntityDtoTransform {

	public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory();

}
