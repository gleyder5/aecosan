package es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AbstractPersonData;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_DATOS_ENVIO)
public class DatosEnvioEntity extends AbstractPersonData {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.DATOS_ENVIO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.DATOS_ENVIO_SEQUENCE, sequenceName = SequenceNames.DATOS_ENVIO_SEQUENCE, allocationSize = 1)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DatosEnvioEntity other = (DatosEnvioEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DatosContactoEntity [id=" + id + "]";
	}

}
