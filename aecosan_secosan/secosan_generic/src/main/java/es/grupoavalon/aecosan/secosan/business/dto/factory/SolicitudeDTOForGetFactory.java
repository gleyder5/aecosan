package es.grupoavalon.aecosan.secosan.business.dto.factory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.*;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.CeseComercializacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos.ModificacionDatosAlimentosGruposDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos.PuestaMercadoAlimentosGruposDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.ModificacionDatosComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.PuestaMercadoComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos.EvaluacionRiesgosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.AlterOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.BajaOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.IncOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudSugerenciaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroAguas.RegistroAguasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias.RegistroIndustriasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu.RegistroReacuDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesAsocuaeDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesJuntasDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.ProcedimientoGeneralEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesJuntasConsumoEntity;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;

/**
 * 
 * @author jmanuel.cabeza
 *
 */
public class SolicitudeDTOForGetFactory {

	private static DtoEntityFactory<SolicitudEntity, SolicitudDTO> dtoSolicitudeFactory;
	private static Map<String, EntityDtoTransform> solicitudeMap;

	private SolicitudeDTOForGetFactory() {

	}

	public static SolicitudDTO instanciateDTOFromEntity(SolicitudEntity solicitudEntity) {
		SolicitudDTO solicitudDTO = generateSolicitudDTO(solicitudEntity);
		solicitudDTO.setDocumentacion(solicitudeSetDocumentationToNull(solicitudEntity));
		return solicitudDTO;
	}

	@SuppressWarnings("unchecked")
	private static List<DocumentacionDTO> solicitudeSetDocumentationToNull(SolicitudEntity solicitudEntity) {
		List<DocumentacionDTO> documentationList = null;
		if (solicitudEntity.getDocumentation() != null && !solicitudEntity.getDocumentation().isEmpty()) {
			DtoEntityFactory<DocumentacionEntity, DocumentacionDTO> dtoDocumentationfactory = DtoEntityFactory.getInstance(DocumentacionEntity.class, DocumentacionDTO.class);
			documentationList = dtoDocumentationfactory.generateListDTOFromEntity(solicitudEntity.getDocumentation());
		}
		return documentationList;
	}


	private static SolicitudDTO generateSolicitudDTO(SolicitudEntity solicitudEntity) {
		solicitudeMapInit();
		Class<?> clazz = solicitudEntity.getClass();
		final  String simpleName = clazz.getSimpleName();
		dtoSolicitudeFactory = solicitudeMap.get(simpleName).getDtoEntityFactory();
		return dtoSolicitudeFactory.generateDTOFromEntity(solicitudEntity);

	}

	@SuppressWarnings("unchecked")
	private static void solicitudeMapInit() {
		if (solicitudeMap == null) {
			solicitudeMap = new HashMap<String, EntityDtoTransform>();
			solicitudeMap.put(EntityDtoNames.CESE_COMERCIALIZACION_CA_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(CeseComercializacionCAEntity.class, CeseComercializacionDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.CESE_COMERCIALIZACION_AG_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(CeseComercializacionAGEntity.class, CeseComercializacionDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.PUESTA_MERCADO_AG_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(PuestaMercadoAGEntity.class, PuestaMercadoAlimentosGruposDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.PUESTA_MERCADO_CA_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(PuestaMercadoCAEntity.class, PuestaMercadoComplementosAlimenticiosDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.MODIFICACION_DATOS_AG_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(ModificacionDatosAGEntity.class, ModificacionDatosAlimentosGruposDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.MODIFICACION_DATOS_CA_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(ModificacionDatosCAEntity.class, ModificacionDatosComplementosAlimenticiosDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SOLICITUD_QUEJA_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SolicitudQuejaEntity.class, SolicitudQuejaDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SOLICITUD_SUGERENCIA_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SolicitudSugerenciaEntity.class, SolicitudSugerenciaDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SOLICITUD_LOGO_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SolicitudLogoEntity.class, SolicitudLogoDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.EVALUACION_RIESGOS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(EvaluacionRiesgosEntity.class, EvaluacionRiesgosDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SOLICITUD_LMR_FITOSANITARIOS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SolicitudLmrFitosanitariosEntity.class, SolicitudLmrFitosanitariosDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.INC_OFER_ALIM_UME_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(IncOferAlimUMEEntity.class, IncOferAlimUMEDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.ALTER_OFER_ALIM_UME_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(AlterOferAlimUMEEntity.class, AlterOferAlimUMEDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.BAJA_OFER_ALIM_UME_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(BajaOferAlimUMEEntity.class, BajaOferAlimUMEDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.REGISTRO_AGUAS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(RegistroAguasEntity.class, RegistroAguasDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.REGISTRO_INDUSTRIAS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(RegistroIndustriasEntity.class, RegistroIndustriasDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.PROCEDIMIENTO_GENERAL_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(ProcedimientoGeneralEntity.class, ProcedimientoGeneralDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.REGISTRO_REACU_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(RegistroReacuEntity.class, RegistroReacuDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SUBVENCIONES_ASOCUAE_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SubvencionesAsocuaeEntity.class, SubvencionesAsocuaeDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.SUBVENCIONES_JUNTAS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(SubvencionesJuntasConsumoEntity.class,SubvencionesJuntasDTO.class);
				}
			});
			solicitudeMap.put(EntityDtoNames.BECAS_ENTITY, new EntityDtoTransform() {
				@Override
				public DtoEntityFactory<SolicitudEntity, SolicitudDTO> getDtoEntityFactory() {
					return DtoEntityFactory.getInstance(BecasEntity.class, BecasDTO.class);
				}
			});

		}
	}

}
