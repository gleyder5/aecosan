package es.grupoavalon.aecosan.secosan.business.dto;

import org.apache.commons.lang.StringUtils;

public class PaisDTO extends AbstractCatalogDto implements IsNulable<PaisDTO> {


	@Override
	public PaisDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		} else {
			return this;
		}
	}

	private boolean checkNullability() {
		boolean nullabillity = true;

		nullabillity = nullabillity && StringUtils.isEmpty(this.getId());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getCatalogValue());

		return nullabillity;
	}


}
