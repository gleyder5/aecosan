package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesAsocuaeDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("subvencionesAsocuaeRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/subvencionesAsocuae")
public class SubvencionesAsocuaeRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addSubvencionesAsocuae(@PathParam("user") String user, SubvencionesAsocuaeDTO subvencionesAsocuae) {
		logger.debug("-- Method addSubvencionesAsocuae POST Init--");
		Response response = addSolicitud(user, subvencionesAsocuae);
		logger.debug("-- Method addSubvencionesAsocuae POST End--");
		return response;
	}
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateSubvencionesAsocuae(@PathParam("user") String user, SubvencionesAsocuaeDTO subvencionesAsocuae) {
			logger.debug("-- Method updateSubvencionesAsocuae PUT Init--");
		Response response = updateSolicitud(user, subvencionesAsocuae);
		logger.debug("-- Method updateSubvencionesAsocuae PUT End--");
		return response;
	}

}
