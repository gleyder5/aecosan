package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class PropuestaDTO {
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("typeCode")
	@XmlElement(name = "typeCode")
	@BeanToBeanMapping(getValueFrom = "typeCode")
	private String typeCode;

	@JsonProperty("subtypeCode")
	@XmlElement(name = "subtypeCode")
	@BeanToBeanMapping(getValueFrom = "subtypeCode")
	private String subtypeCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getSubtypeCode() {
		return subtypeCode;
	}

	public void setSubtypeCode(String subtypeCode) {
		this.subtypeCode = subtypeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((subtypeCode == null) ? 0 : subtypeCode.hashCode());
		result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropuestaDTO other = (PropuestaDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (subtypeCode == null) {
			if (other.subtypeCode != null)
				return false;
		} else if (!subtypeCode.equals(other.subtypeCode))
			return false;
		if (typeCode == null) {
			if (other.typeCode != null)
				return false;
		} else if (!typeCode.equals(other.typeCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PropuestaDTO [id=" + id + ", typeCode=" + typeCode + ", subtypeCode=" + subtypeCode + "]";
	}

}
