package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;

public interface TipoDocumentacionRepository extends CrudRepository<TipoDocumentacionEntity, Long> {


}
