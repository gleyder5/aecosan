package es.grupoavalon.aecosan.secosan.util;

public enum SecosanActivities {

	SUCCESSFULL_LOGIN(1, " LOGIN EXITOSO"), UNSUCCESSFULL_LOGIN(2, "LOGIN FALLIDO"), SUCCESSFULL_CLAVE_LOGIN(3, "CLAVE LOGIN EXITOSO"), SUCCESSFULL_ADMIN_LOGIN(4, "LOGIN ZONA ADMIN EXITOSO"), UNSUCCESSFULL_ADMIN_LOGIN(
			5, "LOGIN ZONA ADMIN FALLIDO"), SUCCESSFULL_ADMIN_CLAVE_LOGIN(6, "CLAVE ADMIN LOGIN EXITOSO"), ;
	private long id;

	private String description;

	private SecosanActivities(long id, String description) {
		this.id = id;
		this.description = description;
	}

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

}
