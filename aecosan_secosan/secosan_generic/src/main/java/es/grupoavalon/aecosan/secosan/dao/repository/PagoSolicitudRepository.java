package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;

public interface PagoSolicitudRepository extends CrudRepository<PagoSolicitudEntity, Long> {

	PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeId(long solicitudeId);

	PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeRefNum(String refNum);

}
