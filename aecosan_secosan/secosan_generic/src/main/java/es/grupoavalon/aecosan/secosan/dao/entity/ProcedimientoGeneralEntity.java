package es.grupoavalon.aecosan.secosan.dao.entity;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import javax.persistence.*;

@Entity
@Table(name = TableNames.TABLA_PROCEDIMIENTO_GENERAL)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = ColumnNames.SOLICITUDE_TYPE, discriminatorType = DiscriminatorType.INTEGER)
public class ProcedimientoGeneralEntity extends SolicitudEntity {

	@Column(name = "ASUNTO")
	private String asunto;

	@Column(name = "EXPONE")
	private String expone;

	@Column(name = "SOLICITA")
	private String solicita;

	@Column(name = "TIPO")
	private String tipo;

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getExpone() {
		return expone;
	}

	public void setExpone(String expone) {
		this.expone = expone;
	}

	public String getSolicita() {
		return solicita;
	}

	public void setSolicita(String solicita) {
		this.solicita = solicita;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
}
