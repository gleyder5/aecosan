package es.grupoavalon.aecosan.secosan.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.CatalogDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("catalogRest")
public class CatalogeRestService {
	
	private static final String DEFAULT_REST_URL =SecosanConstants.SECURE_CONTEXT + "/catalog/";
	
	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/catalog/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL+"{catalogueName}")
	public Response listCatalogeItems(@PathParam("catalogueName") String catalogName) {

		return catalogueItems(catalogName);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"{catalogueName}")
	public Response listCatalogeItemsAdmin(@PathParam("catalogueName") String catalogName) {

		return catalogueItems(catalogName);
	}
	
	private Response catalogueItems(String catalogName){
		Response response;
		List<CatalogDTO> data;
		try {
			data = bfacade.getCatalogItems(catalogName);
			response = responseFactory.generateCatalogResponse(data);
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}

		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL+"{catalogueName}/{parentEntity}/{parentId}")
	public Response listCatalogeItemsByParent(@PathParam("catalogueName") String catalogName, @PathParam("parentEntity") String parentEntity, @PathParam("parentId") String parentId) {

		return catalogueItemsByParent(catalogName, parentEntity, parentId);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"{catalogueName}/{parentEntity}/{parentId}")
	public Response listCatalogeItemsByParentAdmin(@PathParam("catalogueName") String catalogName, @PathParam("parentEntity") String parentEntity, @PathParam("parentId") String parentId) {

		return catalogueItemsByParent(catalogName, parentEntity, parentId);
	}
	
	private Response catalogueItemsByParent(String catalogName,  String parentEntity,  String parentId){
		Response response;
		List<CatalogDTO> data;
		try {
			data = bfacade.getCatalogItems(catalogName, parentEntity, parentId);
			response = responseFactory.generateCatalogResponse(data);
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}

		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL+"{catalogueName}/{tag}")
	public Response listCatalogeItemsByTag(@PathParam("catalogueName") String catalogName, @PathParam("tag") String tag) {

		
		return catalogueItemByTag(catalogName, tag);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"{catalogueName}/{tag}")
	public Response listCatalogeItemsByTagAdmin(@PathParam("catalogueName") String catalogName, @PathParam("tag") String tag) {
		return catalogueItemByTag(catalogName, tag);
	}

	private Response catalogueItemByTag(String catalogName, String tag){
		Response response;
		List<CatalogDTO> data;
		try {
			data = bfacade.getCatalogItems(catalogName, tag);
			response = responseFactory.generateCatalogResponse(data);
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}

		return response;
	}
}
