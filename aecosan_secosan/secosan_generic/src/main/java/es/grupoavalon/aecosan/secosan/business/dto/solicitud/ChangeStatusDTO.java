package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ChangeStatusDTO {

	@JsonProperty("solicitudeId")
	@XmlElement(name = "solicitudeId")
	private String solicitudeId;

	@JsonProperty("statusId")
	@XmlElement(name = "statusId")
	private String statusId;

	@JsonProperty("statusText")
	@XmlElement(name = "statusText")
	private String statusText;

	@JsonProperty("fileName")
	@XmlElement(name = "fileName")
	private String fileName;

	@JsonProperty("fileB64")
	@XmlElement(name = "fileB64")
	private String fileB64;


	@JsonProperty("fileCarteraName")
	@XmlElement(name = "fileCarteraName")
	private String fileCarteraName;

	@JsonProperty("fileCarteraB64")
	@XmlElement(name = "fileCarteraB64")
	private String fileCarteraB64;


	@JsonProperty("fileCopiaAutenticaName")
	@XmlElement(name = "fileCopiaAutenticaName")
	private String fileCopiaAutenticaName;

	@JsonProperty("fileCopiaAutenticaB64")
	@XmlElement(name = "fileCopiaAutenticaB64")
	private String fileCopiaAutenticaB64;

	@JsonProperty("typeSubmit")
	@XmlElement(name = "typeSubmit")
	private String typeSubmit;

	@JsonProperty("emailRemision")
	@XmlElement(name = "emailRemision")
	private String emailRemision;

	@JsonProperty("otraTitulacion")
	@XmlElement(name = "otraTitulacion")
	private String otraTitulacion;

	@JsonProperty("otraTitulacionPuntos")
	@XmlElement(name = "otraTitulacionPuntos")
	private Double otraTitulacionPuntos;

	@JsonProperty("languages")
	@XmlElement(name = "languages")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "languages")
	private List<IdiomasBecasDTO> idiomas;

	@JsonProperty("score")
	@XmlElement(name = "score")
	private Integer score;

	@JsonProperty("puntosOfimatica")
	@XmlElement(name = "puntosOfimatica")
	private Integer puntosOfimatica;

	@JsonProperty("puntosVoluntariado")
	@XmlElement(name = "puntosVoluntariado")
	private Integer puntosVoluntariado;

	@JsonProperty("puntosEntrevista")
	@XmlElement(name = "puntosEntrevista")
	private Integer puntosEntrevista;

	@JsonProperty("puntosExpediente")
	@XmlElement(name = "puntosExpediente")
	private Integer puntosExpediente;

	@JsonProperty("area")
	@XmlElement(name = "area")
	private String area;


	@JsonProperty("onlySendEmail")
	@XmlElement(name = "onlySendEmail")
	private Boolean onlySendEmail;

	public String getFileCopiaAutenticaName() {
		return fileCopiaAutenticaName;
	}

	public void setFileCopiaAutenticaName(String fileCopiaAutenticaName) {
		this.fileCopiaAutenticaName = fileCopiaAutenticaName;
	}

	public String getFileCopiaAutenticaB64() {
		return fileCopiaAutenticaB64;
	}

	public void setFileCopiaAutenticaB64(String fileCopiaAutenticaB64) {
		this.fileCopiaAutenticaB64 = fileCopiaAutenticaB64;
	}

	public String getSolicitudeId() {
		return solicitudeId;
	}

	public void setSolicitudeId(String solicitudeId) {
		this.solicitudeId = solicitudeId;
	}

	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileB64() {
		return fileB64;
	}

	public void setFileB64(String fileB64) {
		this.fileB64 = fileB64;
	}

	public String getTypeSubmit() {
		return typeSubmit;
	}

	public void setTypeSubmit(String typeSubmit) {
		this.typeSubmit = typeSubmit;
	}

	public String getOtraTitulacion() {
		return otraTitulacion;
	}

	public void setOtraTitulacion(String otraTitulacion) {
		this.otraTitulacion = otraTitulacion;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Integer getPuntosOfimatica() {
		return puntosOfimatica;
	}

	public void setPuntosOfimatica(Integer puntosOfimatica) {
		this.puntosOfimatica = puntosOfimatica;
	}

	public Integer getPuntosVoluntariado() {
		return puntosVoluntariado;
	}

	public void setPuntosVoluntariado(Integer puntosVoluntariado) {
		this.puntosVoluntariado = puntosVoluntariado;
	}

	public Integer getPuntosEntrevista() {
		return puntosEntrevista;
	}

	public void setPuntosEntrevista(Integer puntosEntrevista) {
		this.puntosEntrevista = puntosEntrevista;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public List<IdiomasBecasDTO> getIdiomas() {
		return idiomas;
	}

	public void setIdiomas(List<IdiomasBecasDTO> idiomas) {
		this.idiomas = idiomas;
	}

	public Integer getPuntosExpediente() {
		return puntosExpediente;
	}

	public void setPuntosExpediente(Integer puntosExpediente) {
		this.puntosExpediente = puntosExpediente;
	}

	public Double getOtraTitulacionPuntos() {
		return otraTitulacionPuntos;
	}

	public void setOtraTitulacionPuntos(Double otraTitulacionPuntos) {
		this.otraTitulacionPuntos = otraTitulacionPuntos;
	}

	public String getFileCarteraName() {
		return fileCarteraName;
	}

	public void setFileCarteraName(String fileCarteraName) {
		this.fileCarteraName = fileCarteraName;
	}

	public String getFileCarteraB64() {
		return fileCarteraB64;
	}

	public String getEmailRemision() {
		return emailRemision;
	}

	public void setEmailRemision(String emailRemision) {
		this.emailRemision = emailRemision;
	}

	public void setFileCarteraB64(String fileCarteraB64) {
		this.fileCarteraB64 = fileCarteraB64;
	}

	public Boolean getOnlySendEmail() {
		return onlySendEmail;
	}

	public void setOnlySendEmail(Boolean onlySendEmail) {
		this.onlySendEmail = onlySendEmail;
	}

	public byte[] getFileCarteraBytes() {
		byte[] fileBytes = null;
		if (StringUtils.isNotBlank(this.fileCarteraB64)) {
			fileBytes = Base64.decodeBase64(this.fileCarteraB64);
		}
		return fileBytes;
	}

	public byte[] getFileBytes() {
		byte[] fileBytes = null;
		if (StringUtils.isNotBlank(this.fileB64)) {
			fileBytes = Base64.decodeBase64(this.fileB64);
		}
		return fileBytes;
	}

	@Override
	public String toString() {
		return "ChangeStatusDTO [solicitudeId=" + solicitudeId + ", statusId=" + statusId + ", statusText=" + statusText + ", fileName=" + fileName + ", typeSubmit="+typeSubmit+ "]";
	}

}
