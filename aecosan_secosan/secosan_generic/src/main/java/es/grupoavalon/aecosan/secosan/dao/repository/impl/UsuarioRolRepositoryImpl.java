package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.AbstractTaggedCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericComplexCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericTaggedCatalog;
import es.grupoavalon.aecosan.secosan.dao.repository.TaggedCatalog;
import es.grupoavalon.aecosan.secosan.dao.repository.UsuarioRolRepository;

import javax.persistence.PersistenceContext;

@Component("UserRole")
public class UsuarioRolRepositoryImpl extends AbstractTaggedCatalogRepository<UsuarioRolEntity> implements UsuarioRolRepository, GenericTaggedCatalog<UsuarioRolEntity>,
		GenericComplexCatalogRepository<UsuarioRolEntity> {

	@Override
	protected Class<UsuarioRolEntity> getClassType() {
		return UsuarioRolEntity.class;
	}

	@Override
	public UsuarioRolEntity findUsuarioRol(long pk) {

		return findOne(pk);

	}

	@Override
	public UsuarioRolEntity findOne(long pk) {
		return findOne(pk);
	}

	@Override
	public List<UsuarioRolEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	@TaggedCatalog(tagMethod = "internalRoles")
	public List<UsuarioRolEntity> getInternalRoles() {
		return findByNamedQuery(NamedQueriesLibrary.GET_ROLES_INTERNAL);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioRolEntity> callMethod(Method method) throws DaoException {
		List<UsuarioRolEntity> list = new ArrayList<UsuarioRolEntity>();
		try {
			list = (List<UsuarioRolEntity>) method.invoke(this);
		} catch (Exception e) {
			throw new DaoException(DaoException.TAGGED_CATALOG_INVOKE_ERROR + e.getMessage(), e);
		}
		return list;
	}

}
