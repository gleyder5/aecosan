package es.grupoavalon.aecosan.secosan.business.mail.dto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import org.apache.commons.lang.StringEscapeUtils;

public class SolicitudeNotificationDTO {

	private String to;
	private String idRequest;
	private String date;

	private String userName;
	private String userLastName;
	private String userIdentificationNumber;
	private String userAddress;
	private String userPhone;
	private String userEmail;
	private List<DocumentacionDTO> documentacionDTOList;

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = StringEscapeUtils.escapeHtml(to);
	}

	public String getDate() {
		return date;
	}

	public void setDate(Long datelong) {
		Date date = new Date(datelong);
		SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
		this.date = df2.format(date);
	}

	public String getIdRequest() {
		return idRequest;
	}

	public void setIdRequest(String idRequest) {
		this.idRequest = StringEscapeUtils.escapeHtml(idRequest);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = StringEscapeUtils.escapeHtml(userName);
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = StringEscapeUtils.escapeHtml(userLastName);
	}

	public String getUserIdentificationNumber() {
		return userIdentificationNumber;
	}

	public void setUserIdentificationNumber(String userIdentificationNumber) {
		this.userIdentificationNumber = StringEscapeUtils.escapeHtml(userIdentificationNumber);
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = StringEscapeUtils.escapeHtml(userAddress);
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = StringEscapeUtils.escapeHtml(userPhone);
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = StringEscapeUtils.escapeHtml(userEmail);
	}

	public List<DocumentacionDTO> getDocumentacionDTOList() {
		return documentacionDTOList;
	}

	public void setDocumentacionDTOList(List<DocumentacionDTO> documentacionDTOList) {
		this.documentacionDTOList = documentacionDTOList;
	}
}
