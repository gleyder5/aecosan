package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.RegistroReacuService;


import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu.RegistroReacuDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.RegistroReacuDAO;

import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistroReacuServiceImpl extends AbstractSolicitudService implements RegistroReacuService {

	@Autowired
	private RegistroReacuDAO registroReacuDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO registroReacuDTO) {
		try {
			solicitudPrepareNullDTO(registroReacuDTO);
			prepareRegistroReacuNullability((RegistroReacuDTO) registroReacuDTO);
			addRegistroReacuEntity(user, (RegistroReacuDTO) registroReacuDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + registroReacuDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO registroReacuDTO) {
		try {
			solicitudPrepareNullDTO(registroReacuDTO);
			prepareRegistroReacuNullability((RegistroReacuDTO) registroReacuDTO);
			updateProcediminetoGeneral(user, (RegistroReacuDTO) registroReacuDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + registroReacuDTO);
		}
	}

	private void addRegistroReacuEntity(String user, RegistroReacuDTO registroReacuDTO) {
		final UbicacionGeograficaDTO location = registroReacuDTO.getLugarOMedio().getLocation();
		if (location != null) {
			location.setProvince(SecosanUtil.checkDtoNullability(location.getProvince()));
			location.setMunicipio(SecosanUtil.checkDtoNullability(location.getMunicipio()));
		}
		registroReacuDTO.getLugarOMedio().setLocation(location);

		RegistroReacuEntity registroReacuEntity	=	generateRegistroReacuEntityFromDTO(registroReacuDTO);
		setUserCreatorByUserId(user, registroReacuEntity);
		prepareAddSolicitudEntity(registroReacuEntity,registroReacuDTO);
		RegistroReacuEntity registroReacuEntityNew	=	registroReacuDAO.addRegistroReacuEntity(registroReacuEntity);
		if(registroReacuDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroReacuEntityNew);
		}
	}

	private void updateProcediminetoGeneral(String user, RegistroReacuDTO registroReacuDTO) {
		RegistroReacuEntity registroReacuEntity= transformDTOToEntity(registroReacuDTO, RegistroReacuEntity.class);
		setUserCreatorByUserId(user, registroReacuEntity);
		prepareUpdateSolicitudEntity(registroReacuEntity, registroReacuDTO);
		registroReacuDAO.updateRegistroReacuEntity(registroReacuEntity);
		if(registroReacuDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroReacuEntity);
		}
	}

	private RegistroReacuEntity generateRegistroReacuEntityFromDTO(RegistroReacuDTO registroReacu) {
		prepareSolicitudDTO(registroReacu);
		return transformDTOToEntity(registroReacu,RegistroReacuEntity.class);
	}
	private void prepareRegistroReacuNullability(RegistroReacuDTO registroReacuDTO) {
		if (null != registroReacuDTO.getLugarOMedio()) {
			if (null != registroReacuDTO.getLugarOMedio().getLocation()) {
				prepareLocationNull(registroReacuDTO.getLugarOMedio().getLocation());
				registroReacuDTO.getLugarOMedio().getLocation().setCountry(SecosanUtil.checkDtoNullability(registroReacuDTO.getLugarOMedio().getLocation().getCountry()));
			}
		}
	}
}
