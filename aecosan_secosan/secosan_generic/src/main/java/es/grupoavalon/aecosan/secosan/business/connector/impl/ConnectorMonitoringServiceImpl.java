package es.grupoavalon.aecosan.secosan.business.connector.impl;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;
import es.grupoavalon.aecosan.secosan.business.dto.MonitoringConnectorDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.business.impl.AbstractManager;
import es.grupoavalon.aecosan.secosan.dao.MonitorizacionConectoresDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.MonitorizacionConectoresEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;

@Service
@Transactional(propagation = Propagation.NOT_SUPPORTED, noRollbackFor = { ServiceException.class, DaoException.class })
public class ConnectorMonitoringServiceImpl extends AbstractManager implements ConnectorMonitoringService  {

	
	@Autowired
	private MonitorizacionConectoresDAO monitorizacionConectoresDAO;


	/* (non-Javadoc)
	 * @see es.grupoavalon.aecosan.secosan.business.connector.impl.ConnectorMonitoringService#monitoringSuccess(es.grupoavalon.aecosan.secosan.business.connector.MonitorizableConnectors)
	 */
	@Override
	public void monitoringSuccess(MonitorizableConnectors connector) {
		try {
			MonitoringConnectorDTO monitor = instanciateDefaultMonitoringConnectorDTOSuccess(connector.getName());
			this.registerNewConnectorActivity(monitor);
			this.logger.debug("Registered success for: {}", connector);
		} catch (Exception e) {
			logger.warn("Can not log successfull activity in connector :{0} {1} {2}",connector.getName()," due an error: ", e.getMessage(), e);
		}

	}

	/* (non-Javadoc)
	 * @see es.grupoavalon.aecosan.secosan.business.connector.impl.ConnectorMonitoringService#monitoringError(es.grupoavalon.aecosan.secosan.business.connector.MonitorizableConnectors, java.lang.Exception, java.lang.String)
	 */
	@Override
	public void monitoringError(MonitorizableConnectors connector, Throwable e) {
		try {
			MonitoringConnectorDTO monitor = instanciateDefaultMonitoringConnectorDTOError(connector.getName(), e);
			this.registerNewConnectorActivity(monitor);
			this.logger.debug("Registered error for: {} {} {}", connector, " ERROR:", e);
		} catch (Exception e2) {
			logger.warn("Can not log error [" + e.getMessage() + "] activity in connector :" + connector.getName() + " due an error: " + e2.getMessage(), e2);
		}
	}

	private void registerNewConnectorActivity(MonitoringConnectorDTO monitor) {
			MonitorizacionConectoresEntity monitorEntity = transformDTOToEntity(monitor, MonitorizacionConectoresEntity.class);
			monitorizacionConectoresDAO.registerNewConnectorActivity(monitorEntity);
	}

	protected static MonitoringConnectorDTO instanciateDefaultMonitoringConnectorDTOSuccess(String connectorName) {
		MonitoringConnectorDTO monitor = new MonitoringConnectorDTO();
		monitor.setConnectorName(connectorName);
		monitor.setRegisterDate(System.currentTimeMillis());
		monitor.setSuccess(Boolean.TRUE);
		return monitor;
	}

	protected static MonitoringConnectorDTO instanciateDefaultMonitoringConnectorDTOError(String connectorName, Throwable e) {
		Throwable rootCause = getOriginalException(e);
		MonitoringConnectorDTO monitor = new MonitoringConnectorDTO();
		monitor.setConnectorName(connectorName);
		monitor.setRegisterDate(System.currentTimeMillis());
		monitor.setSuccess(Boolean.FALSE);
		monitor.setErrorType(rootCause.getClass().getSimpleName());
		monitor.setErrorDescription(rootCause.getLocalizedMessage());
		return monitor;
	}

	private static Throwable getOriginalException(Throwable e) {
		Throwable rootEx = ExceptionUtils.getRootCause(e);
		if (rootEx == null) {
			rootEx = e;
		}
		return rootEx;
	}

}
