package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class OnlinePaymentAddressDTO {

	@JsonProperty("streetTypeId")
	@XmlElement(name = "streetTypeId")
	@BeanToBeanMapping(getValueFrom = "streetAvenue", getSecondValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String streetTypeId;

	@JsonProperty("streetName")
	@XmlElement(name = "streetName")
	@BeanToBeanMapping
	private String streetName;

	@JsonProperty("number")
	@XmlElement(name = "number")
	@BeanToBeanMapping
	private String number;

	@JsonProperty("stair")
	@XmlElement(name = "stair")
	@BeanToBeanMapping
	private String stair;

	@JsonProperty("story")
	@XmlElement(name = "story")
	@BeanToBeanMapping
	private String story;

	// hold the..
	@JsonProperty("door")
	@XmlElement(name = "door")
	@BeanToBeanMapping
	private String door;

	public String getStreetTypeId() {
		return streetTypeId;
	}

	public void setStreetTypeId(String streetTypeId) {
		this.streetTypeId = streetTypeId;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStair() {
		return stair;
	}

	public void setStair(String stair) {
		this.stair = stair;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((door == null) ? 0 : door.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((stair == null) ? 0 : stair.hashCode());
		result = prime * result + ((story == null) ? 0 : story.hashCode());
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		result = prime * result + ((streetTypeId == null) ? 0 : streetTypeId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OnlinePaymentAddressDTO other = (OnlinePaymentAddressDTO) obj;
		if (door == null) {
			if (other.door != null) {
				return false;
			}
		} else if (!door.equals(other.door)) {
			return false;
		}
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		if (stair == null) {
			if (other.stair != null) {
				return false;
			}
		} else if (!stair.equals(other.stair)) {
			return false;
		}
		if (story == null) {
			if (other.story != null) {
				return false;
			}
		} else if (!story.equals(other.story)) {
			return false;
		}
		if (streetName == null) {
			if (other.streetName != null) {
				return false;
			}
		} else if (!streetName.equals(other.streetName)) {
			return false;
		}
		if (streetTypeId == null) {
			if (other.streetTypeId != null) {
				return false;
			}
		} else if (!streetTypeId.equals(other.streetTypeId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OnlinePaymentAddressDTO [streetTypeId=" + streetTypeId + ", streetName=" + streetName + ", number=" + number + ", stair=" + stair + ", story=" + story + ", door=" + door + "]";
	}

}
