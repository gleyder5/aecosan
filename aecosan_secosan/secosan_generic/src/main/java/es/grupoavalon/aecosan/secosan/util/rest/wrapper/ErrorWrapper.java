/**
 * 
 */
package es.grupoavalon.aecosan.secosan.util.rest.wrapper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author otto.abreu
 *
 */
@XmlRootElement(name="error")
public class ErrorWrapper {
	
	@XmlElement(name="error")
	private String error;
	@XmlElement(name="description")
	private String errorDescription;
		
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
		
	@Override
	public String toString() {
		return "ErrorWrapper [error=" + error + ", errorDescription="
				+ errorDescription + "]";
	}

}
