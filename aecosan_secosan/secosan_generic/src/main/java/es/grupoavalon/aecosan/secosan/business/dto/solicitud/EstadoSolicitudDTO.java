package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;

public class EstadoSolicitudDTO extends AbstractCatalogDto{

	@Override
	public String toString() {
		return "EstadoSolicitudDTO [catalogId=" + id + "]";
	}

}
