package es.grupoavalon.aecosan.secosan.util.pagination;

import org.apache.commons.lang.StringUtils;

public enum QueryOrder {
	DESC, ASC;

	public static QueryOrder parseFromString(String orderString) {
		QueryOrder order = null;
		if (StringUtils.equalsIgnoreCase(orderString, "desc")) {
			order = QueryOrder.DESC;
		} else {
			order = QueryOrder.ASC;
		}
		return order;
	}
}
