package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.FormaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("Forma")
public class FormaCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<FormaEntity, Long> implements GenericCatalogRepository<FormaEntity> {

	@Override
	public List<FormaEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<FormaEntity> getClassType() {

		return FormaEntity.class;
	}

}
