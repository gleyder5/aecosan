package es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroAguas;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.PaisDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class RegistroAguasDTO extends SolicitudDTO {

	@JsonProperty("commercialProductName")
	@XmlElement(name = "commercialProductName")
	@BeanToBeanMapping(getValueFrom = "commercialProductName")
	private String commercialProductName;

	@JsonProperty("rgseaa")
	@XmlElement(name = "rgseaa")
	@BeanToBeanMapping(getValueFrom = "rgseaa")
	private String rgseaa;

	@JsonProperty("nombreManantial")
	@XmlElement(name = "nombreManantial")
	@BeanToBeanMapping(getValueFrom = "nombreManantial")
	private String nombreManantial;

	@JsonProperty("lugarExplotacion")
	@XmlElement(name = "lugarExplotacion")
	@BeanToBeanMapping(getValueFrom = "lugarExplotacion")
	private String lugarExplotacion;

	@JsonProperty("nombreAguaPaisOrigen")
	@XmlElement(name = "nombreAguaPaisOrigen")
	@BeanToBeanMapping(getValueFrom = "nombreAguaPaisOrigen")
	private String nombreAguaPaisOrigen;

	@JsonProperty("paisOrigen")
	@XmlElement(name = "paisOrigen")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "paisOrigen")
	private PaisDTO paisOrigen;

	@JsonProperty("tratamientoAgua")
	@XmlElement(name = "tratamientoAgua")
	@BeanToBeanMapping(getValueFrom = "tratamientoAgua")
	private String tratamientoAgua;

	@JsonProperty("datosOficiales")
	@XmlElement(name = "datosOficiales")
	@BeanToBeanMapping(getValueFrom = "datosOficiales")
	private String datosOficiales;

	@JsonProperty("solicitudePayment")
	@XmlElement(name = "solicitudePayment")
	@BeanToBeanMapping(getValueFrom = "solicitudePayment", generateOther = true)
	private PagoSolicitudDTO solicitudePayment;

	public String getCommercialProductName() {
		return commercialProductName;
	}

	public void setCommercialProductName(String commercialProductName) {
		this.commercialProductName = commercialProductName;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public String getNombreManantial() {
		return nombreManantial;
	}

	public void setNombreManantial(String nombreManantial) {
		this.nombreManantial = nombreManantial;
	}

	public String getLugarExplotacion() {
		return lugarExplotacion;
	}

	public void setLugarExplotacion(String lugarExplotacion) {
		this.lugarExplotacion = lugarExplotacion;
	}

	public String getNombreAguaPaisOrigen() {
		return nombreAguaPaisOrigen;
	}

	public void setNombreAguaPaisOrigen(String nombreAguaPaisOrigen) {
		this.nombreAguaPaisOrigen = nombreAguaPaisOrigen;
	}

	public PaisDTO getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(PaisDTO paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public String getTratamientoAgua() {
		return tratamientoAgua;
	}

	public void setTratamientoAgua(String tratamientoAgua) {
		this.tratamientoAgua = tratamientoAgua;
	}

	public String getDatosOficiales() {
		return datosOficiales;
	}

	public void setDatosOficiales(String datosOficiales) {
		this.datosOficiales = datosOficiales;
	}

	public PagoSolicitudDTO getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudDTO solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "RegistroAguasDTO [commercialProductName=" + commercialProductName + ", rgseaa=" + rgseaa + ", nombreManantial=" + nombreManantial + ", lugarExplotacion=" + lugarExplotacion
				+ ", nombreAguaPaisOrigen=" + nombreAguaPaisOrigen + ", paisOrigen=" + paisOrigen + ", tratamientoAgua=" + tratamientoAgua + ", datosOficiales=" + datosOficiales
				+ ", solicitudePayment=" + solicitudePayment + "]";
	}
}
