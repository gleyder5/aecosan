package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import java.util.List;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.PresentacionEnvaseEntity;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class FinanciacionAlimentosDTO extends AbstractFinanciacionAlimentosDTO {

	@JsonProperty("companyRegisterNumber")
	@XmlElement(name = "companyRegisterNumber")
	@BeanToBeanMapping(getValueFrom = "companyRegisterNumber")
	private String companyRegisterNumber;

	@JsonProperty("productName")
	@XmlElement(name = "productName")
	@BeanToBeanMapping(getValueFrom = "productName")
	private String productName;

	@JsonProperty("productReferenceNumber")
	@XmlElement(name = "productReferenceNumber")
	@BeanToBeanMapping(getValueFrom = "productReferenceNumber")
	private String productReferenceNumber;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}



	@JsonProperty("age")
	@XmlElement(name = "age")
	@BeanToBeanMapping(getValueFrom = "age")
	private String age;

	@JsonProperty("caloricDensity")
	@XmlElement(name = "caloricDensity")
	@BeanToBeanMapping(getValueFrom = "caloricDensity")
	private String caloricDensity;

	@JsonProperty("osmolarity")
	@XmlElement(name = "osmolarity")
	@BeanToBeanMapping(getValueFrom = "osmolarity")
	private String osmolarity;

	@JsonProperty("osmolality")
	@XmlElement(name = "osmolality")
	@BeanToBeanMapping(getValueFrom = "osmolality")
	private String osmolality;

	@JsonProperty("molecularWeight")
	@XmlElement(name = "molecularWeight")
	@BeanToBeanMapping(getValueFrom = "molecularWeight")
	private String molecularWeight;

	@JsonProperty("instruction")
	@XmlElement(name = "instruction")
	@BeanToBeanMapping(getValueFrom = "instruction")
	private String instruction;

	@JsonProperty("groupOfPatient")
	@XmlElement(name = "groupOfPatient")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "groupOfPatient")
	private GrupoPacientesDTO groupOfPatient;

	@JsonProperty("physicalState")
	@XmlElement(name = "physicalState")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "physicalState")
	private EstadoFisicoDTO physicalState;

	@JsonProperty("caloricDistribution")
	@XmlElement(name = "caloricDistribution")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "caloricDistribution")
	private RepartoCaloricoDTO caloricDistribution;

	@JsonProperty("fiber")
	@XmlElement(name = "fiber")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "fiber")
	private FibraDTO fiber;

	@JsonProperty("proposal")
	@XmlElement(name = "proposal")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "proposal")
	private PropuestaDTO proposal;

	@JsonProperty("administrationMode")
	@XmlElement(name = "administrationMode")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "administrationMode")
	private ModoAdministracionDTO administrationMode;

	@JsonProperty("presentations")
	@XmlElement(name = "presentations")
	@BeanToBeanMapping(getValueFrom = "presentations", generateListOf = PresentacionEnvaseDTO.class, generateListFrom = PresentacionEnvaseEntity.class)
	private List<PresentacionEnvaseDTO> presentaciones;

	@JsonProperty("solicitudePayment")
	@XmlElement(name = "solicitudePayment")
	@BeanToBeanMapping(getValueFrom = "solicitudePayment", generateOther = true)
	private PagoSolicitudDTO solicitudePayment;

	@JsonProperty("companyName")
	@XmlElement(name = "companyName")
	@BeanToBeanMapping(getValueFrom = "companyName")
	private String companyName;

	@Column(name = "MOTIVO_INCLUSION")
	private String incluMotive;




	public String getCompanyRegisterNumber() {
		return companyRegisterNumber;
	}

	public void setCompanyRegisterNumber(String companyRegisterNumber) {
		this.companyRegisterNumber = companyRegisterNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductReferenceNumber() {
		return productReferenceNumber;
	}

	public void setProductReferenceNumber(String productReferenceNumber) {
		this.productReferenceNumber = productReferenceNumber;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCaloricDensity() {
		return caloricDensity;
	}

	public void setCaloricDensity(String caloricDensity) {
		this.caloricDensity = caloricDensity;
	}

	public String getOsmolarity() {
		return osmolarity;
	}

	public void setOsmolarity(String osmolarity) {
		this.osmolarity = osmolarity;
	}

	public String getOsmolality() {
		return osmolality;
	}

	public void setOsmolality(String osmolality) {
		this.osmolality = osmolality;
	}

	public String getMolecularWeight() {
		return molecularWeight;
	}

	public void setMolecularWeight(String molecularWeight) {
		this.molecularWeight = molecularWeight;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public GrupoPacientesDTO getGroupOfPatient() {
		return groupOfPatient;
	}

	public void setGroupOfPatient(GrupoPacientesDTO groupOfPatient) {
		this.groupOfPatient = groupOfPatient;
	}

	public EstadoFisicoDTO getPhysicalState() {
		return physicalState;
	}

	public void setPhysicalState(EstadoFisicoDTO physicalState) {
		this.physicalState = physicalState;
	}

	public RepartoCaloricoDTO getCaloricDistribution() {
		return caloricDistribution;
	}

	public void setCaloricDistribution(RepartoCaloricoDTO caloricDistribution) {
		this.caloricDistribution = caloricDistribution;
	}

	public FibraDTO getFiber() {
		return fiber;
	}

	public void setFiber(FibraDTO fiber) {
		this.fiber = fiber;
	}

	public PropuestaDTO getProposal() {
		return proposal;
	}

	public void setProposal(PropuestaDTO proposal) {
		this.proposal = proposal;
	}

	public ModoAdministracionDTO getAdministrationMode() {
		return administrationMode;
	}

	public void setAdministrationMode(ModoAdministracionDTO administrationMode) {
		this.administrationMode = administrationMode;
	}

	public List<PresentacionEnvaseDTO> getPresentaciones() {
		return presentaciones;
	}

	public void setPresentaciones(List<PresentacionEnvaseDTO> presentaciones) {
		this.presentaciones = presentaciones;
	}

	public PagoSolicitudDTO getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudDTO solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "FinanciacionAlimentosDTO [companyRegisterNumber=" + companyRegisterNumber + ", productName=" + productName + ", productReferenceNumber=" + productReferenceNumber + ", age=" + age
				+ ", caloricDensity=" + caloricDensity + ", osmolarity=" + osmolarity + ", osmolality=" + osmolality + ", molecularWeight=" + molecularWeight + ", instruction=" + instruction
				+ ", groupOfPatient=" + groupOfPatient + ", physicalState=" + physicalState + ", caloricDistribution=" + caloricDistribution + ", fiber=" + fiber + ", proposal=" + proposal
				+ ", administrationMode=" + administrationMode + ", presentaciones=" + presentaciones + ", solicitudePayment=" + solicitudePayment + ", companyName=" + companyName
				+ "]";
	}


}
