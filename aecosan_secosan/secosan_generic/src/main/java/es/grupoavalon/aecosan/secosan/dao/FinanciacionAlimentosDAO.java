package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;

public interface FinanciacionAlimentosDAO {

	IncOferAlimUMEEntity addIncOferAlimUME(IncOferAlimUMEEntity incOferAlimUME);

	void updateIncOferAlimUME(IncOferAlimUMEEntity incOferAlimUME);

	AlterOferAlimUMEEntity addAlterOferAlimUME(AlterOferAlimUMEEntity alterOferAlimUMEEntity);

	void updateAlterOferAlimUME(AlterOferAlimUMEEntity alterOferAlimUMEEntity);

	BajaOferAlimUMEEntity addBajaOferAlimUME(BajaOferAlimUMEEntity bajaOferAlimUMEEntity);

	void updateBajaOferAlimUME(BajaOferAlimUMEEntity bajaOferAlimUMEEntity);

	void deleteSuspendedProduct(long id);
	void deletePresentacionEnvases(long id);

}
