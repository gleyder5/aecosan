package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoProductoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PUESTA_MERCADO_AG)
public class PuestaMercadoAGEntity extends PuestaMercadoEntity implements ComplementosAlimenticios {

	@Column(name = "INGREDIENTES")
	private String ingredientes;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_PRODUCTO_ID")
	private TipoProductoEntity productType;

	public String getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(String ingredientes) {
		this.ingredientes = ingredientes;
	}

	public TipoProductoEntity getProductType() {
		return productType;
	}

	public void setProductType(TipoProductoEntity productType) {
		this.productType = productType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ingredientes == null) ? 0 : ingredientes.hashCode());
		result = prime * result + ((productType == null) ? 0 : productType.getIdAsString().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PuestaMercadoAGEntity other = (PuestaMercadoAGEntity) obj;
		if (ingredientes == null) {
			if (other.ingredientes != null)
				return false;
		} else if (!ingredientes.equals(other.ingredientes))
			return false;
		if (productType == null) {
			if (other.productType != null)
				return false;
		} else if (!productType.getIdAsString().equals(other.productType.getIdAsString()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String tipoProductoId = ((productType == null) ? "" : productType.getIdAsString());
		return "PuestaMercadoAGEntity [ingredientes=" + ingredientes + " productType= " + tipoProductoId + "]";
	}

}
