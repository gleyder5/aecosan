package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesJuntasDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("subvencionesJuntasRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/subvencionesJuntas")
public class SubvencionesJuntasConsumoRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addSubvencionesJuntas(@PathParam("user") String user, SubvencionesJuntasDTO subvencionesJuntas) {
		logger.debug("-- Method addSubvencionesJuntas POST Init--");
		Response response = addSolicitud(user, subvencionesJuntas);
		logger.debug("-- Method addSubvencionesJuntas POST End--");
		return response;
	}
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateSubvencionesJuntas(@PathParam("user") String user, SubvencionesJuntasDTO subvencionesJuntas) {
			logger.debug("-- Method updateSubvencionesJuntas PUT Init--");
		Response response = updateSolicitud(user, subvencionesJuntas);
		logger.debug("-- Method updateSubvencionesJuntas PUT End--");
		return response;
	}

}
