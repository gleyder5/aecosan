package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesJuntasConsumoEntity;

public interface SubvencionesJuntasRepository extends CrudRepository<SubvencionesJuntasConsumoEntity, Long> {

}
