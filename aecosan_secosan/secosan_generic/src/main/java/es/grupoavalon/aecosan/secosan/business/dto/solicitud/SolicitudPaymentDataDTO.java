	package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.LongToDateTransformer;

public class SolicitudPaymentDataDTO {

	@JsonProperty("epago_nrc")
	@XmlElement(name = "epago_nrc")
	@BeanToBeanMapping(getValueFrom = "nrc")
	private String epagoNRC;

	@JsonProperty("fecha_pago")
	@XmlElement(name = "fecha_pago")
	@BeanToBeanMapping(getValueFrom = "paymentDate", useCustomTransformer = LongToDateTransformer.class)
	private Long paymentDate;

	public String getEpagoNRC() {
		return epagoNRC;
	}

	public void setEpagoNRC(String epagoNRC) {
		this.epagoNRC = epagoNRC;
	}

	public Long getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Long paymentDate) {
		this.paymentDate = paymentDate;
	}

}
