package es.grupoavalon.aecosan.secosan.dao.entity.pago;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_DIRECCION_EXTENDIDA)
public class DireccionExtendidaEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.EXTENDED_ADDRESS_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.EXTENDED_ADDRESS_SEQUENCE, sequenceName = SequenceNames.EXTENDED_ADDRESS_SEQUENCE, allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NOMBRE_VIA_PUBLICA")
	private String streetName;

	@Column(name = "NUMERO", length = 50)
	private String number;

	@Column(name = "ESCALERA", length = 50)
	private String stair;

	@Column(name = "PISO", length = 20)
	private String story;

	@Column(name = "PUERTA", length = 50)
	private String door;

	@Column(name = "TIPO_VIA_PUBLICA")
	private String streetAvenue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStair() {
		return stair;
	}

	public void setStair(String stair) {
		this.stair = stair;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getStreetAvenue() {
		return streetAvenue;
	}

	public void setStreetAvenue(String streetAvenue) {
		this.streetAvenue = streetAvenue;
	}

	@Override
	public String toString() {
		return "DireccionExtendida [id=" + id + ", streetName=" + streetName + ", number=" + number + ", stair=" + stair + ", story=" + story + ", door=" + door + ", streetAvenue=" + streetAvenue
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((door == null) ? 0 : door.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((stair == null) ? 0 : stair.hashCode());
		result = prime * result + ((story == null) ? 0 : story.hashCode());
		result = prime * result + ((streetAvenue == null) ? 0 : streetAvenue.hashCode());
		result = prime * result + ((streetName == null) ? 0 : streetName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DireccionExtendidaEntity other = (DireccionExtendidaEntity) obj;
		if (door == null) {
			if (other.door != null) {
				return false;
			}
		} else if (!door.equals(other.door)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		if (stair == null) {
			if (other.stair != null) {
				return false;
			}
		} else if (!stair.equals(other.stair)) {
			return false;
		}
		if (story == null) {
			if (other.story != null) {
				return false;
			}
		} else if (!story.equals(other.story)) {
			return false;
		}
		if (streetAvenue == null) {
			if (other.streetAvenue != null) {
				return false;
			}
		} else if (!streetAvenue.equals(other.streetAvenue)) {
			return false;
		}
		if (streetName == null) {
			if (other.streetName != null) {
				return false;
			}
		} else if (!streetName.equals(other.streetName)) {
			return false;
		}
		return true;
	}

}
