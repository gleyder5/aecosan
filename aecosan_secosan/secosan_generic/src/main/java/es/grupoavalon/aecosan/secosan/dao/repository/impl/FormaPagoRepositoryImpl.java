package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.FormaPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.FormaPagoRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("FormaPago")
public class FormaPagoRepositoryImpl extends CrudCatalogRepositoryImpl<FormaPagoEntity, Long> implements FormaPagoRepository, GenericCatalogRepository<FormaPagoEntity> {


	@Override
	protected Class<FormaPagoEntity> getClassType() {
		return FormaPagoEntity.class;
	}

	@Override
	public List<FormaPagoEntity> getCatalogList() {
		return findAllOrderByValue();
	}
}
