package es.grupoavalon.aecosan.secosan.business.dto.user;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

public class ChangePasswordDTO {

	@JsonProperty("userId")
	@XmlElement(name = "userId")
	private String userId;
	@JsonProperty("newPassword")
	@XmlElement(name = "newPassword")
	private String newPassword;

	@JsonProperty("oldPassword")
	@XmlElement(name = "oldPassword")
	private String oldPassword;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNewPassword() {

		return newPassword;
	}

	public String getNewPasswordDecoded() {

		return SecosanUtil.decodeStringIfBase64(this.newPassword);
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public String getOldPasswordDecoded() {
		return SecosanUtil.decodeStringIfBase64(oldPassword);
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((newPassword == null) ? 0 : newPassword.hashCode());
		result = prime * result + ((oldPassword == null) ? 0 : oldPassword.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ChangePasswordDTO other = (ChangePasswordDTO) obj;
		if (newPassword == null) {
			if (other.newPassword != null) {
				return false;
			}
		} else if (!newPassword.equals(other.newPassword)) {
			return false;
		}
		if (oldPassword == null) {
			if (other.oldPassword != null) {
				return false;
			}
		} else if (!oldPassword.equals(other.oldPassword)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ChangePasswordDTO [userId=" + userId + ", newPassword=" + newPassword + ", oldPassword=" + oldPassword + "]";
	}

}
