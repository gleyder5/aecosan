package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoUsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoUsuarioRepository;

@Component
public class TipoUsuarioRepositoryImpl extends AbstractCrudRespositoryImpl<TipoUsuarioEntity, Long>
		implements TipoUsuarioRepository {

	@Override
	protected Class<TipoUsuarioEntity> getClassType() {
		return TipoUsuarioEntity.class;
	}
}
