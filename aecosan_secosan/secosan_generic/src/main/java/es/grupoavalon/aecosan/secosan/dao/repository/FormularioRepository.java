package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;

public interface FormularioRepository extends CrudRepository<TipoSolicitudEntity, Long>{

}
