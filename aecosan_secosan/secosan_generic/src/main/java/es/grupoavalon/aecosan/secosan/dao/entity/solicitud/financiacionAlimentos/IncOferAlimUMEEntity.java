package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_INC_OFER_ALIMENTOS)
public class IncOferAlimUMEEntity extends FinanciacionAlimentosEntity {

	@Column(name = "INSCRIPTION_DATE")
	private Date inscriptionDate;

	@Column(name = "PHONE")
	private String phone;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "REGISTER_NUMER")
	private String registerNumber;

	@Column(name = "ADDRESS")
	private String address;


	@Transient
	private Long inscriptionDateString;

	public Long getInscriptionDateString() {
		if (inscriptionDate != null) {
			return inscriptionDate.getTime();
		}
		return inscriptionDateString;
	}

	public void setInscriptionDateString(Long inscriptionDateString) {
		this.inscriptionDate = new Date(inscriptionDateString);
		this.inscriptionDateString = inscriptionDateString;
	}

	public Date getInscriptionDate() {
		return inscriptionDate;
	}


	public void setInscriptionDate(Date inscriptionDate) {
		this.inscriptionDate = inscriptionDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
