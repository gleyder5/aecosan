package es.grupoavalon.aecosan.secosan.rest;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.LogDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("logRestService")
public class LogRestService {

	private static final Logger logger = LoggerFactory.getLogger(LogRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/log/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "list")
	public Response getLogNames() {
		Response response = null;
		try {
			logger.debug("-- Method getLogNames -- ");
			Collection<String> logsNames = bfacade.getLogNames();
			response = responseFactory.generateOkGenericResponse(logsNames);

		} catch (Exception e) {
			logger.error("Error getLogNames || ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{name}")
	public Response getLog(@PathParam("name") String name) {
		Response response = null;
		try {
			logger.debug("-- Method getLog -- ");
			LogDTO log = bfacade.getLog(name);
			response = responseFactory.generateOkGenericResponse(log);
			logger.debug("-- Method getLog end-- ");
		} catch (Exception e) {
			logger.error("Error getLogNames || ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

}
