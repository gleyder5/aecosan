package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class IdentificadorPagoDTO implements IsNulable<IdentificadorPagoDTO> {
	@Override
	public String toString() {
		return "IdentificadorPagoDTO [id=" + id + ", name=" + name + ", lastname=" + lastname + ", secondlastName=" + secondlastName + ", identificationNumber=" + identificationNumber
				+ ", telephoneNumber=" + telephoneNumber + ", email=" + email + ", address=" + address + ", location=" + location + "]";
	}

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("name")
	@XmlElement(name = "name")
	@BeanToBeanMapping(getValueFrom = "name")
	private String name;

	@JsonProperty("lastName")
	@XmlElement(name = "lastName")
	@BeanToBeanMapping(getValueFrom = "lastName")
	private String lastname;

	@JsonProperty("secondlastName")
	@XmlElement(name = "secondlastName")
	@BeanToBeanMapping(getValueFrom = "secondLastName")
	private String secondlastName;

	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	private String identificationNumber;

	@JsonProperty("telephoneNumber")
	@XmlElement(name = "telephoneNumber")
	@BeanToBeanMapping(getValueFrom = "telephoneNumber", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String telephoneNumber;

	@JsonProperty("email")
	@XmlElement(name = "email")
	@BeanToBeanMapping(getValueFrom = "email")
	private String email;

	@JsonProperty("address")
	@XmlElement(name = "address")
	@BeanToBeanMapping(getValueFrom = "address")
	private String address;

	@JsonProperty("location")
	@XmlElement(name = "location")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "location")
	private UbicacionGeograficaDTO location;
	
	@JsonProperty("extendedAddress")
	@XmlElement(name = "extendedAddress")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "extendedAddress")
	private DireccionExtendidaDTO extendedAddress;

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UbicacionGeograficaDTO getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaDTO location) {
		this.location = location;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSecondlastName() {
		return secondlastName;
	}

	public void setSecondlastName(String secondlastName) {
		this.secondlastName = secondlastName;
	}
	
	public DireccionExtendidaDTO getExtendedAddress() {
		return extendedAddress;
	}

	public void setExtendedAddress(DireccionExtendidaDTO extendedAddress) {
		this.extendedAddress = extendedAddress;
	}

	@Override
	public IdentificadorPagoDTO shouldBeNull() {
		IdentificadorPagoDTO idpago = this;

		if (StringUtils.isBlank(name) && StringUtils.isBlank(lastname) && StringUtils.isBlank(secondlastName) && StringUtils.isBlank(address) && StringUtils.isBlank(identificationNumber)
				&& (this.location.shouldBeNull() == null)) {
			idpago = null;
		}
		return idpago;
	}
}
