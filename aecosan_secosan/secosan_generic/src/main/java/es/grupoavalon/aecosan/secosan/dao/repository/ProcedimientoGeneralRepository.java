package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.ProcedimientoGeneralEntity;

public interface ProcedimientoGeneralRepository extends CrudRepository<ProcedimientoGeneralEntity, Long> {

}
