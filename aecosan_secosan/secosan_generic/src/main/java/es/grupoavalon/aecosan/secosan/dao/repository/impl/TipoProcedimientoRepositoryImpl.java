package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoProcedimientoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoProcedimientoRepository;
import org.springframework.stereotype.Component;

@Component
public class TipoProcedimientoRepositoryImpl extends AbstractCrudRespositoryImpl<TipoProcedimientoEntity, Long> implements TipoProcedimientoRepository {

	@Override
	protected Class<TipoProcedimientoEntity> getClassType() {
		return TipoProcedimientoEntity.class;
	}

}
