package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.SolicitudLmrFitosanitariosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudLmrFitosanitariosDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.SolicitudLmrFitosanitariosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class SolicitudLmrFitosanitariosServiceImpl extends AbstractSolicitudService implements SolicitudLmrFitosanitariosService {

	@Autowired
	private SolicitudLmrFitosanitariosDAO solicitudeLmrFitosanitariosDAO;
	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO solicitudeLmrFitosanitariosDTO) {
		try {
			solicitudPrepareNullDTO(solicitudeLmrFitosanitariosDTO);
			LmrFitosanitariosPrepareNullPaymentDTO((SolicitudLmrFitosanitariosDTO) solicitudeLmrFitosanitariosDTO);
			addLmrFitosanitarios(user, (SolicitudLmrFitosanitariosDTO) solicitudeLmrFitosanitariosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + solicitudeLmrFitosanitariosDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO solicitudeLmrFitosanitariosDTO) {
		try {
			solicitudPrepareNullDTO(solicitudeLmrFitosanitariosDTO);
			LmrFitosanitariosPrepareNullPaymentDTO((SolicitudLmrFitosanitariosDTO) solicitudeLmrFitosanitariosDTO);
			updateSolicitudLmrFitosanitariosDTO(user, (SolicitudLmrFitosanitariosDTO) solicitudeLmrFitosanitariosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update| ERROR: " + solicitudeLmrFitosanitariosDTO);
		}
	}

	private void LmrFitosanitariosPrepareNullPaymentDTO(SolicitudLmrFitosanitariosDTO solicitudeLmrFitosanitariosDTO) {
		solicitudeLmrFitosanitariosDTO.setSolicitudePayment(SecosanUtil.checkDtoNullability(solicitudeLmrFitosanitariosDTO.getSolicitudePayment()));
		if (null != solicitudeLmrFitosanitariosDTO.getSolicitudePayment()) {
			solicitudeLmrFitosanitariosDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getPayment()));
			solicitudeLmrFitosanitariosDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getIdPayment()));
			solicitudeLmrFitosanitariosDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getDataPayment()));
			if (null != solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getIdPayment()) {
				solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getIdPayment()
						.setLocation(SecosanUtil.checkDtoNullability(solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getIdPayment().getLocation()));
				prepareLocationNull(solicitudeLmrFitosanitariosDTO.getSolicitudePayment().getIdPayment().getLocation());
			}
		}
	}

	private void addLmrFitosanitarios(String user, SolicitudLmrFitosanitariosDTO solicitudLmrFitosanitariosDTO) {
		SolicitudLmrFitosanitariosEntity solicitudeLmrFitosanitariosEntity = generateSolicitudLmrFitosanitariosEntityFromDTO(solicitudLmrFitosanitariosDTO);
		setUserCreatorByUserId(user, solicitudeLmrFitosanitariosEntity);
		prepareAddSolicitudEntity(solicitudeLmrFitosanitariosEntity, solicitudLmrFitosanitariosDTO);
		SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntityNew=  solicitudeLmrFitosanitariosDAO.addSolicitudLmrFitosanitarios(solicitudeLmrFitosanitariosEntity);
		if(solicitudLmrFitosanitariosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudLmrFitosanitariosEntityNew );
		}

	}

	private void updateSolicitudLmrFitosanitariosDTO(String user, SolicitudLmrFitosanitariosDTO solicitudLmrFitosanitariosDTO) {
		SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity = transformDTOToEntity(solicitudLmrFitosanitariosDTO, SolicitudLmrFitosanitariosEntity.class);
		setUserCreatorByUserId(user, solicitudLmrFitosanitariosEntity);
		prepareUpdateSolicitudEntity(solicitudLmrFitosanitariosEntity, solicitudLmrFitosanitariosDTO);
		if(solicitudLmrFitosanitariosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudLmrFitosanitariosEntity);
		}
		solicitudeLmrFitosanitariosDAO.updateSolicitudLmrFitosanitarios(solicitudLmrFitosanitariosEntity);
	}

	private SolicitudLmrFitosanitariosEntity generateSolicitudLmrFitosanitariosEntityFromDTO(SolicitudLmrFitosanitariosDTO solicitudLmrFitosanitariosDTO) {
		prepareSolicitudDTO(solicitudLmrFitosanitariosDTO);
		return transformDTOToEntity(solicitudLmrFitosanitariosDTO, SolicitudLmrFitosanitariosEntity.class);
	}

}
