package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_MUNICIPIOS)
public class MunicipioEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.MUNICIPIO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.MUNICIPIO_SEQUENCE, sequenceName = SequenceNames.MUNICIPIO_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "IDLOCALIDAD")
	private String idlocalidad;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PROVINCIA_ID", nullable = false)
	private ProvinciasEntity province;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdlocalidad() {
		return idlocalidad;
	}

	public void setIdlocalidad(String idlocalidad) {
		this.idlocalidad = idlocalidad;
	}

	public ProvinciasEntity getProvince() {
		return province;
	}

	public void setProvince(ProvinciasEntity province) {
		this.province = province;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		if(idAsString.equals("")){
			id = null;
		}else{
			id = Long.valueOf(idAsString);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idlocalidad == null) ? 0 : idlocalidad.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MunicipioEntity other = (MunicipioEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (idlocalidad == null) {
			if (other.idlocalidad != null)
				return false;
		} else if (!idlocalidad.equals(other.idlocalidad))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MunicipioEntity [id=" + id + ", idlocalidad=" + idlocalidad + "]";
	}
}
