package es.grupoavalon.aecosan.secosan.business.enums;


public enum PlaceholdersFichaTecnicaIncl implements EnumPrototype {
    NO_REGISTRO("NO_REGISTRO"),

    NOMBRE_PERSONA("NOMBRE_PERSONA"),
    NOMBRE_EMPRESA("NOMBRE_EMPRESA"),
    TELEFONO("TELEFONO"),
    CORREO("CORREO"),
    DIRECCION("DIRECCION"),
    NOMBRE_PRODUCTO("NOMBRE_PRODUCTO"),
    NO_REF_PRODUCTO("NO_REF_PRODUCTO"),
    GRUPO_PACIENTES_DESTINADO("GRUPO_PACIENTES_DESTINADO"),
    LACTANTES("LACTANTES") ,
    NINOS("NINOS")	,
    ADULTOS("ADULTOS")	,
    MODO_ADMINISTRACION("MODO_ADMINISTRACION"),
    SONDA("SONDA"),
    ORAL("ORAL") ,
    MIXTA("MIXTA") ,
    ESTADO_FISICO("ESTADO_FISICO"),
    POLVO("POLVO") ,
    LIQUIDO("LIQUIDO") ,
    SOLUCION_ESTANDAR("SOLUCION_ESTANDAR"),
    EDAD("EDAD"),
    DENSIDAD_CALORICA("DENSIDAD_CALORICA"),
    OSMOLARIDAD("OSMOLARIDAD"),
    OSMOLALIDAD("OSMOLALIDAD"),
    PESO_MOLECULAR("PESO_MOLECULAR"),
    REPARTO_CALORICO("REPARTO_CALORICO"),
    PROTEINAS("PROTEINAS"),
    LIPIDOS("LIPIDOS") 	,
    HIDRATOS_DE_CARBONO("HIDRATOS_DE_CARBONO")  ,
    FIBRA("FIBRA") ,
    CANTIDAD("CANTIDAD") ,
    PROPUESTA_("PROPUESTA_"),
    CODIGO_TIPO("CODIGO_TIPO"),
    CODIGO_SUBTIPO("CODIGO_SUBTIPO"),
    INDICACIONES_PROD_SOLICITUD_INCLUSION_OFERTA("INDICACIONES_PROD_SOLICITUD_INCLUSION_OFERTA"),
    TIPO_ENVASE_PRESENTACION1("TIPO_ENVASE_PRESENTACION1"),
    TIPO_ENVASE_PRESENTACION2("TIPO_ENVASE_PRESENTACION2"),
    TIPO_ENVASE_PRESENTACION3("TIPO_ENVASE_PRESENTACION3"),
    TIPO_ENVASE_PRESENTACION4("TIPO_ENVASE_PRESENTACION4"),
    CONTENIDO_ENVASE_PRESENTACION1("CONTENIDO_ENVASE_PRESENTACION1"),
    CONTENIDO_ENVASE_PRESENTACION2("CONTENIDO_ENVASE_PRESENTACION2"),
    CONTENIDO_ENVASE_PRESENTACION3("CONTENIDO_ENVASE_PRESENTACION3"),
    CONTENIDO_ENVASE_PRESENTACION4("CONTENIDO_ENVASE_PRESENTACION4"),
    NUM_ENVASES_PRESENTACION1("NUM_ENVASES_PRESENTACION1"),
    NUM_ENVASES_PRESENTACION2("NUM_ENVASES_PRESENTACION2"),
    NUM_ENVASES_PRESENTACION3("NUM_ENVASES_PRESENTACION3"),
    NUM_ENVASES_PRESENTACION4("NUM_ENVASES_PRESENTACION4"),
    SABOR_PRESENTACION1("SABOR_PRESENTACION1"),
    SABOR_PRESENTACION2("SABOR_PRESENTACION2"),
    SABOR_PRESENTACION3("SABOR_PRESENTACION3"),
    SABOR_PRESENTACION4("SABOR_PRESENTACION4"),
    PRECIO_VENTA_EMPRESA_PRESENTACION1("PRECIO_VENTA_EMPRESA_PRESENTACION1"),
    PRECIO_VENTA_EMPRESA_PRESENTACION2("PRECIO_VENTA_EMPRESA_PRESENTACION2"),
    PRECIO_VENTA_EMPRESA_PRESENTACION4("PRECIO_VENTA_EMPRESA_PRESENTACION4"),
    PRECIO_VENTA_EMPRESA_PRESENTACION3("PRECIO_VENTA_EMPRESA_PRESENTACION3");




    private String name;

    private PlaceholdersFichaTecnicaIncl(final String name) {
        this.name = name;
    }


    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return "desc";
    }


    public String getKey() {
        return super.toString();
    }
}

