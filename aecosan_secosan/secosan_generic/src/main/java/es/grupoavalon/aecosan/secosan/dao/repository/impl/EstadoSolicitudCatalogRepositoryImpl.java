package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("EstadoSolicitud")
public class EstadoSolicitudCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<EstadoSolicitudEntity, Long> implements GenericCatalogRepository<EstadoSolicitudEntity> {

	@Override
	public List<EstadoSolicitudEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<EstadoSolicitudEntity> getClassType() {

		return EstadoSolicitudEntity.class;
	}

}
