package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Repository
public class SolicitudDAOImpl extends GenericDao implements SolicitudDAO {

	private static final String ERROR = " ERROR: ";

	@Autowired
	private SolicitudRepository solicitudRepository;

	@Override
	public List<SolicitudEntity> getSolicList(PaginationParams paginationParams) {
		/*
		 * Filter example: filter[0][id]=1223 filter[1][formulario]=2
		 * filter[2][fechaCreacionTo]=1461772066276
		 * filter[3][fechaCreacionFrom]=1459893600000 filter[4][estado]=1
		 */
		logger.debug("Paginacion: de " + paginationParams.getStart() + " a " + paginationParams.getLength());
		logger.debug("Filtros: " + paginationParams.getFilters().toString());

		List<SolicitudEntity> results = new ArrayList<SolicitudEntity>();
		try {
			results = solicitudRepository.getFilteredList(paginationParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getSolicList |" + paginationParams + ERROR + e.getMessage());
		}

		return results;
	}


	@Override
	public int getTotalListRecords(List<FilterParams> filters) {
		int total = 0;
		try {
			// Long userId = Long.parseLong(user);
			total = solicitudRepository.getTotalListRecords(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalListRecords | " + filters + ERROR + e.getMessage());
		}
		return total;
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {
		int totalFiltered = 0;
		try {
			totalFiltered = solicitudRepository.getFilteredListRecords(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getFilteredListRecords |" + filters + "|" + ERROR + e.getMessage());
		}
		return totalFiltered;

	}

	@Override
	public SolicitudEntity findSolicitudById(Long id) {
		SolicitudEntity solicitudEntity = null;
		try {
			solicitudEntity = solicitudRepository.findOne(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " findSolicitudById | id: " + id);
		}

		return solicitudEntity;
	}

	@Override
	public void deleteSolicitudById(String solicitudeId) {
		try {
			solicitudRepository.delete(Long.valueOf(solicitudeId));
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteSolicitudById | " + solicitudeId);
		}

	}

	@Override
	public void updateSolicitudById(SolicitudEntity solicitudEntity) {
		try {
			solicitudRepository.update(solicitudEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateSolicitudById | " + solicitudEntity);
		}
	}

}
