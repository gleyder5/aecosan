package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.FicheroInstruccionesRepository;

@Component
public class FicheroInstruccionesRespositoryImpl extends AbstractCrudRespositoryImpl<FicheroInstruccionesEntity, Long> implements FicheroInstruccionesRepository {

	private static final String FORM = "solicitudeType";
	private static final String AREA = "area";

	@Override
	protected Class<FicheroInstruccionesEntity> getClassType() {
		return FicheroInstruccionesEntity.class;
	}

	@Override
	public FicheroInstruccionesEntity findBySolicitudeType(Long idFormulario) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(FORM, idFormulario);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_INSTRUCTIONS_BY_SOLICITUDETYPE, queryParams);
	}

	@Override
	public List<FicheroInstruccionesEntity> findAllByArea(Long area) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(AREA, area);
		return findByNamedQuery(NamedQueriesLibrary.GET_INSTRUCTIONS_BY_LIST_AREA, queryParams);
	}

}
