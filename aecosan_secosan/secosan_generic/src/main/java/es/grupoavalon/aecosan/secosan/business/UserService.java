package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.user.LoginUserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface UserService {

	LoginUserDTO doLogin(String username, String password) throws AutenticateException;

	LoginUserDTO doLoginAdmin(String username, String password) throws AutenticateException;

	void doRegistry(UserDTO user);

	LoginUserDTO getExternalUserByIdenticationDocument(String identificationNumber,boolean isCLave);

	LoginUserDTO doClaveLogin(UserClaveDTO claveUser);

	boolean loginIsTaken(String login);

	boolean loginIsTakenInternal(String login);

	List<UserInternalDTO> getInternalUserList(PaginationParams paginationParams);

	void deleteInternalUser(String userId, String userToDeleteId);

	void updateInternalUser(String userId, UserInternalDTO userToUpdate);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);

	LoginUserDTO doClaveLoginAdmin(UserClaveDTO claveUser) throws AutenticateException;

	UserDTO getInternalUserByIdentificationDocument(String identificationDocument);

}