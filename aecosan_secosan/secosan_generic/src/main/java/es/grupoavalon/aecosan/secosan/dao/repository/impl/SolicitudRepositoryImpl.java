package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudRepository;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;

@Component
public class SolicitudRepositoryImpl extends AbstractCrudRespositoryImpl<SolicitudEntity, Long> implements SolicitudRepository {

	private static final String FECHA_CREACION_FROM_FILTER = "fechaCreacionFrom";
	private static final String FECHA_CREACION_TO_FILTER = "fechaCreacionTo";
	private static final String NIF_FILTER = "nif";
	private static final String QUERY = "#Query: ";
	private static final String IDENTIFICADOR_PETICION_FILTER = "identificadorPeticion";

	private static final String WHEREPART_USER = "userCreator.id =:user";
	private static final String WHEREPART_AREA = "area.id =:area";
	private static final String WHEREPART_STATUS = "status.id";
	private static final String NIF_FIELD = "solicitante.identificationNumber";
	private static final String FECHA_CREACION_FIELD = "fechaCreacion";
	private static final String STATUS_FIELD = "status.catalogValue";


	@Override
	protected Class<SolicitudEntity> getClassType() {
		return SolicitudEntity.class;
	}

	@Override
	public List<SolicitudEntity> getFilteredList(PaginationParams paginationParams) {
		List<FilterParams> filters = paginationParams.getFilters();

		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryWherePart = getQueryParts(filters, queryParams);
		logger.debug(QUERY + queryWherePart);
		String sortField = translateSortField(paginationParams.getSortField());

		List<SolicitudEntity> resultList;
		if (paginationParams.getOrder().equals(QueryOrder.ASC)) {
			resultList = findAllPaginatedAndSortedASC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		} else {
			resultList = findAllPaginatedAndSortedDESC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		}

		return resultList;

	}

	@Override
	public int getTotalListRecords(List<FilterParams> filters) {

		Map<String, Object> queryParams = new HashMap<String, Object>();
		return getTotalRecords(getQueryParts(filters, queryParams), queryParams);
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {

		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryName = getQueryParts(filters, queryParams);
		logger.debug(QUERY + queryName);
		return getTotalRecords(queryName, queryParams);
	}

	private String translateSortField(String sortField) {
		String translatedSortField = sortField;
		if (StringUtils.equals(IDENTIFICADOR_PETICION_FILTER, sortField)) {
			translatedSortField = SecosanConstants.FIELD_IDENTIFICACIONPETICION;
		} else if (StringUtils.equals(SecosanConstants.FILTER_STATUS, sortField)) {
			translatedSortField = STATUS_FIELD;
		}
		return translatedSortField;
	}

	private String getQueryParts(List<FilterParams> filters, Map<String, Object> queryParams) {
		QueryWhereParts queryParts = new QueryWhereParts();

		for (FilterParams f : filters) {
			if (f.getFilterName().equals(SecosanConstants.FIELD_USER)) {
				queryParts.setPart(WHEREPART_USER);
				queryParams.put(SecosanConstants.FIELD_USER, Long.valueOf(f.getFilterValue()));
			} else if (f.getFilterName().equals(FECHA_CREACION_FROM_FILTER)) {
				queryParts.setPart(FECHA_CREACION_FIELD + ">=" + ":" + FECHA_CREACION_FROM_FILTER);
				queryParams.put(FECHA_CREACION_FROM_FILTER, new Date(Long.valueOf(f.getFilterValue())));
			} else if (f.getFilterName().equals(FECHA_CREACION_TO_FILTER)) {
				queryParts.setPart(FECHA_CREACION_FIELD + "<=" + ":" + FECHA_CREACION_TO_FILTER);
				queryParams.put(FECHA_CREACION_TO_FILTER, new Date(Long.valueOf(f.getFilterValue())));
			} else if (f.getFilterName().equals(NIF_FILTER)) {
				queryParts.setPart("LOWER ( " + NIF_FIELD + " ) = " + " LOWER ( :" + NIF_FILTER + " )");
				queryParams.put(NIF_FILTER, f.getFilterValue());
			} else if (f.getFilterName().equals(SecosanConstants.FIELD_AREA)) {
				queryParts.setPart(WHEREPART_AREA);
				queryParams.put(SecosanConstants.FIELD_AREA, Long.valueOf(f.getFilterValue()));
			} else if (f.getFilterName().equals(SecosanConstants.FILTER_NOT_STATUS)) {
				queryParts.setPart(WHEREPART_STATUS + "!=" + ":" + SecosanConstants.FILTER_NOT_STATUS);
				queryParams.put(SecosanConstants.FILTER_NOT_STATUS, Long.valueOf(f.getFilterValue()));
			} else if (f.getFilterName().equals(SecosanConstants.FIELD_STATUS)) {
				queryParts.setPart(WHEREPART_STATUS + "=" + ":" + SecosanConstants.FIELD_STATUS);
				queryParams.put(SecosanConstants.FIELD_STATUS, Long.valueOf(f.getFilterValue()));
			} else if (f.getFilterName().equals(SecosanConstants.FIELD_ID)) {
				queryParts.setPart(SecosanConstants.FIELD_IDENTIFICACIONPETICION + " LIKE" + ":" + IDENTIFICADOR_PETICION_FILTER);
				queryParams.put(IDENTIFICADOR_PETICION_FILTER, f.getFilterValue() + "%");
			} else {
				queryParts.setPart(f.getFilterName() + "=" + f.getFilterValue());
			}
		}
		return assambleQuery("", queryParts);
	}

}
