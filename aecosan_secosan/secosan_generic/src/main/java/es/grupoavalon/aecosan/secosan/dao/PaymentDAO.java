package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;

public interface PaymentDAO {

	void savePayment(PagoSolicitudEntity paymentData);

	void updatePayment(PagoSolicitudEntity paymentData);

	PagoSolicitudEntity getPagoSolicitudEntityByPetitionNumber(String petitionNumber);

	PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeId(long solicitudId);
}