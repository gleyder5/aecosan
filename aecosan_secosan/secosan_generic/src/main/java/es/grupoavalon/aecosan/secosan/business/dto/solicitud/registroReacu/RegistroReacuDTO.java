package es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.LugarMedioNotificacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ProcedimientoGeneralDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlElement;

public class RegistroReacuDTO extends SolicitudDTO  implements IsNulable<RegistroReacuDTO> {

    @JsonProperty("lugarOMedio")
    @XmlElement(name = "lugarOMedio")
    @BeanToBeanMapping(generateOther = true, getValueFrom = "lugarOMedio")
    private LugarMedioNotificacionDTO lugarOMedio;

//    @JsonProperty("fechaBoe")
//    @XmlElement(name = "fechaBoe")
//    @BeanToBeanMapping(getValueFrom = "fechaBoeString")
//    private Long fechaBoe;

    public LugarMedioNotificacionDTO getLugarOMedio() {
        return lugarOMedio;
    }

    public void setLugarOMedio(LugarMedioNotificacionDTO lugarOMedio) {
        this.lugarOMedio = lugarOMedio;
    }

//    public Long getFechaBoe() {
//        return fechaBoe;
//    }
//
//    public void setFechaBoe(Long fechaBoe) {
//        this.fechaBoe = fechaBoe;
//    }


    @Override
    public RegistroReacuDTO shouldBeNull() {
        if (checkNullability()) {
            return null;
        }
        return this;
    }

    private boolean checkNullability() {
        boolean nullabillity = true;
        nullabillity = nullabillity && getId() == null;
        return nullabillity;
    }


}

