package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JustifierRequestDTO {

	@JsonProperty("personIdentificationNumber")
	@XmlElement(name = "personIdentificationNumber")
	private String personIdentificationNumber;
	@JsonProperty("ammount")
	@XmlElement(name = "ammount")
	private double ammount;
	@JsonProperty("quantity")
	@XmlElement(name = "quantity")
	private double quantity;
	@JsonProperty("accountData")
	@XmlElement(name = "accountData")
	private JustifierAccountRequestDTO accountPaymentData;
	@JsonProperty("creditCardData")
	@XmlElement(name = "creditCardData")
	private JustifierCreditCardRequestDTO creditCardPaymentData;
	@JsonProperty("formType")
	@XmlElement(name = "formType")
	private Long formType;
	@JsonProperty("serviceType")
	@XmlElement(name = "serviceType")
	private Long serviceType;

	public double getAmmount() {
		return ammount;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	public JustifierAccountRequestDTO getAccountPaymentData() {
		return accountPaymentData;
	}

	public void setAccountPaymentData(JustifierAccountRequestDTO accountPaymentData) {
		this.accountPaymentData = accountPaymentData;
	}

	public JustifierCreditCardRequestDTO getCreditCardPaymentData() {
		return creditCardPaymentData;
	}

	public void setCreditCardPaymentData(JustifierCreditCardRequestDTO creditCardPaymentData) {
		this.creditCardPaymentData = creditCardPaymentData;
	}

	public String getPersonIdentificationNumber() {
		return personIdentificationNumber;
	}

	public void setPersonIdentificationNumber(String personIdentificationNumber) {
		this.personIdentificationNumber = personIdentificationNumber;
	}

	public Long getFormType() {
		return formType;
	}

	public void setFormType(Long formType) {
		this.formType = formType;
	}

	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountPaymentData == null) ? 0 : accountPaymentData.hashCode());
		result = prime * result + Double.valueOf(ammount).byteValue();
		result = prime * result + ((creditCardPaymentData == null) ? 0 : creditCardPaymentData.hashCode());
		result = prime * result + ((formType == null) ? 0 : formType.hashCode());
		result = prime * result + ((personIdentificationNumber == null) ? 0 : personIdentificationNumber.hashCode());
		result = prime * result + ((serviceType == null) ? 0 : serviceType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustifierRequestDTO other = (JustifierRequestDTO) obj;
		if (accountPaymentData == null) {
			if (other.accountPaymentData != null) {
				return false;
			}
		} else if (!accountPaymentData.equals(other.accountPaymentData)) {
			return false;
		}
		if (Double.doubleToLongBits(ammount) != Double.doubleToLongBits(other.ammount)) {
			return false;
		}
		if (creditCardPaymentData == null) {
			if (other.creditCardPaymentData != null) {
				return false;
			}
		} else if (!creditCardPaymentData.equals(other.creditCardPaymentData)) {
			return false;
		}
		if (formType == null) {
			if (other.formType != null) {
				return false;
			}
		} else if (!formType.equals(other.formType)) {
			return false;
		}
		if (personIdentificationNumber == null) {
			if (other.personIdentificationNumber != null) {
				return false;
			}
		} else if (!personIdentificationNumber.equals(other.personIdentificationNumber)) {
			return false;
		}
		if (serviceType == null) {
			if (other.serviceType != null) {
				return false;
			}
		} else if (!serviceType.equals(other.serviceType)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "JustifierRequestDTO [personIdentificationNumber=" + personIdentificationNumber + ", ammount=" + ammount + ", accountPaymentData=" + accountPaymentData + ", creditCardPaymentData="
				+ creditCardPaymentData + ", formType=" + formType + ", serviceType=" + serviceType + "]";
	}

}
