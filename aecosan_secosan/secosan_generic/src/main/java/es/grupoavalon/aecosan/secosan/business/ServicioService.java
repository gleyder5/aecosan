package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.ServicioDTO;

public interface ServicioService {
	public List<ServicioDTO> findServicesByPayTypeId(String payTypeId);
}
