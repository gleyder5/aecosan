package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

import javax.xml.bind.annotation.XmlElement;


public class TipoProcedimientoDTO {
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("descripcion")
	@XmlElement(name = "descripcion")
	@BeanToBeanMapping( getValueFrom = "descripcion")
	private String descripcion;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "TipoProcedimientoDTO [id=" + id + "]";
	}

}
