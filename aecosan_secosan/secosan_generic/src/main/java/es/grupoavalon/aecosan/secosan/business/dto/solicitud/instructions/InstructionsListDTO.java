package es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions;

import java.util.List;

public class InstructionsListDTO {

	List<SolicitudeInstructionsDTO> listSolicitudeInstructions;
	List<PaymentInstructionsDTO> listPaymentInstructions;

	public List<SolicitudeInstructionsDTO> getListSolicitudeInstructions() {
		return listSolicitudeInstructions;
	}

	public void setListSolicitudeInstructions(List<SolicitudeInstructionsDTO> listSolicitudeInstructions) {
		this.listSolicitudeInstructions = listSolicitudeInstructions;
	}

	public List<PaymentInstructionsDTO> getListPaymentInstructions() {
		return listPaymentInstructions;
	}

	public void setListPaymentInstructions(List<PaymentInstructionsDTO> listPaymentInstructions) {
		this.listPaymentInstructions = listPaymentInstructions;
	}

}
