package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.IdentificacionPeticionDTO;

public interface IdentificacionPeticionService {

	IdentificacionPeticionDTO getIdentificationRequestByAreaId(String areaId);
}
