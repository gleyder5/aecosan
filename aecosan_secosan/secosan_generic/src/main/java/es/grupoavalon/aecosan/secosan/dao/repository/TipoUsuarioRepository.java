package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoUsuarioEntity;

public interface TipoUsuarioRepository extends CrudRepository<TipoUsuarioEntity, Long>{

}
