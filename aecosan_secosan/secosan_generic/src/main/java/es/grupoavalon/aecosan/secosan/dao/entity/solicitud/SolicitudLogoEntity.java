package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_SOLICITUD_LOGO)
public class SolicitudLogoEntity extends
		SolicitudEntity {
	
	@Column(name = "MATERIAL_SOLICITADO")
	private String requestedMaterial;

	@Column(name = "FINALIDAD_Y_USO")
	private String purposeAndUse;

	public String getRequestedMaterial() {
		return requestedMaterial;
	}

	public void setRequestedMaterial(String requestedMaterial) {
		this.requestedMaterial = requestedMaterial;
	}

	public String getPurposeAndUse() {
		return purposeAndUse;
	}

	public void setPurposeAndUse(String purposeAndUse) {
		this.purposeAndUse = purposeAndUse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((purposeAndUse == null) ? 0 : purposeAndUse.hashCode());
		result = prime * result + ((requestedMaterial == null) ? 0 : requestedMaterial.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitudLogoEntity other = (SolicitudLogoEntity) obj;
		if (purposeAndUse == null) {
			if (other.purposeAndUse != null)
				return false;
		} else if (!purposeAndUse.equals(other.purposeAndUse))
			return false;
		if (requestedMaterial == null) {
			if (other.requestedMaterial != null)
				return false;
		} else if (!requestedMaterial.equals(other.requestedMaterial))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SolicitudLogoEntity [requestedMaterial=" + requestedMaterial + ", purposeAndUse=" + purposeAndUse + "]";
	}


}
