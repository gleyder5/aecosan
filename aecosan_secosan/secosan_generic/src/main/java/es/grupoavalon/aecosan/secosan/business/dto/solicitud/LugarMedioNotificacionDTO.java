package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import org.apache.commons.lang.StringUtils;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;


public class LugarMedioNotificacionDTO implements IsNulable<LugarMedioNotificacionDTO> {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;
	@JsonProperty("fax")
	@XmlElement(name = "fax")
	@BeanToBeanMapping(getValueFrom = "fax")
	private String fax;
	@JsonProperty("email")
	@XmlElement(name = "email")
	@BeanToBeanMapping(getValueFrom = "email")
	private String email;
	@JsonProperty("address")
	@XmlElement(name = "address")
	@BeanToBeanMapping(getValueFrom = "address")
	private String address;
	@JsonProperty("location")
	@XmlElement(name = "location")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "location")
	private UbicacionGeograficaDTO location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UbicacionGeograficaDTO getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaDTO location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LugarMedioNotificacionDTO that = (LugarMedioNotificacionDTO) o;

		if (!id.equals(that.id)) return false;
		if (!address.equals(that.fax)) return false;
		if (!fax.equals(that.fax)) return false;
		return email.equals(that.email);
	}

	@Override
	public int hashCode() {
		int result = id.hashCode();
		result = 31 * result + address.hashCode();
		result = 31 * result + email.hashCode();
		result = 31 * result + fax.hashCode();
		return result;
	}
	private boolean checkNullability() {
		boolean nullabillity = true;
		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && StringUtils.isEmpty(this.email.toString());
		nullabillity = nullabillity && getLocation()==null;


		return nullabillity;
	}

	@Override
	public LugarMedioNotificacionDTO shouldBeNull() {
		return null;
	}
}
