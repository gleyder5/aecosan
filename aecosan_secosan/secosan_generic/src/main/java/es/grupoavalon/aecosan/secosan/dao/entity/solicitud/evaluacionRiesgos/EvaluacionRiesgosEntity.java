package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_EVALUACION_RIESGOS)
public class EvaluacionRiesgosEntity extends SolicitudEntity {

	@Column(name = "OBJETO_SOLICITUD")
	private String solicitudeSubject;

	@Column(name = "NOMBRE_COMERCIAL")
	private String commercialProductName;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_PAGO_SERVICIO_ID")
	private TipoPagoServicioEntity payTypeService;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PAGO_SOLICITUD_ID")
	private PagoSolicitudEntity solicitudePayment;

	public String getSolicitudeSubject() {
		return solicitudeSubject;
	}

	public void setSolicitudeSubject(String solicitudeSubject) {
		this.solicitudeSubject = solicitudeSubject;
	}

	public String getCommercialProductName() {
		return commercialProductName;
	}

	public void setCommercialProductName(String commercialProductName) {
		this.commercialProductName = commercialProductName;
	}

	public TipoPagoServicioEntity getPayTypeService() {
		return payTypeService;
	}

	public void setPayTypeService(TipoPagoServicioEntity payTypeService) {
		this.payTypeService = payTypeService;
	}

	public PagoSolicitudEntity getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudEntity solicitudePayment) {
		if (solicitudePayment != null) {
			solicitudePayment.setIdentificadorPeticion(this.getIdentificadorPeticion());
		}
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((commercialProductName == null) ? 0 : commercialProductName.hashCode());
		result = prime * result + ((payTypeService == null) ? 0 : payTypeService.getId().hashCode());
		result = prime * result + ((solicitudePayment == null) ? 0 : solicitudePayment.getId().hashCode());
		result = prime * result + ((solicitudeSubject == null) ? 0 : solicitudeSubject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EvaluacionRiesgosEntity other = (EvaluacionRiesgosEntity) obj;
		if (commercialProductName == null) {
			if (other.commercialProductName != null) {
				return false;
			}
		} else if (!commercialProductName.equals(other.commercialProductName)) {
			return false;
		}
		if (payTypeService == null) {
			if (other.payTypeService != null) {
				return false;
			}
		} else if (!payTypeService.getId().equals(other.payTypeService.getId())) {
			return false;
		}
		if (solicitudePayment == null) {
			if (other.solicitudePayment != null) {
				return false;
			}
		} else if (!solicitudePayment.getId().equals(other.solicitudePayment.getId())) {
			return false;
		}
		if (solicitudeSubject == null) {
			if (other.solicitudeSubject != null) {
				return false;
			}
		} else if (!solicitudeSubject.equals(other.solicitudeSubject)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String payTypeServiceId = null;
		String solicitudePaymentId = null;

		if (payTypeService != null) {
			payTypeServiceId = String.valueOf(payTypeService.getId());
		} else {
			payTypeServiceId = "";
		}

		if (solicitudePayment != null) {
			solicitudePaymentId = String.valueOf(solicitudePayment.getId());
		} else {
			solicitudePaymentId = "";
		}

		return "EvaluacionRiesgosEntity [solicitudeSubject=" + solicitudeSubject + ", commercialProductName=" + commercialProductName + ", payTypeService=" + payTypeServiceId + ", solicitudePayment="
				+ solicitudePaymentId + "]";
	}

}
