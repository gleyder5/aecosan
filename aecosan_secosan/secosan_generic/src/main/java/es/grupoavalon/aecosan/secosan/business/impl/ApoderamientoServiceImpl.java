package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.connector.apoderamiento.ApoderamientoConnectorApi;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoRequestDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;
import es.grupoavalon.aecosan.connector.apoderamiento.exception.ApoderamientoConnectorOperationException;
import es.grupoavalon.aecosan.connector.apoderamiento.impl.ApoderamientoConnectorApiImpl;


import es.grupoavalon.aecosan.secosan.business.ApoderamientoService;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;

import es.grupoavalon.aecosan.secosan.business.exception.ApoderamientoException;

import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ApoderamientoServiceImpl extends AbstractManager implements ApoderamientoService {

    @Autowired
    private ConnectorMonitoringService monitoringService;

    private String pasarelaUrl;
    private int organismo;
    @PostConstruct
    public void instanciateParams() {
        this.pasarelaUrl = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_APODERAMIENTO_URL);
        this.organismo = Properties.getInt(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_APODERAMIENTO_AECOSAN_ID);
    }
    private static ApoderamientoConnectorApi getApoderamientoConnectorApiInstance(){
        return new ApoderamientoConnectorApiImpl();
    }

    private ConsultarApoderamientoRequestDTO instanciateConsultarApoderamientoRequestDTO(String nif) {
        ConsultarApoderamientoRequestDTO consultarApoderamientoRequestDTO =  new ConsultarApoderamientoRequestDTO();
        consultarApoderamientoRequestDTO.setUrl(pasarelaUrl);

        consultarApoderamientoRequestDTO.setIdOrganismo(String.valueOf(organismo));
        consultarApoderamientoRequestDTO.setCIFPoderdante(nif);
        consultarApoderamientoRequestDTO.setNIFPoderdante(nif);
        return consultarApoderamientoRequestDTO;
    }

    @Override
    public ConsultarApoderamientoResponseDTO consultarApoderamiento(String nif) {
        ConsultarApoderamientoResponseDTO  connectorResponse  = null;
        try {
            ApoderamientoConnectorApi apoderamientoConnector = getApoderamientoConnectorApiInstance();
            ConsultarApoderamientoRequestDTO connectorRequestPayload = instanciateConsultarApoderamientoRequestDTO(nif);
            logger.debug("consultar apoderamiento dto: {0}", connectorRequestPayload);
            connectorResponse = apoderamientoConnector.consultarApoderamiento(connectorRequestPayload);
            logger.debug("consulta apoderamiento response dto: {0}", connectorResponse);
            return connectorResponse;
        } catch (ApoderamientoConnectorOperationException ap) {
            this.logger.error("error while trying to request to apoderamiento service ", ap);
            this.monitoringService.monitoringError(MonitorizableConnectors.APODERAMINETO, ap);
            throw  new ApoderamientoException(ap.getMessage(),ap.getErrorCode(),ap.getErrorDescription());
        }
        catch (Exception e) {
            this.logger.error("error while trying to request to apoderamiento service {0}", e);
            this.monitoringService.monitoringError(MonitorizableConnectors.APODERAMINETO, e);
            this.handleException(e);
        }
        return  connectorResponse;

    }
}
