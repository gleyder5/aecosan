/**
 * 
 */
package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.abstractobjfactory.DefaultRelatedObjectFactory;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.abstractobjfactory.RelatedObjectFactory;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IsAbstractRelatedObject;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanInstantiationException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.FactoryFillingException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.CustomValueTransformer;

/**
 * @author otto.abreu
 *
 */
public class ReverseBeanFromBeanFactory<T> extends AbstractBeanFactory<T> {

	private Object dtoInstance;
	private Field[] annotatedFields;

	// LOGGER
	private static final Logger logger = LoggerFactory
			.getLogger(ReverseBeanFromBeanFactory.class);

	/**
	 * Returns an instance of this factory
	 * 
	 * @param dtoInstance
	 *            Object dto (should be annotated)
	 * @param entityClass
	 *            Class of the desired entity
	 * @return {@link BeanFromBeanFactory}
	 */
	public static ReverseBeanFromBeanFactory<?> getInstance(Object dtoInstance,
			Class<?> entityClass) {

		ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
				dtoInstance, entityClass);
		return factoryInstance;
	}

	private ReverseBeanFromBeanFactory(Object dtoInstance, Class<?> entityClass) {
		super(entityClass);
		this.dtoInstance = dtoInstance;
		Class<?> dtoClass = getClassFromObject(dtoInstance);
		this.annotatedFields = getFields(dtoClass);

	}

	/**
	 * Constructor that not sets the entity, use it when it is necessary to
	 * create a list
	 * 
	 * @param dtoClass
	 * @return
	 */
	public static ReverseBeanFromBeanFactory<?> getInstance(Class<?> entityClass) {

		ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
				entityClass);
		return factoryInstance;
	}

	/**
	 * Constructor
	 * 
	 * @param toInstantiateClass
	 */
	protected ReverseBeanFromBeanFactory(Class<?> toInstantiateClass) {
		super(toInstantiateClass);
	}

	/**
	 * Execute the reverse generation of an object, this means from the
	 * annotated object ( DTO) to a non annotated (Entity)
	 * 
	 * @return
	 */
	public T generateEntityFromDTO() {
		return this.generateObjectFromOtherObject(this.annotatedFields);
	}

	/**
	 * Execute the reverse generation of an object, this means from the
	 * annotated object ( DTO) to a non annotated (Entity) but ignores the
	 * fields that extends from collection
	 * 
	 * @return
	 */
	public T generateEntityFromDTONoCollections() {

		return includeExcludeCollectionsFromNewInstance(
				FilterCollectionFields.FILTER_FIELDS_NOCOLLECTIONS,
				this.annotatedFields);
	}

	/**
	 * Execute the reverse generation of an object, this means from the
	 * annotated object ( DTO) to a non annotated (Entity) but ignores the
	 * fields that do not extends from collection (normal fields are exclude)
	 * 
	 * @return
	 */
	public T generateEntityFromDTOOnlyCollections()
			throws BeanInstantiationException, BeanFactoryException {

		return includeExcludeCollectionsFromNewInstance(
				FilterCollectionFields.FILTER_FIELDS_ONLYCOLLECTIONS,
				this.annotatedFields);
	}

	/**
	 * Execute the reverse generation of a list of objects, this means from the
	 * annotated objects (DTO) to a non annotated (Entity)
	 * 
	 * @param dtoList
	 * @return List<T>
	 */
	public List<T> generateListEntityFromDTO(List<?> dtoList) {
		List<T> listEntity = new ArrayList<T>();

		for (Object dto : dtoList) {
			ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
					dto, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T entity = (T) factoryInstance.generateEntityFromDTO();
			listEntity.add(entity);

		}

		return listEntity;
	}

	/**
	 * Execute the reverse generation of a list of objects, this means from the
	 * annotated objects (DTO) to a non annotated (Entity) but ignores the
	 * fields that extends from collection
	 * 
	 * @param dtoList
	 * @return List<T>
	 */
	public List<T> generateListEntityFromDTONoCollections(List<?> dtoList) {
		List<T> listDto = new ArrayList<T>();

		for (Object dto : dtoList) {
			ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
					dto, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T entity = (T) factoryInstance.generateEntityFromDTONoCollections();
			listDto.add(entity);
		}

		return listDto;
	}

	/**
	 * Execute the reverse generation of a list of objects, this means from the
	 * annotated objects (DTO) to a non annotated (Entity) but ignores the
	 * fields that do not extends from collection (normal fields)
	 * 
	 * @param dtoList
	 * @return List<T>
	 */
	public List<T> generateListEntityFromDTOOnlyCollections(List<?> dtoList) {
		List<T> listDto = new ArrayList<T>();

		for (Object dto : dtoList) {
			ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
					dto, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T entity = (T) factoryInstance
					.generateEntityFromDTOOnlyCollections();
			listDto.add(entity);
		}

		return listDto;
	}

	@Override
	protected void moveValuesFromObjectToNewInstance(Field[] fields,
			T newInstance) throws FactoryFillingException, BeanFactoryException {

		try {
			for (Field field : fields) {
				this.executeMappingInField(field, newInstance);
			}
		} catch (Exception e) {
			handlleReverseObjectCreationException(e);
		}

	}

	private void executeMappingInField(Field dtoAnnotatedfield,
			T newEntityInstance) throws SecurityException,
			NoSuchFieldException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		
		if (!ignoreInReverse(dtoAnnotatedfield)) {
			BeanToBeanMapping annotation = dtoAnnotatedfield
					.getAnnotation(BeanToBeanMapping.class);
			logger.debug("annotation found:" + annotation);
			// the field is a collection of others entities
			if (fieldIsACollectionOfOtherBean(annotation)) {

				this.executeFieldIsACollectionOfOtherBean(annotation,
						dtoAnnotatedfield, newEntityInstance);
				// is a 1 on 1 mapping Object Java bean
			} else if (fieldIsOneToOtherMapping(annotation)) {
				executeSetMethodOnetoOneRelationBeanObject(annotation,
						dtoAnnotatedfield, newEntityInstance);
				// is a 1 on 1 mapping
			} else if (fieldIsOneToOneMapping(annotation)) {

				this.executeSetMethodOnetoOneRelation(annotation,
						dtoAnnotatedfield, newEntityInstance);

				// is necessary to obtain the value from other entity
			} else if (fieldValueIsObtainedFromOtherBean(annotation)) {

				this.executeSetInRelatedObject(annotation, dtoAnnotatedfield,
						newEntityInstance);
				// the annotation is present but there is no information to
				// Retrieve the value
			} else if (fieldHaveAnnotationButMissingConfiguration(annotation)) {
				logger.debug("Field have the same name in both objects");
				this.executeSetMethodOnetoOneRelationSameName(annotation,
						dtoAnnotatedfield, newEntityInstance);

			} else {
				logger.warn("Can't map the given field "
						+ dtoAnnotatedfield.getName()
						+ " because the mapping annotation is not configured properly or does not have any annotation (ignore)");
			}

		}else {
			logger.debug("Ignoring (ignore annotation configured in reverse)" + dtoAnnotatedfield.getName());
		}
	}
	
	private static boolean ignoreInReverse(Field field){
		boolean ignore = false;
	
		if(field.isAnnotationPresent(IgnoreField.class)){
			IgnoreField ignoreAnnotation = field.getAnnotation(IgnoreField.class);
			ignore = ignoreAnnotation.inReverse();
		}
		
		return ignore;
	}

	private void executeSetMethodOnetoOneRelation(BeanToBeanMapping annotation,
			Field field, T entityInstance) throws SecurityException,
			NoSuchFieldException {
		logger.debug("reverse mapping 1 on 1  mapping");

		Object obtainedValueFromDto = this.executeGetMethod(field.getName(),
				this.dtoInstance);
		String fieldName = annotation.getValueFrom();

		this.setValueInEntity(obtainedValueFromDto, annotation, fieldName,
				entityInstance);
	}

	private void executeSetMethodOnetoOneRelationBeanObject(
			BeanToBeanMapping annotation, Field field, T entityInstance)
			throws NoSuchFieldException {
		logger.debug("reverse mapping 1 on 1  mapping Generate Other");

		String fieldName = getFieldNameToGetValueFrom(field.getName(),
				annotation);

		Field entityField = getDeclaredField(fieldName,
				entityInstance.getClass());

		Object obtainedValueFromDto = this.executeGetMethod(field.getName(),
				this.dtoInstance);
		
		if (obtainedValueFromDto != null) {
			ReverseBeanFromBeanFactory<?> factory = ReverseBeanFromBeanFactory
					.getInstance(obtainedValueFromDto, entityField.getType());

			Object otherEntity = factory.generateEntityFromDTO();

			this.setValueInEntity(otherEntity, annotation, fieldName,
					entityInstance);
		}
	}

	private void executeSetMethodOnetoOneRelationSameName(
			BeanToBeanMapping annotation, Field field, T entityInstance)
			throws SecurityException, NoSuchFieldException {
		logger.debug("reverse mapping 1 on 1  mapping same name");
		String fieldName = getFieldNameToGetValueFrom(field.getName(),
				annotation);
		Object obtainedValueFromDto = this.executeGetMethod(fieldName,
				this.dtoInstance);

		this.setValueInEntity(obtainedValueFromDto, annotation, fieldName,
				entityInstance);

	}

	private void setValueInEntity(Object obtainedValueFromDto,
			BeanToBeanMapping annotation, String fieldName, T entityInstance)
			throws SecurityException, NoSuchFieldException {
		Class<?> setClass = null;
		Field entityField = getDeclaredField(fieldName, this.toInstatiate);
		Class<?> fieldClass = entityField.getType();

		if (obtainedValueFromDto != null) {
			logger.debug("Obtained value from gette method: "
					+ obtainedValueFromDto);
			if (fieldUseTransformer(annotation)) {
				logger.debug("Obtained value use a custom transformer");
				obtainedValueFromDto = this.executeReverseTransformer(
						annotation, obtainedValueFromDto, fieldClass);
			}
			logger.debug("field type:" + entityField.getType());
			if (fieldIsPrimitive(entityField)) {
				logger.debug("is primitive value");
				setClass = fieldClass;
			}

			this.executeSetMethod(fieldName, obtainedValueFromDto,
					entityInstance, setClass);
		}
	}

	private Object executeReverseTransformer(BeanToBeanMapping annotation,
			Object value, Class<?> outputType) {

		Object transformedValue = null;
		Class<? extends CustomValueTransformer> transformerClass = annotation
				.useCustomTransformer();
		logger.debug("Obtained transformer class: " + transformerClass);
		CustomValueTransformer transformer = null;

		try {
			transformer = instanciateTransformer(transformerClass);
			transformedValue = transformer.reverseTransform(value, outputType);
			logger.debug("transformed value: " + transformedValue);
		} catch (Exception e) {
			this.handleTransformerInstanciationException(e);
		}

		return transformedValue;
	}

	private void executeSetInRelatedObject(BeanToBeanMapping annotation,
			Field fieldDto, T entityInstance) throws SecurityException,
			NoSuchFieldException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {
		logger.debug("Is related object");
		String fieldName = annotation.getValueFrom();
		String relatedFieldName = annotation.getSecondValueFrom();
		logger.debug("Entity field relatedObject:" + fieldName);
		Field entityField = getDeclaredField(fieldName, this.toInstatiate);

		Object obtainedValueFromDto = this.executeGetMethod(fieldDto.getName(),
				this.dtoInstance);
		logger.debug("obtained value from dto:" + obtainedValueFromDto);
		if (obtainedValueFromDto != null) {
			Object relatedObject = this.getRelatedObject(entityField,fieldDto,
					entityInstance);

			executeSetMethodInRelatedObject(relatedFieldName, relatedObject,
					obtainedValueFromDto, annotation);

		}

	}

	private Object getRelatedObject(Field field, Field fieldDto,Object entity)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, InstantiationException,
			IllegalAccessException, InvocationTargetException {

		String fieldName = field.getName();

		Object relatedObject = this.executeGetMethod(fieldName, entity);
		relatedObject = lazyInstantiateRelatedObject(field,fieldDto, entity,
				relatedObject);
		return relatedObject;
	}

	private void executeSetMethodInRelatedObject(String fieldRelatedName,
			Object relatedObject, Object value, BeanToBeanMapping annotation)
			throws SecurityException, IllegalArgumentException,
			NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, NoSuchFieldException {

		Class<?> relatedObjectClass = relatedObject.getClass();
		Field relatedObjectField = getDeclaredField(fieldRelatedName,
				relatedObjectClass);
		Class<?> relatedTypeClass = relatedObjectField.getType();

		if (fieldUseTransformer(annotation)) {
			logger.debug("Obtained related value use a custom transformer");
			value = this.executeReverseTransformer(annotation, value,
					relatedTypeClass);
		}

		executeGenericSetMethod(fieldRelatedName, value, relatedObject,
				relatedTypeClass, relatedObjectClass);

	}

	private  Object lazyInstantiateRelatedObject(Field field,Field fieldDto,
			Object entity, Object relatedObject) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException {

		if (relatedObject == null) {
			IsAbstractRelatedObject isAbstractObject = fieldDto.getAnnotation(IsAbstractRelatedObject.class);
			RelatedObjectFactory objectFactory = null;
			if(isAbstractObject != null){
				Class <? extends RelatedObjectFactory > customFactoryClass = isAbstractObject.instancefactory();
				//PASAR EL OBJETO DTO QUE ES PRIVADO
				objectFactory = RelatedObjectFactory.instanciateFactory(customFactoryClass,field,entity,this.dtoInstance,isAbstractObject);
			
			}else{
				objectFactory = RelatedObjectFactory.instanciateFactory(DefaultRelatedObjectFactory.class,field,entity);
			}

			relatedObject = objectFactory.instanciateObject();
			executeGenericSetMethod(field.getName(), relatedObject, entity,
					objectFactory.getRelatedObjectClass(), entity.getClass());
			logger.debug("Related object instantiated");
		}
		return relatedObject;
	}

	@SuppressWarnings("rawtypes")
	private void executeFieldIsACollectionOfOtherBean(
			BeanToBeanMapping annotation, Field fieldDto, T entityInstanc)
			throws SecurityException, NoSuchFieldException {

		String collectionFieldNameInEntity = getFieldNameToGetValueFrom(
				fieldDto.getName(), annotation);
		Class<?> relatedEntityClass = annotation.generateListFrom();
		logger.debug("Entity list from:" + relatedEntityClass);
		if (relatedEntityClass != Object.class) {

			Collection<?> relatedListValueFromDTO = (Collection<?>) this
					.executeGetMethod(fieldDto.getName(), this.dtoInstance);

			Class<? extends Collection> listClass = annotation
					.listReverseSetterClass();
			Collection<Object> newEtityList = transformCollectionType(listClass);

			for (Object relatedDtoInList : relatedListValueFromDTO) {

				this.instanciateNewEntityFromListDTO(relatedDtoInList,
						relatedEntityClass, newEtityList);
			}

			Field collectionField = getDeclaredField(
					collectionFieldNameInEntity, this.toInstatiate);
			this.executeSetMethod(collectionFieldNameInEntity, newEtityList,
					entityInstanc, collectionField.getType());

		} else {
			throw new BeanFactoryException(
					"Missing annotation generateListFrom, please indicate the reverse object to instanciate in the class"
							+ this.toInstatiate);
		}
	}

	private void instanciateNewEntityFromListDTO(Object otherDto,
			Class<?> otherEntityClass, Collection<Object> newEtityList) {

		// execute the same process of 1 on 1 mapping
		ReverseBeanFromBeanFactory<?> factoryInstance = new ReverseBeanFromBeanFactory<Object>(
				otherDto, otherEntityClass);

		Object newDTOInstance = factoryInstance.generateEntityFromDTO();
		newEtityList.add(newDTOInstance);

	}
}
