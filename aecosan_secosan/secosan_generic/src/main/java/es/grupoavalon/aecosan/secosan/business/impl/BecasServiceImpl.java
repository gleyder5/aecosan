package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.BecasService;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.BecasDAO;
import es.grupoavalon.aecosan.secosan.dao.IdiomasBecasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BecasServiceImpl extends AbstractSolicitudService implements BecasService {

	@Autowired
	private BecasDAO becasDAO;

	@Autowired
	private IdiomasBecasDAO idiomasBecasDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO becasDTO) {
		try {
			solicitudPrepareNullDTO(becasDTO);
			prepareBecasNullability((BecasDTO) becasDTO);
			addBecasEntity(user, (BecasDTO) becasDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + becasDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO becasDTO) {
		try {
			solicitudPrepareNullDTO(becasDTO);
			prepareBecasNullability((BecasDTO) becasDTO);
			updateProcediminetoGeneral(user, (BecasDTO) becasDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + becasDTO);
		}
	}

	private void addBecasEntity(String user, BecasDTO becasDTO) {
		UbicacionGeograficaDTO location = null;

		BecasEntity becasEntity	=	generateBecasEntityFromDTO(becasDTO);
		setUserCreatorByUserId(user, becasEntity);
		prepareAddSolicitudEntity(becasEntity,becasDTO);
//		becasEntity.setLanguages(AbstractManager.transformDTOListToEntityList(becasDTO.getLanguages(), IdiomasBecasEntity.class, IdiomasBecasDTO.class));

		BecasEntity becasEntityNew	=	becasDAO.addBecasEntity(becasEntity);

		for(IdiomasBecasDTO idiomasBecasDTO : becasDTO.getLanguages() ){
			IdiomasBecasEntity idiomasBecasEntity = idiomasBecasDAO.findIdiomasBecasEntityByBecaAndIdioma(becasEntity,idiomasBecasDTO.getNombre());
			if(idiomasBecasEntity==null){
				idiomasBecasEntity = transformDTOToEntity(idiomasBecasDTO,IdiomasBecasEntity.class);
				idiomasBecasEntity.setBeca(becasEntity);
				idiomasBecasDAO.addIdiomasBecasEntity(idiomasBecasEntity);
			}else {

			}
		}
		if(becasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(becasEntityNew);
		}
	}

	private void updateProcediminetoGeneral(String user, BecasDTO becasDTO) {

		BecasEntity becasEntity= transformDTOToEntity(becasDTO, BecasEntity.class);
		setUserCreatorByUserId(user, becasEntity);
		prepareUpdateSolicitudEntity(becasEntity, becasDTO);

		idiomasBecasDAO.deleteIdiomasBecasByBeca(becasEntity);

		List<IdiomasBecasEntity> idiomasBecasEntities = new ArrayList<IdiomasBecasEntity>();
		for(IdiomasBecasDTO idiomasBecasDTO : becasDTO.getLanguages()){
			IdiomasBecasEntity idiomasBecasEntity = transformDTOToEntity(idiomasBecasDTO,IdiomasBecasEntity.class);
			idiomasBecasEntity.setBeca(becasEntity);
			IdiomasBecasEntity idBecasEntity  = idiomasBecasDAO.addIdiomasBecasEntity(idiomasBecasEntity);
			idiomasBecasEntities.add(idBecasEntity);
		}
		becasEntity.setLanguages(idiomasBecasEntities);

		becasDAO.updateBecasEntity(becasEntity);
		if(becasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(becasEntity);
		}

	}

	private BecasEntity generateBecasEntityFromDTO(BecasDTO becas) {
		prepareSolicitudDTO(becas);
		return transformDTOToEntity(becas,BecasEntity.class);
	}
	private void prepareBecasNullability(BecasDTO becasDTO) {
		if(becasDTO.getLanguages().size()>3){
			Iterator<IdiomasBecasDTO> idiomaIT= becasDTO.getLanguages().iterator();
			while(idiomaIT.hasNext()){
				IdiomasBecasDTO idioma  = idiomaIT.next();
				if(idioma.getNombre().length()<2){
					idiomaIT.remove();
				}
			}
		}

	}

	@Override
	public List<BecasSolicitanteDTO> getBecasSolicitantesDTOPaginated(String[] otheParams, PaginationParams paginationParams) {
		List<BecasSolicitanteDTO> listDTO = new ArrayList<BecasSolicitanteDTO>();
		List<BecasEntity> listEntity;

		try {
			listEntity = becasDAO.getBecasListPaginated(paginationParams);

			for(BecasEntity beca : listEntity){

				BecasSolicitanteDTO becasSolicitanteDTO = new BecasSolicitanteDTO();
				becasSolicitanteDTO.setId(Integer.parseInt(Long.toString(beca.getId())));
				becasSolicitanteDTO.setNombre(beca.getSolicitante().getName());
				becasSolicitanteDTO.setApellido(beca.getSolicitante().getLastName());
				becasSolicitanteDTO.setDNI(beca.getSolicitante().getIdentificationNumber());
				becasSolicitanteDTO.setScore(beca.getScore());
				listDTO.add(becasSolicitanteDTO);

			}
			Collections.sort(listDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getSolicList | ERROR: " + e.getMessage());
		}

		return listDTO;
	}

	@Override
	public int getTotal(List<FilterParams> filterParams) {
		int totalRecords = 0;
		try {
			totalRecords = becasDAO.getTotal(filterParams	);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListRecords | ERROR: " + exception.getMessage());
		}
		return totalRecords;
	}

	@Override
	public List<BecasSolicitanteDTO> getBecasSolicitantesDTO() {

		List<BecasEntity> becasList=  becasDAO.getBecasList();
		List<BecasSolicitanteDTO> becasSolicitanteDTOList =new ArrayList<BecasSolicitanteDTO>();
		for(BecasEntity beca : becasList){

			BecasSolicitanteDTO becasSolicitanteDTO = new BecasSolicitanteDTO();
			becasSolicitanteDTO.setId(Integer.parseInt(Long.toString(beca.getId())));
			becasSolicitanteDTO.setNombre(beca.getSolicitante().getName());
			becasSolicitanteDTO.setApellido(beca.getSolicitante().getLastName());
			becasSolicitanteDTO.setDNI(beca.getSolicitante().getIdentificationNumber());
			becasSolicitanteDTO.setScore(beca.getScore());
			becasSolicitanteDTOList.add(becasSolicitanteDTO);

		}
		Collections.sort(becasSolicitanteDTOList);
		return  becasSolicitanteDTOList;
	}

	@Override
	public Double getBecaScore(String becaId) {
		return  becasDAO.findBecasById(Long.valueOf(becaId)).getScore();
	}

	@Override
	public List<IdiomasBecasDTO> getIdiomasBecasDTOByBeca(Long id) {
		BecasEntity becasEntity = becasDAO.findBecasById(id);
		List<IdiomasBecasEntity> idiomasBecasEntityList = idiomasBecasDAO.findIdiomasBecasEntityByBeca(becasEntity);
		List<IdiomasBecasDTO> idiomasBecasDTOList = AbstractManager.transformEntityListToDTOList(idiomasBecasEntityList,IdiomasBecasEntity.class,IdiomasBecasDTO.class);
		return idiomasBecasDTOList;
	}

	@Override
	public List<BecasSolicitanteDTO> getBecasDTOByStatus(Long id) {
		List<BecasEntity> becasList=  becasDAO.getBecasByStatus(id);
		List<BecasSolicitanteDTO> becasSolicitanteDTOList =new ArrayList<BecasSolicitanteDTO>();
		for(BecasEntity beca : becasList){

			BecasSolicitanteDTO becasSolicitanteDTO = new BecasSolicitanteDTO();
			becasSolicitanteDTO.setId(Integer.parseInt(Long.toString(beca.getId())));
			becasSolicitanteDTO.setNombre(beca.getSolicitante().getName());
			becasSolicitanteDTO.setApellido(beca.getSolicitante().getLastName());
			becasSolicitanteDTO.setDNI(beca.getSolicitante().getIdentificationNumber());
			becasSolicitanteDTO.setScore(beca.getScore());
			becasSolicitanteDTOList.add(becasSolicitanteDTO);

		}
		Collections.sort(becasSolicitanteDTOList);
		return  becasSolicitanteDTOList;
	}
}
