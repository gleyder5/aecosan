package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ProcedimientoGeneralDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("procedimientoGeneralRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/procedimientoGeneral")
public class ProcedimientoGeneralRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response addProcedimientoGeneral(@PathParam("user") String user, ProcedimientoGeneralDTO procedimientoGeneralDTO) {
		logger.debug("-- Method addProcedimientoGeneral POST Init--");
		Response response = addSolicitud(user, procedimientoGeneralDTO);
		logger.debug("-- Method addProcedimientoGeneral POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/{user}")
	public Response updateProcedimientoGeneral(@PathParam("user") String user, ProcedimientoGeneralDTO procedimientoGeneralDTO) {
		logger.debug("-- Method updateProcedimientoGeneral PUT Init--");
		Response response = updateSolicitud(user, procedimientoGeneralDTO);
		logger.debug("-- Method updateProcedimientoGeneral PUT End--");
		return response;
	}

}
