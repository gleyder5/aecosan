package es.grupoavalon.aecosan.secosan.dao.entity.registroReacu;

import es.grupoavalon.aecosan.secosan.dao.entity.LugarMedioNotificacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = TableNames.TABLA_REGISTRO_REACU)

public class RegistroReacuEntity extends SolicitudEntity {


    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "LUGAR_MEDIO_NOTIFICACION_ID")
    private LugarMedioNotificacionEntity lugarOMedio;


//    @Column(name = "FECHA_BOE", nullable = false)
//    private Date fechaBoe;
//
//    @Transient
//    private Long fechaBoeString;

//    public Long getFechaBoeString() {
//        return fechaBoe.getTime();
//    }
//f
//    public void setFechaBoeString(Long fechaBoeString) {
//        this.fechaBoeString= fechaBoeString;
//        this.fechaBoe = new Date(fechaBoeString);
//    }
//
    public LugarMedioNotificacionEntity getLugarOMedio() {
        return lugarOMedio;
    }

    public void setLugarOMedio(LugarMedioNotificacionEntity lugarOMedio) {
        this.lugarOMedio = lugarOMedio;
    }

//    public Date getFechaBoe() {
//        return fechaBoe;
//    }
//
//    public void setFechaBoe(Date fechaBoe) {
//        this.fechaBoe = fechaBoe;
//    }
}
