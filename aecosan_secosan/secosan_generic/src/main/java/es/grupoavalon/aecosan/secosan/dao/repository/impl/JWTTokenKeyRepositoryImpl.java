package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.JWTTokenKeyEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.JWTTokenKeyRepository;

@Component
public class JWTTokenKeyRepositoryImpl extends AbstractCrudRespositoryImpl<JWTTokenKeyEntity, String> implements JWTTokenKeyRepository {

	@Override
	protected Class<JWTTokenKeyEntity> getClassType() {

		return JWTTokenKeyEntity.class;
	}

	@Override
	public List<JWTTokenKeyEntity> getCurrents() {
		Map<String, Object> param = new HashMap<String, Object>(1);
		param.put("admin", false);
		return this.findByNamedQuery(NamedQueriesLibrary.GET_CURRENT_TOKENKEY, param);
	}

	@Override
	public List<JWTTokenKeyEntity> getCurrentsAdmin() {
		Map<String, Object> param = new HashMap<String, Object>(1);
		param.put("admin", true);
		return this.findByNamedQuery(NamedQueriesLibrary.GET_CURRENT_TOKENKEY, param);
	}

}
