package es.grupoavalon.aecosan.secosan.business.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

import javax.xml.bind.annotation.XmlElement;

public class HistorialSolicitudDTO {
    @JsonProperty("id")
    @XmlElement(name = "id")
    @BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
    private String id;

    @JsonProperty("solicitud")
    @XmlElement(name = "solicitud")
    @BeanToBeanMapping(getValueFrom = "solicitud", getSecondValueFrom = "id")
    private Long solicitud;

    @JsonProperty("status")
    @XmlElement(name = "status")
    @BeanToBeanMapping(getValueFrom = "status", generateOther = true)
    private EstadoSolicitudDTO status;

    @JsonProperty("documento")
    @XmlElement(name = "documento")
    @BeanToBeanMapping(getValueFrom = "documento", generateOther = true)
    private DocumentacionDTO documento;

    @JsonProperty("descripcion")
    @XmlElement(name = "descripcion")
    @BeanToBeanMapping(getValueFrom = "descripcion")
    private String descripcion;

    @JsonProperty("fechaCreacion")
    @XmlElement(name = "fechaCreacion")
    @BeanToBeanMapping(getValueFrom = "fechaCreacionString")
    private Long fechaCreacion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Long solicitud) {
        this.solicitud = solicitud;
    }

    public EstadoSolicitudDTO getStatus() {
        return status;
    }

    public void setStatus(EstadoSolicitudDTO status) {
        this.status = status;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Long fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public DocumentacionDTO getDocumento() {
        return documento;
    }

    public void setDocumento(DocumentacionDTO documentacionDTO) {
        this.documento = documentacionDTO;
    }
}
