package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoProcedimientoDTO;

import javax.xml.soap.SAAJResult;
import java.util.List;

public interface TipoProcedimientoService {

	void add( TipoProcedimientoDTO TipoProcedimientoDTO);

	void update(TipoProcedimientoDTO TipoProcedimientoDTO);

	void delete(String userId, String tipoProcedimientoId);

	List<TipoProcedimientoDTO> getTipoProcedimientoDTOList();
}
