package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ModoAdministracionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.ModoAdministracionRepository;

@Component("ModoAdministracion")
public class ModoAdministracionRepositoryImpl extends CrudCatalogRepositoryImpl<ModoAdministracionEntity, Long>
		implements ModoAdministracionRepository, GenericCatalogRepository<ModoAdministracionEntity> {
	@Override
	protected Class<ModoAdministracionEntity> getClassType() {
		return ModoAdministracionEntity.class;
	}

	@Override
	public List<ModoAdministracionEntity> getCatalogList() {
		return findAllOrderByValue();
	}

}
