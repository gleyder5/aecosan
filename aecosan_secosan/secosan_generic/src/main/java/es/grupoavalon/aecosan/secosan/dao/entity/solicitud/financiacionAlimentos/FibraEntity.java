package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_FIBRA)
public class FibraEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.FIBRA)
	@SequenceGenerator(name = SequenceNames.FIBRA, sequenceName = SequenceNames.FIBRA, allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "FIBRA")
	private Boolean fiber;

	@Column(name = "TIPO_FIBRA")
	private String fiberType;

	@Column(name = "CANTIDAD_FIBRA")
	private String fiberQuantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getFiber() {
		return fiber;
	}

	public void setFiber(Boolean fiber) {
		this.fiber = fiber;
	}

	public String getFiberType() {
		return fiberType;
	}

	public void setFiberType(String fiberType) {
		this.fiberType = fiberType;
	}

	public String getFiberQuantity() {
		return fiberQuantity;
	}

	public void setFiberQuantity(String fiberQuantity) {
		this.fiberQuantity = fiberQuantity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fiber == null) ? 0 : fiber.hashCode());
		result = prime * result + ((fiberQuantity == null) ? 0 : fiberQuantity.hashCode());
		result = prime * result + ((fiberType == null) ? 0 : fiberType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FibraEntity other = (FibraEntity) obj;
		if (fiber == null) {
			if (other.fiber != null)
				return false;
		} else if (!fiber.equals(other.fiber))
			return false;
		if (fiberQuantity == null) {
			if (other.fiberQuantity != null)
				return false;
		} else if (!fiberQuantity.equals(other.fiberQuantity))
			return false;
		if (fiberType == null) {
			if (other.fiberType != null)
				return false;
		} else if (!fiberType.equals(other.fiberType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FibraEntity [id=" + id + ", fiber=" + fiber + ", fiberType=" + fiberType + ", fiberQuantity=" + fiberQuantity + "]";
	}

}
