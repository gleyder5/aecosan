package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.RegistroAguasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroAguasRepository;

@Repository
public class RegistroAguasDAOImpl extends GenericDao implements RegistroAguasDAO {

	@Autowired
	private RegistroAguasRepository registroAguasRepository;

	@Override
	public RegistroAguasEntity addRegistroAguas(RegistroAguasEntity registroAguas) {
		RegistroAguasEntity registroAguasEntity = null;
		try {
			registroAguasEntity = registroAguasRepository.save(registroAguas);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addRegistroAguas | registroAguas: " + registroAguas);
		}
		return registroAguasEntity;
	}

	@Override
	public void updateRegistroAguas(RegistroAguasEntity registroAguas) {
		try {
			registroAguasRepository.update(registroAguas);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateRegistroAguas | ERROR: " + registroAguas);
		}

	}

}
