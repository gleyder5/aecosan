package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;

public interface TipoPersonaDAO {
	
	TipoPersonaEntity findTipoPersona(Long pk);
}
