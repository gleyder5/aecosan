package es.grupoavalon.aecosan.secosan.dao.repository.impl;


import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesAsocuaeEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SubvencionesAsocuaeRepository;
import org.springframework.stereotype.Component;

@Component
public class SubvencionesAsocuaeRepositoryImpl extends AbstractCrudRespositoryImpl<SubvencionesAsocuaeEntity, Long> implements SubvencionesAsocuaeRepository {

	@Override
	protected Class<SubvencionesAsocuaeEntity> getClassType() {
		return SubvencionesAsocuaeEntity.class;
	}

}
