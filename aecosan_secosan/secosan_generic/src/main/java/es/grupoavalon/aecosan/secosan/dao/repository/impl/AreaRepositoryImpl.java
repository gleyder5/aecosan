package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.AreaRepository;

@Component
public class AreaRepositoryImpl extends AbstractCrudRespositoryImpl<AreaEntity, Long>
		implements AreaRepository {

	@Override
	protected Class<AreaEntity> getClassType() {
		return AreaEntity.class;
	}
}
