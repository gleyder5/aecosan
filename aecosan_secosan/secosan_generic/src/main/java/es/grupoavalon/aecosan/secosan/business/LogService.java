package es.grupoavalon.aecosan.secosan.business;

import java.util.Collection;

import es.grupoavalon.aecosan.secosan.business.dto.LogDTO;

public interface LogService {

	Collection<String> getLogsName();

	LogDTO getLogFile(String fileName);

}