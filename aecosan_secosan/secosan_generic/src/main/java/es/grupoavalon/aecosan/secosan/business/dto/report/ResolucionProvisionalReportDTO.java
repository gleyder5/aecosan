package es.grupoavalon.aecosan.secosan.business.dto.report;


import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.ResolucionProvisionalDTO;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class ResolucionProvisionalReportDTO {
    @JsonProperty("id")
    @XmlElement(name = "id")
    private String identificationNumber;

    @JsonProperty("formId")
    @XmlElement(name = "formId")
    private Long formId;

    @JsonProperty("formType")
    @XmlElement(name = "formType")
    private Long formType;

    @JsonProperty("reportName")
    @XmlElement(name = "reportName")
    private String reportName;

    @JsonProperty("date")
    @XmlElement(name = "date")
    private Long date;

    @JsonProperty("pdfB64")
    @XmlElement(name = "pdfB64")
    private String pdfB64;

    List<ResolucionProvisionalDTO> resolucionProvisionalDTOList = new ArrayList<ResolucionProvisionalDTO>();



    public String getPdfB64() {
        return pdfB64;
    }

    public void setPdfB64(String pdfB64) {
        this.pdfB64 = pdfB64;
    }

    public Long getFormType() {
        return formType;
    }

    public void setFormType(Long formType) {
        this.formType = formType;
    }


    public List<ResolucionProvisionalDTO> getResolucionProvisionalDTOList() {
        return resolucionProvisionalDTOList;
    }

    public void setResolucionProvisionalDTOList(List<ResolucionProvisionalDTO> resolucionProvisionalDTOList) {
        this.resolucionProvisionalDTOList = resolucionProvisionalDTOList;
    }

    public String getIdentificationNumber() {
        return identificationNumber;
    }

    public void setIdentificationNumber(String identificationNumber) {
        this.identificationNumber = identificationNumber;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }
}
