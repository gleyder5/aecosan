package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

@JsonInclude(Include.NON_NULL)
public class TasaDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;


	@JsonProperty("tasaCode")
	@XmlElement(name = "tasaCode")
	@BeanToBeanMapping(getValueFrom = "tasaCode")
	private String tasaCode;

	@JsonProperty("model")
	@XmlElement(name = "model")
	@BeanToBeanMapping(getValueFrom = "model")
	private String model;

	@JsonProperty("amount")
	@XmlElement(name = "amount")
	@BeanToBeanMapping(getValueFrom = "ammount", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String amount;

	@JsonProperty("description")
	@XmlElement(name = "description")
	@BeanToBeanMapping(getValueFrom = "catalogValue")
	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTasaCode() {
		return tasaCode;
	}

	public void setTasaCode(String tasaCode) {
		this.tasaCode = tasaCode;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getAmount() {

		return amount;
	}

	public void setAmount(String amount) {
		this.amount = parseAmmountStringToValidFloat(amount);
	}

	private static String parseAmmountStringToValidFloat(String ammountString) {
		String parsedFloat = ammountString;
		if (StringUtils.isNotBlank(parsedFloat) && parsedFloat.matches("^\\d+(,\\d+)*$")) {
			parsedFloat = StringUtils.replace(parsedFloat, ",", ".");
		} else if (StringUtils.isNotBlank(parsedFloat) && parsedFloat.matches("^(\\d+(\\.\\d{3})+)(,\\d+)$")) {
			parsedFloat = StringUtils.replace(parsedFloat, ".", "");
			parsedFloat = StringUtils.replace(parsedFloat, ",", ".");
		}
		return parsedFloat;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + ((tasaCode == null) ? 0 : tasaCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TasaDTO other = (TasaDTO) obj;
		if (amount == null) {
			if (other.amount != null) {
				return false;
			}
		} else if (!amount.equals(other.amount)) {
			return false;
		}
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (model == null) {
			if (other.model != null) {
				return false;
			}
		} else if (!model.equals(other.model)) {
			return false;
		}
		if (tasaCode == null) {
			if (other.tasaCode != null) {
				return false;
			}
		} else if (!tasaCode.equals(other.tasaCode)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "TasaDTO [id=" + id + ", tasaCode=" + tasaCode + ", model=" + model + ", amount=" + amount + ", description=" + description + "]";
	}


}
