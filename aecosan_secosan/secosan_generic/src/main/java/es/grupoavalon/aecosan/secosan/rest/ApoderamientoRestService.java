package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ApoderamientoException;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("apoderamientoRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/apoderamiento")
public class ApoderamientoRestService {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    protected BusinessFacade bfacade;
    @Autowired
    protected ResponseFactory responseFactory;
   @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Consumes({ MediaType.APPLICATION_JSON })
    @Path("/consultar/{nif}")
    public Response consultarApoderamiento(@PathParam("nif") String nif) {
        logger.debug("-- Method consultarApoderamiento GET Init--");
       Response response= null ;
       try {
            ConsultarApoderamientoResponseDTO consultarApoderamientoResponseDTO = bfacade.consultarApoderamiento(nif);
            response = this.responseFactory.generateOkGenericResponse(consultarApoderamientoResponseDTO);
       }catch (ApoderamientoException apodEx){
           logger.error("Error consultar Apoderamiento || nif: "+nif+"  ERROR: "+apodEx.getDescription());
           response = responseFactory.generateErrorResponseForApoderamientoException(apodEx);
       }
       catch (Exception ex){
           logger.error("Error consultar Apoderamiento || nif: "+ nif +" ERROR: "+ ex.getMessage());
           response = responseFactory.generateErrorResponse(ex);
       }
        logger.debug("-- Method consultarApoderamiento GET End--");
        return response;
    }
}
