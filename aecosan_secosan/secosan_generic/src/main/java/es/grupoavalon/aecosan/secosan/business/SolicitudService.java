package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;

public interface SolicitudService {

	void add(String user, SolicitudDTO solicitudDTO);

	void update(String user, SolicitudDTO solicitudDTO);
}
