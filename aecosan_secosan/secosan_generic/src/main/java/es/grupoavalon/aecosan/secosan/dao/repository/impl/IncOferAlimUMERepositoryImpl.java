package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.IncOferAlimUMERepository;

@Component
public class IncOferAlimUMERepositoryImpl extends AbstractCrudRespositoryImpl<IncOferAlimUMEEntity, Long> implements IncOferAlimUMERepository {

	@Override
	protected Class<IncOferAlimUMEEntity> getClassType() {
		return IncOferAlimUMEEntity.class;
	}
}
