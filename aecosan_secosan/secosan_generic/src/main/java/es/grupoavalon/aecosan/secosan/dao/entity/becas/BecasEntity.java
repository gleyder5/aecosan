package es.grupoavalon.aecosan.secosan.dao.entity.becas;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = TableNames.TABLA_BECAS_ENTITY)
public
@NamedQueries({
        @NamedQuery(name = NamedQueriesLibrary.GET_BECAS_BY_STATUS,query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE + ".becas.BecasEntity AS p WHERE status.id = :estadoSolicitudId" )

}) class BecasEntity extends SolicitudEntity {
//Becas de formación para el programa de trabajo del consejo de consumidores y usuarios

    @Column(name = "SCORE")
    private Double score =   0.0;


    @Column(name = "OTRA_TITULACION")
    private String otraTitulacion;

    @Column(name = "PUNTOS_OFIMATICA")
    private Integer puntosOfimatica;

    @Column(name = "PUNTOS_EXPEDIENTE")
    private Integer puntosExpediente;

    @Column(name = "PUNTOS_VOLUNTARIADO")
    private Integer puntosVoluntariado;

    @Column(name = "PUNTOS_ENTREVISTA")
    private Integer puntosEntrevista;

    @Column(name = "PUNTOS_OTRA_TITULACION")
    private Double otraTitulacionPuntos;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "beca", cascade = { CascadeType.MERGE, CascadeType.ALL })
    @JoinColumn(name = "BECAS_ID", referencedColumnName = "ID")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<IdiomasBecasEntity> languages;

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getOtraTitulacion() {
        return otraTitulacion;
    }

    public void setOtraTitulacion(String otraTitulacion) {
        this.otraTitulacion = otraTitulacion;
    }

    public Integer getPuntosOfimatica() {
        return puntosOfimatica;
    }

    public void setPuntosOfimatica(Integer puntosOfimatica) {
        this.puntosOfimatica = puntosOfimatica;
    }

    public Integer getPuntosVoluntariado() {
        return puntosVoluntariado;
    }

    public Integer getPuntosEntrevista() {
        return puntosEntrevista;
    }

    public void setPuntosEntrevista(Integer puntosEntrevista) {
        this.puntosEntrevista = puntosEntrevista;
    }

    public void setPuntosVoluntariado(Integer puntosVoluntariado) {
        this.puntosVoluntariado = puntosVoluntariado;
    }

    public Double getOtraTitulacionPuntos() {
        return otraTitulacionPuntos;
    }

    public void setOtraTitulacionPuntos(Double otraTitulacionPuntos) {
        this.otraTitulacionPuntos = otraTitulacionPuntos;
    }

    public List<IdiomasBecasEntity> getLanguages() {
        return languages;
    }

    public void setLanguages(List<IdiomasBecasEntity> languages) {
        this.languages = languages;
    }

    public Integer getPuntosExpediente() {
        return puntosExpediente;
    }

    public void setPuntosExpediente(Integer puntosExpediente) {
        this.puntosExpediente = puntosExpediente;
    }
}

