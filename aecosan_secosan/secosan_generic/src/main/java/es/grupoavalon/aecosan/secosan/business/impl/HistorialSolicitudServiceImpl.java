package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.HistorialSolicitudService;
import es.grupoavalon.aecosan.secosan.business.dto.HistorialSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistorialSolicitudServiceImpl extends  AbstractManager implements HistorialSolicitudService {

    @Autowired
    private HistorialSolicitudDAO historialSolicitudDAO;

    @Autowired
    private SolicitudDAO solicitudDAO;

    @Autowired
    private EstadoSolicitudDAO estadoSolicitudDAO;

    @Override
    public void add(SolicitudDTO solicitudDTO) {

        SolicitudEntity solicitudEntity = transformDTOToEntity(solicitudDTO,SolicitudEntity.class);
        historialSolicitudDAO.addHistorialSolicitud(solicitudEntity);

    }

    @Override
    public List<HistorialSolicitudDTO> getHistorialList(PaginationParams paginationParams) {
        List<HistorialSolicitudDTO> listDTO = null;
        List<HistorialSolicitudEntity> listEntity;
        try {
            listEntity = historialSolicitudDAO.getHistorialList(paginationParams);
            listDTO = transformEntityListToDTOList(listEntity, HistorialSolicitudEntity.class, HistorialSolicitudDTO.class);
        } catch (Exception e) {
            handleException(e, ServiceException.SERVICE_ERROR + "getHistorialSolicitudList | ERROR: " + e.getMessage());
        }
        return listDTO;
    }

    @Override
    public HistorialSolicitudDTO getBySolicitudAndStatus(Integer solicitudId, Integer statusId) {

        HistorialSolicitudEntity historialSolicitudEntity = null;
        HistorialSolicitudDTO historialSolicitudDTO = null;
        EstadoSolicitudEntity estadoSolicitudEntity = estadoSolicitudDAO.findEstadoSolicitud(Long.valueOf(statusId.intValue()));
        try {
            historialSolicitudEntity  = historialSolicitudDAO.getBySolicitudAndStatus(solicitudId,estadoSolicitudEntity);

            historialSolicitudDTO = transformEntityToDTO(historialSolicitudEntity, HistorialSolicitudDTO.class);
        } catch (Exception e) {
            handleException(e, ServiceException.SERVICE_ERROR + "getBySolicitudAndStatus | ERROR: " + e.getMessage());
        }
        return historialSolicitudDTO;
    }

    @Override
    public int getTotal(List<FilterParams> filters) {
        int totalRecords = 0;
        try {
            totalRecords = historialSolicitudDAO.getTotal(filters	);
        } catch (Exception exception) {
            handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListRecords | ERROR: " + exception.getMessage());
        }
        return totalRecords;
    }
}
