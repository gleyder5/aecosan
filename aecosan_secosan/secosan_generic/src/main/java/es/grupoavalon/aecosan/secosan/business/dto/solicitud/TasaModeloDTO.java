package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class TasaModeloDTO extends AbstractCatalogDto {

	@JsonProperty("tasaCode")
	@XmlElement(name = "tasaCode")
	@BeanToBeanMapping
	private String tasaCode;

	@JsonProperty("model")
	@XmlElement(name = "model")
	@BeanToBeanMapping
	private String model;

	@JsonProperty("ammount")
	@XmlElement(name = "ammount")
	@BeanToBeanMapping
	private Float ammount;

	public String getTasaCode() {
		return tasaCode;
	}

	public void setTasaCode(String tasaCode) {
		this.tasaCode = tasaCode;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Float getAmmount() {
		return ammount;
	}

	public void setAmmount(Float ammount) {
		this.ammount = ammount;
	}

}
