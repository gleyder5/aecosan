package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JustifierResponseDTO {

	@JsonProperty("justifierNumber")
	@XmlElement(name = "justifierNumber")
	private String justifierNumber;
	@JsonProperty("signData")
	@XmlElement(name = "signData")
	private String signData;

	public String getJustifierNumber() {
		return justifierNumber;
	}

	public void setJustifierNumber(String justifierNumber) {
		this.justifierNumber = justifierNumber;
	}

	public String getSignData() {
		return signData;
	}

	public void setSignData(String signData) {
		this.signData = signData;
	}

	@Override
	public String toString() {
		return "JustifierResponseDTO [justifierNumber=" + justifierNumber + ", signData=" + signData + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((justifierNumber == null) ? 0 : justifierNumber.hashCode());
		result = prime * result + ((signData == null) ? 0 : signData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustifierResponseDTO other = (JustifierResponseDTO) obj;
		if (justifierNumber == null) {
			if (other.justifierNumber != null) {
				return false;
			}
		} else if (!justifierNumber.equals(other.justifierNumber)) {
			return false;
		}
		if (signData == null) {
			if (other.signData != null) {
				return false;
			}
		} else if (!signData.equals(other.signData)) {
			return false;
		}
		return true;
	}


}
