package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsFileDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsListDTO;

public interface FicheroInstruccionesService {

	InstructionsFileDTO findInstructionsFileBysolicitudeType(String formularioID);

	void uploadInstructionsToPay(InstructionsFileDTO ficheroInstrucciones);

	InstructionsListDTO findInstructionsLists(String userId);

	InstructionsFileDTO findInstructionsFileByForm(String formId);

	void uploadInstructions(InstructionsFileDTO ficheroInstrucciones);
}
