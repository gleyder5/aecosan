package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.report.ResolucionProvisionalReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.*;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("resolucionProvisionalRest")
public class ResolucionProvisionalRestService {

	private static final Logger logger = LoggerFactory.getLogger(ResolucionProvisionalRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/resolucionProvisional/{user}/{solicitudId}")
	public Response getPdfReport(@PathParam("user") String user,@PathParam("solicitudId") String solicitudId) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ", user );
			ResolucionProvisionalReportDTO reportToSign;
			reportToSign = bfacade.getResolucionProvisionalReporte(user, solicitudId);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}



}
