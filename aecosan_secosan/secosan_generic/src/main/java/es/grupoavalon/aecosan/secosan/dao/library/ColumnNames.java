package es.grupoavalon.aecosan.secosan.dao.library;

public class ColumnNames {

	public static final String CATALOG_ID = "ID";
	public static final String CATALOG_NOMBRE = "DESCRIPCION";
	public static final String USER_TYPE="TIPO_ID";
	public static final String LOGIN_COLUMN ="LOGIN";
	public static final String PASSWRD_COLUMN ="PASSWORD";
	public static final String SOLICITUDE_TYPE="TIPO_ID";
	public static final String LAST_LOGIN="ULTIMO_ACCESO";
	public static final String BLOCKED_TIME="FECHA_HORA_BLOQUEO";
	public static final String FAILED_ATTEMPT="INTENTOS_FALLIDOS";
	public static final String INE_CODE="CODIGO_INE";
	public static final String OFFER_TYPE = "TIPO_OFERTA";
	public static final String USER_BLOCKED = "BLOQUEO";
	public static final String USER_DELETED = "BORRADO";
	public static final String IDENTIFICATION_NUMBER = "IDENTIFICATION_NUMBER";
	public static final String LAST_NAME = "APELLIDOS";
	public static final String SOLICITUDE_FORM_TYPE = "FORMULARIO_TIPO_ID";
	public static final String ACTIVE = "ACTIVO";


	private ColumnNames() {
		super();
	}

}
