package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.SolicitudListDTO;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface SolicitudListService {

	List<SolicitudListDTO> getSolicList(PaginationParams paginationParams);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);

}
