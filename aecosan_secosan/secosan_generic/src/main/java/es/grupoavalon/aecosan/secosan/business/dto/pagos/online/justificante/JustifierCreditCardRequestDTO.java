package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JustifierCreditCardRequestDTO {
	@JsonProperty("expirationDate")
	@XmlElement(name = "expirationDate")
	private Long expirationDate;
	@JsonProperty("cardNumber")
	@XmlElement(name = "cardNumber")
	private String cardNumber;
	@JsonProperty("cardIssuerCode")
	@XmlElement(name = "cardIssuerCode")
	private String cardIssuerCode;

	public Long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public String getCardNumberDecoded() {
		String creditCardDecoded = "";
		if (StringUtils.isNotBlank(this.cardNumber)) {
			creditCardDecoded = new String(Base64.decodeBase64(this.cardNumber));
		}
		return creditCardDecoded;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardIssuerCode() {
		return cardIssuerCode;
	}

	public void setCardIssuerCode(String cardIssuerCode) {
		this.cardIssuerCode = cardIssuerCode;
	}

	@Override
	public String toString() {
		return "JustifierCreditCardRequestDTO [expirationDate=" + expirationDate + ", cardNumber=" + cardNumber + ", cardIssuerCode=" + cardIssuerCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cardIssuerCode == null) ? 0 : cardIssuerCode.hashCode());
		result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
		result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		JustifierCreditCardRequestDTO other = (JustifierCreditCardRequestDTO) obj;
		if (cardIssuerCode == null) {
			if (other.cardIssuerCode != null) {
				return false;
			}
		} else if (!cardIssuerCode.equals(other.cardIssuerCode)) {
			return false;
		}
		if (cardNumber == null) {
			if (other.cardNumber != null) {
				return false;
			}
		} else if (!cardNumber.equals(other.cardNumber)) {
			return false;
		}
		if (expirationDate == null) {
			if (other.expirationDate != null) {
				return false;
			}
		} else if (!expirationDate.equals(other.expirationDate)) {
			return false;
		}
		return true;
	}

}
