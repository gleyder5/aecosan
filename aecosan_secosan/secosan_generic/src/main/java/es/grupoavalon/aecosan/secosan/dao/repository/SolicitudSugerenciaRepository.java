package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;

public interface SolicitudSugerenciaRepository extends CrudRepository<SolicitudSugerenciaEntity, Long> {

}
