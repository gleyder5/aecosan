package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TasaModeloRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;

@Component("TasaModelo")
public class TasaModeloRepositoryImpl extends CrudCatalogRepositoryImpl<TasaModeloEntity, Long> implements TasaModeloRepository {

	private static final String DESCRIPCION_TASA_FILTER_SORT = "desc";
	private static final String WHEREPART_DESCRIPCION_FILTER_SORT = "LOWER(e.catalogValue) like LOWER(:" + DESCRIPCION_TASA_FILTER_SORT + ")";
	private static final String AMOUNT_TASA_FILTER_SORT = "amount";
	private static final String WHEREPART_TASA_FILTER = "e.ammount = :" + AMOUNT_TASA_FILTER_SORT;

	@Override
	public List<TasaModeloEntity> getCatalogList() {

		return super.findAllOrderByValue();
	}

	@Override
	protected Class<TasaModeloEntity> getClassType() {

		return TasaModeloEntity.class;
	}

	@Override
	public List<TasaModeloEntity> getFilteredTasaList(PaginationParams paginationParams) {
		List<FilterParams> filters = paginationParams.getFilters();

		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryWherePart = getQueryParts(filters, queryParams);
		logger.debug("query" + queryWherePart);
		String sortField = translateSortField(paginationParams.getSortField());

		List<TasaModeloEntity> resultList;
		if (paginationParams.getOrder().equals(QueryOrder.ASC)) {
			resultList = findAllPaginatedAndSortedASC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		} else {
			resultList = findAllPaginatedAndSortedDESC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		}

		return resultList;
	}

	@Override
	public int getFilteredTasaListRecords(List<FilterParams> filters) {

		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryName = getQueryParts(filters, queryParams);
		logger.debug("query:" + queryName);
		return getTotalRecords(queryName, queryParams);
	}

	private String getQueryParts(List<FilterParams> filters, Map<String, Object> queryParams) {
		QueryWhereParts queryParts = new QueryWhereParts();

		for (FilterParams f : filters) {
			if (StringUtils.equals(f.getFilterName(), DESCRIPCION_TASA_FILTER_SORT) && (StringUtils.isNotEmpty(f.getFilterValue()))) {
				queryParts.setPart(WHEREPART_DESCRIPCION_FILTER_SORT);
				queryParams.put(DESCRIPCION_TASA_FILTER_SORT, f.getFilterValue() + "%");
			} else if (StringUtils.equals(f.getFilterName(), AMOUNT_TASA_FILTER_SORT) && (StringUtils.isNotEmpty(f.getFilterValue())) && f.getFilterValue().matches("^(\\d+)(.{0,1}(\\d)+)*$")) {
				queryParts.setPart(WHEREPART_TASA_FILTER);
				queryParams.put(AMOUNT_TASA_FILTER_SORT, Float.parseFloat(f.getFilterValue()));
			}
		}
		return assambleQuery("", queryParts);
	}

	private String translateSortField(String sortField) {
		String translatedSortField = sortField;
		if (StringUtils.equals(DESCRIPCION_TASA_FILTER_SORT, sortField)) {
			translatedSortField = "catalogValue";
		} else if (StringUtils.equals(AMOUNT_TASA_FILTER_SORT, sortField)) {
			translatedSortField = "ammount";
		}
		return translatedSortField;
	}

}
