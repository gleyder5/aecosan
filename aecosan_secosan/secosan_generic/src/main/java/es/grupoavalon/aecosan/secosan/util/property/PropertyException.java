package es.grupoavalon.aecosan.secosan.util.property;

import es.grupoavalon.aecosan.secosan.util.exception.SecosanException;

public class PropertyException extends SecosanException {

	/**
	 * MISSING_RESOUORCE="THe given resource was not found:";
	 */
	public static final String MISSING_RESOUORCE = "THe given resource was not found:";
	/**
	 * NULL_KEY="The given key was null or blank";
	 */
	public static final String NULL_KEY = "The given key was null or blank";

	/**
	 * FORMAT_ERROR=
	 * "Can not generate expected value because the value in the property files is incorrect"
	 * ;
	 */
	public static final String FORMAT_ERROR = "Can not generate expected value because the value in the property files is incorrect, key:";

	public static final String RESOURCE_LOAD_ERROR = "The resource can not be load due an error, resource:";

	public PropertyException(String message) {
		super(message);

	}

	public PropertyException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public PropertyException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

}
