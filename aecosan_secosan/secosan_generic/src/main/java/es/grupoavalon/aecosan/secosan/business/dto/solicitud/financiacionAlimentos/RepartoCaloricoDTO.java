package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class RepartoCaloricoDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("protein")
	@XmlElement(name = "protein")
	@BeanToBeanMapping(getValueFrom = "protein")
	private String protein;

	@JsonProperty("lipids")
	@XmlElement(name = "lipids")
	@BeanToBeanMapping(getValueFrom = "lipids")
	private String lipids;

	@JsonProperty("carbohydrates")
	@XmlElement(name = "carbohydrates")
	@BeanToBeanMapping(getValueFrom = "carbohydrates")
	private String carbohydrates;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProtein() {
		return protein;
	}

	public void setProtein(String protein) {
		this.protein = protein;
	}

	public String getLipids() {
		return lipids;
	}

	public void setLipids(String lipids) {
		this.lipids = lipids;
	}

	public String getCarbohydrates() {
		return carbohydrates;
	}

	public void setCarbohydrates(String carbohydrates) {
		this.carbohydrates = carbohydrates;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carbohydrates == null) ? 0 : carbohydrates.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lipids == null) ? 0 : lipids.hashCode());
		result = prime * result + ((protein == null) ? 0 : protein.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepartoCaloricoDTO other = (RepartoCaloricoDTO) obj;
		if (carbohydrates == null) {
			if (other.carbohydrates != null)
				return false;
		} else if (!carbohydrates.equals(other.carbohydrates))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lipids == null) {
			if (other.lipids != null)
				return false;
		} else if (!lipids.equals(other.lipids))
			return false;
		if (protein == null) {
			if (other.protein != null)
				return false;
		} else if (!protein.equals(other.protein))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RepartoCaloricoDTO [id=" + id + ", protein=" + protein + ", lipids=" + lipids + ", carbohydrates=" + carbohydrates + "]";
	}

}
