package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoDocumentacionFormularioRepository;

@Component
public class TipoDocumentacionFormularioRepositoryImpl extends AbstractCrudRespositoryImpl<TipoDocumentacionFormularioEntity, Long> implements TipoDocumentacionFormularioRepository {

	@Override
	protected Class<TipoDocumentacionFormularioEntity> getClassType() {
		return TipoDocumentacionFormularioEntity.class;
	}

}
