/**
 * 
 */
package es.grupoavalon.aecosan.secosan.util.property;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

/**
 * @author ottoabreu
 *
 */
@Component
public class MessagesPropertiesProvider {

	
	/**
	 * Returns the message based on the key and the default locale
	 * @param key
	 * @param locale
	 * @return String
	 */
	
	public String getMessage(String key){
		return Properties.getString(PropertiesFilesNames.MESSAGES, key);
	}
	
	public String getErrorMessage(String key){
		return Properties.getString(PropertiesFilesNames.ERRORMESSAGES, key);
	}
	
	/**
	 * Load all the properties that should be outside the application
	 * @throws Exception
	 */
	@PostConstruct
	public void preloadAllExternalProperties() {
		Properties.preloadExternalProperties();
	}

}
