package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.JWTTokenKeyEntity;

public interface JWTTokenKeyDAO {

	void saveTokenKey(JWTTokenKeyEntity tokenKey);

	void deleteTokenKey(String tokenKey);

	JWTTokenKeyEntity getCurrentTokenKey();

	JWTTokenKeyEntity getCurrentTokenKeyAdmin();
	

}