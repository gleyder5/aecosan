package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;

public class SituacionDTO extends AbstractCatalogDto implements IsNulable<SituacionDTO> {

	@Override
	public SituacionDTO shouldBeNull() {
		if("".equals(id)){
			return null;
		}
		else{
			return this;
		}
	}

}
