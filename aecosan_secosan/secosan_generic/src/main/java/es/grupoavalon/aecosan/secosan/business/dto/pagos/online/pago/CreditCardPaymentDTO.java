package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CreditCardPaymentDTO {

	@JsonProperty("creditCard")
	@XmlElement(name = "creditCard")
	private String creditCard;

	@JsonProperty("bankCode")
	@XmlElement(name = "bankCode")
	private String bankCode;

	@JsonProperty("expirationDate")
	@XmlElement(name = "expirationDate")
	private Long expirationDate;

	public String getCreditCard() {
		return creditCard;
	}

	public String getCreditCardDecoded() {
		String creditCardDecoded = "";
		if (StringUtils.isNotBlank(this.creditCard)) {
			creditCardDecoded = new String(Base64.decodeBase64(this.creditCard));
		}
		return creditCardDecoded;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public Long getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Long expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "CreditCardPaymentDTO [creditCard=" + creditCard + ", bankCode=" + bankCode + ", expirationDate=" + expirationDate + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bankCode == null) ? 0 : bankCode.hashCode());
		result = prime * result + ((creditCard == null) ? 0 : creditCard.hashCode());
		result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CreditCardPaymentDTO other = (CreditCardPaymentDTO) obj;
		if (bankCode == null) {
			if (other.bankCode != null) {
				return false;
			}
		} else if (!bankCode.equals(other.bankCode)) {
			return false;
		}
		if (creditCard == null) {
			if (other.creditCard != null) {
				return false;
			}
		} else if (!creditCard.equals(other.creditCard)) {
			return false;
		}
		if (expirationDate == null) {
			if (other.expirationDate != null) {
				return false;
			}
		} else if (!expirationDate.equals(other.expirationDate)) {
			return false;
		}
		return true;
	}

}
