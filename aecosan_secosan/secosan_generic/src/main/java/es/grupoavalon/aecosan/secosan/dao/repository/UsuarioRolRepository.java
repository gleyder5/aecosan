package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;

public interface UsuarioRolRepository extends CrudCatalogRepository<UsuarioRolEntity, Long>, GenericCatalogRepository<UsuarioRolEntity> {

	public UsuarioRolEntity findUsuarioRol(long pk);
	public UsuarioRolEntity findOne(long pk);

	List<UsuarioRolEntity> getInternalRoles();

}
