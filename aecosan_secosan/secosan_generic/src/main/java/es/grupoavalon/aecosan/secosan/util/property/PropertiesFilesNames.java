package es.grupoavalon.aecosan.secosan.util.property;

public enum PropertiesFilesNames {
	
	SECOSAN("secosan"), ERRORMESSAGES("errormessages"), MESSAGES("messages");
	private String propertyName;

	private PropertiesFilesNames(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyName() {
		return propertyName;
	}
	
	public static PropertiesFilesNames[] getAllProps(){
		return new PropertiesFilesNames[]{SECOSAN,ERRORMESSAGES,MESSAGES};
	}

}
