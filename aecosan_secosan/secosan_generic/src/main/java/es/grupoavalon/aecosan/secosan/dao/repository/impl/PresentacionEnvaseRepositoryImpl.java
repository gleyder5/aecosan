package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.PresentacionEnvaseEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.PresentacionEnvaseRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.ProductoSuspendidoRepository;
import org.springframework.stereotype.Component;

import javax.ws.rs.QueryParam;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PresentacionEnvaseRepositoryImpl extends AbstractCrudRespositoryImpl<PresentacionEnvaseEntity, Long> implements PresentacionEnvaseRepository {

	@Override
	protected Class<PresentacionEnvaseEntity> getClassType() {
		// TODO Auto-generated method stub
		return PresentacionEnvaseEntity.class;
	}

	@Override
	public void deleteByFinanciacionId(Long id) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("financiacionId", id);
		List<PresentacionEnvaseEntity> presentacionEnvaseEntityList = this.findByNamedQuery(NamedQueriesLibrary.GET_PRESENTACION_ENVASE_BY_FINANCIACION_ALIMENTOS_ID,queryParams);
		if(presentacionEnvaseEntityList.size()>0){
			for (PresentacionEnvaseEntity presentacionEnvaseEntity: presentacionEnvaseEntityList){
				this.delete(presentacionEnvaseEntity.getId());
			}
		}
	}
}
