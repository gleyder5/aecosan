package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;

public interface ComplementosAlimenticiosDAO {

	CeseComercializacionCAEntity addCeseComercializacion(CeseComercializacionCAEntity ceseComercializacion);

	void updateCeseComercializacion(CeseComercializacionCAEntity ceseComercializacionEntity);

	PuestaMercadoCAEntity addPuestaMercado(PuestaMercadoCAEntity puestaMercado);

	void updatePuestaMercado(PuestaMercadoCAEntity puestaMercadoEntity);

	ModificacionDatosCAEntity addModificacionDatos(ModificacionDatosCAEntity modificacionDatosEntity);

	void updateModificacionDatos(ModificacionDatosCAEntity modificacionDatosEntity);

}
