package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.EvaluacionRiesgosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos.EvaluacionRiesgosDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.EvaluacionRiesgosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class EvaluacionRiesgosServiceImpl extends AbstractSolicitudService implements EvaluacionRiesgosService {

	@Autowired
	private EvaluacionRiesgosDAO evaluacionRiesgosDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO evaluacionRiesgosDTO) {
		try {
			solicitudPrepareNullDTO(evaluacionRiesgosDTO);
			prepareEvaluacionRiesgosNullability((EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
			addEvaluacionRiesgos(user, (EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + evaluacionRiesgosDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO evaluacionRiesgosDTO) {
		try {
			solicitudPrepareNullDTO(evaluacionRiesgosDTO);
			prepareEvaluacionRiesgosNullability((EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
			updateEvaluacionRiesgos(user, (EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + evaluacionRiesgosDTO);
		}
	}

	private void addEvaluacionRiesgos(String user, EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		EvaluacionRiesgosEntity evaluacionRiesgosEntity = generateEvaluacionRiesgosEntityFromDTO(evaluacionRiesgosDTO);
		setUserCreatorByUserId(user, evaluacionRiesgosEntity);
		prepareAddSolicitudEntity(evaluacionRiesgosEntity, evaluacionRiesgosDTO);
		EvaluacionRiesgosEntity evaluacionRiesgosEntityNew  = evaluacionRiesgosDAO.addEvaluacionRiesgos(evaluacionRiesgosEntity);
		if(evaluacionRiesgosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(evaluacionRiesgosEntityNew);
		}
	}

	private void updateEvaluacionRiesgos(String user, EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		EvaluacionRiesgosEntity evaluacionRiesgosEntity = transformDTOToEntity(evaluacionRiesgosDTO, EvaluacionRiesgosEntity.class);
		setUserCreatorByUserId(user, evaluacionRiesgosEntity);
		prepareUpdateSolicitudEntity(evaluacionRiesgosEntity, evaluacionRiesgosDTO);
		evaluacionRiesgosDAO.updateEvaluacionRiesgos(evaluacionRiesgosEntity);
		if(evaluacionRiesgosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(evaluacionRiesgosEntity);
		}
	}

	private EvaluacionRiesgosEntity generateEvaluacionRiesgosEntityFromDTO(EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		prepareSolicitudDTO(evaluacionRiesgosDTO);
		return transformDTOToEntity(evaluacionRiesgosDTO, EvaluacionRiesgosEntity.class);
	}

	private void prepareEvaluacionRiesgosNullability(EvaluacionRiesgosDTO evaluacionRiesgosDTO) {
		evaluacionRiesgosDTO.setSolicitudePayment(SecosanUtil.checkDtoNullability(evaluacionRiesgosDTO.getSolicitudePayment()));
		if (null != evaluacionRiesgosDTO.getSolicitudePayment()) {
			evaluacionRiesgosDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(evaluacionRiesgosDTO.getSolicitudePayment().getPayment()));
			evaluacionRiesgosDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(evaluacionRiesgosDTO.getSolicitudePayment().getIdPayment()));
			evaluacionRiesgosDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(evaluacionRiesgosDTO.getSolicitudePayment().getDataPayment()));
			if (null != evaluacionRiesgosDTO.getSolicitudePayment().getIdPayment()) {
				evaluacionRiesgosDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(evaluacionRiesgosDTO.getSolicitudePayment().getIdPayment().getLocation()));
				prepareLocationNull(evaluacionRiesgosDTO.getSolicitudePayment().getIdPayment().getLocation());
			}
		}
	}
}
