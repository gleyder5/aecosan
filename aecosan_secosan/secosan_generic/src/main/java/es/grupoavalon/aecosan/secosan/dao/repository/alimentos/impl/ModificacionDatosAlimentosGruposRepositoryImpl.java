package es.grupoavalon.aecosan.secosan.dao.repository.alimentos.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.ModificacionDatosAlimentosGrupoRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.impl.AbstractCrudRespositoryImpl;

@Component
public class ModificacionDatosAlimentosGruposRepositoryImpl extends AbstractCrudRespositoryImpl<ModificacionDatosAGEntity, Long> implements ModificacionDatosAlimentosGrupoRepository {

	@Override
	protected Class<ModificacionDatosAGEntity> getClassType() {
		return ModificacionDatosAGEntity.class;
	}
}
