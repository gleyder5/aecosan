package es.grupoavalon.aecosan.secosan.util.property;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

public final class Properties {

	protected static Logger logger = LoggerFactory.getLogger(Properties.class);

	private Properties() {
		super();
	}

	protected static final Map<PropertiesFilesNames, ResourceBundle> LOADED_PROPERTIES = new HashMap<PropertiesFilesNames, ResourceBundle>();

	public static String getString(PropertiesFilesNames propertyName, String key) {
		String value = null;
		if (StringUtils.isNotBlank(key)) {
			try {
				preloadInMap(propertyName);
				value = LOADED_PROPERTIES.get(propertyName).getString(key);

			} catch (MissingResourceException mre) {
				throw new PropertyException(PropertyException.MISSING_RESOUORCE
						+ propertyName+" "+key, mre);

			}
		} else {
			throw new PropertyException(PropertyException.NULL_KEY);
		}

		return value;
	}

	public static Integer getInt(PropertiesFilesNames prop, String key) {
		Integer value = null;
		try {
			value = Integer.parseInt(getString(prop, key));
		} catch (NumberFormatException e) {
			throw new PropertyException(PropertyException.FORMAT_ERROR+key,e);
		}
		
		return value;
	}
	
	public static Long getLong(PropertiesFilesNames prop, String key) {
		Long value = null;
		try {
			value = Long.parseLong(getString(prop, key));
		} catch (NumberFormatException e) {
			throw new PropertyException(PropertyException.FORMAT_ERROR + key, e);
		}

		return value;
	}

	public static boolean getBool(PropertiesFilesNames prop, String key)
 {
		Boolean value =null;
		try {
			value = Boolean.valueOf(getString(prop, key));
		} catch (Exception e) {
			throw new PropertyException(PropertyException.FORMAT_ERROR+key,e);
		}
		
		return value;
	}

	public static ResourceBundle getMyBundle(String propertiesFile) {

		try {
			File file = new File(SecosanConstants.PATH_REPO + "config");

			ClassLoader loader = null;
			URL[] urls = { file.toURI().toURL() };
			loader = new URLClassLoader(urls);
			logger.debug("Finding property file:" + propertiesFile
					+ " in PATH_REPO " + SecosanConstants.PATH_REPO + "config ");
			ResourceBundle bundle = ResourceBundle.getBundle(propertiesFile,
					java.util.Locale.getDefault(), loader);
			return bundle;
		} catch (Exception e) {
			logger.warn("Property file " + propertiesFile
					+ " not found in PATH_REPO " + SecosanConstants.PATH_REPO
					+ "config ");
			logger.warn("looking in inner classpath " + propertiesFile);
			try {
				// En caso de no encontrar el fichero lo leemos del classpath
				return ResourceBundle.getBundle(propertiesFile);
			} catch (MissingResourceException mre) {
				logger.error("Properties file" + propertiesFile
						+ " not found in CLASSPATH");
				throw new PropertyException(PropertyException.MISSING_RESOUORCE, mre);
			}
		}
	}

	public static java.util.Properties getProperty(String propertyFileName, ClassLoader... loaderArg) {
		ClassLoader loader = null;
		if (loaderArg != null && loaderArg.length > 0) {
			loader = loaderArg[0];
		} else {
			loader = Properties.class.getClassLoader().getParent().getParent();
		}
		return loadPropertyFile(propertyFileName, loader);
	}

	private static java.util.Properties loadPropertyFile(String propertyFileName, ClassLoader loader) {
		java.util.Properties props = new java.util.Properties();
		InputStream input = null;
		try {
			input = getPropertiesInputStream(propertyFileName, loader);
			props.load(input);
		} catch (IOException e) {
			throw new PropertyException(PropertyException.RESOURCE_LOAD_ERROR + propertyFileName, e);
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					logger.warn("Can't close the property resource:" + propertyFileName, e);
				}
			}
		}

		return props;
	}

	private static InputStream getPropertiesInputStream(String propertyFileName, ClassLoader loader) {
		InputStream input = null;
		try {
			input = new FileInputStream(SecosanConstants.PATH_REPO + "config/" + propertyFileName + ".properties");
		} catch (FileNotFoundException e) {
			input = loader.getResourceAsStream(propertyFileName);
			if (input == null) {
				throw new PropertyException(PropertyException.MISSING_RESOUORCE + propertyFileName);
			}
		}
		return input;
	}

	protected static void preloadExternalProperties() {
		for (PropertiesFilesNames prop : PropertiesFilesNames.getAllProps()) {
			preloadInMap(prop);
			logger.info("Property: " + prop + " added to map");
		}
	}

	private static void preloadInMap(PropertiesFilesNames prop) {
		if (!LOADED_PROPERTIES.containsKey(prop)) {
			LOADED_PROPERTIES.put(prop,
					Properties.getMyBundle(prop.getPropertyName()));
		}

	}
}
