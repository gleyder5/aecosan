package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.FormaPagoEntity;

public interface FormaPagoRepository extends CrudRepository<FormaPagoEntity, Long>{

}
