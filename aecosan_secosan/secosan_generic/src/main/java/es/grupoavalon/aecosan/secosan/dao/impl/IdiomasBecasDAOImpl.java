package es.grupoavalon.aecosan.secosan.dao.impl;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.dao.BecasDAO;
import es.grupoavalon.aecosan.secosan.dao.IdiomasBecasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.BecasRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.IdiomasBecasRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class IdiomasBecasDAOImpl extends GenericDao implements IdiomasBecasDAO {
	@Autowired
	private IdiomasBecasRepository idiomasbecasRepository;

	@Override
	public IdiomasBecasEntity addIdiomasBecasEntity(IdiomasBecasEntity idiomas) {
		IdiomasBecasEntity idiomasBecasEntity= null;
		try {
			idiomasBecasEntity = idiomasbecasRepository.save(idiomas);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addIdiomasBecas | idiomasBecas: " + idiomas);
		}
		return idiomasBecasEntity;
	}

	@Override
	public IdiomasBecasEntity updateIdiomasBecasEntity(IdiomasBecasEntity idiomasBecasEntity, IdiomasBecasDTO idiomasBecasDTO) {
		try {
			idiomasBecasEntity.setNombre(idiomasBecasDTO.getNombre());
			idiomasBecasEntity.setPuntos(idiomasBecasDTO.getPuntos());
			idiomasBecasEntity = idiomasbecasRepository.save(idiomasBecasEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addIdiomasBecas | idiomasBecas: " + idiomasBecasDTO);
		}
		return idiomasBecasEntity;
	}

	@Override
	public void deleteIdiomasBecasByBeca(BecasEntity beca) {
		try {
			List<IdiomasBecasEntity> idiomasBecasEntityList = idiomasbecasRepository.findByBeca(beca);
			for(IdiomasBecasEntity  idioma : idiomasBecasEntityList){
				idiomasbecasRepository.delete(idioma.getId());
			}
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteIdiomasBecas | idiomasBecas: " + beca);
		}

	}

	@Override
	public List<IdiomasBecasEntity> findIdiomasBecasEntityByBeca(BecasEntity beca) {
		try {
			List<IdiomasBecasEntity> idiomasBecasEntityList = idiomasbecasRepository.findByBeca(beca);
			return idiomasBecasEntityList;
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteIdiomasBecas | idiomasBecas: " + beca);
		}
		return  null;

	}

	@Override
	public IdiomasBecasEntity findIdiomasBecasEntityByBecaAndIdioma(BecasEntity beca, String idioma) {

		try {
			IdiomasBecasEntity idiomasBecasEntity = idiomasbecasRepository.findByBecaAndIdioma(beca,idioma);
			return idiomasBecasEntity;
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteIdiomasBecas | idiomasBecas: " + beca);
		}
		return  null;
	}

}
