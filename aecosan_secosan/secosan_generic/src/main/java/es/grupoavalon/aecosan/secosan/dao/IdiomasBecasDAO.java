package es.grupoavalon.aecosan.secosan.dao;


import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.IdiomasBecasEntity;

import java.util.List;

public interface IdiomasBecasDAO {


	IdiomasBecasEntity addIdiomasBecasEntity(IdiomasBecasEntity becasEntity);

	IdiomasBecasEntity updateIdiomasBecasEntity(IdiomasBecasEntity becasEntity, IdiomasBecasDTO idiomasBecasDTO);

	void deleteIdiomasBecasByBeca(BecasEntity beca);

	List<IdiomasBecasEntity> findIdiomasBecasEntityByBeca(BecasEntity beca);

	IdiomasBecasEntity findIdiomasBecasEntityByBecaAndIdioma(BecasEntity beca,String idioma);


}
