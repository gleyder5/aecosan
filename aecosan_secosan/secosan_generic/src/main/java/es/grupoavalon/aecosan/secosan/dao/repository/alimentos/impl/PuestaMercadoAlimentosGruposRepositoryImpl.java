package es.grupoavalon.aecosan.secosan.dao.repository.alimentos.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.PuestaMercadoAlimentosGruposRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.impl.AbstractCrudRespositoryImpl;

@Component
public class PuestaMercadoAlimentosGruposRepositoryImpl extends AbstractCrudRespositoryImpl<PuestaMercadoAGEntity, Long> implements PuestaMercadoAlimentosGruposRepository {

	@Override
	protected Class<PuestaMercadoAGEntity> getClassType() {
		return PuestaMercadoAGEntity.class;
	}
}
