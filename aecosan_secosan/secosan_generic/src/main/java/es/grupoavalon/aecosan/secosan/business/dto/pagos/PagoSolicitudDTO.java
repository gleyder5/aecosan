package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PagoSolicitudDTO implements IsNulable<PagoSolicitudDTO> {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("rateAmount")
	@XmlElement(name = "rateAmount")
	@BeanToBeanMapping(getValueFrom = "rateAmount")
	private Float rateAmount;

	@JsonProperty("amount")
	@XmlElement(name = "amount")
	@BeanToBeanMapping(getValueFrom = "amount")
	private Float amount;
	
	@JsonProperty("payedTasas")
	@XmlElement(name = "payedTasas")
	@BeanToBeanMapping(getValueFrom = "payedTasas")
	private Integer payedTasas;

	@JsonProperty("payment")
	@XmlElement(name = "payment")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "payment")
	private FormaPagoDTO payment;

	@JsonProperty("idPayment")
	@XmlElement(name = "idPayment")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "idPayment")
	private IdentificadorPagoDTO idPayment;

	@JsonProperty("dataPayment")
	@XmlElement(name = "dataPayment")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "dataPayment")
	private DatosPagoDTO dataPayment;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Float getRateAmount() {
		return rateAmount;
	}

	public void setRateAmount(Float rateAmount) {
		this.rateAmount = rateAmount;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public FormaPagoDTO getPayment() {
		return payment;
	}

	public void setPayment(FormaPagoDTO payment) {
		this.payment = payment;
	}

	public IdentificadorPagoDTO getIdPayment() {
		return idPayment;
	}

	public void setPayedTasas(Integer payedTasas){
		this.payedTasas = payedTasas;
	}
	
	public Integer getPayedTasas(){
		return payedTasas;
	}
	
	public void setIdPayment(IdentificadorPagoDTO idPayment) {
		this.idPayment = idPayment;
	}

	public DatosPagoDTO getDataPayment() {
		return dataPayment;
	}

	public void setDataPayment(DatosPagoDTO dataPayment) {
		this.dataPayment = dataPayment;
	}

	@Override
	public String toString() {
		return "PagoSolicitudDTO [id=" + id + ", rateAmount=" + rateAmount + ", amount=" + amount + ", payment=" + payment + ", idPayment=" + idPayment + ", dataPayment=" + dataPayment + "]";
	}

	@Override
	public PagoSolicitudDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}

	private boolean checkNullability() {
		boolean nullabillity = true;

		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && getRateAmount() == null;
		nullabillity = nullabillity && getAmount() == null;
		nullabillity = nullabillity && payment.shouldBeNull() == null;
		nullabillity = nullabillity && idPayment.shouldBeNull() == null;
		return nullabillity;
	}

}
