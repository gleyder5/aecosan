package es.grupoavalon.aecosan.secosan.business.exception;

import es.grupoavalon.aecosan.secosan.util.exception.CodedError;
import es.grupoavalon.aecosan.secosan.util.exception.HttpCodedError;

public class AutenticateException extends Exception implements CodedError, HttpCodedError {
	
	private static final int HTTP_RELATED_CODE = 401;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum AutenticateErrorCodes{
		ERROR_INVALID_CREDENTIALS(-1, "wrong credentials", HTTP_RELATED_CODE), ERROR_LOCKED_USER(-2, "user is locked", HTTP_RELATED_CODE), ERROR_PASSWORD_MUST_CHANGE(-3, "the password must change",
				HTTP_RELATED_CODE), ERROR_PASSWORD_EXPIRED(-4, "password is expipred", HTTP_RELATED_CODE), TOKEN_INVALID(-5, "invalid token", HTTP_RELATED_CODE), ERROR_NOT_A_REGISTRED_USER(-6,
				"User Not In the system", HTTP_RELATED_CODE), ERROR_INSUFICIENT_PRIVILEGES(-7, "User lacks of login privileges", HTTP_RELATED_CODE);
		private int code;
		private String description;
		private int httpCode;
		
		private AutenticateErrorCodes(int code, String description, int httpCode) {
			this.code = code;
			this.description = description;
			this.httpCode = httpCode;
		}

		public int getCode() {
			return code;
		}

		public String getDescription() {
			return description;
		}

		public int getHttpCode() {
			return httpCode;
		}

		protected void setHttpCode(int httpCode) {
			this.httpCode = httpCode;
		}

	}
	
	private final AutenticateErrorCodes errorCode;

	public AutenticateException(AutenticateErrorCodes errorCode) {
		super(errorCode.description);
		this.errorCode = errorCode;

	}

	public AutenticateException(String message, AutenticateErrorCodes code) {
		super(message);
		this.errorCode = code;
	}

	public AutenticateErrorCodes getErrorCode() {
		return errorCode;
	}

	@Override
	public int getCodeError() {
		return this.errorCode.code;
	}

	@Override
	public String getDescription() {
		return this.errorCode.description;
	}

	@Override
	public int getHttpCodeRelated() {
		return this.errorCode.httpCode;
	}

}
