package es.grupoavalon.aecosan.secosan.business.dto.report;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import es.grupoavalon.aecosan.secosan.util.report.Reportable;

public class SignedReportDTO implements Reportable {
	private String identificationNumber;

	private String formName;

	private String reportName;

	private String date;

	private String jobPosition;


	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public void setDate(long date) {
		Date reportDate = new Date(date);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.date = sdf.format(reportDate);
	}

	public String getJobPosition() {
		if (StringUtils.isEmpty(this.jobPosition)) {
			this.jobPosition = ". . . . . . . . . . . . . . . . . ";
		}
		return jobPosition;
	}

	public void setJobPosition(String jobPosition) {

		this.jobPosition = jobPosition;
	}
}
