package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component("becasRest")

public class BecasRestService extends AbstractSolicitudRestService {

	@Autowired
	private BusinessFacade businessFacade;


	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/becas/{user}")
	public Response addBecas(@PathParam("user") String user, BecasDTO becas) {
		logger.debug("-- Method addBecas POST Init--");
		Response response = addSolicitud(user, becas);
		logger.debug("-- Method addBecas POST End--");
		return response;
	}
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/becas/{user}")
	public Response updateBecas(@PathParam("user") String user, BecasDTO becas) {
		logger.debug("-- Method updateBecas PUT Init--");
		Response response = updateSolicitud(user, becas);
		logger.debug("-- Method updateBecas PUT End--");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/becas/{becaID}/idiomas/{user}")
	public Response getIdiomasBecasByBecaAdmin(@PathParam("user") String user, @PathParam("becaID") String becaId) {
		return getIdiomasBecasByBeca(user,becaId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/becas/{becaID}/idiomas/{user}")
	public Response getIdiomasBecasByBecaPublic(@PathParam("user") String user, @PathParam("becaID") String becaId) {
		return getIdiomasBecasByBeca(user,becaId);
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/becas/{becaID}/{user}")
	public Response getBecaById(@PathParam("user") String user, @PathParam("becaID") String becaId) {
		return findSolicitudById(user,becaId);
	}



	private Response getIdiomasBecasByBeca(String user,String becaId){
		logger.debug("-- Method updateBecas PUT Init--");
		Response response = null;
		try {
			List<IdiomasBecasDTO> list = businessFacade.getIdiomasBecasByBeca(Long.valueOf(becaId));

			response = responseFactory.generateOkGenericResponse(list);
		}
		catch (Exception  ex){

		}
		logger.debug("-- Method updateBecas PUT End--");
		return response;
	}

}
