package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class GrupoPacientesDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("breastfed")
	@XmlElement(name = "breastfed")
	@BeanToBeanMapping(getValueFrom = "breastfed")
	private Boolean breastfed;

	@JsonProperty("child")
	@XmlElement(name = "child")
	@BeanToBeanMapping(getValueFrom = "child")
	private Boolean child;

	@JsonProperty("adult")
	@XmlElement(name = "adult")
	@BeanToBeanMapping(getValueFrom = "adult")
	private Boolean adult;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getBreastfed() {
		return breastfed;
	}

	public void setBreastfed(Boolean breastfed) {
		this.breastfed = breastfed;
	}

	public Boolean getChild() {
		return child;
	}

	public void setChild(Boolean child) {
		this.child = child;
	}

	public Boolean getAdult() {
		return adult;
	}

	public void setAdult(Boolean adult) {
		this.adult = adult;
	}

	@Override
	public String toString() {
		return "GrupoPacientesDTO [id=" + id + ", breastfed=" + breastfed + ", child=" + child + ", adult=" + adult + "]";
	}

}
