package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;

public interface SolicitudQuejaSugerenciaDAO {

	SolicitudQuejaEntity addSolicitudQueja(SolicitudQuejaEntity solicitudQuejaEntity);
	SolicitudQuejaEntity findSolicitudQuejaById(Long id);
	void updateSolicitudQueja(SolicitudQuejaEntity solicitudQuejaEntity);

	SolicitudSugerenciaEntity addSolicitudSugerencia(SolicitudSugerenciaEntity solicitudSugerenciaEntity);
	void updateSolicitudSugerencia(SolicitudSugerenciaEntity solicitudSugerenciaEntity);
	SolicitudSugerenciaEntity findSolicitudSugerenciaById(Long id);
}
