package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.HashMap;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.PuestaMercadoComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.AlimentosGruposService;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceAddDTO;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceUpdateDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos.CeseComercializacionAlimentosGruposDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos.ModificacionDatosAlimentosGruposDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.alimentosGrupos.PuestaMercadoAlimentosGruposDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.AlimentosGruposDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class AlimentosGruposServiceImpl extends AbstractAlimentosService implements AlimentosGruposService {

	private Map<String, ServicePersistenceAddDTO> solicitudeAddMap;
	private Map<String, ServicePersistenceUpdateDTO> solicitudeUpdateMap;

	@Autowired
	private AlimentosGruposDAO alimentosGruposDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO alimentosGrupos) {
		try {
			solicitudPrepareNullDTO(alimentosGrupos);
			solicitudeAddMapInit();
			solicitudeAddMap.get(alimentosGrupos.getClass().getSimpleName()).addSolicitudDTO(user, alimentosGrupos);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + alimentosGrupos);
		}
	}

	private void solicitudeAddMapInit() {
		if (solicitudeAddMap == null) {
			solicitudeAddMap = new HashMap<String, ServicePersistenceAddDTO>();
			solicitudeAddMap.put(EntityDtoNames.CESE_COMERCIALIZACION_AG_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addCeseComercializacionAlimentosGruposDTO(user, (CeseComercializacionAlimentosGruposDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.MODIFICACION_DATOS_AG_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addModificacionDatosComplementosAlimenticiosDTO(user, (ModificacionDatosAlimentosGruposDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.PUESTA_MERCADO_AG_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addPuestaMercadoComplementosAlimenticiosDTO(user, (PuestaMercadoAlimentosGruposDTO) solicitud);
				}
			});
		}
	}

	private void addCeseComercializacionAlimentosGruposDTO(String user, CeseComercializacionAlimentosGruposDTO ceseComercializacionDTO) {
		CeseComercializacionAGEntity ceseComercializacionEntity = generateCeseComercializacionAGEntityFromDTO(ceseComercializacionDTO);
		setUserCreatorByUserId(user, ceseComercializacionEntity);
		prepareAddSolicitudEntity(ceseComercializacionEntity, ceseComercializacionDTO);
		CeseComercializacionAGEntity ceseComercializacionAGEntitNew = alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
		if(ceseComercializacionDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(ceseComercializacionAGEntitNew);
		}
	}

	private CeseComercializacionAGEntity generateCeseComercializacionAGEntityFromDTO(CeseComercializacionAlimentosGruposDTO alimentosGrupos) {
		prepareSolicitudDTO(alimentosGrupos);
		return transformDTOToEntity(alimentosGrupos, CeseComercializacionAGEntity.class);
	}

	private void addModificacionDatosComplementosAlimenticiosDTO(String user, ModificacionDatosAlimentosGruposDTO alimentosGrupos) {
		ModificacionDatosAGEntity modificacionDatosEntity = generateModificacionDatosAGEntityFromDTO(alimentosGrupos);
		setUserCreatorByUserId(user, modificacionDatosEntity);
		prepareAddSolicitudEntity(modificacionDatosEntity, alimentosGrupos);
		ModificacionDatosAGEntity modificacionDatosAGEntityNew = alimentosGruposDAO.addModificacionDatos(modificacionDatosEntity);
		if(alimentosGrupos.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(modificacionDatosAGEntityNew );
		}
	}

	private ModificacionDatosAGEntity generateModificacionDatosAGEntityFromDTO(ModificacionDatosAlimentosGruposDTO alimentosGrupos) {
		prepareSolicitudDTO(alimentosGrupos);
		return transformDTOToEntity(alimentosGrupos, ModificacionDatosAGEntity.class);
	}

	private void addPuestaMercadoComplementosAlimenticiosDTO(String user, PuestaMercadoAlimentosGruposDTO alimentosGrupos) {
		puestaMercadoPrepareNullDTOalimentosGrupos(alimentosGrupos);
		PuestaMercadoAGEntity puestaMercadoEntity = generatePuestaMercadoAGEntityFromDTO(alimentosGrupos);
		setUserCreatorByUserId(user, puestaMercadoEntity);
		preparePuestaMercadoEntity(puestaMercadoEntity, alimentosGrupos);
		PuestaMercadoAGEntity  puestaMercadoCAEntityNew  =  alimentosGruposDAO.addPuestaMercado(puestaMercadoEntity);
		if(alimentosGrupos.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud( puestaMercadoCAEntityNew );
		}
	}

	private PuestaMercadoAGEntity generatePuestaMercadoAGEntityFromDTO(PuestaMercadoAlimentosGruposDTO alimentosGrupos) {
		prepareSolicitudDTO(alimentosGrupos);
		return transformDTOToEntity(alimentosGrupos, PuestaMercadoAGEntity.class);
	}

	@Override
	public void update(String user, SolicitudDTO alimentosGrupos) {
		try {
			solicitudPrepareNullDTO(alimentosGrupos);
			solicitudeUpdateMapInit();
			solicitudeUpdateMap.get(alimentosGrupos.getClass().getSimpleName()).updateSolicitudDTO(user, alimentosGrupos);

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + alimentosGrupos);
		}
	}

	private void puestaMercadoPrepareNullDTOalimentosGrupos(PuestaMercadoAlimentosGruposDTO alimentosGrupos) {
		alimentosGrupos.setProductType(SecosanUtil.checkDtoNullability(alimentosGrupos.getProductType()));
	}

	private void solicitudeUpdateMapInit() {
		if (solicitudeUpdateMap == null) {
			solicitudeUpdateMap = new HashMap<String, ServicePersistenceUpdateDTO>();
			solicitudeUpdateMap.put(EntityDtoNames.CESE_COMERCIALIZACION_AG_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateCeseComercializacionAlimentosGruposDTO(user, (CeseComercializacionAlimentosGruposDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.MODIFICACION_DATOS_AG_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateModificacionDatosAlimentosGruposDTO(user, (ModificacionDatosAlimentosGruposDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.PUESTA_MERCADO_AG_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updatePuestaMercadoAlimentosGruposDTO(user, (PuestaMercadoAlimentosGruposDTO) solicitud);
				}
			});
		}
	}

	private void updateCeseComercializacionAlimentosGruposDTO(String user, CeseComercializacionAlimentosGruposDTO alimentosGrupos) {
		CeseComercializacionAGEntity ceseComercializacionEntity = transformDTOToEntity(alimentosGrupos, CeseComercializacionAGEntity.class);
		setUserCreatorByUserId(user, ceseComercializacionEntity);
		prepareUpdateSolicitudEntity(ceseComercializacionEntity, alimentosGrupos);
		alimentosGruposDAO.updateCeseComercializacion(ceseComercializacionEntity);
		historialSolicitudDAO.addHistorialSolicitud(ceseComercializacionEntity);
	}

	private void updateModificacionDatosAlimentosGruposDTO(String user, ModificacionDatosAlimentosGruposDTO alimentosGrupos) {
		ModificacionDatosAGEntity modificacionDatosEntity = transformDTOToEntity(alimentosGrupos, ModificacionDatosAGEntity.class);
		setUserCreatorByUserId(user, modificacionDatosEntity);
		prepareUpdateSolicitudEntity(modificacionDatosEntity, alimentosGrupos);
		alimentosGruposDAO.updateModificacionDatos(modificacionDatosEntity);
	}

	private void updatePuestaMercadoAlimentosGruposDTO(String user, PuestaMercadoAlimentosGruposDTO alimentosGrupos) {
		puestaMercadoPrepareNullDTOalimentosGrupos(alimentosGrupos);
		PuestaMercadoAGEntity puestaMercadoEntity = transformDTOToEntity(alimentosGrupos, PuestaMercadoAGEntity.class);
		setUserCreatorByUserId(user, puestaMercadoEntity);
		preparePuestaMercadoEntityUpdate(puestaMercadoEntity, alimentosGrupos);
		alimentosGruposDAO.updatePuestaMercado(puestaMercadoEntity);
	}

}
