package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios;

import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.CeseComercializacionDTO;

public class CeseComercializacionComplementosAlimenticiosDTO extends CeseComercializacionDTO implements ComplementosAlimenticiosDTO{

}
