package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("solicitudeRest")
public class SolicitudeRestService extends AbstractSolicitudRestService {

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/solicitud/";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/solicitud/";

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{user}/{id}")
	public Response findSolicitudeById(@PathParam("user") String user, @PathParam("id") String id) {
		logger.debug("-- Method findSolicitudeById GET Init--");
		Response response = findSolicitudById(user, id);
		logger.debug("-- Method findSolicitudeById GET END--");
		return response;
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{user}/{id}")
	public Response deleteSolicitudeById(@PathParam("user") String user, @PathParam("id") String id) {
		logger.debug("-- Method deleteSolicitudeById DELETE Init--");
		Response response = deleteSolicitud(user, id);
		logger.debug("-- Method deleteSolicitudeById DELETE End--");
		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "{user}/{id}")
	public Response findSolicitudeByIdAdmin(@PathParam("user") String user, @PathParam("id") String id) {
		logger.debug("-- Method findSolicitudeByIdAdmin GET Init--");
		Response response = findSolicitudById(user, id);
		logger.debug("-- Method findSolicitudeByIdAdmin GET END--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "{user}")
	public Response solicitudeUpdateStatusBySolicitudeId(@PathParam("user") String user, ChangeStatusDTO changeStatusDTO) {
		logger.debug("-- Method solicitudeUpdateStatusBySolicitude POST Init --");
		Response response;
		try {
			logger.debug("-- Method solicitudeUpdateStatusBySolicitudeId | user: " + user + " | changeStatusDTO: " + changeStatusDTO);
			bfacade.solicitudeUpdateStatusBySolicitudeId(user, changeStatusDTO);

			response = responseFactory.generateOkResponse();

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method solicitudeUpdateStatusBySolicitude POST End --");
		return response;
	}

}
