package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;

public interface BajaOferAlimUMERepository extends CrudRepository<BajaOferAlimUMEEntity, Long> {

}
