package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_SERVICIOS)
@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_SERVICES_BY_PAY_TYPE_ID, query = "SELECT s FROM " + TableNames.ENTITY_PACKAGE_COMPLEMENTOS_EVALUACION_RIESGOS
		+ ".ServicioEntity AS s WHERE s.id in (SELECT tps.service.id FROM " + TableNames.ENTITY_PACKAGE_COMPLEMENTOS_EVALUACION_RIESGOS
		+ ".TipoPagoServicioEntity tps WHERE tps.payType.id = :payType)") })
public class ServicioEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(name = "TASA_MODELO_ID", referencedColumnName = "ID", nullable = true)
	private TasaModeloEntity tasaModelo;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TasaModeloEntity getTasaModelo() {
		return tasaModelo;
	}

	public void setTasaModelo(TasaModeloEntity tasaModelo) {
		this.tasaModelo = tasaModelo;
	}

	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((tasaModelo == null) ? 0 : tasaModelo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ServicioEntity other = (ServicioEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (tasaModelo == null) {
			if (other.tasaModelo != null) {
				return false;
			}
		} else if (!tasaModelo.equals(other.tasaModelo)) {
			return false;
		}
		return true;
	}


}
