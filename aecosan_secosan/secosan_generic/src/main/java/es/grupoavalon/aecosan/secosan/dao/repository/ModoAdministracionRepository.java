package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ModoAdministracionEntity;

public interface ModoAdministracionRepository extends CrudRepository<ModoAdministracionEntity, Long> {

}
