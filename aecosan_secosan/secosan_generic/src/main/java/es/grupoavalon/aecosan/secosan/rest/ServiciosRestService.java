package es.grupoavalon.aecosan.secosan.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.ServicioDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("serviciosRest")
public class ServiciosRestService {

	private static final Logger logger = LoggerFactory.getLogger(ServiciosRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/servicios";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/servicios";


	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "/{payTypeId}")
	public Response findServicesByPayTypeId(@PathParam("payTypeId") String payTypeId) {
		logger.debug("-- Method findServicesByPayTypeId GET Init --");

		return this.servicesByPayTypeId(payTypeId);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "/{payTypeId}")
	public Response findServicesByPayTypeIdAdmin(@PathParam("payTypeId") String payTypeId) {
		logger.debug("-- Method findServicesByPayTypeId GET Init --");

		return this.servicesByPayTypeId(payTypeId);
	}

	private Response servicesByPayTypeId(String payTypeId) {
		logger.debug("-- Method findServicesByPayTypeId GET Init --");
		Response response;
		List<ServicioDTO> servicios;
		try {
			servicios = bfacade.findServicesByPayTypeId(payTypeId);
			response = responseFactory.generateOkGenericResponse(servicios);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method findServicesByPayTypeId GET End --");
		return response;
	}
}
