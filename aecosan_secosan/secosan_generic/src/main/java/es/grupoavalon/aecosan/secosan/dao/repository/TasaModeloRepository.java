package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface TasaModeloRepository extends CrudCatalogRepository<TasaModeloEntity, Long>, GenericCatalogRepository<TasaModeloEntity>, CrudRepository<TasaModeloEntity, Long> {

	List<TasaModeloEntity> getFilteredTasaList(PaginationParams paginationParams);

	int getFilteredTasaListRecords(List<FilterParams> filters);

}
