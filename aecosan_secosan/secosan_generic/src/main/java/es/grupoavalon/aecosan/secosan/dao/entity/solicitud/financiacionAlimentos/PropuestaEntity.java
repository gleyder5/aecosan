package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PROPUESTA)
public class PropuestaEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.PROPUESTA)
	@SequenceGenerator(name = SequenceNames.PROPUESTA, sequenceName = SequenceNames.PROPUESTA, allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "CODIGO_TIPO")
	private String typeCode;

	@Column(name = "CODIGO_SUBTIPO")
	private String subtypeCode;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getSubtypeCode() {
		return subtypeCode;
	}

	public void setSubtypeCode(String subtypeCode) {
		this.subtypeCode = subtypeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((subtypeCode == null) ? 0 : subtypeCode.hashCode());
		result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropuestaEntity other = (PropuestaEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (subtypeCode == null) {
			if (other.subtypeCode != null)
				return false;
		} else if (!subtypeCode.equals(other.subtypeCode))
			return false;
		if (typeCode == null) {
			if (other.typeCode != null)
				return false;
		} else if (!typeCode.equals(other.typeCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PropuestaEntity [id=" + id + ", typeCode=" + typeCode + ", subtypeCode=" + subtypeCode + "]";
	}

}
