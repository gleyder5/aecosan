package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;

public interface TipoDocumentosDAO {
	TipoDocumentacionEntity findTipoDocumentacion(Long pk);

	void addTipoDocumentacion(TipoDocumentacionEntity tipoDocumentacionEntity);
}
