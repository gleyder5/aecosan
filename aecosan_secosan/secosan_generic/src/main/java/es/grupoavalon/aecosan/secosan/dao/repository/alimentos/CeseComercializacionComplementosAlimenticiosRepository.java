package es.grupoavalon.aecosan.secosan.dao.repository.alimentos;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

public interface CeseComercializacionComplementosAlimenticiosRepository extends CrudRepository<CeseComercializacionCAEntity, Long> {

}
