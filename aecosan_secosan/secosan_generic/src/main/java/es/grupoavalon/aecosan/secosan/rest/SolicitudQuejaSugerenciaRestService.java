package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudSugerenciaDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("solicitudQuejaSugerenciaRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/solicitudQuejaSugerencia")
public class SolicitudQuejaSugerenciaRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/queja/{user}")
	public Response addSolicitudQueja(@PathParam("user") String user, SolicitudQuejaDTO quejaDTO) {
		logger.debug("-- Method addQueja POST Init--");
		Response response = addSolicitud(user, quejaDTO);
		logger.debug("-- Method addQueja POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/queja/{user}")
	public Response updateSolicitudQueja(@PathParam("user") String user, SolicitudQuejaDTO solicitudQuejaDTO) {
		logger.debug("-- Method updateSolicitudQueja PUT Init--");
		Response response = updateSolicitud(user, solicitudQuejaDTO);
		logger.debug("-- Method updateSolicitudQueja PUT End--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/sugerencia/{user}")
	public Response addSolicitudSugerencia(@PathParam("user") String user, SolicitudSugerenciaDTO solicitudSugerenciaDTO) {
		logger.debug("-- Method addSugerencia POST Init--");
		Response response = addSolicitud(user, solicitudSugerenciaDTO);
		logger.debug("-- Method addSugerencia POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/sugerencia/{user}")
	public Response updateSolicitudSugerencia(@PathParam("user") String user, SolicitudSugerenciaDTO solicitudSugerenciaDTO) {
		logger.debug("-- Method updateSolicitudsugerencia PUT Init--");
		Response response = updateSolicitud(user, solicitudSugerenciaDTO);
		logger.debug("-- Method updateSolicitudsugerencia PUT End--");
		return response;
	}

}
