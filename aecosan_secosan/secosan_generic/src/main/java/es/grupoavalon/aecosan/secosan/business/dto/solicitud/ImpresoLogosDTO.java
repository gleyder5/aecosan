package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImpresoLogosDTO extends ImpresoFirmadoDTO {

	@JsonProperty("nombreSolicitante")
	@XmlElement(name = "nombreSolicitante")
	private String nombreSolicitante;

	@JsonProperty("direccionSolicitante")
	@XmlElement(name = "direccionSolicitante")
	private String direccionSolicitante;

	@JsonProperty("nifSolicitante")
	@XmlElement(name = "nifSolicitante")
	private String nifSolicitante;

	@JsonProperty("telefonoSolicitante")
	@XmlElement(name = "telefonoSolicitante")
	private String telefonoSolicitante;

	@JsonProperty("mailSolicitante")
	@XmlElement(name = "mailSolicitante")
	private String mailSolicitante;

	@JsonProperty("nombreRepresentante")
	@XmlElement(name = "nombreRepresentante")
	private String nombreRepresentante;

	@JsonProperty("direccionRepresentante")
	@XmlElement(name = "direccionRepresentante")
	private String direccionRepresentante;

	@JsonProperty("nifRepresentante")
	@XmlElement(name = "nifRepresentante")
	private String nifRepresentante;

	@JsonProperty("telefonoRepresentante")
	@XmlElement(name = "telefonoRepresentante")
	private String telefonoRepresentante;

	@JsonProperty("mailRepresentante")
	@XmlElement(name = "mailRepresentante")
	private String mailRepresentante;

	@JsonProperty("paisSolicitante")
	@XmlElement(name = "paisSolicitante")
	private String paisSolicitante;

	@JsonProperty("paisRepresentante")
	@XmlElement(name = "paisRepresentante")
	private String paisRepresentante;

	@JsonProperty("material")
	@XmlElement(name = "material")
	private String material;

	@JsonProperty("finalidad")
	@XmlElement(name = "finalidad")
	private String finalidad;

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getDireccionSolicitante() {
		return direccionSolicitante;
	}

	public void setDireccionSolicitante(String direccionSolicitante) {
		this.direccionSolicitante = direccionSolicitante;
	}

	public String getNifSolicitante() {
		return nifSolicitante;
	}

	public void setNifSolicitante(String nifSolicitante) {
		this.nifSolicitante = nifSolicitante;
	}

	public String getTelefonoSolicitante() {
		return telefonoSolicitante;
	}

	public void setTelefonoSolicitante(String telefonoSolicitante) {
		this.telefonoSolicitante = telefonoSolicitante;
	}

	public String getMailSolicitante() {
		return mailSolicitante;
	}

	public void setMailSolicitante(String mailSolicitante) {
		this.mailSolicitante = mailSolicitante;
	}

	public String getNifRepresentante() {
		return nifRepresentante;
	}

	public void setNifRepresentante(String nifRepresentante) {
		this.nifRepresentante = nifRepresentante;
	}

	public String getNombreRepresentante() {
		return nombreRepresentante;
	}

	public void setNombreRepresentante(String nombreRepresentante) {
		this.nombreRepresentante = nombreRepresentante;
	}

	public String getDireccionRepresentante() {
		return direccionRepresentante;
	}

	public void setDireccionRepresentante(String direccionRepresentante) {
		this.direccionRepresentante = direccionRepresentante;
	}

	public String getTelefonoRepresentante() {
		return telefonoRepresentante;
	}

	public void setTelefonoRepresentante(String telefonoRepresentante) {
		this.telefonoRepresentante = telefonoRepresentante;
	}

	public String getMailRepresentante() {
		return mailRepresentante;
	}

	public void setMailRepresentante(String mailRepresentante) {
		this.mailRepresentante = mailRepresentante;
	}

	public String getPaisSolicitante() {
		return paisSolicitante;
	}

	public void setPaisSolicitante(String paisSolicitante) {
		this.paisSolicitante = paisSolicitante;
	}

	public String getPaisRepresentante() {
		return paisRepresentante;
	}

	public void setPaisRepresentante(String paisRepresentante) {
		this.paisRepresentante = paisRepresentante;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getFinalidad() {
		return finalidad;
	}

	public void setFinalidad(String finalidad) {
		this.finalidad = finalidad;
	}

	@Override
	public String toString() {
		return "ImpresoLogosDTO [nombreSolicitante=" + nombreSolicitante + ", direccionSolicitante=" + direccionSolicitante + ", nifSolicitante=" + nifSolicitante + ", telefonoSolicitante="
				+ telefonoSolicitante + ", mailSolicitante=" + mailSolicitante + ", nombreRepresentante=" + nombreRepresentante + ", direccionRepresentante=" + direccionRepresentante
				+ ", telefonoRepresentante=" + telefonoRepresentante + ", mailRepresentante=" + mailRepresentante + ", paisSolicitante=" + paisSolicitante + ", paisRepresentante=" + paisRepresentante
				+ ", material=" + material + ", finalidad=" + finalidad + "]";
	}

}
