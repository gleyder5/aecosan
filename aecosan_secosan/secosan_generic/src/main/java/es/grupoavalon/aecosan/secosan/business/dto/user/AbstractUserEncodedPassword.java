package es.grupoavalon.aecosan.secosan.business.dto.user;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

public abstract class AbstractUserEncodedPassword extends UserDTO {

	public abstract String getPassword();

	@JsonIgnore
	public String getPasswordDecooded() {
		String decrypted = "";
		if (StringUtils.isNotEmpty(getPassword())) {
			decrypted = new String(Base64.decodeBase64(getPassword()));
		}
		return decrypted;
	}

}
