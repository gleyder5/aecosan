package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.EstadoSolicitudRepository;

@Repository("estadoSolicitudDAO")
public class EstadoSolicitudDAOImpl extends GenericDao implements EstadoSolicitudDAO {

	private static final Long INITAL_STATE = 1L;
	private static final String ERROR = " ERROR: ";

	@Autowired
	private EstadoSolicitudRepository estadoSolicitudRepository;

	@Override
	public EstadoSolicitudEntity addEstadoSolicitud(EstadoSolicitudEntity estadoSolicitudEntity) {
		EstadoSolicitudEntity estadoSolicitud = null;
		try {
			estadoSolicitud = estadoSolicitudRepository.save(estadoSolicitudEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addEstadoSolicitud | " + estadoSolicitudEntity + ERROR + exception.getMessage());
		}

		return estadoSolicitud;
	}

	@Override
	public EstadoSolicitudEntity updateEstadoSolicitud(EstadoSolicitudEntity estadoSolicitudEntity) {
		EstadoSolicitudEntity estadoSolicitud = null;
		try {
			estadoSolicitud = estadoSolicitudRepository.save(estadoSolicitudEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "updateEstadoSolicitud | " + estadoSolicitudEntity + ERROR + exception.getMessage());
		}

		return estadoSolicitud;
	}

	@Override
	public EstadoSolicitudEntity findEstadoSolicitud(Long pk) {

		EstadoSolicitudEntity estadoSolicitud = null;

		try {
			estadoSolicitud = estadoSolicitudRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findEstadoSolicitud | " + pk + ERROR + exception.getMessage());
		}

		return estadoSolicitud;
	}

	@Override
	public EstadoSolicitudEntity getInitialSolicitudeStatus() {
		EstadoSolicitudEntity estadoSolicitudEntity = null;
		logger.debug("## getInitialSolicitudeEstatus");
		try {
			estadoSolicitudEntity = estadoSolicitudRepository.findOne(INITAL_STATE);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "getInitialSolicitudeEstatus " + ERROR + exception.getMessage());
		}
		return estadoSolicitudEntity;
	}

	@Override
	public EstadoSolicitudEntity getStatusBySolicitudeId(Long solicitudeId) {
		EstadoSolicitudEntity estadoSolicitudEntity = null;
		try {
			estadoSolicitudEntity = estadoSolicitudRepository.getStatusBySolicitudeId(solicitudeId);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "getStatusBySolicitudeId " + ERROR + exception.getMessage());
		}
		return estadoSolicitudEntity;
	}

	@Override
	public List<EstadoSolicitudEntity> getStatusByAreaId(Long areaId) {
		List<EstadoSolicitudEntity> listStatus = null;
		try {
			listStatus = estadoSolicitudRepository.getStatusByAreaId(areaId);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "getStatusByAreaId " + ERROR + exception.getMessage());
		}
		return listStatus;
	}

}
