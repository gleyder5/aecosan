package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;

public interface DocumentacionRepository extends CrudRepository<DocumentacionEntity, Long> {

}
