package es.grupoavalon.aecosan.secosan.dao.repository;

import java.io.Serializable;
import java.util.List;

public interface CrudCatalogRepository<T, PK extends Serializable> {
	
	List<T> findAllOrderByValue();

	List<T> getCatalogListByParent(String parentElement, String parentId);

}
