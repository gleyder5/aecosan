package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios;

import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.ModificacionDatosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.interfaces.ComplementosAlimenticios;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_MODIFICACION_DATOS_CA)
public class ModificacionDatosCAEntity extends ModificacionDatosEntity implements ComplementosAlimenticios {


}
