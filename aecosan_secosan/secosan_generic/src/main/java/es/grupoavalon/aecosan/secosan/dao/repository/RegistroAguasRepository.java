package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;

public interface RegistroAguasRepository extends CrudRepository<RegistroAguasEntity, Long> {

}
