package es.grupoavalon.aecosan.secosan.dao.entity.registroAguas;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.PaisEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_REGISTRO_AGUAS)
public class RegistroAguasEntity extends SolicitudEntity {

	@Column(name = "COMERCIAL_PRODUCT_NAME")
	private String commercialProductName;

	@Column(name = "RGSEAA")
	private String rgseaa;

	@Column(name = "NOMBRE_MANANTIAL")
	private String nombreManantial;

	@Column(name = "LUGAR_EXPLOTACION")
	private String lugarExplotacion;

	@Column(name = "NOMBRE_EN_ORIGEN")
	private String nombreAguaPaisOrigen;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PAIS_ID")
	private PaisEntity paisOrigen;

	@Column(name = "TRATAMIENTO_AGUA")
	private String tratamientoAgua;

	@Column(name = "DATOS_OFICIALES")
	private String datosOficiales;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PAGO_SOLICITUD_ID")
	private PagoSolicitudEntity solicitudePayment;

	public String getCommercialProductName() {
		return commercialProductName;
	}

	public void setCommercialProductName(String commercialProductName) {
		this.commercialProductName = commercialProductName;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public String getNombreManantial() {
		return nombreManantial;
	}

	public void setNombreManantial(String nombreManantial) {
		this.nombreManantial = nombreManantial;
	}

	public String getLugarExplotacion() {
		return lugarExplotacion;
	}

	public void setLugarExplotacion(String lugarExplotacion) {
		this.lugarExplotacion = lugarExplotacion;
	}

	public String getNombreAguaPaisOrigen() {
		return nombreAguaPaisOrigen;
	}

	public void setNombreAguaPaisOrigen(String nombreAguaPaisOrigen) {
		this.nombreAguaPaisOrigen = nombreAguaPaisOrigen;
	}

	public PaisEntity getPaisOrigen() {
		return paisOrigen;
	}

	public void setPaisOrigen(PaisEntity paisOrigen) {
		this.paisOrigen = paisOrigen;
	}

	public String getTratamientoAgua() {
		return tratamientoAgua;
	}

	public void setTratamientoAgua(String tratamientoAgua) {
		this.tratamientoAgua = tratamientoAgua;
	}

	public String getDatosOficiales() {
		return datosOficiales;
	}

	public void setDatosOficiales(String datosOficiales) {
		this.datosOficiales = datosOficiales;
	}

	public PagoSolicitudEntity getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudEntity solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "RegistroAguasEntity [commercialProductName=" + commercialProductName + ", rgseaa=" + rgseaa + ", nombreManantial=" + nombreManantial + ", lugarExplotacion=" + lugarExplotacion
				+ ", nombreAguaPaisOrigen=" + nombreAguaPaisOrigen + ", paisOrigen=" + paisOrigen + ", tratamientoAgua=" + tratamientoAgua + ", datosOficiales=" + datosOficiales
				+ ", solicitudePayment=" + solicitudePayment + "]";
	}

}
