package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

import java.util.List;

public interface BecasService extends SolicitudService {

    List<BecasSolicitanteDTO> getBecasSolicitantesDTOPaginated(String[] otheParams, PaginationParams paginationParams);

    int getTotal(List<FilterParams> filters);
    List<BecasSolicitanteDTO> getBecasSolicitantesDTO();
    Double getBecaScore(String becaId);

    List<IdiomasBecasDTO> getIdiomasBecasDTOByBeca(Long id);
    List<BecasSolicitanteDTO> getBecasDTOByStatus(Long id);

}
