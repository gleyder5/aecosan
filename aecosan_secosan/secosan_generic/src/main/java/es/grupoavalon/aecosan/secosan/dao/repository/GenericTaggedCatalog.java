package es.grupoavalon.aecosan.secosan.dao.repository;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

public interface GenericTaggedCatalog<T> {

	List<T> callMethod(Method method) throws DaoException;

	Map<String, Method> getTaggedMethods();
}
