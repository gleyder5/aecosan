package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.HistorialSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

import java.util.List;

public interface HistorialSolicitudService {

    void add( SolicitudDTO historialSolicitudDTO);
    List<HistorialSolicitudDTO> getHistorialList(PaginationParams paginationParams);
    HistorialSolicitudDTO getBySolicitudAndStatus(Integer solicitudId, Integer statusId);
    int getTotal(List<FilterParams> filters);





}
