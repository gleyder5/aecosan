package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.SubvencionesAsocuaeService;
import es.grupoavalon.aecosan.secosan.business.SubvencionesJuntasService;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesJuntasDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.SubvencionesAsocuaeDAO;
import es.grupoavalon.aecosan.secosan.dao.SubvencionesJuntasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesJuntasConsumoEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SubvencionesJuntasServiceImpl extends AbstractSolicitudService implements SubvencionesJuntasService{

	@Autowired
	private SubvencionesJuntasDAO subvencionesJuntasDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO subvencionesJuntasDTO) {
		try {
			solicitudPrepareNullDTO(subvencionesJuntasDTO);
			prepareSubvencionesAsocuaeNullability((SubvencionesJuntasDTO) subvencionesJuntasDTO);
			addSubvencionesJuntasConsumoEntity(user, (SubvencionesJuntasDTO) subvencionesJuntasDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + subvencionesJuntasDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO subvencionesJuntasDTO) {
		try {
			solicitudPrepareNullDTO(subvencionesJuntasDTO);
			prepareSubvencionesAsocuaeNullability((SubvencionesJuntasDTO) subvencionesJuntasDTO);
			updateProcediminetoGeneral(user, (SubvencionesJuntasDTO) subvencionesJuntasDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + subvencionesJuntasDTO);
		}
	}

	private void addSubvencionesJuntasConsumoEntity(String user, SubvencionesJuntasDTO subvencionesJuntasDTO) {
		UbicacionGeograficaDTO location = null;
		if(null != subvencionesJuntasDTO.getLugarOMedio()) {
			location = subvencionesJuntasDTO.getLugarOMedio().getLocation();
			if (location != null) {
				location.setProvince(SecosanUtil.checkDtoNullability(location.getProvince()));
				location.setMunicipio(SecosanUtil.checkDtoNullability(location.getMunicipio()));
			}
			subvencionesJuntasDTO.getLugarOMedio().setLocation(location);
		}
		SubvencionesJuntasConsumoEntity subvencionesJuntasConsumoEntity	=	generateSubvencionesJuntasConsumoEntityFromDTO(subvencionesJuntasDTO);
		setUserCreatorByUserId(user, subvencionesJuntasConsumoEntity);
		prepareAddSolicitudEntity(subvencionesJuntasConsumoEntity,subvencionesJuntasDTO);
		SubvencionesJuntasConsumoEntity subvencionesJuntasConsumoEntityNew	=	subvencionesJuntasDAO.addSubvencionesJuntasEntity(subvencionesJuntasConsumoEntity);
		if(subvencionesJuntasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(subvencionesJuntasConsumoEntityNew);
		}
	}

	private void updateProcediminetoGeneral(String user, SubvencionesJuntasDTO subvencionesJuntasDTO) {
		SubvencionesJuntasConsumoEntity subvencionesJuntasConsumoEntity= transformDTOToEntity(subvencionesJuntasDTO, SubvencionesJuntasConsumoEntity.class);
		setUserCreatorByUserId(user, subvencionesJuntasConsumoEntity);
		prepareUpdateSolicitudEntity(subvencionesJuntasConsumoEntity, subvencionesJuntasDTO);
		if(subvencionesJuntasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(subvencionesJuntasConsumoEntity);
		}
		subvencionesJuntasDAO.updateSubvencionesJuntasEntity(subvencionesJuntasConsumoEntity);
	}

	private SubvencionesJuntasConsumoEntity generateSubvencionesJuntasConsumoEntityFromDTO(SubvencionesJuntasDTO subvencionesAsocuae) {
		prepareSolicitudDTO(subvencionesAsocuae);
		return transformDTOToEntity(subvencionesAsocuae,SubvencionesJuntasConsumoEntity.class);
	}
	private void prepareSubvencionesAsocuaeNullability(SubvencionesJuntasDTO subvencionesJuntasDTO) {
		if (null != subvencionesJuntasDTO.getLugarOMedio()) {
//			subvencionesJuntasDTO.getLugarOMedio().setLocation(SecosanUtil.checkDtoNullability(subvencionesJuntasDTO.getLugarOMedio().getLocation()));
			if (null != subvencionesJuntasDTO.getLugarOMedio().getLocation()) {
				prepareLocationNull(subvencionesJuntasDTO.getLugarOMedio().getLocation());
			}
		}

	}
}
