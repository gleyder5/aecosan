/**
 * 
 */
package es.grupoavalon.aecosan.secosan.util.dtofactory;

import java.util.List;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.BeanFromBeanFactory;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.ReverseBeanFromBeanFactory;

/**
 * @author otto.abreu
 *
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class DtoEntityFactory<ENTITY, DTO> {

	private Class<?> entityClass;
	private Class<?> dtoClass;

	private DtoEntityFactory(Class<?> entityClass, Class<?> dtoClass) {
		super();
		this.entityClass = entityClass;
		this.dtoClass = dtoClass;
	}

	public static DtoEntityFactory getInstance(Class<?> entityClass,
			Class<?> dtoClass) {
		return new DtoEntityFactory(entityClass, dtoClass);
	}

	public DTO generateDTOFromEntity(Object entity) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForOneToOne(entity);
		return dtoFactory.generateDTOFromEntity();
	}

	public DTO generateDTOFromEntityNoCollections(Object entity) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForOneToOne(entity);
		return dtoFactory.generateDTOFromEntityNoCollections();
	}

	public DTO generateDTOFromEntityOnllyCollections(Object entity) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForOneToOne(entity);
		return dtoFactory.generateDTOFromEntityOnllyCollections();
	}

	private BeanFromBeanFactory<DTO> instanciateDtoFactoryForOneToOne(
			Object entity) {
		BeanFromBeanFactory<DTO> dtoFactory = (BeanFromBeanFactory<DTO>) BeanFromBeanFactory
				.getInstance(entity, dtoClass);
		return dtoFactory;
	}

	public List<DTO> generateListDTOFromEntity(List<?> entityList) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForColections();
		return dtoFactory.generateListDTOFromEntity(entityList);
	}

	public List<DTO> generateListDTOFromEntityNoCollections(List<?> entityList) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForColections();
		return dtoFactory.generateListDTOFromEntityNoCollections(entityList);
	}

	public List<DTO> generateListDTOFromEntityOnlyCollections(List<?> entityList) {
		BeanFromBeanFactory<DTO> dtoFactory = this
				.instanciateDtoFactoryForColections();
		return dtoFactory.generateListDTOFromEntityOnlyCollections(entityList);
	}

	private BeanFromBeanFactory<DTO> instanciateDtoFactoryForColections() {
		BeanFromBeanFactory<DTO> dtoFactory = (BeanFromBeanFactory<DTO>) BeanFromBeanFactory
				.getInstance(dtoClass);
		return dtoFactory;
	}

	public ENTITY generateEntityFromDTO(Object dto) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForOneToOne(dto);
		return factory.generateEntityFromDTO();
	}

	public ENTITY generateEntityFromDTONoCollections(Object dto) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForOneToOne(dto);
		return factory.generateEntityFromDTONoCollections();
	}

	public ENTITY generateEntityFromDTOOnlyCollections(Object dto) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForOneToOne(dto);
		return factory.generateEntityFromDTOOnlyCollections();
	}

	private ReverseBeanFromBeanFactory<ENTITY> instanciateEntityFactoryForOneToOne(
			Object dto) {
		ReverseBeanFromBeanFactory<ENTITY> entityFactory = (ReverseBeanFromBeanFactory<ENTITY>) ReverseBeanFromBeanFactory
				.getInstance(dto, entityClass);
		return entityFactory;
	}

	public List<ENTITY> generateListEntityFromDTO(List<?> dtoList) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForColections();
		return factory.generateListEntityFromDTO(dtoList);
	}

	public List<ENTITY> generateListEntityFromDTONoCollection(List<?> dtoList) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForColections();
		return factory.generateListEntityFromDTONoCollections(dtoList);
	}

	public List<ENTITY> generateListEntityFromDTOOnlyCollection(List<?> dtoList) {
		ReverseBeanFromBeanFactory<ENTITY> factory = this
				.instanciateEntityFactoryForColections();
		return factory.generateListEntityFromDTOOnlyCollections(dtoList);
	}

	private ReverseBeanFromBeanFactory<ENTITY> instanciateEntityFactoryForColections() {
		ReverseBeanFromBeanFactory<ENTITY> entityFactory = (ReverseBeanFromBeanFactory<ENTITY>) ReverseBeanFromBeanFactory
				.getInstance(entityClass);
		return entityFactory;
	}

}
