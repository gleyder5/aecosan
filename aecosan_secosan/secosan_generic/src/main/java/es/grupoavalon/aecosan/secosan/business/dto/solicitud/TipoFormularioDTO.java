package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class TipoFormularioDTO extends AbstractCatalogDto {

	@JsonProperty("singDocumentTemplate")
	@XmlElement(name = "singDocumentTemplate")
	@BeanToBeanMapping
	private String singDocumentTemplate;
	
	@JsonProperty("tasaModelo")
	@XmlElement(name = "tasaModelo")
	@BeanToBeanMapping(generateOther=true)
	TasaModeloDTO tasaModelo;
	
	@JsonProperty("solicitudeType")
	@XmlElement(name = "solicitudeType")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "solicitudeType")
	private TipoSolicitudDTO solicitudeType;

	public TipoSolicitudDTO getSolicitudeType() {
		return solicitudeType;
	}

	public void setSolicitudeType(TipoSolicitudDTO solicitudeType) {
		this.solicitudeType = solicitudeType;
	}

	public String getSingDocumentTemplate() {
		return singDocumentTemplate;
	}

	public void setSingDocumentTemplate(String singDocumentTemplate) {
		this.singDocumentTemplate = singDocumentTemplate;
	}

	public TasaModeloDTO getTasaModelo() {
		return tasaModelo;
	}

	public void setTasaModelo(TasaModeloDTO tasaModelo) {
		this.tasaModelo = tasaModelo;
	}
	
}
