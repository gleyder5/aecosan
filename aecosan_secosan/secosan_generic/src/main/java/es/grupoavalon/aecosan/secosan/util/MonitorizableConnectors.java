package es.grupoavalon.aecosan.secosan.util;

public enum MonitorizableConnectors {

	SIGM("SIGM"), SNEC("SNEC"), EPAGO("EPAGO"), CLAVE("CLAVE"),APODERAMINETO("APODERAMIENTO");

	private String name;

	private MonitorizableConnectors(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
