package es.grupoavalon.aecosan.secosan.dao.entity.pago;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_DATOS_PAGO)
public class DatosPagoEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.DATOS_PAGO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.DATOS_PAGO_SEQUENCE, sequenceName = SequenceNames.DATOS_PAGO_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "ENTIDAD_BANCARIA")
	private String entity;

	@Column(name = "CUENTA")
	private String account;

	@Column(name = "NRC_PAGO_ELECTRONICO")
	private String nrc;

	@Column(name = "JUSTIFICANTE_PAGO_ELECTRONICO")
	private String justifier;

	@Column(name = "NUMERO_TARJETA")
	private String cardNumber;

	@Column(name = "VENCIMIENTO_TARJETA")
	private Date expirationCardDate;

	@Column(name = "FECHA_PAGO")
	private Date paymentDate;

	@Transient
	private Long paymentDateLong;

	public Long getPaymentDateLong() {
		return paymentDateLong;
	}

	public void setPaymentDate(long paymentDateLong) {
		this.setPaymentDate(new Date(paymentDateLong));
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
		if (this.paymentDate != null) {
			this.paymentDateLong = Long.valueOf(paymentDate.getTime());
		}
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public Date getExpirationCardDate() {
		return expirationCardDate;
	}

	public void setExpirationCardDate(Date expirationCardDate) {
		this.expirationCardDate = expirationCardDate;
	}

	public String getJustifier() {
		return justifier;
	}

	public void setJustifier(String justifier) {
		this.justifier = justifier;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((account == null) ? 0 : account.hashCode());
		result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
		result = prime * result + ((entity == null) ? 0 : entity.hashCode());
		result = prime * result + ((expirationCardDate == null) ? 0 : expirationCardDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nrc == null) ? 0 : nrc.hashCode());
		result = prime * result + ((justifier == null) ? 0 : justifier.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DatosPagoEntity other = (DatosPagoEntity) obj;
		if (account == null) {
			if (other.account != null) {
				return false;
			}
		} else if (!account.equals(other.account)) {
			return false;
		}
		if (cardNumber == null) {
			if (other.cardNumber != null) {
				return false;
			}
		} else if (!cardNumber.equals(other.cardNumber)) {
			return false;
		}



		if (entity == null) {
			if (other.entity != null) {
				return false;
			}
		} else if (!entity.equals(other.entity)) {
			return false;
		}
		if (expirationCardDate == null) {
			if (other.expirationCardDate != null) {
				return false;
			}
		} else if (!expirationCardDate.equals(other.expirationCardDate)) {
			return false;
		}

		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (nrc == null) {
			if (other.nrc != null) {
				return false;
			}
		} else if (!nrc.equals(other.nrc)) {
			return false;
		}
		if (justifier == null) {
			if (other.justifier != null) {
				return false;
			}
		} else if (!justifier.equals(other.justifier)) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "DatosPagoEntity [id=" + id + ", entity=" + entity + ", account=" + account + ", nrc=" + nrc + ", justifier=" + justifier + ", cardNumber=" + cardNumber + ", expirationCardDate="
				+ expirationCardDate + ", paymentDate=" + paymentDate + ", paymentDateLong=" + paymentDateLong + "]";
	}

}
