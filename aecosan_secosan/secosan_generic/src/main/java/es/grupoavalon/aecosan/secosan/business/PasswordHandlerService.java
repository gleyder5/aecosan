package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.user.ChangePasswordDTO;

public interface PasswordHandlerService {

	void changeUserPassword(ChangePasswordDTO changePasswordDto);

}