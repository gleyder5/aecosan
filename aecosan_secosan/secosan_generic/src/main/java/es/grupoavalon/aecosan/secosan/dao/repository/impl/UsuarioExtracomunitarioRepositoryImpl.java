package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioExtraComunitarioEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.UsuarioExtracomunitarioRepository;

@Component
public class UsuarioExtracomunitarioRepositoryImpl extends AbstractCrudRespositoryImpl<UsuarioExtraComunitarioEntity, Long> implements UsuarioExtracomunitarioRepository {

	@Override
	protected Class<UsuarioExtraComunitarioEntity> getClassType() {
		return UsuarioExtraComunitarioEntity.class;
	}

	@Override
	public UsuarioExtraComunitarioEntity findByUserLogin(String userLogin) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("login", userLogin);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_USUARIO_BY_LOGIN, queryParams);
	}

}
