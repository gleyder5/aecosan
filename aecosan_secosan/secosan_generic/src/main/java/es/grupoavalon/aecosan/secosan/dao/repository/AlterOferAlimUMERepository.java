package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;

public interface AlterOferAlimUMERepository extends CrudRepository<AlterOferAlimUMEEntity, Long> {

}
