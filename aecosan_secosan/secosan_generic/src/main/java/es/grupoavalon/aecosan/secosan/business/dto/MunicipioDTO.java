package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class MunicipioDTO extends AbstractCatalogDto implements IsNulable<MunicipioDTO> {

	@JsonProperty("idlocalidad")
	@XmlElement(name = "idlocalidad")
	@BeanToBeanMapping(getValueFrom = "idlocalidad")
	private String localidad;

	@JsonProperty("provincia")
	@XmlElement(name = "provincia")
	@BeanToBeanMapping(getValueFrom = "province", getSecondValueFrom = "id")
	Long province;

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Long getProvince() {
		return province;
	}

	public void setProvince(Long province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "MunicipioDTO [id=" + id + ", localidad=" + localidad + ", provincia=" + province + "]";
	}

	@Override
	public MunicipioDTO shouldBeNull() {
		if ("".equals(this.getId())) {
			return null;
		} else {
			return this;
		}
	}

}
