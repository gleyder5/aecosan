package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_ESTADOS_AREAS)
public class EstadosAreasEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.ESTADOS_AREAS_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.ESTADOS_AREAS_SEQUENCE, sequenceName = SequenceNames.ESTADOS_AREAS_SEQUENCE, allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "AREA_ID", nullable = false)
	private AreaEntity area;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "ESTADO_ID", nullable = false)
	private EstadoSolicitudEntity status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public AreaEntity getArea() {
		return area;
	}

	public void setArea(AreaEntity area) {
		this.area = area;
	}

	public EstadoSolicitudEntity getStatus() {
		return status;
	}

	public void setStatus(EstadoSolicitudEntity status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((status == null) ? 0 : status.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadosAreasEntity other = (EstadosAreasEntity) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.getId().equals(other.area.getId()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.getId().equals(other.status.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {

		String areaId = "";
		String statusId = "";

		if (area != null && area.getId() != null) {
			areaId = String.valueOf(area.getId());
		}

		if (status != null && status.getId() != null) {
			statusId = String.valueOf(status.getId());
		}

		return "EstadosAreasEntity [id=" + id + ", area=" + areaId + ", status=" + statusId + "]";
	}

}
