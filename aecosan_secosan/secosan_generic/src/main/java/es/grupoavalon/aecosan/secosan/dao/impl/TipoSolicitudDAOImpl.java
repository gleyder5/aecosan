package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.TipoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.FormularioRepository;

@Repository
public class TipoSolicitudDAOImpl extends GenericDao implements TipoSolicitudDAO {

	@Autowired
	private FormularioRepository formularioRepository;
	

	@Override
	public TipoSolicitudEntity findFormulario(Long pk) {

		TipoSolicitudEntity tipoSolicitud = null;

		try {
			tipoSolicitud = formularioRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " findFormulario | " + pk + " ERROR: " + exception.getMessage());
		}

		return tipoSolicitud;
	}

}
