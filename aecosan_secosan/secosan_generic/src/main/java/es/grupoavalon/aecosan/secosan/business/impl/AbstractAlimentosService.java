package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PresentacionProductoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;

public abstract class AbstractAlimentosService extends AbstractSolicitudService {

	protected void preparePuestaMercadoEntity(PuestaMercadoEntity puestaMercadoEntity, SolicitudDTO solicitudDTO) {
		prepareAddSolicitudEntity(puestaMercadoEntity, solicitudDTO);
		puestaMercadoPreparePresentacionEntity(puestaMercadoEntity);
	}

	protected void preparePuestaMercadoEntityUpdate(PuestaMercadoEntity puestaMercadoEntity, SolicitudDTO solicitudDTO) {
		prepareUpdateSolicitudEntity(puestaMercadoEntity, solicitudDTO);
		puestaMercadoPreparePresentacionEntity(puestaMercadoEntity);
	}

	protected static void puestaMercadoPreparePresentacionEntity(PuestaMercadoEntity puestaMercadoEntity) {
		if (puestaMercadoEntity.getPresentaciones() != null) {
			for (PresentacionProductoEntity presentacion : puestaMercadoEntity.getPresentaciones()) {
				presentacion.setPuestaMercado(puestaMercadoEntity);
			}
		}
	}

}
