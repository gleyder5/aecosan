package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.RegistroActividadDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.RegistroActividadEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroActividadRepository;

@Repository
public class RegistroActividadDAOImpl extends GenericDao implements RegistroActividadDAO {

	@Autowired
	private RegistroActividadRepository registroActividadRepo;

	@Override
	public void registerActivity(RegistroActividadEntity activity) {

		try {
			this.registroActividadRepo.save(activity);
		} catch (Exception e) {
			this.handleException(e, "can not save activity: " + activity);
		}
	}

}
