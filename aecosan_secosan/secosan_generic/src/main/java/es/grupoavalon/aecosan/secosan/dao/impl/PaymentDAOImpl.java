package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.dao.PaymentDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.PagoSolicitudRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudRepository;

@Repository
@Transactional
public class PaymentDAOImpl extends GenericDao implements PaymentDAO {

	@Autowired
	private PagoSolicitudRepository pagoRepo;

	@Autowired
	private SolicitudRepository solicitudRepo;

	@Override
	public void savePayment(PagoSolicitudEntity paymentData) {
		try {
			this.pagoRepo.save(paymentData);
		} catch (Exception e) {
			this.handleException(e, "savePayment | paymentData:" + paymentData);
		}
	}
	
	@Override
	public void updatePayment(PagoSolicitudEntity paymentData) {
		try {
			this.pagoRepo.update(paymentData);
		} catch (Exception e) {
			this.handleException(e, "update | paymentData:" + paymentData);
		}
	}

	@Override
	public PagoSolicitudEntity getPagoSolicitudEntityByPetitionNumber(String refNum) {
		PagoSolicitudEntity pagoEntity = null;

		try {
			pagoEntity = this.pagoRepo.getPagoSolicitudEntityBySolicitudeRefNum(refNum);
		} catch (Exception e) {
			this.handleException(e, "getPagoSolicitudEntityByPetitionNumber | refNum:" + refNum);
		}

		return pagoEntity;
	}

	@Override
	public PagoSolicitudEntity getPagoSolicitudEntityBySolicitudeId(long solicitudId) {
		PagoSolicitudEntity pagoEntity = null;

		try {
			pagoEntity = this.pagoRepo.getPagoSolicitudEntityBySolicitudeId(solicitudId);
			if (pagoEntity == null) {
				SolicitudEntity solicitudEntity = this.solicitudRepo.findOne(solicitudId);
				String refNum = solicitudEntity.getIdentificadorPeticion();
				pagoEntity = this.pagoRepo.getPagoSolicitudEntityBySolicitudeRefNum(refNum);
				if (pagoEntity != null) {
					logger.warn("The payment with id:" + pagoEntity.getId() + " was found using the solicitud pettion number instead of solicitud id, update the database ");
				}
			}
		} catch (Exception e) {
			this.handleException(e, "getPagoSolicitudEntityBySolicitudeId | solicitudId:" + solicitudId);
		}

		return pagoEntity;
	}
}
