package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;

public class BajaOSuspensionDTO extends AbstractCatalogDto implements IsNulable<BajaOSuspensionDTO> {

	@Override
	public BajaOSuspensionDTO shouldBeNull() {
		if ("".equals(id) || id == null) {
			return null;
		} else {
			return this;
		}
	}

}
