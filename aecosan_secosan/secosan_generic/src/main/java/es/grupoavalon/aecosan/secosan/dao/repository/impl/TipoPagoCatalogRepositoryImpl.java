package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("TipoPago")
public class TipoPagoCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<TipoPagoEntity, Long> implements GenericCatalogRepository<TipoPagoEntity> {

	@Override
	public List<TipoPagoEntity> getCatalogList() {

		return findAll();
	}

	@Override
	protected Class<TipoPagoEntity> getClassType() {

		return TipoPagoEntity.class;
	}

}
