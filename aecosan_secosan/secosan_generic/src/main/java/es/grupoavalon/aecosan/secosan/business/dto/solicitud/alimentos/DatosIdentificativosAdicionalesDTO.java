package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class DatosIdentificativosAdicionalesDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("isProductMaker")
	@XmlElement(name = "isProductMaker")
	@BeanToBeanMapping(getValueFrom = "isProductMaker")
	private Boolean isProductMaker;

	@JsonProperty("rgseaa")
	@XmlElement(name = "rgseaa")
	@BeanToBeanMapping(getValueFrom = "rgseaa")
	private String rgseaa;

	@JsonProperty("solicitudeInclude")
	@XmlElement(name = "solicitudeInclude")
	@BeanToBeanMapping(getValueFrom = "solicitudeInclude")
	private Boolean solicitudeInclude;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsProductMaker() {
		return isProductMaker;
	}

	public void setIsProductMaker(Boolean isProductMaker) {
		this.isProductMaker = isProductMaker;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public Boolean isSolicitudeInclude() {
		return solicitudeInclude;
	}

	public void setSolicitudeInclude(Boolean solicitudeInclude) {
		this.solicitudeInclude = solicitudeInclude;
	}

	@Override
	public String toString() {
		return "DatosIdentificativosAdicionalesDTO [id=" + id + ", isProductMaker=" + isProductMaker + ", rgseaa=" + rgseaa + ", solicitudeInclude=" + solicitudeInclude + "]";
	}

}
