package es.grupoavalon.aecosan.secosan.business.impl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.IdentificacionPeticionService;
import es.grupoavalon.aecosan.secosan.business.dto.IdentificacionPeticionDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;

@Service
public class IdentificacionPeticionServiceImpl extends AbstractManager implements IdentificacionPeticionService {

	private static final String DATE_FORMAT = "yyyyMMddHHmmss";

	@Override
	public IdentificacionPeticionDTO getIdentificationRequestByAreaId(String areaId) {
		Long area = 0L;
		IdentificacionPeticionDTO identificationRequest = new IdentificacionPeticionDTO();
		try {
			area = Long.valueOf(areaId);
			identificationRequest.setIdentificationRequest(sysDate() + areaId);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getIdentificationRequestByAreaId | area : " + area + " | ERROR: " + e.getMessage());
		}
		return identificationRequest;
	}

	private static String sysDate() {
		Date sysDate = new Date(System.currentTimeMillis());
		return new SimpleDateFormat(DATE_FORMAT).format(sysDate);
	}

}
