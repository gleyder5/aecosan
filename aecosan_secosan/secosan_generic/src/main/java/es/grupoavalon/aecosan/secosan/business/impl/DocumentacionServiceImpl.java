package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.DocumentacionService;
import es.grupoavalon.aecosan.secosan.business.dto.TipoDocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.DocumentacionDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;

@Service
public class DocumentacionServiceImpl extends AbstractManager implements DocumentacionService {

	@Autowired
	private DocumentacionDAO documentacionDAO;

	@Override
	public void deleteDocumentacionById(String user, String id) {
		try {
			Long documentacionId = Long.valueOf(id);
			documentacionDAO.deleteDocumentacion(documentacionId);

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " deleteDocumentacionById | user: " + user + "| id: " + id);
		}

	}

	@Override
	public DocumentacionDTO findDocumentationById(String docId) {
		DocumentacionDTO doc = null;
		try {
			DocumentacionEntity docEntity = documentacionDAO.findDocumentacionById(Long.valueOf(docId));
			doc = transformEntityToDTO(docEntity, DocumentacionDTO.class);
			prepareDocumentation(doc, docEntity);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " findDocumentationById | docId: " + docId);
		}
		return doc;
	}

	private void prepareDocumentation(DocumentacionDTO doc, DocumentacionEntity docEntity) {
		Base64TobytesArrayTransformer b64transform = new Base64TobytesArrayTransformer();
		String base64 = String.valueOf(b64transform.transform(docEntity.getDocument()));
		doc.setBase64(base64);
	}


	@Override
	public List<TipoDocumentacionDTO> findDocumentationByFormId(String formId) {
		List<TipoDocumentacionDTO> listDocumentationTypeDto = null;
		try {
			List<TipoDocumentacionEntity> listDocumentationTypeEntity = documentacionDAO.findDocumentationByFormId(formId);
			listDocumentationTypeDto = transformEntityListToDTOList(listDocumentationTypeEntity, TipoDocumentacionEntity.class, TipoDocumentacionDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " findDocumentationByFormId | formId: " + formId);
		}

		return listDocumentationTypeDto;
	}

}
