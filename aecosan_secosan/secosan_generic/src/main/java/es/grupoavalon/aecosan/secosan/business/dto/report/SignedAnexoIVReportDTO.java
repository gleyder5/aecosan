package es.grupoavalon.aecosan.secosan.business.dto.report;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.ProductoSuspendidoDTO;

public class SignedAnexoIVReportDTO extends SignedReportDTO {

	private String company;

	private String signer;

	private String address;

	private String companyRegNumber;

	private String personInCharge;

	private String phone;

	private String mail;

	private String comunicationDate;

	private String comunicantionType;

	private String motive;

	private String suspension;

	private String baja;



	private List<ProductoSuspendidoDTO> products;

	public String getSuspension() {
		return suspension;
	}

	public void setSuspension(String suspension) {
		this.suspension = suspension;
	}

	public String getBaja() {
		return baja;
	}

	public void setBaja(String baja) {
		this.baja = baja;
	}


	public void setComunicationDate(String comunicationDate) {
		this.comunicationDate = comunicationDate;
	}

	@Override
	public String toString() {
		return "SignedAnexoIVReportDTO [ " + super.toString() + ",company=" + company + ", signer=" + signer + ", address=" + address + ", companyRegNumber=" + companyRegNumber + ", personInCharge="
				+ personInCharge + ", phone=" + phone + ", mail=" + mail + ", domunicationDate=" + comunicationDate + ", comunicantionType=" + comunicantionType + ", motive=" + motive + ", products="
				+ products + "]";
	}

	public List<ProductoSuspendidoDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductoSuspendidoDTO> products) {
		this.products = products;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSigner() {
		return signer;
	}

	public void setSigner(String signer) {
		this.signer = signer;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompanyRegNumber() {
		return companyRegNumber;
	}

	public void setCompanyRegNumber(String companyRegNumber) {
		this.companyRegNumber = companyRegNumber;
	}

	public String getPersonInCharge() {
		return personInCharge;
	}

	public void setPersonInCharge(String personInCharge) {
		this.personInCharge = personInCharge;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getComunicationDate() {
		return comunicationDate;
	}

	public void setComunicationDate(Long comunicationDate) {
		if (comunicationDate == null) {
			this.comunicationDate = ". . . . . . . . . . . . . . . . . ";
		} else {
			Date reportDate = new Date(comunicationDate);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			this.comunicationDate = sdf.format(reportDate);
		}
	}

	public String getComunicantionType() {
		return comunicantionType;
	}

	public void setComunicantionType(String comunicantionType) {
		this.comunicantionType = comunicantionType;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

}
