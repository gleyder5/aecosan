package es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.PagoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;

public class EvaluacionRiesgosDTO extends SolicitudDTO implements IsNulable<EvaluacionRiesgosDTO> {

	@JsonProperty("solicitudeSubject")
	@XmlElement(name = "solicitudeSubject")
	@BeanToBeanMapping(getValueFrom = "solicitudeSubject")
	private String solicitudeSubject;

	@JsonProperty("commercialProductName")
	@XmlElement(name = "commercialProductName")
	@BeanToBeanMapping(getValueFrom = "commercialProductName")
	private String commercialProductName;

	@JsonProperty("payTypeService")
	@XmlElement(name = "payTypeService")
	@BeanToBeanMapping(getValueFrom = "payTypeService", generateOther = true)
	private TipoPagoServicioDTO payTypeService;

	@JsonProperty("solicitudePayment")
	@XmlElement(name = "solicitudePayment")
	@BeanToBeanMapping(getValueFrom = "solicitudePayment", generateOther = true)
	@IgnoreField(inForward=true)
	private PagoSolicitudDTO solicitudePayment;

	public String getSolicitudeSubject() {
		return solicitudeSubject;
	}

	public void setSolicitudeSubject(String solicitudeSubject) {
		this.solicitudeSubject = solicitudeSubject;
	}

	public String getCommercialProductName() {
		return commercialProductName;
	}

	public void setCommercialProductName(String commercialProductName) {
		this.commercialProductName = commercialProductName;
	}

	public TipoPagoServicioDTO getPayTypeService() {
		return payTypeService;
	}

	public void setPayTypeService(TipoPagoServicioDTO payTypeService) {
		this.payTypeService = payTypeService;
	}

	public PagoSolicitudDTO getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudDTO solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public String toString() {
		return "EvaluacionRiesgosDTO [solicitudeSubject=" + solicitudeSubject + ", commercialProductName=" + commercialProductName + ", payTypeService=" + payTypeService + ", solicitudePayment="
				+ solicitudePayment + "]";
	}

	@Override
	public EvaluacionRiesgosDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}

	private boolean checkNullability() {
		boolean nullabillity = true;

		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getSolicitudeSubject());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getCommercialProductName());
		nullabillity = nullabillity && solicitudePayment.shouldBeNull() == null;

		return nullabillity;
	}

}
