package es.grupoavalon.aecosan.secosan.util.rest.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * Class used for rest security with Spring security the commence is invoked
 * when user tries to access a secured REST resource without supplying any
 * credentials We should just send a 401 Unauthorized response because there is
 * no 'login page' to redirect to
 * 
 * 
 * @author ottoabreu
 *
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {

	private String realmName;
	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException arg2)
			throws IOException, ServletException {
		if(arg2 == null){
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		}else{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Unauthorized 401 "+getRealmName());
		}
		

	}
	public String getRealmName() {
		return realmName;
	}
	public void setRealmName(String realmName) {
		this.realmName = realmName;
	}
	

}
