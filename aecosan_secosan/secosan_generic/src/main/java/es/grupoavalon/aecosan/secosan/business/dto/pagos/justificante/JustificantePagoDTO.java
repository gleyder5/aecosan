package es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.DatosPagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.FormaPagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.IdentificadorPagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoFormularioDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;


public class JustificantePagoDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;
	
	@JsonProperty("amount")
	@XmlElement(name = "amount")
	@BeanToBeanMapping(useCustomTransformer = PrimitiveToStringTransformer.class)
	private String amount;
	
	@JsonProperty("payedTasas")
	@XmlElement(name = "payedTasas")
	@BeanToBeanMapping(getValueFrom = "payedTasas",useCustomTransformer = PrimitiveToStringTransformer.class)
	private String payedTasas = "1";
	
	
	@JsonProperty("identificadorPeticion")
	@XmlElement(name = "identificadorPeticion")
	@BeanToBeanMapping
	private String identificadorPeticion;
	
	@JsonProperty("payment")
	@XmlElement(name = "payment")
	@BeanToBeanMapping(generateOther = true)
	private FormaPagoDTO payment;
	
	@JsonProperty("idPayment")
	@XmlElement(name = "idPayment")
	@BeanToBeanMapping(generateOther = true)
	private IdentificadorPagoDTO idPayment;
	
	@JsonProperty("dataPayment")
	@XmlElement(name = "dataPayment")
	@BeanToBeanMapping(generateOther = true)
	private DatosPagoDTO dataPayment;
	
	@JsonProperty("formType")
	@XmlElement(name = "formType")
	@BeanToBeanMapping(generateOther = true)
	private TipoFormularioDTO formType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getIdentificadorPeticion() {
		return identificadorPeticion;
	}

	public void setIdentificadorPeticion(String identificadorPeticion) {
		this.identificadorPeticion = identificadorPeticion;
	}

	public FormaPagoDTO getPayment() {
		return payment;
	}

	public void setPayment(FormaPagoDTO payment) {
		this.payment = payment;
	}

	public IdentificadorPagoDTO getIdPayment() {
		return idPayment;
	}

	public void setIdPayment(IdentificadorPagoDTO idPayment) {
		this.idPayment = idPayment;
	}

	public DatosPagoDTO getDataPayment() {
		return dataPayment;
	}

	public void setDataPayment(DatosPagoDTO dataPayment) {
		this.dataPayment = dataPayment;
	}

	public TipoFormularioDTO getFormType() {
		return formType;
	}

	public void setFormType(TipoFormularioDTO formType) {
		this.formType = formType;
	}

	public String getPayedTasas() {
		return payedTasas;
	}

	public void setPayedTasas(String payedTasas) {
		this.payedTasas = payedTasas;
	}

	@Override
	public String toString() {
		return "JustificantePagoDTO [id=" + id + ", amount=" + amount + ", identificadorPeticion=" + identificadorPeticion + ", payment=" + payment + "payedTasas=" + payedTasas + ", idPayment="
				+ idPayment + ", dataPayment="
				+ dataPayment + "]";
	}
	
}
