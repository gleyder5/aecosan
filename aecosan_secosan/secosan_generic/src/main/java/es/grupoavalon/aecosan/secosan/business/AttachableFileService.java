package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.AttachableFileDTO;

public interface AttachableFileService {

	AttachableFileDTO downloadAttachableFileByName(String fileName);

}
