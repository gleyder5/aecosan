package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.MonitorizacionConectoresEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface MonitorizacionConectoresDAO {

	public MonitorizacionConectoresEntity registerNewConnectorActivity(MonitorizacionConectoresEntity monitorEntity);

	public List<MonitorizacionConectoresEntity> getListMonitoringConnector(PaginationParams paginationParams);

	public int getTotalListMonitoringConnector(List<FilterParams> filters);

	public int getFilteredListMonitoringConnector(List<FilterParams> filters);

}
