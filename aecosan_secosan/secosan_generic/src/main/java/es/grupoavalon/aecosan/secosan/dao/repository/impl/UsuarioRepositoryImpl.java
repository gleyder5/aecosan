package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.UsuarioRepository;

@Component
public class UsuarioRepositoryImpl extends AbstractCrudRespositoryImpl<UsuarioEntity, Long> implements UsuarioRepository {

	@Override
	protected Class<UsuarioEntity> getClassType() {

		return UsuarioEntity.class;
	}

	@Override
	public List<UsuarioEntity> findByIdentificationNumber(String identificationNumber) {
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("identificationNumber", identificationNumber);
		return this.findByNamedQuery(NamedQueriesLibrary.GET_USUARIO_BY_IDENTIFICATORNUMBER, params);
	}

}
