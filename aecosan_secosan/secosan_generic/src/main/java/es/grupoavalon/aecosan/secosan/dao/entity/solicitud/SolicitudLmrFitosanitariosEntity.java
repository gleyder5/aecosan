package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_SOLICITUD_LMR_FITOSANITARIOS)
public class SolicitudLmrFitosanitariosEntity extends SolicitudEntity {

	@Column(name = "SUSTANCIA_ACTIVA")
	private String activeSubstance;

	@Column(name = "PAIS_ORIGEN")
	private String originCountry;

	@Column(name = "ALIMENTOS")
	private String foods;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PAGO_SOLICITUD_ID")
	private PagoSolicitudEntity solicitudePayment;

	public String getActiveSubstance() {
		return activeSubstance;
	}

	public void setActiveSubstance(String activeSubstance) {
		this.activeSubstance = activeSubstance;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getFoods() {
		return foods;
	}

	public void setFoods(String foods) {
		this.foods = foods;
	}

	public PagoSolicitudEntity getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudEntity solicitudePayment) {
		if (solicitudePayment != null) {
			solicitudePayment.setIdentificadorPeticion(this.getIdentificadorPeticion());
		}
		this.solicitudePayment = solicitudePayment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((activeSubstance == null) ? 0 : activeSubstance.hashCode());
		result = prime * result + ((foods == null) ? 0 : foods.hashCode());
		result = prime * result + ((originCountry == null) ? 0 : originCountry.hashCode());
		result = prime * result + ((solicitudePayment == null) ? 0 : solicitudePayment.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitudLmrFitosanitariosEntity other = (SolicitudLmrFitosanitariosEntity) obj;
		if (activeSubstance == null) {
			if (other.activeSubstance != null) {
				return false;
			}
		} else if (!activeSubstance.equals(other.activeSubstance)) {
			return false;
		}
		if (foods == null) {
			if (other.foods != null) {
				return false;
			}
		} else if (!foods.equals(other.foods)) {
			return false;
		}
		if (originCountry == null) {
			if (other.originCountry != null) {
				return false;
			}
		} else if (!originCountry.equals(other.originCountry)) {
			return false;
		}
		if (solicitudePayment == null) {
			if (other.solicitudePayment != null) {
				return false;
			}
		} else if (!solicitudePayment.getId().equals(other.solicitudePayment.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SolicitudLmrFitosanitariosEntity [activeSubstance=" + activeSubstance + ", originCountry=" + originCountry + ", foods=" + foods + ", solicitudePayment="
				+ ((solicitudePayment == null) ? "" : solicitudePayment.getId()) + "]";
	}

}
