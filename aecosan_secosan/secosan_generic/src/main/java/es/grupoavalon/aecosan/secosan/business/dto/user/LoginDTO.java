package es.grupoavalon.aecosan.secosan.business.dto.user;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LoginDTO {

	@JsonProperty("login")
	@XmlElement(name = "login")
	private String login;
	@JsonProperty("password")
	@XmlElement(name = "password")
	private String pw;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public String getPwDecrypted() {
		String decrypted = "";
		if (StringUtils.isNotEmpty(this.pw)) {
			decrypted = new String(Base64.decodeBase64(pw));
		}
		return decrypted;
	}

}
