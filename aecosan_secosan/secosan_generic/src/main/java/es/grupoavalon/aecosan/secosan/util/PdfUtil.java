package es.grupoavalon.aecosan.secosan.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import net.sf.jasperreports.engine.JRException;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;

import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.BarcodeEAN;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.report.ReportUtil;
import es.grupoavalon.aecosan.secosan.util.report.Reportable;

public class PdfUtil {
	
	private static final String IMAGES_PATH_GENERIC_PDF;
	static {
		IMAGES_PATH_GENERIC_PDF = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_IMAGES_PATH);
	}

	public static byte[] generatePdfGenericReportFromListOfBean(List<Reportable> listOfBeanReport, String reportToSign, Logger logger) throws JRException {
		String imagesPath = IMAGES_PATH_GENERIC_PDF;

		logger.debug("generatePdfGenericReportFromListOfBean || reportToSign:" + reportToSign);

		ReportUtil reportUtil = new ReportUtil(reportToSign);
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("imagesPath", imagesPath);
		return reportUtil.generatePdfFromCollectionBean(listOfBeanReport, parameters);
	}
	
	public static String getPdfB64ToDTO(byte[] pdfReport) {
		Base64TobytesArrayTransformer base = new Base64TobytesArrayTransformer();
		return (String) base.transform(pdfReport);
	}
	
	public static String generateBarCodeImage(String code) throws IOException {

		Barcode128 code128 = new Barcode128();
		code128.setCodeType(BarcodeEAN.CODE128);
		code128.setCode(code);
		java.awt.Image imageEAN = code128.createAwtImage(Color.BLACK, Color.WHITE);

		BufferedImage bImage = new BufferedImage(imageEAN.getWidth(null), imageEAN.getHeight(null), BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bImage.createGraphics();
		g.drawImage(imageEAN, 0, 0, null);
		g.dispose();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "png", baos);
		return Base64.encodeBase64String(baos.toByteArray());
	}
}
