package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.MunicipioEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.AbstractTaggedCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericComplexCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericTaggedCatalog;

@Component("Municipio")
public class MunicipioRepositoryImpl extends AbstractTaggedCatalogRepository<MunicipioEntity> implements GenericComplexCatalogRepository<MunicipioEntity>, GenericTaggedCatalog<MunicipioEntity> {

	@Override
	protected Class<MunicipioEntity> getClassType() {
		return MunicipioEntity.class;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MunicipioEntity> callMethod(Method method) {
		List<MunicipioEntity> list = new ArrayList<MunicipioEntity>();

		try {
			list = (List<MunicipioEntity>) method.invoke(this);
		} catch (Exception e) {
			throw new DaoException(DaoException.TAGGED_CATALOG_INVOKE_ERROR + e.getMessage(), e);
		}

		return list;
	}

}
