package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.SolicitudLogoService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudLogoDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.SolicitudLogoDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;

@Service
public class SolicitudLogoServiceImpl extends AbstractSolicitudService implements SolicitudLogoService {

	@Autowired
	private SolicitudLogoDAO solicitudeLogoDAO;
	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO solicitudeLogoDTO) {
		try {
			solicitudPrepareNullDTO(solicitudeLogoDTO);
			addLogo(user, (SolicitudLogoDTO) solicitudeLogoDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + solicitudeLogoDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO solicitudeLogoDTO) {
		try {
			solicitudPrepareNullDTO(solicitudeLogoDTO);
			updateSolicitudLogo(user, (SolicitudLogoDTO) solicitudeLogoDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update| ERROR: " + solicitudeLogoDTO);
		}
	}

	private void addLogo(String user, SolicitudLogoDTO solicitudLogoDTO) {
		SolicitudLogoEntity solicitudeLogoEntity = generateSolicitudLogoEntityFromDTO(solicitudLogoDTO);
		setUserCreatorByUserId(user, solicitudeLogoEntity);
		prepareAddSolicitudEntity(solicitudeLogoEntity, solicitudLogoDTO);
		SolicitudLogoEntity solicitudLogoEntityNew = solicitudeLogoDAO.addSolicitudLogo(solicitudeLogoEntity);
		if(solicitudLogoDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudLogoEntityNew );
		}
	}

	private void updateSolicitudLogo(String user, SolicitudLogoDTO solicitudLogoDTO) {
		SolicitudLogoEntity solicitudLogoEntity = transformDTOToEntity(solicitudLogoDTO, SolicitudLogoEntity.class);
		setUserCreatorByUserId(user, solicitudLogoEntity);
		prepareUpdateSolicitudEntity(solicitudLogoEntity, solicitudLogoDTO);
		if(solicitudLogoDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(solicitudLogoEntity);
		}
		solicitudeLogoDAO.updateSolicitudLogo(solicitudLogoEntity);
	}

	private SolicitudLogoEntity generateSolicitudLogoEntityFromDTO(SolicitudLogoDTO solicitudLogoDTO) {
		prepareSolicitudDTO(solicitudLogoDTO);
		return transformDTOToEntity(solicitudLogoDTO, SolicitudLogoEntity.class);
	}

}
