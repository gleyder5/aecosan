package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_CESE_COMERCIALIZACION)
public class CeseComercializacionEntity extends SolicitudEntity {

	@Column(name = "NOMBRE_COMERCIAL_PRODUCTO")
	private String nombreComercialProducto;

	@Column(name = "OBSERVACIONES")
	private String observaciones;

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nombreComercialProducto == null) ? 0 : nombreComercialProducto.hashCode());
		result = prime * result + ((observaciones == null) ? 0 : observaciones.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CeseComercializacionEntity other = (CeseComercializacionEntity) obj;
		if (nombreComercialProducto == null) {
			if (other.nombreComercialProducto != null)
				return false;
		} else if (!nombreComercialProducto.equals(other.nombreComercialProducto))
			return false;
		if (observaciones == null) {
			if (other.observaciones != null)
				return false;
		} else if (!observaciones.equals(other.observaciones))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CeseComercializacionEntity [nombreComercialProducto=" + nombreComercialProducto + ", observaciones=" + observaciones + "]";
	}

}
