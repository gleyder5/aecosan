/**
 * 
 */
package es.grupoavalon.aecosan.secosan.dao.repository;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

/**
 * @author ottoabreu
 *
 */
public interface CrudRepository<T, PK extends Serializable> {

	T save(T o);

	T findOne(PK id);

	List<T> findAll();

	void update(T o);

	void delete(PK pk);

	Long getSequenceNextVal(String namedQuery);

	Query getNamedQuery(String namedQuery);

	T findOneByNamedQuery(String queryName);

	T findOneByNamedQuery(String queryName, Map<String, Object> params);

	T findOneByQuery(String query);

	T findOneByQuery(String query, Map<String, Object> params);

	List<T> findByNamedQuery(String queryName, Map<String, Object> params);

	List<T> findByNamedQuery(String queryName);

	List<T> findByQuery(String queryName, Map<String, Object> params);

	List<T> findByQuery(String queryName);

	int getTotalRecords();
	
	int getTotalRecords(String whereClause, Map<String, Object> params);

	List<T> findAllPaginatedAndSortedDESC(int start, int end, String fieldName);

	List<T> findAllPaginatedAndSortedASC(int start, int end, String fieldName);

	List<T> findAllPaginatedAndSortedDESC(String whereClause, int start,
			int end, String fieldName, Map<String, Object> params);

	List<T> findAllPaginatedAndSortedASC(String whereClause, int start,
			int end, String fieldName, Map<String, Object> params);
	
}
