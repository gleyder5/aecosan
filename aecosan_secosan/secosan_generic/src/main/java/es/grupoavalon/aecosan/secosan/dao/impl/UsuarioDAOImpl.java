package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.entity.*;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Repository
public class UsuarioDAOImpl extends GenericDao implements UsuarioDAO {

	private static final String ERROR = " ERROR: ";

	@Autowired
	private RegistroActividadRepository registroActividadRepository;

	@Autowired
	private UsuarioExtracomunitarioRepository usuarioExtracomunitarioRepository;

	@Autowired
	private UsuarioRolRepository usuarioRolRepository;

	@Autowired
	private TipoUsuarioRepository tipoUsuariolRepository;

	@Autowired
	private UsuarioCreadorRepository usuarioCreadorRepository;

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UsuarioInternoRepository usuarioInternoRepository;

	@Override
	public UsuarioRolEntity findRol(long pk) {

		UsuarioRolEntity usuarioRol = null;

		try {
			usuarioRol = usuarioRolRepository.findUsuarioRol(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findRol | " + pk + ERROR + exception.getMessage());
		}

		return usuarioRol;
	}

	@Override
	public TipoUsuarioEntity findTipoUsuario(long pk) {

		TipoUsuarioEntity tipoUsuario = null;

		try {
			tipoUsuario = tipoUsuariolRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findTipoUsuario | " + pk);
		}

		return tipoUsuario;
	}

	@Override
	public UsuarioCreadorEntity findCreatorById(long pk) {
		UsuarioCreadorEntity usuarioCreadorEntity = null;
		try {
			usuarioCreadorEntity = usuarioCreadorRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findById | " + pk + ERROR + exception.getMessage());
		}

		return usuarioCreadorEntity;
	}

	@Override
	public UsuarioCreadorEntity findCreatorUsersByIdentificationDocument(String ididentificationNumber,boolean isClave) {
		UsuarioCreadorEntity usuarioCreadorEntity = null;
		try {
			List<UsuarioEntity> users = usuarioRepository.findByIdentificationNumber(ididentificationNumber);
			if (CollectionUtils.isNotEmpty(users)) {
				if(isClave) {
					usuarioCreadorEntity = (UsuarioCreadorEntity) findUserInList(users, UsuarioClaveEntity.class, ididentificationNumber);
				}else{
					usuarioCreadorEntity = (UsuarioCreadorEntity) findUserInList(users, UsuarioCreadorEntity.class, ididentificationNumber);
				}
				}
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "identificationNumber | " + ididentificationNumber + ERROR + exception.getMessage());
		}

		return usuarioCreadorEntity;
	}

	@Override
	public UsuarioInternoEntity findInternalUsersByIdentificationDocument(String identificationNumber,boolean isClave) {
		UsuarioInternoEntity usuarioInternoEntity = null;
		try {
			List<UsuarioEntity> users = usuarioRepository.findByIdentificationNumber(identificationNumber);
			if (CollectionUtils.isNotEmpty(users)) {
				if(isClave) {
					usuarioInternoEntity = (UsuarioInternoEntity) findUserInList(users, UsuarioClaveEntity.class, identificationNumber);
				}else{
					usuarioInternoEntity = (UsuarioInternoEntity) findUserInList(users, UsuarioInternoEntity.class, identificationNumber);
				}
				logger.info("User found by identification number "+usuarioInternoEntity);
			}
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "identificationNumber | " + identificationNumber + ERROR + exception.getMessage());
		}

		return usuarioInternoEntity;

	}

	private static UsuarioEntity findUserInList(List<UsuarioEntity> users, Class<? extends UsuarioEntity> objectClass, String idDocument) {
		UsuarioEntity foundUser = null;
		for (UsuarioEntity user : users) {

			if (user.getClass().equals(objectClass) && StringUtils.equalsIgnoreCase(idDocument, user.getIdentificationNumber())) {
				foundUser = user;
				break;
			}
		}
		return foundUser;
	}

	@Override
	public UsuarioEntity addUser(UsuarioEntity user) {
		try {
			user = this.usuarioRepository.save(user);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addUser | " + user + ERROR + exception.getMessage());
		}
		return user;
	}

	@Override
	public LoginUser findUserByLoginExternal(String userLogin) {
		UsuarioExtraComunitarioEntity user = null;
		try {
			user = this.usuarioExtracomunitarioRepository.findByUserLogin(userLogin);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "findUserByLoginExternal | " + userLogin + ERROR + e.getMessage());
		}
		return user;
	}

	@Override
	public LoginUser findUserByLoginInternal(String username) {
		UsuarioInternoEntity user = null;
		try {
			user = this.usuarioInternoRepository.findByUserName(username);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "findUserByLoginEternal | " + username + ERROR + e.getMessage());
		}
		return user;
	}

	@Override
	public LoginUser findUserByLogin(String username) {
		LoginUser user = this.findUserByLoginInternal(username);
		if (user == null) {
			user = this.findUserByLoginExternal(username);
		}
		return user;
	}

	@Override
	public void updateUser(UsuarioEntity user) {
		try {
			this.usuarioRepository.update(user);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "updateUser | " + user + ERROR + e.getMessage());
		}
	}

	@Override
	public List<UsuarioInternoEntity> getInternalUserList(PaginationParams paginationParams) {
		List<UsuarioInternoEntity> internalUserList = null;
		try {
			addNotDeletedCondition(paginationParams);
			internalUserList = this.usuarioInternoRepository.findAllByPaginationParams(paginationParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getInternalUserList ERROR " + e.getMessage());
		}
		return internalUserList;
	}

	private static void addNotDeletedCondition(PaginationParams paginationParams) {
		List<FilterParams> filterParam = paginationParams.getFilters();
		FilterParams notDeleted = new FilterParams();
		notDeleted.setFilterName(SecosanConstants.FIELD_DELETED);
		notDeleted.setFilterValue(Boolean.FALSE.toString());
		filterParam.add(notDeleted);
	}

	@Override
	public void deleteUserInternalById(Long pk) {
		try {
			UsuarioInternoEntity userToDelete = this.usuarioInternoRepository.findOne(pk);
			Map<String, Object> queryParams = new HashMap<String, Object>();

			queryParams.put("userid", Long.valueOf(pk));
			List<RegistroActividadEntity> registroActividadEntities = this.registroActividadRepository.findByNamedQuery(NamedQueriesLibrary.GET_REGISTRO_ACTIVIDADES_BY_USER,queryParams);
			for (RegistroActividadEntity registroActividadEntity: registroActividadEntities) {
				registroActividadEntity.setUser(null);
				this.registroActividadRepository.update(registroActividadEntity);
			}
			this.usuarioRepository.delete(userToDelete.getId());
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "deleteUserInternalById | id: " + pk + " | ERROR  " + e.getMessage());
		}
	}

	@Override
	public void updateUserInternalById(UsuarioInternoEntity userInternalToUpdate) {
		try {
			usuarioInternoRepository.update(userInternalToUpdate);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "updateUserInternalById | userInternalToUpdate: " + userInternalToUpdate + " | ERROR  " + e.getMessage());
		}

	}

	@Override
	public UsuarioInternoEntity findUserInternalById(long id) {
		UsuarioInternoEntity internalUser = null;
		try {
			internalUser = usuarioInternoRepository.findOne(id);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "findUserInternalById | id: " + id + " | ERROR " + e.getMessage());
		}
		return internalUser;
	}

	@Override
	public int getTotalListRecords(List<FilterParams> filters) {
		int total = 0;
		try {
			total = usuarioInternoRepository.getTotalListRecords(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalListRecords | " + filters + ERROR + e.getMessage());
		}
		return total;
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {
		int totalFiltered = 0;
		try {
			totalFiltered = usuarioInternoRepository.getFilteredListRecords(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getFilteredListRecords |" + filters + "|" + ERROR + e.getMessage());
		}
		return totalFiltered;
	}

	@Override
	public UsuarioEntity getUserById(long userId) {
		UsuarioEntity user = null;

		try {
			user = this.usuarioRepository.findOne(userId);
		} catch (Exception e) {
			this.handleException(e, DaoException.CRUD_OPERATION_ERROR + " getUserById |" + userId + "|" + e.getMessage());
		}

		return user;
	}

}
