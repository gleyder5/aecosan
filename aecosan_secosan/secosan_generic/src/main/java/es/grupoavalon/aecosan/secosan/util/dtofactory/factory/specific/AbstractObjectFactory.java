package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanInstantiationException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.FactoryFillingException;

/**
 * 
 * Contains the basic Factory methods, such as those who executes getters and
 * setters<BR>
 * The concrete class should implements theirs own methods to construct the
 * objects
 * 
 * @author ottoabreu
 * 
 */
public abstract class AbstractObjectFactory<T> {

	protected static final String GETTER_PREFIX = "get";
	protected static final String GETTER_BOOLEAN_PREFIX = "is";
	protected static final String FIELD_MSG = "Field";
	protected static final String SETTER_PREFIX = "set";
	private Class<?> toInstatiate;
	// LOGGER
	private static final Logger logger = LoggerFactory
			.getLogger(AbstractObjectFactory.class);

	/**
	 * Constructor
	 * 
	 * @param toInstatiate
	 */
	protected AbstractObjectFactory(Class<?> toInstatiate) {
		super();
		this.toInstatiate = toInstatiate;
	}

	/**
	 * Puts the value obtained from the given object into the new object
	 * instance
	 * 
	 * @param fieldName
	 *            String with the attribute name
	 * @param value
	 *            actual value obtained from the entity
	 * @param newInstanceo
	 *            T actual new instance object
	 * @param setClass
	 *            Class<?> used to specify the param type, can be null
	 */
	protected void executeSetMethod(String fieldName, Object value,
			T newInstanceo, Class<?> setClass) {
	
		try {

			this.executeSetMethodInNewInstance(fieldName, value, newInstanceo,
					setClass);

		} catch (SecurityException e) {
			logger.error(
					FactoryFillingException.EXECUTE_SETMETHOD_SECURITY_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_SETMETHOD_SECURITY_ERROR, e);
		} catch (NoSuchMethodException e) {
			logger.error(
					FactoryFillingException.EXECUTE_SETMETHOD_NOMETHOD_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_SETMETHOD_NOMETHOD_ERROR, e);
		} catch (IllegalArgumentException e) {
			logger.error(
					FactoryFillingException.EXECUTE_SETMETHOD_ILEGALARGUMENT_ERROR,
					e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_SETMETHOD_ILEGALARGUMENT_ERROR,
					e);
		} catch (IllegalAccessException e) {
			logger.error(
					FactoryFillingException.EXECUTE_SETMETHOD_ILEGALACCES_ERROR,
					e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_SETMETHOD_ILEGALACCES_ERROR,
					e);
		} catch (InvocationTargetException e) {
			logger.error(
					FactoryFillingException.EXECUTE_SETMETHOD_IVOTGT_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_SETMETHOD_IVOTGT_ERROR, e);
		}

	}

	private void executeSetMethodInNewInstance(String fieldName, Object value,
			T newInstanceo, Class<?> setClass) throws SecurityException,
			NoSuchMethodException,
			IllegalAccessException, InvocationTargetException {


		executeGenericSetMethod(fieldName, value, newInstanceo, setClass, this.toInstatiate);

	}
	
	protected static void executeGenericSetMethod(String fieldName, Object value,
			Object instance, Class<?> setClass,Class<?> invokeFromClass) throws SecurityException,
			NoSuchMethodException,
			IllegalAccessException, InvocationTargetException {
		
		String setterName = getSetterGetterMethodName(fieldName,
				SETTER_PREFIX);
		logger.debug("method to execute:  {} Value  {}  instance {}", setterName ,  value,instance);
		Class<?> setterParamClass = null;
		if (setClass != null) {
			setterParamClass = setClass;
		} else {
			setterParamClass = value.getClass();
		}
		executeMethod(setterName, setterParamClass, instance,
				invokeFromClass, value);
		
	}

	/**
	 * Executes an getter method from the entity and return the value
	 * 
	 * @param fieldName
	 *            String with the obtained field name from the annotation
	 * @param instanceFrom
	 *            Object with the values to be included in the new instance
	 * @return Object
	 */
	protected Object executeGetMethod(String fieldName, Object instanceFrom) {
		// Obtain the method name

		Object obtainedValue = null;

		try {
			// set the class and the value null because is a getmethod, there is
			// no need for setting the param class or value
			logger.debug("method to execute:  : {} class: {}", instanceFrom, instanceFrom.getClass());
			obtainedValue = this.executeGetMethodInInstance(fieldName,
					instanceFrom, GETTER_PREFIX);
		} catch (NoSuchMethodException e) {
			logger.debug("trying to execute the getter method using IS prefix ");
			obtainedValue = this.executeGetMethodBoolean(fieldName, instanceFrom);

		} catch (Exception e) {
			this.handleInstanciationExceptions(e);
		}
		return obtainedValue;

	}

	/**
	 * calls a get method for booleans (is)
	 * 
	 * @param fieldName
	 * @param instanceFrom
	 * @return object
	 */
	protected Object executeGetMethodBoolean(String fieldName,
			Object instanceFrom) {
		Object obtainedValue = null;
		try {

			obtainedValue = this.executeGetMethodInInstance(fieldName,
					instanceFrom, GETTER_BOOLEAN_PREFIX);

		} catch (Exception e) {
			this.handleInstanciationExceptions(e);
		}

		return obtainedValue;
	}

	/**
	 * Call the get Method using reflexion
	 * 
	 * @param fieldName
	 * @param instanceFrom
	 * @param getterPrefix
	 * @return object
	 * @throws SecurityException
	 * @throws IllegalArgumentException
	 * @throws NoSuchMethodException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	private Object executeGetMethodInInstance(String fieldName,
			Object instanceFrom, String getterPrefix) throws SecurityException,
			NoSuchMethodException,
			IllegalAccessException, InvocationTargetException {
		Object obtainedValue = null;
		String getterName = getSetterGetterMethodName(fieldName,
				getterPrefix);
		obtainedValue = executeMethod(getterName, null, instanceFrom,
				instanceFrom.getClass(), null);
		return obtainedValue;

	}

	private void handleInstanciationExceptions(Exception e) {
		if (e instanceof SecurityException) {
			logger.error(
					FactoryFillingException.EXECUTE_GETMETHOD_SECURITY_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_GETMETHOD_SECURITY_ERROR, e);

		} else if (e instanceof IllegalArgumentException) {
			logger.error(
					FactoryFillingException.EXECUTE_GETMETHOD_ILEGALARGUMENT_ERROR,
					e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_GETMETHOD_ILEGALARGUMENT_ERROR,
					e);
		} else if (e instanceof NoSuchMethodException) {
			logger.error(
					FactoryFillingException.EXECUTE_GETMETHOD_NOMETHOD_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_GETMETHOD_NOMETHOD_ERROR, e);
		} else if (e instanceof IllegalAccessException) {
			logger.error(
					FactoryFillingException.EXECUTE_GETMETHOD_ILEGALACCES_ERROR,
					e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_GETMETHOD_ILEGALACCES_ERROR,
					e);
		} else if (e instanceof InvocationTargetException) {
			logger.error(
					FactoryFillingException.EXECUTE_GETMETHOD_IVOTGT_ERROR, e);
			throw new FactoryFillingException(
					FactoryFillingException.EXECUTE_GETMETHOD_IVOTGT_ERROR, e);
		}
	}

	/**
	 * Method that generates a new instance of an object from the class (uses
	 * {@link Class#newInstance()})
	 * 
	 * @return T
	 *
	 */
	@SuppressWarnings("unchecked")
	protected T instanciateObject() {
		T newInstance = null;

		try {
			
			
			newInstance = (T) this.toInstatiate.newInstance();

		} catch (InstantiationException e) {
			logger.error(BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
			throw new BeanInstantiationException(
					BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
		} catch (IllegalAccessException e) {
			logger.error(BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
			throw new BeanInstantiationException(
					BeanInstantiationException.INSTANTIATE_DTO_ERROR, e);
		}
		return newInstance;
	}


	
	protected static Set<Field> getAllFieldsThroughInherance(Class<?> baseClass){
		Set<Field> allFields = new HashSet<Field>();
		while(baseClass.getSuperclass()!=null){ // we don't want to process Object.class
		    // do something with current's fields
			baseClass = baseClass.getSuperclass();
			Field[] declaredFieldSuper =baseClass.getDeclaredFields();
			allFields.addAll(Arrays.asList(declaredFieldSuper));
		}
		
		logger.debug("parent fields: {}",allFields.size());
		return allFields;
	}
	
	
	protected static Field getDeclaredField(String fieldName,Class<?> objectClass) throws NoSuchFieldException{
		Field declaredField = null;
		try {
			
			declaredField = objectClass.getDeclaredField(fieldName);
			 
		} catch (NoSuchFieldException e) {
			if (logger.isDebugEnabled()) {
				logger.debug(FIELD_MSG +fieldName+" was not found in class:"+objectClass+" trying using inherance");
			}
			Set<Field> allFields = getAllFieldsThroughInherance(objectClass);
			declaredField = findFieldbyName(fieldName, allFields);
			
			if(declaredField == null){
				if(logger.isErrorEnabled()) {
					logger.error(FIELD_MSG + fieldName + " was not found in class:" + objectClass);
				}
				throw e;
			}
			if(logger.isDebugEnabled()) {
				logger.debug(FIELD_MSG + fieldName + "found through inherence");
			}
		}
		return declaredField;
	}
	
	private static Field findFieldbyName(String name,Collection<Field> allFields){
		Field field = null;
		for(Field fieldToAsk: allFields){
			if(fieldToAsk.getName().equals(name)){
				field = fieldToAsk;
				break;
			}
		}
		
		return field;
	}

	/**
	 * Returns the class to instantiate
	 * 
	 * @return
	 */
	protected Class<?> getToInstanciateClass() {
		return this.toInstatiate;
	}

	/**
	 * Executes the given method
	 * 
	 * @param methodName
	 * @param paramClass
	 * @param invokeFrom
	 * @param invokeFromClass
	 * @param value
	 * @return
	 * @throws SecurityException
	 *             read throw conditions from
	 *             {@link Class#getMethod(java.lang.String, java.lang.Class...)}
	 * @throws NoSuchMethodException
	 *             read throw conditions from
	 *             {@link Class#getMethod(java.lang.String, java.lang.Class...)}
	 * @throws IllegalArgumentException
	 *             read throw conditions from
	 *             {@link Method#invoke(Object, Object...)}
	 * @throws IllegalAccessException
	 *             read throw conditions from
	 *             {@link Method#invoke(Object, Object...)}
	 * @throws InvocationTargetException
	 *             read throw conditions from
	 *             {@link Method#invoke(Object, Object...)}
	 */
	private static Object executeMethod(String methodName, Class<?> paramClass,
			Object invokeFrom, Class<?> invokeFromClass, Object value)
			throws SecurityException, NoSuchMethodException,
			IllegalAccessException,
			InvocationTargetException {

		Object toReturn = null;
		Method method = null;

		if (value != null || paramClass != null) {
			method = invokeFromClass.getMethod(methodName, paramClass);
			toReturn = method.invoke(invokeFrom, value);

		} else {
			method = invokeFromClass.getMethod(methodName);
			toReturn = method.invoke(invokeFrom);
		}
		return toReturn;

	}

	/**
	 * Generates the setter name from a field name
	 * 
	 * @param fieldName
	 *            String
	 * @param prefix
	 *            String should be get or set
	 * @return String
	 */
	protected static String getSetterGetterMethodName(String fieldName, String prefix) {

		StringBuilder setterName = new StringBuilder(prefix);
		// the first letter have to be uppercase
		setterName.append(fieldName.substring(0, 1).toUpperCase());

		// append the rest of the name
		setterName.append(fieldName.substring(1, fieldName.length()));
		logger.debug("method name: {} ", setterName);
		return setterName.toString();
	}
	
	/**
	 * Returns the class from the given instance
	 * @param instance
	 * @return
	 */
	protected static Class<?> getClassFromObject(Object instance){
		return instance.getClass();
	}
	@SuppressWarnings("rawtypes")
	protected static Collection<Object> transformCollectionType(
Class<? extends Collection> collectionType) {
	
		Collection<Object> instanciatedCollection = null;
		if(collectionType.equals(Set.class)){
			instanciatedCollection = new HashSet<Object>();
		}else if (collectionType.equals(Queue.class)){
			instanciatedCollection = new LinkedList<Object>();
		}else if(collectionType == null || collectionType.equals(List.class)){
			instanciatedCollection = new ArrayList<Object>();
		} else {
			instanciatedCollection = instanciateSpecificCollectionClass(collectionType);
		}
		
		return instanciatedCollection;
	}

	@SuppressWarnings("unchecked")
	private static Collection<Object> instanciateSpecificCollectionClass(Class<? extends Collection> collectionType) {
		Constructor<?> collectionConstructor;
		Collection<Object> instanciatedCollection = null;
		try {
			collectionConstructor = collectionType.getConstructor();
			instanciatedCollection = (Collection<Object>) collectionConstructor.newInstance();
		} catch (Exception e) {
			throw new BeanInstantiationException(BeanInstantiationException.INSTANTIATE_COLLECTION_ERROR, e);
		}
		return instanciatedCollection;
	}
	
}
