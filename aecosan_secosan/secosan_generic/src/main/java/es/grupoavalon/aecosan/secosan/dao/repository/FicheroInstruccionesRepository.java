package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesEntity;

public interface FicheroInstruccionesRepository extends CrudRepository<FicheroInstruccionesEntity, Long> {

	public FicheroInstruccionesEntity findBySolicitudeType(Long idFormulario);

	public List<FicheroInstruccionesEntity> findAllByArea(Long area);
}
