package es.grupoavalon.aecosan.secosan.util.report;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;

import com.itextpdf.text.FontFactory;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import es.grupoavalon.aecosan.secosan.business.enums.EnumPrototype;
import es.grupoavalon.aecosan.secosan.business.enums.PlaceholdersFichaTecnicaIncl;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharSet;
import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import static com.itextpdf.tool.xml.css.CSS.Property.FONT;

public class ReportUtil {

	private static final Logger logger = LoggerFactory.getLogger(ReportUtil.class);

	private JasperReport jasperReport;
	private JRBeanCollectionDataSource coleccion;
	private JasperPrint jasperPrint;


	public ReportUtil(String compileReport) throws JRException {
		try {
			jasperReport = JasperCompileManager.compileReport(compileReport);
			logger.debug("ReportUtil || compileReport: " + compileReport);
		} catch (Exception e) {
			logger.error("Error ReportUtil || " + compileReport + " ERROR: " + e.getMessage());
			String reportName = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_GENERAL_REPORT_LOCAL_NAME);
			jasperReport = JasperCompileManager.compileReport(this.getClass().getClassLoader().getResourceAsStream(reportName));
		}
	}

	public byte[] generatePdfFromCollectionBean(List<Reportable> collectionBean, Map<String, Object> parameters) throws JRException {
		setBeanCollectionDataSource(collectionBean);
		fillReport(parameters);
		return exportReportToPdfFile();
	}

	private void setBeanCollectionDataSource(List<Reportable> reportableList) {
		coleccion = new JRBeanCollectionDataSource(reportableList);
	}

	private void fillReport(Map<String, Object> parameters) throws JRException {
		jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, coleccion);
	}

	private byte[] exportReportToPdfFile() throws JRException {
		return JasperExportManager.exportReportToPdf(jasperPrint);
	}


	public static String getPath(final Class<?> cls, final String resource) throws URISyntaxException {
		final ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return  cl.getResource(resource).getPath();
	}

	public static String generatePdfFromHTML(File input,String outputFileName,String content){
		Document document = new Document();
		logger.info("Generando pdf From Html fichero template {}  fichero salida : {} ",input.getName(),outputFileName);
		final String font  = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + SecosanConstants.FONT;
		String contentBase64 = null;
		FileInputStream fileInputStream = null;
		try {
			File output = File.createTempFile(outputFileName.substring(0,outputFileName.indexOf(".")),".pdf");
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(output.getPath()) );
			document.open();

			XMLWorkerFontProvider fontImp = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
			for (String s : fontImp.getRegisteredFamilies()) {
			    	logger.info(s);
			}
			// fontImp.setUseUnicode(true);
			fontImp.register(font,"Arial Unicode Ms");
			FontFactory.setFontImp(fontImp);

			fileInputStream= new FileInputStream(input);
			XMLWorkerHelper.getInstance().parseXHtml(writer, document,
					fileInputStream, null, Charset.forName("UTF-8"), fontImp);


//			XMLWorkerHelper.getInstance().parseXHtml(writer, document,
//					new FileInputStream(input) );
			document.close();
			contentBase64 = Base64TobytesArrayTransformer.transformFileToBase64(output);
			fileInputStream.close();
		}catch (FileNotFoundException fex){
			logger.error("No se encontró el fichero {} mensaje excepcion : {}",outputFileName,fex);
			fex.printStackTrace();
		}catch (Exception ex) {
			logger.error("Error al generar PDF from Html mensaje excepcion : {}",outputFileName,ex);
		}
		logger.info("Generado correctamente base64  para {} ",input.getName());
		return contentBase64;
	}




	public static <T extends EnumPrototype> String replaceValues(final List<String> values, T[] keys,
																 String content) {
		if(values.size()>0) {
			for (int i = 0; i < values.size(); ++i) {
				final String name = keys[i].getName();
				//if(values.get(i)!=null) {
					content = content.replace(name, values.get(i));
				//}
			}
		}
		return content;
	}

	public static String replaceValuesMap(HashMap<String,String> datosReemplazo,
																 String content) {
		logger.info("Reemplazando valores de plantilla  con formulario " );
		if(datosReemplazo.keySet().size()>0) {
			for(String key: datosReemplazo.keySet()){
				logger.info("Reemplazando key {}  de plantilla  con valor {} del formulario ",key, datosReemplazo.get(key) );
				if(datosReemplazo.get(key)==null){
					content = content.replace(key, " ");
				}else{
					content = content.replace(key, datosReemplazo.get(key));
				}
			}
		}
		return content;
	}

	public static <T extends EnumPrototype> String replaceValues(final HashMap<PlaceholdersFichaTecnicaIncl,String> keyValuesToReplace,String content) {
		logger.info("Reemplazando valores de  plantilla con formulario " );

		for(PlaceholdersFichaTecnicaIncl keyPlaceholder :  keyValuesToReplace.keySet()){
			if(keyPlaceholder.getName()!=null && keyValuesToReplace.get(keyPlaceholder)!=null) {
				content = content.replace(keyPlaceholder.getName(), keyValuesToReplace.get(keyPlaceholder));
			}
		}
		logger.info("Finalizado el reemplazo de  placholders con valores" );
		return content;
	}
}
