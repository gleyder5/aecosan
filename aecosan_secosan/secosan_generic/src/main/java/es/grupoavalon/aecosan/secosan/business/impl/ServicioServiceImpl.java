package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.ServicioService;
import es.grupoavalon.aecosan.secosan.business.dto.ServicioDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.ServicioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;

@Service
public class ServicioServiceImpl extends AbstractManager implements ServicioService {

	@Autowired
	private ServicioDAO servicioDAO;

	@Override
	public List<ServicioDTO> findServicesByPayTypeId(String payTypeId) {
		List<ServicioDTO> listServices = null;

		try {
			Long idPayType = Long.valueOf(payTypeId);
			List<ServicioEntity> listEntities = servicioDAO.findServiceByPayTypeId(idPayType);
			listServices = transformEntityListToDTOList(listEntities, ServicioEntity.class, ServicioDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " findServicesByPayTypeId | payTypeId: " + payTypeId);
		}
		return listServices;
	}

}
