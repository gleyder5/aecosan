package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.TipoEnvioEntity;

public interface TipoEnvioRepository extends CrudRepository<TipoEnvioEntity, Long> {

}
