package es.grupoavalon.aecosan.secosan.dao.impl;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.dao.DocumentacionDAO;
import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.TipoDocumentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.BecasRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.HistorialSolicitudRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class HistorialSolicitudDAOImpl extends GenericDao implements HistorialSolicitudDAO {

    @Autowired
    private HistorialSolicitudRepository historialSolicitudRepository;
    @Autowired
    EstadoSolicitudDAO estadoSolicitudDao;
    @Autowired
    DocumentacionDAO documentacionDAO;

    @Autowired
    BecasRepository becasRepository;
    @Autowired
    TipoDocumentosDAO tipoDocumentosDAO;


    @Override
    public List<HistorialSolicitudEntity> getHistorialList(PaginationParams paginationParams) {

        List<HistorialSolicitudEntity> historialSolicitudEntityList = new ArrayList<HistorialSolicitudEntity>();
        try {
            logger.debug("Paginacion: de " + paginationParams.getStart() + " a " + paginationParams.getLength());
            historialSolicitudEntityList = historialSolicitudRepository.getPaginatedList(paginationParams);
        } catch (Exception exception) {
            handleException(exception, DaoException.CRUD_OPERATION_ERROR + "getHistorialSolicitud | Historial: " );
        }
        return  historialSolicitudEntityList;
    }

    @Override
    public HistorialSolicitudEntity getBySolicitudAndStatus(Integer solicitudId,EstadoSolicitudEntity status) {
        HistorialSolicitudEntity historialSolicitudEntity = null;
        List<HistorialSolicitudEntity> historialSolicitudEntityList = null;
        try {
            Map<String, Object> queryParams = new HashMap<String, Object>();
            queryParams.put("solicitudId", Long.valueOf(solicitudId));
            queryParams.put("statusId", Long.valueOf(status.getId()));
            historialSolicitudEntityList = historialSolicitudRepository.findByNamedQuery(NamedQueriesLibrary.GET_HISTORIAL_BY_SOLICITUD_ID_AND_STATUS_ID, queryParams);
            if(historialSolicitudEntityList.size()>=1){
                historialSolicitudEntity = historialSolicitudEntityList.get(0);
            }
        } catch (Exception exception) {

            handleException(exception, DaoException.CRUD_OPERATION_ERROR + "getHistorialSolicitud | Historial: " );
        }
        return  historialSolicitudEntity;
    }

    @Override
    public HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity) {
        HistorialSolicitudEntity historial  = null ;
        historial = addHistorial(solicitudEntity,null,null);
        return historial;
    }

    @Override
    public HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity, Long status) {
        return addHistorial(solicitudEntity,estadoSolicitudDao.findEstadoSolicitud(status),null);
    }

    @Override
    public HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity, EstadoSolicitudEntity status,ChangeStatusDTO changeStatusDTO) {
        return addHistorial(solicitudEntity,status,changeStatusDTO);

    }

    @Override
    public int getTotal(List<FilterParams> filterParams) {
        int total = 0;
        try {
            // Long userId = Long.parseLong(user);
            Map<String,Object> queryParams  = new HashMap<String, Object>();
            for (FilterParams fp :filterParams) {
                if(fp.getFilterName().equals("solicitudId")){
                    queryParams.put("solicitudId", Long.valueOf(fp.getFilterValue()));
                }
            }
            total = historialSolicitudRepository.getTotalRecords("WHERE solicitud.id=:solicitudId",queryParams);
        } catch (Exception e) {
            handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalListRecords | " + " " + e.getMessage());
        }
        return total;
    }

    private HistorialSolicitudEntity addHistorial(SolicitudEntity solicitudEntity, EstadoSolicitudEntity estadoSolicitudEntity, ChangeStatusDTO changeStatusDTO){
        HistorialSolicitudEntity historial  = null ;
        try {
            historial =  new HistorialSolicitudEntity();
            historial.setSolicitud(solicitudEntity);

            if(changeStatusDTO!=null && changeStatusDTO.getFileB64()!=null && StringUtils.isNotBlank(changeStatusDTO.getFileB64())) {
                DocumentacionEntity documentacionEntity = new DocumentacionEntity();
                documentacionEntity.setSendAsAttachment(false);
                documentacionEntity.setDocumentName(changeStatusDTO.getFileName());
                documentacionEntity.setDocument(changeStatusDTO.getFileBytes());
                documentacionEntity.setSolicitud(solicitudEntity);
                TipoDocumentacionEntity tipoDocumentacionEntity = tipoDocumentosDAO.findTipoDocumentacion(41L);
                documentacionEntity.setDocType(tipoDocumentacionEntity);
                documentacionDAO.addDocumentacionToSolicitude(solicitudEntity, documentacionEntity);
                historial.setDocumento(documentacionEntity);
            }
            historial.setFecha(new Date());
            if(estadoSolicitudEntity==null) {
                historial.setStatus(estadoSolicitudDao.findEstadoSolicitud(Long.valueOf(2))); //Enviada por defecto, al ser guardada
                historial.setDescripcion("Solicitud Generada.");
            }else if(changeStatusDTO!=null) {
                if(solicitudEntity.getArea().getId().equals(8L)){
                    historial.setStatus(solicitudEntity.getStatus());
                    historial.setDescripcion(changeStatusDTO.getStatusText());
                    if(changeStatusDTO.getTypeSubmit()!=null){
                        historial.setStatus(estadoSolicitudDao.findEstadoSolicitud(17L));
                        if(changeStatusDTO.getTypeSubmit().equals("correct")) {
                            solicitudEntity.setStatus(estadoSolicitudDao.findEstadoSolicitud(18L));
                        }else if(changeStatusDTO.getTypeSubmit().equals("accept")) {
                            solicitudEntity.setStatus(estadoSolicitudDao.findEstadoSolicitud(19L));
                        }
                    }else{
                        if(solicitudEntity.getStatus().getId().equals(19L)) {
                                solicitudEntity.setStatus(estadoSolicitudDao.findEstadoSolicitud(20L));
                        }else if(solicitudEntity.getStatus().getId().equals(20L)) {
                            solicitudEntity.setStatus(estadoSolicitudDao.findEstadoSolicitud(21L));
                        }else if(solicitudEntity.getStatus().getId().equals(21L)) {
                            solicitudEntity.setStatus(estadoSolicitudDao.findEstadoSolicitud(22L));
                        }
                    }
                    BecasEntity beca = becasRepository.findOne(solicitudEntity.getId());
                    beca.setStatus(solicitudEntity.getStatus());
                    becasRepository.save(beca);

                }else{
                    historial.setStatus(solicitudEntity.getStatus());
                    historial.setDescripcion(changeStatusDTO.getStatusText());
                }
            }
            historial = historialSolicitudRepository.save(historial);
        } catch (Exception exception) {
            handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addHistorialSolicitud | Historial para solicitud: " + solicitudEntity);
        }
            return  historial;
    }

}
