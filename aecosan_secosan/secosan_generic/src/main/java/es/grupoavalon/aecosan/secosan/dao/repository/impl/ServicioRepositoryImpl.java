package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.ServicioRepository;

@Component("Servicios")
public class ServicioRepositoryImpl extends AbstractCrudRespositoryImpl<ServicioEntity, Long> implements ServicioRepository {

	@Override
	protected Class<ServicioEntity> getClassType() {
		return ServicioEntity.class;
	}

}
