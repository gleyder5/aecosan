package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("TipoFormulario")
public class TipoSolicitudCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<TipoSolicitudEntity, Long> implements GenericCatalogRepository<TipoSolicitudEntity> {

	@Override
	public List<TipoSolicitudEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<TipoSolicitudEntity> getClassType() {

		return TipoSolicitudEntity.class;
	}

}
