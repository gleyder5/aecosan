package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.FactoryFillingException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.CustomValueTransformer;

/**
 * Generates a Java Bean ( or DTO ) from another Java bean or entity<br>
 * This factory executes the getMethods, ,<I>be careful if you are using JPA
 * entities with {@link javax.persistence.FetchType.LAZY}, you could get an
 * exception</i><br>
 * <br>
 * <br>
 * <i><u>Note</u>: The meaning of DTO is Data Transfer Object, this means that
 * is a plain POJO
 * 
 * @author ottoabreu
 * 
 * @param <T>
 *            DTO class
 */
@SuppressWarnings("rawtypes")
public final class BeanFromBeanFactory<T> extends AbstractBeanFactory<T> {

	private Object entity;

	// LOGGER
	private static final Logger logger = LoggerFactory
			.getLogger(BeanFromBeanFactory.class);

	/**
	 * Returns an instance of this factory
	 * 
	 * @param entity
	 *            Object entity
	 * @param dtoClass
	 *            Class of the desired DTO
	 * @return {@link BeanFromBeanFactory}
	 */
	public static BeanFromBeanFactory<?> getInstance(Object entity,
			Class<?> dtoClass) {

		BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
				entity, dtoClass);
		return factoryInstance;
	}

	/**
	 * Constructor that not sets the entity, use it when it is necessary to
	 * create a list
	 * 
	 * @param dtoClass
	 * @return
	 */
	public static BeanFromBeanFactory<?> getInstance(Class<?> dtoClass) {

		BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
				dtoClass);
		return factoryInstance;
	}

	/**
	 * Generate a DTO from the values of an entity<br>
	 * The DTO <B>MUST HAVE THE {@link BeanToBeanMapping} annotation </b>
	 * otherwise will not do anything
	 * 
	 * @return T
	 * @throws BeanFactoryException
	 *             if can not instantiate the new DTO or execute the getter or
	 *             setter methods needed
	 */

	public T generateDTOFromEntity() throws BeanFactoryException {
		return this.generateObjectFromOtherObject();
	}

	/**
	 * Generate a DTO from the values of an entity, ignoring those that are
	 * instances of {@link java.util.Collection}<br>
	 * The DTO <B>MUST HAVE THE {@link DTOMapping} annotation </b> otherwise
	 * will not do anything.<br>
	 * The {@link IgnoreField} must match with attributes in the POJO DTO, those
	 * attributes will be ignored
	 * 
	 * @return T
	 * @throws BeanFactoryException
	 *             if can not instantiate the new DTO or execute the getter or
	 *             setter methods needed
	 */
	public T generateDTOFromEntityNoCollections() {

		return includeExcludeCollectionsFromDTO(FilterCollectionFields.FILTER_FIELDS_NOCOLLECTIONS);
	}

	/**
	 * Generate a DTO from the values of an entity, ignoring those that are
	 * <b>NOT</b>instances of {@link java.util.Collection}<br>
	 * The DTO <B>MUST HAVE THE {@link DTOMapping} annotation </b> otherwise
	 * will not do anything.<br>
	 * The {@link IgnoreField} must match with attributes in the POJO DTO, those
	 * attributes will be ignored
	 * 
	 * @return T
	 * @throws BeanFactoryException
	 *             if can not instantiate the new DTO or execute the getter or
	 *             setter methods needed
	 */
	public T generateDTOFromEntityOnllyCollections() {

		return includeExcludeCollectionsFromDTO(FilterCollectionFields.FILTER_FIELDS_ONLYCOLLECTIONS);
	}

	private T includeExcludeCollectionsFromDTO(
			FilterCollectionFields includeExclude) {

		return this.includeExcludeCollectionsFromNewInstance(includeExclude);

	}

	/**
	 * From a list of entities generate a list of DTO
	 * 
	 * @param entityList
	 * @return List
	 * @throws BeanFactoryException
	 *             if can not create the list
	 */
	public List<T> generateListDTOFromEntity(List<?> entityList)
			throws BeanFactoryException {
		List<T> listDto = new ArrayList<T>();

		for (Object entity : entityList) {
			BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
					entity, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T dto = (T) factoryInstance.generateDTOFromEntity();
			listDto.add(dto);

		}

		return listDto;
	}

	/**
	 * From a list of entities generate a list of DTO, ignoring the fields in
	 * the DTO that are instances of {@link java.util.Collection}
	 * 
	 * @param entityList
	 * @return list
	 * @throws BeanFactoryException
	 *             if can not create the list
	 */
	public List<T> generateListDTOFromEntityNoCollections(List<?> entityList) {
		List<T> listDto = new ArrayList<T>();

		for (Object entity : entityList) {
			BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
					entity, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T dto = (T) factoryInstance.generateDTOFromEntityNoCollections();
			listDto.add(dto);
		}

		return listDto;
	}

	/**
	 * From a list of entities generate a list of DTO, ignoring the fields in
	 * the DTO that are <b>NOT</b> instances of {@link java.util.Collection}
	 * 
	 * @param entityList
	 * @return list
	 * @throws BeanFactoryException
	 *             if can not create the list
	 */
	public List<T> generateListDTOFromEntityOnlyCollections(List<?> entityList) {
		List<T> listDto = new ArrayList<T>();

		for (Object entity : entityList) {
			BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
					entity, this.toInstatiate);
			@SuppressWarnings("unchecked")
			T dto = (T) factoryInstance.generateDTOFromEntityOnllyCollections();
			listDto.add(dto);
		}

		return listDto;
	}

	/**
	 * Gets the annotations from the dto field, extract the value from the
	 * entity and set that value to the corresponding field in the dto
	 * 
	 * @param dtoFields
	 *            Field[] that belongs to the DTO
	 * @param dtoInstance
	 *            T actual instance of the DTO
	 * @throws BeanFactoryException
	 *             if an error occurs executing the getter or setter method
	 * @throws FactoryFillingException
	 *             If can not fill the DTO
	 */
	@Override
	protected void moveValuesFromObjectToNewInstance(Field[] dtoFields,
			T dtoInstance) {

		for (Field field : dtoFields) {

			this.executeMappingInField(field, dtoInstance);

		}
	}

	private void executeMappingInField(Field field, T dtoInstance) {
		
		if (!ignoreInForward(field)) {
			BeanToBeanMapping annotation = field
					.getAnnotation(BeanToBeanMapping.class);
			logger.debug("annotation found: {}", annotation);
			// the field is a collection of others entities
			if (fieldIsACollectionOfOtherBean(annotation)) {

				this.executeSetMethodToFillList(annotation, field, dtoInstance);

				// is a 1 on 1 mapping other DTO
			} else if (fieldIsOneToOtherMapping(annotation)) {
				this.executeSetMethodOnetoOneRelationBeanObject(annotation,
						field, dtoInstance);
				// is a 1 on 1 mapping
			} else if (fieldIsOneToOneMapping(annotation)) {

				this.executeSetMethodOnetoOneRelation(annotation, field,
						dtoInstance);

				// is necessary to obtain the value from other entity
			} else if (fieldValueIsObtainedFromOtherBean(annotation)) {

				this.executeSetMethodValueInRelatedEntity(annotation, field,
						dtoInstance);
				// the annotation is present but there is no information to
				// Retrieve the value
			} else if (fieldHaveAnnotationButMissingConfiguration(annotation)) {
				logger.debug("Field have the same name in both objects");
				this.executeSetMethodOnetoOneRelationSameName(annotation,
						field, dtoInstance);

			} else {
				logger.warn("Can't map the given field "
						+ field.getName()
						+ " because the mapping annotation is not configured properly or does not have any annotation (ignore)");
			}

		} else {
			logger.debug("Ignoring (ignore annotation configured) " + field.getName());
		}
	}
	
	private static boolean ignoreInForward(Field field){
		boolean ignore = false;
	
		if(field.isAnnotationPresent(IgnoreField.class)){
			IgnoreField ignoreAnnotation = field.getAnnotation(IgnoreField.class);
			ignore = ignoreAnnotation.inForward();
		}
		
		return ignore;
	}

	private void executeSetMethodToFillList(BeanToBeanMapping annotation,
			Field field, T dtoInstance) {

		logger.debug("mapping list mapping");
		String fieldName = getFieldNameToGetValueFrom(field.getName(), annotation);
		// the list of other entities
		Collection<?> obtainedListValueFromEntity = (Collection<?>) this
				.executeGetMethod(fieldName, this.entity);

		// obtain the new class for the other dto inside the list
		Class<?> otherDTOClass = annotation.generateListOf();
		// verify if the obtained entity list have some elements
		if (obtainedListValueFromEntity != null
				&& !obtainedListValueFromEntity.isEmpty()) {

			
			Class<? extends Collection> listClass = annotation.listSetterClass();
			Collection<Object> newDTOList = transformCollectionType(listClass);
	
			// for each entity i create a new DTO an put it inside a
			// list
			for (Object otherEntity : obtainedListValueFromEntity) {
				
				this.instanciateNewDTOFromListEntity(otherEntity,
						otherDTOClass, newDTOList);
				// put the list inside the DTO
				this.executeSetMethod(field.getName(), newDTOList, dtoInstance,
						listClass);
			}
		}


	}
	

	private void instanciateNewDTOFromListEntity(Object otherEntity,
			Class<?> otherDTOClass, Collection<Object> newDTOList) {

		// execute the same process of 1 on 1 mapping
		BeanFromBeanFactory<?> factoryInstance = new BeanFromBeanFactory<Object>(
				otherEntity, otherDTOClass);

		Object newDTOInstance = factoryInstance.generateDTOFromEntity();
		logger.debug("new newDTOList: {}",newDTOList);
		newDTOList.add(newDTOInstance);

	}

	private void executeSetMethodOnetoOneRelation(BeanToBeanMapping annotation,
			Field field, T dtoInstance) {
		logger.debug("mapping 1 on 1  mapping");

		Object obtainedValueFromEntity = this.executeGetMethod(
				annotation.getValueFrom(), this.entity);
		this.setValueInDto(obtainedValueFromEntity, annotation, field,
				dtoInstance);
	}

	private void executeSetMethodOnetoOneRelationBeanObject(
			BeanToBeanMapping annotation, Field field, T dtoInstance) {
		logger.debug("mapping 1 on 1  mapping Generate Other");
		Class<?> otherDTOClass = field.getType();
		
		String methodGetValueFrom = getFieldNameToGetValueFrom(field.getName(), annotation);
		Object obtainedOtherEntity = this.executeGetMethod(
				methodGetValueFrom, this.entity);
		if (obtainedOtherEntity != null) {
			BeanFromBeanFactory<?> factory = BeanFromBeanFactory.getInstance(
					obtainedOtherEntity, otherDTOClass);

			Object otherDto = factory.generateDTOFromEntity();

			this.setValueInDto(otherDto, annotation, field, dtoInstance);
		}
	}
	
	
	private void executeSetMethodOnetoOneRelationSameName(
			BeanToBeanMapping annotation, Field field, T dtoInstance) {
		logger.debug("mapping 1 on 1  mapping same name");

		Object obtainedValueFromEntity = this.executeGetMethod(field.getName(),
				this.entity);
		this.setValueInDto(obtainedValueFromEntity, annotation, field,
				dtoInstance);
	}

	private void setValueInDto(Object obtainedValueFromEntity,
			BeanToBeanMapping annotation, Field field, T dtoInstance) {
		Class<?> setClass = null;
		if (obtainedValueFromEntity != null) {
			logger.debug("Obtained value from getter method: {} ", obtainedValueFromEntity);
			if (fieldUseTransformer(annotation)) {
				logger.debug("Obtained value use a custom transformer");
				obtainedValueFromEntity = this.executeTransformer(annotation,
						obtainedValueFromEntity);
			}
			logger.debug("field type:" + field.getType());
			if (fieldIsPrimitive(field)) {
				logger.debug("is primitive value");
				setClass = field.getType();
			}

			this.executeSetMethod(field.getName(), obtainedValueFromEntity,
					dtoInstance, setClass);
		}
	}

	private Object executeTransformer(BeanToBeanMapping annotation, Object input) {

		Object transformedValue = null;
		Class<? extends CustomValueTransformer> transformerClass = annotation
				.useCustomTransformer();
		logger.debug("Obtained transformer class: {}", transformerClass);
		CustomValueTransformer transformer = null;

		try {
			transformer = instanciateTransformer(transformerClass);
			transformedValue = transformer.transform(input);
			logger.debug("transformed value:  {} " ,transformedValue);
		} catch (Exception e) {
			this.handleTransformerInstanciationException(e);
		}

		return transformedValue;
	}

	private void executeSetMethodValueInRelatedEntity(
			BeanToBeanMapping annotation, Field field, T dtoInstance)
			throws FactoryFillingException {

		logger.debug("mapping other references mapping");
		Object obtainedFirstObjectFromEntity = this.executeGetMethod(
				annotation.getValueFrom(), this.entity);

		if (obtainedFirstObjectFromEntity != null) {

			logger.debug("Obtained first value from getter method: {} ", obtainedFirstObjectFromEntity);
			Object obtainedValueFromEntity = this.executeGetMethod(
					annotation.getSecondValueFrom(),

					obtainedFirstObjectFromEntity);
			this.setValueInDto(obtainedValueFromEntity, annotation, field,
					dtoInstance);
		} else {
			logger.warn("The first value is null, can not proceed with the extraction of the second");
		}

	}

	/**
	 * private Constructor
	 * 
	 * @param entity
	 * @param Class
	 *            <?> dtoClass
	 */
	private BeanFromBeanFactory(Object entity, Class<?> dtoClass) {
		super(dtoClass);
		this.entity = entity;

	}

	/**
	 * private Constructor
	 * 
	 * @param entity
	 * @param Class
	 *            <?> dtoClass
	 */
	private BeanFromBeanFactory(Class<?> dtoClass) {
		super(dtoClass);

	}

}
