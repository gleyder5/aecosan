package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class DireccionExtendidaDTO {
	
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("streetName")
	@XmlElement(name = "streetName")
	@BeanToBeanMapping(getValueFrom = "streetName")
	private String streetName;

	@JsonProperty("number")
	@XmlElement(name = "number")
	@BeanToBeanMapping(getValueFrom = "number")
	private String number;
	
	@JsonProperty("stair")
	@XmlElement(name = "stair")
	@BeanToBeanMapping(getValueFrom = "stair")
	private String stair;
	
	@JsonProperty("story")
	@XmlElement(name = "story")
	@BeanToBeanMapping(getValueFrom = "story")
	private String story;
	
	@JsonProperty("door")
	@XmlElement(name = "door")
	@BeanToBeanMapping(getValueFrom = "door")
	private String door;
	
	@JsonProperty("streetAvenue")
	@XmlElement(name = "streetAvenue")
	@BeanToBeanMapping(getValueFrom = "streetAvenue")
	private String streetAvenue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStair() {
		return stair;
	}

	public void setStair(String stair) {
		this.stair = stair;
	}

	public String getStory() {
		return story;
	}

	public void setStory(String story) {
		this.story = story;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	public String getStreetAvenue() {
		return streetAvenue;
	}

	public void setStreetAvenue(String streetAvenue) {
		this.streetAvenue = streetAvenue;
	}
	
}
