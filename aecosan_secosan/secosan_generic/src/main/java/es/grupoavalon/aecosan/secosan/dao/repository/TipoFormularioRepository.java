package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;

public interface TipoFormularioRepository extends CrudRepository<TipoFormularioEntity, Long>, GenericCatalogRepository<TipoFormularioEntity> {

}
