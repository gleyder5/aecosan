package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class ModificacionDatosDTO extends SolicitudDTO {

	@JsonProperty("nombreComercialProducto")
	@XmlElement(name = "nombreComercialProducto")
	@BeanToBeanMapping(getValueFrom = "nombreComercialProducto")
	private String nombreComercialProducto;

	@JsonProperty("rgseaa")
	@XmlElement(name = "rgseaa")
	@BeanToBeanMapping(getValueFrom = "rgseaa")
	private String rgseaa;

	@JsonProperty("nombreRazonSocial")
	@XmlElement(name = "nombreRazonSocial")
	@BeanToBeanMapping(getValueFrom = "nombreRazonSocial")
	private String nombreRazonSocial;

	@JsonProperty("nif")
	@XmlElement(name = "nif")
	@BeanToBeanMapping(getValueFrom = "nif")
	private String nif;

	@JsonProperty("nuevoNombreComercialProducto")
	@XmlElement(name = "nuevoNombreComercialProducto")
	@BeanToBeanMapping(getValueFrom = "nuevoNombreComercialProducto")
	private String nuevoNombreComercialProducto;

	@JsonProperty("modComposicionCualCuan")
	@XmlElement(name = "modComposicionCualCuan")
	@BeanToBeanMapping(getValueFrom = "modComposicionCualCuan")
	private Boolean modComposicionCualCuan;

	@JsonProperty("modDiseno")
	@XmlElement(name = "modDiseno")
	@BeanToBeanMapping(getValueFrom = "modDiseno")
	private Boolean modDiseno;

	@JsonProperty("modAmplPresentaciones")
	@XmlElement(name = "modAmplPresentaciones")
	@BeanToBeanMapping(getValueFrom = "modAmplPresentaciones")
	private Boolean modAmplPresentaciones;

	@JsonProperty("otrasModificaciones")
	@XmlElement(name = "otrasModificaciones")
	@BeanToBeanMapping(getValueFrom = "otrasModificaciones")
	private Boolean otrasModificaciones;

	@JsonProperty("descripcionModificaciones")
	@XmlElement(name = "descripcionModificaciones")
	@BeanToBeanMapping(getValueFrom = "descripcionModificaciones")
	private String descripcionModificaciones;

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public String getNombreRazonSocial() {
		return nombreRazonSocial;
	}

	public void setNombreRazonSocial(String nombreRazonSocial) {
		this.nombreRazonSocial = nombreRazonSocial;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getNuevoNombreComercialProducto() {
		return nuevoNombreComercialProducto;
	}

	public void setNuevoNombreComercialProducto(String nuevoNombreComercialProducto) {
		this.nuevoNombreComercialProducto = nuevoNombreComercialProducto;
	}

	public Boolean isModComposicionCualCuan() {
		return modComposicionCualCuan;
	}

	public void setModComposicionCualCuan(Boolean modComposicionCualCuan) {
		this.modComposicionCualCuan = modComposicionCualCuan;
	}

	public Boolean isModDiseno() {
		return modDiseno;
	}

	public void setModDiseno(Boolean modDiseno) {
		this.modDiseno = modDiseno;
	}

	public Boolean isModAmplPresentaciones() {
		return modAmplPresentaciones;
	}

	public void setModAmplPresentaciones(Boolean modAmplPresentaciones) {
		this.modAmplPresentaciones = modAmplPresentaciones;
	}

	public Boolean isOtrasModificaciones() {
		return otrasModificaciones;
	}

	public void setOtrasModificaciones(Boolean otrasModificaciones) {
		this.otrasModificaciones = otrasModificaciones;
	}

	public String getDescripcionModificaciones() {
		return descripcionModificaciones;
	}

	public void setDescripcionModificaciones(String descripcionModificaciones) {
		this.descripcionModificaciones = descripcionModificaciones;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [nombreComercialProducto=" + nombreComercialProducto + ", rgseaa=" + rgseaa + ", nombreRazonSocial=" + nombreRazonSocial + ", nif=" + nif
				+ ", nuevoNombreComercialProducto=" + nuevoNombreComercialProducto + ", modComposicionCualCuan=" + modComposicionCualCuan + ", modDiseno=" + modDiseno + ", modAmplPresentaciones="
				+ modAmplPresentaciones + ", otrasModificaciones=" + otrasModificaciones + ", descripcionModificaciones=" + descripcionModificaciones + "]";
	}

}
