package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.HistorialSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Component("historialRest")
public class HistorialSolicitudRestService {
    static final Logger logger = LoggerFactory.getLogger(HistorialSolicitudRestService.class);

    @Autowired
    private BusinessFacade bfacade;

    @Autowired
    private ResponseFactory responseFactory;

    private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/historial/";

    private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/historial/";

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path(ADMIN_REST_URL+"{solicitudId}")
    public  Response listItemsAdmin(@PathParam(AbstractListRestHandler.PARAM_USER) String user, @DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
                               @DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
                               @DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
                               @DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw,@PathParam("solicitudId") String solicitudId) {
        logger.debug("-- Method getHistorialSolicitudes GET Init--");
        FilterParams fp = new FilterParams();
        fp.setFilterName("solicitudId");
        fp.setFilterValue(solicitudId);
        return listItemsGeneric(start, length, columnorder, order, filtrosRaw, user,fp);

    }
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path(DEFAULT_REST_URL+"{solicitudId}")
    public  Response listItems(@PathParam(AbstractListRestHandler.PARAM_USER) String user, @DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
                              @DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw,@PathParam("solicitudId") String solicitudId) {
        logger.debug("-- Method getHistorialSolicitudes GET Init--");
        logger.debug("Entrada petición GET listItems()");
        FilterParams fp = new FilterParams();
        fp.setFilterName("solicitudId");
        fp.setFilterValue(solicitudId);
        filtrosRaw.getQueryParameters().add("solicitudId",solicitudId);
        return listItemsGeneric(start, length, columnorder, order, filtrosRaw, user,fp);
    }

    private Response listItemsGeneric(int start, int length, String columnorder, String order, UriInfo filtrosRaw, String user, final FilterParams filterParams){

        AbstractListRestHandler<HistorialSolicitudDTO> listHandler = new AbstractListRestHandler<HistorialSolicitudDTO>() {
            @Override
            protected AbstractResponseFactory getResponseFactory() {
                return responseFactory;
            }

            @Override
            PaginatedListWrapper<HistorialSolicitudDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
                paginationParams.getFilters().add(filterParams);
                return bfacade.getHistorialSolicitudList(otherParams, paginationParams);
            }

        };
        logger.debug("-- Method getBecasSolicitantes GET End--");
        return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters(), user);

    }

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path(ADMIN_REST_URL+"{solicitudId}/{statusId}")
    public Response historialSolicitudBySolicitudAndStatusRestAdmin(@PathParam("solicitudId") int solicitudId,@PathParam("statusId") int statusId) {

        return historialSolicitudBySolicitudAndStatus(solicitudId,statusId);
    }
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    @Path(DEFAULT_REST_URL+"{solicitudId}/{statusId}")
    public Response  historialSolicitudBySolicitudAndStatusRest(@PathParam("solicitudId") int solicitudId,@PathParam("statusId") int statusId) {

        return historialSolicitudBySolicitudAndStatus(solicitudId,statusId);
    }


    private  Response historialSolicitudBySolicitudAndStatus(int solicitudId,int statusId){
        Response response;
        HistorialSolicitudDTO data;
        try {
            data = bfacade.getHistorialSolicitudBySolicitudAndStatus(solicitudId,statusId);
            response = responseFactory.generateOkGenericResponse(data);
        } catch (Exception e) {
            response = responseFactory.generateErrorResponse(e);
        }
        return  response;

    }


}
