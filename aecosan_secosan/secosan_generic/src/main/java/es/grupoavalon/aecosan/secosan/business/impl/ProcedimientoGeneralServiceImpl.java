package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.business.ProcedimientoGeneralService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ProcedimientoGeneralDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.ProcedimientoGeneralDAO;
import es.grupoavalon.aecosan.secosan.dao.TipoDocumentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.ProcedimientoGeneralEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcedimientoGeneralServiceImpl extends AbstractSolicitudService implements ProcedimientoGeneralService {

	@Autowired
	private ProcedimientoGeneralDAO procedimientoGeneralDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Autowired
	private TipoDocumentosDAO tipoDocumentosDAO;

	@Override
	public void add(String user, SolicitudDTO procedimentoGeneralDTO) {
		try {
			solicitudPrepareNullDTO(procedimentoGeneralDTO);
			//TODO
//			prepareProcedimientoGeneralNullability((ProcedimientoGeneralDTO) procedimentoGeneralDTO);
			addProcedimientoGeneralEntity(user, (ProcedimientoGeneralDTO) procedimentoGeneralDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + procedimentoGeneralDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO procedimientoGeneralDTO) {
		try {
			solicitudPrepareNullDTO(procedimientoGeneralDTO);
//			prepareEvaluacionRiesgosNullability((EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
			updateProcediminetoGeneral(user, (ProcedimientoGeneralDTO) procedimientoGeneralDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + procedimientoGeneralDTO);
		}
	}

	private void addProcedimientoGeneralEntity(String user, ProcedimientoGeneralDTO procedimientoGeneralDTO) {
		ProcedimientoGeneralEntity procedimientoGeneralEntity	=	generateProcedimientoGeneralEntityFromDTO(procedimientoGeneralDTO);
		setUserCreatorByUserId(user, procedimientoGeneralEntity);
		prepareAddSolicitudEntity(procedimientoGeneralEntity,procedimientoGeneralDTO);

		ProcedimientoGeneralEntity procedimientoGeneralEntityNew	=	procedimientoGeneralDAO.addProcedimientoGeneralEntity(procedimientoGeneralEntity);
		if(procedimientoGeneralDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(procedimientoGeneralEntityNew);
		}
	}

	private void updateProcediminetoGeneral(String user, ProcedimientoGeneralDTO procedimientoGeneralDTO) {
		ProcedimientoGeneralEntity procedimientoGeneralEntity= transformDTOToEntity(procedimientoGeneralDTO, ProcedimientoGeneralEntity.class);
		setUserCreatorByUserId(user, procedimientoGeneralEntity);
		prepareUpdateSolicitudEntity(procedimientoGeneralEntity, procedimientoGeneralDTO);
		prepareDocumentacionEntityForUpdate(procedimientoGeneralEntity,procedimientoGeneralDTO);
		if(procedimientoGeneralDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(procedimientoGeneralEntity);
		}

		procedimientoGeneralDAO.updateProcedimientoGeneralEntity(procedimientoGeneralEntity);
	}

	private ProcedimientoGeneralEntity generateProcedimientoGeneralEntityFromDTO(ProcedimientoGeneralDTO procedimientoGeneral) {
		prepareSolicitudDTO(procedimientoGeneral);
		return transformDTOToEntity(procedimientoGeneral,ProcedimientoGeneralEntity.class);
	}
//TODO
	private void prepareProcedimientoGeneralDTONullability(ProcedimientoGeneralDTO procedimientoGeneralDTO) {

//		if (null != procedimientoGeneralDTO.ge()) {
//			procedimientoGeneralDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(procedimientoGeneralDTO.getSolicitudePayment().getPayment()));
//			procedimientoGeneralDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(procedimientoGeneralDTO.getSolicitudePayment().getIdPayment()));
//			procedimientoGeneralDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(procedimientoGeneralDTO.getSolicitudePayment().getDataPayment()));
//			if (null != procedimientoGeneralDTO.getSolicitudePayment().getIdPayment()) {
//				procedimientoGeneralDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(procedimientoGeneralDTO.getSolicitudePayment().getIdPayment().getLocation()));
//				prepareLocationNull(procedimientoGeneralDTO.getSolicitudePayment().getIdPayment().getLocation());
//			}
//		}
//		if (null != procedimientoGeneralDTO) {
//			procedimientoGeneralDTO.setPaisOrigen(SecosanUtil.checkDtoNullability(procedimientoGeneralDTO.getPaisOrigen()));
//		}
	}
}
