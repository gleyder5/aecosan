package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.abstractobjfactory.RelatedObjectFactory;

/**
 * This annotation indicates that a field is related to an object that is abstract (or interface).
 * It is required to specify the factory that creates the instance
 * @author otto.abreu
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface IsAbstractRelatedObject {
	/**
	 * INdicate the factory class to use 
	 * @return
	 */
	Class<? extends RelatedObjectFactory> instancefactory();
	/**
	 * Indicate if there is a method that returns a value that can be use to
	 * instanciate diferent concrete classes in runtime;
	 * It is required to write the name of the method exactly.
	 * The method should not have parameters
	 * @return
	 */
	String discriminateMethod() default "";

}
