package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class AlterOferAlimUMEDTO extends FinanciacionAlimentosDTO {

	@JsonProperty("alterMotive")
	@XmlElement(name = "alterMotive")
	@BeanToBeanMapping(getValueFrom = "alterMotive")
	private String alterMotive;

	public String getAlterMotive() {
		return alterMotive;
	}

	public void setAlterMotive(String alterMotive) {
		this.alterMotive = alterMotive;
	}

}
