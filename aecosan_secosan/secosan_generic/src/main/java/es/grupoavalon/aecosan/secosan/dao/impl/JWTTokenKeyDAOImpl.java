package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.JWTTokenKeyDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.JWTTokenKeyEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.JWTTokenKeyRepository;

@Repository
public class JWTTokenKeyDAOImpl extends GenericDao implements JWTTokenKeyDAO {

	private static final boolean ADMIN_TOKEN = true;
	private static final boolean NORMAL_TOKEN = false;

	@Autowired
	private JWTTokenKeyRepository tokenKeyRepo;

	/*
	 * (non-Javadoc)
	 * 
	 * @see es.grupoavalon.eami.eventia.dao.impl.JWTTokenKeyDAO#saveTokenKey(es.
	 * grupoavalon.eami.eventia.dao.entity.JWTTokenKeyEntity)
	 */
	@Override
	public void saveTokenKey(JWTTokenKeyEntity tokenKey) {

		try {
			tokenKeyRepo.save(tokenKey);
		} catch (Exception e) {
			this.handleException(e, "Can't save tokenKey:" + tokenKey);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.eami.eventia.dao.impl.JWTTokenKeyDAO#deleteTokenKey(java.
	 * lang.String)
	 */
	@Override
	public void deleteTokenKey(String tokenKey) {
		try {
			tokenKeyRepo.delete(tokenKey);
		} catch (Exception e) {
			this.handleException(e, "Can't delete tokenKey:" + tokenKey);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.eami.eventia.dao.impl.JWTTokenKeyDAO#getCurrentTokenKey()
	 */
	@Override
	public JWTTokenKeyEntity getCurrentTokenKey() {
		JWTTokenKeyEntity currentKey = null;
		try {
			List<JWTTokenKeyEntity> currents = tokenKeyRepo.getCurrents();
			currentKey = this.extractKey(currents, NORMAL_TOKEN);
		} catch (Exception e) {
			this.handleException(e, "Cant get Token key");
		}
		return currentKey;
	}

	@Override
	public JWTTokenKeyEntity getCurrentTokenKeyAdmin() {
		JWTTokenKeyEntity currentKey = null;
		try {
			List<JWTTokenKeyEntity> currents = tokenKeyRepo.getCurrentsAdmin();
			currentKey = this.extractKey(currents, ADMIN_TOKEN);
		} catch (Exception e) {
			this.handleException(e, "Cant get Token admin key");
		}
		return currentKey;
	}

	private JWTTokenKeyEntity extractKey(List<JWTTokenKeyEntity> currents, boolean adminKey) {
		JWTTokenKeyEntity currentKey = null;
		if (currents != null && currents.size() == 1) {
			currentKey = currents.get(0);

		} else if (currents != null && currents.size() > 1) {
			currentKey = currents.get(0);
			if (adminKey) {
				this.deleteExtraAdminKey(currents);
			} else {
				this.deleteExtraKey(currents);
			}

		}
		return currentKey;

	}

	// it is possible that when the app is in a cluster environment
	// creates more than one key when startup if the instances run at the same
	// time
	// to avoid that behavior we delete the extra tokens leaving only one
	// that the reason why the index start with one
	private void deleteExtraAdminKey(List<JWTTokenKeyEntity> currents) {
		try {
			for (int i = 1; i < currents.size(); i++) {
				JWTTokenKeyEntity addedExtraMustDelete = currents.get(i);
				if (addedExtraMustDelete.isAdminkey()) {
					this.tokenKeyRepo.delete(addedExtraMustDelete.getKey());
				}
			}
		} catch (Exception e) {
			this.handleException(e, "Cant delete extra key from DB " + currents);
		}
	}

	private void deleteExtraKey(List<JWTTokenKeyEntity> currents) {
		try {
			for (int i = 1; i < currents.size(); i++) {
				JWTTokenKeyEntity addedExtraMustDelete = currents.get(i);
				if (!addedExtraMustDelete.isAdminkey()) {
					this.tokenKeyRepo.delete(addedExtraMustDelete.getKey());
				}
			}
		} catch (Exception e) {
			this.handleException(e, "Cant delete extra key from DB " + currents);
		}
	}
}
