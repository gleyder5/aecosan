/**
 * 
 */
package es.grupoavalon.aecosan.secosan.business.exception;

import es.grupoavalon.aecosan.secosan.util.exception.SecosanException;

/**
 * Indicate an error in the service layer
 * @author ottoabreu
 *
 */
public class ServiceException extends SecosanException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * SERVICE_ERROR="Can not complete the operation due an error: ";
	 */
	public static final String SERVICE_ERROR="Can not complete the operation due an error: ";
	
	/**
	 * DB_INCONSISTENCE_DETECTED="Was detected an inconsistency in the database related to the following data: ";
	 */
	public static final String DB_INCONSISTENCY_DETECTED="Was detected an inconsistency in the database related to the following data: ";
	
	/**
	 * PASSWORD_CHANGE_PWMISSMATCH="The given password does not match with the user password";
	 */
	public static final String PASSWRD_CHANGE_PWMISSMATCH="The given password does not match with the user password";
	
	/**
	 * KEY_PASSWORD_MISSMATCH=ServiceException.class.getSimpleName()+".passwordchange.passnotmatch"
	 */
	public static final String KEY_PASSWORD_MISSMATCH=ServiceException.class.getSimpleName()+".passwordchange.passnotmatch";
	
	public static final String NULL_REQUIRED_FIELD = "Required field null: ";
	public static final String FILE_NOT_FOUND = "File not found in Database for this Form";
	
	public static final String FILE_ATTACHABLE_NOT_FOUND = "File PDF not found:";

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ServiceException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}


	public ServiceException(String message) {
		super(message);
	}



}
