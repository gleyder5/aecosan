package es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.AbstractPersonDataDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class DatosEnvioDTO extends AbstractPersonDataDTO implements IsNulable<DatosEnvioDTO> {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "DatosEnvioDTO [id=" + id + "]";
	}

	@Override
	public DatosEnvioDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}
}
