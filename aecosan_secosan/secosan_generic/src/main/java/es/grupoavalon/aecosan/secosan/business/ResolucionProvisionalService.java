package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.report.ResolucionProvisionalReportDTO;

public interface ResolucionProvisionalService {

	ResolucionProvisionalReportDTO getPDFToSign(String user, String solicitudId);

}
