package es.grupoavalon.aecosan.secosan.dao.repository;


import es.grupoavalon.aecosan.secosan.dao.entity.TipoProcedimientoEntity;



public interface TipoProcedimientoRepository extends CrudRepository<TipoProcedimientoEntity, Long> {



}
