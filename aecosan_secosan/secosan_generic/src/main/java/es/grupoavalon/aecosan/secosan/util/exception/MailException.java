package es.grupoavalon.aecosan.secosan.util.exception;


public class MailException extends SecosanException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String EXCEPTION_MESSAGE = "Error sending notification";
	
	public static final String TEMPLATE_READ_ERROR = "Mail template can not be read";
	
	public static final String TEMPLATE_EMPTY_NULL_ERROR = "Mail template is empty or null";
	
	public static final String MAIL_SEND_ADDRESS_EXCEPTION="Can not send notification due an Address error:";
	
	public static final String MAIL_SEND_MESSAGING_EXCEPTION="Can not send notification due an Messaging error:";
	

	public MailException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public MailException(String message) {
		super(message);
	}

}
