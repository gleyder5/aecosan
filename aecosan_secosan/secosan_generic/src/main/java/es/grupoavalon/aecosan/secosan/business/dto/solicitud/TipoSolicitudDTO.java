package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class TipoSolicitudDTO extends AbstractCatalogDto{

	@JsonProperty("relatedArea")
	@XmlElement(name = "relatedArea")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "relatedArea")
	private AreaDTO relatedArea;

	public AreaDTO getRelatedArea() {
		return relatedArea;
	}

	public void setRelatedArea(AreaDTO relatedArea) {
		this.relatedArea = relatedArea;
	}

	@Override
	public String toString() {
		return "TipoSolicitudDTO [id=" + id + ", relatedArea=" + relatedArea + "]";
	}

}
