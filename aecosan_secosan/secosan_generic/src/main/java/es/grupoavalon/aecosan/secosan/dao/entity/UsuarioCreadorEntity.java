package es.grupoavalon.aecosan.secosan.dao.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_USUARIO_EXTERNOS)
public abstract class UsuarioCreadorEntity extends UsuarioEntity {


	@OneToMany(mappedBy = "userCreator", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<SolicitudEntity> createdSolicitud;

	public List<SolicitudEntity> getCreatedSolicitud() {
		return createdSolicitud;
	}


	public void setCreatedSolicitud(List<SolicitudEntity> createdSolicitud) {
		this.createdSolicitud = createdSolicitud;
	}



}
