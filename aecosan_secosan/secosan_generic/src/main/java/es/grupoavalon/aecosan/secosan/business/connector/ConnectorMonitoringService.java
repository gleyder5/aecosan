package es.grupoavalon.aecosan.secosan.business.connector;

import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;


public interface ConnectorMonitoringService {

	void monitoringSuccess(MonitorizableConnectors connector);

	void monitoringError(MonitorizableConnectors connector, Throwable e);

}