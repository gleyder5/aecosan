package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_FINANCIACION_ALIMENTOS)
public class FinanciacionAlimentosEntity extends AbstractFinanciacionAlimentos {
	@Column(name = "NUMERO_REGISTRO_EMPRESA")
	private String companyRegisterNumber;

	@Column(name = "NOMBRE_PRODUCTO")
	private String productName;

	@Column(name = "NUMERO_REFERENCIA_PRODUCTO")
	private String productReferenceNumber;

	@Column(name = "EDAD")
	private String age;

	@Column(name = "DENSIDAD_CALORICA")
	private String caloricDensity;

	@Column(name = "OSMOLARIDAD")
	private String osmolarity;

	@Column(name = "OSMOLALIDAD")
	private String osmolality;

	@Column(name = "PESO_MOLECULAR")
	private String molecularWeight;

	@Column(name = "INDICACIONES")
	private String instruction;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = ColumnNames.OFFER_TYPE)
	private String offerType;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "GRUPO_PACIENTES_ID")
	private GrupoPacientesEntity groupOfPatient;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ESTADO_FISICO_ID")
	private EstadoFisicoEntity physicalState;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "REPARTO_CALORICO_ID")
	private RepartoCaloricoEntity caloricDistribution;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "FIBRA_ID")
	private FibraEntity fiber;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PROPUESTA_ID")
	private PropuestaEntity proposal;

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "MODO_ADMINISTRACION_ID")
	private ModoAdministracionEntity administrationMode;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "financiacionAlimentos", cascade = CascadeType.ALL)
	@JoinColumn(name = "OFER_ALIM_UME_ID")
	private List<PresentacionEnvaseEntity> presentations;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "PAGO_SOLICITUD_ID")
	private PagoSolicitudEntity solicitudePayment;

	public String getCompanyRegisterNumber() {
		return companyRegisterNumber;
	}

	public void setCompanyRegisterNumber(String companyRegisterNumber) {
		this.companyRegisterNumber = companyRegisterNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductReferenceNumber() {
		return productReferenceNumber;
	}

	public void setProductReferenceNumber(String productReferenceNumber) {
		this.productReferenceNumber = productReferenceNumber;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getCaloricDensity() {
		return caloricDensity;
	}

	public void setCaloricDensity(String caloricDensity) {
		this.caloricDensity = caloricDensity;
	}

	public String getOsmolarity() {
		return osmolarity;
	}

	public void setOsmolarity(String osmolarity) {
		this.osmolarity = osmolarity;
	}

	public String getOsmolality() {
		return osmolality;
	}

	public void setOsmolality(String osmolality) {
		this.osmolality = osmolality;
	}

	public String getMolecularWeight() {
		return molecularWeight;
	}

	public void setMolecularWeight(String molecularWeight) {
		this.molecularWeight = molecularWeight;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}

	public GrupoPacientesEntity getGroupOfPatient() {
		return groupOfPatient;
	}

	public void setGroupOfPatient(GrupoPacientesEntity groupOfPatient) {
		this.groupOfPatient = groupOfPatient;
	}

	public EstadoFisicoEntity getPhysicalState() {
		return physicalState;
	}

	public void setPhysicalState(EstadoFisicoEntity physicalState) {
		this.physicalState = physicalState;
	}

	public RepartoCaloricoEntity getCaloricDistribution() {
		return caloricDistribution;
	}

	public void setCaloricDistribution(RepartoCaloricoEntity caloricDistribution) {
		this.caloricDistribution = caloricDistribution;
	}

	public FibraEntity getFiber() {
		return fiber;
	}

	public void setFiber(FibraEntity fiber) {
		this.fiber = fiber;
	}

	public PropuestaEntity getProposal() {
		return proposal;
	}

	public void setProposal(PropuestaEntity proposal) {
		this.proposal = proposal;
	}

	public ModoAdministracionEntity getAdministrationMode() {
		return administrationMode;
	}

	public void setAdministrationMode(ModoAdministracionEntity administrationMode) {
		this.administrationMode = administrationMode;
	}

	public List<PresentacionEnvaseEntity> getPresentations() {
		return presentations;
	}

	public void setPresentations(List<PresentacionEnvaseEntity> presentations) {
		this.presentations = presentations;
	}

	public PagoSolicitudEntity getSolicitudePayment() {
		return solicitudePayment;
	}

	public void setSolicitudePayment(PagoSolicitudEntity solicitudePayment) {
		this.solicitudePayment = solicitudePayment;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((osmolarity == null) ? 0 : osmolarity.hashCode());
		result = prime * result + ((osmolarity == null) ? 0 : osmolarity.hashCode());
		result = prime * result + ((administrationMode == null) ? 0 : administrationMode.getId().hashCode());
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((caloricDensity == null) ? 0 : caloricDensity.hashCode());
		result = prime * result + ((caloricDistribution == null) ? 0 : caloricDistribution.getId().hashCode());
		result = prime * result + ((companyRegisterNumber == null) ? 0 : companyRegisterNumber.hashCode());
		result = prime * result + ((fiber == null) ? 0 : fiber.getId().hashCode());
		result = prime * result + ((groupOfPatient == null) ? 0 : groupOfPatient.getId().hashCode());
		result = prime * result + ((instruction == null) ? 0 : instruction.hashCode());
		result = prime * result + ((molecularWeight == null) ? 0 : molecularWeight.hashCode());
		result = prime * result + ((physicalState == null) ? 0 : physicalState.getId().hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((productReferenceNumber == null) ? 0 : productReferenceNumber.hashCode());
		result = prime * result + ((proposal == null) ? 0 : proposal.getId().hashCode());
		result = prime * result + ((solicitudePayment == null) ? 0 : solicitudePayment.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinanciacionAlimentosEntity other = (FinanciacionAlimentosEntity) obj;
		if (osmolarity == null) {
			if (other.osmolarity != null)
				return false;
		} else if (!osmolarity.equals(other.osmolarity))
			return false;
		if (osmolarity == null) {
			if (other.osmolarity != null)
				return false;
		} else if (!osmolarity.equals(other.osmolarity))
			return false;
		if (administrationMode == null) {
			if (other.administrationMode != null)
				return false;
		} else if (!administrationMode.getId().equals(other.administrationMode.getId()))
			return false;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (caloricDensity == null) {
			if (other.caloricDensity != null)
				return false;
		} else if (!caloricDensity.equals(other.caloricDensity))
			return false;
		if (caloricDistribution == null) {
			if (other.caloricDistribution != null)
				return false;
		} else if (!caloricDistribution.getId().equals(other.caloricDistribution.getId()))
			return false;
		if (companyRegisterNumber == null) {
			if (other.companyRegisterNumber != null)
				return false;
		} else if (!companyRegisterNumber.equals(other.companyRegisterNumber))
			return false;
		if (fiber == null) {
			if (other.fiber != null)
				return false;
		} else if (!fiber.getId().equals(other.fiber.getId()))
			return false;
		if (groupOfPatient == null) {
			if (other.groupOfPatient != null)
				return false;
		} else if (!groupOfPatient.getId().equals(other.groupOfPatient.getId()))
			return false;
		if (instruction == null) {
			if (other.instruction != null)
				return false;
		} else if (!instruction.equals(other.instruction))
			return false;
		if (molecularWeight == null) {
			if (other.molecularWeight != null)
				return false;
		} else if (!molecularWeight.equals(other.molecularWeight))
			return false;
		if (physicalState == null) {
			if (other.physicalState != null)
				return false;
		} else if (!physicalState.getId().equals(other.physicalState.getId()))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (productReferenceNumber == null) {
			if (other.productReferenceNumber != null)
				return false;
		} else if (!productReferenceNumber.equals(other.productReferenceNumber))
			return false;
		if (proposal == null) {
			if (other.proposal != null)
				return false;
		} else if (!proposal.getId().equals(other.proposal.getId()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [companyRegisterNumber=" + companyRegisterNumber + ", productName=" + productName + ", productReferenceNumber=" + productReferenceNumber + ", age="
				+ age + ", caloricDensity=" + caloricDensity + ", osmolarity=" + osmolarity + ", Osmolarity2=" + osmolality + ", molecularWeight=" + molecularWeight + ", instruction=" + instruction
				+ ", groupOfPatient=" + ((groupOfPatient == null) ? "" : groupOfPatient.getId()) + ", physicalState=" + ((physicalState == null) ? "" : physicalState.getId())
				+ ", caloricDistribution=" + ((caloricDistribution == null) ? "" : caloricDistribution.getId()) + ", fibre=" + ((fiber == null) ? "" : fiber.getId()) + ", proposal="
				+ ((proposal == null) ? "" : proposal.getId()) + ", administrationMode=" + ((administrationMode == null) ? "" : administrationMode.getId()) + ", solicitudePayment= "
				+ ((solicitudePayment == null) ? "" : solicitudePayment.getId()) + "]";
	}

}
