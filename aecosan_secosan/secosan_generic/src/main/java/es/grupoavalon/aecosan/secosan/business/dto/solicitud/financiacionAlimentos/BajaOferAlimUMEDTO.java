package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class BajaOferAlimUMEDTO extends AbstractFinanciacionAlimentosDTO {

	@JsonProperty("communicationDate")
	@XmlElement(name = "communicationDate")
	@BeanToBeanMapping(getValueFrom = "communicationDateLong")
	private Long communicationDate;

	@JsonProperty("suspensionReasonDuration")
	@XmlElement(name = "suspensionReasonDuration")
	@BeanToBeanMapping(getValueFrom = "suspensionReasonDuration")
	private String suspensionReasonDuration;

	@JsonProperty("cancelOrSuspension")
	@XmlElement(name = "cancelOrSuspension")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "cancelOrSuspension")
	private BajaOSuspensionDTO cancelOrSuspension;

	@JsonProperty("products")
	@XmlElement(name = "products")
	@BeanToBeanMapping(getValueFrom = "products", generateListOf = ProductoSuspendidoDTO.class, generateListFrom = ProductoSuspendidoEntity.class)
	private List<ProductoSuspendidoDTO> products;

	@JsonProperty("companyRegisterNumber")
	@XmlElement(name = "companyRegisterNumber")
	@BeanToBeanMapping(getValueFrom = "companyRegisterNumber")
	private String companyRegisterNumber;

	@JsonProperty("companyName")
	@XmlElement(name = "companyName")
	@BeanToBeanMapping(getValueFrom = "companyName")
	private String companyName;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyRegisterNumber() {
		return companyRegisterNumber;
	}

	public void setCompanyRegisterNumber(String companyRegisterNumber) {
		this.companyRegisterNumber = companyRegisterNumber;
	}

	public Long getCommunicationDate() {
		return communicationDate;
	}

	public void setCommunicationDate(Long communicationDate) {
		this.communicationDate = communicationDate;
	}

	public String getSuspensionReasonDuration() {
		return suspensionReasonDuration;
	}

	public void setSuspensionReasonDuration(String suspensionReasonDuration) {
		this.suspensionReasonDuration = suspensionReasonDuration;
	}

	public BajaOSuspensionDTO getCancelOrSuspension() {
		return cancelOrSuspension;
	}

	public void setCancelOrSuspension(BajaOSuspensionDTO cancelOrSuspension) {
		this.cancelOrSuspension = cancelOrSuspension;
	}

	public List<ProductoSuspendidoDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductoSuspendidoDTO> products) {
		this.products = products;
	}

	@Override
	public String toString() {
		return "BajaOferAlimUMEDTO [communicationDate=" + communicationDate + ", suspensionReasonDuration=" + suspensionReasonDuration + ", cancelOrSuspension=" + cancelOrSuspension + ", products="
				+ products + "]";
	}

}
