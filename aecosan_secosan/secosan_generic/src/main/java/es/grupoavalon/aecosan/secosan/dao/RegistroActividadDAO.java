package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.RegistroActividadEntity;

public interface RegistroActividadDAO {

	void registerActivity(RegistroActividadEntity activity);

}