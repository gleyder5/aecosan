package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.TipoSuspensionFinanciacionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoSuspensionFinanciacionRepository;


@Component("TipoSuspensionFinanciacion")
public class TipoSuspensionFinanciacionRepositoryImpl extends CrudCatalogRepositoryImpl<TipoSuspensionFinanciacionEntity, Long>
		implements TipoSuspensionFinanciacionRepository, GenericCatalogRepository<TipoSuspensionFinanciacionEntity> {

	@Override
	protected Class<TipoSuspensionFinanciacionEntity> getClassType() {
		return TipoSuspensionFinanciacionEntity.class;
	}

	@Override
	public List<TipoSuspensionFinanciacionEntity> getCatalogList() {
		return findAll();
	}
}
