package es.grupoavalon.aecosan.secosan.business.dto.report;

public class SignedLogosReportDTO extends SignedReportDTO {

	private String denomination;

	private String nombreSolicitante;
	private String direccionSolicitante;
	private String nifSolicitante;
	private String telefonoSolicitante;
	private String mailSolicitante;
	private String nombreRepresentante;
	private String direccionRepresentante;
	private String nifRepresentante;
	private String telefonoRepresentante;
	private String mailRepresentante;
	private String paisSolicitante;
	private String paisRepresentante;
	private String material;

	public String getDenomination() {
		return denomination;
	}

	public void setDenomination(String denomination) {
		this.denomination = denomination;
	}

	public String getNombreSolicitante() {
		return nombreSolicitante;
	}

	public void setNombreSolicitante(String nombreSolicitante) {
		this.nombreSolicitante = nombreSolicitante;
	}

	public String getDireccionSolicitante() {
		return direccionSolicitante;
	}

	public void setDireccionSolicitante(String direccionSolicitante) {
		this.direccionSolicitante = direccionSolicitante;
	}

	public String getNifSolicitante() {
		return nifSolicitante;
	}

	public void setNifSolicitante(String nifSolicitante) {
		this.nifSolicitante = nifSolicitante;
	}

	public String getTelefonoSolicitante() {
		return telefonoSolicitante;
	}

	public void setTelefonoSolicitante(String telefonoSolicitante) {
		this.telefonoSolicitante = telefonoSolicitante;
	}

	public String getMailSolicitante() {
		return mailSolicitante;
	}

	public void setMailSolicitante(String mailSolicitante) {
		this.mailSolicitante = mailSolicitante;
	}

	public String getNombreRepresentante() {
		return nombreRepresentante;
	}

	public void setNombreRepresentante(String nombreRepresentante) {
		this.nombreRepresentante = nombreRepresentante;
	}

	public String getDireccionRepresentante() {
		return direccionRepresentante;
	}

	public void setDireccionRepresentante(String direccionRepresentante) {
		this.direccionRepresentante = direccionRepresentante;
	}

	public String getNifRepresentante() {
		return nifRepresentante;
	}

	public void setNifRepresentante(String nifRepresentante) {
		this.nifRepresentante = nifRepresentante;
	}

	public String getTelefonoRepresentante() {
		return telefonoRepresentante;
	}

	public void setTelefonoRepresentante(String telefonoRepresentante) {
		this.telefonoRepresentante = telefonoRepresentante;
	}

	public String getMailRepresentante() {
		return mailRepresentante;
	}

	public void setMailRepresentante(String mailRepresentante) {
		this.mailRepresentante = mailRepresentante;
	}

	public String getPaisSolicitante() {
		return paisSolicitante;
	}

	public void setPaisSolicitante(String paisSolicitante) {
		this.paisSolicitante = paisSolicitante;
	}

	public String getPaisRepresentante() {
		return paisRepresentante;
	}

	public void setPaisRepresentante(String paisRepresentante) {
		this.paisRepresentante = paisRepresentante;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getFinalidad() {
		return finalidad;
	}

	public void setFinalidad(String finalidad) {
		this.finalidad = finalidad;
	}

	private String finalidad;

	@Override
	public String toString() {
		return "SignedLogosReportDTO [denomination=" + denomination + ", nombreSolicitante=" + nombreSolicitante + ", direccionSolicitante=" + direccionSolicitante + ", nifSolicitante="
				+ nifSolicitante + ", telefonoSolicitante=" + telefonoSolicitante + ", mailSolicitante=" + mailSolicitante + ", nombreRepresentante=" + nombreRepresentante
				+ ", direccionRepresentante=" + direccionRepresentante + ", nifRepresentante=" + nifRepresentante + ", telefonoRepresentante=" + telefonoRepresentante + ", mailRepresentante="
				+ mailRepresentante + ", paisSolicitante=" + paisSolicitante + ", paisRepresentante=" + paisRepresentante + ", material=" + material + ", finalidad=" + finalidad + "]";
	}

}
