package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioInternoEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface UsuarioInternoRepository extends CrudRepository<UsuarioInternoEntity, Long> {

	UsuarioInternoEntity findByUserName(String userName);

	List<UsuarioInternoEntity> findAllByPaginationParams(PaginationParams paginationParams);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);
}
