package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class ProductoSuspendidoDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("bajaOferAlim")
	@XmlElement(name = "bajaOferAlim")
	@BeanToBeanMapping(getValueFrom = "bajaOferAlim", getSecondValueFrom = "id")
	private Long bajaOferAlim;

	@JsonProperty("productRegistrationNumber")
	@XmlElement(name = "productRegistrationNumber")
	@BeanToBeanMapping(getValueFrom = "productRegistrationNumber")
	private String productRegistrationNumber;

	@JsonProperty("identificationCode")
	@XmlElement(name = "identificationCode")
	@BeanToBeanMapping(getValueFrom = "identificationCode")
	private String identificationCode;

	@JsonProperty("description")
	@XmlElement(name = "description")
	@BeanToBeanMapping(getValueFrom = "description")
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getBajaOferAlim() {
		return bajaOferAlim;
	}

	public void setBajaOferAlim(Long bajaOferAlim) {
		this.bajaOferAlim = bajaOferAlim;
	}

	public String getProductRegistrationNumber() {
		return productRegistrationNumber;
	}

	public void setProductRegistrationNumber(String productRegistrationNumber) {
		this.productRegistrationNumber = productRegistrationNumber;
	}

	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ProductoSuspendidoDTO [id=" + id + ", bajaOferAlim=" + bajaOferAlim + ", productRegistrationNumber=" + productRegistrationNumber + ", identificationCode=" + identificationCode
				+ ", description=" + description + "]";
	}

}
