package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioExtraComunitarioEntity;

public interface UsuarioExtracomunitarioRepository extends CrudRepository<UsuarioExtraComunitarioEntity, Long> {

	UsuarioExtraComunitarioEntity findByUserLogin(String userName);

}
