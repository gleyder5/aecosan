package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.IncOferAlimUMEDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIIDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIIIDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIVDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoFirmadoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoLogosDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("impresoFirmaRest")
public class ImpresoFirmaRestService {

	private static final Logger logger = LoggerFactory.getLogger(ImpresoFirmaRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirma/{user}")
	public Response getPdfReport(@PathParam("user") String user, ImpresoFirmadoDTO impresoFirma) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ", user );

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.getPDFToSign(user, impresoFirma);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}



	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirmaANEXOIIFichaTecnicaDetail/{user}")
	public Response getPdfReportFichaTecnicaDetail(@PathParam("user") String user,IncOferAlimUMEDTO form) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ", user );

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.generarFichaTecnicaAnexoII(form);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirmaANEXOII/{user}")
	public Response getPdfReportAnexoII(@PathParam("user") String user, IncOferAlimUMEDTO form) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ", user );

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.getPDFAnexoIIPDF(user, form);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirmaANEXOIII/{user}")
	public Response getPdfReportAnexoIII(@PathParam("user") String user, ImpresoAnexoIIIDTO impresoFirma) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ",user);

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.getPDFToSign(user, impresoFirma);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirmaANEXOIV/{user}")
	public Response getPdfReportAnexoIV(@PathParam("user") String user, ImpresoAnexoIVDTO impresoFirma) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} ", user );

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.getPDFToSign(user, impresoFirma);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path(SecosanConstants.SECURE_CONTEXT + "/impresoFirmaLogos/{user}")
	public Response getPdfLogos(@PathParam("user") String user, ImpresoLogosDTO impresoFirma) {
		Response response = null;
		try {
			logger.debug("-- Method getPdfReport || user: {} --", user );

			ImpresoFirmadoDTO reportToSign;
			reportToSign = bfacade.getPDFToSign(user, impresoFirma);
			response = responseFactory.generateOkGenericResponse(reportToSign);

		} catch (Exception e) {
			logger.error("Error getPdfReport || user: " + user + " ERROR: " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

}
