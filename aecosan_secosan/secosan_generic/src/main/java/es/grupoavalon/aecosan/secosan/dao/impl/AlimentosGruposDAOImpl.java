package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.AlimentosGruposDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.CeseComercializacionAlimentosGruposRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.ModificacionDatosAlimentosGrupoRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.PuestaMercadoAlimentosGruposRepository;

@Repository
public class AlimentosGruposDAOImpl extends GenericDao implements AlimentosGruposDAO {

	@Autowired
	private CeseComercializacionAlimentosGruposRepository ceseComercializacionAlimentosGruposRepository;

	@Autowired
	private ModificacionDatosAlimentosGrupoRepository modificacionDatosAlimentosGrupoRepository;

	@Autowired
	private PuestaMercadoAlimentosGruposRepository puestaMercadoAlimentosGruposRepository;

	@Override
	public CeseComercializacionAGEntity addCeseComercializacion(CeseComercializacionAGEntity ceseComercializacionEntity) {

		CeseComercializacionAGEntity ceseComercializacion = null;
		try {
			ceseComercializacion = ceseComercializacionAlimentosGruposRepository.save(ceseComercializacionEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addCeseComercializacion | ERROR: " + ceseComercializacionEntity);
		}

		return ceseComercializacion;
	}

	@Override
	public void updateCeseComercializacion(CeseComercializacionAGEntity ceseComercializacionEntity) {
		try {
			ceseComercializacionAlimentosGruposRepository.update(ceseComercializacionEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateCeseComercializacion | ERROR: " + ceseComercializacionEntity);
		}
	}

	@Override
	public ModificacionDatosAGEntity addModificacionDatos(ModificacionDatosAGEntity modificacionDatosEntity) {
		try {
			modificacionDatosAlimentosGrupoRepository.save(modificacionDatosEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addModificacionDatos | ERROR: " + modificacionDatosEntity);
		}
		return modificacionDatosEntity;
	}

	@Override
	public void updateModificacionDatos(ModificacionDatosAGEntity modificacionDatosEntity) {
		try {
			modificacionDatosAlimentosGrupoRepository.update(modificacionDatosEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateModificacionDatos | ERROR: " + modificacionDatosEntity);
		}
	}

	@Override
	public PuestaMercadoAGEntity addPuestaMercado(PuestaMercadoAGEntity puestaMercadoEntity) {
		try {
			puestaMercadoAlimentosGruposRepository.save(puestaMercadoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addPuestaMercado | ERROR: " + puestaMercadoEntity);
		}
		return puestaMercadoEntity;
	}

	@Override
	public void updatePuestaMercado(PuestaMercadoAGEntity puestaMercadoEntity) {
		try {
			puestaMercadoAlimentosGruposRepository.update(puestaMercadoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updatePuestaMercado | ERROR: " + puestaMercadoEntity);
		}

	}
}
