package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudLmrFitosanitariosRepository;

@Component
public class SolicitudLmrFitosanitariosRepositoryImpl extends AbstractCrudRespositoryImpl<SolicitudLmrFitosanitariosEntity, Long> implements SolicitudLmrFitosanitariosRepository {
	
	@Override
	protected Class<SolicitudLmrFitosanitariosEntity> getClassType() {
		return SolicitudLmrFitosanitariosEntity.class;
	}
}