package es.grupoavalon.aecosan.secosan.business.connector;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.MonitoringConnectorDTO;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface ConnectorMonitoringListService {

	public List<MonitoringConnectorDTO> getListMonitoringConnector(PaginationParams paginationParams);

	public int getTotalListMonitoringConnector(List<FilterParams> filters);

	public int getFilteredListMonitoringConnector(List<FilterParams> filters);


}
