package es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante;

import es.grupoavalon.aecosan.secosan.util.report.Reportable;

public class ImpresoJustificantePagoDTO implements Reportable {

	private String identificationNumber;
	private String formName;
	private String codigo;
	private String ingresoComplementario;
	private String nombreAgencia;
	private String textoNumRegistro;
	private String textoDescripcionTasa;
	private String ejercicio;
	private String nif;
	private String apellidos;
	private String direccion;
	private String numero;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	private String escalera;
	private String piso;
	private String puerta;
	private String tipoCalle;
	private String telefono;
	private String municipio;
	private String provincia;
	private String codigoBarras;
	private String codigoBarrasNumerico;
	private String codigoControl;
	private String codigoPostal;
	private String claveTasa;
	private String nombreClaveTasa;
	private String importeTasa;
	private String cantidadTasa;
	private String totalNumeroClaves;
	private String totalImporte;
	private String numeroJustificante;
	private String diaIngreso;
	private String mesIngreso;
	private String anyoIngreso;
	private String ciudadDeclarante;
	private String diaDeclarante;
	private String mesDeclarante;
	private String anyoDeclarante;
	private String textoClaves;
	private String textoIngresoComplementario;
	private String textoIngresoTesoro;
	private String euros;
	private String centimos;
	private String pagoEfectivo;
	private String pagoCuenta;
	private String iban;
	private String importe;
	private String textoEjemplar;
	private String numeroRegistroEmpresa;

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getNumeroRegistroEmpresa() {
		return numeroRegistroEmpresa;
	}

	public void setNumeroRegistroEmpresa(String numeroRegistroEmpresa) {
		this.numeroRegistroEmpresa = numeroRegistroEmpresa;
	}

	public String getEuros() {
		return euros;
	}

	public void setEuros(String euros) {
		this.euros = euros;
	}
	
	public String getCentimos() {
		return centimos;
	}

	public void setCentimos(String centimos) {
		this.centimos = centimos;
	}

	public String getTextoEjemplar() {
		return textoEjemplar;
	}

	public void setTextoEjemplar(String textoEjemplar) {
		this.textoEjemplar = textoEjemplar;
	}

	public String getImporte() {
		return importe;
	}

	public String getIngresoComplementario() {
		return ingresoComplementario;
	}

	public void setIngresoComplementario(String ingresoComplementario) {
		this.ingresoComplementario = ingresoComplementario;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getEjercicio() {
		return ejercicio;
	}

	public void setEjercicio(String ejercicio) {
		this.ejercicio = ejercicio;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	public String getEscalera() {
		return escalera;
	}

	public void setEscalera(String escalera) {
		this.escalera = escalera;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getPuerta() {
		return puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}
	
	public String getTipoCalle() {
		return tipoCalle;
	}

	public void setTipoCalle(String tipoCalle) {
		this.tipoCalle = tipoCalle;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCodigoBarras() {
		return codigoBarras;
	}

	public void setCodigoBarras(String codigoBarras) {
		this.codigoBarras = codigoBarras;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getClaveTasa() {
		return claveTasa;
	}

	public void setClaveTasa(String claveTasa) {
		this.claveTasa = claveTasa;
	}

	public String getNombreClaveTasa() {
		return nombreClaveTasa;
	}

	public void setNombreClaveTasa(String nombreClaveTasa) {
		this.nombreClaveTasa = nombreClaveTasa;
	}

	public String getImporteTasa() {
		return importeTasa;
	}

	public void setImporteTasa(String importeTasa) {
		this.importeTasa = importeTasa;
	}

	public String getCantidadTasa() {
		return cantidadTasa;
	}

	public void setCantidadTasa(String cantidadTasa) {
		this.cantidadTasa = cantidadTasa;
	}

	public String getTotalNumeroClaves() {
		return totalNumeroClaves;
	}

	public void setTotalNumeroClaves(String totalNumeroClaves) {
		this.totalNumeroClaves = totalNumeroClaves;
	}

	public String getTotalImporte() {
		return totalImporte;
	}

	public void setTotalImporte(String totalImporte) {
		this.totalImporte = totalImporte;
	}

	public String getNumeroJustificante() {
		return numeroJustificante;
	}

	public void setNumeroJustificante(String numeroJustificante) {
		this.numeroJustificante = numeroJustificante;
	}

	public String getDiaIngreso() {
		return diaIngreso;
	}

	public void setDiaIngreso(String diaIngreso) {
		this.diaIngreso = diaIngreso;
	}

	public String getMesIngreso() {
		return mesIngreso;
	}

	public void setMesIngreso(String mesIngreso) {
		this.mesIngreso = mesIngreso;
	}

	public String getAnyoIngreso() {
		return anyoIngreso;
	}

	public void setAnyoIngreso(String anyoIngreso) {
		this.anyoIngreso = anyoIngreso;
	}

	public String getCiudadDeclarante() {
		return ciudadDeclarante;
	}

	public void setCiudadDeclarante(String ciudadDeclarante) {
		this.ciudadDeclarante = ciudadDeclarante;
	}

	public String getDiaDeclarante() {
		return diaDeclarante;
	}

	public void setDiaDeclarante(String diaDeclarante) {
		this.diaDeclarante = diaDeclarante;
	}

	public String getMesDeclarante() {
		return mesDeclarante;
	}

	public void setMesDeclarante(String mesDeclarante) {
		this.mesDeclarante = mesDeclarante;
	}

	public String getAnyoDeclarante() {
		return anyoDeclarante;
	}

	public void setAnyoDeclarante(String anyoDeclarante) {
		this.anyoDeclarante = anyoDeclarante;
	}

	public String getTextoClaves() {
		return textoClaves;
	}

	public void setTextoClaves(String textoClaves) {
		this.textoClaves = textoClaves;
	}

	public String getTextoIngresoComplementario() {
		return textoIngresoComplementario;
	}

	public void setTextoIngresoComplementario(String textoIngresoComplementario) {
		this.textoIngresoComplementario = textoIngresoComplementario;
	}

	public String getTextoIngresoTesoro() {
		return textoIngresoTesoro;
	}

	public void setTextoIngresoTesoro(String textoIngresoTesoro) {
		this.textoIngresoTesoro = textoIngresoTesoro;
	}

	public String getPagoEfectivo() {
		return pagoEfectivo;
	}

	public void setPagoEfectivo(String pagoEfectivo) {
		this.pagoEfectivo = pagoEfectivo;
	}

	public String getPagoCuenta() {
		return pagoCuenta;
	}

	public void setPagoCuenta(String pagoCuenta) {
		this.pagoCuenta = pagoCuenta;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getNombreAgencia() {
		return nombreAgencia;
	}

	public void setNombreAgencia(String nombreAgencia) {
		this.nombreAgencia = nombreAgencia;
	}

	public String getTextoNumRegistro() {
		return textoNumRegistro;
	}

	public void setTextoNumRegistro(String textoNumRegistro) {
		this.textoNumRegistro = textoNumRegistro;
	}

	public String getTextoDescripcionTasa() {
		return textoDescripcionTasa;
	}

	public void setTextoDescripcionTasa(String textoDescripcionTasa) {
		this.textoDescripcionTasa = textoDescripcionTasa;
	}

	public String getCodigoBarrasNumerico() {
		return codigoBarrasNumerico;
	}

	public void setCodigoBarrasNumerico(String codigoBarrasNumerico) {
		this.codigoBarrasNumerico = codigoBarrasNumerico;
	}

	public String getCodigoControl() {
		return codigoControl;
	}

	public void setCodigoControl(String codigoControl) {
		this.codigoControl = codigoControl;
	}

	@Override
	public String toString() {
		return "ImpresoJustificantePagoDTO [identificationNumber=" + identificationNumber + ", formName=" + formName + ", codigo=" + codigo + ", ingresoComplementario=" + ingresoComplementario
				+ ", nombreAgencia=" + nombreAgencia + ", textoNumRegistro=" + textoNumRegistro + ", textoDescripcionTasa=" + textoDescripcionTasa + ", ejercicio=" + ejercicio + ", nif=" + nif
				+ ", apellidos=" + apellidos + ", direccion=" + direccion + ", numero=" + numero + ", escalera=" + escalera + ", piso=" + piso + ", puerta=" + puerta + ", tipoCalle=" + tipoCalle
				+ ", telefono=" + telefono + ", municipio=" + municipio + ", provincia=" + provincia + ", codigoBarras=" + codigoBarras + ", codigoBarrasNumerico=" + codigoBarrasNumerico
				+ ", codigoControl=" + codigoControl + ", codigoPostal=" + codigoPostal + ", claveTasa=" + claveTasa + ", nombreClaveTasa=" + nombreClaveTasa + ", importeTasa=" + importeTasa
				+ ", cantidadTasa=" + cantidadTasa + ", totalNumeroClaves=" + totalNumeroClaves + ", totalImporte=" + totalImporte + ", numeroJustificante=" + numeroJustificante + ", diaIngreso="
				+ diaIngreso + ", mesIngreso=" + mesIngreso + ", anyoIngreso=" + anyoIngreso + ", ciudadDeclarante=" + ciudadDeclarante + ", diaDeclarante=" + diaDeclarante + ", mesDeclarante="
				+ mesDeclarante + ", anyoDeclarante=" + anyoDeclarante + ", textoClaves=" + textoClaves + ", textoIngresoComplementario=" + textoIngresoComplementario + ", textoIngresoTesoro="
				+ textoIngresoTesoro + ", euros=" + euros + ", centimos=" + centimos + ", pagoEfectivo=" + pagoEfectivo + ", pagoCuenta=" + pagoCuenta + ", iban=" + iban + ", importe=" + importe
				+ ", textoEjemplar=" + textoEjemplar + ", numeroRegistroEmpresa=" + numeroRegistroEmpresa + "]";
	}

	
}
