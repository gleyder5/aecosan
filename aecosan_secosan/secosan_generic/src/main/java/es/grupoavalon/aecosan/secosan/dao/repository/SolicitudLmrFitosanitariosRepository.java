package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;

public interface SolicitudLmrFitosanitariosRepository extends CrudRepository<SolicitudLmrFitosanitariosEntity, Long> {

}
