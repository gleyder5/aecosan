package es.grupoavalon.aecosan.secosan.business.dto;

public class LogDTO {

	private String name;

	private String logBase64;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogBase64() {
		return logBase64;
	}

	public void setLogBase64(String logBase64) {
		this.logBase64 = logBase64;
	}

	@Override
	public String toString() {
		return "LogDTO [name=" + name + ", logBase64=" + logBase64 + "]";
	}

}
