package es.grupoavalon.aecosan.secosan.business.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.ImpresoFirmadoService;
import es.grupoavalon.aecosan.secosan.business.dto.report.SignedAnexoIIIReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.SignedAnexoIIReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.SignedAnexoIVReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.SignedLogosReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.SignedReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIIDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIIIDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoAnexoIVDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoFirmadoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoLogosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.ProductoSuspendidoDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.TipoFormularioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.report.ReportUtil;
import es.grupoavalon.aecosan.secosan.util.report.Reportable;

@Service
public class ImpresoFirmadoServiceImpl extends AbstractManager implements ImpresoFirmadoService {

	private static final String EMPTY_SPACE = ". . . . . . . . . . . . . . . . . ";
	private static final String EMPTY_SPACE_LARGE = ". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ";

	@Autowired
	private CatalogDAO catalogueDao;

	@Autowired
	private TipoFormularioDAO tipoFormularioDao;


	@Override
	public ImpresoFirmadoDTO getPDFAnexoIIToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO) {
		String fileName  = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + SecosanConstants.ANEXO_II_PDF ;

		File reporte  = new File(fileName);
		String fileToBase64 = Base64TobytesArrayTransformer.transformFileToBase64(reporte  );
		impresoFirmadoDTO.setPdfB64(fileToBase64);
		impresoFirmadoDTO.setReportName(reporte.getName());

		return impresoFirmadoDTO;
	}

	@Override
	public ImpresoFirmadoDTO getPDFToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO) {
		Map<String, Object> parameters = null;
		try {
			byte[] pdfReport = null;
			List<Reportable> listOfBeanReport = null;
			if (impresoFirmadoDTO instanceof ImpresoAnexoIIDTO) {
//				listOfBeanReport = generateListOfBeanFromAnexoIIDTO(impresoFirmadoDTO);
				return getPDFAnexoIIToSign(user,impresoFirmadoDTO);
			} else if (impresoFirmadoDTO instanceof ImpresoAnexoIIIDTO) {
				listOfBeanReport = generateListOfBeanFromAnexoIIIDTO(impresoFirmadoDTO);
			} else if (impresoFirmadoDTO instanceof ImpresoAnexoIVDTO) {
				List<ProductoSuspendidoDTO> productTable = getProductTable(impresoFirmadoDTO);
				JRBeanCollectionDataSource productsDatasource = new JRBeanCollectionDataSource(productTable);
				parameters = new HashMap<String, Object>();
				parameters.put("productsDatasource", productsDatasource);
				listOfBeanReport = generateListOfBeanFromAnexoIVDTO(impresoFirmadoDTO);
			} else if (impresoFirmadoDTO instanceof ImpresoLogosDTO) {
				String imagesPath = getImagePath();
				parameters = new HashMap<String, Object>();
				parameters.put("imagesPath", imagesPath);
				listOfBeanReport = generateListOfBeanFromLogosDTO(impresoFirmadoDTO);
			} else {
				String imagesPath = getImagePath();
				parameters = new HashMap<String, Object>();
				parameters.put("imagesPath", imagesPath);
				listOfBeanReport = generateListOfBeanFromGenericDTO(impresoFirmadoDTO);
			}

			pdfReport = generatePdfGenericReportFromListOfBean(listOfBeanReport, impresoFirmadoDTO.getFormType(),parameters);

			impresoFirmadoDTO.setPdfB64(getPdfB64ToDTO(pdfReport));
			impresoFirmadoDTO.setReportName("Impreso para firma.pdf");
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getPDFToSign | ERROR: " + e.getMessage());
		}
		return impresoFirmadoDTO;
	}


	private String getImagePath() {
		String imagesPath = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_IMAGES_PATH);
		logger.debug("getPDFToSign | imagesPath: {} ", imagesPath);
		return imagesPath;
	}

	private List<Reportable> generateListOfBeanFromAnexoIIDTO(ImpresoFirmadoDTO impresoFirmadoDTO) {
		ImpresoAnexoIIDTO report = (ImpresoAnexoIIDTO) impresoFirmadoDTO;
		List<Reportable> lista = new ArrayList<Reportable>();

		SignedAnexoIIReportDTO signedReport = new SignedAnexoIIReportDTO();
		signedReport.setIdentificationNumber(report.getIdentificationNumber());
		signedReport.setFormName(getCatalogueName(report.getFormId()));

		signedReport.setDate(report.getDate());
		signedReport.setInscriptionDate(report.getInscriptionDate());

		signedReport.setSigner(StringUtils.isNotBlank(report.getSigner()) ? report.getSigner() : EMPTY_SPACE);
		signedReport.setCompany(StringUtils.isNotBlank(report.getCompany()) ? report.getCompany() : EMPTY_SPACE);
		signedReport.setDenomination(StringUtils.isNotBlank(report.getDenomination()) ? report.getDenomination() : EMPTY_SPACE);
		signedReport.setReferenceNumber(StringUtils.isNotBlank(report.getReferenceNumber()) ? report.getReferenceNumber() : EMPTY_SPACE);

		lista.add(signedReport);
		return lista;
	}

	private List<Reportable> generateListOfBeanFromAnexoIIIDTO(ImpresoFirmadoDTO impresoFirmadoDTO) {
		ImpresoAnexoIIIDTO report = (ImpresoAnexoIIIDTO) impresoFirmadoDTO;
		List<Reportable> lista = new ArrayList<Reportable>();

		SignedAnexoIIIReportDTO signedReport = new SignedAnexoIIIReportDTO();
		signedReport.setIdentificationNumber(report.getIdentificationNumber());
		signedReport.setFormName(getCatalogueName(report.getFormId()));

		signedReport.setDate(report.getDate());
		signedReport.setMotive(StringUtils.isNotBlank(report.getMotivo()) ? report.getMotivo() : EMPTY_SPACE);

		signedReport.setSigner(StringUtils.isNotBlank(report.getSigner()) ? report.getSigner() : EMPTY_SPACE);
		signedReport.setCompany(StringUtils.isNotBlank(report.getCompany()) ? report.getCompany() : EMPTY_SPACE);
		signedReport.setDenomination(StringUtils.isNotBlank(report.getDenomination()) ? report.getDenomination() : EMPTY_SPACE);
		signedReport.setReferenceNumber(StringUtils.isNotBlank(report.getReferenceNumber()) ? report.getReferenceNumber() : EMPTY_SPACE);

		lista.add(signedReport);
		return lista;
	}

	private List<Reportable> generateListOfBeanFromAnexoIVDTO(ImpresoFirmadoDTO impresoFirmadoDTO) {
		ImpresoAnexoIVDTO report = (ImpresoAnexoIVDTO) impresoFirmadoDTO;
		List<Reportable> lista = new ArrayList<Reportable>();

		SignedAnexoIVReportDTO signedReport = new SignedAnexoIVReportDTO();
		signedReport.setCompany(StringUtils.isNotBlank(report.getCompany()) ? report.getCompany() : EMPTY_SPACE);
		signedReport.setSigner(StringUtils.isNotBlank(report.getSigner()) ? report.getSigner() : EMPTY_SPACE);
		signedReport.setAddress(StringUtils.isNotBlank(report.getAddress()) ? report.getAddress() : EMPTY_SPACE);
		signedReport.setCompanyRegNumber(StringUtils.isNotBlank(report.getCompanyRegNumber()) ? report.getCompanyRegNumber() : EMPTY_SPACE_LARGE);
		signedReport.setPersonInCharge(StringUtils.isNotBlank(report.getPersonInCharge()) ? report.getPersonInCharge() : EMPTY_SPACE);
		signedReport.setPhone(StringUtils.isNotBlank(report.getPhone()) ? report.getPhone() : EMPTY_SPACE);
		signedReport.setMail(StringUtils.isNotBlank(report.getMail()) ? report.getMail() : EMPTY_SPACE);
		signedReport.setComunicationDate(report.getComunicationDate());
		signedReport.setComunicantionType(StringUtils.isNotBlank(report.getComunicantionType()) ? report.getComunicantionType() : EMPTY_SPACE);



		if (StringUtils.equals(report.getComunicantionType(), SecosanConstants.COMUNICATION_TYPE_SUSPENSION)) {
			signedReport.setSuspension("X");
			signedReport.setMotive(StringUtils.isNotBlank(report.getMotive()) ? report.getMotive() : EMPTY_SPACE);
		} else if (StringUtils.isNotBlank(report.getComunicantionType())) {
			signedReport.setBaja("X");
			signedReport.setMotive("");
		}

		signedReport.setProducts(report.getProducts());

		lista.add(signedReport);
		return lista;
	}

	private List<ProductoSuspendidoDTO> getProductTable(ImpresoFirmadoDTO impresoFirmadoDTO) {
		ImpresoAnexoIVDTO report = (ImpresoAnexoIVDTO) impresoFirmadoDTO;
		return report.getProducts();
	}

	private List<Reportable> generateListOfBeanFromLogosDTO(ImpresoFirmadoDTO impresoFirmadoDTO) {
		ImpresoLogosDTO report = (ImpresoLogosDTO) impresoFirmadoDTO;
		List<Reportable> lista = new ArrayList<Reportable>();

		SignedLogosReportDTO signedReport = new SignedLogosReportDTO();
		signedReport.setDate(report.getDate());
		signedReport.setIdentificationNumber(report.getIdentificationNumber());

		signedReport.setNombreSolicitante(StringUtils.isNotBlank(report.getNombreSolicitante()) ? report.getNombreSolicitante() : EMPTY_SPACE);
		signedReport.setDireccionSolicitante(StringUtils.isNotBlank(report.getDireccionSolicitante()) ? report.getDireccionSolicitante() : EMPTY_SPACE);
		signedReport.setNifSolicitante(StringUtils.isNotBlank(report.getNifSolicitante()) ? report.getNifSolicitante() : EMPTY_SPACE);
		signedReport.setTelefonoSolicitante(StringUtils.isNotBlank(report.getTelefonoSolicitante()) ? report.getTelefonoSolicitante() : EMPTY_SPACE);
		signedReport.setMailSolicitante(StringUtils.isNotBlank(report.getMailSolicitante()) ? report.getMailSolicitante() : EMPTY_SPACE);
		signedReport.setPaisSolicitante(StringUtils.isNotBlank(report.getPaisSolicitante()) ? report.getPaisSolicitante() : EMPTY_SPACE);

		signedReport.setNombreRepresentante(StringUtils.isNotBlank(report.getNombreRepresentante()) ? report.getNombreRepresentante() : EMPTY_SPACE);
		signedReport.setDireccionRepresentante(StringUtils.isNotBlank(report.getDireccionRepresentante()) ? report.getDireccionRepresentante() : EMPTY_SPACE);
		signedReport.setNifRepresentante(StringUtils.isNotBlank(report.getNifRepresentante()) ? report.getNifRepresentante() : EMPTY_SPACE);
		signedReport.setTelefonoRepresentante(StringUtils.isNotBlank(report.getTelefonoRepresentante()) ? report.getTelefonoRepresentante() : EMPTY_SPACE);
		signedReport.setMailRepresentante(StringUtils.isNotBlank(report.getMailRepresentante()) ? report.getMailRepresentante() : EMPTY_SPACE);
		signedReport.setPaisRepresentante(StringUtils.isNotBlank(report.getPaisRepresentante()) ? report.getPaisRepresentante() : EMPTY_SPACE);

		signedReport.setMaterial(StringUtils.isNotBlank(report.getMaterial()) ? report.getMaterial() : "");
		signedReport.setFinalidad(StringUtils.isNotBlank(report.getFinalidad()) ? report.getFinalidad() : "");

		lista.add(signedReport);
		return lista;
	}

	private byte[] generatePdfGenericReportFromListOfBean(List<Reportable> listOfBeanReport, Long formType, Map<String, Object> parameters) throws JRException {

		String template = null;
		if (formType != null) {
			template = tipoFormularioDao.getSignByFormId(formType);
		}
		String reportToSign = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + template;

		ReportUtil reportUtil = new ReportUtil(reportToSign);
		return reportUtil.generatePdfFromCollectionBean(listOfBeanReport, parameters);
	}

	private List<Reportable> generateListOfBeanFromGenericDTO(ImpresoFirmadoDTO report) {
		List<Reportable> lista = new ArrayList<Reportable>();
		SignedReportDTO signedReport = new SignedReportDTO();
		signedReport.setIdentificationNumber(report.getIdentificationNumber());
		signedReport.setFormName(getCatalogueName(report.getFormType()));
		signedReport.setDate(report.getDate());
		lista.add(signedReport);
		return lista;
	}

	private String getCatalogueName(long id) {
		String catalogueName;
		String catalogueNameRepo = "TipoFormularioRepository";
		AbstractCatalogEntity formularioCatalogue = this.catalogueDao.getCatalogItem(catalogueNameRepo, id);
		if (formularioCatalogue != null) {
			catalogueName = formularioCatalogue.getCatalogValue();
		} else {
			throw new ServiceException("Solicitude type not found, can not create sign pdf:" + id);
		}
		return catalogueName;
	}

	private String getPdfB64ToDTO(byte[] pdfReport) {
		Base64TobytesArrayTransformer base = new Base64TobytesArrayTransformer();
		return (String) base.transform(pdfReport);
	}

}
