package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.*;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import java.util.Date;

@Entity
@Table(name = TableNames.TABLA_USUARIO_SOLICITANTES)
public class SolicitanteEntity extends AbstractPersonAddressExtendedData {

	@Id
	@GeneratedValue(generator = SequenceNames.SOLICITANTE_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.SOLICITANTE_SEQUENCE, sequenceName = SequenceNames.SOLICITANTE_SEQUENCE, allocationSize = 1)
	private Long id;

	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_PERSONA_ID", nullable = false)
	private TipoPersonaEntity personType;

	@Column(name = "FECHA_NAC", nullable = true)
	private Date fechaNacimiento;


	@Transient
	private Long fechaNacString;

	public Long getFechaNacString() {
		if(fechaNacimiento!=null){
			return fechaNacimiento.getTime();
		}
		return null;
	}

	public void setFechaNacString(Long fechaNacString) {
		this.fechaNacString= fechaNacString;
		this.fechaNacimiento = new Date(fechaNacString);
	}


	public TipoPersonaEntity getPersonType() {
		return personType;
	}

	public void setPersonType(TipoPersonaEntity personType) {
		this.personType = personType;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((personType == null) ? 0 : personType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitanteEntity other = (SolicitanteEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (personType == null) {
			if (other.personType != null) {
				return false;
			}
		} else if (!personType.equals(other.personType)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SolicitanteEntity [id=" + id + ", " + super.toString() + ",  personType=" + personType + "]";
	}



}
