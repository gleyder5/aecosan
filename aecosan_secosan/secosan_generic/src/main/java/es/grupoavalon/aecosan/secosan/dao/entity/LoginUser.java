package es.grupoavalon.aecosan.secosan.dao.entity;

import java.sql.Timestamp;

public interface LoginUser {

	Long getId();

	String getLogin();

	String getName();

	String getLastName();

	String getSecondLastName();

	String getIdentificationNumber();

	String getPassword();

	String getEmail();

	void setPassword(String password);

	Timestamp getLastLogin();
	
	Timestamp getBlockedTime();
	
	Integer getFailedLoginAttempts();

	void increaseBlockAttempt();

	void blockUser();

	Boolean getUserBlocked();

	void setLastSuccessfullLoginNow();

	void resetFailedLoginAttemps();

	void unBlockUser();

	boolean isFirstLogin();

}
