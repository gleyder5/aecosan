package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.ServicioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.ServicioRepository;

@Repository
public class ServicioDAOImpl extends GenericDao implements ServicioDAO {

	@Autowired
	private ServicioRepository servicioRepository;

	@Override
	public List <ServicioEntity> findServiceByPayTypeId(Long payTypeId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("payType", payTypeId);
		return servicioRepository.findByNamedQuery(NamedQueriesLibrary.GET_SERVICES_BY_PAY_TYPE_ID, queryParams);
	}
}
