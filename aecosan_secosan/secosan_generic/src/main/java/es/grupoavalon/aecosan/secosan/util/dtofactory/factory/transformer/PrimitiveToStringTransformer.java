package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.TransformerException;

public class PrimitiveToStringTransformer extends CustomValueTransformer {

	@Override
	public Object transform(Object input) {
		String transformedValue = null;
		if (input != null) {
			try {
				transformedValue = input.toString();
			} catch (Exception e) {
				throw new TransformerException(
						TransformerException.GENERAL_ERROR, e);
			}
		}
		return transformedValue;
	}

	@Override
	public Object reverseTransform(Object input, Class<?> outputType) {
		Object output = null;
		if (input != null) {
			try {
				output = transformFromString(input, outputType);
			} catch (Exception e) {
				handleException(e);
			}
		}

		return output;
	}

}
