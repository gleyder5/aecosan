package es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions;

import java.io.File;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;

@JsonIgnoreProperties(value = { "file" })
public class InstructionsFileDTO extends AbstractCatalogDto {

	private File file;

	private String base64;

	@JsonIgnore
	private File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
		this.base64 = Base64TobytesArrayTransformer.transformFileToBase64(file);
	}


	public void setBase64(String base64) {
		this.base64 = base64;
	}


	public String getBase64() {
		return base64;
	}

	@Override
	public String toString() {
		return "FicheroInstruccionesDTO [id=" + id + "]";
	}

}
