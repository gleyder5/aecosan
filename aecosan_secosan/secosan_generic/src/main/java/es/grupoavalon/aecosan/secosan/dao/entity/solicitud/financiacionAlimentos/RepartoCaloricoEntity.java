package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_REPARTO_CALORICO)
public class RepartoCaloricoEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.REPARTO_CALORICO)
	@SequenceGenerator(name = SequenceNames.REPARTO_CALORICO, sequenceName = SequenceNames.REPARTO_CALORICO, allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	@Column(name = "PROTEINAS")
	private String protein;

	@Column(name = "LIPIDOS")
	private String lipids;

	@Column(name = "HIDRATOS_CARBONO")
	private String carbohydrates;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProtein() {
		return protein;
	}

	public void setProtein(String protein) {
		this.protein = protein;
	}

	public String getLipids() {
		return lipids;
	}

	public void setLipids(String lipids) {
		this.lipids = lipids;
	}

	public String getCarbohydrates() {
		return carbohydrates;
	}

	public void setCarbohydrates(String carbohydrates) {
		this.carbohydrates = carbohydrates;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((carbohydrates == null) ? 0 : carbohydrates.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lipids == null) ? 0 : lipids.hashCode());
		result = prime * result + ((protein == null) ? 0 : protein.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepartoCaloricoEntity other = (RepartoCaloricoEntity) obj;
		if (carbohydrates == null) {
			if (other.carbohydrates != null)
				return false;
		} else if (!carbohydrates.equals(other.carbohydrates))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lipids == null) {
			if (other.lipids != null)
				return false;
		} else if (!lipids.equals(other.lipids))
			return false;
		if (protein == null) {
			if (other.protein != null)
				return false;
		} else if (!protein.equals(other.protein))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "RepartoCaloricoEntity [id=" + id + ", protein=" + protein + ", lipids=" + lipids + ", carbohydrates=" + carbohydrates + "]";
	}

}
