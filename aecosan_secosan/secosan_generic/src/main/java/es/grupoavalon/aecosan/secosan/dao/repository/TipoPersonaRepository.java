package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;

public interface TipoPersonaRepository extends CrudRepository<TipoPersonaEntity, Long>{

}
