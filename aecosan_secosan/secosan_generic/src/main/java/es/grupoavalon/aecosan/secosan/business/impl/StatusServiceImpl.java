package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.StatusService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;

@Service
public class StatusServiceImpl extends AbstractManager implements StatusService {

	@Autowired
	private EstadoSolicitudDAO statusDAO;

	@Override
	public List<EstadoSolicitudDTO> getStatusByAreaId(String areaId) {
		List<EstadoSolicitudDTO> listStatusDTO = null;
		try {
			Long idArea = Long.valueOf(areaId);
			List<EstadoSolicitudEntity> listStatusEntity = statusDAO.getStatusByAreaId(idArea);

			listStatusDTO = transformEntityListToDTOList(listStatusEntity, EstadoSolicitudEntity.class, EstadoSolicitudDTO.class);

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getStatusByAreaId | areaId: " + areaId);
		}

		return listStatusDTO;
	}

}
