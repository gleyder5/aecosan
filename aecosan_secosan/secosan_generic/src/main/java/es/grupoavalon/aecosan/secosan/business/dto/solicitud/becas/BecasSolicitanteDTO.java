package es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas;

public class BecasSolicitanteDTO implements    Comparable<BecasSolicitanteDTO>{
    private Integer id;
    private String nombre;
    private String apellido;
    private String DNI;
    private Double score;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int compareTo(BecasSolicitanteDTO o) {
        // ascending
//        return(this.score - o.score);
        // descending
        return(o.score.compareTo(this.score));

    }
}
