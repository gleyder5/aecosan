package es.grupoavalon.aecosan.secosan.business.dto;

public class IdentificacionPeticionDTO {

	private String identificationRequest;

	public String getIdentificationRequest() {
		return identificationRequest;
	}

	public void setIdentificationRequest(String identificationRequest) {
		this.identificationRequest = identificationRequest;
	}

	@Override
	public String toString() {
		return "IdentificacionPeticionDTO [identificationRequest=" + identificationRequest + "]";
	}

}
