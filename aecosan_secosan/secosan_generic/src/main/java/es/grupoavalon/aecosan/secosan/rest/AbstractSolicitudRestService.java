package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.SolicitudDTOReadWrapper;

public abstract class AbstractSolicitudRestService {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected BusinessFacade bfacade;

	@Autowired
	protected ResponseFactory responseFactory;

	protected Response addSolicitud(String user, SolicitudDTO solicitudDTO) {
		Response response = null;
		try {
			logger.debug("-- Method addSolicitud POST --");
			bfacade.addSolicitud(user, solicitudDTO);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			logger.error("Error addSolicitud: |" + solicitudDTO + " | " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	protected Response updateSolicitud(String user, SolicitudDTO solicitudDTO) {
		Response response = null;
		try {
			logger.debug("-- Method updateSolicitud PUT --");
			bfacade.updateSolicitud(user, solicitudDTO);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			logger.error("Error updateSolicitud: |" + solicitudDTO + " | " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	protected Response deleteSolicitud(String user, String id) {
		Response response = null;
		try {
			logger.debug("-- Method deleteSolicitud DELETE --");
			bfacade.deleteSolicitud(user, id);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			logger.error("Error deleteSolicitud: |" + id + " | " + e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

	protected Response findSolicitudById(String user, String id) {
		Response response = null;
		SolicitudDTOReadWrapper solicitudDTO;
		try {
			logger.debug("-- Method findSolicitudById || user: {} || id:{} ", user,id);

			solicitudDTO = bfacade.findSolicitudById(user, id);

			response = responseFactory.generateOkGenericResponse(solicitudDTO);

		} catch (Exception e) {
			logger.error("Error findSolicitudById || user: {} || id: {} ERROR: {}", user, id , e.getMessage());
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}

}
