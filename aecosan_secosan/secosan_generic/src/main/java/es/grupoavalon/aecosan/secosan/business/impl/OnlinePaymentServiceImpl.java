package es.grupoavalon.aecosan.secosan.business.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PostConstruct;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TasaModeloDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.connector.epago.EpagoConnectorApi;
import es.grupoavalon.aecosan.connector.epago.dto.DatosPagoInDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierAndSignOriginResponseDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetJustifierRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoGetSignOriginRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentRequestDTO;
import es.grupoavalon.aecosan.connector.epago.dto.EpagoSendPaymentResponseDTO;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorException;
import es.grupoavalon.aecosan.connector.epago.exception.EpagoConnectorOperationException;
import es.grupoavalon.aecosan.connector.epago.impl.EpagoConnectorApiImpl;
import es.grupoavalon.aecosan.connector.epago.util.DocumentType;
import es.grupoavalon.aecosan.connector.epago.util.PaymentType;
import es.grupoavalon.aecosan.secosan.business.OnlinePaymentService;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.ImpresoJustificantePagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierAccountRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierCreditCardRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.AccountPaymentDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.CreditCardPaymentDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentAddressDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentLocationDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentResponseDTO;
import es.grupoavalon.aecosan.secosan.business.exception.EpaymentException;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.PaymentDAO;
import es.grupoavalon.aecosan.secosan.dao.TasaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UbicacionGeograficaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.DatosPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.DireccionExtendidaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.FormaPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.IdentificadorPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.PdfUtil;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.report.Reportable;

@Component
@Transactional
public class OnlinePaymentServiceImpl extends AbstractManager implements OnlinePaymentService {

	private String pasarelaUrl;
	private int organismo;
	private String template791;
	private String reportToSign;
	@Autowired
	private PaymentDAO paymentDao;

	@Autowired
	private TasaDAO tasaDAO;

	@Autowired
	private ConnectorMonitoringService monitoringService;

	@PostConstruct
	public void instanciateParams() {
		this.pasarelaUrl = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_PASARELA_EPAGOS_URL);
		this.organismo = Properties.getInt(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_PASARELA_EPAGOS_AECOSAN_ID);
		this.template791 = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.TEMPLATE_MODELO_791);
		this.reportToSign = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + template791;
	}

	@Override
	public JustifierResponseDTO getPaymentJustifier(JustifierRequestDTO justifierRequest) {
		JustifierResponseDTO response = null;

		try {
			EpagoConnectorApi epagoConnector = getEpagoConnectorApiInstance();
			EpagoGetJustifierAndSignOriginRequestDTO connectorRequestPayload = instanciateGetJustifierAndSignDTOFromRequestDTO(justifierRequest);
			logger.debug("payment justifier request dto: {}" + connectorRequestPayload);
			EpagoGetJustifierAndSignOriginResponseDTO connectorResponse = epagoConnector.getJustifierAndSignOrigin(connectorRequestPayload);
			logger.debug("payment dto: {}", connectorResponse);
			response = getJustifierResponseFromConnectorResponse(connectorResponse);
		} catch (Exception e) {
			this.monitoringService.monitoringError(MonitorizableConnectors.EPAGO, e);
			this.handleException(e);
		}

		return response;
	}

	@Override
	public JustificantePagoReportDTO getJustificantePagoPDF(String identificadorPeticion,Long serviceId) {
		JustificantePagoReportDTO justificantePagoReportDTO = new JustificantePagoReportDTO();
		List<Reportable> listOfBeanReport;
		try {
			JustificantePagoDTO justificantePagoDTO = getJustificantePagoDTO(identificadorPeticion);
			String codeBar = generateCodeforImageCodeBar(justificantePagoDTO.getIdentificadorPeticion());
			if(serviceId!=null){ // if it is a service, look for the TasaModelo of the service on SECO_SERVICE table using serviceId
				DtoEntityFactory<TasaModeloEntity,TasaModeloDTO> tasaModeloEntityFactory = DtoEntityFactory.getInstance(TasaModeloEntity.class, TasaModeloDTO.class);
				TasaModeloEntity tasaModeloEntity = getTasaModeloEntityBySerivicio(serviceId);
				TasaModeloDTO tasaModeloDTO = tasaModeloEntityFactory.generateDTOFromEntity(tasaModeloEntity);
				justificantePagoDTO.getFormType().setTasaModelo(tasaModeloDTO);
			}
			listOfBeanReport = generateListOfBean(justificantePagoDTO, codeBar);
			byte[] pdfReport = PdfUtil.generatePdfGenericReportFromListOfBean(listOfBeanReport, reportToSign, logger);
			justificantePagoReportDTO.setPdfB64(PdfUtil.getPdfB64ToDTO(pdfReport));
			justificantePagoReportDTO.setReportName("JustificantePago.pdf");
			updateDataPayment(justificantePagoDTO.getIdentificadorPeticion(), codeBar);

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getJustificantePagoPDF | ERROR: " + e.getMessage());
		}

		logger.debug("getJustificantePagoPDF || identificadorPeticion: {}" , identificadorPeticion);

		return justificantePagoReportDTO;
	}

	@Override
	public JustificantePagoDTO getJustificantePagoBySolicitudeID(String identificadorPeticion) {
		JustificantePagoDTO justificantePagoDTO = new JustificantePagoDTO();

		try {
			logger.debug("getJustificantePagoBySolicitudeID || identificadorPeticion: {0}" , identificadorPeticion);
			PagoSolicitudEntity pagoSolicitudEntity = paymentDao.getPagoSolicitudEntityByPetitionNumber(identificadorPeticion);
			logger.debug("getJustificantePagoBySolicitudeID || pagoSolicitudEntity {}", pagoSolicitudEntity);
			justificantePagoDTO = getJustificantePagoDTO(identificadorPeticion);

			logger.debug("getJustificantePagoBySolicitudeID || justificantePagoDTO: {}", justificantePagoDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getJustificantePagoBySolicitudeID | identificadorPeticion: " + identificadorPeticion + " ERROR: " + e.getMessage());
		}
		return justificantePagoDTO;
	}

	private void updateDataPayment(String identificadorPeticion, String codeBar) {
		PagoSolicitudEntity pagoSolicitudEntity = paymentDao.getPagoSolicitudEntityByPetitionNumber(identificadorPeticion);
		pagoSolicitudEntity.setBarCode(codeBar);
		paymentDao.updatePayment(pagoSolicitudEntity);
	}

	private JustificantePagoDTO getJustificantePagoDTO(String identificadorPeticion) {
		JustificantePagoDTO justificantePagoDTO = null;
		PagoSolicitudEntity pagoSolicitudEntity = paymentDao.getPagoSolicitudEntityByPetitionNumber(identificadorPeticion);
		if (pagoSolicitudEntity != null) {
			justificantePagoDTO = transformEntityToDTO(pagoSolicitudEntity, JustificantePagoDTO.class);
			logger.debug("getJustificantePagoDTO || pagoSolicitudEntity: {}", pagoSolicitudEntity);
		}
		return justificantePagoDTO;
	}

	private List<Reportable> generateListOfBean(JustificantePagoDTO justificantePago, String codeBar) throws IOException {
		logger.debug("generateListOfBean | justificantePago {}", justificantePago);

		List<Reportable> lista = new ArrayList<Reportable>();

		ImpresoJustificantePagoDTO report = new ImpresoJustificantePagoDTO();

		report.setCodigoBarrasNumerico(codeBar);
		report.setCodigoBarras(PdfUtil.generateBarCodeImage(codeBar));
		report.setCodigo(getCodigo(justificantePago.getIdentificadorPeticion()));

		report.setNombreAgencia(Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_AECOSAN_NAME));

		report.setDireccion(justificantePago.getIdPayment().getExtendedAddress().getStreetName());
		report.setNumero(justificantePago.getIdPayment().getExtendedAddress().getNumber());
		report.setEscalera(justificantePago.getIdPayment().getExtendedAddress().getStair());
		report.setPiso(justificantePago.getIdPayment().getExtendedAddress().getStory());
		report.setPuerta(justificantePago.getIdPayment().getExtendedAddress().getDoor());
		report.setTipoCalle(justificantePago.getIdPayment().getExtendedAddress().getStreetAvenue());

		setReportLocationFields(report, justificantePago);

		report.setTelefono(justificantePago.getIdPayment().getTelephoneNumber());

		report.setApellidos(getUserName(justificantePago));
		report.setNif(justificantePago.getIdPayment().getIdentificationNumber());

		Long paymentDateLong = justificantePago.getDataPayment().getPaymentDate();
		if (paymentDateLong != null) {
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };

			Date paymentDate = new Date(paymentDateLong);
			Calendar cal = Calendar.getInstance();
			cal.setTime(paymentDate);
			report.setDiaDeclarante(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
			report.setMesDeclarante(meses[cal.get(Calendar.MONTH)]);
			report.setAnyoDeclarante(String.valueOf(cal.get(Calendar.YEAR)));

		}
		report.setDiaIngreso("");
		report.setMesIngreso("");
		report.setAnyoIngreso("");

		report.setImporte(justificantePago.getAmount());

		String[] importe = SecosanUtil.getSeparateAmount(justificantePago.getAmount());
		report.setEuros(importe[0]);
		report.setCentimos(importe[1]);

		report.setImporteTasa(String.valueOf(justificantePago.getFormType().getTasaModelo().getAmmount()));

		report.setCantidadTasa(String.valueOf(justificantePago.getPayedTasas()));

		reportSetTipoPago(report, justificantePago);

		report.setClaveTasa(justificantePago.getFormType().getTasaModelo().getTasaCode());
		report.setNombreClaveTasa(justificantePago.getFormType().getTasaModelo().getCatalogValue());
		report.setTextoDescripcionTasa(justificantePago.getFormType().getTasaModelo().getCatalogValue());
		report.setTextoClaves(justificantePago.getFormType().getTasaModelo().getCatalogValue());

		report.setEjercicio(SecosanUtil.sysDate(SecosanUtil.DATE_FORMAT_YYYY));

		report.setIban(justificantePago.getDataPayment().getAccount());

		report.setTextoNumRegistro(justificantePago.getDataPayment().getNrc());
		report.setNumeroJustificante(justificantePago.getDataPayment().getNrc());

		report.setTextoIngresoComplementario("");
		report.setTextoIngresoTesoro("");
		report.setTotalImporte(justificantePago.getAmount());
		logger.debug("generateListOfBean | ImpresoJustificantePagoDTO (report): {}", report);

		lista.add(report);
		return lista;
	}

	private String getUserName(JustificantePagoDTO justificantePagoDTO) {
		StringBuilder user = new StringBuilder();

		if (justificantePagoDTO.getIdPayment().getLastname() != null) {
			user.append(justificantePagoDTO.getIdPayment().getLastname() + " ");
		}
		if (justificantePagoDTO.getIdPayment().getSecondlastName() != null) {
			user.append(justificantePagoDTO.getIdPayment().getSecondlastName() + " ");
		}
		if (justificantePagoDTO.getIdPayment().getName() != null) {
			user.append(justificantePagoDTO.getIdPayment().getName());
		}
		return user.toString();
	}

	private void reportSetTipoPago(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getDataPayment() != null && impresoFirmadoDTO.getDataPayment().getExpirationCardDate() != null) {
			report.setPagoCuenta("");
			report.setPagoEfectivo(SecosanConstants.CHECKED);
		} else {
			report.setPagoCuenta(SecosanConstants.CHECKED);
			report.setPagoEfectivo("");
		}
	}

	private void setReportLocationFields(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getIdPayment().getLocation() != null) {
			setReportLocationCodigoMunicipio(report, impresoFirmadoDTO);
			setReportLocationProvincia(report, impresoFirmadoDTO);
			setReportLocationCodigoPostal(report, impresoFirmadoDTO);
		}
	}

	private void setReportLocationCodigoMunicipio(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getIdPayment().getLocation().getMunicipio() != null) {
			report.setMunicipio(impresoFirmadoDTO.getIdPayment().getLocation().getMunicipio().getCatalogValue());
			setReportLocationCiudadDeclarante(report, impresoFirmadoDTO);
		} else {
			report.setMunicipio("");
		}

	}

	private void setReportLocationCiudadDeclarante(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getIdPayment().getLocation().getMunicipio().getLocalidad() != null) {
			report.setCiudadDeclarante(impresoFirmadoDTO.getIdPayment().getLocation().getMunicipio().getLocalidad());
		} else {
			report.setCiudadDeclarante("");
		}
	}

	private void setReportLocationProvincia(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getIdPayment().getLocation().getProvince() != null) {
			report.setProvincia(impresoFirmadoDTO.getIdPayment().getLocation().getProvince().getCatalogValue());
		} else {
			report.setProvincia("");
		}
	}

	private void setReportLocationCodigoPostal(ImpresoJustificantePagoDTO report, JustificantePagoDTO impresoFirmadoDTO) {
		if (impresoFirmadoDTO.getIdPayment().getLocation().getCodigoPostal() != null) {
			report.setCodigoPostal(impresoFirmadoDTO.getIdPayment().getLocation().getCodigoPostal());
		} else {
			report.setCodigoPostal("");
		}
	}

	private String getCodigo(String identificadoPeticion) {
		String code = "606";
		if (isRiesgosNutricionales(identificadoPeticion)) {
			code += "607";
		}
		return code;
	}

	private String generateCodeforImageCodeBar(String identificadoPeticion) {
		String code = "971";
		if (isRiesgosNutricionales(identificadoPeticion)) {
			code += "607";
		} else {
			code += "606";
		}
		code += identificadoPeticion;
		code += "2";
		return code;
	}

	private boolean isRiesgosNutricionales(String identificadoPeticion) {
		return (identificadoPeticion.subSequence(identificadoPeticion.length() - 1, identificadoPeticion.length())).equals(SecosanConstants.RIESGOS_NUTRICIONALES_FORMS);
	}

	private static EpagoConnectorApi getEpagoConnectorApiInstance() {
		return new EpagoConnectorApiImpl();
	}

	private EpagoGetJustifierAndSignOriginRequestDTO instanciateGetJustifierAndSignDTOFromRequestDTO(JustifierRequestDTO justifierRequest) {
		EpagoGetJustifierAndSignOriginRequestDTO epagoJustifierAndSignRequest = new EpagoGetJustifierAndSignOriginRequestDTO();
		EpagoGetJustifierRequestDTO justifierConnectorRequest = instanciateEpagoGetJustifierRequestDTO(justifierRequest);
		EpagoGetSignOriginRequestDTO signOriginConnectorRequest = instanciateEpagoGetSignOriginRequestDTO(justifierRequest);
		epagoJustifierAndSignRequest.setGetJustifierRequestDTO(justifierConnectorRequest);
		epagoJustifierAndSignRequest.setGetSignOriginRequestDTO(signOriginConnectorRequest);
		return epagoJustifierAndSignRequest;
	}

	private EpagoGetJustifierRequestDTO instanciateEpagoGetJustifierRequestDTO(JustifierRequestDTO justifierRequestDTO) {
		EpagoGetJustifierRequestDTO justifierRequest = new EpagoGetJustifierRequestDTO();
		justifierRequest.setUrl(pasarelaUrl);
		justifierRequest.setIdOrganismo(organismo);
		this.setCodigoTasaAndModeloToJustifierRequest(justifierRequestDTO, justifierRequest);
		return justifierRequest;
	}

	private void setCodigoTasaAndModeloToJustifierRequest(JustifierRequestDTO justifierRequestDTO, EpagoGetJustifierRequestDTO justifierRequestEpago) {
		TasaModeloEntity tasaModelo = new TasaModeloEntity();
		if (justifierRequestDTO.getFormType() != null && justifierRequestDTO.getServiceType() == null) {
			tasaModelo = getTasaModeloEntityByFormulario(justifierRequestDTO.getFormType());
		} else if (justifierRequestDTO.getServiceType() != null) {
			tasaModelo = getTasaModeloEntityBySerivicio(justifierRequestDTO.getServiceType());
		}

		justifierRequestEpago.setCodigoTasa(tasaModelo.getTasaCode());
		justifierRequestEpago.setModelo(tasaModelo.getModel());
	}

	private TasaModeloEntity getTasaModeloEntityByFormulario(long typeFormId) {
		TasaModeloEntity tasaModelo = this.tasaDAO.getTasaByFormularioId(typeFormId);
		if (tasaModelo == null) {
			throw new ServiceException("Can not obtain tasa and model from formulario with id:" + typeFormId);
		}
		return tasaModelo;
	}

	private TasaModeloEntity getTasaModeloEntityBySerivicio(long typeServiceId) {
		TasaModeloEntity tasaModelo = this.tasaDAO.getTasaBySerivicioId(typeServiceId);
		if (tasaModelo == null) {
			throw new ServiceException("Can not obtain tasa and model from formulario with service id:" + typeServiceId);
		}
		return tasaModelo;
	}

	private static EpagoGetSignOriginRequestDTO instanciateEpagoGetSignOriginRequestDTO(JustifierRequestDTO justifierRequest) {
		EpagoGetSignOriginRequestDTO signConnectorRequest = new EpagoGetSignOriginRequestDTO();
		if (justifierRequest.getAccountPaymentData() != null) {
			setAccountDataIntoIntoEpagoGetSignOriginRequestDTO(justifierRequest.getAccountPaymentData(), signConnectorRequest);
		} else if (justifierRequest.getCreditCardPaymentData() != null) {
			setCreditcarDataIntoEpagoGetSignOriginRequestDTO(justifierRequest.getCreditCardPaymentData(), signConnectorRequest);
		}
		signConnectorRequest.setImporte(justifierRequest.getAmmount());
		signConnectorRequest.setDocumentoObligado(justifierRequest.getPersonIdentificationNumber());

		return signConnectorRequest;
	}

	private static void setAccountDataIntoIntoEpagoGetSignOriginRequestDTO(JustifierAccountRequestDTO justifierRequestAccount, EpagoGetSignOriginRequestDTO signConnectorRequest) {
		signConnectorRequest.setCodigoBanco(justifierRequestAccount.getIbanBankIdentifier());
		signConnectorRequest.setCcc(justifierRequestAccount.getAccountNumberDecoded());
	}

	private static void setCreditcarDataIntoEpagoGetSignOriginRequestDTO(JustifierCreditCardRequestDTO justifierRequestCreditCard, EpagoGetSignOriginRequestDTO signConnectorRequest) {
		signConnectorRequest.setNumeroTarjeta(justifierRequestCreditCard.getCardNumberDecoded());
		signConnectorRequest.setCodigoBanco(justifierRequestCreditCard.getCardIssuerCode());
		GregorianCalendar cal = getCalendarDateFromLong(justifierRequestCreditCard.getExpirationDate());
		signConnectorRequest.setFechaCaducidadTarjeta(cal);
	}

	private JustifierResponseDTO getJustifierResponseFromConnectorResponse(EpagoGetJustifierAndSignOriginResponseDTO connectorResponse) {
		JustifierResponseDTO responseDTO = new JustifierResponseDTO();
		responseDTO.setJustifierNumber(connectorResponse.getJustificante());
		responseDTO.setSignData(connectorResponse.getOrigenFirma());
		return responseDTO;
	}

	@Override
	public OnlinePaymentResponseDTO makePayment(OnlinePaymentRequestDTO paymentRequest) {
		OnlinePaymentResponseDTO paymentResponse = null;

		try {
			EpagoConnectorApi epagoConnector = getEpagoConnectorApiInstance();
			EpagoSendPaymentRequestDTO epagoRequest = instanciateEpagoSendPaymentRequestDTOFromRequest(paymentRequest);
			EpagoSendPaymentResponseDTO epagoResponse = epagoConnector.sendPayment(epagoRequest);
			monitoringService.monitoringSuccess(MonitorizableConnectors.EPAGO);
			paymentResponse = instanciateOnlinePaymentResponseDTOFromEpagoResponse(epagoResponse);
			this.registerPayment(epagoResponse, paymentRequest);
		} catch (EpagoConnectorException e) {
			this.monitoringService.monitoringError(MonitorizableConnectors.EPAGO, e);
			this.handleException(e);
		}

		return paymentResponse;
	}

	private EpagoSendPaymentRequestDTO instanciateEpagoSendPaymentRequestDTOFromRequest(OnlinePaymentRequestDTO paymentRequest) {
		EpagoSendPaymentRequestDTO epagoSendRequest = new EpagoSendPaymentRequestDTO();
		epagoSendRequest.setIdOrganismo(this.organismo);
		epagoSendRequest.setUrl(pasarelaUrl);
		DatosPagoInDTO datosPago = instanciateDatosPagosFromRequest(paymentRequest);
		epagoSendRequest.setDatosPagoIn(datosPago);
		logger.debug("payment data: {0}" , epagoSendRequest);
		return epagoSendRequest;
	}

	private DatosPagoInDTO instanciateDatosPagosFromRequest(OnlinePaymentRequestDTO paymentRequest) {
		DatosPagoInDTO datosPagos = instanciateDatosPagoInDTOWithBasicInfoIntoDatosPagoInDTO(paymentRequest);
		if (isAccountPayment(paymentRequest)) {
			setAccountDataIntoDatosPagoInDTO(paymentRequest.getAccountPaymentInfo(), datosPagos);
		} else if (isCreditCardPayment(paymentRequest)) {
			setCreditCardDataIntoDatosPagoInDTO(paymentRequest.getCreditCardPaymentInfo(), datosPagos);
		} else {
			throw new ServiceException("Payment request object missing payment method data, check if account or creditcard data is set");
		}
		this.setCodigoTasaAndModeloToPaymentRequest(paymentRequest, datosPagos);
		return datosPagos;
	}

	private void setCodigoTasaAndModeloToPaymentRequest(OnlinePaymentRequestDTO paymentRequest, DatosPagoInDTO datosPagos) {
		TasaModeloEntity tasaModelo;
		if (StringUtils.isNotBlank(paymentRequest.getFormType()) && paymentRequest.getServiceType() == null) {
			tasaModelo = getTasaModeloEntityByFormulario(Long.parseLong(paymentRequest.getFormType()));
		} else if (paymentRequest.getServiceType() != null) {
			tasaModelo = getTasaModeloEntityBySerivicio(paymentRequest.getServiceType());
		} else {
			throw new ServiceException("it is required to set the formType or the serviceType in the request");
		}
		datosPagos.setCodigoTasa(tasaModelo.getTasaCode());
	}

	private static DatosPagoInDTO instanciateDatosPagoInDTOWithBasicInfoIntoDatosPagoInDTO(OnlinePaymentRequestDTO paymentRequest) {
		DatosPagoInDTO datosPagos = new DatosPagoInDTO();
		datosPagos.setImporte(paymentRequest.getAmmount());
		datosPagos.setNombre(paymentRequest.getName());
		datosPagos.setApellido1(paymentRequest.getLastname());
		datosPagos.setApellido2(paymentRequest.getSecondlastName());
		datosPagos.setJustificante(paymentRequest.getJustifier());
		datosPagos.setFirma1(paymentRequest.getSignedJustifier());
		datosPagos.setOrigenFirma(paymentRequest.getSignedJustifierRaw());
		datosPagos.setDocumentoObligado(paymentRequest.getPersonIdentificationNumber());
		datosPagos.setTipoDocumentoObligado(DocumentType.instanciateByName(paymentRequest.getPersonIdentificationtype()));

		return datosPagos;
	}

	private static void setAccountDataIntoDatosPagoInDTO(AccountPaymentDTO accountInfo, DatosPagoInDTO datosPagos) {
		datosPagos.setTipoCargo(PaymentType.ACCOUNT_PAYMENT);
		datosPagos.setCcc(accountInfo.getAccountNumberDecoded());
		datosPagos.setCodigoBanco(accountInfo.getBankCode());
	}

	private static void setCreditCardDataIntoDatosPagoInDTO(CreditCardPaymentDTO creditCardInfo, DatosPagoInDTO datosPagos) {
		datosPagos.setTipoCargo(PaymentType.CREDIT_CARD);
		datosPagos.setNumeroTarjeta(creditCardInfo.getCreditCardDecoded());
		datosPagos.setCodigoBanco(creditCardInfo.getBankCode());
		datosPagos.setFechaCaducidadTarjeta(getCalendarDateFromLong(creditCardInfo.getExpirationDate()));
	}

	private static GregorianCalendar getCalendarDateFromLong(long dateLong) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTimeInMillis(dateLong);
		return cal;
	}

	private static OnlinePaymentResponseDTO instanciateOnlinePaymentResponseDTOFromEpagoResponse(EpagoSendPaymentResponseDTO epagoResponse) {
		OnlinePaymentResponseDTO onlinePaymentResponse = new OnlinePaymentResponseDTO();
		onlinePaymentResponse.setNcr(epagoResponse.getNrc());
		return onlinePaymentResponse;
	}

	private void registerPayment(EpagoSendPaymentResponseDTO epagoResponse, OnlinePaymentRequestDTO paymentRequest) {
		PagoSolicitudEntity pagoSoliciturEntity = instanciatePagoSolicitudEntity(epagoResponse, paymentRequest);
		this.paymentDao.savePayment(pagoSoliciturEntity);
	}

	private static PagoSolicitudEntity instanciatePagoSolicitudEntity(EpagoSendPaymentResponseDTO epagoResponse, OnlinePaymentRequestDTO paymentRequest) {
		PagoSolicitudEntity onlinePaymentRegistry = instanciatePagoSolicitudEntityWithBasicInfo(paymentRequest);
		onlinePaymentRegistry.setPayment(instanciateFormaPagoEntity(paymentRequest));
		onlinePaymentRegistry.setIdPayment(instanciateIdentificadorPagoEntity(paymentRequest));
		onlinePaymentRegistry.setDataPayment(instanciateDatosPagoEntity(epagoResponse, paymentRequest));

		return onlinePaymentRegistry;
	}

	private static PagoSolicitudEntity instanciatePagoSolicitudEntityWithBasicInfo(OnlinePaymentRequestDTO paymentRequest) {
		PagoSolicitudEntity onlinePaymentRegistry = new PagoSolicitudEntity();
		onlinePaymentRegistry.setAmount(paymentRequest.getAmmount());
		onlinePaymentRegistry.setPayedTasas(paymentRequest.getQuantity());
		onlinePaymentRegistry.setIdentificadorPeticion(paymentRequest.getPetitionIdentificator());
		onlinePaymentRegistry.setFormType(instanciateTipoFormularioForPagoSolicitudEntity(paymentRequest));
		return onlinePaymentRegistry;
	}

	private static TipoFormularioEntity instanciateTipoFormularioForPagoSolicitudEntity(OnlinePaymentRequestDTO paymentRequest) {
		TipoFormularioEntity tipoform = new TipoFormularioEntity();
		tipoform.setId(Long.parseLong(paymentRequest.getFormType()));
		return tipoform;
	}

	private static FormaPagoEntity instanciateFormaPagoEntity(OnlinePaymentRequestDTO paymentRequest) {
		FormaPagoEntity formaPago = new FormaPagoEntity();
		if (isAccountPayment(paymentRequest)) {
			formaPago.setId(SecosanConstants.PAYMENT_TYPE_ONLINE_PAYMENT_ACCOUNT_ID);
		} else if (isCreditCardPayment(paymentRequest)) {
			formaPago.setId(SecosanConstants.PAYMENT_TYPE_ONLINE_PAYMENT_CREDITCARD_ID);
		}
		return formaPago;
	}

	private static boolean isAccountPayment(OnlinePaymentRequestDTO paymentRequest) {
		boolean accountPayment = false;
		if (paymentRequest.getAccountPaymentInfo() != null) {
			accountPayment = true;
		}
		return accountPayment;
	}

	private static boolean isCreditCardPayment(OnlinePaymentRequestDTO paymentRequest) {
		boolean creditCardPayment = false;
		if (paymentRequest.getCreditCardPaymentInfo() != null) {
			creditCardPayment = true;
		}
		return creditCardPayment;
	}

	private static IdentificadorPagoEntity instanciateIdentificadorPagoEntity(OnlinePaymentRequestDTO paymentRequest) {
		IdentificadorPagoEntity identificadorPago = new IdentificadorPagoEntity();
		identificadorPago.setExtendedAddress(instanciateDireccionExtendidaEntityFromDTO(paymentRequest.getAddress()));
		setPersonDataIntoIdentificadorPagoEntity(identificadorPago, paymentRequest);
		setLocationDataIntoIdentificadorPagoEntity(identificadorPago, paymentRequest.getLocation());
		return identificadorPago;
	}

	private static void setPersonDataIntoIdentificadorPagoEntity(IdentificadorPagoEntity identificadorPago, OnlinePaymentRequestDTO paymentRequest) {
		identificadorPago.setIdentificationNumber(paymentRequest.getPersonIdentificationNumber());
		identificadorPago.setName(paymentRequest.getName());
		identificadorPago.setLastName(paymentRequest.getLastname());
		identificadorPago.setSecondLastName(paymentRequest.getSecondlastName());
		if (StringUtils.isNotBlank(paymentRequest.getTelephone())) {
			identificadorPago.setTelephoneNumber(paymentRequest.getTelephone());
		}
	}

	private static void setLocationDataIntoIdentificadorPagoEntity(IdentificadorPagoEntity identificadorPago, OnlinePaymentLocationDTO onlinePaymentLocation) {

		UbicacionGeograficaEntity entity = transformDTOToEntity(onlinePaymentLocation, UbicacionGeograficaEntity.class);
		identificadorPago.setLocation(entity);
	}

	private static DatosPagoEntity instanciateDatosPagoEntity(EpagoSendPaymentResponseDTO epagoResponse, OnlinePaymentRequestDTO paymentRequest) {
		DatosPagoEntity datosPagoEntity = new DatosPagoEntity();
		if (isAccountPayment(paymentRequest)) {
			AccountPaymentDTO accountPayment = paymentRequest.getAccountPaymentInfo();
			setAccountDataIntoDatosPagosEntity(datosPagoEntity, accountPayment);
		} else if (isCreditCardPayment(paymentRequest)) {
			CreditCardPaymentDTO creditCardPaymentDTO = paymentRequest.getCreditCardPaymentInfo();
			setCreditCardDataIntoDatosPagosEntity(datosPagoEntity, creditCardPaymentDTO);
		}
		datosPagoEntity.setPaymentDate(System.currentTimeMillis());
		datosPagoEntity.setJustifier(paymentRequest.getJustifier());
		datosPagoEntity.setNrc(epagoResponse.getNrc());
		return datosPagoEntity;
	}

	private static void setAccountDataIntoDatosPagosEntity(DatosPagoEntity datosPagoEntity, AccountPaymentDTO accountPayment) {
		datosPagoEntity.setAccount(accountPayment.getAccountNumber());
	}

	private static void setCreditCardDataIntoDatosPagosEntity(DatosPagoEntity datosPagoEntity, CreditCardPaymentDTO creditCardPaymentDTO) {
		datosPagoEntity.setCardNumber(SecosanUtil.maskCreditCard(creditCardPaymentDTO.getCreditCard()));
		datosPagoEntity.setExpirationCardDate(getExpirationCardDate(creditCardPaymentDTO));
	}

	private static Date getExpirationCardDate(CreditCardPaymentDTO cardPaymentDTO) {
		return new Date(cardPaymentDTO.getExpirationDate());
	}

	private static DireccionExtendidaEntity instanciateDireccionExtendidaEntityFromDTO(OnlinePaymentAddressDTO paymentAddress) {
		return transformDTOToEntity(paymentAddress, DireccionExtendidaEntity.class);
	}

	private void handleException(Exception e) {
		if (e instanceof EpagoConnectorOperationException) {
			EpagoConnectorOperationException ex = (EpagoConnectorOperationException) e;
			this.logger.error("error while executing a epago operation ", e);
			throw new EpaymentException("Operation exception", ex.getErrorOrigenDescription(), e);
		} else {
			super.handleException(e);
		}
	}
}
