package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;

public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Long> {

	List<UsuarioEntity> findByIdentificationNumber(String identificationNumber);

}
