package es.grupoavalon.aecosan.secosan.dao.repository.alimentos.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.CeseComercializacionAlimentosGruposRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.impl.AbstractCrudRespositoryImpl;

@Component
public class CeseComercializacionAlimentosGruposRepositoryImpl extends AbstractCrudRespositoryImpl<CeseComercializacionAGEntity, Long>
		implements CeseComercializacionAlimentosGruposRepository {

	@Override
	protected Class<CeseComercializacionAGEntity> getClassType() {
		return CeseComercializacionAGEntity.class;
	}
}
