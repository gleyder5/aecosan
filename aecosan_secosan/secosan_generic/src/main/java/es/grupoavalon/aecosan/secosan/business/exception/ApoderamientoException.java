package es.grupoavalon.aecosan.secosan.business.exception;

public class ApoderamientoException extends  ServiceException {

    private String errCode;
    private String description;

    public String getErrCode() {
        return errCode;
    }

    public String getDescription() {
        return description;
    }

    public ApoderamientoException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }


    public ApoderamientoException(String message,String errCode,String description ) {
        super(message);
        this.errCode = errCode;
        this.description = description;
    }
}
