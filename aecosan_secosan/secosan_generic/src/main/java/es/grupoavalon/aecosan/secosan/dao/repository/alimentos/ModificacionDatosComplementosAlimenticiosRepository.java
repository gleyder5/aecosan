package es.grupoavalon.aecosan.secosan.dao.repository.alimentos;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

public interface ModificacionDatosComplementosAlimenticiosRepository extends CrudRepository<ModificacionDatosCAEntity, Long> {

}
