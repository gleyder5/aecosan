package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PresentacionProductoDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("puestaMercadoId")
	@XmlElement(name = "puestaMercadoId")
	@BeanToBeanMapping(getValueFrom = "puestaMercado", getSecondValueFrom = "id")
	private Long puestaMercadoId;

	@JsonProperty("forma")
	@XmlElement(name = "forma")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "forma")
	private FormaDTO forma;

	@JsonProperty("sabores")
	@XmlElement(name = "sabores")
	@BeanToBeanMapping(getValueFrom = "sabores")
	private String sabores;

	@JsonProperty("envase")
	@XmlElement(name = "envase")
	@BeanToBeanMapping(getValueFrom = "envase")
	private String envase;

	@JsonProperty("otrasCaracteristicas")
	@XmlElement(name = "otrasCaracteristicas")
	@BeanToBeanMapping(getValueFrom = "otrasCaracteristicas")
	private String otrasCaracteristicas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPuestaMercadoId() {
		return puestaMercadoId;
	}

	public void setPuestaMercadoId(Long puestaMercadoId) {
		this.puestaMercadoId = puestaMercadoId;
	}

	public FormaDTO getForma() {
		return forma;
	}

	public void setForma(FormaDTO forma) {
		this.forma = forma;
	}

	public String getSabores() {
		return sabores;
	}

	public void setSabores(String sabores) {
		this.sabores = sabores;
	}

	public String getEnvase() {
		return envase;
	}

	public void setEnvase(String envase) {
		this.envase = envase;
	}

	public String getOtrasCaracteristicas() {
		return otrasCaracteristicas;
	}

	public void setOtrasCaracteristicas(String otrasCaracteristicas) {
		this.otrasCaracteristicas = otrasCaracteristicas;
	}

	@Override
	public String toString() {
		return "PresentacionProductoDTO [id=" + id + ", puestaMercadoId=" + puestaMercadoId + ", forma=" + forma + ", sabores=" + sabores + ", envase=" + envase + ", otrasCaracteristicas="
				+ otrasCaracteristicas + "]";
	}

}
