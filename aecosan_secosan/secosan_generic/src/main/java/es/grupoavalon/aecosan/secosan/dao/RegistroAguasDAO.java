package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;

public interface RegistroAguasDAO {
	RegistroAguasEntity addRegistroAguas(RegistroAguasEntity registroAguasEntity);

	void updateRegistroAguas(RegistroAguasEntity registroAguasEntity);

}
