package es.grupoavalon.aecosan.secosan.util.exception;

public interface HttpCodedError {

	int getHttpCodeRelated();

}
