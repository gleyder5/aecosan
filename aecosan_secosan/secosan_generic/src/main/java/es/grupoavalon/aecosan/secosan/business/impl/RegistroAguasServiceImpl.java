package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.RegistroAguasService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroAguas.RegistroAguasDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.RegistroAguasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroAguas.RegistroAguasEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class RegistroAguasServiceImpl extends AbstractSolicitudService implements RegistroAguasService {

	@Autowired
	private RegistroAguasDAO registroAguasDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO registroAguasDTO) {
		try {
			solicitudPrepareNullDTO(registroAguasDTO);
			prepareRegistroAguasNullability((RegistroAguasDTO) registroAguasDTO);
			addRegistroAguas(user, (RegistroAguasDTO) registroAguasDTO);
		} catch (

		Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + registroAguasDTO);
		}

	}

	@Override
	public void update(String user, SolicitudDTO registroAguasDTO) {
		try {
			solicitudPrepareNullDTO(registroAguasDTO);
			prepareRegistroAguasNullability((RegistroAguasDTO) registroAguasDTO);
			updateRegistroAguas(user, (RegistroAguasDTO) registroAguasDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + registroAguasDTO);
		}
	}

	private void prepareRegistroAguasNullability(RegistroAguasDTO registroAguasDTO) {
		registroAguasDTO.setPaisOrigen(SecosanUtil.checkDtoNullability(registroAguasDTO.getPaisOrigen()));
		registroAguasDTO.setSolicitudePayment(SecosanUtil.checkDtoNullability(registroAguasDTO.getSolicitudePayment()));
		if (null != registroAguasDTO.getSolicitudePayment()) {
			registroAguasDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(registroAguasDTO.getSolicitudePayment().getPayment()));
			registroAguasDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(registroAguasDTO.getSolicitudePayment().getIdPayment()));
			registroAguasDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(registroAguasDTO.getSolicitudePayment().getDataPayment()));
			if (null != registroAguasDTO.getSolicitudePayment().getIdPayment()) {
				registroAguasDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(registroAguasDTO.getSolicitudePayment().getIdPayment().getLocation()));
				prepareLocationNull(registroAguasDTO.getSolicitudePayment().getIdPayment().getLocation());
			}
		}
		if (null != registroAguasDTO) {
			registroAguasDTO.setPaisOrigen(SecosanUtil.checkDtoNullability(registroAguasDTO.getPaisOrigen()));
		}
	}

	private void addRegistroAguas(String user, RegistroAguasDTO registroAguasDTO) {
		RegistroAguasEntity registroAguasEntity = generateRegistroAguasEntityFromDTO(registroAguasDTO);
		setUserCreatorByUserId(user, registroAguasEntity);
		prepareAddSolicitudEntity(registroAguasEntity, registroAguasDTO);
		RegistroAguasEntity registroAguasEntityNew = registroAguasDAO.addRegistroAguas(registroAguasEntity);
		if(registroAguasDTO .getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroAguasEntityNew );
		}
	}

	private RegistroAguasEntity generateRegistroAguasEntityFromDTO(RegistroAguasDTO registroAguasDTO) {
		logger.debug("generateRegistroAguasEntityFromDTO || registroAguasDTO: " + registroAguasDTO);
		prepareSolicitudDTO(registroAguasDTO);
		RegistroAguasEntity registroAguasEntity = transformDTOToEntity(registroAguasDTO, RegistroAguasEntity.class);
		logger.debug("generateRegistroAguasEntityFromDTO || registroAguasEntity: " + registroAguasEntity);
		return registroAguasEntity;
	}

	private void updateRegistroAguas(String user, RegistroAguasDTO registroAguasDTO) {
		RegistroAguasEntity registroAguasEntity = transformDTOToEntity(registroAguasDTO, RegistroAguasEntity.class);
		setUserCreatorByUserId(user, registroAguasEntity);
		prepareUpdateSolicitudEntity(registroAguasEntity, registroAguasDTO);
		if(registroAguasDTO .getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroAguasEntity);
		}
		registroAguasDAO.updateRegistroAguas(registroAguasEntity);
	}
}
