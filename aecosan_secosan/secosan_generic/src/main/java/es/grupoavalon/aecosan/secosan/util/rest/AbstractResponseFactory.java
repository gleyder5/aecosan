package es.grupoavalon.aecosan.secosan.util.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.util.exception.CodedError;
import es.grupoavalon.aecosan.secosan.util.exception.HttpCodedError;
import es.grupoavalon.aecosan.secosan.util.property.MessagesPropertiesProvider;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.CodedErrorWrapper;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.ErrorWrapper;

public abstract class AbstractResponseFactory {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/**
	 * Returns a OK message with the given message
	 * 
	 * @param message
	 * @return
	 */
	public Response generateOkResponseWithMessage(String message) {
		return this.generateOkGenericResponse(message);
	}

	/**
	 * Generates a ok response no message
	 * 
	 * @return
	 */
	public Response generateOkResponse() {
		return Response.ok().build();
	}

	/**
	 * Returns a generic 200 response based on the given object
	 * 
	 * @param object
	 * @return
	 */
	public Response generateOkGenericResponse(Object object) {
		return Response.ok(object).build();
	}


	/**
	 * Returns a generic 200 response based on the given object. If the object
	 * is null, will return a empty json response ({})
	 * 
	 * @param object
	 * @return
	 */
	public Response generateOkGenericResponseNullAsEmpty(Object object) {

		if (object == null) {
			object = "{}";
		}

		return Response.ok(object).build();

	}

	/**
	 * Generates a error response in case of error
	 * 
	 * @param t
	 * @return Response
	 */
	public Response generateErrorResponse(Throwable t) {
		Response response = null;
		ErrorWrapper errorWrapper = instanciateErrorWrapperDepedingOnException(t);
		setErrorBascDataInErrorWrapper(errorWrapper, t);
		if (t instanceof HttpCodedError) {
			response = generateResponseBasedOnHTTPCode((HttpCodedError) t, errorWrapper);
		} else {
			response = generateErrowWrapperResponse(errorWrapper);
		}
		return response;
	}

	private ErrorWrapper instanciateErrorWrapperDepedingOnException(Throwable t) {
		ErrorWrapper errorWrapper;
		if (t instanceof CodedError) {
			errorWrapper = this.instanciateCodeErrorWrapper((CodedError) t);
		} else {
			errorWrapper = new ErrorWrapper();
		}
		return errorWrapper;
	}

	protected void setErrorBascDataInErrorWrapper(ErrorWrapper errorWrapper, Throwable t) {

		String errorDescription = getErrorMessageBasedOnException(t);
		errorWrapper.setErrorDescription(errorDescription);
		errorWrapper.setError(t.getClass().getSimpleName());
	}

	protected Response generateResponseBasedOnHTTPCode(HttpCodedError error, ErrorWrapper errorWrapper) {

		Status status = null;
		int code = error.getHttpCodeRelated();
		if (Response.Status.UNAUTHORIZED.getStatusCode() == code) {
			status = Response.Status.UNAUTHORIZED;
		} else if (Response.Status.FORBIDDEN.getStatusCode() == code) {
			status = Response.Status.FORBIDDEN;
		} else if (Response.Status.METHOD_NOT_ALLOWED.getStatusCode() == code) {
			status = Response.Status.METHOD_NOT_ALLOWED;
		}
		return Response.status(status).entity(errorWrapper).build();
	}

	protected Response generateErrowWrapperResponse(ErrorWrapper errorWrapper) {
		return Response.serverError().entity(errorWrapper).build();
	}

	private CodedErrorWrapper instanciateCodeErrorWrapper(CodedError e) {
		String error = e.getClass().getSimpleName();
		int code = e.getCodeError();
		String description = e.getDescription();

		CodedErrorWrapper errorWrapper = new CodedErrorWrapper();
		errorWrapper.setError(error);
		errorWrapper.setCode(code);
		errorWrapper.setErrorDescription(description);
		return errorWrapper;
	}


	protected Response generateErrorResponseFromErrorWrapper(ErrorWrapper errorWrapper) {
		return Response.serverError().entity(errorWrapper).build();
	}

	/**
	 * Generates a plain error 500 response with no description
	 * 
	 * @return
	 */
	public Response generateErrorResponse() {
		return Response.serverError().build();
	}

	private String getErrorMessageBasedOnException(Throwable t) {
		String errorDescription = null;
		String key = "default.msg";

		if (t instanceof DaoException) {
			logger.debug("Dao exception ");
			key = DaoException.class.getSimpleName();
		} else if (t instanceof ServiceException) {
			logger.debug("Service Exception");
			
				key = ServiceException.class.getSimpleName();
			
		} else if (t instanceof AutenticateException) {
			AutenticateException authEx = (AutenticateException) t;
			logger.warn("Invalid login: code:" + authEx.getCodeError() + "cause:" + authEx.getDescription());
		} else {
			logger.error("General Exception:", t);
		}

		if (StringUtils.isBlank(errorDescription)) {
			logger.debug(" message key {}", key);
			errorDescription = getErrorMessageProvider().getErrorMessage(key);
		}
		return errorDescription;
	}

	protected abstract MessagesPropertiesProvider getErrorMessageProvider();

}
