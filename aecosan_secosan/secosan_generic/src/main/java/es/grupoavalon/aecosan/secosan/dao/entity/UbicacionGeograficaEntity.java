package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_UBICACION_GEOGRAFICA)
public class UbicacionGeograficaEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.UBICACION_GEOGRAFICA_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.UBICACION_GEOGRAFICA_SEQUENCE, sequenceName = SequenceNames.UBICACION_GEOGRAFICA_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "COD_POSTAL")
	private String postalCode;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PAIS_ID")
	private PaisEntity country;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PROVINCIA_ID")
	private ProvinciasEntity province;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "MUNICIPIO_ID")
	private MunicipioEntity municipio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public PaisEntity getCountry() {
		return country;
	}

	public void setCountry(PaisEntity country) {
		this.country = country;
	}

	public MunicipioEntity getMunicipio() {
		return municipio;
	}

	public void setMunicipio(MunicipioEntity municipio) {
		this.municipio = municipio;
	}

	public ProvinciasEntity getProvince() {
		return province;
	}

	public void setProvince(ProvinciasEntity province) {
		this.province = province;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.getIdAsString().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((municipio == null) ? 0 : (municipio.getIdAsString()!=null ? municipio.getIdAsString().hashCode(): 0));
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UbicacionGeograficaEntity other = (UbicacionGeograficaEntity) obj;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.getIdAsString().equals(other.country.getIdAsString()))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.getIdAsString().equals(other.municipio.getIdAsString()))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String countryId = "";
		String municipioId = "";

		if (country != null) {
			countryId = country.getIdAsString();
		}

		if (municipio != null) {
			municipioId = municipio.getIdAsString();
		}

		return "UbicacionGeograficaEntity [id=" + id + ", postalCode=" + postalCode + ", country=" + countryId + ", municipio=" + municipioId + "]";
	}

}
