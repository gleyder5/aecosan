package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer;

import java.sql.Timestamp;
import java.util.Date;

public class LongToDateTransformer extends CustomValueTransformer {

	@Override
	public Object transform(Object input) {
		Long transformedValue = null;
		if (input != null) {
			if (input instanceof java.sql.Date) {
				transformedValue = transformSQLDateToLong((java.sql.Date) input);
			} else if (input instanceof Timestamp) {
				transformedValue = transformDateToLong((Timestamp) input);
			} else if (input instanceof Date) {
				transformedValue = transformDateToLong((Date) input);
			}
		}
		return transformedValue;
	}

	private static Long transformDateToLong(Date toTransform) {
		return toTransform.getTime();
	}

	private static Long transformSQLDateToLong(java.sql.Date toTransform) {
		return toTransform.getTime();
	}

	@Override
	public Object reverseTransform(Object input, Class<?> outputType) {
		Object returnObject = null;
		if (input != null) {
			if (outputType.equals(Date.class)) {
				returnObject = genereateDateFromLong((Long) input);
			} else if (outputType.equals(java.sql.Date.class)) {
				returnObject = genereateDateSQLFromLong((Long) input);
			} else if (outputType.equals(Timestamp.class)) {
				returnObject = genereateTimestampFromLong((Long) input);
			}
		}
		return returnObject;
	}

	public static Date genereateDateFromLong(Long dateLong) {

		Date dateGenerated = null;
		if (dateLong > 0) {
			dateGenerated = new Date(dateLong);
		}
		return dateGenerated;
	}

	public static java.sql.Date genereateDateSQLFromLong(Long dateLong) {
		java.sql.Date dateGenerated = null;
		if (dateLong > 0) {
			dateGenerated = new java.sql.Date(dateLong);
		}
		return dateGenerated;
	}

	public static Timestamp genereateTimestampFromLong(Long dateLong) {
		Timestamp dateGenerated = null;
		if (dateLong > 0) {
			dateGenerated = new Timestamp(dateLong);
		}
		return dateGenerated;
	}

}
