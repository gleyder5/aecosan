package es.grupoavalon.aecosan.secosan.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("statusRest")
@Path(SecosanConstants.ADMIN_SECURE_CONTEXT + "/status")
public class EstadoRestService {

	private static final Logger logger = LoggerFactory.getLogger(EstadoRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{areaID}")
	public Response getStatusByAreaId(@PathParam("areaID") String areaID) {
		logger.debug("-- Method getStatusByAreaId GET Init --");
		Response response;

		List<EstadoSolicitudDTO> listStatus;

		try {

			logger.debug("-- Method getStatusByAreaId | areaID: {} ", areaID);
			listStatus = bfacade.getStatusByAreaId(areaID);

			response = responseFactory.generateOkGenericResponse(listStatus);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method getStatusByAreaId GET End --");
		return response;
	}

}
