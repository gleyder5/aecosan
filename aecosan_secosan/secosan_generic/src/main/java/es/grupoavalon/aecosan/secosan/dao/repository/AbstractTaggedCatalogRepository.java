package es.grupoavalon.aecosan.secosan.dao.repository;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.repository.impl.CrudCatalogRepositoryImpl;

public abstract class AbstractTaggedCatalogRepository<T> extends CrudCatalogRepositoryImpl<T, Long> {

	Map<String, Method> taggedMethods;

	{
		taggedMethods = new HashMap<String, Method>();
		for (Method method : this.getClass().getDeclaredMethods()) {
			if (method.isAnnotationPresent(TaggedCatalog.class)) {
				TaggedCatalog annotation = (TaggedCatalog) method.getAnnotation(TaggedCatalog.class);
				taggedMethods.put(annotation.tagMethod(), method);
			}
		}
	}

	public Map<String, Method> getTaggedMethods() {
		return taggedMethods;
	}

	public List<T> getCatalogList() {
		return findAllOrderByValue();
	}

}
