package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import org.joda.time.LocalDate;

import java.util.List;

public interface BecasRepository extends CrudRepository<BecasEntity, Long> {

    List<BecasEntity> findByStatus(EstadoSolicitudEntity status);

    List<BecasEntity> findByStatusAndDate(EstadoSolicitudEntity status,LocalDate date);

    List<BecasEntity> getBecasSolicitantesPaginated(PaginationParams paginationParamas);

    int getTotalList(List<FilterParams> filterParams);
}
