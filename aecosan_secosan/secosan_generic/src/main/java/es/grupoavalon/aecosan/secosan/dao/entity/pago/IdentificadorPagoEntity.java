package es.grupoavalon.aecosan.secosan.dao.entity.pago;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AbstractPersonData;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_IDENTIFICADOR_PAGO)
public class IdentificadorPagoEntity extends AbstractPersonData {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.IDENTIFICADOR_PAGO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.IDENTIFICADOR_PAGO_SEQUENCE, sequenceName = SequenceNames.IDENTIFICADOR_PAGO_SEQUENCE, allocationSize = 1)
	private Long id;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DIRECCION_EXTENDIDA", referencedColumnName = "ID")
	private DireccionExtendidaEntity extendedAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DireccionExtendidaEntity getExtendedAddress() {
		return extendedAddress;
	}

	public void setExtendedAddress(DireccionExtendidaEntity extendedAddress) {
		this.extendedAddress = extendedAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((extendedAddress == null) ? 0 : extendedAddress.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		IdentificadorPagoEntity other = (IdentificadorPagoEntity) obj;
		if (extendedAddress == null) {
			if (other.extendedAddress != null) {
				return false;
			}
		} else if (!extendedAddress.equals(other.extendedAddress)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "IdentificadorPagoEntity [id=" + id + "," + super.toString() + ", extendedAddress=" + extendedAddress + "]";
	}


}
