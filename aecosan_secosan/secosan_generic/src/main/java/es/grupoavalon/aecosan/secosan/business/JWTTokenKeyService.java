package es.grupoavalon.aecosan.secosan.business;

public interface JWTTokenKeyService {

	String getTokenKey();

	String getTokenKeyAdmin();

}