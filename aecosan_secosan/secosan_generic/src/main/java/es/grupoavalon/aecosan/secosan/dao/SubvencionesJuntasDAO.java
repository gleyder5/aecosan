package es.grupoavalon.aecosan.secosan.dao;


import es.grupoavalon.aecosan.secosan.dao.entity.subvenciones.SubvencionesJuntasConsumoEntity;

public interface SubvencionesJuntasDAO {

	SubvencionesJuntasConsumoEntity addSubvencionesJuntasEntity(SubvencionesJuntasConsumoEntity subvencionesJuntasEntity);
	void updateSubvencionesJuntasEntity(SubvencionesJuntasConsumoEntity subvencionesJuntasEntity);

}
