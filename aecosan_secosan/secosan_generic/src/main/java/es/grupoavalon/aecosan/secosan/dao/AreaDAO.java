package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;

public interface AreaDAO {

	AreaEntity findArea(Long pk);

	AreaEntity findAreaByFormId(Long pk);
}
