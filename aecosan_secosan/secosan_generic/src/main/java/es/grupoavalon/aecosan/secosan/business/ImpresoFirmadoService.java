package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ImpresoFirmadoDTO;

public interface ImpresoFirmadoService {

	ImpresoFirmadoDTO getPDFToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO);
	ImpresoFirmadoDTO getPDFAnexoIIToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO);


}
