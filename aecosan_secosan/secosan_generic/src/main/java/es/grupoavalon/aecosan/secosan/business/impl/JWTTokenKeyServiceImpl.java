package es.grupoavalon.aecosan.secosan.business.impl;

import java.sql.Timestamp;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.JWTTokenKeyService;
import es.grupoavalon.aecosan.secosan.dao.JWTTokenKeyDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.JWTTokenKeyEntity;

@Component
public class JWTTokenKeyServiceImpl extends AbstractManager implements JWTTokenKeyService {
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTTokenKeyServiceImpl.class);

	private static final boolean ADMIN_TOKEN = true;
	private static final boolean NORMAL_TOKEN = false;

	@Autowired
	private JWTTokenKeyDAO jwtKeyDao;

	@PostConstruct
	public void postConstruct() {
		createTokenKey();
		createTokenKeyAdmin();
	}

	private void createTokenKey() {
		JWTTokenKeyEntity current = null;
		try {
			current = getCurrentKey();
			createNewTokenKeyIfRequired(current, NORMAL_TOKEN);
		} catch (Exception e) {
			this.handleCreateKeyException(e, current);
		}
	}

	private void createTokenKeyAdmin() {
		JWTTokenKeyEntity current = null;
		try {
			current = getCurrentKeyAdmin();
			createNewTokenKeyIfRequired(current, ADMIN_TOKEN);
		} catch (Exception e) {
			this.handleCreateKeyException(e, current);
		}
	}

	private JWTTokenKeyEntity createNewTokenKeyIfRequired(JWTTokenKeyEntity current, boolean admin) {
		JWTTokenKeyEntity newKey = null;
		if (isRequiredTCreateNew(current)) {
			deleteCurrentKey(current);
			newKey = generateNewKey(admin);
			jwtKeyDao.saveTokenKey(newKey);
			LOGGER.info("new Token created in DB: {} ", newKey);
		}
		return newKey;

	}

	private void handleCreateKeyException(Exception e, JWTTokenKeyEntity current) {
		if (current != null) {
			LOGGER.warn("Cant generate new token, using old: {} ", current);
		} else {
			LOGGER.error("Imposible to generate tokenKey", e);
			this.handleException(e);
		}
	}

	private JWTTokenKeyEntity generateNewKey(boolean admin) {
		JWTTokenKeyEntity tokenKey = new JWTTokenKeyEntity();
		tokenKey.setKey(generateRandomKey());
		tokenKey.setAdminkey(admin);
		tokenKey.setCreatedAt(new Timestamp(System.currentTimeMillis()));
		return tokenKey;
	}

	private static String generateRandomKey() {
		return RandomStringUtils.randomAlphanumeric(generateRandomKeyLenght());

	}

	private static int generateRandomKeyLenght() {
		Random randomGenerator = new Random();
		int low = 16;
		int high = 25;
		return randomGenerator.nextInt(high - low) + low;

	}

	private static boolean isRequiredTCreateNew(JWTTokenKeyEntity current) {
		boolean isRequired = true;
		if (current != null) {
			Timestamp createdAt = current.getCreatedAt();
			long diff = Math.abs(System.currentTimeMillis() - createdAt.getTime());
			long time = TimeUnit.MILLISECONDS.convert(5, TimeUnit.MINUTES);
			if (diff <= time) {
				isRequired = false;
				LOGGER.info("not required to generate new key token");
			}

		}
		return isRequired;
	}

	private void deleteCurrentKey(JWTTokenKeyEntity current) {
		if (current != null) {
			this.jwtKeyDao.deleteTokenKey(current.getKey());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * es.grupoavalon.eami.eventia.business.impl.JWTTokenKeyService#getTokenKey
	 * ()
	 */
	@Override
	public String getTokenKey() {
		String tokenKey = "";
		try {
			JWTTokenKeyEntity currentKey = getCurrentKey();
			tokenKey = currentKey.getKey();
		} catch (Exception e) {
			this.handleException(e);
		}
		return tokenKey;
	}

	@Override
	public String getTokenKeyAdmin() {
		String tokenKey = "";
		try {
			JWTTokenKeyEntity currentKey = getCurrentKeyAdmin();
			tokenKey = currentKey.getKey();
		} catch (Exception e) {
			this.handleException(e);
		}
		return tokenKey;
	}

	private JWTTokenKeyEntity getCurrentKey() {
		JWTTokenKeyEntity tokenKey = this.jwtKeyDao.getCurrentTokenKey();
		if (tokenKey == null) {
			LOGGER.warn("There was no token key, a new one will be created");
			tokenKey = createNewTokenKeyIfRequired(tokenKey, NORMAL_TOKEN);
		}
		return tokenKey;
	}

	private JWTTokenKeyEntity getCurrentKeyAdmin() {
		JWTTokenKeyEntity tokenKey = this.jwtKeyDao.getCurrentTokenKeyAdmin();
		if (tokenKey == null) {
			LOGGER.warn("There was no ADMIN token key, a new one will be created");
			tokenKey = createNewTokenKeyIfRequired(tokenKey, ADMIN_TOKEN);
		}
		return tokenKey;
	}

}
