package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;

@MappedSuperclass
public abstract class AbstractSolicitudesQuejaSugerencia extends SolicitudEntity {

	@Column(name = "UNIDAD")
	private String unidad;
	@Column(name = "FECHA_HORA_INCIDENCIA")
	private Timestamp dateTimeIncidence;
	@Column(name = "MOTIVO")
	private String motive;

	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPOCOMUNICACION_ID", nullable = false)
	private TipoComunicacionEntity comunicationType;

	@Transient
	private Long dateTimeIncidenceLong;

	public Long getDateTimeIncidenceLong() {
		return dateTimeIncidence.getTime();
	}

	public void setDateTimeIncidenceLong(Long dateTimeIncidenceLong) {
		this.dateTimeIncidenceLong = dateTimeIncidenceLong;
		this.dateTimeIncidence = new Timestamp(dateTimeIncidenceLong);
	}

	public Timestamp getDateTimeIncidence() {
		return dateTimeIncidence;
	}

	public void setDateTimeIncidence(Timestamp dateTimeIncidence) {
		this.dateTimeIncidence = dateTimeIncidence;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public TipoComunicacionEntity getComunicationType() {
		return comunicationType;
	}

	public void setComunicationType(TipoComunicacionEntity comunicationType) {
		this.comunicationType = comunicationType;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((comunicationType == null) ? 0 : comunicationType.getId().hashCode());
		result = prime * result + ((dateTimeIncidence == null) ? 0 : dateTimeIncidence.hashCode());
		result = prime * result + ((motive == null) ? 0 : motive.hashCode());
		result = prime * result + ((unidad == null) ? 0 : unidad.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractSolicitudesQuejaSugerencia other = (AbstractSolicitudesQuejaSugerencia) obj;
		if (comunicationType == null) {
			if (other.comunicationType != null)
				return false;
		} else if (!comunicationType.getId().equals(other.comunicationType.getId()))
			return false;
		if (dateTimeIncidence == null) {
			if (other.dateTimeIncidence != null)
				return false;
		} else if (!dateTimeIncidence.equals(other.dateTimeIncidence))
			return false;
		if (motive == null) {
			if (other.motive != null)
				return false;
		} else if (!motive.equals(other.motive))
			return false;
		if (unidad == null) {
			if (other.unidad != null)
				return false;
		} else if (!unidad.equals(other.unidad))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String comunicationTypeId = "";
		if (comunicationType != null) {
			comunicationTypeId = comunicationType.getIdAsString();
		}
		return "AbstractSolicitudesQuejaSugerencia [unidad=" + unidad + ", dateTimeIncidence=" + dateTimeIncidence + ", motive=" + motive + ", comunicationType=" + comunicationTypeId + "]";
	}

}
