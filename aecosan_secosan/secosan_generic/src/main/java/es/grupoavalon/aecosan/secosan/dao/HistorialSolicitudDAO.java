package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

import java.util.List;

public interface HistorialSolicitudDAO {
    List<HistorialSolicitudEntity> getHistorialList(PaginationParams paginationParams);

    HistorialSolicitudEntity getBySolicitudAndStatus(Integer solicitudId,EstadoSolicitudEntity estadoSolicitudEntity);

    HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity);

    HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity,Long status);

    HistorialSolicitudEntity addHistorialSolicitud(SolicitudEntity solicitudEntity, EstadoSolicitudEntity status, ChangeStatusDTO changeStatusDTO);

    int getTotal(List<FilterParams> filterParams);
}
