package es.grupoavalon.aecosan.secosan.util.rest.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.web.filter.GenericFilterBean;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.property.PropertyException;
import es.grupoavalon.aecosan.secosan.util.rest.security.AbstractJwtAuthenticationProvider.TokenAuth;

public class JwtAuthenticationFilter extends GenericFilterBean {

	private static final Logger LOGGER_JWT = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
	private static final long DEFAULT_RENEWAL_TIME = 120000;
	private AuthenticationEntryPoint entryPoint;
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtUtil;

	private long renewalTokenMilisecs;

	public JwtAuthenticationFilter(AuthenticationManager authenticationManager, AuthenticationEntryPoint entryPoint) {
		this.authenticationManager = authenticationManager;
		this.entryPoint = entryPoint;
		this.renewalTokenMilisecs = getRenewalMilisecs();
	}

	private static long getRenewalMilisecs() {
		long renewalMilisecs = 0;
		try {
			renewalMilisecs = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_TOKEN_RENEWAL_PERIOD);
		} catch (PropertyException e) {
			renewalMilisecs = DEFAULT_RENEWAL_TIME;
		}
		return renewalMilisecs;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		try {
			LOGGER_JWT.debug("Executing jwt filter");
			String token = extractTokenFromHeaders(req);
			if (!isOptionCORSOperation(req) && StringUtils.isNotBlank(token)) {
				LOGGER_JWT.debug("received token in filter: {}", token);
				TokenAuth tokenAuth = new TokenAuth(token);
				Authentication auth = this.executeAuthorization(tokenAuth, req, res);
				renewTokenIfExpiresSoon(tokenAuth, auth, res, req);
				req = setUserInfoInHeader(tokenAuth, req,auth);
			}
		} catch (AuthenticationException e) {
			executeEntryPoint(req, res, e);
		}
		chain.doFilter(req, res);
	}

	private static boolean isOptionCORSOperation(HttpServletRequest req) {
		String method = req.getMethod();
		boolean isOption = false;
		if (StringUtils.equalsIgnoreCase("OPTIONS", method)) {
			isOption = true;
		}
		return isOption;
	}

	private Authentication executeAuthorization(Authentication authObject, HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		Authentication auth = authenticationManager.authenticate(authObject);

		if (!auth.isAuthenticated()) {
			executeEntryPoint(req, res, null);
		} else {
			SecurityContextHolder.getContext().setAuthentication(auth);
		}
		return auth;
	}

	private static String extractTokenFromHeaders(HttpServletRequest req) {
		String stringToken = req.getHeader("Authorization");
		return SecosanUtil.extractTokenFromHeaderValue(stringToken);
	}

	private void executeEntryPoint(HttpServletRequest req, HttpServletResponse res, AuthenticationException e) throws IOException, ServletException {

		if (entryPoint != null) {
			entryPoint.commence(req, res, e);
		} else {
			res.sendError(HttpServletResponse.SC_UNAUTHORIZED, "invalid token");
		}
	}

	private void renewTokenIfExpiresSoon(TokenAuth tokenAuth, Authentication auth, HttpServletResponse res, HttpServletRequest req) {
		boolean isAdmin = isAdminRequest(req);
		String originalToken = tokenAuth.getJwtOriginalToken();
		long remaning = jwtUtil.getRemaningValidationTime(originalToken,isAdmin);
		LOGGER_JWT.debug("is authenticated: {}  < remaining: {} <= {} ", auth.isAuthenticated(),remaning, this.renewalTokenMilisecs);
		if (auth.isAuthenticated() &&  (remaning >0) && (remaning <= this.renewalTokenMilisecs)) {
			String extendedValidationToken = jwtUtil.renewToken(originalToken, isAdmin);
			res.addHeader("AuthorizationRenewal", extendedValidationToken);
			LOGGER_JWT.debug("token renewal sent: {}", extendedValidationToken);
			tokenAuth.setJwtOriginalToken(extendedValidationToken);
		}
	}

	private static boolean isAdminRequest(HttpServletRequest req) {
		boolean isRequestAdmin = JwtUtil.NORMAL_TOKEN;
		if (StringUtils.contains(req.getRequestURI(), "/admin/")) {
			isRequestAdmin = JwtUtil.ADMIN_TOKEN;
		}
		LOGGER_JWT.debug("req context path: {}  is admin: {} ",req.getRequestURI(),isRequestAdmin);
		return isRequestAdmin;
	}

	private CustomHttpServletRequest setUserInfoInHeader(TokenAuth tokenAuth, HttpServletRequest req,Authentication auth) {
		String originalToken = tokenAuth.getJwtOriginalToken();

		return setUserFromTokenInRequestHeader(originalToken, req, auth);

	}

	private CustomHttpServletRequest setUserFromTokenInRequestHeader(String token, HttpServletRequest req, Authentication auth) {
		String id;
		LOGGER_JWT.debug("Setting user in header: {} ", token);
		CustomHttpServletRequest customReq = new CustomHttpServletRequest(req);
		if (auth.isAuthenticated()) {
			Object userIdClaim = this.jwtUtil.getClaimFromToken(token, "id", isAdminRequest(req));
			LOGGER_JWT.debug("userIdClaim--> {} ", userIdClaim);
			if (userIdClaim != null) {
				id = userIdClaim.toString();
				customReq = new CustomHttpServletRequest(req, id);
			}
		}
		return customReq;
	}

	public static class CustomHttpServletRequest extends HttpServletRequestWrapper {

		private HttpServletRequest request;
		private String userheaderValue;

		public CustomHttpServletRequest(HttpServletRequest request) {
			super(request);
			this.request = request;
		}

		public CustomHttpServletRequest(HttpServletRequest request, String userheaderValue) {
			this(request);
			this.userheaderValue = userheaderValue;
		}

		@Override
		public String getHeader(String name) {
			String value;
			if (StringUtils.equalsIgnoreCase(SecosanConstants.REQUEST_HEADER_USER_ID, name)) {
				value = userheaderValue;
			} else {

				value = this.request.getHeader(name);
			}
			return value;
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		@Override
		public Enumeration getHeaderNames() {
			List list = new ArrayList();
			HttpServletRequest httpServletRequest = (HttpServletRequest) getRequest();
			Enumeration e = httpServletRequest.getHeaderNames();
			while (e.hasMoreElements()) {
				String n = (String) e.nextElement();
				list.add(n);
			}
			list.add(SecosanConstants.REQUEST_HEADER_USER_ID);
			return Collections.enumeration(list);
		}
	}

}
