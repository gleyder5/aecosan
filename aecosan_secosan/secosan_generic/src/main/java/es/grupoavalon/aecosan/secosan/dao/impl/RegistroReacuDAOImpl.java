package es.grupoavalon.aecosan.secosan.dao.impl;

import es.grupoavalon.aecosan.secosan.dao.RegistroReacuDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroReacu.RegistroReacuEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.RegistroReacuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RegistroReacuDAOImpl extends GenericDao implements RegistroReacuDAO {
	@Autowired
	private RegistroReacuRepository registroReacuRepository;
	@Override
	public RegistroReacuEntity addRegistroReacuEntity(RegistroReacuEntity registroReacu) {
		RegistroReacuEntity registroReacuEntity= null;
		try {
			registroReacuEntity = registroReacuRepository.save(registroReacu);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addRegistroReacu | registroReacu: " + registroReacu);
		}
		return registroReacuEntity;
	}
	@Override
	public void updateRegistroReacuEntity(RegistroReacuEntity registroReacu) {
		try {
			registroReacuRepository.update(registroReacu);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateRegistroReacu | ERROR: " + registroReacu);
		}
	}
}
