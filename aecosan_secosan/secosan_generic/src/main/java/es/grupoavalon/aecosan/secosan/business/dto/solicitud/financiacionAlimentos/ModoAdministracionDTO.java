package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;

public class ModoAdministracionDTO extends AbstractCatalogDto implements IsNulable<ModoAdministracionDTO> {

	@Override
	public ModoAdministracionDTO shouldBeNull() {
		if ("".equals(id) || id == null) {
			return null;
		} else {
			return this;
		}
	}

}
