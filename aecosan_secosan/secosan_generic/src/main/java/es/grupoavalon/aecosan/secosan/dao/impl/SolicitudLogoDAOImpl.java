package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.SolicitudLogoDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudLogoRepository;

@Repository
public class SolicitudLogoDAOImpl extends GenericDao implements SolicitudLogoDAO {

	@Autowired
	private SolicitudLogoRepository solicitudLogoRepository;


	@Override
	public SolicitudLogoEntity addSolicitudLogo(SolicitudLogoEntity solicitudLogo) {
		SolicitudLogoEntity solicitudeLogoEntity = null;
		try {
			solicitudeLogoEntity = solicitudLogoRepository.save(solicitudLogo);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addSolicitudLogo | solicitudLogo: " + solicitudLogo);
		}
		return solicitudeLogoEntity;
	}

	@Override
	public SolicitudLogoEntity findSolicitudLogoById(Long id) {
		SolicitudLogoEntity solicitudLogoEntity = null;
		try {
			solicitudLogoEntity = solicitudLogoRepository.findOne(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findSolicitudLogoById | ID: " + id);
		}

		return solicitudLogoEntity;
	}
	
	@Override
	public void updateSolicitudLogo(SolicitudLogoEntity solicitudLogoEntity) {
		try {
			solicitudLogoRepository.update(solicitudLogoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateSolicitudLogo | ERROR: " + solicitudLogoEntity);
		}

	}

}
