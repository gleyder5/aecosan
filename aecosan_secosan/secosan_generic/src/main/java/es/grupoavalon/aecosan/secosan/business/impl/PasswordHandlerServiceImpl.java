package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.PasswordHandlerService;
import es.grupoavalon.aecosan.secosan.business.dto.user.ChangePasswordDTO;
import es.grupoavalon.aecosan.secosan.business.exception.PasswordOperationException;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.LoginUser;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class PasswordHandlerServiceImpl extends AbstractManager implements PasswordHandlerService {

	private static final String PASSWORD_REGEX;
	static {
		PASSWORD_REGEX = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_PW_REGEX);
	}

	@Autowired
	private UsuarioDAO usuarioDAO;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	public void changeUserPassword(ChangePasswordDTO changePasswordDto) {

		try {
			logger.debug("change pw:" + changePasswordDto);
			LoginUser user = getUserById(changePasswordDto.getUserId());
			String currentPassword = user.getPassword();
			String newPassword = changePasswordDto.getNewPasswordDecoded();
			String currentPasswordSent = changePasswordDto.getOldPasswordDecoded();

			validateIfActualPasswordMatches(currentPassword, currentPasswordSent);
			validadIfNewPasswordIsNotActual(currentPassword, newPassword);
			validateIfNewPasswordIsStrong(newPassword);

			updatePassword(user, newPassword);
		} catch (Exception e) {
			this.handleException(e);
		}
	}

	private LoginUser getUserById(String userId) {
		Long userIdLong = Long.parseLong(userId);
		UsuarioEntity loggedUser = usuarioDAO.getUserById(userIdLong);
		validateIfUserExist(loggedUser);
		return (LoginUser) loggedUser;
	}

	private static void validateIfUserExist(UsuarioEntity loggedUser) {
		if (loggedUser == null || !(loggedUser instanceof LoginUser)) {
			throw new PasswordOperationException(PasswordOperationException.PasswordOperationErrorCodes.ERROR_USER_NOTFOUND);
		}
	}

	private void validateIfActualPasswordMatches(String currentEncoded, String sent) {
		if (!encoder.matches(sent, currentEncoded)) {
			throw new PasswordOperationException(PasswordOperationException.PasswordOperationErrorCodes.ERROR_USER_AUTH_FAIL);
		}
	}

	private void validadIfNewPasswordIsNotActual(String currentEncoded, String sentNew) {
		if (encoder.matches(sentNew, currentEncoded)) {
			throw new PasswordOperationException(PasswordOperationException.PasswordOperationErrorCodes.ERROR_NEW_PASSWORD_EQUAL);
		}
	}

	private void validateIfNewPasswordIsStrong(String newPassword) {
		Pattern patt = Pattern.compile(PASSWORD_REGEX);
		Matcher match = patt.matcher(newPassword);
		if (!match.find()) {
			throw new PasswordOperationException(PasswordOperationException.PasswordOperationErrorCodes.ERROR_PASSWORD_WEAK);
		}
	}

	private void updatePassword(LoginUser loggedUser, String newPassword) {
		String newPasswordEncoded = encoder.encode(newPassword);
		loggedUser.setPassword(newPasswordEncoded);
		this.usuarioDAO.updateUser((UsuarioEntity) loggedUser);
	}

}
