package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.SolicitudListDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;

@Component("solicitudListRest")
public class SolicitudListRestService {

	private static final Logger logger = LoggerFactory.getLogger(SolicitudListRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.SECURE_CONTEXT + "/solicitudList/";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/solicitudList/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "{user}")
	public Response listItems(@PathParam(AbstractListRestHandler.PARAM_USER) String user, @DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		logger.debug("Entrada petición GET listItems()");

		AbstractListRestHandler<SolicitudListDTO> listHandler = new AbstractListRestHandler<SolicitudListDTO>() {
			@Override
			protected AbstractResponseFactory getResponseFactory() {
				return responseFactory;
			}

			@Override
			PaginatedListWrapper<SolicitudListDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
				return bfacade.getPaginatedList(otherParams, paginationParams);
			}

		};
		return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters(), user);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL + "tramitador/{area}")
	public Response listItemsBySolicitante(@PathParam(AbstractListRestHandler.PARAM_AREA) String area, @DefaultValue("0") @QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		logger.debug("Entrada petición GET listItemsBySolicitante()");

		AbstractListRestHandler<SolicitudListDTO> listHandler = new AbstractListRestHandler<SolicitudListDTO>() {
			@Override
			protected AbstractResponseFactory getResponseFactory() {
				return responseFactory;
			}

			@Override
			PaginatedListWrapper<SolicitudListDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
				return bfacade.getPaginatedListByArea(otherParams, paginationParams);
			}

		};
		return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters(), area);
	}

}
