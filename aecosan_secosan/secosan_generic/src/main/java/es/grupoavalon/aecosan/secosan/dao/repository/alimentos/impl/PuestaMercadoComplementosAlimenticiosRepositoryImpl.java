package es.grupoavalon.aecosan.secosan.dao.repository.alimentos.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.PuestaMercadoComplementosAlimenticiosRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.impl.AbstractCrudRespositoryImpl;

@Component
public class PuestaMercadoComplementosAlimenticiosRepositoryImpl extends AbstractCrudRespositoryImpl<PuestaMercadoCAEntity, Long>
		implements PuestaMercadoComplementosAlimenticiosRepository {

	@Override
	protected Class<PuestaMercadoCAEntity> getClassType() {
		return PuestaMercadoCAEntity.class;
	}
}
