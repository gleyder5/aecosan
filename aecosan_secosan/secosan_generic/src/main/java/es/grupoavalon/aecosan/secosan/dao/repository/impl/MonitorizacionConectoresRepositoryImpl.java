package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.MonitorizacionConectoresEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.MonitorizacionConectoresRepository;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;

@Component("MonitorizacionConectores")
public class MonitorizacionConectoresRepositoryImpl extends AbstractCrudRespositoryImpl<MonitorizacionConectoresEntity, Long> implements MonitorizacionConectoresRepository {
	private static final String QUERY = "#Query: ";

	@Override
	protected Class<MonitorizacionConectoresEntity> getClassType() {
		return MonitorizacionConectoresEntity.class;
	}

	@Override
	public List<MonitorizacionConectoresEntity> getListMonitoringConnector(PaginationParams paginationParams) {
		List<FilterParams> filters = paginationParams.getFilters();
		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryWherePart = getQueryParts(filters, queryParams);
		logger.debug(QUERY + queryWherePart);
		String sortField = translateSortField(paginationParams.getSortField());

		List<MonitorizacionConectoresEntity> resultList;
		if (paginationParams.getOrder().equals(QueryOrder.ASC)) {
			resultList = findAllPaginatedAndSortedASC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		} else {
			resultList = findAllPaginatedAndSortedDESC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		}

		return resultList;
	}

	@Override
	public int getFilteredListMonitoringConnector(List<FilterParams> filters) {
		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryName = getQueryParts(filters, queryParams);
		logger.debug(QUERY + queryName);
		return getTotalRecords(queryName, queryParams);
	}

	@Override
	public int getTotalListMonitoringConnector(List<FilterParams> filters) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		return getTotalRecords(getQueryParts(filters, queryParams), queryParams);
	}

	private String translateSortField(String sortField) {
		String translatedSortField = sortField;
		if (StringUtils.equals(SecosanConstants.FILTER_CONNECTOR_NAME, sortField)) {
			translatedSortField = SecosanConstants.FIELD_CONNECTOR_NAME;
		} else if (StringUtils.equals(SecosanConstants.FILTER_CONNECTOR_DATE, sortField)) {
			translatedSortField = SecosanConstants.FIELD_CONNECTOR_DATE;
		} else if (StringUtils.equals(SecosanConstants.FILTER_CONNECTOR_SUCCESS, sortField)) {
			translatedSortField = SecosanConstants.FIELD_CONNECTOR_SUCCESS;
		} else if (StringUtils.equals(SecosanConstants.FILTER_CONNECTOR_ERRORTYPE, sortField)) {
			translatedSortField = SecosanConstants.FIELD_CONNECTOR_ERRORTYPE;
		}
		return translatedSortField;
	}

	private String getQueryParts(List<FilterParams> filters, Map<String, Object> queryParams) {
		QueryWhereParts queryParts = new QueryWhereParts();

		for (FilterParams f : filters) {
			if (f.getFilterName().equals(SecosanConstants.FIELD_CONNECTOR_NAME)) {
				queryParts.setPart(SecosanConstants.FIELD_CONNECTOR_NAME + "=:" + SecosanConstants.FIELD_CONNECTOR_NAME);
				queryParams.put(SecosanConstants.FIELD_CONNECTOR_NAME, f.getFilterValue());
			} else if (f.getFilterName().equals(SecosanConstants.FIELD_CONNECTOR_SUCCESS)) {
				queryParts.setPart(SecosanConstants.FIELD_CONNECTOR_SUCCESS + "=:" + SecosanConstants.FIELD_CONNECTOR_SUCCESS);
				queryParams.put(SecosanConstants.FIELD_CONNECTOR_SUCCESS, Boolean.valueOf(f.getFilterValue()));
			} else if (f.getFilterName().equals(SecosanConstants.FILTER_CONNECTOR_DATE_FROM)) {
				queryParts.setPart(SecosanConstants.FIELD_CONNECTOR_DATE + ">=" + ":" + SecosanConstants.FILTER_CONNECTOR_DATE_FROM);
				queryParams.put(SecosanConstants.FILTER_CONNECTOR_DATE_FROM, new Date(Long.valueOf(f.getFilterValue())));
			} else if (f.getFilterName().equals(SecosanConstants.FILTER_CONNECTOR_DATE_TO)) {
				queryParts.setPart(SecosanConstants.FIELD_CONNECTOR_DATE + "<=" + ":" + SecosanConstants.FILTER_CONNECTOR_DATE_TO);
				queryParams.put(SecosanConstants.FILTER_CONNECTOR_DATE_TO, new Date(Long.valueOf(f.getFilterValue())));
			} else if (f.getFilterName().equals(SecosanConstants.FIELD_CONNECTOR_ERRORTYPE)) {
				queryParts.setPart("LOWER ( " + SecosanConstants.FIELD_CONNECTOR_ERRORTYPE + ") like LOWER (:" + SecosanConstants.FIELD_CONNECTOR_ERRORTYPE + ")");
				queryParams.put(SecosanConstants.FIELD_CONNECTOR_ERRORTYPE, f.getFilterValue() + "%");
			}
		}
		return assambleQuery("", queryParts);
	}

}
