package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;

public interface TipoFormularioDAO {
	
	TipoFormularioEntity findFormulario(Long pk);

	String getSignByFormId(Long pk);
}
