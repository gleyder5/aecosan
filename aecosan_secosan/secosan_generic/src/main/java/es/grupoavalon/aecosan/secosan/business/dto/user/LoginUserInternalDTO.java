package es.grupoavalon.aecosan.secosan.business.dto.user;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class LoginUserInternalDTO extends LoginUserDTO {

	@BeanToBeanMapping(getValueFrom = "role", getSecondValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String roleId;
	@BeanToBeanMapping(getValueFrom = "role", getSecondValueFrom = "catalogValue")
	private String roleDescription;

	private String areaId;

	private String areaDescription;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((areaDescription == null) ? 0 : areaDescription.hashCode());
		result = prime * result + ((areaId == null) ? 0 : areaId.hashCode());
		result = prime * result + ((roleDescription == null) ? 0 : roleDescription.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LoginUserInternalDTO other = (LoginUserInternalDTO) obj;
		if (areaDescription == null) {
			if (other.areaDescription != null) {
				return false;
			}
		} else if (!areaDescription.equals(other.areaDescription)) {
			return false;
		}
		if (areaId == null) {
			if (other.areaId != null) {
				return false;
			}
		} else if (!areaId.equals(other.areaId)) {
			return false;
		}
		if (roleDescription == null) {
			if (other.roleDescription != null) {
				return false;
			}
		} else if (!roleDescription.equals(other.roleDescription)) {
			return false;
		}
		if (roleId == null) {
			if (other.roleId != null) {
				return false;
			}
		} else if (!roleId.equals(other.roleId)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "LoginUserInternalDTO [roleId=" + roleId + ", roleDescription=" + roleDescription + ", areaId=" + areaId + ", areaDescription=" + areaDescription + "]";
	}


}
