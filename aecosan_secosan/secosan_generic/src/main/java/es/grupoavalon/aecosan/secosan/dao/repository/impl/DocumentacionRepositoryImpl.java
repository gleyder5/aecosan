package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.DocumentacionRepository;

@Component
public class DocumentacionRepositoryImpl extends AbstractCrudRespositoryImpl<DocumentacionEntity, Long> implements DocumentacionRepository {


	@Override
	protected Class<DocumentacionEntity> getClassType() {
		return DocumentacionEntity.class;
	}

}
