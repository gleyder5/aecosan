package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoFormularioRepository;

@Component("TipoFormularioRepository")
public class TipoFormularioRepositoryImpl extends CrudCatalogRepositoryImpl<TipoFormularioEntity, Long> implements TipoFormularioRepository {

	@Override
	protected Class<TipoFormularioEntity> getClassType() {
		return TipoFormularioEntity.class;
	}

	@Override
	public List<TipoFormularioEntity> getCatalogList() {

		return findAllOrderByValue();
	}

}
