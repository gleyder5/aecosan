package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;

public interface ServicioRepository extends CrudRepository<ServicioEntity, Long> {

}
