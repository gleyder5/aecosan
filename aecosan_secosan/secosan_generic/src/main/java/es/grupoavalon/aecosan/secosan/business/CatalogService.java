package es.grupoavalon.aecosan.secosan.business;

import java.util.List;

import es.grupoavalon.aecosan.secosan.business.dto.CatalogDTO;

public interface CatalogService {
	List<CatalogDTO> getCatalogItems(String catalogName);

	List<CatalogDTO> getCatalogItems(String catalogName, String parentElement, String parentId);

	List<CatalogDTO> getCatalogItems(String catalogName, String tag);
}