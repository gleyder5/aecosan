package es.grupoavalon.aecosan.secosan.dao.repository.alimentos;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

public interface ModificacionDatosAlimentosGrupoRepository extends CrudRepository<ModificacionDatosAGEntity, Long> {

}
