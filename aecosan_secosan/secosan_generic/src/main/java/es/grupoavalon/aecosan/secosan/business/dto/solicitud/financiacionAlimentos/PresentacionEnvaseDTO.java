package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class PresentacionEnvaseDTO {
	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("financiacionUsosMedicos")
	@XmlElement(name = "financiacionUsosMedicos")
	@BeanToBeanMapping(getValueFrom = "financiacionAlimentos", getSecondValueFrom = "id")
	private Long financiacionUsosMedicos;

	@JsonProperty("containerType")
	@XmlElement(name = "containerType")
	@BeanToBeanMapping(getValueFrom = "containerType")
	private String containerType;

	@JsonProperty("numberOfContainer")
	@XmlElement(name = "numberOfContainer")
	@BeanToBeanMapping(getValueFrom = "numberOfContainer")
	private String numberOfContainer;

	@JsonProperty("companyPriceSale")
	@XmlElement(name = "companyPriceSale")
	@BeanToBeanMapping(getValueFrom = "companyPriceSale")
	private String companyPriceSale;

	@JsonProperty("flavour")
	@XmlElement(name = "flavour")
	@BeanToBeanMapping(getValueFrom = "flavour")
	private String flavour;

	@JsonProperty("contents")
	@XmlElement(name = "contents")
	@BeanToBeanMapping(getValueFrom = "contents")
	private String contents;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFinanciacionUsosMedicos() {
		return financiacionUsosMedicos;
	}

	public void setFinanciacionUsosMedicos(Long financiacionUsosMedicos) {
		this.financiacionUsosMedicos = financiacionUsosMedicos;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getNumberOfContainer() {
		return numberOfContainer;
	}

	public void setNumberOfContainer(String numberOfContainer) {
		this.numberOfContainer = numberOfContainer;
	}

	public String getCompanyPriceSale() {
		return companyPriceSale;
	}

	public void setCompanyPriceSale(String companyPriceSale) {
		this.companyPriceSale = companyPriceSale;
	}

	public String getFlavour() {
		return flavour;
	}

	public void setFlavour(String flavour) {
		this.flavour = flavour;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "PresentacionEnvaseDTO [id=" + id + ", financiacionUsosMedicos=" + financiacionUsosMedicos + ", containerType=" + containerType + ", numberOfContainer=" + numberOfContainer
				+ ", companyPriceSale=" + companyPriceSale + ", flavour=" + flavour + ", contents=" + contents + "]";
	}

}
