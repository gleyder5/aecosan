package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PUESTA_MERCADO)
public class PuestaMercadoEntity extends SolicitudEntity {

	@Column(name = "NOMBRE_COMERCIAL")
	private String nombreComercialProducto;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "puestaMercado", cascade = CascadeType.ALL)
	@JoinColumn(name = "PUESTA_MERCADO_ID")
	private Set<PresentacionProductoEntity> presentaciones;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DATOS_IDENTIF_ADIC_ID")
	private DatosIdentificativosAdicionalesEntity datosIdentificativosAdicionales;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "ANTECEDENTES_PROCEDENCIA_ID")
	private AntecedentesProcedenciaEntity antecedentesProcedencia;

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public Set<PresentacionProductoEntity> getPresentaciones() {
		return presentaciones;
	}

	public void setPresentaciones(Set<PresentacionProductoEntity> presentaciones) {
		this.presentaciones = presentaciones;
	}

	public void setPresentaciones(LinkedHashSet<PresentacionProductoEntity> presentaciones) {
		this.presentaciones = presentaciones;
	}

	public AntecedentesProcedenciaEntity getAntecedentesProcedencia() {
		return antecedentesProcedencia;
	}

	public void setAntecedentesProcedencia(AntecedentesProcedenciaEntity antecedentesProcedencia) {
		this.antecedentesProcedencia = antecedentesProcedencia;
	}

	public DatosIdentificativosAdicionalesEntity getDatosIdentificativosAdicionales() {
		return datosIdentificativosAdicionales;
	}

	public void setDatosIdentificativosAdicionales(DatosIdentificativosAdicionalesEntity datosIdentificativosAdicionales) {
		this.datosIdentificativosAdicionales = datosIdentificativosAdicionales;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((nombreComercialProducto == null) ? 0 : nombreComercialProducto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PuestaMercadoEntity other = (PuestaMercadoEntity) obj;
		if (nombreComercialProducto == null) {
			if (other.nombreComercialProducto != null) {
				return false;
			}
		} else if (!nombreComercialProducto.equals(other.nombreComercialProducto)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {

		return "PuestaMercadoEntity [nombreComercialProducto=" + nombreComercialProducto + "]";
	}

}
