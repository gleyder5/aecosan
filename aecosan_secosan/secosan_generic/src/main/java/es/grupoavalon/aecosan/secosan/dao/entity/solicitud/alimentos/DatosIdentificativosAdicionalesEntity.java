package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_DATOS_IDENTIFICATIVOS_ADICIONALES)
public class DatosIdentificativosAdicionalesEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.DATOS_IDENTIFICATIVOS_ADICIONALES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.DATOS_IDENTIFICATIVOS_ADICIONALES_SEQUENCE, sequenceName = SequenceNames.DATOS_IDENTIFICATIVOS_ADICIONALES_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "ES_FABRICANTE_PRODUCTO")
	private Boolean isProductMaker;

	@Column(name = "RGSEAA")
	private String rgseaa;

	@Column(name = "INCLUSION_SOLICITUD")
	private Boolean solicitudeInclude;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsProductMaker() {
		return isProductMaker;
	}

	public void setIsProductMaker(Boolean isProductMaker) {
		this.isProductMaker = isProductMaker;
	}

	public String getRgseaa() {
		return rgseaa;
	}

	public void setRgseaa(String rgseaa) {
		this.rgseaa = rgseaa;
	}

	public Boolean getSolicitudeInclude() {
		return solicitudeInclude;
	}

	public void setSolicitudeInclude(Boolean solicitudeInclude) {
		this.solicitudeInclude = solicitudeInclude;
	}

	@Override
	public String toString() {
		return "DatosIdentificativosAdicionalesEntity [id=" + id + ", isProductMaker=" + isProductMaker + ", rgseaa=" + rgseaa + ",solicitudeInclude=" + solicitudeInclude + "]";
	}

}
