/**
 * 
 */
package es.grupoavalon.aecosan.secosan.dao.exception;

import es.grupoavalon.aecosan.secosan.util.exception.SecosanException;

/**
 * Indicate an error while executing an operation in the database
 * @author ottoabreu
 *
 */
public class DaoException extends SecosanException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * CRUD_OPERATION_ERROR ="Can not execute the CRUD operation due an error: ";
	 */
	public static final String CRUD_OPERATION_ERROR ="Can not execute the CRUD operation ";
	/**
	 *  RESULT_EXPECTED_ERROR ="The method find more result than expected (expected, Obtained)=";
	 */
	public static final String RESULT_EXPECTED_ERROR ="The method find more result than expected (expected, Obtained)=";
	
	/**
	 * NOT_EXISTING_ENTITY ="The entity object was not found id=";
	 */
	public static final String NOT_EXISTING_ENTITY ="The entity object was not found id=";

	/**
	 * PAREN_CATALOG_ERROR =
	 * "There is a problem with the parent catalog query for: ";
	 */
	public static final String PAREN_CATALOG_ERROR = "There is a problem with the parent catalog query for: ";

	/**
	 * TAGGED_CATALOG_ERROR = "There is a problem with the tagged catalog for: "
	 * ;
	 */
	public static final String TAGGED_CATALOG_ERROR = "There is a problem with the tagged catalog for: ";
	
	
	/**
	 * TAGGED_NO_CATALOG_ERROR = "There is not catalog tagged as: "
	 * ;
	 */
	public static final String TAGGED_NO_CATALOG_ERROR = "There is not catalog tagged as: ";

	/**
	 * NOT_EXISTING_CATALOG = "There is no repostory to manage the catalog: ";
	 */
	public static final String NOT_EXISTING_CATALOG = "There is not repostory to manage the catalog ";

	/**
	 * NOT_COMPLEX_CATALOG = "There is not a complex catalog: ";
	 */
	public static final String NOT_COMPLEX_CATALOG = "There is not a complex catalog ";

	/**
	 * String TAGGED_CATALOG_INVOKE_ERROR =
	 * "There is a problem while invoking method in repository due an exception: "
	 * ;
	 */
	public static final String TAGGED_CATALOG_INVOKE_ERROR = "There is a problem while invoking method in repository due an exception: ";

	public DaoException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public DaoException(String arg0) {
		super(arg0);
		
	}

}
