package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import java.util.LinkedHashSet;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PresentacionProductoEntity;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class PuestaMercadoDTO extends SolicitudDTO {

	@JsonProperty("nombreComercialProducto")
	@XmlElement(name = "nombreComercialProducto")
	@BeanToBeanMapping(getValueFrom = "nombreComercialProducto")
	private String nombreComercialProducto;

	@JsonProperty("antecedentesProcedencia")
	@XmlElement(name = "antecedentesProcedencia")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "antecedentesProcedencia")
	private AntecedentesProcedenciaDTO antecedentesProcedencia;

	@JsonProperty("datosIdentificativosAdicionales")
	@XmlElement(name = "datosIdentificativosAdicionales")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "datosIdentificativosAdicionales")
	private DatosIdentificativosAdicionalesDTO datosIdentificativosAdicionales;

	@JsonProperty("presentaciones")
	@XmlElement(name = "presentaciones")
	@BeanToBeanMapping(getValueFrom = "presentaciones", generateListOf = PresentacionProductoDTO.class, generateListFrom = PresentacionProductoEntity.class, listReverseSetterClass = LinkedHashSet.class)
	private List<PresentacionProductoDTO> presentaciones;

	public AntecedentesProcedenciaDTO getAntecedentesProcedencia() {
		return antecedentesProcedencia;
	}

	public void setAntecedentesProcedencia(AntecedentesProcedenciaDTO antecedentesProcedencia) {
		this.antecedentesProcedencia = antecedentesProcedencia;
	}

	public List<PresentacionProductoDTO> getPresentaciones() {
		return presentaciones;
	}

	public void setPresentaciones(List<PresentacionProductoDTO> presentaciones) {
		this.presentaciones = presentaciones;
	}

	public String getNombreComercialProducto() {
		return nombreComercialProducto;
	}

	public void setNombreComercialProducto(String nombreComercialProducto) {
		this.nombreComercialProducto = nombreComercialProducto;
	}

	public DatosIdentificativosAdicionalesDTO getDatosIdentificativosAdicionales() {
		return datosIdentificativosAdicionales;
	}

	public void setDatosIdentificativosAdicionales(DatosIdentificativosAdicionalesDTO datosIdentificativosAdicionales) {
		this.datosIdentificativosAdicionales = datosIdentificativosAdicionales;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [nombreComercialProducto=" + nombreComercialProducto + ", antecedentesProcedencia=" + antecedentesProcedencia + ", presentaciones=" + presentaciones
				+ "]";
	}

}
