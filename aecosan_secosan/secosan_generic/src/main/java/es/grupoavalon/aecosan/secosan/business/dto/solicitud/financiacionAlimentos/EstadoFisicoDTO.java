package es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class EstadoFisicoDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("dust")
	@XmlElement(name = "dust")
	@BeanToBeanMapping(getValueFrom = "dust")
	private Boolean dust;

	@JsonProperty("liquid")
	@XmlElement(name = "liquid")
	@BeanToBeanMapping(getValueFrom = "liquid")
	private Boolean liquid;

	@JsonProperty("standardSolution")
	@XmlElement(name = "standardSolution")
	@BeanToBeanMapping(getValueFrom = "standardSolution")
	private String standardSolution;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Boolean getDust() {
		return dust;
	}

	public void setDust(Boolean dust) {
		this.dust = dust;
	}

	public Boolean getLiquid() {
		return liquid;
	}

	public void setLiquid(Boolean liquid) {
		this.liquid = liquid;
	}

	public String getStandardSolution() {
		return standardSolution;
	}

	public void setStandardSolution(String standardSolution) {
		this.standardSolution = standardSolution;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dust == null) ? 0 : dust.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((liquid == null) ? 0 : liquid.hashCode());
		result = prime * result + ((standardSolution == null) ? 0 : standardSolution.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoFisicoDTO other = (EstadoFisicoDTO) obj;
		if (dust == null) {
			if (other.dust != null)
				return false;
		} else if (!dust.equals(other.dust))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (liquid == null) {
			if (other.liquid != null)
				return false;
		} else if (!liquid.equals(other.liquid))
			return false;
		if (standardSolution == null) {
			if (other.standardSolution != null)
				return false;
		} else if (!standardSolution.equals(other.standardSolution))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "EstadoFisicoDTO [id=" + id + ", dust=" + dust + ", liquid=" + liquid + ", standardSolution=" + standardSolution + "]";
	}

}
