package es.grupoavalon.aecosan.secosan.dao.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import javax.persistence.*;

@Entity
@Table(name = TableNames.TABLA_LUGAR_MEDIO_NOTIFICACION_ENTITY)
public class LugarMedioNotificacionEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.LUGAR_MEDIO_NOTIFICACION_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.LUGAR_MEDIO_NOTIFICACION_SEQUENCE, sequenceName = SequenceNames.LUGAR_MEDIO_NOTIFICACION_SEQUENCE, allocationSize = 1)
	private Long id;

	private String fax;
	private String email;
	private String address;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="UBICACION_GEOGRAFICA_ID")
	private UbicacionGeograficaEntity location;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public UbicacionGeograficaEntity getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaEntity location) {
		this.location = location;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		LugarMedioNotificacionEntity that = (LugarMedioNotificacionEntity) o;

		if (!id.equals(that.id)) return false;
		if (!fax.equals(that.fax)) return false;
		if (!address.equals(that.address)) return false;
		return email.equals(that.email);
	}

	@Override
	public int hashCode() {
		int result = fax != null ? fax.hashCode() : 0;
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (address != null ? address.hashCode() : 0);
		result = 31 * result + (location != null ? location.hashCode() : 0);
		return result;
	}
}
