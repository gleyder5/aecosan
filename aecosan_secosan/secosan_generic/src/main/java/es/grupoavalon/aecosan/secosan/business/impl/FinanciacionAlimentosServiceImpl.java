package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.FinanciacionAlimentosService;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceAddDTO;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ServicePersistenceUpdateDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.AlterOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.BajaOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.FinanciacionAlimentosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.IncOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.FinanciacionAlimentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.FinanciacionAlimentosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.PresentacionEnvaseEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class FinanciacionAlimentosServiceImpl extends AbstractSolicitudService implements FinanciacionAlimentosService {

	private Map<String, ServicePersistenceAddDTO> solicitudeAddMap;
	private Map<String, ServicePersistenceUpdateDTO> solicitudeUpdateMap;

	@Autowired
	FinanciacionAlimentosDAO financiacionAlimentosDAO;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO financiacionAlimentosDTO) {
		try {
			solicitudPrepareNullDTO(financiacionAlimentosDTO);
			solicitudeAddMapInit();
			solicitudeAddMap.get(financiacionAlimentosDTO.getClass().getSimpleName()).addSolicitudDTO(user, financiacionAlimentosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + financiacionAlimentosDTO);
		}
	}

	private void solicitudeAddMapInit() {
		if (solicitudeAddMap == null) {
			solicitudeAddMap = new HashMap<String, ServicePersistenceAddDTO>();
			solicitudeAddMap.put(EntityDtoNames.INC_OFER_ALIM_UME_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addIncOferAlimUME(user, (IncOferAlimUMEDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.ALTER_OFER_ALIM_UME_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addAlterOferAlimUME(user, (AlterOferAlimUMEDTO) solicitud);
				}
			});
			solicitudeAddMap.put(EntityDtoNames.BAJA_OFER_ALIM_UME_DTO, new ServicePersistenceAddDTO() {
				@Override
				public void addSolicitudDTO(String user, SolicitudDTO solicitud) {
					addBajaOferAlimUME(user, (BajaOferAlimUMEDTO) solicitud);
				}
			});
		}
	}

	private void addIncOferAlimUME(String user, IncOferAlimUMEDTO incOferAlimUMEDTO) {
		prepareIncOrAlterNullDTO(incOferAlimUMEDTO);
		IncOferAlimUMEEntity incOferAlimUMEEntity = generateIncOferAlimUMEEntityFromDTO(incOferAlimUMEDTO);
		setUserCreatorByUserId(user, incOferAlimUMEEntity);
		prepareIncOferAlimUMEEntity(incOferAlimUMEEntity, incOferAlimUMEDTO);
		financiacionAlimentosDAO.addIncOferAlimUME(incOferAlimUMEEntity);
		if(incOferAlimUMEDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(incOferAlimUMEEntity);
		}
	}

	private void prepareIncOrAlterNullDTO(FinanciacionAlimentosDTO finAlimUMEDTO) {
		finAlimUMEDTO.setAdministrationMode(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getAdministrationMode()));
		finAlimUMEDTO.setSolicitudePayment(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getSolicitudePayment()));
		if (null != finAlimUMEDTO.getSolicitudePayment()) {
			finAlimUMEDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getSolicitudePayment().getPayment()));
			finAlimUMEDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getSolicitudePayment().getIdPayment()));
			finAlimUMEDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getSolicitudePayment().getDataPayment()));
			if (null != finAlimUMEDTO.getSolicitudePayment().getIdPayment()) {
				finAlimUMEDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(finAlimUMEDTO.getSolicitudePayment().getIdPayment().getLocation()));
				prepareLocationNull(finAlimUMEDTO.getSolicitudePayment().getIdPayment().getLocation());
			}
		}
	}

	private IncOferAlimUMEEntity generateIncOferAlimUMEEntityFromDTO(IncOferAlimUMEDTO incOferAlimUMEDTO) {
		prepareSolicitudDTO(incOferAlimUMEDTO);
		return transformDTOToEntity(incOferAlimUMEDTO, IncOferAlimUMEEntity.class);
	}

	private void prepareIncOferAlimUMEEntity(IncOferAlimUMEEntity incOferAlimUMEEntity, IncOferAlimUMEDTO incOferAlimUMEDTO) {
		prepareAddSolicitudEntity(incOferAlimUMEEntity, incOferAlimUMEDTO);
		finanAlimUMEPreparePresentacionEntity(incOferAlimUMEEntity);
	}

	private void addAlterOferAlimUME(String user, AlterOferAlimUMEDTO financiacionAlimentosDTO) {
		prepareIncOrAlterNullDTO(financiacionAlimentosDTO);
		AlterOferAlimUMEEntity alterOferAlimUMEEntity = generateAlterOferAlimUMEEntityFromDTO(financiacionAlimentosDTO);
		setUserCreatorByUserId(user, alterOferAlimUMEEntity);
		prepareAlterOferAlimUMEEntity(alterOferAlimUMEEntity, financiacionAlimentosDTO);
		FinanciacionAlimentosEntity financiacionAlimentosEntityNew = financiacionAlimentosDAO.addAlterOferAlimUME(alterOferAlimUMEEntity);
		if(financiacionAlimentosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(financiacionAlimentosEntityNew  );
		}

	}

	private AlterOferAlimUMEEntity generateAlterOferAlimUMEEntityFromDTO(AlterOferAlimUMEDTO alterOferAlimUMEDTO) {
		prepareSolicitudDTO(alterOferAlimUMEDTO);
		return transformDTOToEntity(alterOferAlimUMEDTO, AlterOferAlimUMEEntity.class);
	}

	private void prepareAlterOferAlimUMEEntity(AlterOferAlimUMEEntity alterOferAlimUMEEntity, AlterOferAlimUMEDTO financiacionAlimentosDTO) {
		prepareAddSolicitudEntity(alterOferAlimUMEEntity, financiacionAlimentosDTO);
		finanAlimUMEPreparePresentacionEntity(alterOferAlimUMEEntity);

	}

	private void addBajaOferAlimUME(String user, BajaOferAlimUMEDTO financiacionAlimentosDTO) {
		prepareBajaNullDTO(financiacionAlimentosDTO);
		BajaOferAlimUMEEntity bajaOferAlimUMEEntity = generateBajaOferAlimUMEEntityFromDTO(financiacionAlimentosDTO);
		setUserCreatorByUserId(user, bajaOferAlimUMEEntity);
		prepareBajaOferAlimUMEEntity(bajaOferAlimUMEEntity, financiacionAlimentosDTO);
		financiacionAlimentosDAO.addBajaOferAlimUME(bajaOferAlimUMEEntity);
		if(financiacionAlimentosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(bajaOferAlimUMEEntity);
		}
	}

	private BajaOferAlimUMEEntity generateBajaOferAlimUMEEntityFromDTO(BajaOferAlimUMEDTO bajaOferAlimUMEDTO) {
		prepareSolicitudDTO(bajaOferAlimUMEDTO);
		return transformDTOToEntity(bajaOferAlimUMEDTO, BajaOferAlimUMEEntity.class);
	}

	private void prepareBajaOferAlimUMEEntity(BajaOferAlimUMEEntity bajaOferAlimUMEEntity, BajaOferAlimUMEDTO financiacionAlimentosDTO) {
		prepareAddSolicitudEntity(bajaOferAlimUMEEntity, financiacionAlimentosDTO);
		bajaOferAlimUMEPrepareProductoSuspendidoEntity(bajaOferAlimUMEEntity);
	}

	@Override
	public void update(String user, SolicitudDTO financiacionAlimentosDTO) {
		try {
			solicitudPrepareNullDTO(financiacionAlimentosDTO);
			solicitudeUpdateMapInit();
			solicitudeUpdateMap.get(financiacionAlimentosDTO.getClass().getSimpleName()).updateSolicitudDTO(user, financiacionAlimentosDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + financiacionAlimentosDTO);
		}
	}

	private void solicitudeUpdateMapInit() {
		if (solicitudeUpdateMap == null) {
			solicitudeUpdateMap = new HashMap<String, ServicePersistenceUpdateDTO>();
			solicitudeUpdateMap.put(EntityDtoNames.INC_OFER_ALIM_UME_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateIncOferAlimUME(user, (IncOferAlimUMEDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.ALTER_OFER_ALIM_UME_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateAlterOferAlimUME(user, (AlterOferAlimUMEDTO) solicitud);
				}
			});
			solicitudeUpdateMap.put(EntityDtoNames.BAJA_OFER_ALIM_UME_DTO, new ServicePersistenceUpdateDTO() {
				@Override
				public void updateSolicitudDTO(String user, SolicitudDTO solicitud) {
					updateBajaOferAlimUME(user, (BajaOferAlimUMEDTO) solicitud);
				}
			});
		}
	}

	private void updateAlterOferAlimUME(String user, AlterOferAlimUMEDTO financiacionAlimentosDTO) {
		prepareIncOrAlterNullDTO(financiacionAlimentosDTO);
		AlterOferAlimUMEEntity alterOferAlimUMEEntity = transformDTOToEntity(financiacionAlimentosDTO, AlterOferAlimUMEEntity.class);
		prepareAlterOferAlimUMEEntityUpdate(user, alterOferAlimUMEEntity, financiacionAlimentosDTO);

	}

	private void prepareAlterOferAlimUMEEntityUpdate(String user, AlterOferAlimUMEEntity alterOferAlimUMEEntity, AlterOferAlimUMEDTO financiacionAlimentosDTO) {
		setUserCreatorByUserId(user, alterOferAlimUMEEntity);
		prepareAddSolicitudEntity(alterOferAlimUMEEntity, financiacionAlimentosDTO);
		finanAlimUMEPreparePresentacionEntity(alterOferAlimUMEEntity);
		financiacionAlimentosDAO.updateAlterOferAlimUME(alterOferAlimUMEEntity);
		if(financiacionAlimentosDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(alterOferAlimUMEEntity);
		}
	}

	private void updateIncOferAlimUME(String user, IncOferAlimUMEDTO incOferAlimUMEDTO) {
		prepareIncOrAlterNullDTO(incOferAlimUMEDTO);
		IncOferAlimUMEEntity incOferAlimUMEEntity = transformDTOToEntity(incOferAlimUMEDTO, IncOferAlimUMEEntity.class);
		setUserCreatorByUserId(user, incOferAlimUMEEntity);
		prepareIncOferAlimUMEEntityUpdate(incOferAlimUMEEntity, incOferAlimUMEDTO);
		financiacionAlimentosDAO.updateIncOferAlimUME(incOferAlimUMEEntity);
		if(incOferAlimUMEDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(incOferAlimUMEEntity);
		}

	}

	private void prepareIncOferAlimUMEEntityUpdate(IncOferAlimUMEEntity incOferAlimUMEEntity, IncOferAlimUMEDTO incOferAlimUMEDTO) {
		prepareUpdateSolicitudEntity(incOferAlimUMEEntity, incOferAlimUMEDTO);
		finanAlimUMEPreparePresentacionEntity(incOferAlimUMEEntity);
	}

	private  void finanAlimUMEPreparePresentacionEntity(FinanciacionAlimentosEntity financiacionAlimentos) {
		if(financiacionAlimentos.getId()!=null) {
			financiacionAlimentosDAO.deletePresentacionEnvases(financiacionAlimentos.getId());
		}
		if (isValidNumberOfPresentations(financiacionAlimentos.getPresentations())) {
			for (PresentacionEnvaseEntity presentation : financiacionAlimentos.getPresentations()) {
				presentation.setFinanciacionAlimentos(financiacionAlimentos);
			}
		}
	}

	private void updateBajaOferAlimUME(String user, BajaOferAlimUMEDTO bajaOferAlimUMEDTO) {
		prepareBajaNullDTO(bajaOferAlimUMEDTO);
		BajaOferAlimUMEEntity bajaOferAlimUMEEntity = transformDTOToEntity(bajaOferAlimUMEDTO, BajaOferAlimUMEEntity.class);
		setUserCreatorByUserId(user, bajaOferAlimUMEEntity);
		prepareBajaOferAlimUMEEntityUpdate(bajaOferAlimUMEEntity, bajaOferAlimUMEDTO);
		financiacionAlimentosDAO.updateBajaOferAlimUME(bajaOferAlimUMEEntity);
		if(bajaOferAlimUMEDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(bajaOferAlimUMEEntity);
		}
	}

	private void prepareBajaNullDTO(BajaOferAlimUMEDTO bajaOferAlimUMEDTO) {
		bajaOferAlimUMEDTO.setCancelOrSuspension(SecosanUtil.checkDtoNullability(bajaOferAlimUMEDTO.getCancelOrSuspension()));
	}

	private void prepareBajaOferAlimUMEEntityUpdate(BajaOferAlimUMEEntity bajaOferAlimUMEEntity, BajaOferAlimUMEDTO bajaOferAlimUMEDTO) {
		prepareAddSolicitudEntity(bajaOferAlimUMEEntity, bajaOferAlimUMEDTO);
		bajaOferAlimUMEPrepareProductoSuspendidoEntity(bajaOferAlimUMEEntity);
	}

	private static boolean isValidNumberOfPresentations(List<PresentacionEnvaseEntity> presentations) {
		boolean isValid = true;
		int maxNumberOfPresentations = Properties.getInt(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_MAX_NUMBER_OF_PRESENTATIONS);
		if (presentations == null) {
			isValid = false;
		} else if (presentations.size() > maxNumberOfPresentations) {
			throw new ServiceException("The number of presentations must be <= : " + maxNumberOfPresentations);
		}
		return isValid;
	}

	private static void bajaOferAlimUMEPrepareProductoSuspendidoEntity(BajaOferAlimUMEEntity financiacionAlimentos) {
		if (isValidNumberOfProducts(financiacionAlimentos.getProducts())) {
			for (ProductoSuspendidoEntity product : financiacionAlimentos.getProducts()) {
				product.setBajaOferAlim(financiacionAlimentos);
			}
		}
	}

	private static boolean isValidNumberOfProducts(List<ProductoSuspendidoEntity> products) {
		boolean isValid = true;
		if (products == null) {
			isValid = false;
		}
		return isValid;
	}

	@Override
	public void deleteSuspendedProduct(long id) {
		try {
			this.financiacionAlimentosDAO.deleteSuspendedProduct(id);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " deleteSuspendedProduct | ERROR: " + id);
		}
	}

}
