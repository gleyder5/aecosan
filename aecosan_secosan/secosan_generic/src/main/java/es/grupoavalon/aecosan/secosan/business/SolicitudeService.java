package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;

public interface SolicitudeService {

	SolicitudDTO findSolicitudeById(String user, String id);

	void deleteSolicitudeById(String user, String id);

	public void solicitudeUpdateStatusBySolicitudeId(String user, ChangeStatusDTO statusDTO);

}
