package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class UbicacionGeograficaDTO implements IsNulable<UbicacionGeograficaDTO> {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("codigoPostal")
	@XmlElement(name = "codigoPostal")
	@BeanToBeanMapping(getValueFrom = "postalCode")
	private String codigoPostal;

	@JsonProperty("pais")
	@XmlElement(name = "pais")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "country")
	private PaisDTO country;

	@JsonProperty("provincia")
	@XmlElement(name = "provincia")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "province")
	private ProvinciaDTO province;

	@JsonProperty("municipio")
	@XmlElement(name = "municipio")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "municipio")
	private MunicipioDTO municipio;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public PaisDTO getCountry() {
		return country;
	}

	public void setCountry(PaisDTO country) {
		this.country = country;
	}

	public MunicipioDTO getMunicipio() {
		return municipio;
	}

	public void setMunicipio(MunicipioDTO municipio) {
		this.municipio = municipio;
	}

	public ProvinciaDTO getProvince() {
		return province;
	}

	public void setProvince(ProvinciaDTO province) {
		this.province = province;
	}

	@Override
	public String toString() {
		return "UbicacionGeograficaDTO [id=" + id + ", codigoPostal=" + codigoPostal + ", country=" + country + ", province=" + province + ", municipio=" + municipio + "]";
	}

	@Override
	public UbicacionGeograficaDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		} else {
			return this;
		}
	}

	private boolean checkNullability() {
		boolean nullabillity = true;

		nullabillity = nullabillity && country.shouldBeNull() == null;

		return nullabillity;
	}

}
