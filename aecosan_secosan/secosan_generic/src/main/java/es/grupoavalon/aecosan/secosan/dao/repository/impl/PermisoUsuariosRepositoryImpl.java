package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.PermisoUsuariosEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("PermisoUsuarios")
public class PermisoUsuariosRepositoryImpl extends CrudCatalogRepositoryImpl<PermisoUsuariosEntity, Long> implements GenericCatalogRepository<PermisoUsuariosEntity> {

	@Override
	protected Class<PermisoUsuariosEntity> getClassType() {

		return PermisoUsuariosEntity.class;
	}

	@Override
	public List<PermisoUsuariosEntity> getCatalogList() {
		return findAllOrderByValue();
	}

}
