package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
@Entity
@Table(name=TableNames.TABLA_CONTINENTES)
public class ContinenteEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	private Long id;
	@Column(name = ColumnNames.INE_CODE, nullable = false)
	private String ineCode;
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	

	public String getIneCode() {
		return ineCode;
	}


	public void setIneCode(String ineCode) {
		this.ineCode = ineCode;
	}


	@Override
	public String getIdAsString() {
		this.setIdAsString(id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ineCode == null) ? 0 : ineCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContinenteEntity other = (ContinenteEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ineCode == null) {
			if (other.ineCode != null)
				return false;
		} else if (!ineCode.equals(other.ineCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ContinenteEntity [id=" + id + ", ineCode=" + ineCode + "]";
	}

}
