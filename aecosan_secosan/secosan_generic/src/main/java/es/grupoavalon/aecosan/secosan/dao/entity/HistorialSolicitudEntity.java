package es.grupoavalon.aecosan.secosan.dao.entity;

import java.util.Date;

import javax.persistence.*;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@NamedQueries({
        @NamedQuery(name = NamedQueriesLibrary.GET_HISTORIAL_BY_SOLICITUD_ID, query = "SELECT dt FROM " + TableNames.ENTITY_PACKAGE
                + ".HistorialSolicitudEntity AS dt WHERE dt.solicitud.id = :solicitudId order BY dt.fecha desc"),
        @NamedQuery(name=NamedQueriesLibrary.GET_HISTORIAL_BY_SOLICITUD_ID_AND_STATUS_ID,query = "SELECT hs from "+ TableNames.ENTITY_PACKAGE+".HistorialSolicitudEntity as hs where hs.status.id = :statusId and hs.solicitud.id= :solicitudId ORDER BY hs.fecha DESC")

})
@Entity
@Table(name = TableNames.TABLA_HISTORIAL_SOLICITUD)
public class HistorialSolicitudEntity  {

    @Id
    @GeneratedValue(generator = SequenceNames.HISTORIAL_SOLICITUD_SEQUENCE)
    @SequenceGenerator(name = SequenceNames.HISTORIAL_SOLICITUD_SEQUENCE, sequenceName = SequenceNames.HISTORIAL_SOLICITUD_SEQUENCE, allocationSize = 1)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ESTADO_ID", nullable = false)
	private EstadoSolicitudEntity status;
    
    @Column(name = "FECHA", nullable = false)
    private Date fecha;

    @Column(name = "DESCRIPCION", nullable = true,length = 500)
    private String descripcion;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "SOLICITUD_ID", referencedColumnName = "ID")
    private SolicitudEntity solicitud;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DOCUMENTOS_ID", referencedColumnName = "ID")
    private DocumentacionEntity documento;

    @Transient
    private Long fechaCreacionString;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SolicitudEntity getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(SolicitudEntity solicitud) {
        this.solicitud = solicitud;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public EstadoSolicitudEntity getStatus() {
        return status;
    }

    public void setStatus(EstadoSolicitudEntity status) {
        this.status = status;
    }

    public Long getFechaCreacionString() {
        return fecha.getTime();
    }

    public void setFechaCreacionString(Long fechaCreacionString) {
        this.fechaCreacionString = fechaCreacionString;
        this.fecha = new Date(fechaCreacionString);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public DocumentacionEntity getDocumento() {
        return documento;
    }

    public void setDocumento(DocumentacionEntity documentacionEntity) {
        this.documento = documentacionEntity;
    }
}

