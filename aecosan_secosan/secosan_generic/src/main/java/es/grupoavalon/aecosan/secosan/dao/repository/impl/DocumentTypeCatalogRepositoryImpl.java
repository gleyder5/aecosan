package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("DocumentType")
public class DocumentTypeCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<TipoDocumentacionEntity, Long> implements GenericCatalogRepository<TipoDocumentacionEntity> {

	@Override
	public List<TipoDocumentacionEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<TipoDocumentacionEntity> getClassType() {

		return TipoDocumentacionEntity.class;
	}

}
