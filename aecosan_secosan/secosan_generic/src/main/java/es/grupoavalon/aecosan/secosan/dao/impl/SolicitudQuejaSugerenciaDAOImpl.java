package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.SolicitudQuejaSugerenciaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudQuejaRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudSugerenciaRepository;

@Repository
public class SolicitudQuejaSugerenciaDAOImpl extends GenericDao implements SolicitudQuejaSugerenciaDAO {

	@Autowired
	private SolicitudQuejaRepository solicitudQuejaRepository;

	@Autowired
	private SolicitudSugerenciaRepository solicitudSugerenciaRepository;

	@Override
	public SolicitudQuejaEntity addSolicitudQueja(SolicitudQuejaEntity solicitudQueja) {
		SolicitudQuejaEntity solicitudQuejaEntity = null;
		try {
			solicitudQuejaEntity = solicitudQuejaRepository.save(solicitudQueja);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addSolicitudQueja | solicitudQueja: " + solicitudQueja);
		}
		return solicitudQuejaEntity;
	}

	@Override
	public SolicitudQuejaEntity findSolicitudQuejaById(Long id) {
		SolicitudQuejaEntity solicitudQuejaEntity = null;
		try {
			solicitudQuejaEntity = solicitudQuejaRepository.findOne(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findSolicitudQuejaById | ID: " + id);
		}

		return solicitudQuejaEntity;
	}

	@Override
	public void updateSolicitudQueja(SolicitudQuejaEntity solicitudQuejaEntity) {
		try {
			solicitudQuejaRepository.update(solicitudQuejaEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateSolicitudQueja | ERROR: " + solicitudQuejaEntity);
		}

	}

	@Override
	public SolicitudSugerenciaEntity addSolicitudSugerencia(SolicitudSugerenciaEntity solicitudSugerencia) {
		SolicitudSugerenciaEntity solicitudQuejaEntity = null;
		try {
			solicitudQuejaEntity = solicitudSugerenciaRepository.save(solicitudSugerencia);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addSolicitudSugerencia | solicitudSugerencia: " + solicitudSugerencia);
		}
		return solicitudQuejaEntity;
	}

	@Override
	public void updateSolicitudSugerencia(SolicitudSugerenciaEntity solicitudSugerencia) {
		try {
			solicitudSugerenciaRepository.update(solicitudSugerencia);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "updateSolicitudSugerencia | solicitudSugerencia: " + solicitudSugerencia);
		}
	}

	@Override
	public SolicitudSugerenciaEntity findSolicitudSugerenciaById(Long id) {
		SolicitudSugerenciaEntity solicitudSugerenciaEntity = null;
		try {
			solicitudSugerenciaEntity = solicitudSugerenciaRepository.findOne(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findSolicitudsugerenciaById | ID: " + id);
		}

		return solicitudSugerenciaEntity;
	}

}
