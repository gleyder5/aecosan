package es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JustificantePagoReportDTO {

	@JsonProperty("reportName")
	@XmlElement(name = "reportName")
	private String reportName;

	@JsonProperty("pdfB64")
	@XmlElement(name = "pdfB64")
	private String pdfB64;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getPdfB64() {
		return pdfB64;
	}

	public void setPdfB64(String pdfB64) {
		this.pdfB64 = pdfB64;
	}
}
