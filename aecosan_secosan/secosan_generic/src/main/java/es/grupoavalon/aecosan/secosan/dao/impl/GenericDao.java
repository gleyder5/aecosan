/**
 * 
 */
package es.grupoavalon.aecosan.secosan.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

/**
 * @author ottoabreu
 *
 */
public abstract class GenericDao {

	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	protected void handleException(Exception e, String logMessage) {
		logger.error(logMessage, e);
		if (!(e instanceof DaoException)) {

			throw new DaoException(logMessage, e);
		} else {
			throw (DaoException) e;
		}
	}

}
