package es.grupoavalon.aecosan.secosan.dao;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;

public interface SolicitudLogoDAO {

	SolicitudLogoEntity addSolicitudLogo(SolicitudLogoEntity solicitudeLogoEntity);
	SolicitudLogoEntity findSolicitudLogoById(Long id);
	void updateSolicitudLogo(SolicitudLogoEntity solicitudeLogoEntity);

}
