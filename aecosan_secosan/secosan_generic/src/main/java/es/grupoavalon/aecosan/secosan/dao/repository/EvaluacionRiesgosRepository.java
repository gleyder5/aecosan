package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;

public interface EvaluacionRiesgosRepository extends CrudRepository<EvaluacionRiesgosEntity, Long> {

}
