package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentResponseDTO;

public interface OnlinePaymentService {

	JustifierResponseDTO getPaymentJustifier(JustifierRequestDTO justifierRequest);

	OnlinePaymentResponseDTO makePayment(OnlinePaymentRequestDTO paymentRequest);

	JustificantePagoReportDTO getJustificantePagoPDF(String identificadorPeticion,Long serviceId);

	JustificantePagoDTO getJustificantePagoBySolicitudeID(String identificadorPeticion);

}