package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoProductoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("TipoProducto")
public class TipoProductoCatalogRepositoryImpl extends CrudCatalogRepositoryImpl<TipoProductoEntity, Long> implements GenericCatalogRepository<TipoProductoEntity> {

	@Override
	public List<TipoProductoEntity> getCatalogList() {

		return findAllOrderByValue();
	}

	@Override
	protected Class<TipoProductoEntity> getClassType() {

		return TipoProductoEntity.class;
	}

}
