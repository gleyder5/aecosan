package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.EvaluacionRiesgosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.EvaluacionRiesgosRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoPagoServicioRepository;

@Repository
public class EvaluacionRiesgosDAOImpl extends GenericDao implements EvaluacionRiesgosDAO {

	@Autowired
	private EvaluacionRiesgosRepository evaluacionRiesgosRepository;

	@Autowired
	private TipoPagoServicioRepository tipoPagoServicioRepository;

	@Override
	public EvaluacionRiesgosEntity addEvaluacionRiesgos(EvaluacionRiesgosEntity evaluacionRiesgos) {
		EvaluacionRiesgosEntity evaluacionRiesgosEntity = null;
		try {
			TipoPagoServicioEntity payTypeService = getTipoPagoServicio(evaluacionRiesgos);
			evaluacionRiesgos.setPayTypeService(payTypeService);
			evaluacionRiesgosEntity = evaluacionRiesgosRepository.save(evaluacionRiesgos);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "addEvaluacionRiesgos | evaluacionRiesgos: " + evaluacionRiesgos + " | payTypeId: "
					+ evaluacionRiesgos.getPayTypeService().getPayType().getId() + " | serviceId: " + evaluacionRiesgos.getPayTypeService().getService().getId());
		}
		return evaluacionRiesgosEntity;
	}

	@Override
	public void updateEvaluacionRiesgos(EvaluacionRiesgosEntity evaluacionRiesgos) {
		try {
			TipoPagoServicioEntity payTypeService = getTipoPagoServicio(evaluacionRiesgos);
			evaluacionRiesgos.setPayTypeService(payTypeService);
			evaluacionRiesgosRepository.update(evaluacionRiesgos);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateEvaluacionRiesgos | ERROR: " + evaluacionRiesgos);
		}
	}

	private TipoPagoServicioEntity getTipoPagoServicio(EvaluacionRiesgosEntity evaluacionRiesgos) {
		TipoPagoServicioEntity payTypeService = evaluacionRiesgos.getPayTypeService();
		if (payTypeService.getService() != null && payTypeService.getPayType() != null) {
			Long payTypeId = payTypeService.getPayType().getId();
			Long serviceId = payTypeService.getService().getId();
			return tipoPagoServicioRepository.findByPayTypeIdAndServiceId(payTypeId, serviceId);
		} else {
			return null;
		}
	}
}
