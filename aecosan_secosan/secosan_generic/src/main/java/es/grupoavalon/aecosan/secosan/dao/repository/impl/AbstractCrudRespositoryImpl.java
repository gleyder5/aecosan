/**
 * 
 */
package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;

/**
 * @author ottoabreu
 *
 */
@Transactional
public abstract class AbstractCrudRespositoryImpl<T, P extends Serializable> implements
		CrudRepository<T, P> {

	@PersistenceContext
	protected EntityManager em;

	protected static final Logger logger = LoggerFactory.getLogger(AbstractCrudRespositoryImpl.class);

	protected abstract Class<T> getClassType();

	@Override
	public T save(T o) {
		em.persist(o);
		return o;
	}

	@Override
	public T findOne(P id) {
		
		return em.find(this.getClassType(), id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll() {

		Query query = this.createQuery("FROM " + this.getClassType().getName());
		return query.getResultList();

	}


	@Override
	public void update(T o) {

		em.merge(o);

	}

	@Override
	@Transactional
	public void delete(P pk) {

		em.remove(em.getReference(this.getClassType(), pk));

	}

	@SuppressWarnings("unchecked")
	@Override
	public T findOneByQuery(String query) {
		T result = null;

		try {
			result = (T) this.createQuery(query).getSingleResult();
		} catch (NoResultException e) {
			logger.debug("No result found executing:" + query);
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findOneByNamedQuery(String queryName) {
		T result = null;
		try {
			result = (T) this.findNamedQuery(queryName).getSingleResult();
		} catch (NoResultException e) {
			logger.debug("No result found executing named query:" + queryName);
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T findOneByQuery(String query, Map<String, Object> params) {
		T result = null;
		try {

			Query queryObject = this.createQuery(query);
			this.setParamsToQuery(queryObject, params);
			result = (T) queryObject.getSingleResult();

		} catch (NoResultException e) {
			logger.debug("No result found executing query:" + query
					+ " with params: " + params);
		}
		return result;

	}

	@SuppressWarnings("unchecked")
	@Override
	public T findOneByNamedQuery(String queryName, Map<String, Object> params) {
		T result = null;
		try {
			Query queryObject = this.findNamedQuery(queryName);
			this.setParamsToQuery(queryObject, params);
			result = (T) queryObject.getSingleResult();
		} catch (NoResultException e) {
			logger.debug("No result found executing named query:" + queryName
					+ " with params: " + params);
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String queryName, Map<String, Object> params) {

		Query queryObject = this.findNamedQuery(queryName);
		this.setParamsToQuery(queryObject, params);
		return queryObject.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByNamedQuery(String queryName) {

		Query queryObject = this.findNamedQuery(queryName);
		Map<String, Object> params = new HashMap<String, Object>(0);
		this.setParamsToQuery(queryObject, params);
		return queryObject.getResultList();
	}

	protected long count() {
		String entity = this.getClassType().getSimpleName();
		String queryString = "select count(ent) from " + entity + " ent";
		final Query query = createQuery(queryString);
		return (Long) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByQuery(String queryName, Map<String, Object> params) {

		Query queryObject = this.createQuery(queryName);
		this.setParamsToQuery(queryObject, params);
		return queryObject.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> findByQuery(String queryName) {

		Query queryObject = this.createQuery(queryName);
		Map<String, Object> params = new HashMap<String, Object>(0);
		this.setParamsToQuery(queryObject, params);
		return queryObject.getResultList();
	}

	protected Query createQueryWithParams(String queryString,
			Map<String, Object> params) {
		final Query query = createQuery(queryString);
		return this.setParamsToQuery(query, params);
	}

	protected Query createQuery(String queryString) {
		return this.em.createQuery(queryString);
	}

	protected Query createNamedQueryWithParams(String queryName,
			Map<String, Object> params) {
		final Query query = this.findNamedQuery(queryName);
		return this.setParamsToQuery(query, params);
	}

	protected Query findNamedQuery(String queryName) {
		return this.em.createNamedQuery(queryName);
	}

	private Query setParamsToQuery(final Query query, Map<String, Object> params) {
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query;
	}

	protected static String likeParamDoublePercentage(String param) {
		String percentageCharacter = "%";
		return percentageCharacter + param + percentageCharacter;
	}

	@Override
	public Long getSequenceNextVal(String nativeQuery) {
		logger.debug("Sequence query: {0}",nativeQuery);
		Query query = em.createNativeQuery(nativeQuery);
		@SuppressWarnings("unchecked")
		List<Object> number = query.getResultList();
		Object sequenceNumberObject = number.get(0);
		logger.debug("obtained sequence value: {0}",sequenceNumberObject);
		return Long.parseLong(sequenceNumberObject.toString());
	}

	@Override
	public Query getNamedQuery(String namedQuery) {
		return em.createNamedQuery(namedQuery);
	}

	protected static String assambleQuery(String query, QueryWhereParts queryParts) {
		List<String> parts = queryParts.getQueryParts();
		if (parts != null && !parts.isEmpty()) {
			query += " WHERE ";

			for (int i = 0; i < parts.size(); i++) {
				String part = parts.get(i);
				
				if (((parts.size() > 1) && (i > 0))
						&& (!StringUtils.isEmpty(part))) {
					query += " AND " + part;
				} else if(!StringUtils.isEmpty(part)){
					query += part;
				}
			}
		}

		return query;
	}
	
	protected static QueryWhereParts getQueryWherePartsInstance(){
		return new QueryWhereParts();
	}

	
	public static class QueryWhereParts{
		private List<String> queryParts = new ArrayList<String>();
		
		public void setPart(String part){
			if(StringUtils.isNotBlank(part)){
				queryParts.add(part);
			}
		}

		public List<String> getQueryParts() {
			return queryParts;
		}

	}
	
	@Override
	public int getTotalRecords(){
		Query query = this.createQuery("SELECT COUNT(e) FROM " + this.getClassType().getName()+" AS e");
		Object countTotal = query.getSingleResult();
		return Integer.parseInt(countTotal.toString());
	}
	
	@Override
	public int getTotalRecords(String whereClause, Map<String,Object> params){
		if(whereClause == null){
			whereClause = "";
		}
		logger.debug("where clause to use: {0}",whereClause);
		Query query = this.createQuery("SELECT COUNT(e) FROM " + this.getClassType().getName()+" AS e "+whereClause);
		if (params != null) {
			this.setParamsToQuery(query, params);
		}
		Object countTotal = query.getSingleResult();
		return Integer.parseInt(countTotal.toString());
	}
	
	@Override
	public List<T> findAllPaginatedAndSortedASC(int start,int end, String fieldName ){
		Query query = this.generateQueryFindAllOrderByASC(fieldName,"");
		return executeFindAllPaginatedAndSorted(query, start, end);
	}
	
	@Override
	public List<T> findAllPaginatedAndSortedDESC(int start,int end, String fieldName ){
		Query query = this.generateQueryFindAllOrderByDESC(fieldName,"");
		return executeFindAllPaginatedAndSorted(query, start, end);
	}
	
	@Override
	public List<T> findAllPaginatedAndSortedASC(String whereClause,int start,int end, String fieldName,  Map<String, Object> params ){
		Query query = this.generateQueryFindAllOrderByASC(fieldName,whereClause);
		this.setParamsToQuery(query, params);
		return executeFindAllPaginatedAndSorted(query, start, end);
	}
	
	@Override
	public List<T> findAllPaginatedAndSortedDESC(String whereClause ,int start,int end, String fieldName, Map<String, Object> params ){
		Query query = this.generateQueryFindAllOrderByDESC(fieldName,whereClause);
		this.setParamsToQuery(query, params);
		return executeFindAllPaginatedAndSorted(query, start, end);
	}
	
	@SuppressWarnings("unchecked")
	private List<T> executeFindAllPaginatedAndSorted(Query query,int start, int end){
		query.setFirstResult(start);
		if(end <=0){
			end = this.getTotalRecords();
		}
		query.setMaxResults(end);
		return query.getResultList();
	}
	
	private Query generateQueryFindAllOrderByASC(String fieldName,String whereSentence){
		if(whereSentence == null){
			whereSentence = "";
		}
		return this.createQuery("SELECT e FROM " + this.getClassType().getName()+" AS e "+whereSentence+" ORDER BY e."+fieldName);
	}
	
	private Query generateQueryFindAllOrderByDESC(String fieldName,String whereSentence){
		if(whereSentence == null){
			whereSentence = "";
		}
		
		return this.createQuery("SELECT e FROM " + this.getClassType().getName()+" AS e "+whereSentence+" ORDER BY e."+fieldName+" DESC");
	}

}
