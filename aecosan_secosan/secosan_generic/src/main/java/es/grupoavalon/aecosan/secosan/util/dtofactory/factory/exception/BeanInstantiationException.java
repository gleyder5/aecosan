package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception;

/**
 * Exception that indicate an error during the instantiation of the DTO
 * 
 * @author ottoabreu
 *
 */
public class BeanInstantiationException extends BeanFactoryException {

	/**
	 * INSTANTIATE_DTO_ERROR ="Can not instantiate a new DTO due an error: ";
	 */
	public static final String INSTANTIATE_DTO_ERROR = "Can not instantiate a new Bean Object due an error: ";
	/**
	 * INSTANTIATE_ENTITY_ERROR =
	 * "Can not instantiate a new Bean Reverse Object due an error: ";
	 */
	public static final String INSTANTIATE_ENTITY_ERROR = "Can not instantiate a new Bean Reverse Object due an error: ";

	public static final String INSTANTIATE_COLLECTION_ERROR = "Can not instantiate collection object for insert into object due an error: ";

	public BeanInstantiationException(String instantiateDtoError, Throwable e) {
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8558356468182079307L;

}
