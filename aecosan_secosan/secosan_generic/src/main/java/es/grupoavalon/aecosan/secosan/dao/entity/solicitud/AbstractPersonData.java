package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import es.grupoavalon.aecosan.secosan.dao.entity.UbicacionGeograficaEntity;

@MappedSuperclass
public abstract class AbstractPersonData {
	
	@Column(name="NOMBRE")
	private String name;
	@Column(name="APELLIDO")
	private String lastName;
	@Column(name="SEGUNDO_APELLIDO")
	private String secondLastName;
	@Column(name="NUM_IDENTIFICACION")
	private String identificationNumber;
	@Column(name="NUM_TELEFONO")
	private String telephoneNumber;
	@Column(name="EMAIL")
	private String email;
	@Column(name="DIRECCION")
	private String address;


	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name="UBICACION_GEOGRAFICA_ID")
	private UbicacionGeograficaEntity location;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UbicacionGeograficaEntity getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaEntity location) {
		this.location = location;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((identificationNumber == null) ? 0 : identificationNumber.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((location == null) ? 0 : location.getId().hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((secondLastName == null) ? 0 : secondLastName.hashCode());
		result = prime * result + ((telephoneNumber == null) ? 0 : telephoneNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractPersonData other = (AbstractPersonData) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (identificationNumber == null) {
			if (other.identificationNumber != null) {
				return false;
			}
		} else if (!identificationNumber.equals(other.identificationNumber)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.getId().equals(other.location.getId())) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (secondLastName == null) {
			if (other.secondLastName != null) {
				return false;
			}
		} else if (!secondLastName.equals(other.secondLastName)) {
			return false;
		}
		if (telephoneNumber == null) {
			if (other.telephoneNumber != null) {
				return false;
			}
		} else if (!telephoneNumber.equals(other.telephoneNumber)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String locationId = "";
		if (location != null) {
			locationId = String.valueOf(location.getId());

		}
		return "name=" + name + ", lastName=" + lastName + ", secondLastName=" + secondLastName + ", identificationNumber=" + identificationNumber + ", telephoneNumber=" + telephoneNumber
				+ ", email=" + email + ", address=" + address + ", location=" + locationId;
	}

}
