package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AbstractPersonAddressExtendedData extends AbstractPersonData {

	@Column(name = "TIPO_DIRECCION")
	private String addressType;
	@Column(name = "NUMERO")
	private String addressNumber;
	@Column(name = "ESCALERA")
	private String stair;
	@Column(name = "PISO")
	private String floor;
	@Column(name = "PUERTA")
	private String door;

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public String getStair() {
		return stair;
	}

	public void setStair(String stair) {
		this.stair = stair;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getDoor() {
		return door;
	}

	public void setDoor(String door) {
		this.door = door;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((addressNumber == null) ? 0 : addressNumber.hashCode());
		result = prime * result + ((addressType == null) ? 0 : addressType.hashCode());
		result = prime * result + ((door == null) ? 0 : door.hashCode());
		result = prime * result + ((floor == null) ? 0 : floor.hashCode());
		result = prime * result + ((stair == null) ? 0 : stair.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractPersonAddressExtendedData other = (AbstractPersonAddressExtendedData) obj;
		if (addressNumber == null) {
			if (other.addressNumber != null) {
				return false;
			}
		} else if (!addressNumber.equals(other.addressNumber)) {
			return false;
		}
		if (addressType == null) {
			if (other.addressType != null) {
				return false;
			}
		} else if (!addressType.equals(other.addressType)) {
			return false;
		}
		if (door == null) {
			if (other.door != null) {
				return false;
			}
		} else if (!door.equals(other.door)) {
			return false;
		}
		if (floor == null) {
			if (other.floor != null) {
				return false;
			}
		} else if (!floor.equals(other.floor)) {
			return false;
		}
		if (stair == null) {
			if (other.stair != null) {
				return false;
			}
		} else if (!stair.equals(other.stair)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "addressType=" + addressType + ", addressNumber=" + addressNumber + ", stair=" + stair + ", floor=" + floor + ", door=" + door + "," + super.toString();
	}

}
