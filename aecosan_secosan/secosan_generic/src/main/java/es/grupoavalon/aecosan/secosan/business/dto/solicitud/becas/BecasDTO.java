package es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas;

import com.fasterxml.jackson.annotation.JsonProperty;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.LugarMedioNotificacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu.RegistroReacuDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import org.apache.commons.lang.StringUtils;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;


public class BecasDTO extends SolicitudDTO implements IsNulable<BecasDTO> {


    @JsonProperty("puntosOfimatica")
    @XmlElement(name = "puntosOfimatica")
    @BeanToBeanMapping(getValueFrom = "puntosOfimatica")
    private Integer puntosOfimatica;

    @JsonProperty("puntosVoluntariado")
    @XmlElement(name = "puntosVoluntariado")
    @BeanToBeanMapping(getValueFrom = "puntosVoluntariado")
    private Integer puntosVoluntariado;

    @JsonProperty("puntosEntrevista")
    @XmlElement(name = "puntosEntrevista")
    @BeanToBeanMapping(getValueFrom = "puntosEntrevista")
    private Integer puntosEntrevista;


    @JsonProperty("puntosExpediente")
    @XmlElement(name = "puntosExpediente")
    @BeanToBeanMapping(getValueFrom = "puntosExpediente")
    private Integer puntosExpediente;

    @JsonProperty("otraTitulacion")
    @XmlElement(name = "otraTitulacion")
    @BeanToBeanMapping(getValueFrom = "otraTitulacion")
    private String otraTitulacion;

    @JsonProperty("otraTitulacionPuntos")
    @XmlElement(name = "otraTitulacionPuntos")
    @BeanToBeanMapping(getValueFrom = "otraTitulacionPuntos")
    private Double otraTitulacionPuntos;

    @JsonProperty("languages")
    @XmlElement(name = "languages")
    private List<IdiomasBecasDTO> languages;

    @JsonProperty("score")
    @XmlElement(name = "score")
    @BeanToBeanMapping(getValueFrom = "score")
    private Double score;

    public String getOtraTitulacion() {
        return otraTitulacion;
    }

    public void setOtraTitulacion(String otraTitulacion) {
        this.otraTitulacion = otraTitulacion;
    }

    public List<IdiomasBecasDTO> getLanguages() {
        return languages;
    }

    public void setLanguages(List<IdiomasBecasDTO> languages) {
        this.languages = languages;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getPuntosExpediente() {
        return puntosExpediente;
    }

    public void setPuntosExpediente(Integer puntosExpediente) {
        this.puntosExpediente = puntosExpediente;
    }

    public Integer getPuntosOfimatica() {
        return puntosOfimatica;
    }

    public void setPuntosOfimatica(Integer puntosOfimatica) {
        this.puntosOfimatica = puntosOfimatica;
    }

    public Integer getPuntosVoluntariado() {
        return puntosVoluntariado;
    }

    public void setPuntosVoluntariado(Integer puntosVoluntariado) {
        this.puntosVoluntariado = puntosVoluntariado;
    }

    public Integer getPuntosEntrevista() {
        return puntosEntrevista;
    }

    public void setPuntosEntrevista(Integer puntosEntrevista) {
        this.puntosEntrevista = puntosEntrevista;
    }

    public Double getOtraTitulacionPuntos() {
        return otraTitulacionPuntos;
    }

    public void setOtraTitulacionPuntos(Double otraTitulacionPuntos) {
        this.otraTitulacionPuntos = otraTitulacionPuntos;
    }

    @Override
    public BecasDTO shouldBeNull() {
        if (checkNullability()) {
            return null;
        }
        return this;
    }

    private boolean checkNullability() {
        boolean nullabillity = true;
        nullabillity = nullabillity && getId() == null;
        return nullabillity;
    }

}