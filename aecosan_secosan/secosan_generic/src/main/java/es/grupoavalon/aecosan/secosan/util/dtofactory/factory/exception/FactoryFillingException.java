package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception;

public class FactoryFillingException extends RuntimeException {

	public FactoryFillingException(String executeSetmethodSecurityError,
			Exception e) {
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * EXECUTE_SETMETHOD_SECURITY_ERROR
	 * ="Can not execute the set method of the object due an security error: ";
	 */
	public static final String EXECUTE_SETMETHOD_SECURITY_ERROR = "Can not execute the set method of the object due an security error: ";
	/**
	 * EXECUTE_SETMETHOD_NOMETHOD_ERROR =
	 * "Can not execute the set method because there is none that fits, check the object class: "
	 * ;
	 */
	public static final String EXECUTE_SETMETHOD_NOMETHOD_ERROR = "Can not execute the set method because there is none that fits, check the object class: ";
	/**
	 * EXECUTE_SETMETHOD_ILEGALACCES_ERROR =
	 * "Can not execute the set method of the object because the  get method is inaccessible"
	 * ;
	 */
	public static final String EXECUTE_SETMETHOD_ILEGALACCES_ERROR = "Can not execute the set method of the object because the  get method is inaccessible";
	/**
	 * EXECUTE_SETMETHOD_ILEGALARGUMENT_ERROR =
	 * "Can not execute the set method has received a specified object argument is not an instance of the class or interface declaring the underlying method "
	 * ;
	 */
	public static final String EXECUTE_SETMETHOD_ILEGALARGUMENT_ERROR = "Can not execute the set method has received a specified object argument is not an instance of the class or interface declaring the underlying method ";
	/**
	 * EXECUTE_SETMETHOD_IVOTGT_ERROR =
	 * "Can not execute the set method of the object because it has thrown an exception during the invocation"
	 * ;
	 */
	public static final String EXECUTE_SETMETHOD_IVOTGT_ERROR = "Can not execute the set method of the object because it has thrown an exception during the invocation";

	/**
	 * EXECUTE_GETMETHOD_SECURITY_ERROR
	 * ="Can not execute the get method of the object due an security error: ";
	 */
	public static final String EXECUTE_GETMETHOD_SECURITY_ERROR = "Can not execute the get method of the object due an security error: ";
	/**
	 * EXECUTE_GETMETHOD_NOMETHOD_ERROR =
	 * "Can not execute the get method because there is none that fits, check the object class: "
	 * ;
	 */
	public static final String EXECUTE_GETMETHOD_NOMETHOD_ERROR = "Can not execute the get method because there is none that fits, check the object class: ";
	/**
	 * EXECUTE_GETMETHOD_ILEGALACCES_ERROR =
	 * "Can not execute the get method of the object because the  get method is inaccessible"
	 * ;
	 */
	public static final String EXECUTE_GETMETHOD_ILEGALACCES_ERROR = "Can not execute the get method of the object because the  get method is inaccessible";
	/**
	 * EXECUTE_GETMETHOD_ILEGALARGUMENT_ERROR =
	 * "Can not execute the get method has received a specified object argument is not an instance of the class or interface declaring the underlying method "
	 * ;
	 */
	public static final String EXECUTE_GETMETHOD_ILEGALARGUMENT_ERROR = "Can not execute the get method has received a specified object argument is not an instance of the class or interface declaring the underlying method ";
	/**
	 * EXECUTE_GETMETHOD_IVOTGT_ERROR =
	 * "Can not execute the get method of the object because it has thrown an exception during the invocation"
	 * ;
	 */
	public static final String EXECUTE_GETMETHOD_IVOTGT_ERROR = "Can not execute the get method of the object because it has thrown an exception during the invocation";

}
