package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.ProductoSuspendidoRepository;

@Component
public class ProductoSuspendidoRepositoryImpl extends AbstractCrudRespositoryImpl<ProductoSuspendidoEntity, Long> implements ProductoSuspendidoRepository {

	@Override
	protected Class<ProductoSuspendidoEntity> getClassType() {
		// TODO Auto-generated method stub
		return ProductoSuspendidoEntity.class;
	}

}
