package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.AlterOferAlimUMERepository;

@Component
public class AlterOferAlimUMERepositoryImpl extends AbstractCrudRespositoryImpl<AlterOferAlimUMEEntity, Long> implements AlterOferAlimUMERepository {

	@Override
	protected Class<AlterOferAlimUMEEntity> getClassType() {
		return AlterOferAlimUMEEntity.class;
	}
}
