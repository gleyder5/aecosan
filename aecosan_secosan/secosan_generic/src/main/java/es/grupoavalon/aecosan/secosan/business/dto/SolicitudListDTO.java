package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class SolicitudListDTO {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String id;

	@JsonProperty("formulario")
	@XmlElement(name = "formulario")
	@BeanToBeanMapping(getValueFrom = "formulario", getSecondValueFrom = "catalogValue")
	private String formulario;

	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	@BeanToBeanMapping(getValueFrom = "solicitante", getSecondValueFrom = "identificationNumber")
	private String identificationNumber;

	@JsonProperty("area")
	@XmlElement(name = "area")
	@BeanToBeanMapping(getValueFrom = "area", getSecondValueFrom = "catalogValue")
	private String area;

	@JsonProperty("product")
	@XmlElement(name = "product")
	private String product;

	@JsonProperty("fechaCreacion")
	@XmlElement(name = "fechaCreacion")
	@BeanToBeanMapping(getValueFrom = "fechaCreacionString")
	private Long fechaCreacion;

	@JsonProperty("estado")
	@XmlElement(name = "estado")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "status")
	private EstadoSolicitudDTO estado;

	@JsonProperty("identificadorPeticion")
	@XmlElement(name = "identificadorPeticion")
	@BeanToBeanMapping(getValueFrom = "identificadorPeticion")
	private String identificadorPeticion;

	@JsonProperty("entityName")
	@XmlElement(name = "entityName")
	@BeanToBeanMapping(getValueFrom = "entityName")
	private String entityName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}

	public Long getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Long fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public EstadoSolicitudDTO getEstado() {
		return estado;
	}

	public void setEstado(EstadoSolicitudDTO estado) {
		this.estado = estado;
	}

	public String getIdentificadorPeticion() {
		return identificadorPeticion;
	}

	public void setIdentificadorPeticion(String identificadorPeticion) {
		this.identificadorPeticion = identificadorPeticion;
	}

	public String getEntityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

}
