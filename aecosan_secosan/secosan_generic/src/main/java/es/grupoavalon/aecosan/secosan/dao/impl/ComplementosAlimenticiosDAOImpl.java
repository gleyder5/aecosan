package es.grupoavalon.aecosan.secosan.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.ComplementosAlimenticiosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.CeseComercializacionComplementosAlimenticiosRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.ModificacionDatosComplementosAlimenticiosRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.alimentos.PuestaMercadoComplementosAlimenticiosRepository;

@Repository
public class ComplementosAlimenticiosDAOImpl extends GenericDao implements ComplementosAlimenticiosDAO {

	@Autowired
	private CeseComercializacionComplementosAlimenticiosRepository ceseComercializacionRepository;

	@Autowired
	private PuestaMercadoComplementosAlimenticiosRepository puestaMercadoRepository;

	@Autowired
	private ModificacionDatosComplementosAlimenticiosRepository modificacionDatosRepository;

	@Override
	public CeseComercializacionCAEntity addCeseComercializacion(CeseComercializacionCAEntity ceseComercializacionEntity) {

		CeseComercializacionCAEntity ceseComercializacion = null;
		try {
			ceseComercializacion = ceseComercializacionRepository.save(ceseComercializacionEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " saveCeseComercializacion | ERROR: " + ceseComercializacionEntity);
		}

		return ceseComercializacion;
	}

	@Override
	public void updateCeseComercializacion(CeseComercializacionCAEntity ceseComercializacionEntity) {
		try {
			ceseComercializacionRepository.update(ceseComercializacionEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateCeseComercializacion | ERROR: " + ceseComercializacionEntity);
		}
	}

	@Override
	public PuestaMercadoCAEntity addPuestaMercado(PuestaMercadoCAEntity puestaMercado) {
		PuestaMercadoCAEntity puestaMercadoEntity = null;
		try {
			puestaMercadoEntity = puestaMercadoRepository.save(puestaMercado);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " savePuestaMercado | ERROR: " + puestaMercado);
		}
		return puestaMercadoEntity;
	}

	@Override
	public void updatePuestaMercado(PuestaMercadoCAEntity puestaMercadoEntity) {
		try {
			puestaMercadoRepository.update(puestaMercadoEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updatePuestaMercado | ERROR: " + puestaMercadoEntity);
		}
	}


	@Override
	public ModificacionDatosCAEntity addModificacionDatos(ModificacionDatosCAEntity modificacionDatos) {
		ModificacionDatosCAEntity modificacionDatosEntity = null;
		try {
			modificacionDatosEntity = modificacionDatosRepository.save(modificacionDatos);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " saveModificacionDatos | ERROR: " + modificacionDatos);
		}
		return modificacionDatosEntity;
	}


	@Override
	public void updateModificacionDatos(ModificacionDatosCAEntity modificacionDatosEntity) {
		try {
			modificacionDatosRepository.update(modificacionDatosEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateModificacionDatos | ERROR: " + modificacionDatosEntity);
		}
	}

}
