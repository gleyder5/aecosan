package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AccountPaymentDTO {

	@JsonProperty("accountNumber")
	@XmlElement(name = "accountNumber")
	private String accountNumber;

	@JsonProperty("bankCode")
	@XmlElement(name = "bankCode")
	private String bankCode;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountNumberDecoded() {
		String accountNumberDecoded = "";
		if (StringUtils.isNotBlank(this.accountNumber)) {
			accountNumberDecoded = new String(Base64.decodeBase64(this.accountNumber));
			accountNumberDecoded = accountNumberDecoded.toUpperCase();
		}
		return accountNumberDecoded;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Override
	public String toString() {
		return "AccountPaymentDTO [accountNumber=" + accountNumber + ", bankCode=" + bankCode + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountNumber == null) ? 0 : accountNumber.hashCode());
		result = prime * result + ((bankCode == null) ? 0 : bankCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AccountPaymentDTO other = (AccountPaymentDTO) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null) {
				return false;
			}
		} else if (!accountNumber.equals(other.accountNumber)) {
			return false;
		}
		if (bankCode == null) {
			if (other.bankCode != null) {
				return false;
			}
		} else if (!bankCode.equals(other.bankCode)) {
			return false;
		}
		return true;
	}

}
