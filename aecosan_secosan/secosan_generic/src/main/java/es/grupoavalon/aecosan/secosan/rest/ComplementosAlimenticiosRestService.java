package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.CeseComercializacionComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.ModificacionDatosComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.complementosAlimenticios.PuestaMercadoComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

@Component("complementosAlimenticiosRest")
@Path(SecosanConstants.SECURE_CONTEXT + "/complementosAlimenticios")
public class ComplementosAlimenticiosRestService extends AbstractSolicitudRestService {

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/ceseComercializacion/{user}")
	public Response addCeseComercializacion(@PathParam("user") String user, CeseComercializacionComplementosAlimenticiosDTO ceseComercializacion) {
		logger.debug("-- Method addCeseComercializacion POST Init--");
		Response response = addSolicitud(user, ceseComercializacion);
		logger.debug("-- Method addCeseComercializacion POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/ceseComercializacion/{user}")
	public Response updateCeseComercializacion(@PathParam("user") String user, CeseComercializacionComplementosAlimenticiosDTO ceseComercializacion) {
		logger.debug("-- Method updateCeseComercializacion PUT Init--");
		Response response = updateSolicitud(user, ceseComercializacion);
		logger.debug("-- Method updateCeseComercializacion PUT End--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/puestaMercado/{user}")
	public Response addPuestaMercado(@PathParam("user") String user, PuestaMercadoComplementosAlimenticiosDTO puestaMercado) {
		logger.debug("-- Method addPuestaMercado POST Init--");
		Response response = addSolicitud(user, puestaMercado);
		logger.debug("-- Method addPuestaMercado POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/puestaMercado/{user}")
	public Response updatePuestaMercado(@PathParam("user") String user, PuestaMercadoComplementosAlimenticiosDTO puestaMercado) {
		logger.debug("-- Method updatePuestaMercado PUT Init--");
		Response response = updateSolicitud(user, puestaMercado);
		logger.debug("-- Method updatePuestaMercado PUT End--");
		return response;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/modificacionDatos/{user}")
	public Response addModificacionDatos(@PathParam("user") String user, ModificacionDatosComplementosAlimenticiosDTO modificacionDatos) {
		logger.debug("-- Method addModificacionDatos POST Init--");
		Response response = addSolicitud(user, modificacionDatos);
		logger.debug("-- Method addModificacionDatos POST End--");
		return response;
	}

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Path("/modificacionDatos/{user}")
	public Response updateModificacionDatos(@PathParam("user") String user, ModificacionDatosComplementosAlimenticiosDTO modificacionDatos) {
		logger.debug("-- Method updateModificacionDatos PUT Init--");
		Response response = updateSolicitud(user, modificacionDatos);
		logger.debug("-- Method updateModificacionDatos PUT End--");
		return response;
	}

}
