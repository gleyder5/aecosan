package es.grupoavalon.aecosan.secosan.util.rest.wrapper;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class BooleanWrapper {

	@XmlElement(name = "value")
	private boolean value;

	public boolean isValue() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	public BooleanWrapper(boolean value) {
		super();
		this.value = value;
	}

	public BooleanWrapper() {
		super();

	}

}
