package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface SolicitudRepository extends CrudRepository<SolicitudEntity, Long> {

	List<SolicitudEntity> getFilteredList(PaginationParams paginationParams);

	int getTotalListRecords(List<FilterParams> filters);

	int getFilteredListRecords(List<FilterParams> filters);

}
