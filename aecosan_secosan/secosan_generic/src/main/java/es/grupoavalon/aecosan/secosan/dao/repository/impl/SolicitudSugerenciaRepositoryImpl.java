package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudSugerenciaRepository;

@Component
public class SolicitudSugerenciaRepositoryImpl extends AbstractCrudRespositoryImpl<SolicitudSugerenciaEntity, Long> implements SolicitudSugerenciaRepository {

	@Override
	protected Class<SolicitudSugerenciaEntity> getClassType() {
		return SolicitudSugerenciaEntity.class;
	}
}
