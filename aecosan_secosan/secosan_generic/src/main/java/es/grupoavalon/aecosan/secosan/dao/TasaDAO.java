package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.pago.TasaModeloEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

public interface TasaDAO {

	TasaModeloEntity getTasaByFormularioId(long formularioId);

	TasaModeloEntity getTasaBySerivicioId(long servicioId);

	void updateTasa(TasaModeloEntity entity);

	List<TasaModeloEntity> getFilteredTasaList(PaginationParams paginationParams);

	int getFilteredTasaListRecords(List<FilterParams> filters);

	int getTotalTasaListRecords(List<FilterParams> filters);

}