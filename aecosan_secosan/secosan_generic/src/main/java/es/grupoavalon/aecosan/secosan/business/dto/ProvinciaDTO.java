package es.grupoavalon.aecosan.secosan.business.dto;

public class ProvinciaDTO extends AbstractCatalogDto implements IsNulable<ProvinciaDTO> {

	@Override
	public ProvinciaDTO shouldBeNull() {
		if ("".equals(this.getId())) {
			return null;
		} else {
			return this;
		}
	}
	

}
