package es.grupoavalon.aecosan.secosan.business.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.AttachableFileService;
import es.grupoavalon.aecosan.secosan.business.dto.AttachableFileDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class AttachableFileServiceImpl extends AbstractManager implements AttachableFileService {
	protected static final Logger LOGGER = LoggerFactory.getLogger(AttachableFileServiceImpl.class);

	private static final String ATTACHED_FILE_FOLDER;
	private static final String BASE_FILE_FOLDER;
	static {
		BASE_FILE_FOLDER = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_ATTACHABLES_FILE_FOLDER);
		ATTACHED_FILE_FOLDER = SecosanConstants.PATH_REPO + BASE_FILE_FOLDER;
	}

	@Override
	public AttachableFileDTO downloadAttachableFileByName(String fileName) {
		AttachableFileDTO fileToDownload = new AttachableFileDTO();
		try {
			if (fileName == null) {
				throw new ServiceException(ServiceException.FILE_ATTACHABLE_NOT_FOUND + fileName);
			} else {
				fileToDownload.setBase64(getAttachableFileInBase64(fileName));
				fileToDownload.setCatalogValue(fileName);
			}

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " downloadAttachableFileByName | fileName: " + fileName);
		}
		return fileToDownload;
	}

	private static String getAttachableFileInBase64(String fileName) throws IOException {
		String base64Content;
		try {
			base64Content = generateFile(fileName);
		} catch (FileNotFoundException e) {
			LOGGER.warn("Attachable file not found:" + ATTACHED_FILE_FOLDER + "/" + fileName + ", searching in classpath..." + BASE_FILE_FOLDER);
			base64Content = getFileFromClassPath(fileName);
			LOGGER.warn("Attachable file in classpath, consider to copy the file in the repo folder: " + BASE_FILE_FOLDER);
		}

		return base64Content;
	}

	private static String generateFile(String fileName) throws IOException {
		File file = new File(ATTACHED_FILE_FOLDER + "/" + fileName);
		byte[] fileBytes = IOUtils.toByteArray(new FileReader(file));
		return getBase64FromBytes(fileBytes);
	}

	private static String getFileFromClassPath(String fileName) throws IOException {
		String base64Content;
		InputStream is = AttachableFileServiceImpl.class.getClassLoader().getResourceAsStream(getClasspathFriendlyFolder() + "/" + fileName);
		if (is != null) {
			byte[] fileBytes = IOUtils.toByteArray(is);
			base64Content = getBase64FromBytes(fileBytes);
		} else {
			throw new ServiceException(ServiceException.FILE_ATTACHABLE_NOT_FOUND + fileName);
		}

		return base64Content;
	}

	private static final String getClasspathFriendlyFolder() {
		String classPathFriendLy = BASE_FILE_FOLDER;
		if (StringUtils.indexOf(BASE_FILE_FOLDER, "/") > -1) {
			classPathFriendLy = classPathFriendLy.substring(1);
			LOGGER.debug("classpathFriendly: {}", classPathFriendLy);
		}

		return classPathFriendLy;
	}

	private static String getBase64FromBytes(byte[] fileBytes) {

		return Base64.encodeBase64String(fileBytes);
	}

}
