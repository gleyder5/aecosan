package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.entity.HistorialSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.pago.PagoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_SOLICITUD)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = ColumnNames.SOLICITUDE_TYPE, discriminatorType = DiscriminatorType.INTEGER)
public class SolicitudEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.SOLICITUD_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.SOLICITUD_SEQUENCE, sequenceName = SequenceNames.SOLICITUD_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "IDENTIFICADOR_PETICION", nullable = false)
	private String identificadorPeticion;

	@Column(name = "NUMERO_REGISTRO")
	private String registrationNumber;

	@Column(name = "SOLICITA_COPIA_AUTENTICA")
	private Boolean solicitaCopiaAutentica;

	@Column(name = "SOLICITA_COPIA_AUTENTICA_INFO")
	private String solicitaCopiaAutenticaInfo;


	@Column(name = "FECHA_CREACION", nullable = false)
	private Date fechaCreacion;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = ColumnNames.SOLICITUDE_TYPE, nullable = false)
	private TipoSolicitudEntity formulario;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = ColumnNames.SOLICITUDE_FORM_TYPE, nullable = false)
	private TipoFormularioEntity formularioEspecifico;

	@OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name = "SOLICITANTE_ID", nullable = false)
	private SolicitanteEntity solicitante;

	@ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "USUARIO_CREADOR_ID", nullable = false)
	private UsuarioCreadorEntity userCreator;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ESTADO_ID", nullable = false)
	private EstadoSolicitudEntity status;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "AREA_ID", nullable = false)
	private AreaEntity area;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "REPRESENTANTE_ID")
	private RepresentanteEntity representante;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "DATOS_CONTACTO_ID")
	private DatosContactoEntity contactData;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitud", cascade = {CascadeType.MERGE, CascadeType.ALL})
	private List<DocumentacionEntity> documentation;

	@OneToOne(fetch = FetchType.EAGER, mappedBy = "solicitud", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
	private PagoSolicitudEntity payment;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "solicitud", cascade = { CascadeType.MERGE, CascadeType.ALL })
	private List<HistorialSolicitudEntity> historialSolicitud;

	@Transient
	private Long fechaCreacionString;
	
	@Transient
	private String entityName;

	public List<HistorialSolicitudEntity> getHistorialSolicitud() {
		return historialSolicitud;
	}

	public void setHistorialSolicitud(List<HistorialSolicitudEntity> historial) {
		this.historialSolicitud = historial;
	}

	public List<DocumentacionEntity> getDocumentation() {
		return documentation;
	}


	public void setDocumentation(List<DocumentacionEntity> documentation) {
		this.documentation = documentation;
	}

	public Long getFechaCreacionString() {
		return fechaCreacion.getTime();
	}

	public void setFechaCreacionString(Long fechaCreacionString) {
		this.fechaCreacionString = fechaCreacionString;
		this.fechaCreacion = new Date(fechaCreacionString);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoSolicitudEntity getFormulario() {
		return formulario;
	}

	public void setFormulario(TipoSolicitudEntity formulario) {
		this.formulario = formulario;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public EstadoSolicitudEntity getStatus() {
		return status;
	}

	public void setStatus(EstadoSolicitudEntity status) {
		this.status = status;
	}

	public RepresentanteEntity getRepresentante() {
		return representante;
	}

	public void setRepresentante(RepresentanteEntity representante) {
		this.representante = representante;
	}

	public SolicitanteEntity getSolicitante() {
		return solicitante;
	}

	public void setSolicitante(SolicitanteEntity solicitante) {
		this.solicitante = solicitante;
	}

	public AreaEntity getArea() {
		return area;
	}

	public void setArea(AreaEntity area) {
		this.area = area;
	}

	public DatosContactoEntity getContactData() {
		return contactData;
	}

	public void setContactData(DatosContactoEntity contactData) {
		this.contactData = contactData;
	}

	public String getIdentificadorPeticion() {
		return identificadorPeticion;
	}

	public void setIdentificadorPeticion(String identificadorPeticion) {
		this.identificadorPeticion = identificadorPeticion;
	}

	public void loadLazyDocuments() {
		this.getDocumentation();
	}

	public String getEntityName() {
		return getClass().getSimpleName();
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public UsuarioCreadorEntity getUserCreator() {
		return userCreator;
	}

	public void setUserCreator(UsuarioCreadorEntity userCreator) {
		this.userCreator = userCreator;
	}

	public TipoFormularioEntity getFormularioEspecifico() {
		return formularioEspecifico;
	}

	public void setFormularioEspecifico(TipoFormularioEntity formularioEspecifico) {
		this.formularioEspecifico = formularioEspecifico;
	}

	public String getRegistrationNumber() {
		return registrationNumber;
	}

	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}

	public PagoSolicitudEntity getPayment() {
		return payment;
	}

	public void setPayment(PagoSolicitudEntity payment) {
		if (payment != null) {
			payment.setSolicitud(this);
		}
		this.payment = payment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.getId().hashCode());
		result = prime * result + ((contactData == null) ? 0 : contactData.getId().hashCode());
		result = prime * result + ((fechaCreacion == null) ? 0 : fechaCreacion.hashCode());
		result = prime * result + ((fechaCreacionString == null) ? 0 : fechaCreacionString.hashCode());
		result = prime * result + ((formulario == null) ? 0 : formulario.getId().hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((identificadorPeticion == null) ? 0 : identificadorPeticion.hashCode());
		result = prime * result + ((registrationNumber == null) ? 0 : registrationNumber.hashCode());
		result = prime * result + ((representante == null) ? 0 : representante.getId().hashCode());
		result = prime * result + ((solicitante == null) ? 0 : solicitante.getId().hashCode());
		result = prime * result + ((status == null) ? 0 : status.getId().hashCode());
		result = prime * result + ((formularioEspecifico == null) ? 0 : formularioEspecifico.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SolicitudEntity other = (SolicitudEntity) obj;
		if (area == null) {
			if (other.area != null) {
				return false;
			}
		} else if (!area.getId().equals(other.area.getId())) {
			return false;
		}
		if (contactData == null) {
			if (other.contactData != null) {
				return false;
			}
		} else if (!contactData.getId().equals(other.contactData.getId())) {
			return false;
		}
		if (fechaCreacion == null) {
			if (other.fechaCreacion != null) {
				return false;
			}
		} else if (!fechaCreacion.equals(other.fechaCreacion)) {
			return false;
		}
		if (fechaCreacionString == null) {
			if (other.fechaCreacionString != null) {
				return false;
			}
		} else if (!fechaCreacionString.equals(other.fechaCreacionString)) {
			return false;
		}
		if (formulario == null) {
			if (other.formulario != null) {
				return false;
			}
		} else if (!formulario.getId().equals(other.formulario.getId())) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (identificadorPeticion == null) {
			if (other.identificadorPeticion != null) {
				return false;
			}
		} else if (!identificadorPeticion.equals(other.identificadorPeticion)) {
			return false;
		}
		if (registrationNumber == null) {
			if (other.registrationNumber != null) {
				return false;
			}
		} else if (!registrationNumber.equals(other.registrationNumber)) {
			return false;
		}
		if (representante == null) {
			if (other.representante != null) {
				return false;
			}
		} else if (!representante.getId().equals(other.representante.getId())) {
			return false;
		}
		if (solicitante == null) {
			if (other.solicitante != null) {
				return false;
			}
		} else if (!solicitante.getId().equals(other.solicitante.getId())) {
			return false;
		}
		if (status == null) {
			if (other.status != null) {
				return false;
			}
		} else if (!status.getId().equals(other.status.getId())) {
			return false;
		}
		return true;
	}

	public Boolean getSolicitaCopiaAutentica() {
		return solicitaCopiaAutentica;
	}

	public void setSolicitaCopiaAutentica(Boolean solicitaCopiaAutentica) {
		this.solicitaCopiaAutentica = solicitaCopiaAutentica;
	}

	public String getSolicitaCopiaAutenticaInfo() {
		return solicitaCopiaAutenticaInfo;
	}

	public void setSolicitaCopiaAutenticaInfo(String solicitaCopiaAutenticaInfo) {
		this.solicitaCopiaAutenticaInfo = solicitaCopiaAutenticaInfo;
	}

	@Override
	public String toString() {

		return "SolicitudEntity [id=" + id + ", identificadorPeticion=" + identificadorPeticion + ", fechaCreacion=" + fechaCreacion + ", formulario=" + formulario + ", solicitante=" + solicitante
				+ ", status=" + status + ", area=" + area + ", representante=" + representante + ", contactData=" + contactData + ", fechaCreacionString=" + fechaCreacionString + "]";
	}

}
