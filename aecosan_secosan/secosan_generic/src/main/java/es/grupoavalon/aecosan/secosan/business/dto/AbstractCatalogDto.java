package es.grupoavalon.aecosan.secosan.business.dto;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;

public class AbstractCatalogDto {

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "idAsString")
	protected String id;

	@JsonProperty("catalogValue")
	@XmlElement(name = "catalogValue")
	@IgnoreField(inReverse = true)
	@BeanToBeanMapping(getValueFrom = "catalogValue")
	private String catalogValue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCatalogValue() {
		return catalogValue;
	}

	public void setCatalogValue(String catalogValue) {
		this.catalogValue = catalogValue;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [catalogValue=" + catalogValue + "]";
	}

}
