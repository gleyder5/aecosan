package es.grupoavalon.aecosan.secosan.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_MONITORIZACION_CONECTORES)
public class MonitorizacionConectoresEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.MONITORIZACION_CONECTORES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.MONITORIZACION_CONECTORES_SEQUENCE, sequenceName = SequenceNames.MONITORIZACION_CONECTORES_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "FECHA_INVOCACION", nullable = false)
	private Date registerDate;

	@Column(name = "SISTEMA", nullable = false)
	private String connectorName;

	@Column(name = "RESULTADO_OPERACION", nullable = false)
	private Boolean success;

	@Column(name = "CLASE_EXCEPCION")
	private String errorType;

	@Column(name = "DESCRIPCION_ERROR", length = 1000)
	private String errorDescription;

	@Transient
	private Long dateLong;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getRegisterDate() {
		return registerDate;
	}

	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}

	public void setRegisterDate(Long registerDate) {
		this.dateLong = registerDate;
		this.registerDate = new Date(registerDate);
	}

	public String getConnectorName() {
		return connectorName;
	}

	public void setConnectorName(String connectorName) {
		this.connectorName = connectorName;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((connectorName == null) ? 0 : connectorName.hashCode());
		result = prime * result + ((errorDescription == null) ? 0 : errorDescription.hashCode());
		result = prime * result + ((errorType == null) ? 0 : errorType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((registerDate == null) ? 0 : registerDate.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MonitorizacionConectoresEntity other = (MonitorizacionConectoresEntity) obj;
		if (connectorName == null) {
			if (other.connectorName != null) {
				return false;
			}
		} else if (!connectorName.equals(other.connectorName)) {
			return false;
		}
		if (errorDescription == null) {
			if (other.errorDescription != null) {
				return false;
			}
		} else if (!errorDescription.equals(other.errorDescription)) {
			return false;
		}
		if (errorType == null) {
			if (other.errorType != null) {
				return false;
			}
		} else if (!errorType.equals(other.errorType)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (registerDate == null) {
			if (other.registerDate != null) {
				return false;
			}
		} else if (!registerDate.equals(other.registerDate)) {
			return false;
		}
		if (success == null) {
			if (other.success != null) {
				return false;
			}
		} else if (!success.equals(other.success)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "MonitorizacionConectoresEntity [id=" + id + ", registerDate=" + registerDate + ", connectorName=" + connectorName + ", success=" + success + ", errorType=" + errorType
				+ ", errorDescription=" + errorDescription + "]";
	}

}
