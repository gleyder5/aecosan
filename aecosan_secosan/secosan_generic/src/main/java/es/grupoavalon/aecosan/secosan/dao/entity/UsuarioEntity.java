package es.grupoavalon.aecosan.secosan.dao.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = ColumnNames.USER_TYPE, discriminatorType = DiscriminatorType.INTEGER)
@Table(name = TableNames.TABLA_USUARIO)
@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_USUARIO_BY_IDENTIFICATORNUMBER, query = "FROM " + TableNames.ENTITY_PACKAGE
		+ ".UsuarioEntity AS u WHERE UPPER( u.identificationNumber ) = UPPER( :identificationNumber )") })
public abstract class UsuarioEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.USUARIO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.USUARIO_SEQUENCE, sequenceName = SequenceNames.USUARIO_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "NOMBRE")
	private String name;

	@Column(name = "APELLIDO")
	private String lastName;

	@Column(name = "SEGUNDO_APELLIDO")
	private String secondLastName;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "NUMERO_ID_PERSONA", nullable = false)
	private String identificationNumber;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ROL_ID", nullable = false)
	private UsuarioRolEntity role;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = ColumnNames.USER_TYPE, nullable = false)
	private TipoUsuarioEntity tipo;

	@Column(name = ColumnNames.LAST_LOGIN, nullable = true)
	private Timestamp lastLogin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public UsuarioRolEntity getRole() {
		return role;
	}

	public void setRole(UsuarioRolEntity role) {
		this.role = role;
	}

	public String getSecondLastName() {
		return secondLastName;
	}

	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}

	public TipoUsuarioEntity getTipo() {
		return tipo;
	}

	public void setTipo(TipoUsuarioEntity tipo) {
		this.tipo = tipo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((identificationNumber == null) ? 0 : identificationNumber.hashCode());
		result = prime * result + ((lastLogin == null) ? 0 : lastLogin.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		result = prime * result + ((secondLastName == null) ? 0 : secondLastName.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UsuarioEntity other = (UsuarioEntity) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (identificationNumber == null) {
			if (other.identificationNumber != null) {
				return false;
			}
		} else if (!identificationNumber.equals(other.identificationNumber)) {
			return false;
		}
		if (lastLogin == null) {
			if (other.lastLogin != null) {
				return false;
			}
		} else if (!lastLogin.equals(other.lastLogin)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (role == null) {
			if (other.role != null) {
				return false;
			}
		} else if (!role.equals(other.role)) {
			return false;
		}
		if (secondLastName == null) {
			if (other.secondLastName != null) {
				return false;
			}
		} else if (!secondLastName.equals(other.secondLastName)) {
			return false;
		}
		if (tipo == null) {
			if (other.tipo != null) {
				return false;
			}
		} else if (!tipo.equals(other.tipo)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer("UsuarioEntity{");
		sb.append("id=").append(id);
		sb.append(", name='").append(name).append('\'');
		sb.append(", lastName='").append(lastName).append('\'');
		sb.append(", secondLastName='").append(secondLastName).append('\'');
		sb.append(", email='").append(email).append('\'');
		sb.append(", identificationNumber='").append(identificationNumber).append('\'');
		sb.append(", role=").append(role);
		sb.append(", tipo=").append(tipo);
		sb.append(", lastLogin=").append(lastLogin);
		sb.append('}');
		return sb.toString();
	}
}
