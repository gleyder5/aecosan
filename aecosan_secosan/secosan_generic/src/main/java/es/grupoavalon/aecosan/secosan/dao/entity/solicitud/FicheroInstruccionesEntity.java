package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import es.grupoavalon.aecosan.secosan.util.EntityDtoNames;

@NamedQueries({
		@NamedQuery(name = NamedQueriesLibrary.GET_INSTRUCTIONS_BY_SOLICITUDETYPE, query = "FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD + "." + EntityDtoNames.FICHERO_INSTRUCCIONES_ENTITY
				+ " AS e WHERE e.formType.id = :solicitudeType "),
		@NamedQuery(name = NamedQueriesLibrary.GET_INSTRUCTIONS_BY_LIST_AREA, query = "FROM " + TableNames.ENTITY_PACKAGE_SOLICITUD + "." + EntityDtoNames.FICHERO_INSTRUCCIONES_ENTITY
				+ " AS e WHERE e.formType.solicitudeType.relatedArea.id = :area ") })
@Entity
@Table(name = TableNames.TABLA_FICHERO_INSTRUCCIONES)
public class FicheroInstruccionesEntity extends AbstractCatalogEntity {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.FICHERO_INSTRUCCIONES_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.FICHERO_INSTRUCCIONES_SEQUENCE, sequenceName = SequenceNames.FICHERO_INSTRUCCIONES_SEQUENCE, allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "TIPO_FORMULARIO_ID", nullable = false)
	private TipoFormularioEntity formType;

	@Override
	public String getIdAsString() {
		this.setIdAsString(this.id);
		return this.idAsString;
	}

	@Override
	public void setIdAsString(String idAsString) {
		id = Long.valueOf(idAsString);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoFormularioEntity getFormType() {
		return formType;
	}

	public void setFormType(TipoFormularioEntity formType) {
		this.formType = formType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((formType == null) ? 0 : formType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		FicheroInstruccionesEntity other = (FicheroInstruccionesEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (formType == null) {
			if (other.formType != null) {
				return false;
			}
		} else if (!formType.getId().equals(other.formType.getId())) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		String solicitudeTypeId = "";
		if (formType != null) {
			solicitudeTypeId = formType.getIdAsString();
		}
		return "FicheroInstruccionesEntity [id=" + id + ", solicitudeType=" + solicitudeTypeId + "]";
	}

}
