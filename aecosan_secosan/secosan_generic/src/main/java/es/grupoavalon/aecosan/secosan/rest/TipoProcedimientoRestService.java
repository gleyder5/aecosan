package es.grupoavalon.aecosan.secosan.rest;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.EstadoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoProcedimientoDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Component("tipoProcedimientoRest")
public class TipoProcedimientoRestService {

	private static final String DEFAULT_REST_URL =SecosanConstants.SECURE_CONTEXT + "/tipoProcedimiento/";

	private static final String ADMIN_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/tipoProcedimiento/";

	private static final Logger logger = LoggerFactory.getLogger(TipoProcedimientoRestService.class);

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;


	private Response listar(){
		logger.debug("-- Method getTipoProcedimiento GET Init --");
		Response response;

		List<TipoProcedimientoDTO> listStatus;

		try {

			logger.debug("-- Method getTipoProcedimiento ");
			listStatus = bfacade.getTipoProcedimientoList();

			response = responseFactory.generateOkGenericResponse(listStatus);

		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method getTipoProcedimiento GET End --");
		return response;
	}
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"list")
	public Response listItemsAdmin() {
			return listar();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL+"list")
	public Response listItemsDefault() {
			return listar();
	}


	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"add")
	public Response addTipoProcedimiento(TipoProcedimientoDTO tipoProcedimientoDTO) {
		logger.debug("-- Method addTipoProcedimiento POST Init --");
		Response response;
		try {
			logger.debug("-- Method addTipoProcedimiento ");
			bfacade.addTipoProcedimiento(tipoProcedimientoDTO);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		logger.debug("-- Method addTipoProcedimiento POST End --");
		return response;
	}

	@DELETE
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(ADMIN_REST_URL+"delete/{userId}/{tipoProcedimientoId}")
	public Response deleteTipoProcedimiento(@PathParam("userId") String userId, @PathParam("tipoProcedimientoId") String tipoProcedimientoId) {
		logger.debug("-- Method deleteTipoProcedimiento DELETE Init --");
		Response response;
		try {
			logger.debug("-- Method deleteTipoProcedimiento -- ");
			bfacade.deleteTipoProcedimiento(userId,tipoProcedimientoId);
			response = responseFactory.generateOkResponse();
		} catch (Exception e) {
			response = responseFactory.generateErrorResponse(e);
		}
		return response;
	}


}
