package es.grupoavalon.aecosan.secosan.dao.repository;

import java.util.List;

public interface GenericComplexCatalogRepository<T> extends GenericCatalogRepository<T> {

	List<T> getCatalogListByParent(String parentElement, String parentId);

}
