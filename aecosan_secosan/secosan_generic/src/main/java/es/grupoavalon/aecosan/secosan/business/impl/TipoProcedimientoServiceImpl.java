package es.grupoavalon.aecosan.secosan.business.impl;


import es.grupoavalon.aecosan.secosan.business.TipoProcedimientoService;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoProcedimientoDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.TipoProcedimientoDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoProcedimientoEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoProcedimientoServiceImpl extends  AbstractManager implements TipoProcedimientoService {

	@Autowired
	private TipoProcedimientoDAO tipoProcedimientoDAO;

	@Override
	public void add(TipoProcedimientoDTO tipoProcedimientoDTO) {
		try {

//			prepareTipoProcedimientoNullability(tipoProcedimientoDTO);
			addTipoProcedimientoEntity(tipoProcedimientoDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + tipoProcedimientoDTO);
		}

	}




	@Override
	public void update(TipoProcedimientoDTO tipoProcedimientoDTO) {
		try {
//			solicitudPrepareNullDTO(tipoProcedimientoDTO);
//			prepareEvaluacionRiesgosNullability((EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
			updateTipoProcedimiento(tipoProcedimientoDTO );
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + tipoProcedimientoDTO);
		}
	}

	@Override
	public void delete(String userId, String tipoProcedimientoId) {
		try {
//			solicitudPrepareNullDTO(tipoProcedimientoDTO);
//			prepareEvaluacionRiesgosNullability((EvaluacionRiesgosDTO) evaluacionRiesgosDTO);
			deleteTipoProcedimiento(tipoProcedimientoId);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " delete | ERROR: " + tipoProcedimientoId);
		}
	}

	private void addTipoProcedimientoEntity( TipoProcedimientoDTO tipoProcedimientoDTO) {
		TipoProcedimientoEntity tipoProcedimientoEntity	=	generateTipoProcedimientoEntityFromDTO(tipoProcedimientoDTO);
		tipoProcedimientoDAO.addTipoProcedimientoEntity(tipoProcedimientoEntity);
		
	}
	private void deleteTipoProcedimiento(  String tipoProcedimientoId) {
		tipoProcedimientoDAO.deleteTipoProcedimientoEntity(Long.valueOf(tipoProcedimientoId));
	}

	private void updateTipoProcedimiento(TipoProcedimientoDTO tipoProcedimientoDTO) {
		TipoProcedimientoEntity tipoProcedimientoEntity= transformDTOToEntity(tipoProcedimientoDTO, TipoProcedimientoEntity.class);
		tipoProcedimientoDAO.updateTipoProcedimientoEntity(tipoProcedimientoEntity);
	}

	private TipoProcedimientoEntity generateTipoProcedimientoEntityFromDTO(TipoProcedimientoDTO tipoProcedimiento) {
		return transformDTOToEntity(tipoProcedimiento,TipoProcedimientoEntity.class);
	}

	@Override
	public List<TipoProcedimientoDTO> getTipoProcedimientoDTOList() {
		List<TipoProcedimientoDTO> tipoProcedimientoDTOList = null;

		try {
			List<TipoProcedimientoEntity> tipoProcedimientoList = tipoProcedimientoDAO.getTipoProcedimientoList();
			tipoProcedimientoDTOList = transformEntityListToDTOList(tipoProcedimientoList, TipoProcedimientoEntity.class,TipoProcedimientoDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "gTipoProcedimientoDTOList |  ERROR: " + e.getMessage());
		}
		return tipoProcedimientoDTOList;
	}

	//TODO
	private void prepareTipoProcedimientoDTONullability(TipoProcedimientoDTO tipoProcedimientoDTO) {

//		if (null != tipoProcedimientoDTO.ge()) {
//			tipoProcedimientoDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(tipoProcedimientoDTO.getSolicitudePayment().getPayment()));
//			tipoProcedimientoDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(tipoProcedimientoDTO.getSolicitudePayment().getIdPayment()));
//			tipoProcedimientoDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(tipoProcedimientoDTO.getSolicitudePayment().getDataPayment()));
//			if (null != tipoProcedimientoDTO.getSolicitudePayment().getIdPayment()) {
//				tipoProcedimientoDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(tipoProcedimientoDTO.getSolicitudePayment().getIdPayment().getLocation()));
//				prepareLocationNull(tipoProcedimientoDTO.getSolicitudePayment().getIdPayment().getLocation());
//			}
//		}
//		if (null != tipoProcedimientoDTO) {
//			tipoProcedimientoDTO.setPaisOrigen(SecosanUtil.checkDtoNullability(tipoProcedimientoDTO.getPaisOrigen()));
//		}
	}
}
