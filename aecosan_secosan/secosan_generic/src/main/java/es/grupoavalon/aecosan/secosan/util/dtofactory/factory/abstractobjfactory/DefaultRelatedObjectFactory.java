package es.grupoavalon.aecosan.secosan.util.dtofactory.factory.abstractobjfactory;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class DefaultRelatedObjectFactory extends RelatedObjectFactory {


	public DefaultRelatedObjectFactory(Field field, Object entity) {
		super(field, entity);
	}

	@Override
	public Object instanciateObject() throws IllegalArgumentException,
			SecurityException, InstantiationException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {
	
		return newInstance(relatedObjectClass);
	}
	
}
