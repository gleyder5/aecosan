package es.grupoavalon.aecosan.secosan.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.CatalogService;
import es.grupoavalon.aecosan.secosan.business.dto.CatalogDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;

@Service
public class CatalogServiceImpl extends AbstractManager implements CatalogService {

	@Autowired
	private CatalogDAO catalogDAO;

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName) {
		List<CatalogDTO> catalogListDTOs = null;

		try {
			List<AbstractCatalogEntity> catalogListEntities = catalogDAO.getCatalogItems(catalogName);
			catalogListDTOs = transformEntityListToDTOList(catalogListEntities, AbstractCatalogEntity.class, CatalogDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "catalogListEntities | catalogName: " + catalogName + " | ERROR: " + e.getMessage());
		}
		return catalogListDTOs;
	}

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName, String parentElement, String parentId) {
		List<CatalogDTO> catalogListDTOs = null;

		try {
			List<AbstractCatalogEntity> catalogListEntities = catalogDAO.getCatalogItems(catalogName, parentElement, parentId);
			catalogListDTOs = transformEntityListToDTOList(catalogListEntities, AbstractCatalogEntity.class, CatalogDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getCatalogItems | catalogName: " + catalogName + " | parentElement:  " + parentElement + " | parentId: " + parentId + "| ERROR: "
					+ e.getMessage());
		}

		return catalogListDTOs;
	}

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName, String tag) {
		List<CatalogDTO> catalogListDTOs = null;

		try {
			List<AbstractCatalogEntity> catalogListEntities = catalogDAO.getCatalogItems(catalogName, tag);
			catalogListDTOs = transformEntityListToDTOList(catalogListEntities, AbstractCatalogEntity.class, CatalogDTO.class);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getCatalogItems | catalogName: " + catalogName + " | tag:  " + tag + " | ERROR: " + e.getMessage());
		}

		return catalogListDTOs;
	}

}
