package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.SolicitudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.DocumentacionDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.DocumentacionRepository;
import es.grupoavalon.aecosan.secosan.dao.repository.TipoDocumentacionRepository;


@Repository
public class DocumentacionDAOImpl extends GenericDao implements DocumentacionDAO {

	@Autowired
	private DocumentacionRepository documentacionRepository;

	@Autowired
	private SolicitudRepository solicitudRepository;

	@Autowired
	private TipoDocumentacionRepository tipoDocumentacionRepository;

	@Override
	public DocumentacionEntity findDocumentacionById(Long idDocumentacion) {
		DocumentacionEntity documentacionEntity = null;
		try {
			documentacionEntity = documentacionRepository.findOne(idDocumentacion);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " findDocumentacionById | id: " + idDocumentacion + " | " + exception.getMessage());
		}
		return documentacionEntity;
	}

	@Override
	public DocumentacionEntity addDocumentacionToSolicitude(SolicitudEntity solicitudEntity, DocumentacionEntity documentacionEntity) {
		DocumentacionEntity documentacionEntitySaved =null;
		try {
			SolicitudEntity solicitud =  solicitudRepository.findOne(solicitudEntity.getId());
			documentacionEntity.setSolicitud(solicitud);
			documentacionEntitySaved  =documentacionRepository.save(documentacionEntity);

		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addDocumentacionToSolicitude | id  "+documentacionEntity+ " | " + exception.getMessage());
		}
		return  documentacionEntitySaved;
	}

	@Override
	public void deleteDocumentacion(Long idDocumentacion) {
		try {
			documentacionRepository.delete(idDocumentacion);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteDocumentacion | id: " + idDocumentacion + " | " + exception.getMessage());
		}
	}

	@Override
	public List<TipoDocumentacionEntity> findDocumentationByFormId(String formId) {
		List<TipoDocumentacionEntity> listTipoDocumentacionEntity = null;
		try {
			Map<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put("formId", Long.valueOf(formId));

			listTipoDocumentacionEntity = tipoDocumentacionRepository.findByNamedQuery(NamedQueriesLibrary.GET_DOCUMENT_TYPE_BY_FORM_ID, queryParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " findDocumentationByFormId | formId: " + formId + " | " + e.getMessage());
		}

		return listTipoDocumentacionEntity;
	}

}
