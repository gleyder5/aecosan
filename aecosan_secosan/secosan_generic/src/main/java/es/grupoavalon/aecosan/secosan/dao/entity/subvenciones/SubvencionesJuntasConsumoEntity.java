package es.grupoavalon.aecosan.secosan.dao.entity.subvenciones;

import es.grupoavalon.aecosan.secosan.dao.entity.LugarMedioNotificacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import javax.persistence.*;


@Entity
@Table(name = TableNames.TABLA_SUBVENCIONES_JUNTAS_ENTITY)
public class SubvencionesJuntasConsumoEntity extends SolicitudEntity {
//SUBVENCIONES A LAS JUNTAS ARBITRALES DE CONSUMO SIA: 991407

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "LUGAR_MEDIO_NOTIFICACION_ID")
    private LugarMedioNotificacionEntity lugarOMedio;

    public LugarMedioNotificacionEntity getLugarOMedio() {
        return lugarOMedio;
    }

    public void setLugarOMedio(LugarMedioNotificacionEntity lugarOMedio) {
        this.lugarOMedio = lugarOMedio;
    }
}
