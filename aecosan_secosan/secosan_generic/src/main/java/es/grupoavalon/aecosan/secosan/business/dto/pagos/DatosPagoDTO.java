package es.grupoavalon.aecosan.secosan.business.dto.pagos;

import javax.xml.bind.annotation.XmlElement;

import es.grupoavalon.aecosan.secosan.dao.repository.impl.AbstractCrudRespositoryImpl;
import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.LongToDateTransformer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class DatosPagoDTO implements IsNulable<DatosPagoDTO> {

	protected static final Logger logger = LoggerFactory.getLogger(DatosPagoDTO.class);

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("entity")
	@XmlElement(name = "entity")
	@BeanToBeanMapping(getValueFrom = "entity")
	private String entity;

	@JsonProperty("account")
	@XmlElement(name = "account")
	@BeanToBeanMapping(getValueFrom = "account")
	private String account;

	@JsonProperty("nrc")
	@XmlElement(name = "nrc")
	@BeanToBeanMapping(getValueFrom = "nrc")
	private String nrc;

	@JsonProperty("cardNumber")
	@XmlElement(name = "cardNumber")
	@BeanToBeanMapping(getValueFrom = "cardNumber")
	private String cardNumber;

	@JsonProperty("expirationCardDate")
	@XmlElement(name = "expirationCardDate")
	@BeanToBeanMapping(getValueFrom = "expirationCardDate", useCustomTransformer = LongToDateTransformer.class)
	private Long expirationCardDate;

	@JsonProperty("paymentDate")
	@XmlElement(name = "paymentDate")
	@BeanToBeanMapping(getValueFrom = "paymentDate", useCustomTransformer = LongToDateTransformer.class)
	private Long paymentDate;
	
	public Long getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Long paymentDate) {
		this.paymentDate = paymentDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getNrc() {
		return nrc;
	}

	public void setNrc(String nrc) {
		this.nrc = nrc;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {

		if (StringUtils.isBlank(cardNumber)) {
			this.cardNumber = cardNumber;
		} else {
			this.cardNumber = SecosanUtil.maskCreditCard(cardNumber);
		}
	}

	public Long getExpirationCardDate() {
		return expirationCardDate;
	}

	public void setExpirationCardDate(Long expirationCardDate) {
		this.expirationCardDate = expirationCardDate;
	}
	@Override
	public DatosPagoDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}

	private boolean checkNullability() {
		boolean nullabillity = true;

		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getEntity());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getAccount());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getNrc());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getCardNumber());
		nullabillity = nullabillity && getExpirationCardDate() == null;

		return nullabillity;
	}
	@Override
	public String toString() {
		return "DatosPagoDTO [id=" + id + ", entity=" + entity + ", account=" + account + ", nrc=" + nrc + ", cardNumber=" + cardNumber + ", expirationCardDate="
				+ expirationCardDate + ", paymentDate=" + paymentDate + "]";
	}

}
