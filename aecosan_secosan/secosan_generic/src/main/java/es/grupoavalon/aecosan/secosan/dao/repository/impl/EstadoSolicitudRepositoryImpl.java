package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.EstadoSolicitudRepository;

@Component
public class EstadoSolicitudRepositoryImpl extends AbstractCrudRespositoryImpl<EstadoSolicitudEntity, Long>
		implements EstadoSolicitudRepository {

	private static final String SOLICITUDE = "solicitudeId";
	private static final String AREA = "area";

	@Override
	protected Class<EstadoSolicitudEntity> getClassType() {
		return EstadoSolicitudEntity.class;
	}

	@Override
	public EstadoSolicitudEntity getStatusBySolicitudeId(Long solicitudeId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(SOLICITUDE, solicitudeId);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_STATUS_BY_SOLICITUDE_ID, queryParams);
	}

	@Override
	public List<EstadoSolicitudEntity> getStatusByAreaId(Long areaId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(AREA, areaId);
		return findByNamedQuery(NamedQueriesLibrary.GET_STATUS_BY_AREA_ID, queryParams);
	}
}
