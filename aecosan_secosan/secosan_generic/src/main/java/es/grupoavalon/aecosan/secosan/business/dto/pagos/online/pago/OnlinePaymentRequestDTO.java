package es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OnlinePaymentRequestDTO {

	@JsonProperty("name")
	@XmlElement(name = "name")
	private String name;

	@JsonProperty("lastName")
	@XmlElement(name = "lastName")
	private String lastname;

	@JsonProperty("secondlastName")
	@XmlElement(name = "secondlastName")
	private String secondlastName;

	@JsonProperty("telephone")
	@XmlElement(name = "telephone")
	private String telephone;

	@JsonProperty("personIdentificationNumber")
	@XmlElement(name = "personIdentificationNumber")
	private String personIdentificationNumber;

	@JsonProperty("personIdentificationType")
	@XmlElement(name = "personIdentificationType")
	private String personIdentificationtype;

	@JsonProperty("petitionIdentificator")
	@XmlElement(name = "petitionIdentificator")
	private String petitionIdentificator;

	@JsonProperty("solicitudId")
	@XmlElement(name = "solicitudId")
	private String solicitudId;

	@JsonProperty("formType")
	@XmlElement(name = "formType")
	private String formType;

	@JsonProperty("serviceType")
	@XmlElement(name = "serviceType")
	private Long serviceType;

	@JsonProperty("signedJustifier")
	@XmlElement(name = "signedJustifier")
	private String signedJustifier;

	@JsonProperty("signedJustifierRaw")
	@XmlElement(name = "signedJustifierRaw")
	private String signedJustifierRaw;

	@JsonProperty("ammount")
	@XmlElement(name = "ammount")
	private double ammount;

	@JsonProperty("quantity")
	@XmlElement(name = "quantity")
	private int quantity;

	@JsonProperty("justifier")
	@XmlElement(name = "justifier")
	private String justifier;

	@JsonProperty("address")
	@XmlElement(name = "address")
	private OnlinePaymentAddressDTO address;

	@JsonProperty("location")
	@XmlElement(name = "location")
	private OnlinePaymentLocationDTO location;

	@JsonProperty("accountPaymentInfo")
	@XmlElement(name = "accountPaymentInfo")
	private AccountPaymentDTO accountPaymentInfo;

	@JsonProperty("creditCardPaymentInfo")
	@XmlElement(name = "creditCardPaymentInfo")
	private CreditCardPaymentDTO creditCardPaymentInfo;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPersonIdentificationNumber() {
		return personIdentificationNumber;
	}

	public void setPersonIdentificationNumber(String personIdentificationNumber) {
		this.personIdentificationNumber = personIdentificationNumber;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getSignedJustifier() {
		return signedJustifier;
	}

	public void setSignedJustifier(String signedJustifier) {
		this.signedJustifier = signedJustifier;
	}

	public String getSignedJustifierRaw() {
		return signedJustifierRaw;
	}

	public void setSignedJustifierRaw(String signedJustifierRaw) {
		this.signedJustifierRaw = signedJustifierRaw;
	}

	public double getAmmount() {
		return ammount;
	}

	public void setAmmount(double ammount) {
		this.ammount = ammount;
	}

	public String getJustifier() {
		return justifier;
	}

	public void setJustifier(String justifier) {
		this.justifier = justifier;
	}

	public AccountPaymentDTO getAccountPaymentInfo() {
		return accountPaymentInfo;
	}

	public void setAccountPaymentInfo(AccountPaymentDTO accountPaymentInfo) {
		this.accountPaymentInfo = accountPaymentInfo;
	}

	public CreditCardPaymentDTO getCreditCardPaymentInfo() {
		return creditCardPaymentInfo;
	}

	public void setCreditCardPaymentInfo(CreditCardPaymentDTO creditCardPaymentInfo) {
		this.creditCardPaymentInfo = creditCardPaymentInfo;
	}

	public String getPetitionIdentificator() {
		return petitionIdentificator;
	}

	public void setPetitionIdentificator(String petitionIdentificator) {
		this.petitionIdentificator = petitionIdentificator;
	}

	public OnlinePaymentAddressDTO getAddress() {
		return address;
	}

	public void setAddress(OnlinePaymentAddressDTO address) {
		this.address = address;
	}

	public String getSolicitudId() {
		return solicitudId;
	}

	public void setSolicitudId(String solicitudId) {
		this.solicitudId = solicitudId;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getSecondlastName() {
		return secondlastName;
	}

	public void setSecondlastName(String secondlastName) {
		this.secondlastName = secondlastName;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public OnlinePaymentLocationDTO getLocation() {
		return location;
	}

	public void setLocation(OnlinePaymentLocationDTO location) {
		this.location = location;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getPersonIdentificationtype() {
		return personIdentificationtype;
	}

	public void setPersonIdentificationtype(String personIdentificationtype) {
		this.personIdentificationtype = personIdentificationtype;
	}

	public Long getServiceType() {
		return serviceType;
	}

	public void setServiceType(Long serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((accountPaymentInfo == null) ? 0 : accountPaymentInfo.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + Double.valueOf(ammount).hashCode();
		result = prime * result + ((creditCardPaymentInfo == null) ? 0 : creditCardPaymentInfo.hashCode());
		result = prime * result + ((formType == null) ? 0 : formType.hashCode());
		result = prime * result + ((justifier == null) ? 0 : justifier.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((personIdentificationNumber == null) ? 0 : personIdentificationNumber.hashCode());
		result = prime * result + ((personIdentificationtype == null) ? 0 : personIdentificationtype.hashCode());
		result = prime * result + ((petitionIdentificator == null) ? 0 : petitionIdentificator.hashCode());
		result = prime * result + quantity;
		result = prime * result + ((secondlastName == null) ? 0 : secondlastName.hashCode());
		result = prime * result + ((signedJustifier == null) ? 0 : signedJustifier.hashCode());
		result = prime * result + ((signedJustifierRaw == null) ? 0 : signedJustifierRaw.hashCode());
		result = prime * result + ((solicitudId == null) ? 0 : solicitudId.hashCode());
		result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
		result = prime * result + ((serviceType == null) ? 0 : serviceType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		OnlinePaymentRequestDTO other = (OnlinePaymentRequestDTO) obj;
		if (accountPaymentInfo == null) {
			if (other.accountPaymentInfo != null) {
				return false;
			}
		} else if (!accountPaymentInfo.equals(other.accountPaymentInfo)) {
			return false;
		}
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (Double.doubleToLongBits(ammount) != Double.doubleToLongBits(other.ammount)) {
			return false;
		}
		if (creditCardPaymentInfo == null) {
			if (other.creditCardPaymentInfo != null) {
				return false;
			}
		} else if (!creditCardPaymentInfo.equals(other.creditCardPaymentInfo)) {
			return false;
		}
		if (formType == null) {
			if (other.formType != null) {
				return false;
			}
		} else if (!formType.equals(other.formType)) {
			return false;
		}
		if (justifier == null) {
			if (other.justifier != null) {
				return false;
			}
		} else if (!justifier.equals(other.justifier)) {
			return false;
		}
		if (lastname == null) {
			if (other.lastname != null) {
				return false;
			}
		} else if (!lastname.equals(other.lastname)) {
			return false;
		}
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.equals(other.location)) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (personIdentificationNumber == null) {
			if (other.personIdentificationNumber != null) {
				return false;
			}
		} else if (!personIdentificationNumber.equals(other.personIdentificationNumber)) {
			return false;
		}
		if (personIdentificationtype == null) {
			if (other.personIdentificationtype != null) {
				return false;
			}
		} else if (!personIdentificationtype.equals(other.personIdentificationtype)) {
			return false;
		}
		if (petitionIdentificator == null) {
			if (other.petitionIdentificator != null) {
				return false;
			}
		} else if (!petitionIdentificator.equals(other.petitionIdentificator)) {
			return false;
		}
		if (quantity != other.quantity) {
			return false;
		}
		if (secondlastName == null) {
			if (other.secondlastName != null) {
				return false;
			}
		} else if (!secondlastName.equals(other.secondlastName)) {
			return false;
		}
		if (signedJustifier == null) {
			if (other.signedJustifier != null) {
				return false;
			}
		} else if (!signedJustifier.equals(other.signedJustifier)) {
			return false;
		}
		if (signedJustifierRaw == null) {
			if (other.signedJustifierRaw != null) {
				return false;
			}
		} else if (!signedJustifierRaw.equals(other.signedJustifierRaw)) {
			return false;
		}
		if (solicitudId == null) {
			if (other.solicitudId != null) {
				return false;
			}
		} else if (!solicitudId.equals(other.solicitudId)) {
			return false;
		}
		if (telephone == null) {
			if (other.telephone != null) {
				return false;
			}
		} else if (!telephone.equals(other.telephone)) {
			return false;
		}
		if (serviceType == null) {
			if (other.serviceType != null) {
				return false;
			}
		} else if (!serviceType.equals(other.serviceType)) {
			return false;
		}
		return true;
	}


}
