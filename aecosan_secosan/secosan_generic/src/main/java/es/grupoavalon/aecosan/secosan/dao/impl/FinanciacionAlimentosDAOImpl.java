package es.grupoavalon.aecosan.secosan.dao.impl;

import es.grupoavalon.aecosan.secosan.dao.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.FinanciacionAlimentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

@Repository
public class FinanciacionAlimentosDAOImpl extends GenericDao implements FinanciacionAlimentosDAO {

	@Autowired
	private IncOferAlimUMERepository incOferAlimUMERepository;
	@Autowired
	private AlterOferAlimUMERepository alterOferAlimUMERepository;
	@Autowired
	private BajaOferAlimUMERepository bajaOferAlimUMERepository;
	@Autowired
	private ProductoSuspendidoRepository productoRepo;
	@Autowired
	private PresentacionEnvaseRepository presentacionEnvaseRepository;

	@Override
	public IncOferAlimUMEEntity addIncOferAlimUME(IncOferAlimUMEEntity incOferAlimUMEEntity) {

		try {
			incOferAlimUMERepository.save(incOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addIncOferAlimUME | ERROR: " + incOferAlimUMEEntity);
		}

		return incOferAlimUMEEntity;
	}

	@Override
	public void updateIncOferAlimUME(IncOferAlimUMEEntity incOferAlimUMEEntity) {
		try {
			incOferAlimUMERepository.update(incOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateIncOferAlimUME | ERROR: " + incOferAlimUMEEntity);
		}
	}

	@Override
	public AlterOferAlimUMEEntity addAlterOferAlimUME(AlterOferAlimUMEEntity alterOferAlimUMEEntity) {
		try {
			alterOferAlimUMERepository.save(alterOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addAlterOferAlimUME | ERROR: " + alterOferAlimUMEEntity);
		}

		return alterOferAlimUMEEntity;
	}

	@Override
	public void updateAlterOferAlimUME(AlterOferAlimUMEEntity alterOferAlimUMEEntity) {
		try {
			alterOferAlimUMERepository.update(alterOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateAlterOferAlimUME | ERROR: " + alterOferAlimUMEEntity);
		}

	}

	@Override
	public BajaOferAlimUMEEntity addBajaOferAlimUME(BajaOferAlimUMEEntity bajaOferAlimUMEEntity) {
		try {
			bajaOferAlimUMERepository.save(bajaOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " addBajaOferAlimUME | ERROR: " + bajaOferAlimUMEEntity);
		}

		return bajaOferAlimUMEEntity;
	}

	@Override
	public void updateBajaOferAlimUME(BajaOferAlimUMEEntity bajaOferAlimUMEEntity) {
		try {
			bajaOferAlimUMERepository.update(bajaOferAlimUMEEntity);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " updateBajaOferAlimUME | ERROR: " + bajaOferAlimUMEEntity);
		}

	}

	@Override
	public void deleteSuspendedProduct(long id) {
		try {
			this.productoRepo.delete(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteSuspendedProduct | ERROR: " + id);
		}
	}

	@Override
	public void deletePresentacionEnvases(long id) {
		try {
			presentacionEnvaseRepository.deleteByFinanciacionId(id);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + " deleteByFinanciacionId | ERROR: " + id);
		}
	}
}
