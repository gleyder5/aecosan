package es.grupoavalon.aecosan.secosan.business;

import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;

public interface ApoderamientoService {
    ConsultarApoderamientoResponseDTO consultarApoderamiento(String nif);
}


