package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.*;
import javax.ws.rs.DefaultValue;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;
import org.hibernate.annotations.Where;

@Entity
@Table(name=TableNames.TABLA_TIPO_PROCEDIMIENTO_ENTITY)
@Where(clause = "ACTIVO=1")
public class TipoProcedimientoEntity{

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.TIPO_PROCEDIMIENTO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.TIPO_PROCEDIMIENTO_SEQUENCE, sequenceName = SequenceNames.TIPO_PROCEDIMIENTO_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = ColumnNames.CATALOG_NOMBRE, nullable = false)
	private String descripcion;

	@Column(name= ColumnNames.ACTIVE)
	private Boolean active ;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoProcedimientoEntity other = (TipoProcedimientoEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TipoDocumentacionEntity [id=" + id + "]";
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
