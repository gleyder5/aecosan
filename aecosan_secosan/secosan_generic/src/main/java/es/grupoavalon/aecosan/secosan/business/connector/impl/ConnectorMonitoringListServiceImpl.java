package es.grupoavalon.aecosan.secosan.business.connector.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringListService;
import es.grupoavalon.aecosan.secosan.business.dto.MonitoringConnectorDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.business.impl.AbstractManager;
import es.grupoavalon.aecosan.secosan.dao.MonitorizacionConectoresDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.MonitorizacionConectoresEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Service
public class ConnectorMonitoringListServiceImpl extends AbstractManager implements ConnectorMonitoringListService {

	@Autowired
	private MonitorizacionConectoresDAO monitorizacionConectoresDAO;

	@Override
	public List<MonitoringConnectorDTO> getListMonitoringConnector(PaginationParams paginationParams) {
		List<MonitoringConnectorDTO> listDTO = null;
		List<MonitorizacionConectoresEntity> listEntity;

		try {
			logger.debug("getListMonitoringConnector || paginationParams: {} ", paginationParams);
			listEntity = monitorizacionConectoresDAO.getListMonitoringConnector(paginationParams);
			logger.debug("getListMonitoringConnector || listEntity.size(): {}", listEntity.size());
			listDTO = transformEntityListToDTOList(listEntity, MonitorizacionConectoresEntity.class, MonitoringConnectorDTO.class);
			logger.debug("getListMonitoringConnector || listDTO.size(): {} ", listDTO.size());
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + "getListMonitoringConnector | ERROR: " + e.getMessage());
		}

		return listDTO;
	}

	@Override
	public int getTotalListMonitoringConnector(List<FilterParams> filters) {
		int totalRecords = 0;

		try {
			totalRecords = monitorizacionConectoresDAO.getTotalListMonitoringConnector(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getTotalListMonitoringConnector | PaginationParams: " + filters + " | ERROR: " + exception.getMessage());
		}

		return totalRecords;
	}

	@Override
	public int getFilteredListMonitoringConnector(List<FilterParams> filters) {
		int totalRecords = 0;
		try {
			totalRecords = monitorizacionConectoresDAO.getFilteredListMonitoringConnector(filters);
		} catch (Exception exception) {
			handleException(exception, ServiceException.SERVICE_ERROR + "getFilteredListMonitoringConnector | ERROR: " + exception.getMessage());
		}
		return totalRecords;
	}

}
