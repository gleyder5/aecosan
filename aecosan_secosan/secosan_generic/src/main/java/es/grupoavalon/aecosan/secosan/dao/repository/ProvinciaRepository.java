package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.ProvinciasEntity;

public interface ProvinciaRepository extends CrudRepository<ProvinciasEntity, Long> {


}
