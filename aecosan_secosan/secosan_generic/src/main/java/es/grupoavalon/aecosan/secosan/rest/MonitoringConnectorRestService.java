package es.grupoavalon.aecosan.secosan.rest;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.dto.MonitoringConnectorDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.ResponseFactory;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.MonitoringStatisticsWraper;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;

@Component("monitoringConnectorRest")
public class MonitoringConnectorRestService {

	private static final Logger logger = LoggerFactory.getLogger(MonitoringConnectorRestService.class);

	private static final String DEFAULT_REST_URL = SecosanConstants.ADMIN_SECURE_CONTEXT + "/monitoring/";

	@Autowired
	private BusinessFacade bfacade;

	@Autowired
	private ResponseFactory responseFactory;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "getMonitoringList")
	public Response listItems(@QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		logger.debug("Entrada petición GET listItems()");

		AbstractListRestHandler<MonitoringConnectorDTO> listHandler = new AbstractListRestHandler<MonitoringConnectorDTO>() {
			@Override
			protected AbstractResponseFactory getResponseFactory() {
				return responseFactory;
			}

			@Override
			PaginatedListWrapper<MonitoringConnectorDTO> getSimplePaginatedData(PaginationParams paginationParams, String... otherParams) {
				return bfacade.getPaginatedMonitoringConnector(otherParams, paginationParams);
			}

		};
		return listHandler.getSimplePaginatedData(start, length, columnorder, order, filtrosRaw.getQueryParameters());
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path(DEFAULT_REST_URL + "getStatistics")
	public Response getStatistics(@QueryParam(AbstractListRestHandler.PARAM_START) int start,
			@DefaultValue(AbstractListRestHandler.DEFAULT_LENTH) @QueryParam(AbstractListRestHandler.PARAM_LENGTH) int length,
			@DefaultValue(AbstractListRestHandler.DEFAULT_ORDER) @QueryParam(AbstractListRestHandler.PARAM_ORDER) String order,
			@DefaultValue(AbstractListRestHandler.DEFAULT_COLUMN_ORDER) @QueryParam(AbstractListRestHandler.PARAM_COLUMNORDER) String columnorder, @Context UriInfo filtrosRaw) {

		logger.debug("Entrada petición GET getStatistics()");

		MonitoringStatisticsWraper monitoringStatistics = bfacade.getMonitoringStatistics();

		return responseFactory.generateOkGenericResponse(monitoringStatistics);
	}

}
