package es.grupoavalon.aecosan.secosan.dao.entity.pago;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PAGO_SOLICITUD)
@NamedQueries({ @NamedQuery(name = NamedQueriesLibrary.GET_PAGO_BY_SOLICITUDE_REFNUMBER, query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE
		+ ".pago.PagoSolicitudEntity AS p WHERE p.identificadorPeticion = :refnum)"),
		@NamedQuery(name = NamedQueriesLibrary.GET_PAGO_BY_SOLICITUDE_ID, query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE + ".pago.PagoSolicitudEntity AS p WHERE p.solicitud.id = :solicitudeId)"),

})
public class PagoSolicitudEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.PAGO_SOLICITUD_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.PAGO_SOLICITUD_SEQUENCE, sequenceName = SequenceNames.PAGO_SOLICITUD_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "IMPORTE_EUROS")
	private Float amount;

	@Column(name = "CANTIDAD_TASAS")
	private Integer payedTasas;

	@Column(name = "IDENTIFICADOR_PETICION_SOL", nullable = false)
	private String identificadorPeticion;

	@Column(name = "CODIGO_BARRA", nullable = true)
	private String barCode;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "FORMA_PAGO_ID")
	private FormaPagoEntity payment;

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "IDENTIFICADOR_PAGO_ID", nullable = false)
	private IdentificadorPagoEntity idPayment;

	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH })
	@JoinColumn(name = "DATOS_PAGO_ID")
	private DatosPagoEntity dataPayment;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "FORMULARIO_PAGADO")
	private TipoFormularioEntity formType;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SOLICITUD_ID", insertable = false, updatable = false)
	private SolicitudEntity solicitud;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public void setAmount(double amount) {
		BigDecimal bigDecimal = new BigDecimal(amount);
		this.amount = bigDecimal.floatValue();

	}

	public FormaPagoEntity getPayment() {
		return payment;
	}

	public void setPayment(FormaPagoEntity payment) {
		this.payment = payment;
	}

	public IdentificadorPagoEntity getIdPayment() {
		return idPayment;
	}

	public void setIdPayment(IdentificadorPagoEntity idPayment) {
		this.idPayment = idPayment;
	}

	public DatosPagoEntity getDataPayment() {
		return dataPayment;
	}

	public void setDataPayment(DatosPagoEntity dataPayment) {
		this.dataPayment = dataPayment;
	}


	public String getIdentificadorPeticion() {
		return identificadorPeticion;
	}

	public void setIdentificadorPeticion(String identificadorPeticion) {
		this.identificadorPeticion = identificadorPeticion;
	}

	public Integer getPayedTasas() {
		return payedTasas;
	}

	public void setPayedTasas(Integer payedTasas) {
		this.payedTasas = payedTasas;
	}

	public TipoFormularioEntity getFormType() {
		return formType;
	}

	public void setFormType(TipoFormularioEntity formType) {
		this.formType = formType;
	}

	public SolicitudEntity getSolicitud() {
		return solicitud;
	}

	public void setSolicitud(SolicitudEntity solicitud) {
		this.solicitud = solicitud;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	@Override
	public String toString() {
		return "PagoSolicitudEntity [id=" + id + ", amount=" + amount + ", payedTasas=" + payedTasas + ", identificadorPeticion=" + identificadorPeticion + ", barCode=" + barCode + ", payment="
				+ payment + ", idPayment=" + idPayment + ", dataPayment=" + dataPayment + ", formType=" + formType + ", solicitud=" + solicitud + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((barCode == null) ? 0 : barCode.hashCode());
		result = prime * result + ((dataPayment == null) ? 0 : dataPayment.hashCode());
		result = prime * result + ((formType == null) ? 0 : formType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((idPayment == null) ? 0 : idPayment.hashCode());
		result = prime * result + ((identificadorPeticion == null) ? 0 : identificadorPeticion.hashCode());
		result = prime * result + ((payedTasas == null) ? 0 : payedTasas.hashCode());
		result = prime * result + ((payment == null) ? 0 : payment.hashCode());
		result = prime * result + ((solicitud == null) ? 0 : solicitud.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PagoSolicitudEntity other = (PagoSolicitudEntity) obj;
		if (amount == null) {
			if (other.amount != null) {
				return false;
			}
		} else if (!amount.equals(other.amount)) {
			return false;
		}
		if (barCode == null) {
			if (other.barCode != null) {
				return false;
			}
		} else if (!barCode.equals(other.barCode)) {
			return false;
		}
		if (dataPayment == null) {
			if (other.dataPayment != null) {
				return false;
			}
		} else if (!dataPayment.equals(other.dataPayment)) {
			return false;
		}
		if (formType == null) {
			if (other.formType != null) {
				return false;
			}
		} else if (!formType.equals(other.formType)) {
			return false;
		}
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (idPayment == null) {
			if (other.idPayment != null) {
				return false;
			}
		} else if (!idPayment.equals(other.idPayment)) {
			return false;
		}
		if (identificadorPeticion == null) {
			if (other.identificadorPeticion != null) {
				return false;
			}
		} else if (!identificadorPeticion.equals(other.identificadorPeticion)) {
			return false;
		}
		if (payedTasas == null) {
			if (other.payedTasas != null) {
				return false;
			}
		} else if (!payedTasas.equals(other.payedTasas)) {
			return false;
		}
		if (payment == null) {
			if (other.payment != null) {
				return false;
			}
		} else if (!payment.equals(other.payment)) {
			return false;
		}
		if (solicitud == null) {
			if (other.solicitud != null) {
				return false;
			}
		} else if (!solicitud.equals(other.solicitud)) {
			return false;
		}
		return true;
	}

}
