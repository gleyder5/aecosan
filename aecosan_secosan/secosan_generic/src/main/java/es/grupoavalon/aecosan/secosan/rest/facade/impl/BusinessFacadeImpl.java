package es.grupoavalon.aecosan.secosan.rest.facade.impl;

import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;
import es.grupoavalon.aecosan.secosan.business.*;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringListService;
import es.grupoavalon.aecosan.secosan.business.connector.ConnectorMonitoringService;
import es.grupoavalon.aecosan.secosan.business.dto.*;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.AlimentosGrupos;
import es.grupoavalon.aecosan.secosan.business.dto.interfaces.ComplementosAlimenticiosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.TasaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.ResolucionProvisionalReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.*;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos.EvaluacionRiesgosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.BajaOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.FibraDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.FinanciacionAlimentosDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.IncOferAlimUMEDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.PresentacionEnvaseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsFileDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsListDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.AbstractSolicitudesQuejaSugerenciaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroAguas.RegistroAguasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias.RegistroIndustriasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroReacu.RegistroReacuDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesAsocuaeDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.subvenciones.SubvencionesJuntasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.*;
import es.grupoavalon.aecosan.secosan.business.enums.PlaceholdersFichaTecnicaIncl;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.rest.ImpresoFirmaRestService;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.MonitoringStatisticsWraper;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.SolicitudDTOReadWrapper;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.report.ReportUtil;
import es.grupoavalon.aecosan.secosan.util.rest.security.JwtUtil;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.BooleanWrapper;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.TokenJWTWrapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;


import static es.grupoavalon.aecosan.secosan.util.SecosanConstants.FICHA_TEC_ANEXO_II_PDF;
import static es.grupoavalon.aecosan.secosan.util.SecosanConstants.TEMPLATE_FICHA_TEC_ANEXO_II;

@Component
public class BusinessFacadeImpl implements BusinessFacade {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BusinessFacadeImpl.class);
	@Autowired
	private  ApoderamientoService apoderamientoService;

	@Autowired
	private HistorialSolicitudService historialSolicitudService;

	@Autowired
	private SolicitudListService solicitudListService;

	@Autowired
	private SolicitudeService solicitudeService;

	@Autowired
	private CatalogService catalogService;

	@Autowired
	private ComplementosAlimenticiosService complementosAlimenticiosService;

	@Autowired
	private FicheroInstruccionesService ficheroInstruccionesService;

	@Autowired
	private IdentificacionPeticionService identificationRequestService;

	@Autowired
	private SolicitudQuejaSugerenciaService solicitudQuejaSugerenciaService;

	@Autowired
	private SolicitudLogoService solicitudLogoService;

	@Autowired
	private SolicitudLmrFitosanitariosService solicitudLmrFitosanitariosService;

	@Autowired
	private DocumentacionService documentacionService;

	@Autowired
	private EvaluacionRiesgosService evaluacionRiesgosService;

	@Autowired
	private FinanciacionAlimentosService financiacionAlimentosService;

	@Autowired
	private AlimentosGruposService alimentosGruposService;

	@Autowired
	private ServicioService servicioService;

	@Autowired
	private RegistroReacuService registroReacuService;

	@Autowired
	private StatusService statusService;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private UserService userService;

	@Autowired
	private TipoProcedimientoService tipoProcedimientoService;

	@Autowired
	private ImpresoFirmadoService impresoFirmado;

	@Autowired
	private OnlinePaymentService paymentService;

	@Autowired
	private AttachableFileService attachableService;

	@Autowired
	private TasaService tasaService;

	@Autowired
	private RegistroAguasService registroAguasService;

	@Autowired
	private RegistroIndustriasService registroIndustriasService;

	@Autowired
	private ResolucionProvisionalService resolucionProvisionalService;

	@Autowired
	private ConnectorMonitoringListService connectorMonitoringListService;

	@Autowired
	private ConnectorMonitoringService connectorMonitoringService;

	@Autowired
	private PasswordHandlerService passwordHandler;

	@Autowired
	private LogService logService;

	@Autowired
	private ProcedimientoGeneralService procedimientoGeneralService;

	@Autowired
	private SubvencionesAsocuaeService subvencionesAsocuaeService;

	@Autowired
	private SubvencionesJuntasService subvencionesJuntasService;

	@Autowired
	private BecasService becasService;

	@Autowired
	ImpresoFirmadoService impresoFirmadoService;

	@Override
	public PaginatedListWrapper<SolicitudListDTO> getPaginatedList(String[] otherParams, PaginationParams paginationParams) {
		String user = otherParams[0];
		paginationParamsSetUserFilter(user, paginationParams);
		PaginatedListWrapper<SolicitudListDTO> wraper = new PaginatedListWrapper<SolicitudListDTO>();
		wraper.setData(solicitudListService.getSolicList(paginationParams));
		wraper.setTotal(solicitudListService.getTotalListRecords(generatePaginationParamsForUser(user).getFilters()));
		wraper.setRecordsFiltered(solicitudListService.getFilteredListRecords(paginationParams.getFilters()));

		return wraper;
	}

	private static void paginationParamsSetUserFilter(String user, PaginationParams paginationParams) {
		paginationParams.getFilters().add(generaterFilterUser(user));
	}

	private static PaginationParams generatePaginationParamsForUser(String user) {
		PaginationParams paginationParams = new PaginationParams();
		paginationParams.setFilter(generaterFilterUser(user));
		return paginationParams;
	}

	private static FilterParams generaterFilterUser(String user) {
		FilterParams filterUser = new FilterParams();
		filterUser.setFilterName(SecosanConstants.FIELD_USER);
		filterUser.setFilterValue(user);
		return filterUser;
	}

	@Override
	public PaginatedListWrapper<SolicitudListDTO> getPaginatedListByArea(String[] otherParams, PaginationParams paginationParams) {
		String area = otherParams[0];
		paginationParamsSetTramitadorFilters(area, paginationParams);
		PaginatedListWrapper<SolicitudListDTO> wraper = new PaginatedListWrapper<SolicitudListDTO>();
		wraper.setData(solicitudListService.getSolicList(paginationParams));
		wraper.setTotal(solicitudListService.getTotalListRecords(paginationParams.getFilters()));
		wraper.setRecordsFiltered(solicitudListService.getFilteredListRecords(paginationParams.getFilters()));

		return wraper;
	}

	@Override
	public PaginatedListWrapper<HistorialSolicitudDTO> getHistorialSolicitudList(String[] otherParams,PaginationParams paginationParams) {
		PaginatedListWrapper<HistorialSolicitudDTO> wraper = new PaginatedListWrapper<HistorialSolicitudDTO>();
		wraper.setData(historialSolicitudService.getHistorialList(paginationParams));
		wraper.setTotal(historialSolicitudService.getTotal(paginationParams.getFilters()));
		wraper.setRecordsFiltered(historialSolicitudService.getHistorialList(paginationParams).size());
		return wraper;
	}

	@Override
	public HistorialSolicitudDTO getHistorialSolicitudBySolicitudAndStatus(Integer solicitudId, Integer statusId) {

		return historialSolicitudService.getBySolicitudAndStatus(solicitudId,statusId);
	}

	private void paginationParamsSetTramitadorFilters(String area, PaginationParams paginationParams) {

		FilterParams filterArea = new FilterParams();

		filterArea.setFilterName(SecosanConstants.FIELD_AREA);
		filterArea.setFilterValue(area);
		paginationParams.setFilter(filterArea);

		FilterParams filterStatus = new FilterParams();

		String solicitudeStatusDraft = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_SOLICITUDE_STATUS_DRAFT);
		filterStatus.setFilterName(SecosanConstants.FILTER_NOT_STATUS);
		filterStatus.setFilterValue(solicitudeStatusDraft);
		paginationParams.setFilter(filterStatus);

	}

	@Override
	public InstructionsFileDTO findInstructionsFileBySolicitudeType(String solicitudeType) {
		return ficheroInstruccionesService.findInstructionsFileBysolicitudeType(solicitudeType);
	}

	@Override
	public InstructionsFileDTO findInstructionsFileByForm(String formId) {
		return ficheroInstruccionesService.findInstructionsFileByForm(formId);
	}

	@Override
	public IdentificacionPeticionDTO findIdentificationRequestByAreaId(String areaId) {
		return identificationRequestService.getIdentificationRequestByAreaId(areaId);
	}

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName) {
		return catalogService.getCatalogItems(catalogName);
	}

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName, String parentElement, String parentId) {
		return catalogService.getCatalogItems(catalogName, parentElement, parentId);
	}

	@Override
	public List<CatalogDTO> getCatalogItems(String catalogName, String tag) {
		return catalogService.getCatalogItems(catalogName, tag);
	}

	@Override
	public ConsultarApoderamientoResponseDTO consultarApoderamiento(String nif) {
		return  apoderamientoService.consultarApoderamiento(nif);
	}

	@Override
	public void addSolicitud(String user, SolicitudDTO solicitudDTO) {
		if (solicitudDTO instanceof ComplementosAlimenticiosDTO) {
			complementosAlimenticiosService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof AbstractSolicitudesQuejaSugerenciaDTO) {
			solicitudQuejaSugerenciaService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof EvaluacionRiesgosDTO) {
			evaluacionRiesgosService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof SolicitudLogoDTO) {
			solicitudLogoService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof SolicitudLmrFitosanitariosDTO) {
			solicitudLmrFitosanitariosService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof FinanciacionAlimentosDTO) {
			financiacionAlimentosService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof BajaOferAlimUMEDTO) {
			financiacionAlimentosService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof AlimentosGrupos) {
			alimentosGruposService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof RegistroAguasDTO) {
			registroAguasService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof RegistroIndustriasDTO) {
			registroIndustriasService.add(user, solicitudDTO);
		} else if (solicitudDTO instanceof ProcedimientoGeneralDTO) {
			procedimientoGeneralService.add(user, solicitudDTO);
		}else if (solicitudDTO instanceof RegistroReacuDTO) {
			registroReacuService.add(user, solicitudDTO);
		}else if (solicitudDTO instanceof SubvencionesAsocuaeDTO) {
			subvencionesAsocuaeService.add(user, solicitudDTO);
		}else if (solicitudDTO instanceof BecasDTO) {
			becasService.add(user, solicitudDTO);
		}else if (solicitudDTO instanceof SubvencionesJuntasDTO) {
			subvencionesJuntasService.add(user, solicitudDTO);
		}

	}

	@Override
	public void updateSolicitud(String user, SolicitudDTO solicitudDTO) {
		if (solicitudDTO instanceof ComplementosAlimenticiosDTO) {
			complementosAlimenticiosService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof AbstractSolicitudesQuejaSugerenciaDTO) {
			solicitudQuejaSugerenciaService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof EvaluacionRiesgosDTO) {
			evaluacionRiesgosService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof SolicitudLogoDTO) {
			solicitudLogoService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof SolicitudLmrFitosanitariosDTO) {
			solicitudLmrFitosanitariosService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof AlimentosGrupos) {
			alimentosGruposService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof FinanciacionAlimentosDTO) {
			financiacionAlimentosService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof BajaOferAlimUMEDTO) {
			financiacionAlimentosService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof RegistroAguasDTO) {
			registroAguasService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof RegistroIndustriasDTO) {
			registroIndustriasService.update(user, solicitudDTO);
		}else if (solicitudDTO instanceof ProcedimientoGeneralDTO) {
			procedimientoGeneralService.update(user, solicitudDTO);
		}else if (solicitudDTO instanceof RegistroReacuDTO) {
			registroReacuService.update(user, solicitudDTO);
		} else if (solicitudDTO instanceof SubvencionesAsocuaeDTO) {
			subvencionesAsocuaeService.update(user, solicitudDTO);
		}else if (solicitudDTO instanceof BecasDTO) {
			becasService.update(user, solicitudDTO);
		}else if (solicitudDTO instanceof SubvencionesJuntasDTO) {
			subvencionesJuntasService.update(user, solicitudDTO);
		}
	}



	@Override
	public void deleteDocumentacionById(String user, String id) {
		documentacionService.deleteDocumentacionById(user, id);
	}

	@Override
	public SolicitudDTOReadWrapper findSolicitudById(String user, String id) {
		SolicitudDTOReadWrapper solicitudDTOReadWrapper = new SolicitudDTOReadWrapper();
		SolicitudDTO solicitudDTO = solicitudeService.findSolicitudeById(user, id);
		solicitudDTOReadWrapper.setName(solicitudDTO.getClass().getSimpleName());
		solicitudDTOReadWrapper.setSolicitude(solicitudDTO);
		return solicitudDTOReadWrapper;
	}

	@Override
	public List<ServicioDTO> findServicesByPayTypeId(String payTypeId) {
		return servicioService.findServicesByPayTypeId(payTypeId);
	}

	@Override
	public void deleteSolicitud(String user, String id) {
		solicitudeService.deleteSolicitudeById(user, id);
	}

	@Override
	public DocumentacionDTO downloadDocumentationById(String docId) {
		return documentacionService.findDocumentationById(docId);
	}

	@Override
	public TokenJWTWrapper doLogin(LoginDTO user) throws AutenticateException {
		LoginUserDTO userLoged = this.userService.doLogin(user.getLogin(), user.getPwDecrypted());
		return generateTokenFromUser(userLoged);
	}

	@Override
	public TokenJWTWrapper doLoginAdmin(LoginDTO user) throws AutenticateException {
		LoginUserDTO userLoged = this.userService.doLoginAdmin(user.getLogin(), user.getPwDecrypted());
		return generateTokenFromUser(userLoged, JwtUtil.ADMIN_TOKEN);
	}

	@Override
	public TokenJWTWrapper doLoginClave(UserClaveDTO user) {
		LoginUserDTO userLogged = this.userService.doClaveLogin(user);
		return generateTokenFromUser(userLogged);
	}

	@Override
	public TokenJWTWrapper doLoginAdminClave(UserClaveDTO user) throws AutenticateException {
		LoginUserDTO userLogged = this.userService.doClaveLoginAdmin(user);
		return generateTokenFromUser(userLogged, JwtUtil.ADMIN_TOKEN);
	}

	private TokenJWTWrapper generateTokenFromUser(LoginUserDTO user, Boolean... isAdmin) {
		Map<String, Object> payload = generateTokenPayload(user);
		String token = jwtUtil.getJwtToken(payload, isAdmin);
		return instaciateTokenWrapper(token);

	}

	private static Map<String, Object> generateTokenPayload(LoginUserDTO user) {
		Map<String, Object> payload = new HashMap<String, Object>();
		payload.put("name", user.getName());
		payload.put("userLastName", user.getLastName());
		payload.put("id", user.getId());
		payload.put("email", user.getEmail());
		payload.put("lastLogin", user.getLastLogin());
		payload.put("firstLogin", user.isFirstLogin());
		payload.put("loginType", user.getLoginType());
		payload.put("userSecondLastName", user.getSecondLastName());
		payload.put("identificationNumber", user.getIdentificationNumber());

		setRoleIntoPayloadForInternalUsers(user, payload);
		return payload;
	}

	private static void setRoleIntoPayloadForInternalUsers(LoginUserDTO user, Map<String, Object> payload) {
		if (user instanceof LoginUserInternalDTO) {
			LoginUserInternalDTO internalUser = (LoginUserInternalDTO) user;
			payload.put("roleId", internalUser.getRoleId());
			payload.put("roleDescription", internalUser.getRoleDescription());
			payload.put("areaId", internalUser.getAreaId());
			payload.put("areaDescription", internalUser.getAreaDescription());
		}
	}


	private static TokenJWTWrapper instaciateTokenWrapper(String token) {
		TokenJWTWrapper wraper = new TokenJWTWrapper();
		wraper.setToken(token);
		return wraper;
	}

	@Override
	public List<TipoDocumentacionDTO> findDocumentationByFormId(String formId) {
		return documentacionService.findDocumentationByFormId(formId);
	}

	@Override
	public ImpresoFirmadoDTO getPDFToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO) {
		return impresoFirmado.getPDFToSign(user, impresoFirmadoDTO);
	}

	@Override
	public List<EstadoSolicitudDTO> getStatusByAreaId(String areaID) {
		return statusService.getStatusByAreaId(areaID);
	}

	@Override
	public void solicitudeUpdateStatusBySolicitudeId(String user, ChangeStatusDTO statusDTO) {
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(user, statusDTO);
	}

	@Override
	public void addUserNoUeMembership(UserNoEUDTO user) {
		this.userService.doRegistry(user);
	}

	@Override
	public void uploadInstructionsToPay(InstructionsFileDTO ficheroInstrucciones) {
		ficheroInstruccionesService.uploadInstructionsToPay(ficheroInstrucciones);
	}

	@Override
	public void uploadInstructions(InstructionsFileDTO ficheroInstrucciones) {
		ficheroInstruccionesService.uploadInstructions(ficheroInstrucciones);
	}

	@Override
	public InstructionsListDTO findInstructionsLists(String userId) {
		return ficheroInstruccionesService.findInstructionsLists(userId);
	}

	@Override
	public void checkToken(TokenJWTWrapper token) throws AutenticateException {
		if (!this.jwtUtil.isTokenValid(token.getToken(), JwtUtil.NORMAL_TOKEN)) {
			throw new AutenticateException(AutenticateException.AutenticateErrorCodes.TOKEN_INVALID.getDescription(), AutenticateException.AutenticateErrorCodes.TOKEN_INVALID);
		}
	}

	@Override
	public void checkAdminToken(TokenJWTWrapper token) throws AutenticateException {
		if (!this.jwtUtil.isTokenValid(token.getToken(), JwtUtil.ADMIN_TOKEN)) {
			throw new AutenticateException(AutenticateException.AutenticateErrorCodes.TOKEN_INVALID.getDescription(), AutenticateException.AutenticateErrorCodes.TOKEN_INVALID);
		}
	}

	@Override
	public void addUserInternal(UserInternalDTO user) {
		this.userService.doRegistry(user);
	}

	@Override
	public void addTipoProcedimiento(TipoProcedimientoDTO tipoProcedimientoDTO) {
		this.tipoProcedimientoService.add(tipoProcedimientoDTO);
	}

	@Override
	public List<TipoProcedimientoDTO> getTipoProcedimientoList() {
		return this.tipoProcedimientoService.getTipoProcedimientoDTOList();
	}

	@Override
	public void addUserClave(UserClaveDTO userClaveDTO) {
		this.userService.doRegistry(userClaveDTO);
	}

	@Override
	public PaginatedListWrapper<UserInternalDTO> getInternalUserList(PaginationParams paginationParams) {
		PaginatedListWrapper<UserInternalDTO> wraper = new PaginatedListWrapper<UserInternalDTO>();
		wraper.setData(userService.getInternalUserList(paginationParams));
		wraper.setTotal(userService.getTotalListRecords(paginationParams.getFilters()));
		wraper.setRecordsFiltered(userService.getFilteredListRecords(paginationParams.getFilters()));
		return wraper;
	}

	@Override
	public void deleteInternalUser(String userId, String userToDeleteId) {
		userService.deleteInternalUser(userId, userToDeleteId);

	}

	@Override
	public void deleteTipoProcedimiento(String userId, String tipoProcedimientoId) {
		tipoProcedimientoService.delete(userId,tipoProcedimientoId);

	}

	@Override
	public void updateInternalUser(String userId, UserInternalDTO userToUpdate) {
		userService.updateInternalUser(userId, userToUpdate);
	}

	@Override
	public JustifierResponseDTO getPaymentJustifier(JustifierRequestDTO justRequest) {
		return paymentService.getPaymentJustifier(justRequest);
	}

	@Override
	public OnlinePaymentResponseDTO makePayment(OnlinePaymentRequestDTO paymentRequest) {
		return this.paymentService.makePayment(paymentRequest);
	}

	@Override
	public AttachableFileDTO downloadAttachableFileByName(String fileName) {
		return attachableService.downloadAttachableFileByName(fileName);

	}

	@Override
	public JustificantePagoReportDTO getJustificantePago(String identificadorPeticion,Long serviceId) {
		return paymentService.getJustificantePagoPDF(identificadorPeticion,serviceId);
	}

	@Override
	public BooleanWrapper isPaidBySolicitudeID(String identificadorPeticion) {
		BooleanWrapper booleanWrapper = new BooleanWrapper();
		JustificantePagoDTO justificantePagoDTO = paymentService.getJustificantePagoBySolicitudeID(identificadorPeticion);
		if (justificantePagoDTO != null && justificantePagoDTO.getDataPayment() != null) {
			booleanWrapper.setValue(true);
		} else {
			booleanWrapper.setValue(false);
		}

		return booleanWrapper;
	}

	@Override
	public TasaDTO getTasaByFormId(String identificadorFormulario) {
		long idForm = Long.parseLong(identificadorFormulario);
		return tasaService.getTasaByFormId(idForm);
	}

	@Override
	public TasaDTO getTasaByServicioId(String serviceId) {
		long idForm = Long.parseLong(serviceId);
		return tasaService.getTasaByServiceId(idForm);
	}

	@Override
	public PaginatedListWrapper<TasaDTO> getTasaPaginatedList(PaginationParams params) {
		PaginatedListWrapper<TasaDTO> wraper = new PaginatedListWrapper<TasaDTO>();
		wraper.setData(this.tasaService.getPaginatedTasaList(params));
		wraper.setTotal(this.tasaService.getTotalTasaListRecords(params.getFilters()));
		wraper.setRecordsFiltered(this.tasaService.getFilteredListRecords(params.getFilters()));
		return wraper;
	}

	@Override
	public void updateTasa(TasaDTO tasaDto) {

		this.tasaService.updateTasa(tasaDto);
	}

	@Override
	public PaginatedListWrapper<MonitoringConnectorDTO> getPaginatedMonitoringConnector(String[] otherParams, PaginationParams paginationParams) {
		PaginatedListWrapper<MonitoringConnectorDTO> wraper = new PaginatedListWrapper<MonitoringConnectorDTO>();

		wraper.setData(connectorMonitoringListService.getListMonitoringConnector(paginationParams));
		wraper.setTotal(connectorMonitoringListService.getTotalListMonitoringConnector(paginationParams.getFilters()));
		wraper.setRecordsFiltered(connectorMonitoringListService.getFilteredListMonitoringConnector(paginationParams.getFilters()));

		return wraper;
	}

	@Override
	public void monitoringSuccess(MonitorizableConnectors connector) {
		this.connectorMonitoringService.monitoringSuccess(connector);
	}

	@Override
	public void monitoringError(MonitorizableConnectors connector, Throwable e) {
		this.connectorMonitoringService.monitoringError(connector, e);
	}

	@Override
	public MonitoringStatisticsWraper getMonitoringStatistics() {
		MonitoringStatisticsWraper monitoringStatisticsWraper = null;

		PaginationParams paginationParams = new PaginationParams();
		paginationParams.setSortField("id");
		paginationParams.setFilters(getStatisticsFilters());

		List<MonitoringConnectorDTO> totals = connectorMonitoringListService.getListMonitoringConnector(paginationParams);
		monitoringStatisticsWraper = calcTotals(totals);

		return monitoringStatisticsWraper;
	}

	private MonitoringStatisticsWraper calcTotals(List<MonitoringConnectorDTO> totals) {
		MonitoringStatisticsWraper monitoringStatisticsWraper = new MonitoringStatisticsWraper();
		Map<String, MonitoringStatisticsDTO> statistics = new HashMap<String, MonitoringStatisticsDTO>();
		for (MonitoringConnectorDTO mc : totals) {
			
			if (!statistics.containsKey(mc.getConnectorName())) {
				statistics.put(mc.getConnectorName(), new MonitoringStatisticsDTO());
			}
			if (mc.getSuccess()) {
				statistics.get(mc.getConnectorName()).addOks();
			} else {
				statistics.get(mc.getConnectorName()).addError();
			}
		}
		MonitorizableConnectors.CLAVE.getName();
		monitoringStatisticsWraper.setStatistics(statistics);

		return monitoringStatisticsWraper;
	}

	private List<FilterParams> getStatisticsFilters() {
		List<FilterParams> filters = new ArrayList<FilterParams>();
		return filters;
	}

	@Override
	public void changeUserPassword(ChangePasswordDTO changePasswordDto) {
		passwordHandler.changeUserPassword(changePasswordDto);
	}

	@Override
	public BooleanWrapper checkInternalUserExists(String dni) {
		BooleanWrapper booleanWrapper = new BooleanWrapper();
		UserDTO user = this.userService.getInternalUserByIdentificationDocument(dni);
		if (user != null) {
			booleanWrapper.setValue(true);
		}
		return booleanWrapper;
	}

	@Override
	public void deleteSuspendedProduct(String id) {

		this.financiacionAlimentosService.deleteSuspendedProduct(Long.parseLong(id));
	}

	@Override
	public Collection<String> getLogNames() {
		return this.logService.getLogsName();
	}

	@Override
	public PaginatedListWrapper<BecasSolicitanteDTO> getBecasSolicitantesDTOPaginated(String[] otherParams, PaginationParams paginationParams) {
		String user = otherParams[0];
		paginationParamsSetUserFilter(user, paginationParams);
		PaginatedListWrapper<BecasSolicitanteDTO> wraper = new PaginatedListWrapper<BecasSolicitanteDTO>();
		wraper.setData(becasService.getBecasSolicitantesDTOPaginated(otherParams,paginationParams));
//		wraper.setTotal(becasService.getTotal(generatePaginationParamsForUser(user).getFilters()));
		wraper.setTotal(becasService.getBecasDTOByStatus(22L).size());
		wraper.setRecordsFiltered(becasService.getBecasSolicitantesDTOPaginated(otherParams,paginationParams).size());

		return wraper;
	}

	@Override
	public List<BecasSolicitanteDTO> getBecasSolicitantesDTO() {

  		return this.becasService.getBecasSolicitantesDTO();
	}

	@Override
	public List<IdiomasBecasDTO> getIdiomasBecasByBeca(Long beacaID) {
		return this.becasService.getIdiomasBecasDTOByBeca(beacaID);
	}

	@Override
	public ResolucionProvisionalReportDTO getResolucionProvisionalReporte(String userId, String solicitudeId) {
		return resolucionProvisionalService.getPDFToSign(userId,solicitudeId);
	}

	@Override
	public List<BecasSolicitanteDTO> getBecasSolicitantesByStatusDTO(Long statusId) {
		return becasService.getBecasDTOByStatus(statusId);
	}

	@Override
	public ImpresoFirmadoDTO generarFichaTecnicaAnexoII(IncOferAlimUMEDTO form) {
		final String fileNameInputTemplate= TEMPLATE_FICHA_TEC_ANEXO_II;
		final String fileNameOutput= FICHA_TEC_ANEXO_II_PDF;


		File ficheroTemplate  =  new File(SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + fileNameInputTemplate);


		HashMap<PlaceholdersFichaTecnicaIncl,String> datosReemplazo  = new HashMap<PlaceholdersFichaTecnicaIncl,String>();
		PlaceholdersFichaTecnicaIncl[] placeholders  = PlaceholdersFichaTecnicaIncl.values();
		String content = null;
		for(int j = 0 ; j < placeholders.length ;j++){
			datosReemplazo.put(placeholders[j],"");
		}

		try {
			String nameWithoutExt = fileNameOutput.substring(0,fileNameOutput.indexOf("."));

			File processedHtml = File.createTempFile(nameWithoutExt ,".html");
//			UTF-8
			content = IOUtils.toString(new FileInputStream(ficheroTemplate.getPath()),"UTF-8");
			List<String>  valores =  new ArrayList<String>();
			/*
				Agregamos los valores en la lista  de la misma forma como se encuentran
				enlistados en el enumerado
			 */

			String groupOfPatient = "";
			String nomPersona =  "";
			if(form.getRepresentante()!=null && StringUtils.isNotBlank(form.getRepresentante().getName() ) ){
				nomPersona  = form.getRepresentante().getName();

			}else if(form.getSolicitante()!=null && StringUtils.isNotBlank(form.getSolicitante().getName())){
				nomPersona  = form.getSolicitante().getName();
			}



			datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NOMBRE_PERSONA,	nomPersona);



			datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NO_REGISTRO,		StringUtils.isNotBlank(form.getRegisterNumber()) ? form.getRegisterNumber() : " "   );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NOMBRE_EMPRESA,		StringUtils.isNotBlank(form.getCompanyName()) ? form.getCompanyName() : ""  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.TELEFONO,			StringUtils.isNotBlank(form.getPhone()) ? form.getPhone() : ""    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CORREO,				StringUtils.isNotBlank(form.getEmail()) ? form.getEmail() : ""    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.DIRECCION,			StringUtils.isNotBlank(form.getAddress())? form.getAddress() : ""    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NOMBRE_PRODUCTO,	form.getProductName()	    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NO_REF_PRODUCTO,	form.getProductReferenceNumber()    );
			if(form.getGroupOfPatient().getAdult()){
				groupOfPatient+=" Adultos.";
			}
			if(form.getGroupOfPatient().getBreastfed()){
				groupOfPatient+=" Lactantes.";
			}
			if(form.getGroupOfPatient().getChild()) {
				groupOfPatient +=" Niños.";
			}

    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.GRUPO_PACIENTES_DESTINADO,groupOfPatient   );
    		String administrationMode  = "";
			if(form.getAdministrationMode()!=null && form.getAdministrationMode().getId()!=null){
    			if(form.getAdministrationMode().getId().equals("1")){
					administrationMode = "Sonda";
				}else if(form.getAdministrationMode().getId().equals("2")){
					administrationMode = "Oral";
				}else if(form.getAdministrationMode().getId().equals("3")){
					administrationMode = "Mixta";
				}
			}
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.MODO_ADMINISTRACION,administrationMode    );
    		String physicalState = "";
    		String standardSolution = "";
			if(form.getPhysicalState().getLiquid()){
				physicalState =" Liquido. ";
				standardSolution = form.getPhysicalState().getStandardSolution();
			}

			if(form.getPhysicalState().getDust()){
				physicalState +=" Polvo. ";
				standardSolution = form.getPhysicalState().getStandardSolution();
			}

    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.ESTADO_FISICO,physicalState  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.SOLUCION_ESTANDAR,standardSolution  );

    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.EDAD,form.getAge()    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.DENSIDAD_CALORICA,form.getCaloricDensity()   );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.OSMOLARIDAD,  form.getOsmolarity()  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.OSMOLALIDAD,  form.getOsmolality()  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PESO_MOLECULAR,   form.getMolecularWeight() );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.REPARTO_CALORICO,form.getCaloricDistribution().getProtein()    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PROTEINAS,form.getCaloricDistribution().getProtein()  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.LIPIDOS,form.getCaloricDistribution().getLipids()   );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.HIDRATOS_DE_CARBONO,form.getCaloricDistribution().getCarbohydrates()   );
    		String fiber = "";
    		String fiberType  ="";
    		String qty = "";

			if(form.getFiber()!=null) {
				FibraDTO f  = form.getFiber();
				logger.info("Fibra: {} ", f.getFiber() );
				if (f!=null && f.getFiber()!=null &&  f.getFiber()) {
					fiber = "Sí, " + form.getFiber().getFiberType();
					fiberType =form.getFiber().getFiberType()!=null ?form.getFiber().getFiberType() : "";
					qty =  form.getFiber().getFiberQuantity()!=null ? form.getFiber().getFiberQuantity() : "";
				} else {
					fiber = "No"  ;
				}

			}


    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.FIBRA,fiber    );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CANTIDAD,qty    );

       		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PROPUESTA_,form.getProposal().getTypeCode()  );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CODIGO_TIPO,form.getProposal().getTypeCode() );
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CODIGO_SUBTIPO,form.getProposal().getSubtypeCode());

			datosReemplazo.put(PlaceholdersFichaTecnicaIncl.INDICACIONES_PROD_SOLICITUD_INCLUSION_OFERTA,form.getInstruction()    );
			List<String> tipoEnvase = new ArrayList<String>();
			List<String> contenido = new ArrayList<String>();
			List<String> numEnvases = new ArrayList<String>();
			List<String> sabor = new ArrayList<String>();
			List<String> precio = new ArrayList<String>();
			for(PresentacionEnvaseDTO    presentacionEnvaseDTO : form.getPresentaciones() ){
				tipoEnvase.add(presentacionEnvaseDTO.getContainerType());
				contenido.add(presentacionEnvaseDTO.getContents());
				numEnvases.add(presentacionEnvaseDTO.getNumberOfContainer());
				sabor.add(presentacionEnvaseDTO.getFlavour());
				precio.add(presentacionEnvaseDTO.getCompanyPriceSale());
			}
			datosReemplazo.put(PlaceholdersFichaTecnicaIncl.TIPO_ENVASE_PRESENTACION1,tipoEnvase.size()>0 ? tipoEnvase.get(0)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.TIPO_ENVASE_PRESENTACION2,tipoEnvase.size()>1 ? tipoEnvase.get(1)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.TIPO_ENVASE_PRESENTACION3,tipoEnvase.size()>2 ? tipoEnvase.get(2)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.TIPO_ENVASE_PRESENTACION4,tipoEnvase.size()>3 ? tipoEnvase.get(3)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CONTENIDO_ENVASE_PRESENTACION1,contenido.size()>0 ? contenido.get(0)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CONTENIDO_ENVASE_PRESENTACION2,contenido.size()>1 ? contenido.get(1)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CONTENIDO_ENVASE_PRESENTACION3,contenido.size()>2 ? contenido.get(2)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.CONTENIDO_ENVASE_PRESENTACION4,contenido.size()>3 ? contenido.get(3)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NUM_ENVASES_PRESENTACION1,numEnvases.size()>0 ? numEnvases.get(0)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NUM_ENVASES_PRESENTACION2,numEnvases.size()>1 ? numEnvases.get(1)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NUM_ENVASES_PRESENTACION3,numEnvases.size()>2 ? numEnvases.get(2)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.NUM_ENVASES_PRESENTACION4,numEnvases.size()>3 ? numEnvases.get(3)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.SABOR_PRESENTACION1,sabor.size()>0 ? sabor.get(0)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.SABOR_PRESENTACION2,sabor.size()>1 ? sabor.get(1)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.SABOR_PRESENTACION3,sabor.size()>2? sabor.get(2)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.SABOR_PRESENTACION4,sabor.size()>3 ? sabor.get(3)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PRECIO_VENTA_EMPRESA_PRESENTACION1,precio.size()>0 ? precio.get(0)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PRECIO_VENTA_EMPRESA_PRESENTACION2,precio.size()>1 ? precio.get(1)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PRECIO_VENTA_EMPRESA_PRESENTACION3,precio.size()>2 ? precio.get(2)    : "");
    		datosReemplazo.put(PlaceholdersFichaTecnicaIncl.PRECIO_VENTA_EMPRESA_PRESENTACION4,precio.size()>3 ? precio.get(3)    : "");



			content  = ReportUtil.replaceValues(datosReemplazo,content);
			IOUtils.write(content,new FileOutputStream(processedHtml),"UTF-8");
			content=ReportUtil.generatePdfFromHTML(processedHtml,fileNameOutput,content);

		} catch (IOException e) {
			logger.error("Error intentando generar ficha tecnica : {}",e);
		}
		ImpresoFirmadoDTO impresoFirmadoDTO = new ImpresoFirmadoDTO();

		impresoFirmadoDTO.setPdfB64(content);
		impresoFirmadoDTO.setReportName("ficha_tecnica.pdf");


		return  impresoFirmadoDTO;

	}


	@Override
	public ImpresoFirmadoDTO getPDFAnexoIIPDF(String user, IncOferAlimUMEDTO impresoAnexoIIDto) {

		String fileNameInputTemplate  = SecosanConstants.PATH_REPO + Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_REPORT_TO_SIGN_PATH) + SecosanConstants.ANEXO_II_TEMPLATE ;
		final String fileNameOutput= SecosanConstants.ANEXO_II_PDF;
		String content = "";
		try {

			File  ficheroTemplate =  new File(fileNameInputTemplate);


			String nameWithoutExt = fileNameOutput.substring(0,fileNameOutput.indexOf("."));
			File processedHtml = File.createTempFile(nameWithoutExt ,".html");
			//			UTF-8
			content = IOUtils.toString(new FileInputStream(ficheroTemplate.getPath()),"UTF-8");

			HashMap<String,String> datosReemplazo  = new HashMap<String,String>();
			/*Inicializamos */
		/*	datosReemplazo.put("_ANO_","	");
			datosReemplazo.put("_MES_","	");
			datosReemplazo.put("_DIA_","	");*/
			datosReemplazo.put("_NUMREF_","			");
			datosReemplazo.put("_NOMPROD_","		");
			datosReemplazo.put("_NOMPERSONA_","		");
			datosReemplazo.put("_NOMEMPRE_","....................");


			final String numRef = impresoAnexoIIDto.getProductReferenceNumber();
			final String nomProd = impresoAnexoIIDto.getProductName();
			final String nomEmpre = impresoAnexoIIDto.getCompanyName();
			String nomPersona =  "";
			if(impresoAnexoIIDto.getRepresentante()!=null && StringUtils.isNotBlank(impresoAnexoIIDto.getRepresentante().getName() ) ){
					nomPersona  = impresoAnexoIIDto.getRepresentante().getName();

			}else if(impresoAnexoIIDto.getSolicitante()!=null && StringUtils.isNotBlank(impresoAnexoIIDto.getSolicitante().getName())){
					nomPersona  = impresoAnexoIIDto.getSolicitante().getName();
			}



			Calendar c = Calendar.getInstance();
			datosReemplazo.put("_NOMPROD_",nomProd)	;
			datosReemplazo.put("_NUMREF_",numRef);
			datosReemplazo.put("_NOMEMPRE_",nomEmpre);
			datosReemplazo.put("_NOMPERSONA_",nomPersona);


			if(impresoAnexoIIDto.getInscriptionDate()!=null){
				Date date = new Date(impresoAnexoIIDto.getInscriptionDate());
				SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy");
				String dateString = df2.format(date);

				datosReemplazo.put("_FECHA_",dateString);

				// c.setTimeInMillis(impresoAnexoIIDto.getInscriptionDate());
				// final String year = 	  Integer.toString(c.get(Calendar.YEAR));
				// int mes = Calendar.MONTH +1 ;
				// String month = "";
				// if(mes<10) {
				// 	  month = "0" + Integer.toString(mes);
				// }else{
				// 	  month = Integer.toString(mes);
				// }

				// final String dayOfMonth = Integer.toString(c.get(Calendar.DAY_OF_MONTH));
				// datosReemplazo.put("_ANO_",year)		;
				// datosReemplazo.put("_MES_",month);
				// datosReemplazo.put("_DIA_",dayOfMonth);
			}
			content  = ReportUtil.replaceValuesMap(datosReemplazo,content);

			IOUtils.write(content,new FileOutputStream(processedHtml),"UTF-8");

			content=ReportUtil.generatePdfFromHTML(processedHtml,fileNameOutput,content);

		} catch (IOException e) {
			logger.error("Error intentando generar ficha tecnica : {}",e);
		}
		ImpresoFirmadoDTO impresoFirmadoDTO = new ImpresoFirmadoDTO();

		impresoFirmadoDTO.setPdfB64(content);
		impresoFirmadoDTO.setReportName("anexo_ii.pdf");


		return  impresoFirmadoDTO;

	}

	@Override
	public LogDTO getLog(String fileName) {
		return this.logService.getLogFile(fileName);
	}


}