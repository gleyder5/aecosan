package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.financiacionAlimentos.ProductoSuspendidoDTO;

public class ImpresoAnexoIVDTO extends ImpresoFirmadoDTO {

	@JsonProperty("company")
	@XmlElement(name = "company")
	private String company;

	@JsonProperty("signer")
	@XmlElement(name = "signer")
	private String signer;

	@JsonProperty("address")
	@XmlElement(name = "address")
	private String address;

	@JsonProperty("companyRegNumber")
	@XmlElement(name = "companyRegNumber")
	private String companyRegNumber;

	@JsonProperty("personInCharge")
	@XmlElement(name = "personInCharge")
	private String personInCharge;

	@JsonProperty("phone")
	@XmlElement(name = "phone")
	private String phone;

	@JsonProperty("mail")
	@XmlElement(name = "mail")
	private String mail;

	@JsonProperty("comunicationDate")
	@XmlElement(name = "comunicationDate")
	private Long comunicationDate;

	@JsonProperty("comunicantionType")
	@XmlElement(name = "comunicantionType")
	private String comunicantionType;

	@JsonProperty("motive")
	@XmlElement(name = "motive")
	private String motive;

	@JsonProperty("products")
	@XmlElement(name = "products")
	private List<ProductoSuspendidoDTO> products;


	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getSigner() {
		return signer;
	}

	public void setSigner(String signer) {
		this.signer = signer;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCompanyRegNumber() {
		return companyRegNumber;
	}

	public void setCompanyRegNumber(String companyRegNumber) {
		this.companyRegNumber = companyRegNumber;
	}

	public String getPersonInCharge() {
		return personInCharge;
	}

	public void setPersonInCharge(String personInCharge) {
		this.personInCharge = personInCharge;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getComunicantionType() {
		return comunicantionType;
	}

	public void setComunicantionType(String comunicantionType) {
		this.comunicantionType = comunicantionType;
	}

	public String getMotive() {
		return motive;
	}

	public void setMotive(String motive) {
		this.motive = motive;
	}

	public List<ProductoSuspendidoDTO> getProducts() {
		return products;
	}

	public void setProducts(List<ProductoSuspendidoDTO> products) {
		this.products = products;
	}

	public Long getComunicationDate() {
		return comunicationDate;
	}

	public void setComunicationDate(Long comunicationDate) {
		this.comunicationDate = comunicationDate;
	}

	@Override
	public String toString() {
		return "ImpresoAnexoIVDTO [company=" + company + ", signer=" + signer + ", address=" + address + ", companyRegNumber=" + companyRegNumber + ", personInCharge=" + personInCharge + ", phone="
				+ phone + ", mail=" + mail + ", comunicationDate=" + comunicationDate + ", comunicantionType=" + comunicantionType + ", motive=" + motive + ", products=" + products + "]";
	}




}
