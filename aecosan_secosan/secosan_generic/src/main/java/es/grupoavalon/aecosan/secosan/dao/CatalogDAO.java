package es.grupoavalon.aecosan.secosan.dao;

import java.util.List;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;

public interface CatalogDAO {
	List<AbstractCatalogEntity> getCatalogItems(String catalogName);

	List<AbstractCatalogEntity> getCatalogItems(String catalogName, String parentElement, String parentId);

	List<AbstractCatalogEntity> getCatalogItems(String catalogName, String tag);

	AbstractCatalogEntity getCatalogItem(String catalogName, long catalogId);
}
