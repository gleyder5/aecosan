package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.List;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.TipoEnvioEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;

@Component("TipoEnvio")
public class TipoEnvioRepositoryImpl extends CrudCatalogRepositoryImpl<TipoEnvioEntity, Long> implements GenericCatalogRepository<TipoEnvioEntity> {

	@Override
	protected Class<TipoEnvioEntity> getClassType() {
		return TipoEnvioEntity.class;
	}

	@Override
	public List<TipoEnvioEntity> getCatalogList() {
		return findAllOrderByValue();
	}
}
