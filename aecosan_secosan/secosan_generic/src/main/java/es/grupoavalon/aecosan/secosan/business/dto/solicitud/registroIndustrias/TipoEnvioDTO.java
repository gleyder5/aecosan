package es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias;

import es.grupoavalon.aecosan.secosan.business.dto.AbstractCatalogDto;
import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;

public class TipoEnvioDTO extends AbstractCatalogDto implements IsNulable<TipoEnvioDTO> {

	@Override
	public String toString() {
		return "TipoEnvioDTO [id=" + id + "]";
	}

	@Override
	public TipoEnvioDTO shouldBeNull() {
		if ("".equals(getId()) || getId() == null) {
			return null;
		} else {
			return this;
		}
	}

}
