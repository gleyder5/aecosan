package es.grupoavalon.aecosan.secosan.business.dto.solicitud;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public abstract class AbstractPersonDataDTO {

	@JsonProperty("name")
	@XmlElement(name = "name")
	@BeanToBeanMapping(getValueFrom = "name")
	private String name;

	@JsonProperty("identificationNumber")
	@XmlElement(name = "identificationNumber")
	@BeanToBeanMapping(getValueFrom = "identificationNumber")
	private String identificationNumber;

	@JsonProperty("telephoneNumber")
	@XmlElement(name = "telephoneNumber")
	@BeanToBeanMapping(getValueFrom = "telephoneNumber")
	private String telephoneNumber;

	@JsonProperty("email")
	@XmlElement(name = "email")
	@BeanToBeanMapping(getValueFrom = "email")
	private String email;

	@JsonProperty("address")
	@XmlElement(name = "address")
	@BeanToBeanMapping(getValueFrom = "address")
	private String address;

	@JsonProperty("location")
	@XmlElement(name = "location")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "location")
	private UbicacionGeograficaDTO location;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentificationNumber() {
		return identificationNumber;
	}

	public void setIdentificationNumber(String identificationNumber) {
		this.identificationNumber = identificationNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public UbicacionGeograficaDTO getLocation() {
		return location;
	}

	public void setLocation(UbicacionGeograficaDTO location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "AbstractPersonDataDTO [name=" + name + ", identificationNumber=" + identificationNumber + ", telephoneNumber=" + telephoneNumber + ", email=" + email + ", address=" + address
				+ ", location=" + location + "]";
	}

	protected boolean checkNullability() {
		boolean nullabillity = true;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getName());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getIdentificationNumber());
		nullabillity = nullabillity && null == this.getTelephoneNumber();
		nullabillity = nullabillity && StringUtils.isEmpty(this.getEmail());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getAddress());
		nullabillity = nullabillity && null == this.getLocation().shouldBeNull();

		return nullabillity;
	}

}
