package es.grupoavalon.aecosan.secosan.dao.repository;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.PresentacionEnvaseEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ProductoSuspendidoEntity;

public interface PresentacionEnvaseRepository extends CrudRepository<PresentacionEnvaseEntity, Long> {

    void deleteByFinanciacionId(Long id);
}
