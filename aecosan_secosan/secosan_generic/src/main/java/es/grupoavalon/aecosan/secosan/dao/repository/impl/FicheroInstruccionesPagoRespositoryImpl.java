package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.FicheroInstruccionesPagoRepository;

@Component
public class FicheroInstruccionesPagoRespositoryImpl extends AbstractCrudRespositoryImpl<FicheroInstruccionesPagoEntity, Long> implements FicheroInstruccionesPagoRepository {

	private static final String AREA = "area";
	private static final String FORM = "form";

	@Override
	protected Class<FicheroInstruccionesPagoEntity> getClassType() {
		return FicheroInstruccionesPagoEntity.class;
	}

	@Override
	public List<FicheroInstruccionesPagoEntity> findAllByArea(Long areaId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(AREA, areaId);
		return findByNamedQuery(NamedQueriesLibrary.GET_PAYMENT_INSTRUCTIONS_LIST_BY_AREA, queryParams);
	}

	@Override
	public FicheroInstruccionesPagoEntity findByForm(Long formId) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(FORM, formId);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_PAYMENT_INSTRUCTIONS_BY_FORM, queryParams);
	}

}
