package es.grupoavalon.aecosan.secosan.business.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.FicheroInstruccionesService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsFileDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsListDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.PaymentInstructionsDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.SolicitudeInstructionsDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.FicheroInstruccionesDAO;
import es.grupoavalon.aecosan.secosan.dao.TipoFormularioDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.FicheroInstruccionesPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;

@Service
public class FicheroInstruccionesServiceImpl extends AbstractManager implements FicheroInstruccionesService {

	@Autowired
	private FicheroInstruccionesDAO ficheroInstruccionesDAO;

	@Autowired
	private TipoFormularioDAO tipoFormularioDAO;


	@Autowired
	private UsuarioDAO usuarioDAO;

	private static final String FILE_INTRUCTIONS_FOLDER;
	private static final String FILE_PAYMENT_INTRUCTIONS_FOLDER;
	static {
		String baseFileInstructionsFolder = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_FILE_INTRUCTIONS_FOLDER);
		String basePaymentInstructionFolder = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_FILE_PAYMENT_INTRUCTIONS_FOLDER);
		FILE_INTRUCTIONS_FOLDER = SecosanConstants.PATH_REPO + baseFileInstructionsFolder;
		FILE_PAYMENT_INTRUCTIONS_FOLDER = SecosanConstants.PATH_REPO + basePaymentInstructionFolder;
	}

	@Override
	public InstructionsFileDTO findInstructionsFileBysolicitudeType(String solicitudeType) {
		InstructionsFileDTO ficheroInstrucciones = null;
		try {
			FicheroInstruccionesEntity ficheroInstruccionesEntity = ficheroInstruccionesDAO.findInstructionsFileBySolicitudeType(Long.valueOf(solicitudeType));
			ficheroInstrucciones = transformEntityToDTO(ficheroInstruccionesEntity, InstructionsFileDTO.class);
			if (ficheroInstrucciones == null) {
				throw new ServiceException(ServiceException.FILE_NOT_FOUND);
			} else {
				ficheroInstrucciones.setFile(generateInstructionsFile(FILE_INTRUCTIONS_FOLDER, ficheroInstrucciones.getCatalogValue()));
			}

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getFicheroInstruccionesByIdFormulario | formularioId: " + solicitudeType);
		}
		return ficheroInstrucciones;
	}

	@Override
	public void uploadInstructionsToPay(InstructionsFileDTO ficheroInstrucciones) {
		try {
			writeInstructionsInServer(FILE_PAYMENT_INTRUCTIONS_FOLDER, ficheroInstrucciones.getCatalogValue(), ficheroInstrucciones.getBase64());
			addInstructionsToPay(ficheroInstrucciones);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " uploadInstructionsToPay | ficheroInstrucciones: " + ficheroInstrucciones);
		}
	}

	private void addInstructionsToPay(InstructionsFileDTO ficheroInstrucciones) {
		FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity = ficheroInstruccionesDAO.findInstructionsFileByForm(Long.valueOf(ficheroInstrucciones.getId()));
		if (ficheroInstruccionesPagoEntity == null) {
			ficheroInstruccionesDAO.addPaymentInstructions(generateFicheroInstruccionesPagoEntityFromDTO(ficheroInstrucciones));
		} else {
			ficheroInstruccionesDAO.updatePaymentInstructions(updateInstruccionesPagoEntityFromDTO(ficheroInstruccionesPagoEntity, ficheroInstrucciones));
		}
	}

	private FicheroInstruccionesPagoEntity generateFicheroInstruccionesPagoEntityFromDTO(InstructionsFileDTO ficheroInstrucciones) {
		FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity = new FicheroInstruccionesPagoEntity();
		ficheroInstruccionesPagoEntity.setCatalogValue(ficheroInstrucciones.getCatalogValue());
		ficheroInstruccionesPagoEntity.setFormType(findFormType(ficheroInstrucciones.getId()));
		return ficheroInstruccionesPagoEntity;
	}

	private FicheroInstruccionesPagoEntity updateInstruccionesPagoEntityFromDTO(FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity, InstructionsFileDTO ficheroInstrucciones) {
		ficheroInstruccionesPagoEntity.setCatalogValue(ficheroInstrucciones.getCatalogValue());
		return ficheroInstruccionesPagoEntity;
	}

	private TipoFormularioEntity findFormType(String formTypeId) {
		return tipoFormularioDAO.findFormulario(Long.valueOf(formTypeId));
	}

	@Override
	public InstructionsListDTO findInstructionsLists(String userId) {
		InstructionsListDTO listInstructions = new InstructionsListDTO();
		try {
			listInstructions.setListPaymentInstructions(getPaymentInstructionByArea(getAreaFromUser(userId)));
			listInstructions.setListSolicitudeInstructions(getInstructionByArea(getAreaFromUser(userId)));
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " getInstructionsLists | UserId: " + userId);
		}
		return listInstructions;
	}

	private Long getAreaFromUser(String userId) {
		return usuarioDAO.findUserInternalById(Long.valueOf(userId)).getArea().getId();
	}

	private List<PaymentInstructionsDTO> getPaymentInstructionByArea(Long area) {
		List<FicheroInstruccionesPagoEntity> listPaymentInstructions = ficheroInstruccionesDAO.findAllPaymentInstructionsByArea(area);
		return transformEntityListToDTOList(listPaymentInstructions, FicheroInstruccionesPagoEntity.class, PaymentInstructionsDTO.class);
	}

	private List<SolicitudeInstructionsDTO> getInstructionByArea(Long area) {
		List<FicheroInstruccionesEntity> listSolicitudeInstructions = ficheroInstruccionesDAO.findAllInstructionsByArea(area);
		return transformEntityListToDTOList(listSolicitudeInstructions, FicheroInstruccionesEntity.class, SolicitudeInstructionsDTO.class);
	}

	@Override
	public InstructionsFileDTO findInstructionsFileByForm(String formId) {
		InstructionsFileDTO instructionsFile = null;
		try {
			FicheroInstruccionesPagoEntity ficheroInstruccionesPagoEntity = ficheroInstruccionesDAO.findInstructionsFileByForm(Long.valueOf(formId));
			instructionsFile = transformEntityToDTO(ficheroInstruccionesPagoEntity, InstructionsFileDTO.class);

			if (instructionsFile == null) {
				throw new ServiceException(ServiceException.FILE_NOT_FOUND);
			} else {
				instructionsFile.setFile(generateInstructionsFile(FILE_PAYMENT_INTRUCTIONS_FOLDER, instructionsFile.getCatalogValue()));
			}

		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " findFicheroInstruccionesByForm | formId: " + formId);
		}
		return instructionsFile;
	}

	@Override
	public void uploadInstructions(InstructionsFileDTO ficheroInstrucciones) {
		try {
			writeInstructionsInServer(FILE_INTRUCTIONS_FOLDER, ficheroInstrucciones.getCatalogValue(), ficheroInstrucciones.getBase64());
			addInstructions(ficheroInstrucciones);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " uploadInstructionsToPay | ficheroInstrucciones: " + ficheroInstrucciones);
		}
	}

	private void writeInstructionsInServer(String path, String fileName, String b64) throws IOException {
		File instructionsFile = generateInstructionsFile(path, fileName);
		byte[] paymentInstructions = getInstructionsFromBase64ToByteArray(b64);
		FileUtils.writeByteArrayToFile(instructionsFile, paymentInstructions);
	}

	private static File generateInstructionsFile(String path, String fileName) {
		return new File(path + "/" + fileName);
	}

	private static byte[] getInstructionsFromBase64ToByteArray(String b64) {
		return (byte[]) new Base64TobytesArrayTransformer().reverseTransform(b64, byte[].class);
	}

	private void addInstructions(InstructionsFileDTO ficheroInstrucciones) {
		FicheroInstruccionesEntity ficheroInstruccionesEntity = ficheroInstruccionesDAO.findInstructionsFileBySolicitudeType(Long.valueOf(ficheroInstrucciones.getId()));
		if (ficheroInstruccionesEntity == null) {
			ficheroInstruccionesDAO.addInstructions(generateFicheroInstruccionesEntityFromDTO(ficheroInstrucciones));
		} else {
			ficheroInstruccionesDAO.updateInstructions(updateInstruccionesEntityFromDTO(ficheroInstruccionesEntity, ficheroInstrucciones));
		}
	}

	private FicheroInstruccionesEntity generateFicheroInstruccionesEntityFromDTO(InstructionsFileDTO ficheroInstrucciones) {
		FicheroInstruccionesEntity ficheroInstruccionesEntity = new FicheroInstruccionesEntity();
		ficheroInstruccionesEntity.setCatalogValue(ficheroInstrucciones.getCatalogValue());
		ficheroInstruccionesEntity.setFormType(tipoFormularioDAO.findFormulario(Long.valueOf(ficheroInstrucciones.getId())));
		return ficheroInstruccionesEntity;
	}

	private FicheroInstruccionesEntity updateInstruccionesEntityFromDTO(FicheroInstruccionesEntity ficheroInstruccionesEntity, InstructionsFileDTO ficheroInstrucciones) {
		ficheroInstruccionesEntity.setCatalogValue(ficheroInstrucciones.getCatalogValue());
		return ficheroInstruccionesEntity;
	}

}
