package es.grupoavalon.aecosan.secosan.dao.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioInternoEntity;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.UsuarioInternoRepository;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.pagination.QueryOrder;

@Component
public class UsuarioInternoRepositoryImpl extends AbstractCrudRespositoryImpl<UsuarioInternoEntity, Long> implements UsuarioInternoRepository {

	private static final String QUERY = "#Query: ";
	private static final String FIELD_NAME = "name";
	private static final String FIELD_LAST_NAME = "lastName";
	private static final String FIELD_BLOCK = "userBlocked";
	private static final String FIELD_AREA = "area.id";
	private static final String FIELD_AREA_DESC = "area.catalogValue";
	private static final String FIELD_LOGIN = "login";
	private static final String FIELD_DELETED = "userDeleted";

	@Override
	protected Class<UsuarioInternoEntity> getClassType() {
		return UsuarioInternoEntity.class;
	}

	@Override
	public UsuarioInternoEntity findByUserName(String userName) {
		Map<String, Object> queryParams = new HashMap<String, Object>(1);
		queryParams.put(FIELD_LOGIN, userName);
		return findOneByNamedQuery(NamedQueriesLibrary.GET_USUARIO_INTERNO_BY_NAME, queryParams);
	}

	@Override
	public List<UsuarioInternoEntity> findAllByPaginationParams(PaginationParams paginationParams) {
		List<FilterParams> filters = paginationParams.getFilters();

		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryWherePart = getQueryParts(filters, queryParams);
		if(logger.isDebugEnabled()) {
			logger.debug(QUERY + queryWherePart);
		}
		String sortField = getSortField(paginationParams.getSortField());

		logger.debug("sortfield {}",sortField);
		List<UsuarioInternoEntity> resultList;
		if (paginationParams.getOrder().equals(QueryOrder.ASC)) {
			resultList = findAllPaginatedAndSortedASC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		} else {
			resultList = findAllPaginatedAndSortedDESC(queryWherePart, paginationParams.getStart(), paginationParams.getLength(), sortField, queryParams);
		}

		return resultList;
	}

	private String getSortField(String sortField) {
		String sortFiledTranslated = sortField;
		if (StringUtils.equals(sortField, SecosanConstants.FIELD_NAME)) {
			sortFiledTranslated = FIELD_NAME;
		} else if (StringUtils.equals(sortField, SecosanConstants.FIELD_LAST_NAME)) {
			sortFiledTranslated = FIELD_LAST_NAME;
		} else if (StringUtils.equals(sortField, SecosanConstants.FIELD_BLOCK)) {
			sortFiledTranslated = FIELD_BLOCK;
		} else if (StringUtils.equals(sortField, SecosanConstants.FIELD_AREA)) {
			sortFiledTranslated = FIELD_AREA_DESC;
		} else if (StringUtils.equals(sortField, SecosanConstants.FIELD_LOGIN)) {
			sortFiledTranslated = FIELD_LOGIN;
		} else if (StringUtils.equals(sortField, SecosanConstants.FIELD_DELETED)) {
			sortFiledTranslated = FIELD_DELETED;
		}

		return sortFiledTranslated;
	}

	private String getQueryParts(List<FilterParams> filters, Map<String, Object> queryParams) {
		QueryWhereParts queryParts = new QueryWhereParts();

		for (FilterParams f : filters) {
			if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_NAME)) {
				queryParts.setPart(generateLikeSentence(FIELD_NAME, SecosanConstants.FIELD_NAME));
				queryParams.put(SecosanConstants.FIELD_NAME, f.getFilterValue() + "%");
			} else if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_LAST_NAME)) {
				queryParts.setPart(generateLikeSentence(FIELD_LAST_NAME, SecosanConstants.FIELD_LAST_NAME));
				queryParams.put(SecosanConstants.FIELD_LAST_NAME, f.getFilterValue() + "%");
			} else if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_BLOCK)) {
				queryParts.setPart(generateEqualSentence(FIELD_BLOCK, SecosanConstants.FIELD_BLOCK));
				queryParams.put(SecosanConstants.FIELD_BLOCK, Boolean.valueOf(f.getFilterValue()));
			} else if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_AREA)) {
				queryParts.setPart(generateEqualSentence(FIELD_AREA, SecosanConstants.FIELD_AREA));
				queryParams.put(SecosanConstants.FIELD_AREA, Long.valueOf(f.getFilterValue()));
			} else if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_LOGIN)) {
				queryParts.setPart(generateLikeSentence(FIELD_LOGIN, SecosanConstants.FIELD_LOGIN));
				queryParams.put(SecosanConstants.FIELD_LOGIN, f.getFilterValue() + "%");
			} else if (StringUtils.equalsIgnoreCase(f.getFilterName(), SecosanConstants.FIELD_DELETED)) {
				queryParts.setPart(generateEqualSentence(FIELD_DELETED, SecosanConstants.FIELD_DELETED));
				queryParams.put(SecosanConstants.FIELD_DELETED, Boolean.valueOf(f.getFilterValue()));
			} else {
				logger.warn("Using default filter option, the name of the param will be used in the query");
				queryParts.setPart(generateEqualSentence(f.getFilterName(), f.getFilterValue()));
			}
		}
		return assambleQuery("", queryParts);
	}

	private static String generateLikeSentence(String fieldName, String fieldValue) {
		return "LOWER(" + fieldName + ") like LOWER(:" + fieldValue + ")";
	}

	private static String generateEqualSentence(String fieldName, String fieldValue) {
		return fieldName + " = :" + fieldValue;
	}

	@Override
	public int getTotalListRecords(List<FilterParams> filters) {
		Map<String, Object> queryParams = new HashMap<String, Object>();
		return getTotalRecords(getQueryParts(filters, queryParams), queryParams);
	}

	@Override
	public int getFilteredListRecords(List<FilterParams> filters) {
		Map<String, Object> queryParams = new HashMap<String, Object>();

		String queryName = getQueryParts(filters, queryParams);
		if(logger.isDebugEnabled()){
			logger.debug(QUERY + queryName);
		}
		return getTotalRecords(queryName, queryParams);
	}

}
