package es.grupoavalon.aecosan.secosan.rest.response.wrapper;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;

public class SolicitudDTOReadWrapper {

	private String name;
	private SolicitudDTO solicitude;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SolicitudDTO getSolicitude() {
		return solicitude;
	}

	public void setSolicitude(SolicitudDTO solicitude) {
		this.solicitude = solicitude;
	}

}
