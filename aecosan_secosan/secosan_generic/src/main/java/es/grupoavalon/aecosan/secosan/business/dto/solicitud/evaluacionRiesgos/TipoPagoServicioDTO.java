package es.grupoavalon.aecosan.secosan.business.dto.solicitud.evaluacionRiesgos;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;

public class TipoPagoServicioDTO {

	@JsonProperty("idPayType")
	@XmlElement(name = "idPayType")
	@BeanToBeanMapping(getValueFrom = "payType", getSecondValueFrom = "id")
	private Long idPayType;

	@JsonProperty("descPayType")
	@XmlElement(name = "descPayType")
	@BeanToBeanMapping(getValueFrom = "payType", getSecondValueFrom = "catalogValue")
	private String descPayType;

	@JsonProperty("idService")
	@XmlElement(name = "idService")
	@BeanToBeanMapping(getValueFrom = "service", getSecondValueFrom = "id")
	private Long idService;

	@JsonProperty("descService")
	@XmlElement(name = "descService")
	@BeanToBeanMapping(getValueFrom = "service", getSecondValueFrom = "catalogValue")
	@IgnoreField(inReverse = true)
	private String descService;

	public Long getIdPayType() {
		return idPayType;
	}

	public void setIdPayType(Long idPayType) {
		this.idPayType = idPayType;
	}

	public String getDescPayType() {
		return descPayType;
	}

	public void setDescPayType(String descPayType) {
		this.descPayType = descPayType;
	}

	public Long getIdService() {
		return idService;
	}

	public void setIdService(Long idService) {
		this.idService = idService;
	}

	public String getDescService() {
		return descService;
	}

	public void setDescService(String descService) {
		this.descService = descService;
	}

	@Override
	public String toString() {
		return "TipoPagoServicioDTO [idPayType=" + idPayType + ", descPayType=" + descPayType + ", idService=" + idService + ", descService=" + descService + "]";
	}

}
