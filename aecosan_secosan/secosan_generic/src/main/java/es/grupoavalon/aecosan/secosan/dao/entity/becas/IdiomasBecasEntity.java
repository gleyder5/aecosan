package es.grupoavalon.aecosan.secosan.dao.entity.becas;

import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

import javax.persistence.*;

@Entity
@Table(name = TableNames.TABLA_IDIOMAS_BECAS_ENTITY)
@NamedQueries(
        {
                @NamedQuery(name = NamedQueriesLibrary.GET_IDIOMAS_BECAS_BY_BECA, query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE + ".becas.IdiomasBecasEntity AS p WHERE p.beca.id = :becaId"),
                @NamedQuery(name = NamedQueriesLibrary.GET_IDIOMAS_BECAS_BY_BECA_AND_IDIOMA, query = "SELECT p FROM " + TableNames.ENTITY_PACKAGE + ".becas.IdiomasBecasEntity AS p WHERE p.beca.id = :becaId and p.nombre=:idioma"),

        })
public class IdiomasBecasEntity {
    @Id
    @GeneratedValue(generator = SequenceNames.IDIOMAS_BECAS_SEQUENCE)
    @SequenceGenerator(name = SequenceNames.IDIOMAS_BECAS_SEQUENCE, sequenceName = SequenceNames.IDIOMAS_BECAS_SEQUENCE, allocationSize = 1)
    private Long id;


    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "PUNTOS")
    private Double puntos;



   @ManyToOne(optional = false, fetch = FetchType.LAZY)
   @JoinColumn(name = "BECAS_ID", referencedColumnName = "ID")
   private BecasEntity beca;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPuntos() {
        return puntos;
    }

    public void setPuntos(Double puntos) {
        this.puntos = puntos;
    }

    public BecasEntity getBeca() {
        return beca;
    }

    public void setBeca(BecasEntity beca) {
        this.beca = beca;
    }
}


