package es.grupoavalon.aecosan.secosan.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_TIPODOCUMENTO_FORMULARIO)
public class TipoDocumentacionFormularioEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.TIPODOCUMENTO_FORMULARIO_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.TIPODOCUMENTO_FORMULARIO_SEQUENCE, sequenceName = SequenceNames.TIPODOCUMENTO_FORMULARIO_SEQUENCE, allocationSize = 1)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TIPO_DOCUMENTACION_ID", nullable = false)
	private TipoDocumentacionEntity docType;

	@Column(name = "TIPO_FORMULARIO_ID", nullable = false)
	private Long formType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoDocumentacionEntity getDocType() {
		return docType;
	}

	public void setDocType(TipoDocumentacionEntity docType) {
		this.docType = docType;
	}

	public Long getFormType() {
		return formType;
	}

	public void setFormType(Long formType) {
		this.formType = formType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((docType == null) ? 0 : docType.getId().hashCode());
		result = prime * result + ((formType == null) ? 0 : formType.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDocumentacionFormularioEntity other = (TipoDocumentacionFormularioEntity) obj;
		if (docType == null) {
			if (other.docType != null)
				return false;
		} else if (!docType.getId().equals(other.docType.getId()))
			return false;
		if (formType == null) {
			if (other.formType != null)
				return false;
		} else if (!formType.equals(other.formType))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String docTypeId = "";
		if (docType != null) {
			docTypeId = String.valueOf(docType.getId());
		}

		return "TipoDocumentacionFormularioEntity [id=" + id + ", docType=" + docTypeId + ", formType=" + formType + "]";
	}

}
