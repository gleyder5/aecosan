package es.grupoavalon.aecosan.secosan.business.exception;

public class EpaymentException extends ServiceException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String paymentWsRemoteMessage;

	public EpaymentException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}
	public EpaymentException(String message) {
		super(message);
	}

	public EpaymentException(String arg0, String remoteMessage, Throwable arg1) {
		super(arg0, arg1);
		this.paymentWsRemoteMessage = remoteMessage;
	}

	public String getPaymentWsRemoteMessage() {
		return paymentWsRemoteMessage;
	}
}
