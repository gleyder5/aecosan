/**
 * 
 */
package test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto;

/**
 *  Class used only to test the DTO Factory. Emulates a related (relationship 1-n,1-1) Entity
 * @author otto.abreu

 *
 */
public class EntityRelatedMock {
	
	private String stringAtribute;
	private int intValue;
	private boolean boolVal;
	private String haveSameNameStringRelated;
	private int haveSameNameIntRelated;
	
	public String getStringAtribute() {
		return stringAtribute;
	}
	public void setStringAtribute(String stringAtribute) {
		this.stringAtribute = stringAtribute;
	}
	public int getIntValue() {
		return intValue;
	}
	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}
	public boolean isBoolVal() {
		return boolVal;
	}
	public void setBoolVal(boolean boolVal) {
		this.boolVal = boolVal;
	}
	
	
	public String getHaveSameNameStringRelated() {
		return haveSameNameStringRelated;
	}
	public void setHaveSameNameStringRelated(String haveSameNameStringRelated) {
		this.haveSameNameStringRelated = haveSameNameStringRelated;
	}
	public int getHaveSameNameIntRelated() {
		return haveSameNameIntRelated;
	}
	public void setHaveSameNameIntRelated(int haveSameNameIntRelated) {
		this.haveSameNameIntRelated = haveSameNameIntRelated;
	}
	@Override
	public String toString() {
		return "EntityRelatedMock [stringAtribute=" + stringAtribute
				+ ", intValue=" + intValue + ", boolVal=" + boolVal
				+ ", haveSameNameStringRelated=" + haveSameNameStringRelated
				+ ", haveSameNameIntRelated=" + haveSameNameIntRelated + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (boolVal ? 1231 : 1237);
		result = prime * result + haveSameNameIntRelated;
		result = prime
				* result
				+ ((haveSameNameStringRelated == null) ? 0
						: haveSameNameStringRelated.hashCode());
		result = prime * result + intValue;
		result = prime * result
				+ ((stringAtribute == null) ? 0 : stringAtribute.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityRelatedMock other = (EntityRelatedMock) obj;
		if (boolVal != other.boolVal)
			return false;
		if (haveSameNameIntRelated != other.haveSameNameIntRelated)
			return false;
		if (haveSameNameStringRelated == null) {
			if (other.haveSameNameStringRelated != null)
				return false;
		} else if (!haveSameNameStringRelated
				.equals(other.haveSameNameStringRelated))
			return false;
		if (intValue != other.intValue)
			return false;
		if (stringAtribute == null) {
			if (other.stringAtribute != null)
				return false;
		} else if (!stringAtribute.equals(other.stringAtribute))
			return false;
		return true;
	}
	

	public boolean equalsReverseFill(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityRelatedMock other = (EntityRelatedMock) obj;
		if (boolVal != other.boolVal)
			return false;
		if (intValue != other.intValue)
			return false;
		if (stringAtribute == null) {
			if (other.stringAtribute != null)
				return false;
		} else if (!stringAtribute.equals(other.stringAtribute))
			return false;
		return true;
	}
	
}
