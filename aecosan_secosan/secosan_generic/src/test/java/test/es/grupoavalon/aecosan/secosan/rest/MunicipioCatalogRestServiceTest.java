package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class MunicipioCatalogRestServiceTest extends GenericRestTest {

	private static final String PETICION_MUNICIPIOS_POR_PROVINCIA = "/Provincia_id/39";
	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = SECURE + "/catalog";
	private static final String REST_SERVICE_PARAMS = "/Municipio";
	private static final String CATALOGOS_JSON_FOLDER = "catalogos/";
	private static final String SOLICITUD_JSON_FOLDER = "municipio/";

	private static final String EXPECTED_OK_JSON_FILE = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + "municipioCatalog.json";
	private static final String EXPECTED_OK_JSON_FILE_PROVINCIA_39 = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + "municipiosProvincia39Catalog.json";

	@Test
	public void testProvinciaCatalogRestServiceTest() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_PARAMS, EXPECTED_OK_JSON_FILE);
	}
	@Test
	public void testProvinciaCatalogRestServiceTestProvincia39() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_PARAMS + PETICION_MUNICIPIOS_POR_PROVINCIA, EXPECTED_OK_JSON_FILE_PROVINCIA_39);
	}	

}
