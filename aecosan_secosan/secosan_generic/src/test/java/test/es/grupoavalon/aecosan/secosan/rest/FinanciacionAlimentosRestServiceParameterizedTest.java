package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericParameterizedRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class FinanciacionAlimentosRestServiceParameterizedTest extends GenericParameterizedRestTest {

	@Parameters(name = "{index}: operationName:{0}")
	public static Collection<Object[]> generateData() {
		return Arrays.asList(new Object[][] { { REST_SERVICE_INC_OFER_ALIM_UME }, { REST_SERVICE_ALTER_OFER_ALIM_UME } });
	}

	private String operationName;
	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "financiacionAlimentos/";
	private static final String REST_SERVICE_INC_OFER_ALIM_UME = "incOferAlimUME/";
	private static final String REST_SERVICE_ALTER_OFER_ALIM_UME = "alterOferAlimUME/";
	private static final String REST_SERVICE_USER = "1/";

	private String sendedJsonFileAddIncOferAlimUMEOk;
	private String sendedJsonFileAddIncOferAlimUMEPayByAccountOk;
	private String sendedJsonFileAddIncOferAlimUMEPayByAccountOnlineOk;
	private String sendedJsonFileAddIncOferAlimUMEPayByCreditCardOk;
	private String sendedJsonFileUpdateIncOferAlimUMEOk;
	private String sendedJsonFileIncOferAlimUMEKoPresentationNumberToBig;

	public FinanciacionAlimentosRestServiceParameterizedTest(String operationName) {
		this.operationName = operationName;

		sendedJsonFileAddIncOferAlimUMEOk = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEAddOk.json";
		sendedJsonFileAddIncOferAlimUMEPayByAccountOk = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEAddPayByAccountOk.json";
		sendedJsonFileAddIncOferAlimUMEPayByAccountOnlineOk = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEAddPayByCreditCardOk.json";
		sendedJsonFileAddIncOferAlimUMEPayByCreditCardOk = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEAddPayByCreditCardOk.json";
		sendedJsonFileUpdateIncOferAlimUMEOk = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEUpdateOk.json";
		sendedJsonFileIncOferAlimUMEKoPresentationNumberToBig = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + operationName + "oferAlimUMEAddKoPresentationSizeToBig.json";
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestAddOferAlimUMEOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileAddIncOferAlimUMEOk);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestAddOferAlimUMEPayByAccountOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileAddIncOferAlimUMEPayByAccountOk);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestAddOferAlimUMEPayByAccountOnlineOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileAddIncOferAlimUMEPayByAccountOnlineOk);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestAddOferAlimUMEPayByCreditCardOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileAddIncOferAlimUMEPayByCreditCardOk);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestAddOferAlimUMEKoPresentationSizeToBig() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileIncOferAlimUMEKoPresentationNumberToBig);
		UtilRestTestClass.assertReponse500(response);
	}

	@Test
	public void testFinanciacionAlimentosRestServiceTestUpdateOferAlimUMEOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + operationName + REST_SERVICE_USER, sendedJsonFileUpdateIncOferAlimUMEOk);
		UtilRestTestClass.assertReponseOk(response);
	}

}
