package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

public class SolicitudeRestServiceTest extends GenericRestTest {

	private static final String REST_SERVICE_PATH = "solicitud/";
	private static final String ADMIN = "admin/";
	private static final String SECURE_PATH = "secure/";
	private static final String REST_SERVICE_USER = "1/";
	private static final String SOLICITUDE_FIND_BY_ID_NOT_EXIST = "800056";

	private static final String COMPLEMENTOS_ALIMENTICIOS = "complementosAlimenticios/";
	private static final String ALIMENTOS_GRUPOS = "alimentosGrupos/";
	private static final String CESE_COMERCIALIZACION_CA_FIND_BY_ID = "1001";
	private static final String CESE_COMERCIALIZACION_AG_FIND_BY_ID = "1002";
	private static final String EXPECTED_JSON_FILE_CESE_SOLICITUD_CA_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + COMPLEMENTOS_ALIMENTICIOS
			+ "ceseComercializacionFindById.json";
	private static final String EXPECTED_JSON_FILE_CESE_SOLICITUD_AG_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + ALIMENTOS_GRUPOS + "ceseComercializacionFindById.json";
	private static final String PUESTA_MERCADO_AG_FIND_BY_ID = "2006";
	private static final String PUESTA_MERCADO_CA_FIND_BY_ID = "2007";
	private static final String EXPECTED_JSON_FILE_PUESTA_MERCADO_CA_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + COMPLEMENTOS_ALIMENTICIOS + "puestaMercadoFindById.json";
	private static final String EXPECTED_JSON_FILE_PUESTA_MERCADO_AG_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + ALIMENTOS_GRUPOS + "puestaMercadoFindById.json";
	private static final String MODIFICACION_DATOS_AG_FIND_BY_ID = "3012";
	private static final String MODIFICACION_DATOS_CA_FIND_BY_ID = "3013";
	private static final String EXPECTED_JSON_FILE_MODIFICACION_DATOS_CA_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + COMPLEMENTOS_ALIMENTICIOS
			+ "modificacionDatosFindById.json";
	private static final String EXPECTED_JSON_FILE_MODIFICACION_DATOS_AG_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + ALIMENTOS_GRUPOS + "modificacionDatosFindById.json";

	private static final String QUEJA_SUGERENCIA = "quejaSugerencia/";
	private static final String EXPECTED_JSON_FILE_QUEJA_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + QUEJA_SUGERENCIA + "solicitudQuejaOkFindByID.json";
	private static final String QUEJA_FIND_BY_ID = "3014";
	private static final String EXPECTED_JSON_FILE_SUGERENCIA_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + QUEJA_SUGERENCIA + "solicitudSugerenciaOkFindByID.json";
	private static final String SUGERENCIA_FIND_BY_ID = "3015";

	private static final String LOGO = "logo/";
	private static final String EXPECTED_JSON_FILE_LOGO_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + LOGO + "solicitudLogoOkFindByID.json";
	private static final String LOGO_FIND_BY_ID = "3016";

	private static final String EVALUACION_RIESGOS = "evaluacionRiesgos/";
	private static final String EXPECTED_JSON_FILE_EVALUACION_RIESGOS_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + EVALUACION_RIESGOS + "evaluacionRiesgosByID.json";
	private static final String EVALUACION_RIESGOS_FIND_BY_ID = "3017";

	private static final String LMR_FITOSANITARIOS = "lmrFitosanitarios/";
	private static final String EXPECTED_JSON_FILE_LMR_FITOSANITARIOS_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + LMR_FITOSANITARIOS
			+ "solicitudLmrFitosanitariosOkFindByID.json";
	private static final String LMR_FITOSANITARIOS_FIND_BY_ID = "2011";

	private static final String FINANCIACION_ALIMENTOS = "financiacionAlimentos/";
	private static final String INC_OFER_ALIM_UME_FIND_BY_ID = "2008";
	private static final String EXPECTED_JSON_FILE_INC_OFER_ALIM_UME_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + FINANCIACION_ALIMENTOS
			+ "incOferAlimUME.json";

	private static final String ALTER_OFER_ALIM_UME_FIND_BY_ID = "1004";
	private static final String EXPECTED_JSON_FILE_ALTER_OFER_ALIM_UME_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + FINANCIACION_ALIMENTOS + "alterOferAlimUME.json";

	private static final String BAJA_OFER_ALIM_UME_FIND_BY_ID = "1005";
	private static final String EXPECTED_JSON_FILE_BAJA_OFER_ALIM_UME_FIND_BY_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + FINANCIACION_ALIMENTOS + "bajaOferAlimUME.json";

	private static final String SENDED_JSON_UPDATE_STATUS_OK = UtilRestTestClass.SENDED_JSON_FOLDER + "solicitudeUpdateStatus.json";

	// INIT--> Test Alimentos Grupos

	@Test
	public void testSolicitudeRestServiceTestFindCeseComercializacionAGByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + CESE_COMERCIALIZACION_AG_FIND_BY_ID, EXPECTED_JSON_FILE_CESE_SOLICITUD_AG_FIND_BY_ID,ignoredFields);
	}

	@Test
	public void testSolicitudeRestServiceTestFindPuestaMercadoAGByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + PUESTA_MERCADO_AG_FIND_BY_ID, EXPECTED_JSON_FILE_PUESTA_MERCADO_AG_FIND_BY_ID,ignoredFields);
	}

	@Test
	public void testSolicitudeRestServiceTestFindModificacionDatosAGByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + MODIFICACION_DATOS_AG_FIND_BY_ID, EXPECTED_JSON_FILE_MODIFICACION_DATOS_AG_FIND_BY_ID,ignoredFields);
	}

	@Test
	public void testSolicitudeRestServiceTestDeleteCeseComercializacionAGByIdOk() throws JsonProcessingException, IOException {
		UtilRestTestClass.invokeRestServiceDelete(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + CESE_COMERCIALIZACION_AG_FIND_BY_ID);
	}

	// END--> Test Alimentos Grupos

	// INIT--> Test Complementos Alimenticios

	@Test
	public void testSolicitudeRestServiceTestFindCeseComercializacionCAByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + CESE_COMERCIALIZACION_CA_FIND_BY_ID, EXPECTED_JSON_FILE_CESE_SOLICITUD_CA_FIND_BY_ID,ignoredFields);
	}

	@Test
	public void testSolicitudeRestServiceTestFindPuestaMercadoCAByIdOk() throws JsonProcessingException, IOException {
//		UtilRestTestClass.executeRestOKGetTest(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + PUESTA_MERCADO_CA_FIND_BY_ID, EXPECTED_JSON_FILE_PUESTA_MERCADO_CA_FIND_BY_ID);
		UtilRestTestClass.executeRestOKGetTest(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + PUESTA_MERCADO_CA_FIND_BY_ID);
	}

	@Test
	public void testSolicitudeRestServiceTestFindModificacionDatosCAByIdOk() throws JsonProcessingException, IOException {
		String[] ignoredFields ={"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + MODIFICACION_DATOS_CA_FIND_BY_ID, EXPECTED_JSON_FILE_MODIFICACION_DATOS_CA_FIND_BY_ID,ignoredFields);
	}

	// END--> Test Complementos Alimenticios

	// INIT--> Test Queja Sugerencia

	@Test
	public void testSolicitudeRestServiceTestFindQuejaByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion","dateTimeIncidence"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + QUEJA_FIND_BY_ID, EXPECTED_JSON_FILE_QUEJA_FIND_BY_ID,ignoredFields);
	}

	@Test
	public void testSolicitudeRestServiceTestFindSugerenciaByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion","dateTimeIncidence"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + SUGERENCIA_FIND_BY_ID, EXPECTED_JSON_FILE_SUGERENCIA_FIND_BY_ID,ignoredFields);
	}

	// END--> Test Queja Sugerencia

	// INIT--> Test Solicitud Logo

	@Test
	public void testSolicitudeRestServiceTestFindLogoByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + LOGO_FIND_BY_ID, EXPECTED_JSON_FILE_LOGO_FIND_BY_ID,ignoredFields);
	}

	// END--> Test Solicitud Logo

	// INIT--> Test EvaluacionRiesgos

	@Test
	public void testSolicitudeRestServiceTestFindEvaluacionRiesgosByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion","datos_pago"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + EVALUACION_RIESGOS_FIND_BY_ID, EXPECTED_JSON_FILE_EVALUACION_RIESGOS_FIND_BY_ID,ignoredFields);
	}

	// END--> Test EvaluacionRiesgos

	// INIT--> Test Lmr Fitosanitarios

	@Test
	public void testSolicitudeRestServiceTestFindLmrFitosanitariosByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + LMR_FITOSANITARIOS_FIND_BY_ID, EXPECTED_JSON_FILE_LMR_FITOSANITARIOS_FIND_BY_ID,ignoredFields);
	}

	// END--> Test Lmr Fitosanitarios

	// INIT--> Test IncOferAlimUMEEntity

	@Test
	public void testSolicitudeRestServiceTestFindIncOferAlimUMEEntityByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + INC_OFER_ALIM_UME_FIND_BY_ID, EXPECTED_JSON_FILE_INC_OFER_ALIM_UME_FIND_BY_ID,ignoredFields);

	}

	// END--> Test IncOferAlimUMEEntity

	// INIT--> Test AlterOferAlimUMEEntity

	@Test
	public void testSolicitudeRestServiceTestFindAlterOferAlimUMEEntityByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + ALTER_OFER_ALIM_UME_FIND_BY_ID, EXPECTED_JSON_FILE_ALTER_OFER_ALIM_UME_FIND_BY_ID,ignoredFields);
	}

	// END--> Test AlterOferAlimUMEEntity

	// INIT--> Test BajaOferAlimUMEEntity

	@Test
	public void testSolicitudeRestServiceTestFindBajaOferAlimUMEEntityByIdOk() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion","communicationDate"};
		UtilRestTestClass.validateJsonResponse(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + BAJA_OFER_ALIM_UME_FIND_BY_ID, EXPECTED_JSON_FILE_BAJA_OFER_ALIM_UME_FIND_BY_ID,ignoredFields);
	}

	// END--> Test BajaOferAlimUMEEntity

	@Test
	public void testSolicitudeRestServiceTestFindSolicitudeByIdKoIdNotExist() throws JsonProcessingException, IOException {
		final String[] ignoredFields = {"fechaCreacion"};
		UtilRestTestClass.executeRestErrorGetTest(SECURE_PATH + REST_SERVICE_PATH + REST_SERVICE_USER + SOLICITUDE_FIND_BY_ID_NOT_EXIST);
	}


	@Test
	public void testSolicitudeRestServiceTestSolicitudeUpdateStatusBySolicitudeIdOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(ADMIN + SECURE_PATH + REST_SERVICE_PATH + CESE_COMERCIALIZACION_CA_FIND_BY_ID, SENDED_JSON_UPDATE_STATUS_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

}
