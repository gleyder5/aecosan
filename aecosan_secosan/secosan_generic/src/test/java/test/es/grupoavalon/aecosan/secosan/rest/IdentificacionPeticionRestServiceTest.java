package test.es.grupoavalon.aecosan.secosan.rest;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabeza
 *
 */
public class IdentificacionPeticionRestServiceTest extends GenericRestTest {

	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = "/nuevoIdentificacionPeticion";
	private static final String REST_SERVICE_PARAMS = "/6";
	private static final String REST_SERVICE_PARAMS_ERROR404 = "/6/FAKE";
	private static final String REST_SERVICE_PARAMS_ERROR500 = "/FAKE";

	private static final String DATE_FORMAT = "yyyyMMddHHmm";

	@Test
	public void testgetIdentificacionRestServiceTest() {
		Response response = UtilRestTestClass.invokeRestServiceGet(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS);
		UtilRestTestClass.assertReponseOk(response);
		String expectedJsonFile = generateMockId();
		UtilRestTestClass.compareJSONReponseMock(response, expectedJsonFile);
	}

	@Test
	public void testgetIdentificacionRestServiceTestERROR404() {
		UtilRestTestClass.executeRestClientNotFound(REST_SERVICE_PATH + REST_SERVICE_PARAMS_ERROR404);
	}

	@Test
	public void testgetIdentificacionRestServiceTestERROR500() {
		Response response = UtilRestTestClass.invokeRestServiceGet(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS_ERROR500);
		UtilRestTestClass.assertReponse500(response);
	}

	private String generateMockId() {
		String jsonId = "{\"identificationRequest\":\"" + sysDate();
		return jsonId;
	}

	private String sysDate() {
		Date sysDate = new Date(System.currentTimeMillis());
		return new SimpleDateFormat(DATE_FORMAT).format(sysDate);
	}

}
