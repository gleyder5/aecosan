/**
 * 
 */
package test.es.grupoavalon.aecosan.secosan.testutils.spring;

import javax.sql.DataSource;

import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.setup.TestDBSetup;

/**
 * @author ottoabreu
 *
 */
public abstract class GenericDBTestClass extends GenericSpringTestClass {

	@Autowired
	protected DataSource dataSource;



	@Before
	public void before() {
		 this.getLogger().debug(
		 "Loading DB data for test class:"
		 + this.getClass().getSimpleName());
		 TestDBSetup dbSetUp = TestDBSetup.getInstance(dataSource);
		 dbSetUp.setUpDB();

	}

}
