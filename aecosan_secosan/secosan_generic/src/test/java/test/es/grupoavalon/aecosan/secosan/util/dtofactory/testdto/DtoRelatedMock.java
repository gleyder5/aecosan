package test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class DtoRelatedMock {

	@BeanToBeanMapping(getValueFrom = "stringAtribute", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String stringAtribute;
	@BeanToBeanMapping(getValueFrom = "intValue", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String intValue;
	@BeanToBeanMapping(getValueFrom = "boolVal", useCustomTransformer = PrimitiveToStringTransformer.class)
	private String boolVal;
	@BeanToBeanMapping
	private String haveSameNameStringRelated;
	@BeanToBeanMapping
	private int haveSameNameIntRelated;

	public String getStringAtribute() {
		return stringAtribute;
	}

	public void setStringAtribute(String stringAtribute) {
		this.stringAtribute = stringAtribute;
	}

	public String getIntValue() {
		return intValue;
	}

	public void setIntValue(String intValue) {
		this.intValue = intValue;
	}

	public String getBoolVal() {
		return boolVal;
	}

	public void setBoolVal(String boolVal) {
		this.boolVal = boolVal;
	}

	@Override
	public String toString() {
		return "DtoRelatedMock [stringAtribute=" + stringAtribute
				+ ", intValue=" + intValue + ", boolVal=" + boolVal + "]";
	}

	public String getHaveSameNameStringRelated() {
		return haveSameNameStringRelated;
	}

	public void setHaveSameNameStringRelated(String haveSameNameStringRelated) {
		this.haveSameNameStringRelated = haveSameNameStringRelated;
	}

	public int getHaveSameNameIntRelated() {
		return haveSameNameIntRelated;
	}

	public void setHaveSameNameIntRelated(int haveSameNameIntRelated) {
		this.haveSameNameIntRelated = haveSameNameIntRelated;
	}
	

}
