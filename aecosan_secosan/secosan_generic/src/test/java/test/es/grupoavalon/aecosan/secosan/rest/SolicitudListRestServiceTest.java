package test.es.grupoavalon.aecosan.secosan.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runners.Parameterized.Parameters;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericParameterizedRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class SolicitudListRestServiceTest extends GenericParameterizedRestTest {

	@Parameters(name = "{index}: path:{0}")
	public static Collection<Object[]> generateData() {
		return Arrays.asList(new Object[][] { { REST_SERVICE_PATH }, { REST_SERVICE_PATH_TRAMITADOR } });
	}

	private String path;
	private static final String SECURE = "secure";
	private static final String ADMIN_SECURE = "admin/secure";
	private static final String REST_SERVICE_PATH = "/solicitudList";
	private static final String REST_SERVICE_PATH_TRAMITADOR =  "/solicitudList/tramitador";
	private static final String REST_SERVICE_PARAMS_USER_1 = "/2";
	private static final String REST_SERVICE_PARAMS_USER_5 = "/5";
	private static final String FAKE_URL = "/FAKE_URL";

	private static final String STRING_DATE_FROM = "01-May-2016";
	private static final String STRING_DATE_TO = "30-May-2018";
	private static final String STRING_DATE_FORMAT = "dd-MMM-yyyy";

	private static final String ALLFROMONEUSERDEFAULT_JSON_FILE = "/AllFromOneUserDefault.json";
	private static final String ALLFROMONEUSERPAGE1_JSON_FILE = "/AllFromOneUserPage1.json";
	private static final String ALLFROMONEUSERPAGE2_DESC_JSON_FILE = "/AllFromOneUserPage2Desc.json";
	private static final String ALLFROMONEUSERPAGE2_ASC_JSON_FILE = "/AllFromOneUserPage2Asc.json";
	private static final String ALLFROMONEUSERPAGE1ASC_JSON_FILE = "/AllFromOneUserPage1Asc.json";
	private static final String FILTER_BY_DATE_FROM_DESC_JSON_FILE = "/FilterByDateFromDesc.json";
	private static final String FILTER_BY_DATE_FROM_ASC_JSON_FILE = "/FilterByDateFromAsc.json";
	private static final String FILTER_BY_DATE_TO_DESC_JSON_FILE = "/FilterByDateToDesc.json";
	private static final String FILTER_BY_DATE_TO_ASC_JSON_FILE = "/FilterByDateToAsc.json";
	private static final String FILTER_BY_DATE_BETWEEN_DESC_JSON_FILE = "/FilterByDateBetweenDesc.json";
	private static final String FILTER_BY_DATE_BETWEEN_ASC_JSON_FILE = "/FilterByDateBetweenAsc.json";
	private static final String FILTER_BY_STATUS_DESC_JSON_FILE = "/FilterByStatusDesc.json";
	private static final String FILTER_BY_STATUS_ASC_JSON_FILE = "/FilterByStatusAsc.json";
	private static final String FILTER_BY_ID_JSON_FILE = "/FilterById.json";
	private static final String FILTER_BY_DATE_BETWEEN_AND_STATUS_DESC_JSON_FILE = "/FilterByDateBetweenAndStatusDesc.json";
	private static final String FILTER_BY_DATE_BETWEEN_AND_STATUS_ASC_JSON_FILE = "/FilterByDateBetweenAndStatusAsc.json";

	private static final String ORDER_ASC = "asc";
	private static final String ORDER_DES = "desc";

	public SolicitudListRestServiceTest(String path) {
		if (REST_SERVICE_PATH.equals(path)) {
			this.path = SECURE + path;
		} else {
			this.path = ADMIN_SECURE + path;
		}
	}

	@Test
	public void testListItemsAllFromOneUserDefault() {

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, ALLFROMONEUSERDEFAULT_JSON_FILE));
	}

	@Test
	public void testListItemsAllFromOneUserPage2Asc() {
		Map<String, String> queryparams = new HashMap<String, String>();
		queryparams.put("start", "5");
		queryparams.put("length", "5");
		queryparams.put("order", "asc");
		queryparams.put("columnorder", "id");

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, ALLFROMONEUSERPAGE2_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsAllFromOneUserPage2Desc() {
		Map<String, String> queryparams = new HashMap<String, String>();
		queryparams.put("start", "5");
		queryparams.put("length", "5");
		queryparams.put("order", "desc");
		queryparams.put("columnorder", "id");

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, ALLFROMONEUSERPAGE2_DESC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsAllFromOneUserPage1Asc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);
		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, ALLFROMONEUSERPAGE1ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsAllFromOneUserPage1Desc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, ALLFROMONEUSERPAGE1_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByStatusAsc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);
		queryparams.put("filter[0][estado_id]", "3");
		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_STATUS_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByStatusDesc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);
		queryparams.put("filter[0][estado_id]", "3");
		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_STATUS_DESC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterById() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);
		queryparams.put("filter[0][id]", "2009");

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_ID_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateFromAsc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);

		String dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_FROM_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateFromDesc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);

		String dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_FROM_DESC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateToAsc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);

		String dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_TO_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateToDesc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);

		String dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_TO_DESC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateBetweenAsc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);

		String dateLongString;

		dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		dateLongString = getDate(STRING_DATE_TO);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_BETWEEN_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateBetweenDesc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);

		String dateLongString;

		dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		dateLongString = getDate(STRING_DATE_TO);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_BETWEEN_DESC_JSON_FILE), queryparams);

	}

	@Test
	public void testListItemsFilterByDateBetweenAndStatusAsc() {
		Map<String, String> queryparams = initGenericParams(ORDER_ASC);

		String dateLongString;

		dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		dateLongString = getDate(STRING_DATE_TO);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		queryparams.put("filter[0][estado_id]", "3");

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_BETWEEN_AND_STATUS_ASC_JSON_FILE), queryparams);
	}

	@Test
	public void testListItemsFilterByDateBetweenAndStatusDesc() {
		Map<String, String> queryparams = initGenericParams(ORDER_DES);

		String dateLongString;

		dateLongString = getDate(STRING_DATE_FROM);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionFrom]", dateLongString);
		}

		dateLongString = getDate(STRING_DATE_TO);
		if (dateLongString != null) {
			queryparams.put("filter[0][fechaCreacionTo]", dateLongString);
		}

		queryparams.put("filter[0][estado_id]", "3");

		UtilRestTestClass.executeRestOKGetTest(path + getUser(path), getJsonPath(path, FILTER_BY_DATE_BETWEEN_AND_STATUS_DESC_JSON_FILE), queryparams);
	}

	private String getDate(String strDate) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(STRING_DATE_FORMAT);
		Date date;
		String dateLongString = null;
		try {
			date = simpleDateFormat.parse(strDate);
			long milliseconds = date.getTime();
			dateLongString = String.valueOf(milliseconds);

		} catch (ParseException e) {
			logger.error(e.toString());
		}
		return dateLongString;
	}

	private Map<String, String> initGenericParams(String order) {
		Map<String, String> queryparams = new HashMap<String, String>();
		queryparams.put("start", "0");
		queryparams.put("length", "5");
		queryparams.put("order", order);
		queryparams.put("columnorder", "id");
		return queryparams;
	}

	@Test
	public void testSolicitudList404Error() {
		if (path.equals(SECURE + REST_SERVICE_PATH)) {
			UtilRestTestClass.executeRestClientNotFound(path);
		} else {
			UtilRestTestClass.executeRestClientNotFound(path + getUser(path) + FAKE_URL);
		}
	}

	private static String getUser(String path) {
		if (path.equals(SECURE + REST_SERVICE_PATH)) {
			return REST_SERVICE_PARAMS_USER_1;
		} else {
			return REST_SERVICE_PARAMS_USER_5;
		}
	}

	private static String getJsonPath(String path, String jsonName) {
		String jsonPath = null;
		if (path.equals(SECURE + REST_SERVICE_PATH)) {
			jsonPath = "expectedJSON/" + path.replace("secure/", "") + jsonName;
		} else {
			jsonPath = "expectedJSON/" + path.replace("admin/secure/", "") + jsonName;
		}
		return jsonPath;

	}
}
