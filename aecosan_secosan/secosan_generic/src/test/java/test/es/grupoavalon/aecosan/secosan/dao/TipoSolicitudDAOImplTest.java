package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.TipoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class TipoSolicitudDAOImplTest extends GenericDBTestClass {

	@Autowired
	private TipoSolicitudDAO tipoSolicitudDAO;

	private static final Long FORMULARIO_ID_1 = 1l;
	private static final Long FORMULARIO_ID_NOT_EXIST = 555552l;
	private static final String FORMULARIO1_DESCRIPCION = "Comunicación Queja";

	@Test
	public void testFormularioFindByIdOK() {
		TipoSolicitudEntity formulario = tipoSolicitudDAO.findFormulario(FORMULARIO_ID_1);
		assertEquals(formulario.getCatalogValue(), FORMULARIO1_DESCRIPCION);
	}

	@Test
	public void testFormularioFindByIdNotExist() {
		TipoSolicitudEntity formulario = tipoSolicitudDAO.findFormulario(FORMULARIO_ID_NOT_EXIST);
		assertNull(formulario);
	}

	@Test(expected = DaoException.class)
	public void testFormularioFindByIdNull() {
		tipoSolicitudDAO.findFormulario(null);
	}

}
