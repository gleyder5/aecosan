package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.ServicioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.TipoPagoServicioEntity;

public class EvaluacionRiesgosMock {

	public static EvaluacionRiesgosEntity generateEvaluacionRiesgosEntity() {
		EvaluacionRiesgosEntity evaluacionRiesgosEntity = new EvaluacionRiesgosEntity();
		prepareMockEvaluacionRiesgos(evaluacionRiesgosEntity);
		return evaluacionRiesgosEntity;
	}


	public static void prepareMockEvaluacionRiesgos(EvaluacionRiesgosEntity evaluacionRiesgosEntity) {
		SolicitudMock.prepareMocksolicitudEntity(evaluacionRiesgosEntity);
		evaluacionRiesgosEntity.setCommercialProductName("commercialProductName");
		evaluacionRiesgosEntity.setSolicitudeSubject("SolicitudeSubject");
		evaluacionRiesgosEntity.setPayTypeService(generateMockTypeService());
	}


	private static TipoPagoServicioEntity generateMockTypeService() {

		TipoPagoServicioEntity tipoPagoServicioEntity = new TipoPagoServicioEntity();
		tipoPagoServicioEntity.setPayType(generateMockPayType());
		tipoPagoServicioEntity.setService(generateMockService());
		return tipoPagoServicioEntity;
	}

	private static TipoPagoEntity generateMockPayType() {
		final Long PAY_TYPE_ID = 1L;
		TipoPagoEntity tipoPagoEntity = new TipoPagoEntity();
		tipoPagoEntity.setId(PAY_TYPE_ID);
		return tipoPagoEntity;

	}

	private static ServicioEntity generateMockService() {
		final Long SERVICE_ID = 1L;
		ServicioEntity servicioEntity = new ServicioEntity();
		servicioEntity.setId(SERVICE_ID);
		return servicioEntity;
	}

}
