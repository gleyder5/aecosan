package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.AlimentosGruposMock;
import es.grupoavalon.aecosan.secosan.dao.AlimentosGruposDAO;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

/**
 * 
 * @author xi314616
 *
 */
public class AlimentosGruposDAOImplTest extends GenericDBTestClass {

	@Autowired
	private AlimentosGruposDAO alimentosGruposDAO;

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	private static final Long ID_FIRST_CESECOMERCIALIZACION_ENTITY = 1002L;
	private static final Long ID_FIRST_PUESTA_MERCADO_ENTITY = 2006L;
	private static final Long ID_FIRST_MODIFICACION_DATOS_ENTITY = 3012L;
	private static final Long MODIFICACION_DATOS_TIPO_FORMULARIO_ID = 5L;
	private static final Long CESECOMERCIALIZACION_TIPO_FORMULARIO_ID = 6L;
	private static final Long PUESTA_MERCADO_TIPO_FORMULARIO_ID = 4L;

	private static final Long USER_CREATOR = 1L;

	private CeseComercializacionAGEntity ceseComercializacionEntity;
	private PuestaMercadoAGEntity puestaMercadoEntity;
	private ModificacionDatosAGEntity modificacionDatosEntity;


	// --> INIT CeseComercializacionAGEntity <--

	/**
	 * Try to add new CeseComercializacionAGEntity
	 * 
	 */
	@Test
	public void testAddCeseComercializacion() {
		ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setUserCreator(generateUserCreatorMock());
		ceseComercializacionEntity.getFormularioEspecifico().setId(CESECOMERCIALIZACION_TIPO_FORMULARIO_ID);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
		CeseComercializacionAGEntity ceseComercializacionEntityFind = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ceseComercializacionEntity.getId());
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind, ceseComercializacionEntity);

	}


	/**
	 * Try to update a CeseComercializacionAGEntity
	 * 
	 */
	@Test
	public void testUpdateCeseComercializacion() {
		CeseComercializacionAGEntity ceseComercializacionEntityUpdate = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_ENTITY);
		ceseComercializacionEntityUpdate.setNombreComercialProducto(AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);
		alimentosGruposDAO.updateCeseComercializacion(ceseComercializacionEntityUpdate);

		CeseComercializacionAGEntity ceseComercializacionEntityFind = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ceseComercializacionEntityUpdate.getId());
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getNombreComercialProducto(), AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when the object is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionNull() {
		alimentosGruposDAO.addCeseComercializacion(null);
	}

	// --> INIT Required fields from SolicitudEntity <--

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field
	 * identificadorPeticion is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionIdentificadorPeticionNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setIdentificadorPeticion(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field
	 * FechaCreacion is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionFechaNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setFechaCreacion(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field
	 * formulario is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionFormularioNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setFormulario(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field
	 * Solicitante is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionSolicitanteNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setSolicitante(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field
	 * status is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionStatusNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setStatus(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionAGEntity when The required field Area
	 * is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionAreaNull() {
		CeseComercializacionAGEntity ceseComercializacionEntity = AlimentosGruposMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setArea(null);
		alimentosGruposDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	// --> END Required fields from SolicitudEntity <--

	// --> END Cesecomercialización <--

	// --> INIT PuestaMercadoAGEntity <--

	/**
	 * Try to add new PuestaMercadoAGEntity
	 * 
	 */
	@Test
	public void testAddPuestaMercado() {
		puestaMercadoEntity = AlimentosGruposMock.generatePuestaMercadoEntity();
		puestaMercadoEntity.setUserCreator(generateUserCreatorMock());
		puestaMercadoEntity.getFormularioEspecifico().setId(PUESTA_MERCADO_TIPO_FORMULARIO_ID);
		alimentosGruposDAO.addPuestaMercado(puestaMercadoEntity);

		PuestaMercadoAGEntity puestaMercadoEntityFind = (PuestaMercadoAGEntity) solicitudDAO.findSolicitudById(puestaMercadoEntity.getId());

		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind, puestaMercadoEntity);

	}

	/**
	 * Try to update a PuestaMercadoAGEntity
	 * 
	 */
	@Test
	public void testUpdatePuestaMercado() {
		PuestaMercadoAGEntity puestaMercadoEntityUpdate = (PuestaMercadoAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_PUESTA_MERCADO_ENTITY);
		puestaMercadoEntityUpdate.setNombreComercialProducto(AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);
		alimentosGruposDAO.updatePuestaMercado(puestaMercadoEntityUpdate);

		PuestaMercadoAGEntity puestaMercadoEntityFind = (PuestaMercadoAGEntity) solicitudDAO.findSolicitudById(puestaMercadoEntityUpdate.getId());

		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind.getNombreComercialProducto(), AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);

	}

	// --> END PuestaMercadoAGEntity <--

	// --> INIT PuestaMercadoAGEntity <--

	/**
	 * Try to add new PuestaMercadoAGEntity
	 * 
	 */
	@Test
	public void testAddModificacionDatosOk() {
		modificacionDatosEntity = AlimentosGruposMock.generateModificacionDatosEntity();
		modificacionDatosEntity.setUserCreator(generateUserCreatorMock());
		modificacionDatosEntity.getFormularioEspecifico().setId(MODIFICACION_DATOS_TIPO_FORMULARIO_ID);
		alimentosGruposDAO.addModificacionDatos(modificacionDatosEntity);
		ModificacionDatosAGEntity modificacionDatosEntityFind = (ModificacionDatosAGEntity) solicitudDAO.findSolicitudById(modificacionDatosEntity.getId());
		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind, modificacionDatosEntity);
	}

	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioDAO.findCreatorById(USER_CREATOR);
	}


	/**
	 * Try to update a PuestaMercadoAGEntity
	 * 
	 */
	@Test
	public void testUpdateModificacionDatos() {
		ModificacionDatosAGEntity modificacionDatos = (ModificacionDatosAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_MODIFICACION_DATOS_ENTITY);
		modificacionDatos.setNombreComercialProducto(AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);
		alimentosGruposDAO.updateModificacionDatos(modificacionDatos);

		ModificacionDatosAGEntity modificacionDatosEntityFind = (ModificacionDatosAGEntity) solicitudDAO.findSolicitudById(modificacionDatos.getId());

		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind.getNombreComercialProducto(), AlimentosGruposMock.TEST_NOMBRE_PRODUCTO);

	}

	// --> END PuestaMercadoAGEntity <--


}
