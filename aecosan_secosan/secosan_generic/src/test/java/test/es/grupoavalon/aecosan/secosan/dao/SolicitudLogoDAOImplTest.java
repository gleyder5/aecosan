package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudLogoMock;
import es.grupoavalon.aecosan.secosan.dao.SolicitudLogoDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;

/**
 * 
 * @author xi314616
 *
 */
public class SolicitudLogoDAOImplTest extends GenericDBTestClass {

	@Autowired
	private SolicitudLogoDAO solicitudLogoDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	private SolicitudLogoEntity solicitudLogoEntity;

	private static final Long ID_FIRST_SOLICITUD_LOGO_ENTITY = 3016L;
	private static final Long USER_CREATOR_ID = 1L;
	private static final Long LOGO_TIPO_FORMULARIO = 10L;
	private static final String FIRST_SOLICITUD_LOGO_SET_PURPOSE_USE = "Update for purpose and use";

	// --> INIT solicitudLogo <--
	@Test
	public void testAddSolicitudLogo() {
		solicitudLogoEntity = SolicitudLogoMock.generateSolicitudLogoEntity();
		solicitudLogoEntity.setUserCreator(generateUserCreator());
		solicitudLogoEntity.getFormularioEspecifico().setId(LOGO_TIPO_FORMULARIO);
		solicitudLogoDAO.addSolicitudLogo(solicitudLogoEntity);

		SolicitudLogoEntity solicitudLogoEntityFind = solicitudLogoDAO.findSolicitudLogoById(solicitudLogoEntity.getId());

		assertNotNull(solicitudLogoEntityFind);
		assertEquals(solicitudLogoEntityFind, solicitudLogoEntity);

	}

	private UsuarioCreadorEntity generateUserCreator() {
		return usuarioDAO.findCreatorById(USER_CREATOR_ID);
	}

	@Test
	public void testUpdateSolicitudLogoById() {
		solicitudLogoEntity = solicitudLogoDAO.findSolicitudLogoById(ID_FIRST_SOLICITUD_LOGO_ENTITY);
		solicitudLogoEntity.setPurposeAndUse(FIRST_SOLICITUD_LOGO_SET_PURPOSE_USE);
		solicitudLogoDAO.updateSolicitudLogo(solicitudLogoEntity);
		solicitudLogoEntity = solicitudLogoDAO.findSolicitudLogoById(ID_FIRST_SOLICITUD_LOGO_ENTITY);
		assertNotNull(solicitudLogoEntity);
		assertEquals(solicitudLogoEntity.getPurposeAndUse(), FIRST_SOLICITUD_LOGO_SET_PURPOSE_USE);
	}



}
