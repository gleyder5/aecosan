package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLogoEntity;

public class SolicitudLogoMock {

	private static final String TEST_PURPOSE_USE = "test purpose and use";
	private static final String TEST_REQUESTED_MATERIAL = "test requested material";


	public static SolicitudLogoEntity generateSolicitudLogoEntity() {
		SolicitudLogoEntity solicitudLogoEntity = new SolicitudLogoEntity();
		prepareMockSolicitudLogo(solicitudLogoEntity);
		return solicitudLogoEntity;
	}

	private static void prepareMockSolicitudLogo(SolicitudLogoEntity solicitudLogoEntity) {
		SolicitudMock.prepareMocksolicitudEntity(solicitudLogoEntity);
		solicitudLogoEntity.setPurposeAndUse(TEST_PURPOSE_USE);
		solicitudLogoEntity.setRequestedMaterial(TEST_REQUESTED_MATERIAL);
	}

}
