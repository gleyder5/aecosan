package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

/**
 * 
 * @author xi314616
 *
 */
public class SolicitudDAOImplTest extends GenericDBTestClass {
	@Autowired
	private SolicitudDAO solicitudDAO;


	private static final String ORDER_DESC = "desc";
	private static final String ORDER_BY_FECHA_CREACION = "fechaCreacion";
	private static final Long EXPECTED_ID_ORDER_BY_FECHA_DESC = 3016l;
	private static final int EXPECTED_SUBLIST = 5;
	private static final int EXPECTED_LIST_SIZE = 19;
	private static final int FIRST = 0;
	private static final int LAST = 5;

	private static final Long ID_FIRST_CESECOMERCIALIZACION_CA_ENTITY = 1001L;
	private static final Long ID_FIRST_CESECOMERCIALIZACION_AG_ENTITY = 1002L;
	private static final Long ID_FIRST_PUESTA_MERCADO_CA_ENTITY = 2007L;
	private static final Long ID_FIRST_PUESTA_MERCADO_AG_ENTITY = 2006L;
	private static final Long ID_FIRST_MODIFICACION_CA_DATOS_ENTITY = 3013L;
	private static final Long ID_FIRST_MODIFICACION_AG_DATOS_ENTITY = 3012L;
	private static final String CESE_COMERCIALIZACION_CA_FIND_EXPECTED_PRODUCT_NAME = "nombre del producto 1";
	private static final String CESE_COMERCIALIZACION_AG_FIND_EXPECTED_PRODUCT_NAME = "nombre del producto 2";
	private static final String PUESTA_MERCADO_CA_FIND_EXPECTED_PRODUCT_NAME = "Nombre Puesta Mercado CA";
	private static final String PUESTA_MERCADO_AG_FIND_EXPECTED_PRODUCT_NAME = "Nombre Puesta Mercado AG";
	private static final String MODIFICACION_DATOS_CA_FIND_EXPECTED_PRODUCT_NAME = "Nombre obligatorio";

	@Test
	public void testSolicitudListByUserAndPaginationParams() {
		PaginationParams paginationParams = generatePaginationParams();

		List<SolicitudEntity> lista = solicitudDAO.getSolicList(paginationParams);

		assertTrue(lista.size() == EXPECTED_SUBLIST);
		assertTrue(lista.get(FIRST).getId().equals(EXPECTED_ID_ORDER_BY_FECHA_DESC));

	}

	@Test
	public void testSolicitudListByUserAndFilter() {
		int length = solicitudDAO.getFilteredListRecords(generatePaginationFilters());

		assertTrue(length == EXPECTED_SUBLIST);

	}

	@Test
	@Ignore
	public void testSolicitudTotalRecordsByUser() {
		// int length = solicitudDAO.getTotalListRecords(USER);

		// assertTrue(length == EXPECTED_LIST_SIZE);
	}

	private PaginationParams generatePaginationParams() {
		PaginationParams paginationParams = new PaginationParams();
		List<FilterParams> filters = new ArrayList<FilterParams>();

		paginationParams.setOrder(ORDER_DESC);
		paginationParams.setInterval(FIRST, LAST);
		paginationParams.setSortField(ORDER_BY_FECHA_CREACION);
		paginationParams.setFilters(filters);

		return paginationParams;
	}

	private List<FilterParams> generatePaginationFilters() {
		List<FilterParams> filters = new ArrayList<FilterParams>();

		FilterParams filter1 = new FilterParams();
		filter1.setFilterName("estado_id");
		filter1.setFilterValue("3");

		filters.add(filter1);

		return filters;
	}

	@Test
	public void testFindCeseComercializacionAGByID() {
		CeseComercializacionAGEntity ceseComercializacionEntityFind = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_AG_ENTITY);
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getNombreComercialProducto(), CESE_COMERCIALIZACION_AG_FIND_EXPECTED_PRODUCT_NAME);
	}


	@Test
	public void testFindPuestaMercadoAGByID() {
		PuestaMercadoAGEntity puestaMercadoEntityFind = (PuestaMercadoAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_PUESTA_MERCADO_AG_ENTITY);
		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind.getNombreComercialProducto(), PUESTA_MERCADO_AG_FIND_EXPECTED_PRODUCT_NAME);
	}

	@Test
	public void testFindModificacionDatosAGByID() {
		ModificacionDatosAGEntity modificacionDatosEntityFind = (ModificacionDatosAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_MODIFICACION_AG_DATOS_ENTITY);
		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind.getNombreComercialProducto(), MODIFICACION_DATOS_CA_FIND_EXPECTED_PRODUCT_NAME);
	}

	/**
	 * Try to find a CeseComercializacionEntity
	 * 
	 */
	@Test
	public void testFindCeseComercializacionCAByID() {
		CeseComercializacionCAEntity ceseComercializacionEntityFind = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_CA_ENTITY);

		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getNombreComercialProducto(), CESE_COMERCIALIZACION_CA_FIND_EXPECTED_PRODUCT_NAME);
	}

	/**
	 * Try to find a PuestaMercadoEntity
	 * 
	 */
	@Test
	public void testFindPuestaMercadoCAByID() {
		PuestaMercadoCAEntity puestaMercadoEntityFind = (PuestaMercadoCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_PUESTA_MERCADO_CA_ENTITY);

		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind.getNombreComercialProducto(), PUESTA_MERCADO_CA_FIND_EXPECTED_PRODUCT_NAME);
	}

	/**
	 * Try to find a ModificacionDatosEntity
	 * 
	 */
	@Test
	public void testFindModificacionDatosCAByID() {
		ModificacionDatosCAEntity modificacionDatosEntityFind = (ModificacionDatosCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_MODIFICACION_CA_DATOS_ENTITY);
		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind.getNombreComercialProducto(), MODIFICACION_DATOS_CA_FIND_EXPECTED_PRODUCT_NAME);
	}

	@Test
	public void testDeleteCeseComercializacionAGByID() {
		CeseComercializacionAGEntity ceseComercializacionEntityFind = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_AG_ENTITY);
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getNombreComercialProducto(), CESE_COMERCIALIZACION_AG_FIND_EXPECTED_PRODUCT_NAME);
		solicitudDAO.deleteSolicitudById(String.valueOf(ID_FIRST_CESECOMERCIALIZACION_AG_ENTITY));
		ceseComercializacionEntityFind = (CeseComercializacionAGEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_AG_ENTITY);
		assertNull(ceseComercializacionEntityFind);
	}

}
