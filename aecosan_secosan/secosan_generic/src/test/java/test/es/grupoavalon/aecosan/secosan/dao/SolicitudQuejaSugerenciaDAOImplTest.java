package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;
import es.grupoavalon.aecosan.secosan.dao.SolicitudQuejaSugerenciaDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;

/**
 * 
 * @author xi314616
 *
 */
public class SolicitudQuejaSugerenciaDAOImplTest extends GenericDBTestClass {

	@Autowired
	private SolicitudQuejaSugerenciaDAO solicitudQuejaSugerenciaDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	private static final Long USER_CREATOR = 1L;
	// QUEJA
	private static final Long ID_NEXT_SOLICITUD_QUEJA_ENTITY = 4021L;
	private static final Long ID_FIRST_SOLICITUD_QUEJA_ENTITY = 3014L;
	private static final Long QUEJA_TIPO_FORMULARIO_ID = 8L;
	private static final String FIRST_SOLICITUD_QUEJA_UNIDAD = "Unidad de pruebas";
	private static final String FIRST_SOLICITUD_QUEJA_UPDATE_UNIDAD = "Unidad de pruebas update";
	private static final String FIRST_SOLICITUD_QUEJA_SET_MOTIVE = "update for queja";

	// SUGERENCIA
	private static final Long ID_NEXT_SOLICITUD_SUGERENCIA_ENTITY = 4020L;
	private static final Long ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY = 3015L;
	private static final Long SUGERENCIA_TIPO_FORMULARIO_ID = 9L;
	private static final String FIRST_SOLICITUD_SUGERENCIA_UNIDAD = "Unidad de sugerencia";
	private static final String FIRST_SOLICITUD_SUGERENCIA_SET_MOTIVE = "Test: update";

	private SolicitudQuejaEntity solicitudQuejaEntity;
	private SolicitudSugerenciaEntity solicitudSugerenciaEntity;

	// --> INIT solicitudQuejaSugerencia <--

	/**
	 * Try to add new SolicitudQueja
	 * 
	 */
	@Test
	public void testAddSolicitudQueja() {
		solicitudQuejaEntity = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaEntity();
		SolicitudQuejaEntity solicitudQuejaEntityFind;
		solicitudQuejaEntity.setUserCreator(generateUserCreatorMock());
		solicitudQuejaEntity.getFormularioEspecifico().setId(QUEJA_TIPO_FORMULARIO_ID);
		SolicitudQuejaEntity solicitudQuejaEntitylast =  solicitudQuejaSugerenciaDAO.addSolicitudQueja(solicitudQuejaEntity);


		solicitudQuejaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_NEXT_SOLICITUD_QUEJA_ENTITY);

		assertNotNull(solicitudQuejaEntityFind);
		assertEquals(solicitudQuejaEntityFind, solicitudQuejaEntity);

	}

	/**
	 * Try to update a SolicitudQueja
	 * 
	 */
	@Test
	public void testUpdateSolicitudQuejaByIdMotiveUpdateOk() {
		solicitudQuejaEntity = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		solicitudQuejaEntity.setMotive(FIRST_SOLICITUD_QUEJA_SET_MOTIVE);
		solicitudQuejaSugerenciaDAO.updateSolicitudQueja(solicitudQuejaEntity);
		SolicitudQuejaEntity solicitudQuejaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		assertNotNull(solicitudQuejaEntityFind);
		assertEquals(solicitudQuejaEntityFind.getMotive(), FIRST_SOLICITUD_QUEJA_SET_MOTIVE);
	}

	/**
	 * Try to update a SolicitudQueja
	 * 
	 */
	@Test
	public void testUpdateSolicitudQuejaByIdUpdateDateOk() {

		Timestamp date = new Timestamp(SolicitudMock.sysDate().getTime());

		solicitudQuejaEntity = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		solicitudQuejaEntity.setDateTimeIncidence(date);
		solicitudQuejaSugerenciaDAO.updateSolicitudQueja(solicitudQuejaEntity);
		SolicitudQuejaEntity solicitudQuejaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		assertNotNull(solicitudQuejaEntityFind);
		assertEquals(solicitudQuejaEntityFind.getDateTimeIncidence(), date);
	}

	/**
	 * Try to update a SolicitudQueja
	 * 
	 */
	@Test
	public void testUpdateSolicitudQuejaById() {
		solicitudQuejaEntity = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		solicitudQuejaEntity.setUnidad(FIRST_SOLICITUD_QUEJA_UPDATE_UNIDAD);
		solicitudQuejaSugerenciaDAO.updateSolicitudQueja(solicitudQuejaEntity);
		SolicitudQuejaEntity solicitudQuejaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		assertNotNull(solicitudQuejaEntityFind);
		assertEquals(solicitudQuejaEntityFind.getUnidad(), FIRST_SOLICITUD_QUEJA_UPDATE_UNIDAD);
	}

	/**
	 * Try to Find a SolicitudQueja by Id
	 * 
	 */
	@Test
	public void testFindSolicitudQuejaById() {
		SolicitudQuejaEntity solicitudQuejaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudQuejaById(ID_FIRST_SOLICITUD_QUEJA_ENTITY);
		assertNotNull(solicitudQuejaEntityFind);
		assertEquals(solicitudQuejaEntityFind.getUnidad(), (FIRST_SOLICITUD_QUEJA_UNIDAD));

	}

	// --> END solicitudQueja <--

	/**
	 * Try to add new SolicitudSugerencia
	 * 
	 */
	@Test
	public void testAddSolicitudSugerencia() {
		solicitudSugerenciaEntity = SolicitudQuejaSugerenciaMock.generateSolicitudSugerenciaEntity();
		solicitudSugerenciaEntity.setUserCreator(generateUserCreatorMock());
		solicitudSugerenciaEntity.getFormularioEspecifico().setId(SUGERENCIA_TIPO_FORMULARIO_ID);
		solicitudQuejaSugerenciaDAO.addSolicitudSugerencia(solicitudSugerenciaEntity);

		SolicitudSugerenciaEntity solicitudSugerenciaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_NEXT_SOLICITUD_SUGERENCIA_ENTITY);

		assertNotNull(solicitudSugerenciaEntityFind);
		assertEquals(solicitudSugerenciaEntityFind, solicitudSugerenciaEntity);

	}

	/**
	 * Try to update a SolicitudSugerencia
	 * 
	 */
	@Test
	public void testUpdateSolicitudSugerenciaById() {
		solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		solicitudSugerenciaEntity.setMotive(FIRST_SOLICITUD_SUGERENCIA_SET_MOTIVE);
		solicitudQuejaSugerenciaDAO.updateSolicitudSugerencia(solicitudSugerenciaEntity);
		solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		assertNotNull(solicitudSugerenciaEntity);
		assertEquals(solicitudSugerenciaEntity.getMotive(), FIRST_SOLICITUD_SUGERENCIA_SET_MOTIVE);
	}

	/**
	 * Try to Find a SolicitudSugerencia by Id
	 * 
	 */
	@Test
	public void testFindSolicitudSugerenciaById() {
		SolicitudSugerenciaEntity solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		assertNotNull(solicitudSugerenciaEntity);
		assertEquals(solicitudSugerenciaEntity.getUnidad(), FIRST_SOLICITUD_SUGERENCIA_UNIDAD);

	}

	/**
	 * Try to update a SolicitudSugerencia
	 * 
	 */
	@Test
	public void testUpdateSolicitudSugerenciaByIdMotiveUpdateOk() {
		solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		solicitudSugerenciaEntity.setMotive(FIRST_SOLICITUD_QUEJA_SET_MOTIVE);
		solicitudQuejaSugerenciaDAO.updateSolicitudSugerencia(solicitudSugerenciaEntity);
		SolicitudSugerenciaEntity solicitudSugerenciaFind = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		assertNotNull(solicitudSugerenciaFind);
		assertEquals(solicitudSugerenciaFind.getMotive(), FIRST_SOLICITUD_QUEJA_SET_MOTIVE);
	}

	/**
	 * Try to update a SolicitudSugerencia
	 * 
	 */
	@Test
	public void testUpdateSolicitudsugerenciaById() {
		solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		solicitudSugerenciaEntity.setUnidad(FIRST_SOLICITUD_SUGERENCIA_UNIDAD);
		solicitudQuejaSugerenciaDAO.updateSolicitudSugerencia(solicitudSugerenciaEntity);
		SolicitudSugerenciaEntity solicitudSugerenciaFind = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		assertNotNull(solicitudSugerenciaFind);
		assertEquals(solicitudSugerenciaFind.getUnidad(), FIRST_SOLICITUD_SUGERENCIA_UNIDAD);
	}

	/**
	 * Try to update a SolicitudSugerencia
	 * 
	 */
	@Test
	public void testUpdateSolicitudSugerenciaByIdUpdateDateOk() {

		Timestamp date = new Timestamp(SolicitudMock.sysDate().getTime());

		solicitudSugerenciaEntity = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		solicitudSugerenciaEntity.setDateTimeIncidence(date);
		solicitudQuejaSugerenciaDAO.updateSolicitudSugerencia(solicitudSugerenciaEntity);
		SolicitudSugerenciaEntity solicitudsugerenciaEntityFind = solicitudQuejaSugerenciaDAO.findSolicitudSugerenciaById(ID_FIRST_SOLICITUD_SUGERENCIA_ENTITY);
		assertNotNull(solicitudsugerenciaEntityFind);
		assertEquals(solicitudsugerenciaEntityFind.getDateTimeIncidence(), date);
	}

	// --> END solicitudSugerencia <--

	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioDAO.findCreatorById(USER_CREATOR);
	}
}
