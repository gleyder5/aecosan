/**
 * 
 */
package test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import test.es.grupoavalon.aecosan.secosan.util.dtofactory.GenericDtoFactoryTest.ByteArrayStrategy;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.GenericDtoFactoryTest.StringFLoatStrategy;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.GenericDtoFactoryTest.TimestampStrategy;
import uk.co.jemos.podam.common.PodamStrategyValue;

/**
 * Class used only to test the DTO Factory. Emulates an Entity
 * 
 * @author otto.abreu
 *
 */
public class EntityMock {

	private String myStringAtribute;
	private int myIntValue;
	private Integer myIntegerValue;
	private float myFloatPrimitiveValue;
	private Float myFloatValue;
	private boolean myBoolValue;
	private Boolean myBooleanValue;
	private String haveSameNameString;
	private int haveSameNameInt;
	private EntityRelatedMock myManyToOneRelationship;
	private List<EntityRelatedMock> myOneToManyRelationship;
	private EntityRelatedMock myOtherToOneRelationship;
	private Date myDateToLong;
	private java.sql.Date mySQLDateToLong;
	@PodamStrategyValue(TimestampStrategy.class)
	private Timestamp mytimeStampToLong;
	@PodamStrategyValue(StringFLoatStrategy.class)
	private String myFLoatString;
	@PodamStrategyValue(ByteArrayStrategy.class)
	private byte[] base64ByteValue;
	private Double myDoubleValue;

	public String getMyStringAtribute() {
		return myStringAtribute;
	}

	public void setMyStringAtribute(String myStringAtribute) {
		this.myStringAtribute = myStringAtribute;
	}

	public int getMyIntValue() {
		return myIntValue;
	}

	public void setMyIntValue(int myIntValue) {
		this.myIntValue = myIntValue;
	}

	public Integer getMyIntegerValue() {
		return myIntegerValue;
	}

	public void setMyIntegerValue(Integer myIntegerValue) {
		this.myIntegerValue = myIntegerValue;
	}

	public float getMyFloatPrimitiveValue() {
		return myFloatPrimitiveValue;
	}

	public void setMyFloatPrimitiveValue(float myFloatPrimitiveValue) {
		this.myFloatPrimitiveValue = myFloatPrimitiveValue;
	}

	public Float getMyFloatValue() {
		return myFloatValue;
	}

	public void setMyFloatValue(Float myFloatValue) {
		this.myFloatValue = myFloatValue;
	}

	public EntityRelatedMock getMyManyToOneRelationship() {
		return myManyToOneRelationship;
	}

	public void setMyManyToOneRelationship(
			EntityRelatedMock myManyToOneRelationship) {
		this.myManyToOneRelationship = myManyToOneRelationship;
	}

	public List<EntityRelatedMock> getMyOneToManyRelationship() {
		return myOneToManyRelationship;
	}

	public void setMyOneToManyRelationship(
			List<EntityRelatedMock> myOneToManyRelationship) {
		this.myOneToManyRelationship = myOneToManyRelationship;
	}

	public boolean isMyBoolValue() {
		return myBoolValue;
	}

	public void setMyBoolValue(boolean myBoolValue) {
		this.myBoolValue = myBoolValue;
	}

	public Boolean getMyBooleanValue() {
		return myBooleanValue;
	}

	public void setMyBooleanValue(Boolean myBooleanValue) {
		this.myBooleanValue = myBooleanValue;
	}

	public String getHaveSameNameString() {
		return haveSameNameString;
	}

	public void setHaveSameNameString(String haveSameNameString) {
		this.haveSameNameString = haveSameNameString;
	}

	public int getHaveSameNameInt() {
		return haveSameNameInt;
	}

	public void setHaveSameNameInt(int haveSameNameInt) {
		this.haveSameNameInt = haveSameNameInt;
	}
	
	

	public EntityRelatedMock getMyOtherToOneRelationship() {
		return myOtherToOneRelationship;
	}

	public void setMyOtherToOneRelationship(
			EntityRelatedMock myOtherToOneRelationship) {
		this.myOtherToOneRelationship = myOtherToOneRelationship;
	}

	public Date getMyDateToLong() {
		return myDateToLong;
	}

	public void setMyDateToLong(Date myDateToLong) {
		this.myDateToLong = myDateToLong;
	}

	public java.sql.Date getMySQLDateToLong() {
		return mySQLDateToLong;
	}

	public void setMySQLDateToLong(java.sql.Date mySQLDateToLong) {
		this.mySQLDateToLong = mySQLDateToLong;
	}

	public Timestamp getMytimeStampToLong() {
		return mytimeStampToLong;
	}

	public void setMytimeStampToLong(Timestamp mytimeStampToLong) {
		this.mytimeStampToLong = mytimeStampToLong;
	}

	public String getMyFLoatString() {
		return myFLoatString;
	}

	public void setMyFLoatString(String myFLoatString) {
		this.myFLoatString = myFLoatString;
	}



	public byte[] getBase64ByteValue() {
		return base64ByteValue;
	}

	public void setBase64ByteValue(byte[] base64ByteValue) {
		this.base64ByteValue = base64ByteValue;
	}

	public Double getMyDoubleValue() {
		return myDoubleValue;
	}

	public void setMyDoubleValue(Double myDoubleValue) {
		this.myDoubleValue = myDoubleValue;
	}



	public boolean equalsReverseFill(Object obj, boolean includeCollection) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EntityMock other = (EntityMock) obj;
		if (haveSameNameInt != other.haveSameNameInt) {
			return false;
		}
		if (haveSameNameString == null) {
			if (other.haveSameNameString != null) {
				return false;
			}
		} else if (!haveSameNameString.equals(other.haveSameNameString)) {
			return false;
		}
		if (myBoolValue != other.myBoolValue) {
			return false;
		}
		if (myBooleanValue == null) {
			if (other.myBooleanValue != null) {
				return false;
			}
		} else if (!myBooleanValue.equals(other.myBooleanValue)) {
			return false;
		}
		if (Float.floatToIntBits(myFloatPrimitiveValue) != Float
				.floatToIntBits(other.myFloatPrimitiveValue)) {
			return false;
		}
		if (myFloatValue == null) {
			if (other.myFloatValue != null) {
				return false;
			}
		} else if (!myFloatValue.equals(other.myFloatValue)) {
			return false;
		}
		if (myIntValue != other.myIntValue) {
			return false;
		}
		if (myManyToOneRelationship == null) {
			if (other.myManyToOneRelationship != null) {
				return false;
			}
		} else if (!myManyToOneRelationship
				.equalsReverseFill(other.myManyToOneRelationship)) {
			return false;
		}
		if (myOtherToOneRelationship == null) {
			if (other.myOtherToOneRelationship != null) {
				return false;
			}
		} else if (!myOtherToOneRelationship
				.equalsReverseFill(other.myOtherToOneRelationship)) {
			return false;
		}
		if (includeCollection) {
			if (myOneToManyRelationship == null) {
				if (other.myOneToManyRelationship != null) {
					return false;
				}
			} else if (!equalEachobjectInOneToManyRelationship(other.myOneToManyRelationship)) {
				return false;
			}
		}
		if (myStringAtribute == null) {
			if (other.myStringAtribute != null) {
				return false;
			}
		} else if (!myStringAtribute.equals(other.myStringAtribute)) {
			return false;
		}
		return true;
	}

	private boolean equalEachobjectInOneToManyRelationship(
			List<EntityRelatedMock> other) {
		boolean equal = true;
		if (this.myOneToManyRelationship.size() != other.size()) {
			equal = false;
		} else {
			for (int i = 0; i < this.myOneToManyRelationship.size(); i++) {
				EntityRelatedMock thisMock = this.myOneToManyRelationship
						.get(i);
				EntityRelatedMock otherMock = other.get(i);
				if (!thisMock.equalsReverseFill(otherMock)) {
					equal = false;
					break;
				}
			}
		}

		return equal;
	}
}
