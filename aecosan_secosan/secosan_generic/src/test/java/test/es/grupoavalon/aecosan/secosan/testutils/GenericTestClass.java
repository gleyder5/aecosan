package test.es.grupoavalon.aecosan.secosan.testutils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

public abstract class GenericTestClass {

	protected Logger logger;
	private static Logger loggerStatic;

	@BeforeClass
	public static void beforeClass() throws IllegalStateException,
			NamingException {

		createJNDIPATHVariable();
	}

	private static void createJNDIPATHVariable() throws IllegalStateException,
			NamingException {
		SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
		String pathRepoValue = getPathRepoFromProperties();
		builder.bind("java:comp/env/" + SecosanConstants.PATH_REPO_CONTEXT_KEY,
				pathRepoValue);
		builder.activate();
		String pathRepoJNDI = SecosanConstants.PATH_REPO;

		if (StringUtils.isBlank(pathRepoJNDI)) {
			getLogger(GenericTestClass.class).warn(
					SecosanConstants.PATH_REPO_CONTEXT_KEY + " not set");
		} else {
			getLogger(GenericTestClass.class).debug(
					SecosanConstants.PATH_REPO_CONTEXT_KEY + " " + pathRepoJNDI);
		}
	}

	private static String getPathRepoFromProperties() {
		Properties props = new Properties();
		InputStream is = GenericTestClass.class.getClassLoader()
				.getResourceAsStream("pathrepovalues.properties");
		String pathRepo = null;
		try {
			props.load(is);
			pathRepo = props.getProperty(SecosanConstants.PATH_REPO_CONTEXT_KEY);
		} catch (Exception e) {
			getLogger(GenericTestClass.class).warn(
					"Can not load pathrepovalues.properties: ", e);
		}
		return pathRepo;
	}

	protected static InputStream getResource(String name) {
		InputStream resource = GenericTestClass.class.getClassLoader()
				.getResourceAsStream(name);
		return resource;
	}

	protected static InputStream getResource(Class<?> classObject, String name) {
		InputStream resource = classObject.getClassLoader()
				.getResourceAsStream(name);
		return resource;
	}

	protected static Reader getResourceAsUTF(Class<?> classObject, String name)
			throws UnsupportedEncodingException {
		InputStream resourceIs = getResource(classObject, name);
		Reader reader = new InputStreamReader(resourceIs, "UTF-8");
		return reader;
	}

	protected static Reader getResourceAsUTF(String name)
			throws UnsupportedEncodingException {
		InputStream resourceIs = getResource(name);
		Reader reader = new InputStreamReader(resourceIs, "UTF-8");
		return reader;
	}

	protected Logger getLogger() {
		if (this.logger == null) {
			this.logger = LoggerFactory.getLogger(this.getClass());
		}
		return this.logger;
	}

	protected static Logger getLogger(Class<?> classParam) {
		if (loggerStatic == null) {
			loggerStatic = LoggerFactory.getLogger(classParam);
		}
		return loggerStatic;
	}

	public static void assertEqualStringNullAsBlank(String expected,
			String obtained) {
		if (expected == null) {
			expected = "";
		}

		if (obtained == null) {
			obtained = "";
		}

		Assert.assertTrue("Not equal strings: expected[" + expected
				+ "], obtained[" + obtained + "]", expected.equals(obtained));
	}

	public static void assertEqualsBigDecimalAndDoubleValues(
			BigDecimal expected, Double obtained) {
		if (expected == null && obtained == null) {
			Assert.assertTrue(true);
		} else if (expected != null && obtained != null) {
			Assert.assertTrue(expected.doubleValue() == obtained.doubleValue());
		} else {
			Assert.assertTrue(false);
		}
	}

	public static void assertListNotNullOrNotEmpty(Collection<?> toAssert) {
		Assert.assertTrue("Collection is empty, and should not",
				toAssert != null && !toAssert.isEmpty());
	}

	public static void assertListNullOrEmpty(Collection<?> toAssert) {
		Assert.assertTrue("Collection is not empty and should",
				toAssert == null || toAssert.isEmpty());
	}

	public static String getFileName(String filePath) {
		String[] filepathSeparated = filePath.split("/");
		return filepathSeparated[filepathSeparated.length - 1];
	}

	protected static List<Long> fromStringToLong(List<String> numbersString) {
		List<Long> longNumbers = new ArrayList<Long>();
		for (String numberString : numbersString) {
			longNumbers.add(Long.parseLong(numberString));
		}

		return longNumbers;
	}

	// public static void setTestContextManager(Class<?> cobjectClass,
	// TestContextManager testContextManager, DataSource dataSource) throws
	// Exception {
	// testContextManager = new TestContextManager(cobjectClass.getClass());
	// testContextManager.prepareTestInstance(testContextManager);
	// TestDBSetup dbSetUp = TestDBSetup.getInstance(dataSource);
	// dbSetUp.setUpDB();
	// }
}
