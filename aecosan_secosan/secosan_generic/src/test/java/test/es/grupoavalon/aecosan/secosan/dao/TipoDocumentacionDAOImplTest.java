package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.TipoDocumentosDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoDocumentacionEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class TipoDocumentacionDAOImplTest extends GenericDBTestClass {

	@Autowired
	private TipoDocumentosDAO tipoDocumentosDAO;

	private static final Long TIPO_DOCUMENTO_ID_1 = 1l;
	private static final Long TIPO_DOCUMENTO_ID_NOT_EXIST = 555552l;
	private static final String TIPO_DOCUMENTO1_DESCRIPCION = "Impreso firmado";

	@Test
	public void testTipoDocumentoFindByIdOK() {
		TipoDocumentacionEntity documento = tipoDocumentosDAO.findTipoDocumentacion(TIPO_DOCUMENTO_ID_1);
		assertEquals(documento.getCatalogValue(), TIPO_DOCUMENTO1_DESCRIPCION);
	}

	@Test
	public void testTipoDocumentoFindByIdNotExist() {
		TipoDocumentacionEntity documento = tipoDocumentosDAO.findTipoDocumentacion(TIPO_DOCUMENTO_ID_NOT_EXIST);
		assertNull(documento);
	}

	@Test(expected = DaoException.class)
	public void testTipoDocumentoFindByIdNull() {
		tipoDocumentosDAO.findTipoDocumentacion(null);
	}

}
