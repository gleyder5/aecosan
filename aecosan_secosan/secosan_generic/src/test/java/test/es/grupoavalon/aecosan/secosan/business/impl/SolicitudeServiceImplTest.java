package test.es.grupoavalon.aecosan.secosan.business.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.SolicitudeService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.ChangeStatusDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.CeseComercializacionDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import test.es.grupoavalon.aecosan.secosan.testutils.setup.GenericSMTPDBTestClass;

public class SolicitudeServiceImplTest extends GenericSMTPDBTestClass {

	@Autowired
	SolicitudeService solicitudeService;

	@Autowired
	SolicitudDAO solicitudDAO;

	private static final String USER = "1";
	private static final String SOLICITUDE_ID_STATUS_DELETABLE = "1001";
	private static final String SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002 = "1002";
	private static final String SOLICITUDE_ID_STATUS_NOT_DELETABLE_1003 = "1003";
	private static final String SOLICITUDE_ID_STATUS_NOT_DELETABLE_1004 = "1004";
	private static final String SOLICITUDE_ID_GENERAL_SECRETARY = "3014";
	private static final String SOLICITUDE_ID_COMMUNICATION = "3016";
	private static final String SOLICITUDE_ID_RISKS_EVALUATION = "3017";
	private static final String SOLICITUDE_ID_CHEMICAL_HAZARD = "2011";
	private static final String CESE_COMERCIALIZACION_CA_FIND_EXPECTED_PRODUCT_NAME = "nombre del producto 1";
	private static final String UPDATE_STATUS = "6";
	private static final String STATUS_SENDED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_SENDED);
	private static final String STATUS_PENDING_OF_SUBSANATION = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PENDING_OF_SUBSANATION);
	private static final String STATUS_PROCESSED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_PROCESSED);
	private static final String STATUS_DENIED = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_STATUS_DENIED);

	@Test
	public void testDeleteSolicitudeByIdOk() {
		CeseComercializacionDTO solicitud = (CeseComercializacionDTO) solicitudeService.findSolicitudeById(USER, SOLICITUDE_ID_STATUS_DELETABLE);
		assertNotNull(solicitud);
		assertEquals(solicitud.getNombreComercialProducto(), CESE_COMERCIALIZACION_CA_FIND_EXPECTED_PRODUCT_NAME);
		solicitudeService.deleteSolicitudeById(USER, SOLICITUDE_ID_STATUS_DELETABLE);
	}

	@Test
	public void testUpdateStatusBySolicitudeIdStatusPendingOfSubsanation() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertFalse(STATUS_PENDING_OF_SUBSANATION.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002, generateChangeStatus(STATUS_PENDING_OF_SUBSANATION,"8"));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertEquals(STATUS_PENDING_OF_SUBSANATION, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdStatusProcessed() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertFalse(STATUS_PROCESSED.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002, generateChangeStatus(STATUS_PROCESSED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertEquals(STATUS_PROCESSED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdStatusDenied() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertFalse(STATUS_DENIED.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002, generateChangeStatus(STATUS_DENIED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertEquals(STATUS_DENIED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdNoSnec() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertFalse(UPDATE_STATUS.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002, generateChangeStatus(UPDATE_STATUS));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002));
		assertEquals(UPDATE_STATUS, String.valueOf(solicitude.getStatus().getId()));
	}

	private ChangeStatusDTO generateChangeStatus(String statusId,String areaId) {
		ChangeStatusDTO status = new ChangeStatusDTO();
		status.setSolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002);
		status.setStatusId(statusId);
		status.setFileB64("UHJ1ZWJhIGVudmlvIFNORUM=");
		status.setFileName("filename.pdf");
		status.setArea(areaId);
		return status;
	}
	private ChangeStatusDTO generateChangeStatus(String statusId) {
		ChangeStatusDTO status = new ChangeStatusDTO();
		status.setSolicitudeId(SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002);
		status.setStatusId(statusId);
		status.setFileB64("UHJ1ZWJhIGVudmlvIFNORUM=");
		status.setFileName("filename.pdf");
		return status;
	}

	@Test
	public void testUpdateStatusBySolicitudeIdNoSnecGeneralSecretary() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_GENERAL_SECRETARY));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_GENERAL_SECRETARY, generateChangeStatus(STATUS_SENDED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_GENERAL_SECRETARY));
		assertEquals(STATUS_SENDED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdNoSnecRisksEvaluation() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_RISKS_EVALUATION));
		assertFalse(STATUS_SENDED.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_RISKS_EVALUATION, generateChangeStatus(STATUS_SENDED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_RISKS_EVALUATION));
		assertEquals(STATUS_SENDED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdNoSnecCommunication() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_COMMUNICATION));
		assertFalse(STATUS_SENDED.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_COMMUNICATION, generateChangeStatus(STATUS_SENDED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_COMMUNICATION));
		assertEquals(STATUS_SENDED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test
	public void testUpdateStatusBySolicitudeIdNoSnecChemicalHazard() {
		SolicitudEntity solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_CHEMICAL_HAZARD));
		assertFalse(STATUS_SENDED.equals(String.valueOf(solicitude.getStatus().getId())));
		solicitudeService.solicitudeUpdateStatusBySolicitudeId(SOLICITUDE_ID_CHEMICAL_HAZARD, generateChangeStatus(STATUS_SENDED));
		solicitude = solicitudDAO.findSolicitudById(Long.valueOf(SOLICITUDE_ID_CHEMICAL_HAZARD));
		assertEquals(STATUS_SENDED, String.valueOf(solicitude.getStatus().getId()));
	}

	@Test(expected = ServiceException.class)
	public void testDeleteSolicitudeByIdKoStatus2() {
		solicitudeService.deleteSolicitudeById(USER, SOLICITUDE_ID_STATUS_NOT_DELETABLE_1002);
	}

	@Test(expected = ServiceException.class)
	public void testDeleteSolicitudeByIdKoStatus3() {
		solicitudeService.deleteSolicitudeById(USER, SOLICITUDE_ID_STATUS_NOT_DELETABLE_1003);
	}

	@Test(expected = ServiceException.class)
	public void testDeleteSolicitudeByIdKoStatus4() {
		solicitudeService.deleteSolicitudeById(USER, SOLICITUDE_ID_STATUS_NOT_DELETABLE_1004);
	}

}
