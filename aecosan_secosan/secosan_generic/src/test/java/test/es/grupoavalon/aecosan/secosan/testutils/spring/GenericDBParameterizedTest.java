package test.es.grupoavalon.aecosan.secosan.testutils.spring;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestContextManager;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import test.es.grupoavalon.aecosan.secosan.testutils.GenericTestClass;
import test.es.grupoavalon.aecosan.secosan.testutils.setup.TestDBSetup;

@RunWith(Parameterized.class)
@ContextConfiguration(locations = { "classpath:" + SecosanConstants.SPRING_CONTEXT_CONFIG })
public abstract class GenericDBParameterizedTest extends GenericTestClass {

	@Autowired
	private DataSource dataSource;

	@PersistenceContext
	private EntityManager em;

	private TestContextManager testContextManager;

	@Before
	public void setUpContext() throws Exception {
		this.testContextManager = new TestContextManager(getClass());
		this.testContextManager.prepareTestInstance(this);
		TestDBSetup dbSetUp = TestDBSetup.getInstance(dataSource);
		dbSetUp.setUpDB();
		setupClassBefore();
	}

	public abstract void setupClassBefore();

}
