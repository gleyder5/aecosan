package test.es.grupoavalon.aecosan.secosan.business.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.FinanciacionAlimentosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;

public class FinanciacionAlimentosServiceImplTest extends GenericDBTestClass {

	@Autowired
	FinanciacionAlimentosService financiacionAlimentosService;

	@Test(expected = ServiceException.class)
	public void testAddIncOferAlimUMEEntityKoExceptionThrowerClassNotSuported() {

		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		financiacionAlimentosService.add(SolicitudMock.USER, solicitudQuejaDTO);
	}

	@Test(expected = ServiceException.class)
	public void testUpdateIncOferAlimUMEEntityKoExceptionThrowerClassNotSuported() {

		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		financiacionAlimentosService.update(SolicitudMock.USER, solicitudQuejaDTO);

	}

}
