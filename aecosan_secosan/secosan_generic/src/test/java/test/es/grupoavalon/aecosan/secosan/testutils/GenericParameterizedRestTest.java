package test.es.grupoavalon.aecosan.secosan.testutils;

import javax.naming.NamingException;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBParameterizedTest;

public abstract class GenericParameterizedRestTest extends GenericDBParameterizedTest {
	@Override
	public void setupClassBefore() {
		// TODO Auto-generated method stub

	}

	@BeforeClass
	public static void startServer() throws IllegalStateException, NamingException {
		UtilRestTestClass.startServer();
	}

	@AfterClass
	public static void stopServer() {
		UtilRestTestClass.stopServer();
	}
}
