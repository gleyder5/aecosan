package test.es.grupoavalon.aecosan.secosan.testutils.setup;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subethamail.wiser.Wiser;
import org.subethamail.wiser.WiserMessage;

public class SMTPServiceForTest {

	private static Wiser wiser = new Wiser();
	private static Logger logger = LoggerFactory.getLogger(SMTPServiceForTest.class);

	public static void startSMTPServer() {
		wiser = new Wiser();
		wiser.setPort(2587);
		wiser.setHostname("127.0.0.1");
		wiser.start();
	}

	public static void stopSMTPServer() {
		wiser.getServer().stop();
	}

	public static void dumpMessagesToLog() {

		try {
			List<WiserMessage> messages = wiser.getMessages();
			if (!CollectionUtils.isEmpty(messages)) {
				logger.info("============MAIL GENERATED=============");
				StringBuilder messagesString = new StringBuilder();
				for (WiserMessage message : messages) {
					printMessagess(messagesString, message);
				}
				logger.info(messagesString.toString());
				logger.info("============MAIL GENERATED ENDS=============");
			}
		} catch (Exception e) {
			logger.warn("Can not dump email messages due an error:" + e.getMessage(), e);
		}
	}

	public static List<WiserMessage> getMessages() {
		List<WiserMessage> messages = null;
		try {
			messages = wiser.getMessages();
		} catch (Exception e) {
			logger.warn("Can not get email messages due an error:" + e.getMessage(), e);
		}
		return messages;
	}

	private static void printMessagess(StringBuilder messagesString, WiserMessage message) {
		messagesString.append("-----MESSAGE INIT------\n");
		messagesString.append(message.toString());
		messagesString.append("-----MESSAGE ENDS------\n");
	}

}
