package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class EvaluacionRiesgosRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "evaluacionRiesgos/";
	private static final String REST_SERVICE_USER = "1/";

	private static final String SENDED_JSON_FILE_ADD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "evaluacionRiesgosAddOk.json";
	private static final String SENDED_JSON_FILE_ADD_ACCOUNT_PAYMENT_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "evaluacionRiesgosAddAccountPaymentOk.json";
	private static final String SENDED_JSON_FILE_ADD_ACCOUNT_PAYMENT_ONLINE_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "evaluacionRiesgosAddAccountPaymentOnlineOk.json";
	private static final String SENDED_JSON_FILE_ADD_CREDIT_CARD_PAYMENT_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "evaluacionRiesgosAddAccountPaymentOnlineOk.json";
	private static final String SENDED_JSON_FILE_UPDATE_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "evaluacionRiesgosUpdateOk.json";

	@Test
	public void testEvaluacionRiesgosRestServiceTestAddEvaluacionRiesgosOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_USER, SENDED_JSON_FILE_ADD_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testEvaluacionRiesgosRestServiceTestAddEvaluacionRiesgosPayByAccountOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_USER, SENDED_JSON_FILE_ADD_ACCOUNT_PAYMENT_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testEvaluacionRiesgosRestServiceTestAddEvaluacionRiesgosPayByAccountOnlineOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_USER, SENDED_JSON_FILE_ADD_ACCOUNT_PAYMENT_ONLINE_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testEvaluacionRiesgosRestServiceTestAddEvaluacionRiesgosPayByCreditCardOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_USER, SENDED_JSON_FILE_ADD_CREDIT_CARD_PAYMENT_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testEvaluacionRiesgosRestServiceTestUpdateEvaluacionRiesgosOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_USER, SENDED_JSON_FILE_UPDATE_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

}
