package test.es.grupoavalon.aecosan.secosan.testutils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.naming.NamingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.*;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.eclipse.jetty.util.ajax.JSON;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.rest.security.JwtUtil;

public class UtilRestTestClass {
    private static final String ENDPOINT_ADDRESS = "http://localhost:10002/rest/";
    public static final String EXPECTED_JSON_FOLDER = "expectedJSON/";
    public static final String SENDED_JSON_FOLDER = "sendedJSON/";
    private static final String JSON_END = "\"}";

    public static final Logger logger = LoggerFactory.getLogger(GenericRestTest.class);

    private static Server server;

    public static void startServer() throws IllegalStateException, NamingException {
        GenericTestClass.beforeClass();
        JAXRSServerFactoryBean sf = getServerBean();
        sf.setAddress(ENDPOINT_ADDRESS);
        server = sf.create();
        logger.debug("server running:" + ENDPOINT_ADDRESS);
    }

    private static JAXRSServerFactoryBean getServerBean() {
        @SuppressWarnings("resource")
        ApplicationContext ctx = new ClassPathXmlApplicationContext(SecosanConstants.SPRING_CONTEXT_CONFIG);
        return ctx.getBean(JAXRSServerFactoryBean.class);
    }

    public static void stopServer() {
        if (server != null && server.isStarted()) {
            server.stop();
            server.destroy();
        }
        logger.debug("server stoped:" + ENDPOINT_ADDRESS);
    }

    public static void executeRestOKGetTest(String path, String expectedJsonFile) {
        Response response = invokeRestServiceGet(path);
        assertReponseOk(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestOKGetTest(String path) {
        Response response = invokeRestServiceGet(path);
        assertReponseOk(response);
    }

    public static void executeRestOKGetTest(String path, String expectedJsonFile, Map<String, String> queryparams) {
        Response response = invokeRestServiceGet(path, queryparams);
        assertReponseOk(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestOKGetTest(String path, String queryParamKey, List<String> queryparamsValues) {
        Response response = invokeRestServiceGet(path, queryParamKey, queryparamsValues);
        assertReponseOk(response);
    }

    public static void executeRestOKGetTest(String path, String expectedJsonFile, Map<String, String> queryparams, MultivaluedMap<String, Object> headers) throws IOException {
        Response response = invokeRestServiceGet(path, queryparams, headers);
        assertReponseOk(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestErrorGetTest(String path) {
        Response response = invokeRestServiceGet(path);
        assertReponse500(response);
    }

    public static void executeRestOKPutTest(String path) {
        Response response = invokeRestServicePut(path);
        assertReponseOk(response);
    }

    public static void executeRestOKPostTest(String path) {
        Response response = invokeRestServicePost(path);
        assertReponseOk(response);
    }

    public static void executeRestOKPutTest(String path, String queryParamKey, List<String> queryparamsValues) {
        Response response = invokeRestServicePut(path, queryParamKey, queryparamsValues);
        assertReponseOk(response);
    }

    public static void executeRestOKPostTest(String path, String json) throws JsonProcessingException, IOException {
        Response response = invokeRestServicePostJson(path, json);
        assertReponseOk(response);
    }

    public static void executeRestOKDeleteTest(String path) {
        Response response = invokeRestServiceDelete(path);
        assertReponseOk(response);
    }

    public static void executeRestOKDeleteTest(String path, String queryParamKey, List<String> queryparamsValues) {
        Response response = invokeRestServiceDelete(path, queryParamKey, queryparamsValues);
        assertReponseOk(response);
    }

    public static void executeRestErrorDeleteTest(String path) {
        Response response = invokeRestServiceDelete(path);
        assertReponse500(response);
    }

    public static void executeRestErrorDeleteTest(String path, String expectedJsonFile) {
        Response response = invokeRestServiceDelete(path);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestErrorPutTest(String path, String expectedJsonFile) {
        Response response = invokeRestServicePut(path);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestErrorPutTest(String path, String queryParamKey, List<String> queryparamsValues, String expectedJsonFile) {
        Response response = invokeRestServicePut(path, queryParamKey, queryparamsValues);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestClientNotFound(String path) {
        Response response = invokeRestServiceGet(path);
        assertReponse404(response);
    }

    public static void executeRestServerErrorTest(String path) {
        Response response = invokeRestServiceGet(path);
        assertReponse500(response);
    }

    public static void executeRestErrorDownloadFileTest(String path, String expectedJsonFile) {
        Response response = invokeRestServiceGet(path);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestErrorDeleteTest(String path, String queryParamKey, List<String> queryparamsValues, String expectedJsonFile) {
        Response response = invokeRestServiceDelete(path, queryParamKey, queryparamsValues);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestServerErrorTest(String path, String expectedJsonFile) {
        Response response = invokeRestServiceGet(path);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestServerErrorTest(String path, String queryParamKey, List<String> queryparamsValues, String expectedJsonFile) {
        Response response = invokeRestServiceGet(path, queryParamKey, queryparamsValues);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestPutServerErrorTest(String path, String queryParamKey, List<String> queryparamsValues, String expectedJsonFile) {
        Response response = invokeRestServicePut(path, queryParamKey, queryparamsValues);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static void executeRestDeleteServerErrorTest(String path, String queryParamKey, List<String> queryparamsValues, String expectedJsonFile) {
        Response response = invokeRestServiceDelete(path, queryParamKey, queryparamsValues);
        assertReponse500(response);
        compareJSONReponse(response, expectedJsonFile);
    }

    public static Response invokeRestServiceGet(String urlPath) {

        Invocation.Builder builder = getBuilder(urlPath);
        return builder.get();
    }

    protected static Response invokeRestServiceGet(String urlPath, Map<String, String> queryParams, MultivaluedMap<String, Object> headers) throws JsonProcessingException, IOException {
        WebTarget target = instnaciateWebTargetWithQueryParams(urlPath, queryParams);
        Invocation.Builder builder = target.request();
        builder.headers(headers);

        return builder.get();
    }

    public static Response invokeRestServicePut(String urlPath) {

        Invocation.Builder builder = getBuilder(urlPath);
        return builder.put(null);
    }

    public static Response invokeRestServicePost(String urlPath) {

        Invocation.Builder builder = getBuilder(urlPath);
        return builder.post(null);
    }

    public static Response invokeRestServicePostJson(String urlPath, String jsonFile) throws JsonProcessingException, IOException {

        Invocation.Builder builder = getBuilder(urlPath);
        String expectedJson = getExpectedJsonResource(jsonFile);

        return builder.post(Entity.entity(expectedJson, MediaType.APPLICATION_JSON));
    }

    public static void executeRestOKPostTest(String path, String json, MultivaluedMap<String, Object> headers) throws JsonProcessingException, IOException {
        Response response = invokeRestServicePostJson(path, json, headers);
        assertReponseOk(response);
    }

    public static Response invokeRestServicePostJson(String urlPath, String json, MultivaluedMap<String, Object> headers) throws JsonProcessingException, IOException {

        Invocation.Builder builder = getBuilderWithHeader(urlPath, headers);
        String expectedJson = getExpectedJsonResource(json);
        return builder.post(Entity.entity(expectedJson, MediaType.APPLICATION_JSON));
    }

    private static Invocation.Builder getBuilderWithHeader(String urlPath, MultivaluedMap<String, Object> headers) {
        WebTarget target = getWebTarget();
        target = target.path(urlPath);
        Invocation.Builder builder = target.request();
        builder.headers(headers);
        return builder;
    }

    public static Response invokeRestServicePutJson(String urlPath, String jsonFile) throws JsonProcessingException, IOException {

        Invocation.Builder builder = getBuilder(urlPath);
        String expectedJson = getExpectedJsonResource(jsonFile);

        return builder.put(Entity.entity(expectedJson, MediaType.APPLICATION_JSON));
    }

    public static Response invokeRestServicePut(String urlPath, String queryParamListKey, List<String> queryParamValueList) {

        WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath, queryParamListKey, queryParamValueList);
        Invocation.Builder builder = target.request();
        return builder.put(null);
    }

    public static Response invokeRestServiceDelete(String urlPath) {

        Invocation.Builder builder = getBuilder(urlPath);
        return builder.delete();
    }

    public static Response invokeRestServiceDelete(String urlPath, String queryParamListKey, List<String> queryParamValueList) {

        WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath, queryParamListKey, queryParamValueList);
        Invocation.Builder builder = target.request();
        return builder.delete();
    }

    private static Invocation.Builder getBuilder(String urlPath) {
        WebTarget target = getWebTarget();
        target = target.path(urlPath);
        return target.request();
    }

    public static Response invokeRestServiceGet(String urlPath, Map<String, String> queryParams) {

        WebTarget target = instnaciateWebTargetWithQueryParams(urlPath, queryParams);
        Invocation.Builder builder = target.request();

        return builder.get();
    }

    public static Response invokeRestServiceGet(String urlPath, String queryParamListKey, List<String> queryParamValueList) {

        WebTarget target = instnaciateWebTargetWithQueryParamsList(urlPath, queryParamListKey, queryParamValueList);
        Invocation.Builder builder = target.request();

        return builder.get();
    }

    private static WebTarget instnaciateWebTargetWithQueryParams(String urlPath, Map<String, String> queryParams) {
        WebTarget target = getWebTarget();
        target = target.path(urlPath);
        for (Map.Entry<String, String> entry : queryParams.entrySet()) {
            logger.debug("setting param :" + entry.getKey() + " = " + entry.getValue());
            target = target.queryParam(entry.getKey(), entry.getValue());
        }

        return target;
    }

    private static WebTarget instnaciateWebTargetWithQueryParamsList(String urlPath, String key, List<String> values) {
        WebTarget target = getWebTarget();
        target = target.path(urlPath);
        for (String value : values) {
            logger.debug("setting param :" + key + " = " + value);
            target = target.queryParam(key, value);
        }

        return target;
    }

    private static WebTarget getWebTarget() {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(ENDPOINT_ADDRESS);
        return target;
    }

    public static void assertReponseOk(Response response) {

        compareReponseByStatus(Response.Status.OK, response);
    }

    public static void assertReponseTokenOk(Response response) {

        String cadena = response.readEntity(String.class);
        TokenUtils tokenUtils = new TokenUtils();
        JwtUtil jwtUtil = tokenUtils.getJwtUtil();
        boolean isValid = jwtUtil.isTokenValid(cadena);
        System.out.println(isValid);
        compareReponseByStatus(Response.Status.OK, response);
    }

    public static void assertReponse404(Response response) {

        compareReponseByStatus(Response.Status.NOT_FOUND, response);
    }

    public static void assertReponse500(Response response) {

        compareReponseByStatus(Response.Status.INTERNAL_SERVER_ERROR, response);
    }

    public static void compareReponseByStatus(Response.Status status, Response response) {

        assertEquals(status.getStatusCode(), response.getStatus());

    }
    public static void validateJsonResponse(String path,String expectedJsonFile,String[] ignoredFields){
        Response response = invokeRestServiceGet(path);
        assertReponseOk(response);
        try {
            String expectedJsonString = getExpectedJsonResource(expectedJsonFile);
            validateReceivedAndSendedJSONs(expectedJsonString, response.readEntity(String.class),ignoredFields);
        }catch (Exception e){
            logger.warn("Error processing the JSON Response in TEST", e);
            fail("Error processing the JSON Response in TEST: " + e.getMessage());
        }
    }

    public static void compareJSONReponse(Response response, String expectedJSON) {
        try {
            String expectedJsonString = getExpectedJsonResource(expectedJSON);
            JsonNode expectedJson = parseToJSONNode(expectedJsonString);
            JsonNode obtainedJson = parseToJSONNode(response.readEntity(String.class));
            logger.debug("JSON: " + obtainedJson);
            logger.debug("JSON Expected: " + expectedJson);
            assertEquals("JSON NOT EQUAL", expectedJson, obtainedJson);
        } catch (Exception e) {
            logger.warn("Error processing the JSON Response in TEST", e);
            fail("Error processing the JSON Response in TEST: " + e.getMessage());
        }
    }

    public static String getExpectedJsonResource(String resourceName) {
        InputStream is = GenericRestTest.getResource(resourceName);
        String inputStreamString = new Scanner(is, "UTF-8").useDelimiter("\\A").next();
        return inputStreamString.trim();
    }

    public JsonNode transformToResponseToJSON(Response response) throws JsonProcessingException, IOException {
        JsonNode obtainedJson = parseToJSONNode(response.readEntity(String.class));
        return obtainedJson;
    }

    public static JsonNode parseToJSONNode(String json) throws JsonProcessingException, IOException {

        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.readTree(json);
        return rootNode;

    }
//	Check if both json match

    public static List<String> mapsAreEqual(LinkedHashMap<String, Object> sended, LinkedHashMap<String, Object> received,List<String> fields,String[] ignoredFields) throws JsonProcessingException, IOException {
        boolean isValid   = false;
        List<String>  errorFields =  fields;
        if (!sended.keySet().isEmpty() && !received.keySet().isEmpty()) {
            for (String key : received.keySet()) {
                if (sended.containsKey(key)) {
                    if (received.get(key).getClass().equals(LinkedHashMap.class)) {
                        mapsAreEqual((LinkedHashMap) sended.get(key), (LinkedHashMap) received.get(key),errorFields,ignoredFields);
                    } else {
                        if (received.get(key).equals(sended.get(key)) || Arrays.asList(ignoredFields).contains(key)) {
                            isValid = true;
                            continue;
                        }else{
                            errorFields.add(key);
                        }
                    }
                }else{
                    errorFields.add(key);
                }
            }
        }
        return errorFields;
    }

    public static void validateReceivedAndSendedJSONs(String jsonExpected, String jsonReceived,String[] ignoredFields) throws  IOException {
//        JsonNode treeSended = new ObjectMapper().readTree(jsonExpected);
//        JsonNode treeReceived = new ObjectMapper().readTree(jsonReceived);
//        Assert.assertEquals("JSON Structure doesn't match",treeReceived,treeSended);
        LinkedHashMap<String, Object> jsonExpectedMap = new ObjectMapper().readValue(jsonExpected, LinkedHashMap.class);
        LinkedHashMap<String, Object> jsonReceivedMap = new ObjectMapper().readValue(jsonReceived, LinkedHashMap.class);
        List<String> result = mapsAreEqual(jsonExpectedMap,jsonReceivedMap,new ArrayList<String>(),ignoredFields);
        logger.debug("JSON: " + jsonReceived);
        logger.debug("JSON Expected: " + jsonExpected);
        Assert.assertTrue("These values doesn't match : " +result.toString() ,result.isEmpty());
    }


    public static JsonNode parseToJSONNode(Object toJson) throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootNode = mapper.valueToTree(toJson);
        return rootNode;
    }

    public Response uploadFile(String urlPath, String filePath) {
        urlPath = ENDPOINT_ADDRESS + urlPath;
        logger.debug("Upload to url: " + urlPath);
        WebClient client = WebClient.create(urlPath);
        client.type(MediaType.MULTIPART_FORM_DATA);
        List<Attachment> atts = generateFileToUpload(filePath);
        return client.postCollection(atts, Attachment.class);
    }

    private List<Attachment> generateFileToUpload(String filePath) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(filePath);
        List<Attachment> atts = new LinkedList<Attachment>();
        String fileName = GenericTestClass.getFileName(filePath);
        ContentDisposition cd = new ContentDisposition("attachment;filename=" + fileName);
        atts.add(new Attachment(fileName, stream, cd));
        return atts;
    }

    public Response postJsonParam(String urlPath, Object toJson) throws JsonProcessingException, IOException {

        WebClient client = instanciateWebClientForJSONObject(urlPath);
        JsonNode json = parseToJSONNode(toJson);
        logger.debug("json To post:" + json);
        return client.post(json.toString());

    }

    public Response putJsonParam(String urlPath, Object toJson) throws JsonProcessingException, IOException {

        WebClient client = instanciateWebClientForJSONObject(urlPath);
        JsonNode json = parseToJSONNode(toJson);
        logger.debug("json To put:" + json);
        return client.put(json.toString());

    }

    private static WebClient instanciateWebClientForJSONObject(String urlPath) {
        urlPath = ENDPOINT_ADDRESS + urlPath;
        logger.debug("Upload to url: " + urlPath);
        WebClient client = WebClient.create(urlPath);
        client.type(MediaType.APPLICATION_JSON);

        return client;

    }

    public static void compareJSONReponseMock(Response response, String expectedJSON) {

        try {
            JsonNode obtainedJson = parseToJSONNode(response.readEntity(String.class));
            logger.debug("JSON: " + obtainedJson);
            logger.debug("JSON Expected: " + expectedJSON);
            assertTrue(obtainedJson.toString().contains(expectedJSON));
            assertTrue(obtainedJson.toString().endsWith(JSON_END));

        } catch (Exception e) {
            logger.warn("Error processing the JSON Response in TEST", e);
            fail("Error processing the JSON Response in TEST: " + e.getMessage());
        }

    }

    public static MultivaluedMap<String, Object> generateUserLoggedHeaderMap(String userId) {
        MultivaluedMap<String, Object> headers = new MultivaluedHashMap<String, Object>(1);
        headers.add(SecosanConstants.REQUEST_HEADER_USER_ID, userId);
        return headers;
    }
}
