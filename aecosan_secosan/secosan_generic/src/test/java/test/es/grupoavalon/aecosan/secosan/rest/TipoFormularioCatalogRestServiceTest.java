package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class TipoFormularioCatalogRestServiceTest extends GenericRestTest {

	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = "/catalog";
	private static final String REST_SERVICE_PARAMS = "/TipoFormulario";

	private static final String EXPECTED_OK_JSON_FILE = UtilRestTestClass.EXPECTED_JSON_FOLDER + "tipoFormularioCatalog.json";

	@Test
	public void testTipoFormularioCatalogRestServiceTest() {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS, EXPECTED_OK_JSON_FILE);
	}
}
