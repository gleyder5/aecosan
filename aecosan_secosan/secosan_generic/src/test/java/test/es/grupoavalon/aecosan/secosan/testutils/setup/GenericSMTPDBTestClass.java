package test.es.grupoavalon.aecosan.secosan.testutils.setup;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericTestClass;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

public abstract class GenericSMTPDBTestClass extends GenericDBTestClass {

	@BeforeClass
	public static void setUpServer() throws Throwable {
		GenericTestClass.beforeClass();
		SMTPServiceForTest.startSMTPServer();
	}

	@AfterClass
	public static void after() {
		SMTPServiceForTest.dumpMessagesToLog();
		SMTPServiceForTest.stopSMTPServer();

	}
}
