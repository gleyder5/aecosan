package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudLmrFitosanitariosMock;
import es.grupoavalon.aecosan.secosan.dao.SolicitudLmrFitosanitariosDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;

/**
 * 
 * @author xi314616
 *
 */
public class SolicitudLmrFitosanitariosDAOImplTest extends GenericDBTestClass {

	@Autowired
	private SolicitudLmrFitosanitariosDAO solicitudLmrFitosanitariosDAO;

	private static final Long ID_FIRST_SOLICITUD_LMR_FITOSANITARIOS_ENTITY = 2011L;
	private static final Long LMR_FITOSANITARIOS_TIPO_FORMULARIO_ID = 14L;
	private static final String FIRST_SOLICITUD_LMR_FITOSANITARIOS_SET_ACTIVE_SUBSTANCE = "Update for active substance";

	@Autowired
	private UsuarioDAO usuarioCreadorDAO;

	private static final Long USER_CREATOR = 1L;

	private SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity;

	// --> INIT solicitudLmrFitosanitarios <--
	@Test
	public void testAddSolicitudLmrFitosanitarios() {
		solicitudLmrFitosanitariosEntity = SolicitudLmrFitosanitariosMock.generateSolicitudLmrFitosanitariosEntity();
		SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntityFind;
		solicitudLmrFitosanitariosEntity.setUserCreator(generateUserCreatorMock());
		solicitudLmrFitosanitariosEntity.getFormularioEspecifico().setId(LMR_FITOSANITARIOS_TIPO_FORMULARIO_ID);
		solicitudLmrFitosanitariosDAO.addSolicitudLmrFitosanitarios(solicitudLmrFitosanitariosEntity);

		solicitudLmrFitosanitariosEntityFind = solicitudLmrFitosanitariosDAO.findSolicitudLmrFitosanitariosById(solicitudLmrFitosanitariosEntity.getId());

		assertNotNull(solicitudLmrFitosanitariosEntityFind);
		assertEquals(solicitudLmrFitosanitariosEntityFind, solicitudLmrFitosanitariosEntity);

	}

	@Test
	public void testUpdateSolicitudLmrFitosanitariosById() {
		solicitudLmrFitosanitariosEntity = solicitudLmrFitosanitariosDAO.findSolicitudLmrFitosanitariosById(ID_FIRST_SOLICITUD_LMR_FITOSANITARIOS_ENTITY);
		solicitudLmrFitosanitariosEntity.setActiveSubstance(FIRST_SOLICITUD_LMR_FITOSANITARIOS_SET_ACTIVE_SUBSTANCE);
		solicitudLmrFitosanitariosDAO.updateSolicitudLmrFitosanitarios(solicitudLmrFitosanitariosEntity);
		solicitudLmrFitosanitariosEntity = solicitudLmrFitosanitariosDAO.findSolicitudLmrFitosanitariosById(ID_FIRST_SOLICITUD_LMR_FITOSANITARIOS_ENTITY);
		assertNotNull(solicitudLmrFitosanitariosEntity);
		assertEquals(solicitudLmrFitosanitariosEntity.getActiveSubstance(), FIRST_SOLICITUD_LMR_FITOSANITARIOS_SET_ACTIVE_SUBSTANCE);
	}

	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioCreadorDAO.findCreatorById(USER_CREATOR);
	}

}
