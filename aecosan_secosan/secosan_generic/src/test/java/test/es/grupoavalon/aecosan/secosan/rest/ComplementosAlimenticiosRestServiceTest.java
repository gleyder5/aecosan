package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class ComplementosAlimenticiosRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "complementosAlimenticios/";
	private static final String REST_SERVICE_CESE_COMERCIALIZACION = "ceseComercializacion/";
	private static final String REST_SERVICE_PUESTA_MERCADO = "puestaMercado/";
	private static final String REST_SERVICE_MODIFICACION_DATOS = "modificacionDatos/";
	private static final String REST_SERVICE_USER = "1/";

	// Cese Comercialización
	private static final String SENDED_JSON_FILE_ADD_CESE_SOLICITUD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION
			+ "ceseComercializacionAddOk.json";
	private static final String SENDED_JSON_FILE_UPDATE_CESE_SOLICITUD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION
			+ "ceseComercializacionUpdateOk.json";
	private static final String SENDED_JSON_FILE_CESE_SOLICITUD_KO_SOLICITANTE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION
			+ "ceseComercializacionKoSolicitante.json";

	// Puesta Mercado

	private static final String SENDED_JSON_FILE_PUESTA_MERCADO_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO + "puestaMercadoOk.json";
	private static final String SENDED_JSON_FILE_PUESTA_MERCADO_KO_PRESENTACION = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO
			+ "puestaMercadoKoPresentacion.json";
	private static final String SENDED_JSON_FILE_PUESTA_MERCADO_KO_SOLICITANTE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO
			+ "puestaMercadoKoSolicitante.json";

	private static final String SENDED_JSON_FILE_UPDATE_PUESTA_MERCADO_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO + "puestaMercadoUpdateOk.json";

	// Modificación Datos

	private static final String SENDED_JSON_FILE_MODIFICACION_DATOS_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_MODIFICACION_DATOS + "modificacionDatosOk.json";

	private static final String SENDED_JSON_FILE_UPDATE_MODIFICACION_DATOS_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_MODIFICACION_DATOS
			+ "modificacionDatosUpdateOk.json";

	// INIT--> Test Cese Comercialización

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddCeseComercializacionOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION + REST_SERVICE_USER, SENDED_JSON_FILE_ADD_CESE_SOLICITUD_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddCeseComercializacionKoSolicitante() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION + REST_SERVICE_USER,
				SENDED_JSON_FILE_CESE_SOLICITUD_KO_SOLICITANTE);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testComplementosAlimenticiosRestServiceTestUpdateCeseComercializacionOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION + REST_SERVICE_USER, SENDED_JSON_FILE_UPDATE_CESE_SOLICITUD_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// END--> Test Cese Comercialización

	// INIT--> Test Puesta Mercado

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddPuestaMercadoOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO + REST_SERVICE_USER, SENDED_JSON_FILE_PUESTA_MERCADO_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}


	@Test
	public void testComplementosAlimenticiosRestServiceTestAddPuestaMercadoKoSolicitante() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_CESE_COMERCIALIZACION + REST_SERVICE_USER,
				SENDED_JSON_FILE_PUESTA_MERCADO_KO_SOLICITANTE);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddPuestaMercadoKoPresentacion() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO + REST_SERVICE_USER, SENDED_JSON_FILE_PUESTA_MERCADO_KO_PRESENTACION);
		UtilRestTestClass.assertReponse500(response2);
	}


	@Test
	public void testComplementosAlimenticiosRestServiceTestUpdatePuestaMercadoOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_PUESTA_MERCADO + REST_SERVICE_USER, SENDED_JSON_FILE_UPDATE_PUESTA_MERCADO_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// Fin Test para Puesta Mercado

	// Inicio Test para Modificación Datos

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddModificacionDatosOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_MODIFICACION_DATOS + REST_SERVICE_USER, SENDED_JSON_FILE_MODIFICACION_DATOS_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// KO campos obligatorios

	@Test
	public void testComplementosAlimenticiosRestServiceTestUpdateModificacionDatosOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_MODIFICACION_DATOS + REST_SERVICE_USER,
				SENDED_JSON_FILE_UPDATE_MODIFICACION_DATOS_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// Fin Test para Modificación Datos

}
