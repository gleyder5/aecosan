package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Ignore;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

public class LoginRestServiceTest extends GenericRestTest {
	private static final String REST_SERVICE_PATH = "login/";

	private static final String SENDED_JSON_FILE_NORMAL_USER_LOGIN_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "loginNormalUser.json";

	@Ignore
	@Test
	public void testNormalUserLoginOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(REST_SERVICE_PATH, SENDED_JSON_FILE_NORMAL_USER_LOGIN_OK);
		UtilRestTestClass.assertReponseTokenOk(response);
	}
}
