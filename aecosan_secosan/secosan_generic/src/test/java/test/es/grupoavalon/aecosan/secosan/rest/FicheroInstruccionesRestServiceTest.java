package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

/**
 * 
 * @author juan.cabrerizo
 *
 */

public class FicheroInstruccionesRestServiceTest extends GenericRestTest {


	private static final String REST_SERVICE_PATH = "instructions/";
	private static final String INSTRUCTIONS_LIST = "instructionList/";
	private static final String PAYMENT = "payment/";
	private static final String ADMIN = "/admin";
	private static final String INSTRUCTIONS_TO_PAY = SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH + "instructionsToPay";
	private static final String REST_SERVICE_PATH_ERROR404 = "instructions/5/FAKE";
	private static final String REST_SERVICE_PATH_ERROR500 = "instructions/FAKE";
	private static final String SOLICITUDE_TYPE = "5";
	private static final String FORM = "2";
	private static final String USER = "5";
	private static final String EXPECTED_OK_JSON_FILE5 = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + "ficheroInstrucciones5.json";
	private static final String EXPECTED_OK_JSON_FILE2 = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + "ficheroInstrucciones2.json";
	private static final String EXPECTED_OK_JSON_INSTRUCTIONS_LIST = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + "listaInstruccionesUser.json";
	private static final String EXPECTED_KO_JSON_FILE_DOWNLOAD = UtilRestTestClass.EXPECTED_JSON_FOLDER + REST_SERVICE_PATH + "ficheroInstruccionesDownloadError.json";
	private static final String PAYMENT_INSTRUCTIONS_TO_UPLOAD = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + PAYMENT + "uploadPaymentInstructions.json";
	private static final String INSTRUCTIONS_TO_UPLOAD = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + "uploadInstructions.json";


	@Test
	public void testFicheroInstruccionesRestServiceTestFindInstructionsFileBySolicitudeTypeOk() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH + "" + SOLICITUDE_TYPE, EXPECTED_OK_JSON_FILE5);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestFindInstructionsFileByFormOk() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH + PAYMENT + FORM, EXPECTED_OK_JSON_FILE2);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestuploadInstructionsToPayOk() throws IOException {
		UtilRestTestClass.executeRestOKPostTest(ADMIN + INSTRUCTIONS_TO_PAY, PAYMENT_INSTRUCTIONS_TO_UPLOAD);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestuploadInstructionsToPayError() throws IOException {
		UtilRestTestClass.executeRestOKPostTest(ADMIN + INSTRUCTIONS_TO_PAY, PAYMENT_INSTRUCTIONS_TO_UPLOAD);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestuploadInstructionsOk() throws IOException {
		UtilRestTestClass.executeRestOKPostTest(ADMIN + SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH, INSTRUCTIONS_TO_UPLOAD);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestFindInstructionsListsOk() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(ADMIN + SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH + INSTRUCTIONS_LIST + USER, EXPECTED_OK_JSON_INSTRUCTIONS_LIST);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestError500() {
		UtilRestTestClass.executeRestErrorDownloadFileTest(SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH_ERROR500, EXPECTED_KO_JSON_FILE_DOWNLOAD);
	}

	@Test
	public void testFicheroInstruccionesRestServiceTestError404() {
		UtilRestTestClass.executeRestClientNotFound(SecosanConstants.SECURE_CONTEXT + "/" + REST_SERVICE_PATH_ERROR404);
	}

}
