package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class PaisCatalogRestServiceTest extends GenericRestTest {

	// Peticion
	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = SECURE + "/catalog";
	private static final String REST_SERVICE_PARAMS_PAIS = "/Pais";
	private static final String REST_SERVICE_REAL_PARAMS_AGREGACIONID = "/Pais/Agregacion_id/1";
	private static final String REST_SERVICE_REAL_PARAMS_PAISES_EU = "/Pais/paisesEU";
	private static final String REST_SERVICE_REAL_PARAMS_PAISES_NO_EU = "/Pais/paisesNoEU";
	private static final String CATALOGOS_JSON_FOLDER = "catalogos/";
	private static final String SOLICITUD_JSON_FOLDER = "pais/";

	// Expected JSON
	private static final String EXPECTED_JSON_TODOS_PAISES = "todosPaises.json";
	private static final String EXPECTED_JSON_AGREGACION_PAISES = "agregacionPaises.json";
	private static final String EXPECTED_JSON_PAISES_EU = "paisesUE.json";
	private static final String EXPECTED_JSON_PAISES_NO_EU = "paisesNoUE.json";

	// Test a ejecutar
	private static final String EXPECTED_OK_JSON_FILE_TODOS_PAISES = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + EXPECTED_JSON_TODOS_PAISES;
	private static final String EXPECTED_OK_JSON_FILE_AGREGACION_PAISES = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + EXPECTED_JSON_AGREGACION_PAISES;
	private static final String EXPECTED_OK_JSON_FILE_PAISES_EU = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + EXPECTED_JSON_PAISES_EU;
	private static final String EXPECTED_OK_JSON_FILE_PAISES_NO_EU = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + EXPECTED_JSON_PAISES_NO_EU;

	@Test
	public void testPaisesCatalogRestServiceTestTodosPaises() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_PARAMS_PAIS, EXPECTED_OK_JSON_FILE_TODOS_PAISES);
	}

	@Test
	public void testPaisesCatalogRestServiceTestAgregacionPais() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_REAL_PARAMS_AGREGACIONID, EXPECTED_OK_JSON_FILE_AGREGACION_PAISES);
	}

	@Test
	public void testPaisesCatalogRestServiceTestPaisesEU() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_REAL_PARAMS_PAISES_EU, EXPECTED_OK_JSON_FILE_PAISES_EU);
	}

	@Test
	public void testPaisesCatalogRestServiceTestPaisesNoEU() {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_REAL_PARAMS_PAISES_NO_EU, EXPECTED_OK_JSON_FILE_PAISES_NO_EU);
	}
}
