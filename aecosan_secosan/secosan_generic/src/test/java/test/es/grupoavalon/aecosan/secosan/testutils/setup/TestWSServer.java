package test.es.grupoavalon.aecosan.secosan.testutils.setup;

import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

public final class TestWSServer {

    private static final String TEST_URL = "http://localhost:9000/";

    private JaxWsServerFactoryBean factory;
    private Server cxfServer;

    public void serverCreate(Object webserviceImpl, Class<?> webserviceClassInterface, String servicePublicName) {
        factory = new JaxWsServerFactoryBean();
        // register WebService interface
        factory.setServiceClass(webserviceClassInterface);
        // publish the interface
        factory.setAddress(TEST_URL + servicePublicName);
        factory.setServiceBean(webserviceImpl);
        // create WebService instance
        cxfServer = factory.create();
        System.out.println("Server started at:" + factory.getAddress());
    }

    public void serverDestroy() {
        cxfServer.stop();
        System.out.println("Server stoped at:" + factory.getAddress());
    }

}
