package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.ComplementosAlimenticiosMock;
import es.grupoavalon.aecosan.secosan.dao.ComplementosAlimenticiosDAO;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;

/**
 * 
 * @author xi314616
 *
 */
public class ComplementosAlimenticiosDAOImplTest extends GenericDBTestClass {

	@Autowired
	private ComplementosAlimenticiosDAO complementosAlimenticiosDAO;

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;


	private static final Long ID_FIRST_CESECOMERCIALIZACION_ENTITY = 1001L;

	private static final Long ID_FIRST_PUESTA_MERCADO_ENTITY = 2007L;
	private static final Long ID_FIRST_MODIFICACION_DATOS_ENTITY = 3013L;
	
	private static final Long MODIFICACION_DATOS_TIPO_FORMULARIO_ID = 2l;
	private static final Long CESE_COMERCIALIZACION_TIPO_FORMULARIO_ID = 3L;
	private static final Long PUESTA_MERCADO_TIPO_FORMULARIO_ID = 1L;

	private static final Long USER_CREATOR = 1L;

	private CeseComercializacionCAEntity ceseComercializacionEntity;
	private PuestaMercadoCAEntity puestaMercadoEntity;
	private ModificacionDatosCAEntity modificacionDatosEntity;


	// --> INIT CeseComercializacionCAEntity <--

	/**
	 * Try to add new CeseComercializacionCAEntity
	 * 
	 */
	@Test
	public void testAddCeseComercializacion() {
		ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setUserCreator(generateUserCreatorMock());
		ceseComercializacionEntity.getFormularioEspecifico().setId(CESE_COMERCIALIZACION_TIPO_FORMULARIO_ID);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
		CeseComercializacionCAEntity ceseComercializacionEntityFind = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ceseComercializacionEntity.getId());
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind, ceseComercializacionEntity);

	}


	/**
	 * Try to update a CeseComercializacionCAEntity
	 * 
	 */
	@Test
	public void testUpdateCeseComercializacionProductName() {
		CeseComercializacionCAEntity ceseComercializacionEntityUpdate = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_ENTITY);
		ceseComercializacionEntityUpdate.setNombreComercialProducto(ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);
		complementosAlimenticiosDAO.updateCeseComercializacion(ceseComercializacionEntityUpdate);

		CeseComercializacionCAEntity ceseComercializacionEntityFind = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ceseComercializacionEntityUpdate.getId());
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getNombreComercialProducto(), ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);

	}


	/**
	 * Try to update a CeseComercializacionCAEntity
	 * 
	 */
	@Test
	public void testUpdateCeseComercializacionObservations() {
		CeseComercializacionCAEntity ceseComercializacionEntityUpdate = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_CESECOMERCIALIZACION_ENTITY);
		ceseComercializacionEntityUpdate.setObservaciones(ComplementosAlimenticiosMock.TEST_OBSERVACIONES);
		complementosAlimenticiosDAO.updateCeseComercializacion(ceseComercializacionEntityUpdate);

		CeseComercializacionCAEntity ceseComercializacionEntityFind = (CeseComercializacionCAEntity) solicitudDAO.findSolicitudById(ceseComercializacionEntityUpdate.getId());
		assertNotNull(ceseComercializacionEntityFind);
		assertEquals(ceseComercializacionEntityFind.getObservaciones(), ComplementosAlimenticiosMock.TEST_OBSERVACIONES);

	}

	/**
	 * Try to add new CeseComercializacionCAEntity when the object is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionNull() {
		complementosAlimenticiosDAO.addCeseComercializacion(null);
	}

	// --> INIT Required fields from SolicitudEntity <--

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field
	 * identificadorPeticion is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionIdentificadorPeticionNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setIdentificadorPeticion(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field
	 * FechaCreacion is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionFechaNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setFechaCreacion(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field
	 * formulario is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionFormularioNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setFormulario(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field
	 * Solicitante is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionSolicitanteNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setSolicitante(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field
	 * status is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionStatusNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setStatus(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	/**
	 * Try to add new CeseComercializacionCAEntity when The required field Area
	 * is null
	 * 
	 */
	@Test(expected = DaoException.class)
	public void testAddCeseComercializacionAreaNull() {
		CeseComercializacionCAEntity ceseComercializacionEntity = ComplementosAlimenticiosMock.generateCeseComercializacionEntity();
		ceseComercializacionEntity.setArea(null);
		complementosAlimenticiosDAO.addCeseComercializacion(ceseComercializacionEntity);
	}

	// --> END Required fields from SolicitudEntity <--

	// --> END Cesecomercialización <--

	// --> INIT PuestaMercadoCAEntity <--

	/**
	 * Try to add new PuestaMercadoCAEntity
	 * 
	 */
	@Test
	public void testAddPuestaMercado() {
		puestaMercadoEntity = ComplementosAlimenticiosMock.generatePuestaMercadoEntity();
		puestaMercadoEntity.setUserCreator(generateUserCreatorMock());
		puestaMercadoEntity.getFormularioEspecifico().setId(PUESTA_MERCADO_TIPO_FORMULARIO_ID);
		complementosAlimenticiosDAO.addPuestaMercado(puestaMercadoEntity);

		PuestaMercadoCAEntity puestaMercadoEntityFind = (PuestaMercadoCAEntity) solicitudDAO.findSolicitudById(puestaMercadoEntity.getId());

		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind, puestaMercadoEntity);

	}

	/**
	 * Try to update a PuestaMercadoCAEntity
	 * 
	 */
	@Test
	public void testUpdatePuestaMercado() {
		PuestaMercadoCAEntity puestaMercadoEntityUpdate = (PuestaMercadoCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_PUESTA_MERCADO_ENTITY);
		puestaMercadoEntityUpdate.setNombreComercialProducto(ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);
		complementosAlimenticiosDAO.updatePuestaMercado(puestaMercadoEntityUpdate);

		PuestaMercadoCAEntity puestaMercadoEntityFind = (PuestaMercadoCAEntity) solicitudDAO.findSolicitudById(puestaMercadoEntityUpdate.getId());

		assertNotNull(puestaMercadoEntityFind);
		assertEquals(puestaMercadoEntityFind.getNombreComercialProducto(), ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);

	}

	// --> END PuestaMercadoCAEntity <--

	// --> INIT ModificacionDatosCAEntity <--

	/**
	 * Try to add new ModificacionDatosCAEntity
	 * 
	 */
	@Test
	public void testAddModificacionDatosOk() {
		modificacionDatosEntity = ComplementosAlimenticiosMock.generateModificacionDatosEntity();
		modificacionDatosEntity.setUserCreator(generateUserCreatorMock());
		modificacionDatosEntity.getFormularioEspecifico().setId(MODIFICACION_DATOS_TIPO_FORMULARIO_ID);
		complementosAlimenticiosDAO.addModificacionDatos(modificacionDatosEntity);
		ModificacionDatosCAEntity modificacionDatosEntityFind = (ModificacionDatosCAEntity) solicitudDAO.findSolicitudById(modificacionDatosEntity.getId());
		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind, modificacionDatosEntity);
	}

	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioDAO.findCreatorById(USER_CREATOR);
	}

	/**
	 * Try to update a ModificacionDatosCAEntity
	 * 
	 */
	@Test
	public void testUpdateModificacionDatos() {
		ModificacionDatosCAEntity modificacionDatosEntityUpdate = (ModificacionDatosCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_MODIFICACION_DATOS_ENTITY);
		modificacionDatosEntityUpdate.setNombreComercialProducto(ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);
		complementosAlimenticiosDAO.updateModificacionDatos(modificacionDatosEntityUpdate);

		ModificacionDatosCAEntity modificacionDatosEntityFind = (ModificacionDatosCAEntity) solicitudDAO.findSolicitudById(ID_FIRST_MODIFICACION_DATOS_ENTITY);

		assertNotNull(modificacionDatosEntityFind);
		assertEquals(modificacionDatosEntityFind.getNombreComercialProducto(), ComplementosAlimenticiosMock.TEST_NOMBRE_PRODUCTO);

	}

	// --> END ModificacionDatosCAEntity <--


}
