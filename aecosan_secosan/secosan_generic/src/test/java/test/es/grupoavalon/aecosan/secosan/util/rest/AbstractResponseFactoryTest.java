package test.es.grupoavalon.aecosan.secosan.util.rest;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;

import org.junit.Test;

import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.util.rest.AbstractResponseFactory;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

public abstract class AbstractResponseFactoryTest extends GenericDBTestClass {

	
	@Test
	public void testGenerateErrorResponseDAOException(){
		DaoException testException = new DaoException("Test DAO exception");
		generateResponseBasedOnException(testException);
	}
	
	@Test
	public void testGenerateErrorResponseServiceException(){
		ServiceException testException = new ServiceException("Test Service exception");
		generateResponseBasedOnException(testException);
	}
	

	
	@Test
	public void testGenerateErrorResponseException(){
		Exception testException = new Exception("Test exception");
		generateResponseBasedOnException(testException);
	}
	
	
	private void generateResponseBasedOnException(Throwable testException){
		Response response = getResponseFactory().generateErrorResponse(testException);
		assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),response.getStatus());
//		assertErrorContent(response, testException);
	}
	
//	private void assertErrorContent(Response generatedResponse, Throwable t){
//		assertTrue(generatedResponse.getEntity() instanceof ErrorWrapper);
//		ErrorWrapper wrapper = (ErrorWrapper)generatedResponse.getEntity();
//		getLogger(ResponseFactoryTest.class).debug("Error Wrapper:"+wrapper);
//		assertEquals(t.getClass().getSimpleName(), wrapper.getError());
//		assertTrue(wrapper.getErrorDescription() != null);
//		
//	}
	
	@Test
	public void testGenerateOkGenericResponseNullAsEmpty(){
		Response response = getResponseFactory().generateOkGenericResponseNullAsEmpty(null);
		assertEquals("{}", response.getEntity().toString());
	}
	
	protected abstract AbstractResponseFactory getResponseFactory();
}
