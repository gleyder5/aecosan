package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.sql.Timestamp;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.FinanciacionAlimentosMock;
import es.grupoavalon.aecosan.secosan.dao.FinanciacionAlimentosDAO;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;

/**
 * 
 * @author xi314616
 *
 */
public class FinanciacionAlimentosDAOImplTest extends GenericDBTestClass {

	private static final Long ID_FIRST_BAJA_ENTITY = 1005L;
	private static final Long USER_CREATOR = 1L;
	private static final Long ALTER_FORMULARIO_ESPECIFICO = 12L;
	private static final Long INC_FORMULARIO_ESPECIFICO = 11L;
	private static final Long BAJA_FORMULARIO_ESPECIFICO = 13L;

	@Autowired
	private FinanciacionAlimentosDAO financiacionAlimentosDAO;

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	private IncOferAlimUMEEntity incOferAlimUMEEntity;
	private AlterOferAlimUMEEntity alterOferAlimUMEEntity;
	private BajaOferAlimUMEEntity bajaOferAlimUMEEntity;

	@Test
	public void testAddIncOferAlimUME() {
		incOferAlimUMEEntity = FinanciacionAlimentosMock.generateIncOferAlimUMEEntity();
		incOferAlimUMEEntity.setUserCreator(generateUserCreatorMock());
		incOferAlimUMEEntity.getFormularioEspecifico().setId(INC_FORMULARIO_ESPECIFICO);
		financiacionAlimentosDAO.addIncOferAlimUME(incOferAlimUMEEntity);
		IncOferAlimUMEEntity incOferAlimUMEEntityFind = (IncOferAlimUMEEntity) solicitudDAO.findSolicitudById(incOferAlimUMEEntity.getId());

		assertNotNull(incOferAlimUMEEntityFind);
		assertEquals(incOferAlimUMEEntityFind, incOferAlimUMEEntity);

	}


	@Test
	public void testAddAlterOferAlimUME() {
		alterOferAlimUMEEntity = FinanciacionAlimentosMock.generateAlterOferAlimUMEEntity();
		alterOferAlimUMEEntity.getFormularioEspecifico().setId(ALTER_FORMULARIO_ESPECIFICO);
		alterOferAlimUMEEntity.setUserCreator(generateUserCreatorMock());
		financiacionAlimentosDAO.addAlterOferAlimUME(alterOferAlimUMEEntity);
		AlterOferAlimUMEEntity alterOferAlimUMEEntityFind = (AlterOferAlimUMEEntity) solicitudDAO.findSolicitudById(alterOferAlimUMEEntity.getId());
		assertNotNull(alterOferAlimUMEEntityFind);
		assertEquals(alterOferAlimUMEEntityFind, alterOferAlimUMEEntity);

	}

	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioDAO.findCreatorById(USER_CREATOR);
	}

	@Test
	public void testAddBajaOferAlimUME() {
		bajaOferAlimUMEEntity = FinanciacionAlimentosMock.generateBajaOferAlimUMEEntity();
		bajaOferAlimUMEEntity.setUserCreator(generateUserCreatorMock());
		bajaOferAlimUMEEntity.getFormularioEspecifico().setId(BAJA_FORMULARIO_ESPECIFICO);
		financiacionAlimentosDAO.addBajaOferAlimUME(bajaOferAlimUMEEntity);
		BajaOferAlimUMEEntity bajaOferAlimUMEEntityFind = (BajaOferAlimUMEEntity) solicitudDAO.findSolicitudById(bajaOferAlimUMEEntity.getId());
		assertNotNull(bajaOferAlimUMEEntityFind);
		assertEquals(bajaOferAlimUMEEntityFind, bajaOferAlimUMEEntity);
	}

	@Test
	public void testupdateBajaOferAlimUMESuspensionReasonDurationOK() {
		bajaOferAlimUMEEntity = (BajaOferAlimUMEEntity) solicitudDAO.findSolicitudById(ID_FIRST_BAJA_ENTITY);
		assertNotNull(bajaOferAlimUMEEntity);
		assertEquals(bajaOferAlimUMEEntity.getSuspensionReasonDuration(), FinanciacionAlimentosMock.CANCELLATION_REASON);

		bajaOferAlimUMEEntity.setSuspensionReasonDuration(FinanciacionAlimentosMock.CANCELLATION_REASON_UPDATE);
		financiacionAlimentosDAO.updateBajaOferAlimUME(bajaOferAlimUMEEntity);
		bajaOferAlimUMEEntity = (BajaOferAlimUMEEntity) solicitudDAO.findSolicitudById(ID_FIRST_BAJA_ENTITY);
		assertNotNull(bajaOferAlimUMEEntity);
		assertEquals(bajaOferAlimUMEEntity.getSuspensionReasonDuration(), FinanciacionAlimentosMock.CANCELLATION_REASON_UPDATE);
	}

	@Test
	public void testupdateBajaOferAlimUMECommunicationDateOK() {
		Timestamp communicationDate = FinanciacionAlimentosMock.generateCommunicationDate();
		bajaOferAlimUMEEntity = (BajaOferAlimUMEEntity) solicitudDAO.findSolicitudById(ID_FIRST_BAJA_ENTITY);
		assertNotNull(bajaOferAlimUMEEntity);
		assertFalse(bajaOferAlimUMEEntity.getCommunicationDate().equals(communicationDate));

		bajaOferAlimUMEEntity.setCommunicationDate(communicationDate);
		financiacionAlimentosDAO.updateBajaOferAlimUME(bajaOferAlimUMEEntity);
		bajaOferAlimUMEEntity = (BajaOferAlimUMEEntity) solicitudDAO.findSolicitudById(ID_FIRST_BAJA_ENTITY);
		assertNotNull(bajaOferAlimUMEEntity);
		assertEquals(bajaOferAlimUMEEntity.getCommunicationDate(), communicationDate);
	}

}
