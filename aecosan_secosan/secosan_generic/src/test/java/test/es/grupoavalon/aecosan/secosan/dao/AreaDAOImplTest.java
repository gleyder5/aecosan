package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.AreaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class AreaDAOImplTest extends GenericDBTestClass {

	@Autowired
	private AreaDAO areaDAO;

	private static final Long AREA_ID_1 = 1l;
	private static final Long AREA_ID_NOT_EXIST = 555552l;
	private static final String AREA1_DESCRIPCION = "RGSEAA";

	@Test
	public void testAreaFindByIdOK() {
		AreaEntity area = areaDAO.findArea(AREA_ID_1);
		assertEquals(area.getCatalogValue(), AREA1_DESCRIPCION);
	}

	@Test
	public void testAreaFindByIdNotExist() {
		AreaEntity area = areaDAO.findArea(AREA_ID_NOT_EXIST);
		assertNull(area);
	}

	@Test(expected = DaoException.class)
	public void testAreaFindByIdNull() {
		areaDAO.findArea(null);
	}

}
