package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.DocumentacionDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.DocumentacionEntity;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class DocumentacionDAOImplTest extends GenericDBTestClass {

	@Autowired
	private DocumentacionDAO DocumentacionDAO;

	private static final Long ID_DOCUMENTATION_TO_DELETE = 103L;
	private static final String NOMBRE_DOCUMENTO_TO_DELETE = "Documento de pruebas 4";

	/**
	 * Try to find a DocumentacionEntity by ID
	 * 
	 */
	@Test
	public void testFindDocumentacionById() {
		DocumentacionEntity DocumentacionEntityFind;

		DocumentacionEntityFind = DocumentacionDAO.findDocumentacionById(ID_DOCUMENTATION_TO_DELETE);
		assertNotNull(DocumentacionEntityFind);
		assertEquals(DocumentacionEntityFind.getDocumentName(), NOMBRE_DOCUMENTO_TO_DELETE);
	}

	/**
	 * Try to delete a DocumentacionEntity by ID
	 * 
	 */
	@Test
	public void testDeleteDocumentacionById() {
		DocumentacionEntity DocumentacionEntityFind;

		DocumentacionEntityFind = DocumentacionDAO.findDocumentacionById(ID_DOCUMENTATION_TO_DELETE);
		assertNotNull(DocumentacionEntityFind);
		assertEquals(DocumentacionEntityFind.getDocumentName(), NOMBRE_DOCUMENTO_TO_DELETE);

		DocumentacionDAO.deleteDocumentacion(ID_DOCUMENTATION_TO_DELETE);
		DocumentacionEntityFind = DocumentacionDAO.findDocumentacionById(ID_DOCUMENTATION_TO_DELETE);

		assertNull(DocumentacionEntityFind);

	}

}
