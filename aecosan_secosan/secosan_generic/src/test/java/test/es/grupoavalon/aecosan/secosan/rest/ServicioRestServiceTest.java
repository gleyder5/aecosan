package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author xi314626
 *
 */

public class ServicioRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = SECURE + "servicios/";
	private static final String REST_SERVICE_PATH_ERROR404 = "fake/5/FAKE";
	private static final String REST_SERVICE_OK_PARAMS = "1";
	private static final String EXPECTED_OK_JSON_FILE5 = UtilRestTestClass.EXPECTED_JSON_FOLDER + "findServicesByPayTypeId1.json";


	@Test
	public void testServicioRestServiceTestFindByFormularioOk() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + REST_SERVICE_OK_PARAMS, EXPECTED_OK_JSON_FILE5);
	}

	@Test
	public void testServicioRestServiceTestError404() {
		UtilRestTestClass.executeRestClientNotFound(REST_SERVICE_PATH_ERROR404);
	}

}
