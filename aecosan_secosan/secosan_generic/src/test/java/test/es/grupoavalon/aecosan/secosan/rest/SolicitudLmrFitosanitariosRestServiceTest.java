package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

public class SolicitudLmrFitosanitariosRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "solicitudLmrFitosanitarios/";
	private static final String REST_SERVICE_LMR_FITOSANITARIOS = "lmrFitosanitarios/";
	private static final String REST_SERVICE_USER = "1";
	private static final String SENDED_JSON_FILE_LMR_FITOSANITARIOS_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH  +  "solicitudLmrFitosanitariosAddOk.json";
	private static final String SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_ACCOUNT_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH
			+ "solicitudLmrFitosanitariosAddPayAccountPaymentOk.json";
	private static final String SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_ACCOUNT_ONLINE_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH
			+ "solicitudLmrFitosanitariosAddPaymentByAccountOnlineOk.json";
	private static final String SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_CREDITCARD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH
			+ "solicitudLmrFitosanitariosAddPaymentByCreditCardOk.json";

	private static final String SENDED_JSON_FILE_LMR_FITOSANITARIOS_UPDATE_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH +  "solicitudLmrFitosanitariosUpdateOk.json";
	
	@Test
	public void testSolicitudLmrFitosanitariosRestServiceTestAddLmrFitosanitariosOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LMR_FITOSANITARIOS + REST_SERVICE_USER, SENDED_JSON_FILE_LMR_FITOSANITARIOS_OK);
		UtilRestTestClass.assertReponseOk(response);
	}
	
	@Test
	public void testSolicitudLmrFitosanitariosRestServiceTestAddLmrFitosanitariosPayByAccountOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LMR_FITOSANITARIOS + REST_SERVICE_USER,
				SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_ACCOUNT_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testSolicitudLmrFitosanitariosRestServiceTestAddLmrFitosanitariosPayByAccountOnlineOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LMR_FITOSANITARIOS + REST_SERVICE_USER,
				SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_ACCOUNT_ONLINE_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testSolicitudLmrFitosanitariosRestServiceTestAddLmrFitosanitariosPayByCreditCardOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LMR_FITOSANITARIOS + REST_SERVICE_USER,
				SENDED_JSON_FILE_LMR_FITOSANITARIOS_ADD_PAYMENT_BY_CREDITCARD_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testSolicitudLmrFitosanitariosRestServiceTestUpdateLmrFitosanitariosOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LMR_FITOSANITARIOS + REST_SERVICE_USER, SENDED_JSON_FILE_LMR_FITOSANITARIOS_UPDATE_OK);
		UtilRestTestClass.assertReponseOk(response);
	}
	
}
