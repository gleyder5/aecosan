package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

public class SolicitudLogoRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "solicitudLogo/";
	private static final String REST_SERVICE_LOGO = "logo/";
	private static final String REST_SERVICE_USER = "1";
	private static final String SENDED_JSON_FILE_LOGO_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH  +  "solicitudLogoAddOk.json";
	private static final String SENDED_JSON_FILE_LOGO_UPDATE_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH +  "solicitudLogoUpdateOk.json";
	
	@Test
	public void testSolicitudLogoRestServiceTestAddLogoOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LOGO + REST_SERVICE_USER, SENDED_JSON_FILE_LOGO_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testSolicitudLogoRestServiceTestUpdateLogoOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_LOGO + REST_SERVICE_USER, SENDED_JSON_FILE_LOGO_UPDATE_OK);
		UtilRestTestClass.assertReponseOk(response);
	}
	
}
