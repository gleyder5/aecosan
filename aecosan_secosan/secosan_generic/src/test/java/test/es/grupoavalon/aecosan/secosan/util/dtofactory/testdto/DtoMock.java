package test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto;

import java.math.BigDecimal;
import java.util.List;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.Base64TobytesArrayTransformer;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.FloatToStringTransformer;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.LongToDateTransformer;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.NumberToBigDecimalTransformer;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.transformer.PrimitiveToStringTransformer;

public class DtoMock {

	@BeanToBeanMapping(getValueFrom = "myStringAtribute")
	private String myDtoStringAtribute;
	@BeanToBeanMapping(getValueFrom = "myIntValue")
	private int myDtoIntValue;
	@IgnoreField(inReverse = true)
	private Integer myDtoIntegerValueIgnore;
	@BeanToBeanMapping(getValueFrom = "myFloatPrimitiveValue")
	private float myDtoFloatPrimitiveValue;
	@BeanToBeanMapping(getValueFrom = "myFloatValue")
	private Float myDtoFloatValue;
	@BeanToBeanMapping(getValueFrom = "myBoolValue")
	private boolean myDtoBoolValue;
	@BeanToBeanMapping(getValueFrom = "myBooleanValue")
	private Boolean myDtoBooleanValue;
	@BeanToBeanMapping(getValueFrom = "myManyToOneRelationship", getSecondValueFrom = "stringAtribute")
	private String stringAtributeFromOther;
	@BeanToBeanMapping(getValueFrom = "myManyToOneRelationship", getSecondValueFrom = "intValue")
	private int intValueFromOther;
	@BeanToBeanMapping(getValueFrom = "myManyToOneRelationship", getSecondValueFrom = "boolVal")
	private boolean boolValFromOther;
	@BeanToBeanMapping(getValueFrom = "myOneToManyRelationship", generateListOf = DtoRelatedMock.class, generateListFrom = EntityRelatedMock.class)
	private List<DtoRelatedMock> myRelatedMock;
	@BeanToBeanMapping(getValueFrom = "myFloatValue", useCustomTransformer = NumberToBigDecimalTransformer.class)
	private BigDecimal useTransformer;
	@BeanToBeanMapping(getValueFrom = "myIntValue")
	private Integer myDtoIntegerValue;
	@BeanToBeanMapping
	private String haveSameNameString;
	@BeanToBeanMapping(useCustomTransformer = PrimitiveToStringTransformer.class)
	private String haveSameNameInt;
	@BeanToBeanMapping(generateOther = true, getValueFrom = "myOtherToOneRelationship")
	private DtoRelatedMock nyOtherRelationship;
	@BeanToBeanMapping(getValueFrom = "myDateToLong", useCustomTransformer = LongToDateTransformer.class)
	private Long myLongToDate;
	@BeanToBeanMapping(getValueFrom = "mySQLDateToLong", useCustomTransformer = LongToDateTransformer.class)
	private Long myLongToSQLDate;
	@BeanToBeanMapping(getValueFrom = "mytimeStampToLong", useCustomTransformer = LongToDateTransformer.class)
	private Long myTimestampToDate;
	@BeanToBeanMapping(getValueFrom = "myFLoatString", useCustomTransformer = FloatToStringTransformer.class)
	private Float myDtoFloatStringValue;
	@BeanToBeanMapping(getValueFrom = "myIntValue", useCustomTransformer = NumberToBigDecimalTransformer.class)
	private BigDecimal useTransformerInt;
	@BeanToBeanMapping(getValueFrom = "myDoubleValue", useCustomTransformer = NumberToBigDecimalTransformer.class)
	private BigDecimal useTransformerDouble;

	@BeanToBeanMapping(getValueFrom = "base64ByteValue", useCustomTransformer = Base64TobytesArrayTransformer.class)
	private String base64FromByte;

	public Integer getMyDtoIntegerValueIgnore() {
		return myDtoIntegerValueIgnore;
	}

	public void setMyDtoIntegerValueIgnore(Integer myDtoIntegerValueIgnore) {
		this.myDtoIntegerValueIgnore = myDtoIntegerValueIgnore;
	}

	public Integer getMyDtoIntegerValue() {
		return myDtoIntegerValue;
	}

	public void setMyDtoIntegerValue(Integer myDtoIntegerValue) {
		this.myDtoIntegerValue = myDtoIntegerValue;
	}

	public List<DtoRelatedMock> getMyRelatedMock() {
		return myRelatedMock;
	}

	public void setMyRelatedMock(List<DtoRelatedMock> myRelatedMock) {
		this.myRelatedMock = myRelatedMock;
	}

	public String getMyDtoStringAtribute() {
		return myDtoStringAtribute;
	}

	public void setMyDtoStringAtribute(String myDtoStringAtribute) {
		this.myDtoStringAtribute = myDtoStringAtribute;
	}

	public int getMyDtoIntValue() {
		return myDtoIntValue;
	}

	public void setMyDtoIntValue(int myDtoIntValue) {
		this.myDtoIntValue = myDtoIntValue;
	}

	public float getMyDtoFloatPrimitiveValue() {
		return myDtoFloatPrimitiveValue;
	}

	public void setMyDtoFloatPrimitiveValue(float myDtoFloatPrimitiveValue) {
		this.myDtoFloatPrimitiveValue = myDtoFloatPrimitiveValue;
	}

	public Float getMyDtoFloatValue() {
		return myDtoFloatValue;
	}

	public void setMyDtoFloatValue(Float myDtoFloatValue) {
		this.myDtoFloatValue = myDtoFloatValue;
	}

	public boolean isMyDtoBoolValue() {
		return myDtoBoolValue;
	}

	public void setMyDtoBoolValue(boolean myDtoBoolValue) {
		this.myDtoBoolValue = myDtoBoolValue;
	}

	public Boolean getMyDtoBooleanValue() {
		return myDtoBooleanValue;
	}

	public void setMyDtoBooleanValue(Boolean myDtoBooleanValue) {
		this.myDtoBooleanValue = myDtoBooleanValue;
	}

	public String getStringAtributeFromOther() {
		return stringAtributeFromOther;
	}

	public void setStringAtributeFromOther(String stringAtributeFromOther) {
		this.stringAtributeFromOther = stringAtributeFromOther;
	}

	public int getIntValueFromOther() {
		return intValueFromOther;
	}

	public void setIntValueFromOther(int intValueFromOther) {
		this.intValueFromOther = intValueFromOther;
	}

	public boolean isBoolValFromOther() {
		return boolValFromOther;
	}

	public void setBoolValFromOther(boolean boolValFromOther) {
		this.boolValFromOther = boolValFromOther;
	}

	public BigDecimal getUseTransformer() {
		return useTransformer;
	}

	public void setUseTransformer(BigDecimal useTransformer) {
		this.useTransformer = useTransformer;
	}

	public String getHaveSameNameString() {
		return haveSameNameString;
	}

	public void setHaveSameNameString(String haveSameNameString) {
		this.haveSameNameString = haveSameNameString;
	}

	public String getHaveSameNameInt() {
		return haveSameNameInt;
	}

	public void setHaveSameNameInt(String haveSameNameInt) {
		this.haveSameNameInt = haveSameNameInt;
	}

	public DtoRelatedMock getNyOtherRelationship() {
		return nyOtherRelationship;
	}

	public void setNyOtherRelationship(DtoRelatedMock nyOtherRelationship) {
		this.nyOtherRelationship = nyOtherRelationship;
	}

	public Long getMyLongToDate() {
		return myLongToDate;
	}

	public void setMyLongToDate(Long myLongToDate) {
		this.myLongToDate = myLongToDate;
	}

	public Long getMyLongToSQLDate() {
		return myLongToSQLDate;
	}

	public void setMyLongToSQLDate(Long myLongToSQLDate) {
		this.myLongToSQLDate = myLongToSQLDate;
	}

	public Long getMyTimestampToDate() {
		return myTimestampToDate;
	}

	public void setMyTimestampToDate(Long myTimestampToDate) {
		this.myTimestampToDate = myTimestampToDate;
	}

	public Float getMyDtoFloatStringValue() {
		return myDtoFloatStringValue;
	}

	public void setMyDtoFloatStringValue(Float myDtoFloatStringValue) {
		this.myDtoFloatStringValue = myDtoFloatStringValue;
	}

	public BigDecimal getUseTransformerInt() {
		return useTransformerInt;
	}

	public void setUseTransformerInt(BigDecimal useTransformerInt) {
		this.useTransformerInt = useTransformerInt;
	}

	public BigDecimal getUseTransformerDouble() {
		return useTransformerDouble;
	}

	public void setUseTransformerDouble(BigDecimal useTransformerDouble) {
		this.useTransformerDouble = useTransformerDouble;
	}


	public String getBase64FromByte() {
		return base64FromByte;
	}

	public void setBase64FromByte(String base64FromByte) {
		this.base64FromByte = base64FromByte;
	}


	@Override
	public String toString() {
		return "DtoMock [myDtoStringAtribute=" + myDtoStringAtribute + ", myDtoIntValue=" + myDtoIntValue + ", myDtoIntegerValueIgnore=" + myDtoIntegerValueIgnore + ", myDtoFloatPrimitiveValue="
				+ myDtoFloatPrimitiveValue + ", myDtoFloatValue=" + myDtoFloatValue + ", myDtoBoolValue=" + myDtoBoolValue + ", myDtoBooleanValue=" + myDtoBooleanValue + ", stringAtributeFromOther="
				+ stringAtributeFromOther + ", intValueFromOther=" + intValueFromOther + ", boolValFromOther=" + boolValFromOther + ", myRelatedMock=" + myRelatedMock + ", useTransformer="
				+ useTransformer + ", myDtoIntegerValue=" + myDtoIntegerValue + ", haveSameNameString=" + haveSameNameString + ", haveSameNameInt=" + haveSameNameInt + ", nyOtherRelationship="
				+ nyOtherRelationship + ", myLongToDate=" + myLongToDate + ", myLongToSQLDate=" + myLongToSQLDate + ", myTimestampToDate=" + myTimestampToDate + ", myDtoFloatStringValue="
				+ myDtoFloatStringValue + ", useTransformerInt=" + useTransformerInt + ", useTransformerDouble=" + useTransformerDouble
				+ ", base64FromByte=" + base64FromByte + "]";
	}



}
