package test.es.grupoavalon.aecosan.secosan.util.dtofactory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;

import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.BeanFromBeanFactory;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.DtoMock;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.DtoNotAnnotated;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.DtoRelatedMock;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.EntityMock;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.EntityRelatedMock;

public class BeanFromBeanFactoryTest extends GenericDtoFactoryTest {

	@Test
	public void testGetInstance() {

		BeanFromBeanFactory<DtoMock> factory = getFactoryForListTransformation();
		assertNotNull(factory);
	}

	@Test
	public void testGetInstanceDTOEntity() {

		EntityMock mockEntity = fillEntity();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForOneToOneTransformation(mockEntity);
		assertNotNull(factory);
	}

	@Test
	public void testGenerateDTOFromEntity() {
		EntityMock mockEntity = fillEntity();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForOneToOneTransformation(mockEntity);
		DtoMock filledObject = factory.generateDTOFromEntity();
		getLogger().debug("DTO:" + filledObject);

		assertMockDTOHaveCorrectValuesFromMockEntity(filledObject, mockEntity, true);

	}

	@Test
	public void testGenerateDTOFromEntityNoCollections() {
		EntityMock mockEntity = fillEntity();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForOneToOneTransformation(mockEntity);
		DtoMock filledObject = factory.generateDTOFromEntityNoCollections();
		getLogger().debug("DTO:" + filledObject);

		assertMockDTOHaveCorrectValuesFromMockEntity(filledObject, mockEntity, false);

	}

	@Test
	public void testGenerateDTOFromEntityOnllyCollections() {
		EntityMock mockEntity = fillEntity();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForOneToOneTransformation(mockEntity);
		DtoMock filledObject = factory.generateDTOFromEntityOnllyCollections();

		assertMockDTOHaveCorrectValuesOnlyCollectionsFromMockEntity(filledObject, mockEntity);

	}

	private static BeanFromBeanFactory<DtoMock> getFactoryForOneToOneTransformation(EntityMock mockEntity) {

		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoMock> factory = (BeanFromBeanFactory<DtoMock>) BeanFromBeanFactory.getInstance(mockEntity, DtoMock.class);
		return factory;
	}

	@Test
	public void testGenerateListDTOFromEntity() {
		List<EntityMock> entityList = fillEntityList();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForListTransformation();
		List<DtoMock> dtoList = factory.generateListDTOFromEntity(entityList);
		assertAllMockDTOHaveCorrectValuesFormListMockEntity(dtoList, entityList, true);
	}

	@Test
	public void testGenerateListDTOFromEntityNoCollections() {
		List<EntityMock> entityList = fillEntityList();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForListTransformation();
		List<DtoMock> dtoList = factory.generateListDTOFromEntityNoCollections(entityList);
		assertAllMockDTOHaveCorrectValuesFormListMockEntity(dtoList, entityList, false);
	}

	private static BeanFromBeanFactory<DtoMock> getFactoryForListTransformation() {
		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoMock> factory = (BeanFromBeanFactory<DtoMock>) BeanFromBeanFactory.getInstance(DtoMock.class);
		return factory;
	}

	@Test
	public void testGenerateListDTOFromEntityOnlyCollections() {
		List<EntityMock> entityList = fillEntityList();
		BeanFromBeanFactory<DtoMock> factory = getFactoryForListTransformation();
		List<DtoMock> dtoList = factory.generateListDTOFromEntityOnlyCollections(entityList);

		assertAllMockDTOHaveCorrectValuesFormListMockEntityCollectionOnly(dtoList, entityList);
	}

	private static void assertAllMockDTOHaveCorrectValuesFormListMockEntity(List<DtoMock> dots, List<EntityMock> entities, boolean testCollections) {

		assertListNotNullOrNotEmpty(dots);
		assertEquals(entities.size(), dots.size());

		for (int i = 0; i < dots.size(); i++) {
			DtoMock dto = dots.get(i);
			EntityMock entityMock = entities.get(i);
			assertMockDTOHaveCorrectValuesFromMockEntity(dto, entityMock, testCollections);
		}
	}

	private static void assertAllDtoRelatedHaveCorrectValuesFromEntityRelatedMock(DtoMock dto, EntityMock entityMock) {

		for (int i = 0; i < dto.getMyRelatedMock().size(); i++) {
			DtoRelatedMock relatedMock = dto.getMyRelatedMock().get(i);
			EntityRelatedMock entityRelated = entityMock.getMyOneToManyRelationship().get(i);
			assertDtoRelatedHaveCorrectValuesFromEntityRelatedMock(relatedMock, entityRelated);
		}
	}

	private static void assertMockDTOHaveCorrectValuesFromMockEntity(DtoMock dto, EntityMock entityMock, boolean testCollections) {
		final Logger logger = getLogger(BeanFromBeanFactoryTest.class);
		assertNotNull(dto);

		if (!dto.getMyDtoStringAtribute().equals(entityMock.getMyStringAtribute())) {
			fail("String atributes are not equals MyDtoStringAtribute-MyStringAtribute");
		}

		if (dto.getMyDtoIntValue() != entityMock.getMyIntValue()) {
			fail("int atributes are not equals MyDtoIntAtribute-MyIntAtribute");
		}

		if (dto.getMyDtoIntegerValueIgnore() != null) {
			fail("IntegerValueIgnore should be null");
		}

		if (dto.getMyDtoFloatPrimitiveValue() != entityMock.getMyFloatPrimitiveValue()) {
			fail("float primitives atributes are not equals MyDtoFloatPrimitiveAtribute-MyFloatAtributeAtribute");
		}

		if (!dto.getMyDtoFloatValue().equals(entityMock.getMyFloatValue())) {
			fail("float  atributes are not equals MyDtoFloatAtribute-MyFloatValue");
		}

		if (dto.isMyDtoBoolValue() != entityMock.isMyBoolValue()) {
			logger.debug("DTO Bool Value:" + dto.isMyDtoBoolValue() + " entityMock bool value " + entityMock.isMyBoolValue());
			fail("Boolean primitives atributes are not equals MyDtoBoolAtribute-MyBoolValue");
		}
		if ((dto.getMyDtoBooleanValue().compareTo(entityMock.getMyBooleanValue()) != 0)) {
			logger.debug("DTO Boolean Value:" + dto.getMyDtoBooleanValue() + " entityMock Boolean value " + entityMock.getMyBooleanValue());
			fail("Boolean atributes are not equals MyDtoBooleanAtribute-MyBooleanValue");
		}

		if (!dto.getStringAtributeFromOther().equals(entityMock.getMyManyToOneRelationship().getStringAtribute())) {
			fail("String from other entity atribute are not equals stringAtributeFromOther-myManyToOneRelationship.stringAtribute");
		}

		if (dto.getIntValueFromOther() != entityMock.getMyManyToOneRelationship().getIntValue()) {
			fail("int from other entity atribute are not equal intValueFromOther-myManyToOneRelationship.intValue");
		}

		if (dto.isBoolValFromOther() != entityMock.getMyManyToOneRelationship().isBoolVal()) {
			fail("boolean from other entity atribute are not equals boolValFromOther-myManyToOneRelationship.boolVal");
		}

		if (testCollections && dto.getMyRelatedMock().size() != entityMock.getMyOneToManyRelationship().size()) {
			fail("related mock List size  is not equal to myManyToOneRelationship");
		} else if (testCollections) {

			assertAllDtoRelatedHaveCorrectValuesFromEntityRelatedMock(dto, entityMock);
		} else {
			assertListNullOrEmpty((dto.getMyRelatedMock()));
		}

		if (dto.getUseTransformer().floatValue() != entityMock.getMyFloatValue()) {
			fail("Transformed value is not equal to FloatValue");
		}

		if (dto.getMyDtoIntegerValue().intValue() != entityMock.getMyIntValue()) {
			fail("Integer atributes are not equals MyDtoIntegerValue-MyIntValue");
		}

		if (!dto.getHaveSameNameString().equals(entityMock.getHaveSameNameString())) {
			fail("String  atributes with same names are not equals haveSameNameString");
		}

		if (!dto.getHaveSameNameInt().equals(Integer.toString(entityMock.getHaveSameNameInt()))) {
			fail("Int  atributes with same names are not equals haveSameNameInt");
		}
		assertDtoRelatedHaveCorrectValuesFromEntityRelatedMock(dto.getNyOtherRelationship(), entityMock.getMyOtherToOneRelationship());

	}

	private static void assertDtoRelatedHaveCorrectValuesFromEntityRelatedMock(DtoRelatedMock relatedMock, EntityRelatedMock entityRelated) {
		assertNotNull(relatedMock);

		if (!relatedMock.getStringAtribute().equals(entityRelated.getStringAtribute())) {
			fail("Related String values  are not equals DtoRelatedMock.stringAttribute-EntityRelatedMock.stringAttribute");
		}

		if (!relatedMock.getIntValue().equals(Integer.toString(entityRelated.getIntValue()))) {

			fail("Related int values  are not equals DtoRelatedMock.intValue-EntityRelatedMock.intValue");
		}

		if (!relatedMock.getBoolVal().equals(Boolean.toString(entityRelated.isBoolVal()))) {
			fail("Related boolean values  are not equals DtoRelatedMock.boolVal-EntityRelatedMock.boolVal");
		}

		if (!relatedMock.getHaveSameNameStringRelated().equals(entityRelated.getHaveSameNameStringRelated())) {
			fail("Related String values with same name  are not equals haveSameNameString ");
		}

		if (relatedMock.getHaveSameNameIntRelated() != entityRelated.getHaveSameNameIntRelated()) {
			fail("Related int values with same name  are not equals haveSameNameInt ");
		}
	}

	private static void assertAllMockDTOHaveCorrectValuesFormListMockEntityCollectionOnly(List<DtoMock> dots, List<EntityMock> entities) {

		assertListNotNullOrNotEmpty(dots);
		assertEquals(entities.size(), dots.size());

		for (int i = 0; i < dots.size(); i++) {
			DtoMock dto = dots.get(i);
			EntityMock entityMock = entities.get(i);
			assertMockDTOHaveCorrectValuesOnlyCollectionsFromMockEntity(dto, entityMock);
		}
	}

	private static void assertMockDTOHaveCorrectValuesOnlyCollectionsFromMockEntity(DtoMock dto, EntityMock entityMock) {
		final Logger logger = getLogger(BeanFromBeanFactoryTest.class);

		assertNotNull(dto);

		if (dto.getMyDtoStringAtribute() != null) {
			fail("String atribute should be null MyDtoStringAtribute");
		}

		if (dto.getMyDtoIntValue() != 0) {
			fail("int atribute should be 0  MyDtoIntAtribute");
		}

		if (dto.getMyDtoIntegerValueIgnore() != null) {
			fail("IntegerValueIgnore should be null");
		}

		if (dto.getMyDtoFloatPrimitiveValue() != 0.0) {
			fail("float primitives should be 0.0 MyDtoFloatPrimitiveAtribute");
		}

		if (dto.getMyDtoFloatValue() != null) {
			fail("Float  atribute should be null MyDtoFloatAtribute");
		}

		if (dto.isMyDtoBoolValue()) {
			logger.debug("DTO Bool Value:" + dto.isMyDtoBoolValue());
			fail("Boolean primitive atribute should be false  MyDtoBoolAtribute");
		}
		if (dto.getMyDtoBooleanValue() != null) {
			logger.debug("DTO Boolean Value:" + dto.getMyDtoBooleanValue());
			fail("Boolean atribute should be null  MyDtoBooleanAtribute");
		}

		if (dto.getStringAtributeFromOther() != null) {
			fail("String from other entity should be null  stringAtributeFromOther");
		}

		if (dto.getIntValueFromOther() != 0) {
			fail("int from other entity atribute should be 0 intValueFromOther");
		}

		if (dto.isBoolValFromOther()) {
			fail("boolean should be false boolValFromOther-myManyToOneRelationship.boolVal");
		}

		if (dto.getMyRelatedMock().size() != entityMock.getMyOneToManyRelationship().size()) {
			fail("related mock List size  is not equal to myManyToOneRelationship");
		} else {

			assertAllDtoRelatedHaveCorrectValuesFromEntityRelatedMock(dto, entityMock);
		}

		if (dto.getUseTransformer() != null) {
			fail("Transformed value should be null ");
		}

		if (dto.getMyDtoIntegerValue() != null) {
			fail("Integer atribute should be null MyDtoIntegerValue");
		}
	}

	@Test(expected = BeanFactoryException.class)
	public void testDTONotAnnotated() {
		EntityMock mockEntity = fillEntity();
		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoNotAnnotated> factory = (BeanFromBeanFactory<DtoNotAnnotated>) BeanFromBeanFactory.getInstance(mockEntity, DtoNotAnnotated.class);
		;
		factory.generateDTOFromEntity();
	}

	@Test(expected = BeanFactoryException.class)
	public void testDTONotAnnotatedList() {
		List<EntityMock> mockEntity = fillEntityList();
		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoNotAnnotated> factory = (BeanFromBeanFactory<DtoNotAnnotated>) BeanFromBeanFactory.getInstance(DtoNotAnnotated.class);
		;
		factory.generateListDTOFromEntity(mockEntity);
	}

}
