package test.es.grupoavalon.aecosan.secosan.testutils.setup;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

public final class TestDBSetup{

	private static final Logger logger = LoggerFactory
			.getLogger(TestDBSetup.class);

	public static final String DEFAULT_SCRIPT_XML = "/testdata/Dataset.xml";
	
	private DataSource dataSource;

	private String[] databaseScripts;

	public void setUpDB() {

		try {
;
			logger.debug("Importing data to Test DB");
			insertData();
		} catch (Exception e) {
			logger.error("Can not set Test Data", e);
			throw new RuntimeException("Can not set Test Data", e);
		}

	}

	public void setUpDBUsingDataset(String dataset) {
		try {
			insertDataUsingDatasetName(dataset);
		} catch (Exception e) {
			logger.error("Can not set Test Data", e);
			throw new RuntimeException("Can not set Test Data", e);
		}

	}

	private void insertDataUsingDatasetName(String datasetName)
			throws IOException, DatabaseUnitException, SQLException {
		IDatabaseConnection con = this.getConnection();
		IDataSet dataSet = getDatasetFromClasspath(datasetName);
		DatabaseOperation.CLEAN_INSERT.execute(con, dataSet);
		con.close();
	}

	private TestDBSetup(DataSource ds) {
		super();
		this.dataSource = ds;
		
	}
	
	private TestDBSetup(DataSource ds,String...datasets){
		this(ds);
		this.databaseScripts = datasets;
	}
	


	public static TestDBSetup getInstance(DataSource ds) {
		return new TestDBSetup(ds);
	}

	public static TestDBSetup getInstanceForExport(DataSource ds){
		return new TestDBSetup(ds);
	}
	
	public static TestDBSetup getInstance(DataSource ds,String... datasets) {
		return new TestDBSetup(ds,datasets);
	}

	private void insertData() throws IOException, DatabaseUnitException,
			SQLException {
		IDatabaseConnection con = this.getConnection();

		IDataSet dataSet = getDataset();
		DatabaseOperation.CLEAN_INSERT.execute(con, dataSet);
		con.close();
	}

	private IDataSet getDataset() throws DataSetException {
		IDataSet dataset = null;
		if(this.databaseScripts != null){
			IDataSet[] compositeDataset = this.getCompositeDataset();
			dataset = new CompositeDataSet(compositeDataset);
		}else{
			dataset = this.getDatasetFromClasspath(DEFAULT_SCRIPT_XML);
		}
		return dataset;
	}

	private IDataSet getDatasetFromClasspath(String datasetName)
			throws DataSetException {
		InputStream is = this.getClass().getResourceAsStream(datasetName);
		InputSource iSource = new InputSource(is);
		FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
		builder.setColumnSensing(true);
		return builder.build(iSource);
	}
	
	private IDataSet[] getCompositeDataset() throws DataSetException{
		IDataSet[] datasets = new IDataSet[this.databaseScripts.length];
		for(int i = 0; i < this.databaseScripts.length; i++){
			
			IDataSet dataset = this.getDatasetFromClasspath(this.databaseScripts[i]);
			datasets[i]= dataset;
		}
		return datasets;
	}

	public void exportDataFromDB(String[] tables,String filename,String schema) throws FileNotFoundException, IOException,
			SQLException, DatabaseUnitException {
		// full database export
		IDatabaseConnection connection = this.getConnection(schema);
		if(tables == null || tables.length == 0){
			throw new IllegalArgumentException("Table names required");
		}

		IDataSet fullDataSet = connection.createDataSet(tables);
		FlatXmlDataSet.write(fullDataSet, new FileOutputStream(
				filename));

	}
	
	private IDatabaseConnection getConnection() throws DatabaseUnitException,
			SQLException {
		IDatabaseConnection connection = new DatabaseConnection(
				this.dataSource.getConnection());
		return connection;
	}

	private IDatabaseConnection getConnection(String schema)
			throws DatabaseUnitException, SQLException {
		IDatabaseConnection connection = new DatabaseConnection(
				this.dataSource.getConnection(), schema);
		return connection;
	}


}
