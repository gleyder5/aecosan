package test.es.grupoavalon.aecosan.secosan.testutils.spring;

import org.junit.After;
import org.junit.Before;

import test.es.grupoavalon.aecosan.secosan.testutils.setup.TestWSServer;

public abstract class GenericWSTestClass extends GenericDBTestClass{
	
	private static TestWSServer textWsServer = new TestWSServer();
	
	protected abstract Class<?> getwebserviceClassInterface();
	
	protected abstract Object geWebserviceImpl();
	
	protected abstract String getServicePublicName();
	
	@Override
	@Before
	public void before(){
		textWsServer.serverCreate(this.geWebserviceImpl(), this.getwebserviceClassInterface(), this.getServicePublicName());
	}
	
	@After
	public void after(){
		textWsServer.serverDestroy();
	}
	
	

}
