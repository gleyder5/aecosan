package test.es.grupoavalon.aecosan.secosan.business.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.SolicitudLogoService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;

public class SolicitudLogoServiceImplTest extends GenericDBTestClass {

	@Autowired
	SolicitudLogoService solicitudLogoService;

	@Test(expected = ServiceException.class)
	public void testAddSolicitudLmrFitosanitariosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		solicitudLogoService.add(SolicitudMock.USER, solicitudQuejaDTO);
	}

	@Test(expected = ServiceException.class)
	public void testUpdateSolicitudLmrFitosanitariosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		solicitudLogoService.update(SolicitudMock.USER, solicitudQuejaDTO);
	}

}
