package test.es.grupoavalon.aecosan.secosan.dao.repository;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.GenericCatalogRepository;
import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;

/**
 * 
 * @author xi314616
 *
 */
public class GenericCatalogRepositoryImplTest extends GenericRestTest {

	@Autowired
	private ApplicationContext ctx;

	private static String CATALOG_NAME_PAIS = "Pais";
	private static final int LIST_PAISES_SIZE = 201;

	private static String CATALOG_NAME_MUNICIPIO = "Municipio";
	private static final int LIST_MUNICIPIOS_SIZE = 8119;

	private static String CATALOG_NAME_PROVINCIA = "Provincia";
	private static final int LIST_PROVINCIAS_SIZE = 53;

	@Test
	public void testListPaises() {
		List<AbstractCatalogEntity> paises = getCatalogItems(CATALOG_NAME_PAIS);
		assertTrue(paises.size() == LIST_PAISES_SIZE);
	}

	@Test
	public void testListMunicipios() {
		List<AbstractCatalogEntity> paises = getCatalogItems(CATALOG_NAME_MUNICIPIO);
		assertTrue(paises.size() == LIST_MUNICIPIOS_SIZE);
	}

	@Test
	public void testListProvincias() {
		List<AbstractCatalogEntity> paises = getCatalogItems(CATALOG_NAME_PROVINCIA);
		assertTrue(paises.size() == LIST_PROVINCIAS_SIZE);
	}

	@SuppressWarnings("unchecked")
	private List<AbstractCatalogEntity> getCatalogItems(String catalogName) {

		GenericCatalogRepository<AbstractCatalogEntity> genericCatalogRepository;

		List<AbstractCatalogEntity> results = new ArrayList<AbstractCatalogEntity>();
		genericCatalogRepository = ctx.getBean(catalogName, GenericCatalogRepository.class);
		results = genericCatalogRepository.getCatalogList();

		return results;
	}
}
