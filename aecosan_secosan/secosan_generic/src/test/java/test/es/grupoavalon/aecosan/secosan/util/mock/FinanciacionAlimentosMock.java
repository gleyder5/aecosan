package test.es.grupoavalon.aecosan.secosan.util.mock;

import java.sql.Timestamp;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.AlterOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOSuspensionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.BajaOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.FinanciacionAlimentosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.GrupoPacientesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.IncOferAlimUMEEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos.ModoAdministracionEntity;

public class FinanciacionAlimentosMock {

	public static final String CANCELLATION_REASON = "motivo cualquiera";
	public static final String CANCELLATION_REASON_UPDATE = "motivo cualquiera update";
	public static final Long BAJA_ID = 2L;
	private static final Long SUSPENSION_ID = 1L;

	public static IncOferAlimUMEEntity generateIncOferAlimUMEEntity() {
		IncOferAlimUMEEntity incOferAlimUMEEntity = new IncOferAlimUMEEntity();
		prepareMockFinanciacionalimentos(incOferAlimUMEEntity);
		return incOferAlimUMEEntity;
	}

	public static AlterOferAlimUMEEntity generateAlterOferAlimUMEEntity() {
		AlterOferAlimUMEEntity alterOferAlimUMEEntity = new AlterOferAlimUMEEntity();
		prepareMockFinanciacionalimentos(alterOferAlimUMEEntity);
		return alterOferAlimUMEEntity;
	}

	public static BajaOferAlimUMEEntity generateBajaOferAlimUMEEntity() {
		BajaOferAlimUMEEntity bajaOferAlimUMEEntity = new BajaOferAlimUMEEntity();
		prepareMockBajaAlimentos(bajaOferAlimUMEEntity);
		return bajaOferAlimUMEEntity;
	}

	private static void prepareMockBajaAlimentos(BajaOferAlimUMEEntity bajaOferAlimUMEEntity) {
		SolicitudMock.prepareMocksolicitudEntity(bajaOferAlimUMEEntity);
		bajaOferAlimUMEEntity.setCommunicationDate(generateCommunicationDate());
		bajaOferAlimUMEEntity.setSuspensionReasonDuration(CANCELLATION_REASON);
		bajaOferAlimUMEEntity.setCancelOrSuspension(generateCancelOrSuspension(SUSPENSION_ID));
	}

	public static BajaOSuspensionEntity generateCancelOrSuspension(Long id) {
		BajaOSuspensionEntity bajaOSuspensionEntity = new BajaOSuspensionEntity();
		bajaOSuspensionEntity.setId(id);
		return bajaOSuspensionEntity;
	}

	private static void prepareMockFinanciacionalimentos(FinanciacionAlimentosEntity FinanciacionAlimentos) {
		SolicitudMock.prepareMocksolicitudEntity(FinanciacionAlimentos);
		FinanciacionAlimentos.setAdministrationMode(generateMockAdministrationMode());
		FinanciacionAlimentos.setGroupOfPatient(generateMockGroupOfPatients());
	}

	private static ModoAdministracionEntity generateMockAdministrationMode() {
		ModoAdministracionEntity modoAdministracionEntity = new ModoAdministracionEntity();
		modoAdministracionEntity.setId(1L);
		return modoAdministracionEntity;
	}

	private static GrupoPacientesEntity generateMockGroupOfPatients() {
		GrupoPacientesEntity grupoPacientesEntity = new GrupoPacientesEntity();
		grupoPacientesEntity.setAdult(true);
		grupoPacientesEntity.setChild(true);
		grupoPacientesEntity.setBreastfed(false);
		return grupoPacientesEntity;
	}

	public static Timestamp generateCommunicationDate() {
		return new Timestamp(System.currentTimeMillis());
	}

}
