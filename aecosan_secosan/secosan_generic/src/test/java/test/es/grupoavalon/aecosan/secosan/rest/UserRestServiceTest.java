package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

/**
 * 
 *
 */
public class UserRestServiceTest extends GenericRestTest {


	private static final String USER_NO_UE_REST_SERVICE_PATH = "userNoUEMembership/";
	private static final String USER_PATH = "user/";
	private static final String USER_ADMIN = "8";
	private static final String USER_ADMIN_CHANGE_PW = "9";
	private static final String USER_EXTERNAL_CHANGE_PW = "10";
	private static final String USER_CLAVE_SERVICE_PATH = "userClave/";
	private static final String USER_ADD_INTERNAL_SERVICE_PATH = "addUserInternal/";
	private static final String USER_GET_INTERNAL_LIST_SERVICE_PATH = "getInternalUserList/";
	private static final String USER_UPDATE_INTERNAL_SERVICE_PATH = "updateInternalUser/";
	private static final String USER_CHANGE_PASSWORD = "changePassword";
	private static final String SECURE_ADMIN = SecosanConstants.ADMIN_SECURE_CONTEXT + "/";
	private static final String SECURE_PATH = SecosanConstants.SECURE_CONTEXT + "/";
	private static final String SENDED_JSON_FILE_USER_NO_UE_ADD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + USER_PATH + USER_NO_UE_REST_SERVICE_PATH + "addUserNoUE.json";
	private static final String SENDED_JSON_FILE_USER_CLAVE_ADD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + USER_PATH + USER_CLAVE_SERVICE_PATH + "addUserClave.json";
	private static final String SENDED_JSON_FILE_USER_INTERNAL_ADD_OK = UtilRestTestClass.SENDED_JSON_FOLDER + USER_PATH + USER_ADD_INTERNAL_SERVICE_PATH + "addUserInternal.json";
	private static final String EXPECTED_JSON_FILE_USER_INTERNAL_GET_LIST = UtilRestTestClass.SENDED_JSON_FOLDER + USER_PATH + USER_GET_INTERNAL_LIST_SERVICE_PATH + "getInternalUserList.json";
	private static final String SENDED_JSON_FILE_USER_INTERNAL_UPDATE = UtilRestTestClass.SENDED_JSON_FOLDER + USER_PATH + USER_UPDATE_INTERNAL_SERVICE_PATH + "updateInternalUser.json";
	private static final String PASSWORD_BASE_FOLDER = UtilRestTestClass.SENDED_JSON_FOLDER + "password/";
	private static final String EXPECTED_PASSWORD_BASE_FOLDER = UtilRestTestClass.EXPECTED_JSON_FOLDER + "password/";
	private static final String PASSWORD_CHANGE_JSON_OK = PASSWORD_BASE_FOLDER + "passwordChangeOk.json";
	private static final String PASSWORD_CHANGE_JSON_WEAK = PASSWORD_BASE_FOLDER + "passwordChangenNewWeak.json";
	private static final String PASSWORD_CHANGE_JSON_OLD_NOT_EQUAL = PASSWORD_BASE_FOLDER + "passwordChangeOldNotEqual.json";
	private static final String PASSWORD_CHANGE_JSON_NEW_EQUAL_OLD = PASSWORD_BASE_FOLDER + "passwordChangeNewEqualOld.json";
	private static final String EXPECTED_JSON_PASSWORD_CHANGE_JSON_WEAK = EXPECTED_PASSWORD_BASE_FOLDER + "passwordChangeErrorWeak.json";
	private static final String EXPECTED_JSON_PASSWORD_CHANGE_JSON_OLD_NOT_EQUAL = EXPECTED_PASSWORD_BASE_FOLDER + "passwordChangeErrorOldNotEqual.json";
	private static final String EXPECTED_JSON_PASSWORD_CHANGE_JSON_NEW_EQUAL_OLD = EXPECTED_PASSWORD_BASE_FOLDER + "passwordChangeErrorNewEqualOld.json";

	@Test
	public void testCreateUserNoUEMembershipOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(USER_NO_UE_REST_SERVICE_PATH, SENDED_JSON_FILE_USER_NO_UE_ADD_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testCreateUserClaveOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(USER_CLAVE_SERVICE_PATH, SENDED_JSON_FILE_USER_CLAVE_ADD_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testCreateInternalUserOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServicePostJson(SECURE_ADMIN + USER_ADD_INTERNAL_SERVICE_PATH, SENDED_JSON_FILE_USER_INTERNAL_ADD_OK);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testgetInternalUserListOk() throws JsonProcessingException, IOException {
		UtilRestTestClass.executeRestOKGetTest(SECURE_ADMIN + USER_GET_INTERNAL_LIST_SERVICE_PATH, EXPECTED_JSON_FILE_USER_INTERNAL_GET_LIST);
	}

	@Test
	public void testUpdateInternalUsertOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE_ADMIN + USER_UPDATE_INTERNAL_SERVICE_PATH + USER_ADMIN, SENDED_JSON_FILE_USER_INTERNAL_UPDATE);
		UtilRestTestClass.assertReponseOk(response2);
	}

	@Test
	public void testChangePasswordOkInternal() throws JsonProcessingException, IOException {
		MultivaluedMap<String, Object> userLoggedMap = UtilRestTestClass.generateUserLoggedHeaderMap(USER_ADMIN_CHANGE_PW);
		UtilRestTestClass.executeRestOKPostTest(SECURE_ADMIN + USER_CHANGE_PASSWORD, PASSWORD_CHANGE_JSON_OK, userLoggedMap);
	}

	@Test
	public void testChangePasswordOkExternal() throws JsonProcessingException, IOException {
		MultivaluedMap<String, Object> userLoggedMap = UtilRestTestClass.generateUserLoggedHeaderMap(USER_EXTERNAL_CHANGE_PW);
		UtilRestTestClass.executeRestOKPostTest(SECURE_PATH + USER_CHANGE_PASSWORD, PASSWORD_CHANGE_JSON_OK, userLoggedMap);
	}

	@Test
	public void testChangePasswordWeak() throws JsonProcessingException, IOException {

		this.executeChangePasswordError(SECURE_PATH + USER_CHANGE_PASSWORD, PASSWORD_CHANGE_JSON_WEAK, USER_EXTERNAL_CHANGE_PW, EXPECTED_JSON_PASSWORD_CHANGE_JSON_WEAK);
	}

	@Test
	public void testChangePasswordOldNotEqual() throws JsonProcessingException, IOException {

		this.executeChangePasswordError(SECURE_PATH + USER_CHANGE_PASSWORD, PASSWORD_CHANGE_JSON_OLD_NOT_EQUAL, USER_EXTERNAL_CHANGE_PW, EXPECTED_JSON_PASSWORD_CHANGE_JSON_OLD_NOT_EQUAL);
	}

	@Test
	public void testChangePasswordNewEqualOld() throws JsonProcessingException, IOException {

		this.executeChangePasswordError(SECURE_PATH + USER_CHANGE_PASSWORD, PASSWORD_CHANGE_JSON_NEW_EQUAL_OLD, USER_EXTERNAL_CHANGE_PW, EXPECTED_JSON_PASSWORD_CHANGE_JSON_NEW_EQUAL_OLD);
	}

	 private void executeChangePasswordError(String url, String jsonToSend, String userId, String expectedJson) throws JsonProcessingException, IOException {
		MultivaluedMap<String, Object> headers = UtilRestTestClass.generateUserLoggedHeaderMap(userId);
		Response response = UtilRestTestClass.invokeRestServicePostJson(url, jsonToSend, headers);
		UtilRestTestClass.assertReponse500(response);
		UtilRestTestClass.compareJSONReponse(response, expectedJson);
	}

}
