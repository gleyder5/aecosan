package test.es.grupoavalon.aecosan.secosan.business.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.EvaluacionRiesgosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;

public class EvaluacionRiesgosServiceImplTest extends GenericDBTestClass {

	@Autowired
	EvaluacionRiesgosService evaluacionRiesgosService;

	@Test(expected = ServiceException.class)
	public void testAddEvaluacionRiesgosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		evaluacionRiesgosService.add(SolicitudMock.USER, solicitudQuejaDTO);
	}

	@Test(expected = ServiceException.class)
	public void testUpdateEvaluacionRiesgosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		evaluacionRiesgosService.update(SolicitudMock.USER, solicitudQuejaDTO);
	}

}
