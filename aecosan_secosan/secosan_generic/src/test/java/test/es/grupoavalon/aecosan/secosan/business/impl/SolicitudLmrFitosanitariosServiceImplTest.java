package test.es.grupoavalon.aecosan.secosan.business.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.SolicitudLmrFitosanitariosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;

public class SolicitudLmrFitosanitariosServiceImplTest extends GenericDBTestClass {

	@Autowired
	SolicitudLmrFitosanitariosService solicitudLmrFitosanitariosService;

	@Test(expected = ServiceException.class)
	public void testAddSolicitudLmrFitosanitariosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		solicitudLmrFitosanitariosService.add(SolicitudMock.USER, solicitudQuejaDTO);
	}

	@Test(expected = ServiceException.class)
	public void testUpdateSolicitudLmrFitosanitariosServiceKoExceptionThrowerClassNotSuported() {
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		solicitudLmrFitosanitariosService.update(SolicitudMock.USER, solicitudQuejaDTO);
	}

}
