package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.EvaluacionRiesgosMock;
import es.grupoavalon.aecosan.secosan.dao.EvaluacionRiesgosDAO;
import es.grupoavalon.aecosan.secosan.dao.SolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioCreadorEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.evaluacionRiesgos.EvaluacionRiesgosEntity;

/**
 * 
 * @author xi314616
 *
 */
public class EvaluacionRiesgosDAOImplTest extends GenericDBTestClass {

	private static final Long USER_CREATOR = 1L;
	private static final Long RIESGOS_TIPO_FORMULARIO_ID = 7L;

	@Autowired
	private EvaluacionRiesgosDAO complementosAlimenticiosDAO;

	@Autowired
	private SolicitudDAO solicitudDAO;

	@Autowired
	private UsuarioDAO usuarioDAO;

	EvaluacionRiesgosEntity evaluacionRiesgosEntity;


	/**
	 * Try to add new EvaluacionRiesgosEntity
	 * 
	 */
	@Test
	public void testAddEvaluacionRiesgos() {
		evaluacionRiesgosEntity = EvaluacionRiesgosMock.generateEvaluacionRiesgosEntity();
		evaluacionRiesgosEntity.setUserCreator(generateUserCreatorMock());
		evaluacionRiesgosEntity.getFormularioEspecifico().setId(RIESGOS_TIPO_FORMULARIO_ID);
		complementosAlimenticiosDAO.addEvaluacionRiesgos(evaluacionRiesgosEntity);

		EvaluacionRiesgosEntity evaluacionRiesgosEntityFind = (EvaluacionRiesgosEntity) solicitudDAO.findSolicitudById(evaluacionRiesgosEntity.getId());

		assertNotNull(evaluacionRiesgosEntityFind);
		assertEquals(evaluacionRiesgosEntityFind, evaluacionRiesgosEntity);
	}


	private UsuarioCreadorEntity generateUserCreatorMock() {
		return usuarioDAO.findCreatorById(USER_CREATOR);
	}

}
