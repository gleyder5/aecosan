package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.AbstractCatalogEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class CatalogDAOImplTest extends GenericDBTestClass {

	@Autowired
	private CatalogDAO cut;

	private static final String FAKE_CATALOG_NAME = "FAKE";
	private static final String FAKE_CATALOG_PARENT_NAME = "PARENT_FAKE";
	private static final String FAKE_CATALOG_PARENT_ID = "PARENT_ID";
	private static final String FAKE_CATALOG_PARENT_TAG = "PARENT_TAG";

	private static final String CATALOG_NAME_PROVINCIA = "Provincia";
	private static final String CATALOG_NAME_MOCKPROVINCIA = "MockProvincia";
	private static final String REAL_CATALOG_PARENT_NAME = "Pais_id";
	private static final String REAL_PROVINCIA_CATALOG_PARENT_ID = "5";
	private static final String TAG_CANTABRIA = "Cantabria";

	private static final Object EXPECTE_VALUE = "Ficticia";
	private static final int EXPECTE_SIZE = 1;

	@Test(expected = DaoException.class)
	public void testFakeCatalog() {
		cut.getCatalogItems(FAKE_CATALOG_NAME);
	}

	@Test(expected = DaoException.class)
	public void testFakeCatalogByTag() {
		cut.getCatalogItems(FAKE_CATALOG_NAME, FAKE_CATALOG_PARENT_TAG);
	}

	@Test(expected = DaoException.class)
	public void testFakeCatalogByFakeParent() {
		cut.getCatalogItems(FAKE_CATALOG_NAME, FAKE_CATALOG_PARENT_NAME, FAKE_CATALOG_PARENT_ID);
	}

	@Test(expected = DaoException.class)
	public void testFakeCatalogByFakeParent2() {
		cut.getCatalogItems(FAKE_CATALOG_NAME, CATALOG_NAME_PROVINCIA, FAKE_CATALOG_PARENT_ID);
	}

	@Test(expected = DaoException.class)
	public void testFakeCatalogByRealParent() {
		cut.getCatalogItems(FAKE_CATALOG_NAME, REAL_CATALOG_PARENT_NAME, FAKE_CATALOG_PARENT_ID);
	}

	@Test
	public void testCatalogByRealParentAndRealID() {
		List<AbstractCatalogEntity> lista = cut.getCatalogItems(CATALOG_NAME_PROVINCIA, REAL_CATALOG_PARENT_NAME, REAL_PROVINCIA_CATALOG_PARENT_ID);
		assertTrue(lista.size() == EXPECTE_SIZE);
		assertEquals(lista.get(0).getCatalogValue(), EXPECTE_VALUE);

	}

	@Test(expected = DaoException.class)
	public void testCatalogProvinciasByTagCantabria() {
		cut.getCatalogItems(CATALOG_NAME_MOCKPROVINCIA, TAG_CANTABRIA);
	}

}
