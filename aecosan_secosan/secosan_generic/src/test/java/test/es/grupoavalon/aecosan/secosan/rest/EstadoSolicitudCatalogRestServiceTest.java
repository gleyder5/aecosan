package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class EstadoSolicitudCatalogRestServiceTest extends GenericRestTest {

	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = "/catalog";
	private static final String REST_SERVICE_PARAMS = "/EstadoSolicitud";
	private static final String CATALOGOS_JSON_FOLDER = "catalogos";
	private static final String SOLICITUD_JSON_FOLDER = "/estadoSolicitud/";

	private static final String EXPECTED_OK_JSON_FILE = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + "estadoSolicitudCatalog.json";

	@Test
	public void testEstadoSolicitudCatalogRestServiceTest() {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS, EXPECTED_OK_JSON_FILE);
	}
	
}
