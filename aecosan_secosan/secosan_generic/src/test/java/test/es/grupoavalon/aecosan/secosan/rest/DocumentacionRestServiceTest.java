package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class DocumentacionRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "documentacion/";
	private static final String FORM = "form/";
	private static final String USER = "6/";
	private static final String FORM_ID = "14/";
	private static final String ID_DOCUMENTACION_TO_DELETE = "100";
	private static final String ID_DOCUMENTACION_TO_FIND = "101";
	private static final String ID_DOCUMENTACION_TO_DELETE_NOT_EXIST = "10000";
	private static final String EXPECTED_OK_JSON_FILE5 = UtilRestTestClass.EXPECTED_JSON_FOLDER + "documentacionId101.json";
	private static final String EXPECTED_OK_JSON_FILE5_DOCUMENT_TYPES_BY_FORM_ID = UtilRestTestClass.EXPECTED_JSON_FOLDER + "documentationTypesByFormID14.json";

	@Test
	public void testFicheroInstruccionesRestServiceTestFindByFormularioOk() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + ID_DOCUMENTACION_TO_FIND, EXPECTED_OK_JSON_FILE5);
	}

	@Test
	public void testDocumentacionRestServiceTestDeleteByDocumentacionIdOk() throws JsonProcessingException, IOException {
		Response response = UtilRestTestClass.invokeRestServiceDelete(SECURE + REST_SERVICE_PATH + USER + ID_DOCUMENTACION_TO_DELETE);
		UtilRestTestClass.assertReponseOk(response);
	}

	@Test
	public void testDocumentacionRestServiceTestDeleteByDocumentacionIdKoIdNotExist() throws JsonProcessingException, IOException {
		UtilRestTestClass.executeRestErrorDeleteTest(SECURE + REST_SERVICE_PATH + USER + ID_DOCUMENTACION_TO_DELETE_NOT_EXIST);
	}

	@Test
	public void testDocumentacionRestServiceTestFindDocumentTypeByFormId() throws JsonProcessingException, IOException {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + FORM + FORM_ID, EXPECTED_OK_JSON_FILE5_DOCUMENT_TYPES_BY_FORM_ID);
	}

}
