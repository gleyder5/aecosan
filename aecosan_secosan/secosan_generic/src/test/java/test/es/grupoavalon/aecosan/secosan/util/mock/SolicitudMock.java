package test.es.grupoavalon.aecosan.secosan.util.mock;

import java.util.Date;

import es.grupoavalon.aecosan.secosan.business.dto.TipoPersonaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.TipoUsuarioDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.AreaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.UsuarioRolDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.UsuarioSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.AbstractSolicitudesQuejaSugerenciaDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoFormularioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.TipoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitanteEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.SituacionEntity;

public class SolicitudMock {

	public static final String USER = "1";

	private static final String IDENTIFICADOR_PETICION = "20160504152415xxx";
	private static final Long PERSON_TYPE_ID = 1L;
	private static final Long ROLE_TYPE_ID = 1L;
	private static final Long TIPE_ID = 1L;
	private static final Long AREA_ID = 3L;
	private static final String AREA_CATALOG_VALUE = "Comunicación";
	private static final Long FORMULARIO_ID = 1L;
	private static final String FORMULARIO_CATALOG_VALUE = "Comunicación Queja";

	public static SolicitanteEntity generateMockSolicitante() {
		SolicitanteEntity usuarioSolicitanteEntity = new SolicitanteEntity();
		usuarioSolicitanteEntity.setPersonType(generateMockPersonType());
		// usuarioSolicitanteEntity.setRole(generateMockRoleType());
		// usuarioSolicitanteEntity.setTipo(generateMockTipoType());
		return usuarioSolicitanteEntity;
	}

	public static EstadoSolicitudEntity prepareMockStatus() {
		EstadoSolicitudEntity estadoSolicitud = new EstadoSolicitudEntity();

		estadoSolicitud.setId(1l);
		estadoSolicitud.setCatalogValue("estado1");

		return estadoSolicitud;
	}

	public static TipoSolicitudEntity prepareMockFormulario() {
		TipoSolicitudEntity formulario = new TipoSolicitudEntity();

		formulario.setId(FORMULARIO_ID);
		formulario.setCatalogValue(FORMULARIO_CATALOG_VALUE);
		formulario.setRelatedArea(prepareMockAreaEntity());

		return formulario;
	}

	public static AreaEntity prepareMockAreaEntity() {
		AreaEntity area = new AreaEntity();

		area.setId(AREA_ID);
		area.setCatalogValue(AREA_CATALOG_VALUE);

		return area;
	}

	public static Date sysDate() {
		return new Date(System.currentTimeMillis());
	}

	// private static TipoUsuarioEntity generateMockTipoType() {
	// TipoUsuarioEntity tipoUsuarioEntity = new TipoUsuarioEntity();
	// tipoUsuarioEntity.setId(TIPE_ID);
	// return tipoUsuarioEntity;
	// }
	//
	// private static UsuarioRolEntity generateMockRoleType() {
	// UsuarioRolEntity usuarioRolEntity = new UsuarioRolEntity();
	// usuarioRolEntity.setId(ROLE_TYPE_ID);
	// return usuarioRolEntity;
	// }

	private static TipoPersonaEntity generateMockPersonType() {
		TipoPersonaEntity tipoPersonaEntity = new TipoPersonaEntity();
		tipoPersonaEntity.setId(PERSON_TYPE_ID);
		return tipoPersonaEntity;
	}

	public static UsuarioSolicitanteDTO generateMockSolicitanteDTO() {
		UsuarioSolicitanteDTO usuarioSolicitanteDTO = new UsuarioSolicitanteDTO();
		usuarioSolicitanteDTO.setPersonType(generateMockPersonTypeDTO());
		usuarioSolicitanteDTO.setRole(generateMockRoleDTO());
		usuarioSolicitanteDTO.setTipo(generateMockTipoDTO());
		return usuarioSolicitanteDTO;
	}

	public static TipoSolicitudDTO prepareMockFormularioDTO() {
		TipoSolicitudDTO formulario = new TipoSolicitudDTO();

		formulario.setId(String.valueOf(FORMULARIO_ID));
		formulario.setCatalogValue(FORMULARIO_CATALOG_VALUE);
		formulario.setRelatedArea(prepareMockAreaDTO());

		return formulario;
	}

	private static TipoPersonaDTO generateMockPersonTypeDTO() {
		TipoPersonaDTO tipoPersonaDTO = new TipoPersonaDTO();
		tipoPersonaDTO.setId(String.valueOf(PERSON_TYPE_ID));
		return tipoPersonaDTO;
	}

	public static AreaDTO prepareMockAreaDTO() {
		AreaDTO area = new AreaDTO();
		area.setId(String.valueOf(AREA_ID));
		area.setCatalogValue(AREA_CATALOG_VALUE);
		return area;
	}

	private static UsuarioRolDTO generateMockRoleDTO() {
		UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
		usuarioRolDTO.setId(String.valueOf(ROLE_TYPE_ID));
		return usuarioRolDTO;
	}

	private static TipoUsuarioDTO generateMockTipoDTO() {
		TipoUsuarioDTO tipoUsuarioDTO = new TipoUsuarioDTO();
		tipoUsuarioDTO.setId(String.valueOf(TIPE_ID));
		return tipoUsuarioDTO;
	}

	public static void prepareMocksolicitudEntity(SolicitudEntity SolicitudEntity) {

		SolicitudEntity.setArea(prepareMockAreaEntity());
		SolicitudEntity.setFechaCreacion(sysDate());
		SolicitudEntity.setFormulario(prepareMockFormulario());
		SolicitudEntity.setStatus(prepareMockStatus());
		SolicitudEntity.setIdentificadorPeticion(IDENTIFICADOR_PETICION);
		SolicitudEntity.setSolicitante(generateMockSolicitante());
		SolicitudEntity.setFormularioEspecifico(new TipoFormularioEntity());
	}

	public static void prepareMockSolicitudQuejaSugerenciaDTO(AbstractSolicitudesQuejaSugerenciaDTO solicitudQuejaSugerenciaDTO) {
		solicitudQuejaSugerenciaDTO.setArea(prepareMockAreaDTO());
		solicitudQuejaSugerenciaDTO.setFechaCreacion(sysDate().getTime());
		solicitudQuejaSugerenciaDTO.setFormulario(prepareMockFormularioDTO());
		solicitudQuejaSugerenciaDTO.setStatus(prepareMockStatus().getId());
		solicitudQuejaSugerenciaDTO.setIdentificadorPeticion(IDENTIFICADOR_PETICION);
		solicitudQuejaSugerenciaDTO.setSolicitante(generateMockSolicitanteDTO());

	}

	public static SituacionEntity prepareMockSituacionEntity() {
		SituacionEntity situacionEntity = new SituacionEntity();
		situacionEntity.setId(1L);
		return situacionEntity;
	}

}
