package test.es.grupoavalon.aecosan.secosan.business.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.ComplementosAlimenticiosService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudMock;
import test.es.grupoavalon.aecosan.secosan.util.mock.SolicitudQuejaSugerenciaMock;

public class ComplementosAlimenticiosServiceImplTest extends GenericDBTestClass {

	@Autowired
	ComplementosAlimenticiosService complementosAlimenticiosService;

	@Test(expected = ServiceException.class)
	public void testAddCeseComercializacionKoExceptionThrowerClassNotSuported() {
		
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		complementosAlimenticiosService.add(SolicitudMock.USER, solicitudQuejaDTO);
	}

	@Test(expected = ServiceException.class)
	public void testUpdateCeseComercializacionKoExceptionThrowerClassNotSuported() {
		
		SolicitudQuejaDTO solicitudQuejaDTO = SolicitudQuejaSugerenciaMock.generateSolicitudQuejaDto();
		complementosAlimenticiosService.update(SolicitudMock.USER, solicitudQuejaDTO);
		
	}

}
