package test.es.grupoavalon.aecosan.secosan.testutils.spring;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericTestClass;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

/**
 * @author ottoabreu
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:"+SecosanConstants.SPRING_CONTEXT_CONFIG)
@Transactional
public abstract class GenericSpringTestClass extends GenericTestClass {
	
}
