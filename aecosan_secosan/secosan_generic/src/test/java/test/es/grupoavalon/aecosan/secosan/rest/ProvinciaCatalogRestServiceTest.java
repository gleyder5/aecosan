package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class ProvinciaCatalogRestServiceTest extends GenericRestTest {

	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = "/catalog";
	private static final String REST_SERVICE_PARAMS = "/Provincia";
	private static final String CATALOGOS_JSON_FOLDER = "catalogos/";
	private static final String TEST_PAIS5 = "/Pais_id/5";
	private static final String SOLICITUD_JSON_FOLDER = "provincia/";

	private static final String EXPECTED_OK_JSON_FILE = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + "provinciaCatalog.json";
	private static final String EXPECTED_OK_JSON_FILE_PAIS_5 = UtilRestTestClass.EXPECTED_JSON_FOLDER + CATALOGOS_JSON_FOLDER + SOLICITUD_JSON_FOLDER + "provinciaPais5Catalog.json";

	@Test
	public void testProvinciaCatalogRestServiceTest() {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS, EXPECTED_OK_JSON_FILE);
	}

	@Test
	public void testProvinciaCatalogRestServiceTestPais5() {
		UtilRestTestClass.executeRestOKGetTest(SECURE + REST_SERVICE_PATH + REST_SERVICE_PARAMS + TEST_PAIS5, EXPECTED_OK_JSON_FILE_PAIS_5);
	}

}
