package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import org.junit.Test;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */

public class ImpresoFirmaRestServiceTest extends GenericRestTest {

	private static final String REST_SERVICE_PATH = SecosanConstants.SECURE_CONTEXT + "/impresoFirma/";
	private static final String REST_SERVICE_PATH_ERROR404 = "impresoFirma/5/FAKE";
	private static final String REST_SERVICE_OK_PARAMS = "5";
	private static final String EXPECTED_OK_JSON_FILE5 = UtilRestTestClass.EXPECTED_JSON_FOLDER + "impresoFirma.json";


	@Test
	public void testImpresoFirmaRestServiceTestGetPdfOk() throws IOException {
		UtilRestTestClass.executeRestOKPostTest(REST_SERVICE_PATH + REST_SERVICE_OK_PARAMS, EXPECTED_OK_JSON_FILE5);
	}

	@Test
	public void testImpresoFirmaRestServiceTestError404() {
		UtilRestTestClass.executeRestClientNotFound(REST_SERVICE_PATH_ERROR404);
	}

}
