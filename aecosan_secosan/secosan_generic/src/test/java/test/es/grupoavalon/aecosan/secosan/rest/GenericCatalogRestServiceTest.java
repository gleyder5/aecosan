package test.es.grupoavalon.aecosan.secosan.rest;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 * @author juan.cabrerizo
 *
 */
public class GenericCatalogRestServiceTest extends GenericRestTest {

	private static final String SECURE = "/secure";
	private static final String REST_SERVICE_PATH = SECURE + "/catalog";

	private static final String FAKE_REST_SERVICE_PARAMS = "/FAKE_CATALOGO";
	private static final String FAKE_TAG = "/FAKE_TAG";

	private static final String REAL_TAGGED_REST_SERVICE_PARAMS = "/Provincia";
	private static final String REAL_REST_SERVICE_PARAMS = "/Estados";

	private static final String FAKE_CATALOG_REQUEST_BY_TAG = REST_SERVICE_PATH + REAL_TAGGED_REST_SERVICE_PARAMS + FAKE_TAG;
	private static final String FAKE_CATALOG_REQUEST_BY_NOT_TAGGED = REST_SERVICE_PATH + REAL_REST_SERVICE_PARAMS + FAKE_TAG;
	private static final String FAKE_CATALOG_REQUEST_BY_FAKE_CATALOG = REST_SERVICE_PATH + FAKE_REST_SERVICE_PARAMS + FAKE_TAG;

	private static final String FAKE_CATALOG_REQUEST_BY_CATALOG_NAME = REST_SERVICE_PATH + FAKE_REST_SERVICE_PARAMS;

	@Test
	public void testProvinciaCatalogRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_CATALOG_NAME);
	}

	@Test
	public void testProvinciaCatalogbyFakeTagRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_TAG);
	}

	@Test
	public void testProvinciaCatalogByNotTaggedRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_NOT_TAGGED);
	}

	@Test
	public void testProvinciaCatalogByFakeTaggedRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_NOT_TAGGED);
	}

	@Test
	public void testProvinciaCatalogByFakeCatalogRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_FAKE_CATALOG);
	}

	@Test
	public void testProvinciaCatalogbyFakeByParetnRestServiceTest() {
		UtilRestTestClass.executeRestServerErrorTest(FAKE_CATALOG_REQUEST_BY_TAG);
	}

	@Test
	public void testRootList404Error() {
		UtilRestTestClass.executeRestClientNotFound(REST_SERVICE_PATH);
	}
}
