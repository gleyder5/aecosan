package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;

public class EstadoRestServiceTest extends GenericRestTest {

	private static final String REST_SERVICE_PATH = SecosanConstants.ADMIN_SECURE_CONTEXT + "/status/";
	private static final String REST_SERVICE_PATH_ERROR404 = SecosanConstants.ADMIN_SECURE_CONTEXT + "/status/5/FAKE";
	private static final String STATUS_PATH = UtilRestTestClass.EXPECTED_JSON_FOLDER + "status/";

	private static final String EXPECTED_OK_JSON_FILE_AREA_1 = STATUS_PATH + "StatusByArea1.json";
	private static final String EXPECTED_OK_JSON_FILE_AREA_2 = STATUS_PATH + "StatusByArea2.json";
	private static final String EXPECTED_OK_JSON_FILE_AREA_3 = STATUS_PATH + "StatusByArea3.json";
	private static final String EXPECTED_OK_JSON_FILE_AREA_4 = STATUS_PATH + "StatusByArea4.json";
	private static final String EXPECTED_OK_JSON_FILE_AREA_5 = STATUS_PATH + "StatusByArea5.json";
	private static final String EXPECTED_OK_JSON_FILE_AREA_6 = STATUS_PATH + "StatusByArea6.json";


	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_1() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "1", EXPECTED_OK_JSON_FILE_AREA_1);
	}

	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_2() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "2", EXPECTED_OK_JSON_FILE_AREA_2);
	}

	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_3() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "3", EXPECTED_OK_JSON_FILE_AREA_3);
	}

	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_4() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "4", EXPECTED_OK_JSON_FILE_AREA_4);
	}

	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_5() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "5", EXPECTED_OK_JSON_FILE_AREA_5);
	}

	@Test
	public void testEstadoRestServiceTestGetStatusByAreaId_6() throws IOException {
		UtilRestTestClass.executeRestOKGetTest(REST_SERVICE_PATH + "6", EXPECTED_OK_JSON_FILE_AREA_6);
	}

	@Test
	public void testEstadoRestServiceTestError404() {
		UtilRestTestClass.executeRestClientNotFound(REST_SERVICE_PATH_ERROR404);
	}

}
