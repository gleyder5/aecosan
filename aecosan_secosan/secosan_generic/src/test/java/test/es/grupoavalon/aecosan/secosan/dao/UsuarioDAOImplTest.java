package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoUsuarioEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.UsuarioRolEntity;

/**
 * 
 * @author xi314616
 *
 */
public class UsuarioDAOImplTest extends GenericDBTestClass {

	@Autowired
	private UsuarioDAO usuarioDAO;

	private static final Long USUARIO_ID_1 = 1l;
	private static final Long USUARIO_ID_NOT_EXIST = 555552l;
	private static final String USUARIO1_ROL_DESCRIPCION = "Solicitante";
	private static final String USUARIO1_TIPO_DESCRIPCION = "Usuario Interno";

	// Test Sobre Rol de Usuario
	@Test
	public void testUsuarioFindRolByIdOK() {
		UsuarioRolEntity usuario = usuarioDAO.findRol(USUARIO_ID_1);
		assertEquals(usuario.getCatalogValue(), USUARIO1_ROL_DESCRIPCION);
	}

	@Test
	public void testUsuarioFindRolByIdNotExist() {
		UsuarioRolEntity usuario = usuarioDAO.findRol(USUARIO_ID_NOT_EXIST);
		assertNull(usuario);
	}


	// Test Sobre Tipo Usuario

	@Test
	public void testUsuarioFindTipoByIdOK() {
		TipoUsuarioEntity usuario = usuarioDAO.findTipoUsuario(USUARIO_ID_1);
		assertEquals(usuario.getCatalogValue(), USUARIO1_TIPO_DESCRIPCION);
	}

	@Test
	public void testUsuarioFindTipoByIdNotExist() {
		TipoUsuarioEntity usuario = usuarioDAO.findTipoUsuario(USUARIO_ID_NOT_EXIST);
		assertNull(usuario);
	}


}
