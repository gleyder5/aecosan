package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class EstadoSolicitudDAOImplTest extends GenericDBTestClass {

	@Autowired
	private EstadoSolicitudDAO estadoSolicitudDAO;

	private static final Long ESTADO_ID_1 = 1l;
	private static final Long ESTADO_ID_NOT_EXIST = 555552l;
	private static final String ESTADO1_DESCRIPCION = "Borrador";

	@Test
	public void testEstadoFindByIdOK() {
		EstadoSolicitudEntity estado = estadoSolicitudDAO.findEstadoSolicitud(ESTADO_ID_1);
		assertEquals(estado.getCatalogValue(), ESTADO1_DESCRIPCION);
	}


	@Test
	public void testEstadoFindByIdNotExist() {
		EstadoSolicitudEntity estado = estadoSolicitudDAO.findEstadoSolicitud(ESTADO_ID_NOT_EXIST);
		assertNull(estado);
	}

	@Test(expected = DaoException.class)
	public void testEstadoFindByIdNull() {
		estadoSolicitudDAO.findEstadoSolicitud(null);
	}

	@Test
	public void testInitialSolicitudeStatus() {
		EstadoSolicitudEntity estado = estadoSolicitudDAO.getInitialSolicitudeStatus();
		assertEquals(estado.getCatalogValue(), ESTADO1_DESCRIPCION);
	}

}
