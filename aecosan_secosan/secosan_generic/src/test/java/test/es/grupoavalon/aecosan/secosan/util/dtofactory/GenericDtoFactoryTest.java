package test.es.grupoavalon.aecosan.secosan.util.dtofactory;

import static org.junit.Assert.fail;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.EntityMock;
import uk.co.jemos.podam.api.PodamFactory;
import uk.co.jemos.podam.api.PodamFactoryImpl;
import uk.co.jemos.podam.common.AttributeStrategy;
import es.grupoavalon.aecosan.secosan.dao.repository.CrudRepository;
import es.grupoavalon.aecosan.secosan.util.dtofactory.DtoEntityFactory;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.IgnoreField;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.FactoryFillingException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.AbstractBeanFactory;

public abstract class GenericDtoFactoryTest extends GenericDBTestClass {

	private GenericDtoTestHelper helper;

	private static List<String> DEFAULT_PRIMITIVES_VALUES = new ArrayList<String>();

	static {

		DEFAULT_PRIMITIVES_VALUES.add("0");
		DEFAULT_PRIMITIVES_VALUES.add("false");
		DEFAULT_PRIMITIVES_VALUES.add("0.0d");
		DEFAULT_PRIMITIVES_VALUES.add("0.0f");
		DEFAULT_PRIMITIVES_VALUES.add("0.0");

	}

	public GenericDtoFactoryTest() {
		super();

		this.helper = this.new GenericDtoTestHelper(Object.class);
	}

	public void assertEqualsValuesBetweenEntityListAndDtoList(List<?> entityList, List<?> dtoList) {

		if (entityList.size() == dtoList.size()) {
			for (int i = 0; i < dtoList.size(); i++) {

				Object dto = dtoList.get(i);
				Object entity = entityList.get(i);
				this.assertEqualsValuesBetweenEntityAndDto(entity, dto);
			}

		} else {
			fail("List sizes are not equal: first:" + entityList.size() + " second:" + dtoList.size());
		}
	}

	public void assertEqualsValuesBetweenEntityAndDto(Object entity, Object dto) {

		Field[] fields = getFields(dto);

		for (Field field : fields) {

			this.executeReverseMappingInField(field, dto, entity);
		}

	}

	private static Field[] getFields(Object dto) {

		Field[] dtoFields = dto.getClass().getDeclaredFields();
		return dtoFields;
	}

	private void executeReverseMappingInField(Field field, Object dto, Object entity) {
		boolean ignoreFlag = field.isAnnotationPresent(IgnoreField.class);
		if (!ignoreFlag) {
			BeanToBeanMapping annotation = field.getAnnotation(BeanToBeanMapping.class);

			String fieldName = field.getName();

			if (helper.isACollectionOfOtherBean(annotation)) {

				this.checkValuesInCollectionOfOtherBean(fieldName, annotation, dto, entity);

			} else if (helper.isOneToOtherMapping(annotation)) {

				this.checkInIsOneToOtherMapping(fieldName, annotation, dto, entity);

			} else if (helper.isOneToOneMapping(annotation)) {

				this.checkValuesInOneToOne(fieldName, annotation, dto, entity);

			} else if (helper.valueIsObtainedFromOtherBean(annotation)) {

				this.checkValuesInValuesFromOtherBeans(fieldName, annotation, dto, entity);

			} else if (helper.haveAnnotationButMissingConfiguration(annotation)) {

				this.checkValuesSameName(fieldName, dto, entity);
			}
		}

	}

	private void checkValuesInCollectionOfOtherBean(String fieldName, BeanToBeanMapping annotation, Object dto, Object entity) {
		String entityListField = helper.getFieldName(fieldName, annotation);

		Collection<?> dtoList = (Collection<?>) this.executeGetMethod(fieldName, dto);
		Collection<?> entityList = (Collection<?>) this.executeGetMethod(entityListField, entity);

		if (dtoList != null && entityList != null) {
			validateListSize(dtoList, entityList);
			this.validateListContent(dtoList, entityList);
		}
	}

	private static void validateListSize(Collection<?> dtoList, Collection<?> entityList) {

		if (dtoList.size() != entityList.size()) {
			fail("List values are not the same : First value:" + dtoList.size() + "| Second:" + entityList.size());
		}
	}

	private void validateListContent(Collection<?> dtoList, Collection<?> entityList) {
		Object[] dtoValues = dtoList.toArray();
		Object[] entityValues = entityList.toArray();
		for (int i = 0; i < dtoValues.length; i++) {
			Object value = dtoValues[i];
			Object otherValue = entityValues[i];
			this.assertEqualsValuesBetweenEntityAndDto(value, otherValue);
		}
	}

	private void checkInIsOneToOtherMapping(String fieldName, BeanToBeanMapping annotation, Object dto, Object entity) {
		String entityFieldName = helper.getFieldName(fieldName, annotation);

		Object dtoValue = this.executeGetMethod(fieldName, dto);
		Object entityValue = this.executeGetMethod(entityFieldName, entity);
		getLogger().debug("comparing DTO Other Object:" + dtoValue);
		getLogger().debug("comparing Entity Other Object:" + entityValue);
		this.assertEqualsValuesBetweenEntityAndDto(entityValue, dtoValue);
	}

	protected void checkValuesInOneToOne(String fieldName, BeanToBeanMapping annotation, Object dto, Object entity) {
		String entityFieldName = helper.getFieldName(fieldName, annotation);
		getLogger().debug("comparing DTOfield:" + fieldName);
		getLogger().debug("comparing EntityField:" + entityFieldName);
		Object dtoValue = this.executeGetMethod(fieldName, dto);
		Object entityValue = this.executeGetMethod(entityFieldName, entity);
		if (!valuesAreEqual(dtoValue, entityValue)) {
			fail("values are not the same (1-1):" + dtoValue + "|" + entityValue);
		}

	}

	private void checkValuesInValuesFromOtherBeans(String fieldName, BeanToBeanMapping annotation, Object dto, Object entity) {
		String firstEntityFieldName = annotation.getValueFrom();
		String secondEntityFieldName = annotation.getSecondValueFrom();
		Object entityValue = null;

		Object firstEntityValue = this.executeGetMethod(firstEntityFieldName, entity);
		if (firstEntityValue != null) {
			entityValue = this.executeGetMethod(secondEntityFieldName, firstEntityValue);
		}

		Object dtoValue = this.executeGetMethod(fieldName, dto);
		getLogger().debug("comparing DTOfield:" + fieldName);
		getLogger().debug("comparing EntityField:" + firstEntityFieldName + "." + secondEntityFieldName);
		if (!valuesAreEqual(dtoValue, entityValue)) {
			fail("values are not the same(other beans):" + dtoValue + "|" + entityValue);
		}

	}

	private void checkValuesSameName(String fieldName, Object dto, Object entity) {
		Object dtoValue = this.executeGetMethod(fieldName, dto);
		Object entityValue = this.executeGetMethod(fieldName, entity);

		if (!valuesAreEqual(dtoValue, entityValue)) {
			fail("values are not the same(Same name:" + fieldName + "):" + dtoValue + "|" + entityValue);
		}
	}

	protected Object executeGetMethod(String fieldName, Object instanceFrom) {
		return helper.obtainValueFromeGetMethod(fieldName, instanceFrom);
	}

	private static boolean valuesAreEqual(Object value, Object otherValue) {

		boolean valuesAreEqual = false;
		if (neitherIsNull(value, otherValue)) {

			String dtoStringValue = transformValueToString(value);
			String entitStringyValue = transformValueToString(otherValue);

			if (StringUtils.equals(dtoStringValue, entitStringyValue)) {
				valuesAreEqual = true;
			}

		} else if (bothAreNull(value, otherValue)) {
			valuesAreEqual = true;

		} else if (isNullAndPrimitiveDefaults(value, otherValue)) {
			valuesAreEqual = true;
		}

		return valuesAreEqual;
	}

	private static boolean neitherIsNull(Object value, Object otherValue) {

		if (value != null && otherValue != null) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean bothAreNull(Object value, Object otherValue) {

		if (value == null && otherValue == null) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean isNullAndPrimitiveDefaults(Object value, Object otherValue) {
		boolean isNullAndPrimitiveDefaults = false;
		if (value == null && valueIsPrimitive(otherValue) && valueIsDefaultPrimitive(otherValue)) {
			isNullAndPrimitiveDefaults = true;
		} else if (otherValue == null && valueIsPrimitive(value) && valueIsDefaultPrimitive(value)) {
			isNullAndPrimitiveDefaults = true;
		}

		return isNullAndPrimitiveDefaults;
	}

	private static boolean valueIsPrimitive(Object value) {
		if (value.getClass().isPrimitive()) {
			return true;
		} else {
			return false;
		}
	}

	private static boolean valueIsDefaultPrimitive(Object value) {
		String valueString = transformValueToString(value).toLowerCase();
		if (DEFAULT_PRIMITIVES_VALUES.contains(valueString)) {
			return true;
		} else {
			return false;
		}
	}

	private static String transformValueToString(Object input) {
		String stringValue = null;
		if (input != null && !isValueIsBigInteger(input)) {
			stringValue = input.toString();
		} else if (input != null && isValueIsBigInteger(input)) {
			Float floatValue = ((BigDecimal) input).floatValue();
			stringValue = floatValue.toString();
		}

		return stringValue;
	}

	private static boolean isValueIsBigInteger(Object value) {
		if (value instanceof BigDecimal) {
			return true;
		} else {
			return false;
		}
	}

	private class GenericDtoTestHelper extends AbstractBeanFactory<Object> {

		protected GenericDtoTestHelper(Class<?> toInstatiate) {
			super(toInstatiate);

		}

		public Object obtainValueFromeGetMethod(String fieldName, Object instanceFrom) {
			return this.executeGetMethod(fieldName, instanceFrom);
		}

		@Override
		protected void moveValuesFromObjectToNewInstance(Field[] fields, Object newInstance) throws FactoryFillingException, BeanFactoryException {

		}

		public boolean isACollectionOfOtherBean(BeanToBeanMapping annotation) {
			return super.fieldIsACollectionOfOtherBean(annotation);
		}

		public boolean isOneToOneMapping(BeanToBeanMapping annotation) {
			return super.fieldIsOneToOneMapping(annotation);
		}

		public boolean valueIsObtainedFromOtherBean(BeanToBeanMapping annotation) {
			return super.fieldValueIsObtainedFromOtherBean(annotation);
		}

		public boolean haveAnnotationButMissingConfiguration(BeanToBeanMapping annotation) {

			return super.fieldHaveAnnotationButMissingConfiguration(annotation);
		}

		public boolean isOneToOtherMapping(BeanToBeanMapping annotation) {

			return super.fieldIsOneToOtherMapping(annotation);
		}

		public String getFieldName(String fieldName, BeanToBeanMapping annotation) {
			return getFieldNameToGetValueFrom(fieldName, annotation);
		}

	}

	protected static EntityMock fillEntity() {
		EntityMock myPojo = fillPojo(EntityMock.class);
		getLogger(BeanFromBeanFactoryTest.class).debug("entity to use:" + myPojo);
		return myPojo;
	}

	protected static <T> T fillPojo(Class<T> pojoClass) {
		PodamFactory factory = new PodamFactoryImpl();
		return factory.manufacturePojo(pojoClass);

	}

	protected static List<EntityMock> fillEntityList() {

		return fillPojoList(EntityMock.class);
	}

	protected static <T> List<T> fillPojoList(Class<T> pojoClass) {
		Random random = new Random();
		int maxList = random.nextInt(101);
		List<T> pojoList = new ArrayList<T>();

		for (int i = 0; i < maxList; i++) {
			T pojo = fillPojo(pojoClass);
			pojoList.add(pojo);
		}

		return pojoList;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected static <T> T getEntityToTest(CrudRepository repo, Serializable pk) {
		return (T) repo.findOne(pk);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static <T> List<T> getListEntityToTest(CrudRepository repo) {
		return repo.findAll();
	}

	protected static <ENTITY, DTO> DtoEntityFactory<ENTITY, DTO> getDtoEntityFactory(Class<ENTITY> entityclass, Class<DTO> dtoClass) {
		@SuppressWarnings("unchecked")
		DtoEntityFactory<ENTITY, DTO> factory = DtoEntityFactory.getInstance(entityclass, dtoClass);
		return factory;
	}

	public static class TimestampStrategy implements AttributeStrategy<Timestamp> {

		@Override
		public Timestamp getValue() {
			return new Timestamp(System.currentTimeMillis());
		}

	}

	public static class StringFLoatStrategy implements AttributeStrategy<String> {

		@Override
		public String getValue() {
			float minX = 50.0f;
			float maxX = 100.0f;
			Random rand = new Random();

			float finalX = rand.nextFloat() * (maxX - minX) + minX;
			return Float.toString(finalX);
		}

	}


	public static class ByteArrayStrategy implements AttributeStrategy<byte[]> {

		@Override
		public byte[] getValue() {
			String base64 = "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm"
					+ "/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/" + "8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/"
					+ "9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QP"
					+ "IGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmT"
					+ "kP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4G"
					+ "ImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///"
					+ "wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
					+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhx"
					+ "oYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLP"
					+ "uhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1f"
					+ "DnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6"
					+ "awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlc"
					+ "Dhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGY"
					+ "q/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv"
					+ "9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuq"
					+ "KB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56"
					+ "I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSA"
					+ "JMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTn"
					+ "AUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQ"
					+ "kh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCX"
					+ "iAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5A"
					+ "ChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
			return org.apache.commons.codec.binary.Base64.decodeBase64(base64.getBytes());

		}
	}

}
