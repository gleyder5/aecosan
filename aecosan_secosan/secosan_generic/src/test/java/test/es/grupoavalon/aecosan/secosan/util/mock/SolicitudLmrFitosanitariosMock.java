package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudLmrFitosanitariosEntity;

public class SolicitudLmrFitosanitariosMock {

	private static final String TEST_ACTIVE_SUBSTANCE = "test active sustance";
	private static final String TEST_ORIGIN_COUNTRY = "test origin country";
	private static final String TEST_FOODS = "test foods";

	public static SolicitudLmrFitosanitariosEntity generateSolicitudLmrFitosanitariosEntity() {
		SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity = new SolicitudLmrFitosanitariosEntity();
		prepareMockSolicitudLmrFitosanitarios(solicitudLmrFitosanitariosEntity);
		return solicitudLmrFitosanitariosEntity;
	}

	private static void prepareMockSolicitudLmrFitosanitarios(
			SolicitudLmrFitosanitariosEntity solicitudLmrFitosanitariosEntity) {
		SolicitudMock.prepareMocksolicitudEntity(solicitudLmrFitosanitariosEntity);
		solicitudLmrFitosanitariosEntity.setActiveSubstance(TEST_ACTIVE_SUBSTANCE);
		solicitudLmrFitosanitariosEntity.setOriginCountry(TEST_ORIGIN_COUNTRY);
		solicitudLmrFitosanitariosEntity.setFoods(TEST_FOODS);
	}

}
