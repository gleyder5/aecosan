package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.TipoProductoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.CeseComercializacionAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.ModificacionDatosAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;

public class AlimentosGruposMock extends AlimentosMock {

	public static CeseComercializacionAGEntity generateCeseComercializacionEntity() {
		CeseComercializacionAGEntity ceseComercializacionEntity = new CeseComercializacionAGEntity();
		prepareMockCeseComercializacion(ceseComercializacionEntity);
		return ceseComercializacionEntity;
	}

	public static PuestaMercadoAGEntity generatePuestaMercadoEntity() {
		PuestaMercadoAGEntity puestaMercadoEntity = new PuestaMercadoAGEntity();
		prepareMockPuestaMercado(puestaMercadoEntity);
		puestaMercadoEntity.setProductType(generateProductType());
		return puestaMercadoEntity;
	}

	private static TipoProductoEntity generateProductType() {
		TipoProductoEntity tipoProducto = new TipoProductoEntity();
		tipoProducto.setIdAsString("1");
		return tipoProducto;
	}

	public static ModificacionDatosAGEntity generateModificacionDatosEntity() {
		ModificacionDatosAGEntity modificacionDatosEntity = new ModificacionDatosAGEntity();
		prepareMockModificacionDatos(modificacionDatosEntity);
		return modificacionDatosEntity;
	}

}
