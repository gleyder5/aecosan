package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.CeseComercializacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.IngredientesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.ModificacionDatosEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.PuestaMercadoEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.SituacionEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.alimentosGrupos.PuestaMercadoAGEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;

public class AlimentosMock {

	private static final String INGREDIENTES_PUESTA_MERCADO_ALIMENTOS_GRUPO = "Ingredientes Puesta Mercado Alimentos Grupo";
	private static final String TEST_NIF = "123456789-Z";
	private static final String TEST_DAO_OBSERVACIONES = "Test DAO Observaciones";
	private static final String TEST_DAO_NOMBRE_RAZON_SOCIAL = "Test DAO Nombre Razón social";
	public static final String TEST_NOMBRE_PRODUCTO = "Test DAO ComercialProductName";
	public static final String TEST_OBSERVACIONES = "Test DAO Observaciones";

	protected static void prepareMockCeseComercializacion(CeseComercializacionEntity ceseComercializacionEntity) {
		SolicitudMock.prepareMocksolicitudEntity(ceseComercializacionEntity);
		ceseComercializacionEntity.setNombreComercialProducto(TEST_NOMBRE_PRODUCTO);
		ceseComercializacionEntity.setObservaciones(TEST_DAO_OBSERVACIONES);
	}

	protected static void prepareMockModificacionDatos(ModificacionDatosEntity modificacionDatosEntity) {
		SolicitudMock.prepareMocksolicitudEntity(modificacionDatosEntity);
		modificacionDatosEntity.setNif(TEST_NIF);
		modificacionDatosEntity.setNombreComercialProducto(TEST_NOMBRE_PRODUCTO);
		modificacionDatosEntity.setNombreRazonSocial(TEST_DAO_NOMBRE_RAZON_SOCIAL);

	}

	protected static void prepareMockPuestaMercado(PuestaMercadoEntity puestaMercadoEntity) {
		SolicitudMock.prepareMocksolicitudEntity(puestaMercadoEntity);
		puestaMercadoEntity.setNombreComercialProducto(TEST_NOMBRE_PRODUCTO);
		setIngredientes(puestaMercadoEntity);
	}

	private static void setIngredientes(PuestaMercadoEntity puestaMercadoEntity) {
		if (puestaMercadoEntity instanceof PuestaMercadoCAEntity) {
			((PuestaMercadoCAEntity) puestaMercadoEntity).setIngredientes(prepareMockIngredientesCAEntity());
		} else if (puestaMercadoEntity instanceof PuestaMercadoAGEntity) {
			((PuestaMercadoAGEntity) puestaMercadoEntity).setIngredientes(prepareMockIngredientesAGEntity());
		}

	}

	private static IngredientesEntity prepareMockIngredientesCAEntity() {
		IngredientesEntity ingredientesEntity = new IngredientesEntity();

		ingredientesEntity.setIncluyeNuevosIngredientes(true);
		ingredientesEntity.setIncluyeVitaminas(true);
		ingredientesEntity.setNuevoIngrediente("nuevoIngrediente");
		ingredientesEntity.setOtrasSustancias("Otras");
		SituacionEntity situacionEntity = SolicitudMock.prepareMockSituacionEntity();
		ingredientesEntity.setSituacion(situacionEntity);

		return ingredientesEntity;
	}

	private static String prepareMockIngredientesAGEntity() {
		return INGREDIENTES_PUESTA_MERCADO_ALIMENTOS_GRUPO;
	}

}
