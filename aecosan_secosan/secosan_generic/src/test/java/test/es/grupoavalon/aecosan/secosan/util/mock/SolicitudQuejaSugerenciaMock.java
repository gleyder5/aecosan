package test.es.grupoavalon.aecosan.secosan.util.mock;

import java.sql.Timestamp;

import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.SolicitudQuejaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.quejaSugerencia.TipoComunicacionDTO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.AbstractSolicitudesQuejaSugerencia;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudQuejaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.SolicitudSugerenciaEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.quejaSugerencia.TipoComunicacionEntity;

public class SolicitudQuejaSugerenciaMock {

	public static final String IDENTIFICADOR_PETICION = "20160504152415xxx";
	public static final String TEST_UNITY = "test unity";
	public static final String TEST_MOTIVE = "test motive";
	public static final Timestamp TEST_DATE_TIME = new Timestamp(1466409955L);
	public static final Long COMUNICATION_TYPE_ID = 1L;

	private static void prepareMockSolicitudQuejaEntity(AbstractSolicitudesQuejaSugerencia solicitudQuejaSugerenciaEntity) {
		SolicitudMock.prepareMocksolicitudEntity(solicitudQuejaSugerenciaEntity);
		solicitudQuejaSugerenciaEntity.setUnidad(TEST_UNITY);
		solicitudQuejaSugerenciaEntity.setMotive(TEST_MOTIVE);
		solicitudQuejaSugerenciaEntity.setDateTimeIncidence(TEST_DATE_TIME);
		solicitudQuejaSugerenciaEntity.setComunicationType(generateComunicationType());

	}

	public static SolicitudQuejaEntity generateSolicitudQuejaEntity() {
		SolicitudQuejaEntity solicitudQuejaEntity = new SolicitudQuejaEntity();
		prepareMockSolicitudQuejaEntity(solicitudQuejaEntity);
		return solicitudQuejaEntity;
	}


	public static SolicitudSugerenciaEntity generateSolicitudSugerenciaEntity() {
		SolicitudSugerenciaEntity solicitudSugerenciaEntity = new SolicitudSugerenciaEntity();
		prepareMockSolicitudQuejaEntity(solicitudSugerenciaEntity);
		return solicitudSugerenciaEntity;
	}



	private static TipoComunicacionEntity generateComunicationType() {
		TipoComunicacionEntity tipoComunicacionEntity = new TipoComunicacionEntity();
		tipoComunicacionEntity.setId(COMUNICATION_TYPE_ID);
		return tipoComunicacionEntity;
	}

	public static SolicitudQuejaDTO generateSolicitudQuejaDto() {
		SolicitudQuejaDTO solicitudQuejaDTO = new SolicitudQuejaDTO();
		prepareMockSolicitudQuejaDTO(solicitudQuejaDTO);
		return solicitudQuejaDTO;
	}

	private static void prepareMockSolicitudQuejaDTO(SolicitudQuejaDTO solicitudQuejaDTO) {
		SolicitudMock.prepareMockSolicitudQuejaSugerenciaDTO(solicitudQuejaDTO);
		solicitudQuejaDTO.setUnity(TEST_UNITY);
		solicitudQuejaDTO.setMotive(TEST_MOTIVE);
		solicitudQuejaDTO.setDateTimeIncidence(TEST_DATE_TIME.getTime());
		solicitudQuejaDTO.setComunicationType(generateComunicationDTO());
	}

	private static TipoComunicacionDTO generateComunicationDTO() {
		TipoComunicacionDTO tipoComunicacionDTO = new TipoComunicacionDTO();
		tipoComunicacionDTO.setId(String.valueOf(COMUNICATION_TYPE_ID));
		return tipoComunicacionDTO;
	}

}
