package test.es.grupoavalon.aecosan.secosan.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.dao.TipoPersonaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.TipoPersonaEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

/**
 * 
 * @author xi314616
 *
 */
public class TipoDAOImplTest extends GenericDBTestClass {

	@Autowired
	private TipoPersonaDAO tipoPersonaDAO;

	private static final Long TIPO_PERSONA_ID_1 = 1l;
	private static final Long TIPO_PERSONA_ID_NOT_EXIST = 555552l;
	private static final String TIPO_PERSONA1_DESCRIPCION = "Natural";

	@Test
	public void testTipoPersonaFindByIdOK() {
		TipoPersonaEntity persona = tipoPersonaDAO.findTipoPersona(TIPO_PERSONA_ID_1);
		assertEquals(persona.getCatalogValue(), TIPO_PERSONA1_DESCRIPCION);
	}

	@Test
	public void testTipoPersonaFindByIdNotExist() {
		TipoPersonaEntity persona = tipoPersonaDAO.findTipoPersona(TIPO_PERSONA_ID_NOT_EXIST);
		assertNull(persona);
	}

	@Test(expected = DaoException.class)
	public void testTipoPersonaFindByIdNull() {
		tipoPersonaDAO.findTipoPersona(null);
	}

}
