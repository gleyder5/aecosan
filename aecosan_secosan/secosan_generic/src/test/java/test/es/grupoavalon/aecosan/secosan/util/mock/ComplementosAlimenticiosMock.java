package test.es.grupoavalon.aecosan.secosan.util.mock;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.CeseComercializacionCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.ModificacionDatosCAEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos.complementosAlimenticios.PuestaMercadoCAEntity;

public class ComplementosAlimenticiosMock extends AlimentosMock {

	public static CeseComercializacionCAEntity generateCeseComercializacionEntity() {
		CeseComercializacionCAEntity ceseComercializacionEntity = new CeseComercializacionCAEntity();
		prepareMockCeseComercializacion(ceseComercializacionEntity);
		return ceseComercializacionEntity;
	}

	public static PuestaMercadoCAEntity generatePuestaMercadoEntity() {
		PuestaMercadoCAEntity puestaMercadoEntity = new PuestaMercadoCAEntity();
		prepareMockPuestaMercado(puestaMercadoEntity);
		return puestaMercadoEntity;
	}

	public static ModificacionDatosCAEntity generateModificacionDatosEntity() {
		ModificacionDatosCAEntity modificacionDatosEntity = new ModificacionDatosCAEntity();
		prepareMockModificacionDatos(modificacionDatosEntity);
		return modificacionDatosEntity;
	}

}
