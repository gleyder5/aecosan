package test.es.grupoavalon.aecosan.secosan.business.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import es.grupoavalon.aecosan.secosan.business.UserService;
import es.grupoavalon.aecosan.secosan.business.dto.user.LoginUserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserNoEUDTO;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import test.es.grupoavalon.aecosan.secosan.testutils.spring.GenericDBTestClass;

public class UserServiceImplTest extends GenericDBTestClass {

	private static final String EXISTING_USER_NOUE_LOGIN_1 = "extracom";
	private static final String EXISTING_USER_NOUE_PASSWORD_1 = "Cuenca";
	private static final String EXISTING_USER_NOUE_ID_1 = "1";
	private static final String EXISTING_USER_NOUE_LOGIN_2 = "extracom";
	private static final String EXISTING_USER_NOUE_IDENTIFICATIONNUMBER_ID1 = "26134098A";
	private static final String UNEXISTING_USER_NOUE_LOGIN = "Cuenca";
	private static final String EXISTING_USER_NOUE_WRONG_PASSWORD = "Wrong";
	private static final String EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID4 = "46134098A";
	private static final String EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID5 = "56134098A";
	private static final String UNEXISTING_USER_CLAVE_IDENTIFICATIONNUMBER = "654";
	private static final Long EXISTING_USER_CLAVE_LAST_LOGIN = 1475328255723L;
	private static final String NEW_USER_NOUE_LOGIN = "userlogin";
	private static final String NEW_USER_NOUE_PASSWORD = "passwordNOUE";
	private static final String NEW_USER_NOUE_IDENTIFICATIONNUMBER = "1675349";
	private static final String NEW_USER_CLAVE_IDENTIFICATIONNUMBER = "8375349";
	private static final String EXISTING_USER_INTERNAL_LOGIN = "User5";
	private static final String UNEXISTING_USER_INTERNAL_LOGIN = "FAKELOGIN";
	private static final String FAKE_IDENTIFICATIONNUMBER = "FAKELOGIN";
	private static final String EXISTING_USER_INTERNAL_PASSWORD_ID5 = "Cuenca";
	private static final String EXISTING_USER_INTERNAL_ID_5 = "5";
	private static final String USER_TRAMITADOR_ROLE = "2";
	private static final String NEW_USER_TRAMITADOR_NAME = "NTR1";
	private static final String NEW_USER_TRAMITADOR_LAST_NAME = "NTR1 last";
	private static final String NEW_USER_TRAMITADOR_LOGIN = "NTR1";
	private static final String NEW_USER_TRAMITADOR_PASS = "1234";
	private static final String USER_ADMIN_ROLE = "3";
	private static final String NEW_USER_ADMIN_NAME = "NAD1";
	private static final String NEW_USER_ADMIN_LAST_NAME = "NAD1 last";
	private static final String NEW_USER_ADMIN_LOGIN = "NAD1";
	private static final String NEW_USER_ADMIN_PASS = "12345";
	private static final String USER_AREA_1 = "1";


	@Autowired
	UserService userService;

	@Test
	public void testDoLoginOk() throws AutenticateException {
		LoginUserDTO loginUserDto = userService.doLogin(EXISTING_USER_NOUE_LOGIN_1, EXISTING_USER_NOUE_PASSWORD_1);
		assertEquals(loginUserDto.getId(), EXISTING_USER_NOUE_ID_1);
	}

	@Test(expected = AutenticateException.class)
	public void testDoLoginKoUserBlocked() throws AutenticateException {
		callWithInvalidPasswordToBlockUser();
		userService.doLogin(EXISTING_USER_NOUE_LOGIN_2, EXISTING_USER_NOUE_WRONG_PASSWORD);

	}

	private void callWithInvalidPasswordToBlockUser() {
		try {
			userService.doLogin(EXISTING_USER_NOUE_LOGIN_2, EXISTING_USER_NOUE_WRONG_PASSWORD);
		} catch (Exception e) {
		}

	}

	@Test(expected = AutenticateException.class)
	public void testDoLoginKoUnexistingUser() throws AutenticateException {
		userService.doLogin(UNEXISTING_USER_NOUE_LOGIN, EXISTING_USER_NOUE_PASSWORD_1);
	}

	@Test(expected = AutenticateException.class)
	public void testDoLoginKoWrongPassWord() throws AutenticateException {
		userService.doLogin(EXISTING_USER_NOUE_LOGIN_1, EXISTING_USER_NOUE_WRONG_PASSWORD);
	}

	@Test
	public void testDoClaveLoginOkExistingUser() throws AutenticateException {
		LoginUserDTO loginUser = userService.doClaveLogin(generateExistingUserClave(EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID5));
		assertEquals(EXISTING_USER_CLAVE_LAST_LOGIN, loginUser.getLastLogin());
	}

	@Test
	public void testDoClaveLoginOkExistingUserAndUpdateLastLogin() throws AutenticateException {
		LoginUserDTO loginUser = userService.doClaveLogin(generateExistingUserClave(EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID4));
		assertEquals(EXISTING_USER_CLAVE_LAST_LOGIN, loginUser.getLastLogin());
		loginUser = userService.doClaveLogin(generateExistingUserClave(EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID4));
		assertTrue(EXISTING_USER_CLAVE_LAST_LOGIN < loginUser.getLastLogin());
	}

	private UserClaveDTO generateExistingUserClave(String identificationNumber) {
		UserClaveDTO user = new UserClaveDTO();
		user.setIdentificationNumber(identificationNumber);
		return user;
	}

	@Test
	public void testDoClaveLoginOkUnExistingUser() throws AutenticateException {
		LoginUserDTO loginUser = userService.doClaveLogin(generateExistingUserClave(UNEXISTING_USER_CLAVE_IDENTIFICATIONNUMBER));
		assertNotNull(loginUser);
	}

	@Test
	public void testDoregistryOkUserNoUE() throws AutenticateException {
		userService.doRegistry(generateUserNoUE());
		LoginUserDTO loginUserDto = userService.doLogin(NEW_USER_NOUE_LOGIN, NEW_USER_NOUE_PASSWORD);
		assertNotNull(loginUserDto);

	}

	private UserNoEUDTO generateUserNoUE() {
		UserNoEUDTO user = new UserNoEUDTO();
		user.setLogin(NEW_USER_NOUE_LOGIN);
		user.setPassword(NEW_USER_NOUE_PASSWORD);
		user.setIdentificationNumber(NEW_USER_NOUE_IDENTIFICATIONNUMBER);
		return user;
	}

	@Test
	public void testDoregistryOkUserclave() throws AutenticateException {
		userService.doRegistry(generateClave());
		LoginUserDTO loginUser = userService.doClaveLogin(generateClave());
		assertNotNull(loginUser);
	}

	private UserClaveDTO generateClave() {
		UserClaveDTO user = new UserClaveDTO();
		user.setIdentificationNumber(NEW_USER_CLAVE_IDENTIFICATIONNUMBER);
		return user;
	}

	@Test
	public void testLoginIsTakenOk() {
		assertTrue(userService.loginIsTaken(EXISTING_USER_NOUE_LOGIN_1));
		assertFalse(userService.loginIsTaken(UNEXISTING_USER_NOUE_LOGIN));
	}

	@Test
	public void testLoginIsTakenInternalOk() {
		assertTrue(userService.loginIsTakenInternal(EXISTING_USER_INTERNAL_LOGIN));
		assertFalse(userService.loginIsTakenInternal(UNEXISTING_USER_INTERNAL_LOGIN));
	}

	@Test
	public void testGetUserNOUEByIdenticationDocumentOk() {
		LoginUserDTO user = userService.getExternalUserByIdenticationDocument(FAKE_IDENTIFICATIONNUMBER);
		assertNull(user);
		user = userService.getExternalUserByIdenticationDocument(EXISTING_USER_NOUE_IDENTIFICATIONNUMBER_ID1);
		assertNotNull(user);
	}

	@Test
	public void testGetUserClaveByIdenticationDocumentOk() {
		LoginUserDTO user = userService.getExternalUserByIdenticationDocument(FAKE_IDENTIFICATIONNUMBER);
		assertNull(user);
		user = userService.getExternalUserByIdenticationDocument(EXISTING_USER_CLAVE_IDENTIFICATIONNUMBER_ID4);
		assertNotNull(user);
	}

	@Test
	public void testDoAdminLoginOk() throws AutenticateException {
		LoginUserDTO loginUserDto = userService.doLoginAdmin(EXISTING_USER_INTERNAL_LOGIN, EXISTING_USER_INTERNAL_PASSWORD_ID5);
		assertEquals(loginUserDto.getId(), EXISTING_USER_INTERNAL_ID_5);
	}

	@Test
	public void testDoregistryOkTramitador() throws AutenticateException {
		userService.doRegistry(generateTramitador());
		LoginUserDTO loginUser = userService.doClaveLogin(generateClave());
		assertNotNull(loginUser);
	}

	private UserDTO generateTramitador() {
		UserInternalDTO user = new UserInternalDTO();
		user.setRoleId(USER_TRAMITADOR_ROLE);
		user.setAreaId(USER_AREA_1);
		user.setName(NEW_USER_TRAMITADOR_NAME);
		user.setLastName(NEW_USER_TRAMITADOR_LAST_NAME);
		user.setLogin(NEW_USER_TRAMITADOR_LOGIN);
		user.setPassword(NEW_USER_TRAMITADOR_PASS);
		return user;
	}

	@Test
	public void testDoregistryOkAdmin() throws AutenticateException {
		userService.doRegistry(generateAdmin());
		LoginUserDTO loginUser = userService.doClaveLogin(generateClave());
		assertNotNull(loginUser);
	}

	private UserDTO generateAdmin() {
		UserInternalDTO user = new UserInternalDTO();
		user.setRoleId(USER_ADMIN_ROLE);
		user.setAreaId(USER_AREA_1);
		user.setName(NEW_USER_ADMIN_NAME);
		user.setLastName(NEW_USER_ADMIN_LAST_NAME);
		user.setLogin(NEW_USER_ADMIN_LOGIN);
		user.setPassword(NEW_USER_ADMIN_PASS);
		return user;
	}
}
