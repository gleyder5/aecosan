package test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto;


public class DtoNotAnnotated {


	private String myDtoStringAtribute;
	
	private int myDtoIntValue;
	
	private Integer myDtoIntegerValueIgnore;

	public String getMyDtoStringAtribute() {
		return myDtoStringAtribute;
	}

	public void setMyDtoStringAtribute(String myDtoStringAtribute) {
		this.myDtoStringAtribute = myDtoStringAtribute;
	}

	public int getMyDtoIntValue() {
		return myDtoIntValue;
	}

	public void setMyDtoIntValue(int myDtoIntValue) {
		this.myDtoIntValue = myDtoIntValue;
	}

	public Integer getMyDtoIntegerValueIgnore() {
		return myDtoIntegerValueIgnore;
	}

	public void setMyDtoIntegerValueIgnore(Integer myDtoIntegerValueIgnore) {
		this.myDtoIntegerValueIgnore = myDtoIntegerValueIgnore;
	}
	
}
