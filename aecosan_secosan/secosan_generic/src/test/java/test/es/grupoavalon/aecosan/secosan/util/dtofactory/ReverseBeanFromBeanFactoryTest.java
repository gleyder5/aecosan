package test.es.grupoavalon.aecosan.secosan.util.dtofactory;

import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.DtoMock;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.DtoNotAnnotated;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.EntityMock;
import test.es.grupoavalon.aecosan.secosan.util.dtofactory.testdto.EntityRelatedMock;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.exception.BeanFactoryException;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.BeanFromBeanFactory;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.specific.ReverseBeanFromBeanFactory;

public class ReverseBeanFromBeanFactoryTest extends GenericDtoFactoryTest {

	private EntityMock mockEntityExpected;
	private DtoMock filledDTOObject;
	private List<EntityMock> mockEntityExpectedList;
	private List<DtoMock> filledDTOObjectList;

	@Test
	public void testGenerateEntityFromDTO() {
		this.setupTestDataSingle();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForOneToOneTransformation(filledDTOObject);
		EntityMock filledEntity = factory.generateEntityFromDTO();
		getLogger().debug("filled Entity:" + filledEntity);
		getLogger().debug("expected Entity:" + mockEntityExpected);
		Assert.assertNotNull(filledEntity);
		assertEntityMockEquals(mockEntityExpected, filledEntity, true);
	}

	@Test
	public void testGenerateEntityFromDTONoCollections() {
		this.setupTestDataSingle();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForOneToOneTransformation(filledDTOObject);
		EntityMock filledEntity = factory.generateEntityFromDTONoCollections();
		getLogger().debug("filled Entity:" + filledEntity);
		getLogger().debug("expected Entity:" + mockEntityExpected);
		Assert.assertNotNull(filledEntity);
		assertListNullOrEmpty(filledEntity.getMyOneToManyRelationship());
		assertEntityMockEquals(mockEntityExpected, filledEntity, false);
		assertListNullOrEmpty(filledEntity.getMyOneToManyRelationship());
	}

	@Test
	public void testGenerateEntityFromDTOOnlyCollections() {
		this.setupTestDataSingle();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForOneToOneTransformation(filledDTOObject);
		EntityMock filledEntity = factory
				.generateEntityFromDTOOnlyCollections();
		getLogger().debug("filled Entity:" + filledEntity);
		getLogger().debug("expected Entity:" + mockEntityExpected);
		Assert.assertNotNull(filledEntity);
		assertListNotNullOrNotEmpty(filledEntity.getMyOneToManyRelationship());
		assertAllRelatedListAreEqual(
				mockEntityExpected.getMyOneToManyRelationship(),
				filledEntity.getMyOneToManyRelationship());
		assertEntityMockHaveAllValuesNullExceptCollections(filledEntity);

	}

	private void setupTestDataSingle() {
		this.mockEntityExpected = fillEntity();
		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoMock> factory = (BeanFromBeanFactory<DtoMock>) BeanFromBeanFactory
				.getInstance(this.mockEntityExpected, DtoMock.class);
		filledDTOObject = factory.generateDTOFromEntity();
	}

	private static ReverseBeanFromBeanFactory<EntityMock> getFactoryForOneToOneTransformation(
			DtoMock mockDto) {

		@SuppressWarnings("unchecked")
		ReverseBeanFromBeanFactory<EntityMock> factory = (ReverseBeanFromBeanFactory<EntityMock>) ReverseBeanFromBeanFactory
				.getInstance(mockDto, EntityMock.class);
		return factory;
	}

	@Test
	public void testGenerateListEntityFromDTO() {
		this.setupTestDataList();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForCollectionTransformation();
		this.getLogger().debug("filledDTOObjectList:"+this.filledDTOObjectList);
		List<EntityMock> transformedList = factory
				.generateListEntityFromDTO(this.filledDTOObjectList);
		assertListNotNullOrNotEmpty(transformedList);
		assertListTransformedIsEqualToExpected(this.mockEntityExpectedList,
				transformedList, true);

	}

	@Test
	public void testGenerateListEntityFromDTONoCollections() {
		this.setupTestDataList();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForCollectionTransformation();
		List<EntityMock> transformedList = factory
				.generateListEntityFromDTONoCollections(this.filledDTOObjectList);
		assertListNotNullOrNotEmpty(transformedList);
		assertListTransformedIsEqualToExpected(this.mockEntityExpectedList,
				transformedList, false);
	}

	@Test
	public void testGenerateListEntityFromDTOOnlyCollections() {
		this.setupTestDataList();
		ReverseBeanFromBeanFactory<EntityMock> factory = getFactoryForCollectionTransformation();
		List<EntityMock> transformedList = factory
				.generateListEntityFromDTOOnlyCollections(this.filledDTOObjectList);
		assertListNotNullOrNotEmpty(transformedList);
		assertListTransformedHaveAllValuesNull(this.mockEntityExpectedList,
				transformedList);
	}

	private void setupTestDataList() {
		this.mockEntityExpectedList = fillEntityList();
		@SuppressWarnings("unchecked")
		BeanFromBeanFactory<DtoMock> factory = (BeanFromBeanFactory<DtoMock>) BeanFromBeanFactory
				.getInstance(this.mockEntityExpected, DtoMock.class);
		this.filledDTOObjectList = factory
				.generateListDTOFromEntity(this.mockEntityExpectedList);
	}

	private static ReverseBeanFromBeanFactory<EntityMock> getFactoryForCollectionTransformation() {

		@SuppressWarnings("unchecked")
		ReverseBeanFromBeanFactory<EntityMock> factory = (ReverseBeanFromBeanFactory<EntityMock>) ReverseBeanFromBeanFactory
				.getInstance(EntityMock.class);
		return factory;
	}

	private static void assertListTransformedIsEqualToExpected(
			List<EntityMock> expected, List<EntityMock> obtained,
			boolean includeCollections) {
		Assert.assertEquals(expected.size(), expected.size());
		for (int i = 0; i < expected.size(); i++) {
			EntityMock expectedEntity = expected.get(i);
			EntityMock obtainedEntity = obtained.get(i);
			List<EntityRelatedMock> expectedRelatedList = expectedEntity
					.getMyOneToManyRelationship();
			List<EntityRelatedMock> obtainedRelatedList = obtainedEntity
					.getMyOneToManyRelationship();
			assertEntityMockEquals(expectedEntity, obtainedEntity,
					includeCollections);
			assertAllRelatedEntityListsAreEquals(expectedRelatedList,
					obtainedRelatedList, includeCollections);

		}
	}
	
	private static void assertListTransformedHaveAllValuesNull(
			List<EntityMock> expected, List<EntityMock> obtained) {
		Assert.assertEquals(expected.size(), expected.size());
		for (int i = 0; i < expected.size(); i++) {
			EntityMock expectedEntity = expected.get(i);
			EntityMock obtainedEntity = obtained.get(i);
			List<EntityRelatedMock> expectedRelatedList = expectedEntity
					.getMyOneToManyRelationship();
			List<EntityRelatedMock> obtainedRelatedList = obtainedEntity
					.getMyOneToManyRelationship();
			assertEntityMockHaveAllValuesNullExceptCollections(obtainedEntity);
			assertAllRelatedEntityListsAreEquals(expectedRelatedList,
					obtainedRelatedList, true);

		}
	}

	private static void assertAllRelatedEntityListsAreEquals(
			List<EntityRelatedMock> expectedRelatedList,
			List<EntityRelatedMock> obtainedRelatedList,
			boolean includeCollections) {

		if (includeCollections) {
			assertAllRelatedListAreEqual(expectedRelatedList,
					obtainedRelatedList);
		} else {
			assertListNullOrEmpty(obtainedRelatedList);
		}
	}

	private static void assertEntityMockEquals(EntityMock expected,
			EntityMock obtained, boolean includeCollections) {
		if (!expected.equalsReverseFill(obtained, includeCollections)) {
			getLogger(ReverseBeanFromBeanFactoryTest.class)
					.debug("Not equals: expected:" + expected + " obtained"
							+ obtained);
			fail("Generated object is not equal to expected object");
		}
		
	}

	private static void assertAllRelatedListAreEqual(
			List<EntityRelatedMock> expected, List<EntityRelatedMock> obtained) {
		Assert.assertEquals(expected.size(), obtained.size());
		for (int i = 0; i < expected.size(); i++) {
			EntityRelatedMock expectedRelated = expected.get(i);
			EntityRelatedMock obtainedRelated = obtained.get(i);
			if (!expectedRelated.equalsReverseFill(obtainedRelated)) {
				getLogger(ReverseBeanFromBeanFactoryTest.class).debug(
						"Not equals: expected:" + expectedRelated + " obtained"
								+ obtainedRelated);
				fail("Related entities mock are not equals");
			}
		}
	}

	private static void assertEntityMockHaveAllValuesNullExceptCollections(
			EntityMock entityMock) {
		if (entityMock.getMyStringAtribute() != null) {
			fail("MyStringAttribute is not null");
		}
		if (entityMock.getMyIntValue() != 0) {
			fail("MyIntValue is not a default value");
		}
		if (entityMock.getMyIntegerValue() != null) {
			fail("MyIntegerValue is not null");
		}
		if (entityMock.getMyFloatPrimitiveValue() != 0.0f) {
			fail("MyFloatPrimitiveValue is not a default value");
		}

		if (entityMock.getMyFloatValue() != null) {
			fail("MyFloatValue is not null");
		}
		if (entityMock.isMyBoolValue() != false) {
			fail("MyBoolValue is not a default value");
		}
		if (entityMock.getMyBooleanValue() != null) {
			fail("MyStringAttribute is not null");
		}
		if (entityMock.getHaveSameNameString() != null) {
			fail("HaveSameNameString() is not null");
		}
		if (entityMock.getHaveSameNameInt() != 0) {
			fail("HaveSameNameInt is not a default value");
		}
		if (entityMock.getMyManyToOneRelationship() != null) {
			fail("MyManyToOneRelationship is not null");
		}

	}
	
	@Test(expected=BeanFactoryException.class)
	public void testGenerateEntityFromDTONotAnnotated() {
		DtoNotAnnotated notAnoAnnotated = fillPojo(DtoNotAnnotated.class);
		@SuppressWarnings("unchecked")
		ReverseBeanFromBeanFactory<EntityMock> factory = (ReverseBeanFromBeanFactory<EntityMock>) ReverseBeanFromBeanFactory
				.getInstance(notAnoAnnotated, EntityMock.class);
		factory.generateEntityFromDTO();
		
	}
	
	@Test(expected=BeanFactoryException.class)
	public void testGenerateEntityFromDTONotAnnotatedLIst() {
		List<DtoNotAnnotated> notAnoAnnotated = fillPojoList(DtoNotAnnotated.class);
		@SuppressWarnings("unchecked")
		ReverseBeanFromBeanFactory<EntityMock> factory = (ReverseBeanFromBeanFactory<EntityMock>) ReverseBeanFromBeanFactory
				.getInstance(EntityMock.class);
		factory.generateListEntityFromDTO(notAnoAnnotated);
		
	}

}
