package test.es.grupoavalon.aecosan.secosan.rest.facade;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import es.grupoavalon.aecosan.secosan.business.dto.MunicipioDTO;
import es.grupoavalon.aecosan.secosan.business.dto.PaisDTO;
import es.grupoavalon.aecosan.secosan.business.dto.TipoPersonaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.TipoUsuarioDTO;
import es.grupoavalon.aecosan.secosan.business.dto.UbicacionGeograficaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.AreaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.DocumentacionDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.TipoSolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.UsuarioRolDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.UsuarioSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos.CeseComercializacionDTO;
import es.grupoavalon.aecosan.secosan.rest.facade.BusinessFacade;

public class BusinessFacadeTest extends GenericRestTest {

	@Autowired
	private BusinessFacade bfacade;

	private static final String IDENTIFICADOR_PETICION = "20160504152415xxx";
	private static final String USER = "1";
	private static final Long STATUS_ID = 1L;
	private static final String AREA_ID = "1";

	@Test
	public void testComplementosAlimenticiosRestServiceTestAddCeseComercializacion() {
		testCeseComercializacionAddOk(generateMockCeseComercializacionDTO());
	}

	/**
	 * Método para persistir un objeto de tipo CeseComercializacionDTO
	 * 
	 * @param ceseComercializacionDTO
	 */
	private void testCeseComercializacionAddOk(CeseComercializacionDTO ceseComercializacionDTO) {
		bfacade.addSolicitud(USER, ceseComercializacionDTO);
	}

	private CeseComercializacionDTO generateMockCeseComercializacionDTO() {
		CeseComercializacionDTO ceseComercializacionDTO = new CeseComercializacionDTO();

		// Cese Comercialización
		ceseComercializacionDTO.setObservaciones("Observación Test");
		ceseComercializacionDTO.setNombreComercialProducto("Nombre comercial Test");
		ceseComercializacionDTO.setSolicitante(generateMockUsuarioSolicitanteDTO());
		ceseComercializacionDTO.setDocumentacion(generateMockDocumentacionListDTO());
		ceseComercializacionDTO.setStatus(STATUS_ID);
		ceseComercializacionDTO.setIdentificadorPeticion(IDENTIFICADOR_PETICION);
		ceseComercializacionDTO.setFormulario(generateMockFormularioDTO());
		ceseComercializacionDTO.setArea(ceseComercializacionDTO.getFormulario().getRelatedArea());
		return ceseComercializacionDTO;
	}

	private List<DocumentacionDTO> generateMockDocumentacionListDTO() {
		List<DocumentacionDTO> documentacionList = new ArrayList<DocumentacionDTO>();
		return documentacionList;
	}

	private UsuarioSolicitanteDTO generateMockUsuarioSolicitanteDTO() {
		UsuarioSolicitanteDTO usuarioSolicitante = new UsuarioSolicitanteDTO();

		usuarioSolicitante.setName("Nombre Test");
		usuarioSolicitante.setIdentificationNumber("IdentificationNumber Test ");
		usuarioSolicitante.setAddress("Dirección Test");
		usuarioSolicitante.setTelephoneNumber("123456789");
		usuarioSolicitante.setLocation(generateMockLocationDTO());
		usuarioSolicitante.setEmail("correo@test.com");
		usuarioSolicitante.setRole(generateMockUsuarioRolDTO());
		usuarioSolicitante.setTipo(generateMockTipoUsuarioDTO());
		usuarioSolicitante.setPersonType(generateMockTipoPersonaDTO());

		return usuarioSolicitante;
	}

	private UbicacionGeograficaDTO generateMockLocationDTO() {
		UbicacionGeograficaDTO ubicacionGeografica = new UbicacionGeograficaDTO();

		ubicacionGeografica.setMunicipio(generateMockMunicipio());
		ubicacionGeografica.setCountry(generateMockPais());
		ubicacionGeografica.setCodigoPostal("12345");

		return ubicacionGeografica;
	}

	private PaisDTO generateMockPais() {
		PaisDTO pais = new PaisDTO();
		return pais;
	}

	private MunicipioDTO generateMockMunicipio() {
		MunicipioDTO municipio = new MunicipioDTO();
		municipio.setId("5826");
		municipio.setLocalidad("39075000000");
		return municipio;
	}

	private TipoPersonaDTO generateMockTipoPersonaDTO() {
		TipoPersonaDTO tipoPersona = new TipoPersonaDTO();

		tipoPersona.setId("1");
		tipoPersona.setCatalogValue("Tipo Persona Test");

		return tipoPersona;
	}

	private TipoUsuarioDTO generateMockTipoUsuarioDTO() {
		TipoUsuarioDTO tipoUsuario = new TipoUsuarioDTO();

		tipoUsuario.setId("1");
		tipoUsuario.setCatalogValue("Tipo Usuario Test");

		return tipoUsuario;
	}

	private UsuarioRolDTO generateMockUsuarioRolDTO() {
		UsuarioRolDTO usuarioRol = new UsuarioRolDTO();

		usuarioRol.setCatalogValue("Rol Test");
		usuarioRol.setId("1");

		return usuarioRol;
	}

	private TipoSolicitudDTO generateMockFormularioDTO() {
		TipoSolicitudDTO solicitud = new TipoSolicitudDTO();
		solicitud.setId("1");
		solicitud.setRelatedArea(generateMockAreaDTO());
		return solicitud;
	}

	private AreaDTO generateMockAreaDTO() {
		AreaDTO areaDTO = new AreaDTO();

		areaDTO.setCatalogValue("Area Test");
		areaDTO.setId(AREA_ID);
		return areaDTO;
	}

}
