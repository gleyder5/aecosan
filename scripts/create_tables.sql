--------------------------------------------------------
-- Archivo creado  - sábado-diciembre-10-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table SECO_ACCIONES
--------------------------------------------------------

  CREATE TABLE "SECO_ACCIONES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_AGREGACION_PAISES
--------------------------------------------------------

  CREATE TABLE "SECO_AGREGACION_PAISES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"CODIGO_INE" VARCHAR2(255 CHAR), 
	"CONTINENTE_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ALTER_OFER_ALIM_UME
--------------------------------------------------------

  CREATE TABLE "SECO_ALTER_OFER_ALIM_UME" 
   (	"MOTIVO_ALTERACION" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ANTECEDENTES_PROCED
--------------------------------------------------------

  CREATE TABLE "SECO_ANTECEDENTES_PROCED" 
   (	"ID" NUMBER(19,0), 
	"PRIMERA_COMERCIALIZACION_UE" NUMBER(1,0), 
	"PAIS_COMERC_PREVIA_ID" NUMBER(19,0), 
	"PAIS_PROCEDENCIA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_AREAS
--------------------------------------------------------

  CREATE TABLE "SECO_AREAS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_BAJA_OFER_ALIM_UME
--------------------------------------------------------

  CREATE TABLE "SECO_BAJA_OFER_ALIM_UME" 
   (	"FECHA_COMUNICACION" TIMESTAMP (6), 
	"COMPANY_NAME" VARCHAR2(255 CHAR), 
	"COMPANY_REGISTER_NUMBER" VARCHAR2(255 CHAR), 
	"MOTIVO_DURACION_SUSPENSION" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"BAJA_O_SUSPENSION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_BAJA_O_SUSPENSION
--------------------------------------------------------

  CREATE TABLE "SECO_BAJA_O_SUSPENSION" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_CESE_COMERCIALIZACION
--------------------------------------------------------

  CREATE TABLE "SECO_CESE_COMERCIALIZACION" 
   (	"NOMBRE_COMERCIAL_PRODUCTO" VARCHAR2(255 CHAR), 
	"OBSERVACIONES" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_CESE_COMERCIALIZACION_AG
--------------------------------------------------------

  CREATE TABLE "SECO_CESE_COMERCIALIZACION_AG" 
   (	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_CESE_COMERCIALIZACION_CA
--------------------------------------------------------

  CREATE TABLE "SECO_CESE_COMERCIALIZACION_CA" 
   (	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_CONTINENTES
--------------------------------------------------------

  CREATE TABLE "SECO_CONTINENTES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"CODIGO_INE" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_DATOS_CONTACTOS
--------------------------------------------------------

  CREATE TABLE "SECO_DATOS_CONTACTOS" 
   (	"ID" NUMBER(19,0), 
	"DIRECCION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUM_IDENTIFICACION" VARCHAR2(255 CHAR), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"NUM_TELEFONO" NUMBER(19,0), 
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_DATOS_IDENTIF_ADICIONALES
--------------------------------------------------------

  CREATE TABLE "SECO_DATOS_IDENTIF_ADICIONALES" 
   (	"ID" NUMBER(19,0), 
	"ES_FABRICANTE_PRODUCTO" NUMBER(1,0), 
	"RGSEAA" VARCHAR2(255 CHAR), 
	"INCLUSION_SOLICITUD" NUMBER(1,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_DATOS_PAGO
--------------------------------------------------------

  CREATE TABLE "SECO_DATOS_PAGO" 
   (	"ID" NUMBER(19,0), 
	"CUENTA" VARCHAR2(255 CHAR), 
	"NUMERO_TARJETA" VARCHAR2(255 CHAR), 
	"ENTIDAD_BANCARIA" VARCHAR2(255 CHAR), 
	"VENCIMIENTO_TARJETA" TIMESTAMP (6), 
	"JUSTIFICANTE_PAGO_ELECTRONICO" VARCHAR2(255 CHAR), 
	"NRC_PAGO_ELECTRONICO" VARCHAR2(255 CHAR), 
	"FECHA_PAGO" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_DIRECCION_EXT
--------------------------------------------------------

  CREATE TABLE "SECO_DIRECCION_EXT" 
   (	"ID" NUMBER(19,0), 
	"PUERTA" VARCHAR2(50 CHAR), 
	"NUMERO" VARCHAR2(50 CHAR), 
	"ESCALERA" VARCHAR2(50 CHAR), 
	"PISO" VARCHAR2(20 CHAR), 
	"TIPO_VIA_PUBLICA" VARCHAR2(255 CHAR), 
	"NOMBRE_VIA_PUBLICA" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_DOCUMENTOS_SOLICITUD
--------------------------------------------------------

  CREATE TABLE "SECO_DOCUMENTOS_SOLICITUD" 
   (	"ID" NUMBER(19,0), 
	"DOCUMENTO_BINARIO" BLOB, 
	"NOMBRE_DOCUMENTO" VARCHAR2(255 CHAR), 
	"TIPO_DOCUMENTACION_ID" NUMBER(19,0), 
	"SOLICITUD_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ESTADO_FISICO
--------------------------------------------------------

  CREATE TABLE "SECO_ESTADO_FISICO" 
   (	"ID" NUMBER(19,0), 
	"POLVO" NUMBER(1,0), 
	"LIQUIDO" NUMBER(1,0), 
	"SOLUCION_ESTANDAR" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ESTADOS_AREAS
--------------------------------------------------------

  CREATE TABLE "SECO_ESTADOS_AREAS" 
   (	"ID" NUMBER(19,0), 
	"AREA_ID" NUMBER(19,0), 
	"ESTADO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ESTADOS_SOLICITUDES
--------------------------------------------------------

  CREATE TABLE "SECO_ESTADOS_SOLICITUDES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_EVALUACION_RIESGOS
--------------------------------------------------------

  CREATE TABLE "SECO_EVALUACION_RIESGOS" 
   (	"NOMBRE_COMERCIAL" VARCHAR2(255 CHAR), 
	"OBJETO_SOLICITUD" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"TIPO_PAGO_SERVICIO_ID" NUMBER(19,0), 
	"PAGO_SOLICITUD_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FIBRA
--------------------------------------------------------

  CREATE TABLE "SECO_FIBRA" 
   (	"ID" NUMBER(19,0), 
	"FIBRA" NUMBER(1,0), 
	"CANTIDAD_FIBRA" VARCHAR2(255 CHAR), 
	"TIPO_FIBRA" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FICHEROS_INSTRUCCIONES
--------------------------------------------------------

  CREATE TABLE "SECO_FICHEROS_INSTRUCCIONES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"TIPO_FORMULARIO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FICHEROS_INSTRUCC_PAGOS
--------------------------------------------------------

  CREATE TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"TIPO_FORMULARIO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FINANCIACION_ALIMENTOS
--------------------------------------------------------

  CREATE TABLE "SECO_FINANCIACION_ALIMENTOS" 
   (	"EDAD" VARCHAR2(255 CHAR), 
	"DENSIDAD_CALORICA" VARCHAR2(255 CHAR), 
	"COMPANY_NAME" VARCHAR2(255 CHAR), 
	"NUMERO_REGISTRO_EMPRESA" VARCHAR2(255 CHAR), 
	"INDICACIONES" VARCHAR2(255 CHAR), 
	"PESO_MOLECULAR" VARCHAR2(255 CHAR), 
	"TIPO_OFERTA" VARCHAR2(255 CHAR), 
	"OSMOLALIDAD" VARCHAR2(255 CHAR), 
	"OSMOLARIDAD" VARCHAR2(255 CHAR), 
	"NOMBRE_PRODUCTO" VARCHAR2(255 CHAR), 
	"NUMERO_REFERENCIA_PRODUCTO" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"MODO_ADMINISTRACION_ID" NUMBER(19,0), 
	"REPARTO_CALORICO_ID" NUMBER(19,0), 
	"FIBRA_ID" NUMBER(19,0), 
	"GRUPO_PACIENTES_ID" NUMBER(19,0), 
	"ESTADO_FISICO_ID" NUMBER(19,0), 
	"PROPUESTA_ID" NUMBER(19,0), 
	"PAGO_SOLICITUD_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FORMA_PAGO
--------------------------------------------------------

  CREATE TABLE "SECO_FORMA_PAGO" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_FORMAS
--------------------------------------------------------

  CREATE TABLE "SECO_FORMAS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_GRUPO_PACIENTES
--------------------------------------------------------

  CREATE TABLE "SECO_GRUPO_PACIENTES" 
   (	"ID" NUMBER(19,0), 
	"ADULTOS" NUMBER(1,0), 
	"LACTANTES" NUMBER(1,0), 
	"NINOS" NUMBER(1,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_IDENTIFICADOR_PAGO
--------------------------------------------------------

  CREATE TABLE "SECO_IDENTIFICADOR_PAGO" 
   (	"ID" NUMBER(19,0), 
	"DIRECCION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUM_IDENTIFICACION" VARCHAR2(255 CHAR), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"NUM_TELEFONO" NUMBER(19,0), 
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0), 
	"DIRECCION_EXTENDIDA" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_INC_OFER_ALIM_UME
--------------------------------------------------------

  CREATE TABLE "SECO_INC_OFER_ALIM_UME" 
   (	"INSCRIPTION_DATE" TIMESTAMP (6), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_INGREDIENTES
--------------------------------------------------------

  CREATE TABLE "SECO_INGREDIENTES" 
   (	"ID" NUMBER(19,0), 
	"INCLUYE_NUEVOS_INGREDIENTES" NUMBER(1,0), 
	"INCLUYE_VITAMINAS" NUMBER(1,0), 
	"NUEVO_INGREDIENTE" VARCHAR2(255 CHAR), 
	"OTRAS_SUSTANCIAS" VARCHAR2(255 CHAR), 
	"SITUACION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_JWTOKENKEYS
--------------------------------------------------------

  CREATE TABLE "SECO_JWTOKENKEYS" 
   (	"KEY" VARCHAR2(255 CHAR), 
	"ES_ADMIN_KEY" NUMBER(1,0), 
	"FECHA_CREACION" TIMESTAMP (6)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MODIFICACION_DATOS
--------------------------------------------------------

  CREATE TABLE "SECO_MODIFICACION_DATOS" 
   (	"DESCRIPCION_MODIFICACIONES" VARCHAR2(255 CHAR), 
	"MOD_AMPL_PRESENTACIONES" NUMBER(1,0), 
	"MOD_COMPOSICION_CUAL_CUAN" NUMBER(1,0), 
	"MOD_DISENO" NUMBER(1,0), 
	"NIF" VARCHAR2(255 CHAR), 
	"NOMBRE_COMERCIAL" VARCHAR2(255 CHAR), 
	"NOMBRE_RAZON_SOCIAL" VARCHAR2(255 CHAR), 
	"NUEVO_NOMBRE_COMERCIAL" VARCHAR2(255 CHAR), 
	"OTRAS_MODIFICACIONES" NUMBER(1,0), 
	"RGSEAA" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MODIFICACION_DATOS_AG
--------------------------------------------------------

  CREATE TABLE "SECO_MODIFICACION_DATOS_AG" 
   (	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MODIFICACION_DATOS_CA
--------------------------------------------------------

  CREATE TABLE "SECO_MODIFICACION_DATOS_CA" 
   (	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MODO_ADMINISTRACION
--------------------------------------------------------

  CREATE TABLE "SECO_MODO_ADMINISTRACION" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MONITORIZACION_CONECTORES
--------------------------------------------------------

  CREATE TABLE "SECO_MONITORIZACION_CONECTORES" 
   (	"ID" NUMBER(19,0), 
	"SISTEMA" VARCHAR2(255 CHAR), 
	"DESCRIPCION_ERROR" VARCHAR2(1000 CHAR), 
	"CLASE_EXCEPCION" VARCHAR2(255 CHAR), 
	"FECHA_INVOCACION" TIMESTAMP (6), 
	"RESULTADO_OPERACION" NUMBER(1,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_MUNICIPIOS
--------------------------------------------------------

  CREATE TABLE "SECO_MUNICIPIOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"IDLOCALIDAD" VARCHAR2(255 CHAR), 
	"PROVINCIA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PAGO_SOLICITUD
--------------------------------------------------------

  CREATE TABLE "SECO_PAGO_SOLICITUD" 
   (	"ID" NUMBER(19,0), 
	"IMPORTE_EUROS" FLOAT(126), 
	"CODIGO_BARRA" VARCHAR2(255 CHAR), 
	"IDENTIFICADOR_PETICION_SOL" VARCHAR2(255 CHAR), 
	"CANTIDAD_TASAS" NUMBER(10,0), 
	"DATOS_PAGO_ID" NUMBER(19,0), 
	"FORMULARIO_PAGADO" NUMBER(19,0), 
	"IDENTIFICADOR_PAGO_ID" NUMBER(19,0), 
	"FORMA_PAGO_ID" NUMBER(19,0), 
	"SOLICITUD_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PAISES
--------------------------------------------------------

  CREATE TABLE "SECO_PAISES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"CODIGO_INE" VARCHAR2(255 CHAR), 
	"AGREGACION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PERMISO_USUARIOS
--------------------------------------------------------

  CREATE TABLE "SECO_PERMISO_USUARIOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PRESENTACIONES_ENVASES
--------------------------------------------------------

  CREATE TABLE "SECO_PRESENTACIONES_ENVASES" 
   (	"ID" NUMBER(19,0), 
	"PRECIO_VENTA_EMPRESA" VARCHAR2(255 CHAR), 
	"TIPO_ENVASE" VARCHAR2(255 CHAR), 
	"CONTENIDO_ENVASE" VARCHAR2(255 CHAR), 
	"SABOR" VARCHAR2(255 CHAR), 
	"NUMERO_ENVASES" VARCHAR2(255 CHAR), 
	"OFER_ALIM_UME_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PRESENTACIONES_PRODUCTOS
--------------------------------------------------------

  CREATE TABLE "SECO_PRESENTACIONES_PRODUCTOS" 
   (	"ID" NUMBER(19,0), 
	"TIPO_CAPACIDAD_ENVASE" VARCHAR2(255 CHAR), 
	"OTRAS_CARACTERISTICAS" VARCHAR2(255 CHAR), 
	"SABORES" VARCHAR2(255 CHAR), 
	"FORMA_ID" NUMBER(19,0), 
	"PUESTA_MERCADO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PRODUCTOS_SUSPENDIDOS
--------------------------------------------------------

  CREATE TABLE "SECO_PRODUCTOS_SUSPENDIDOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"COD_IDENTIFICATIVO" VARCHAR2(255 CHAR), 
	"NUM_REG_PROD" VARCHAR2(255 CHAR), 
	"BAJA_ALIM_UME_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PROPUESTA
--------------------------------------------------------

  CREATE TABLE "SECO_PROPUESTA" 
   (	"ID" NUMBER(19,0), 
	"CODIGO_SUBTIPO" VARCHAR2(255 CHAR), 
	"CODIGO_TIPO" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PROVINCIAS
--------------------------------------------------------

  CREATE TABLE "SECO_PROVINCIAS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"CODIGO_INE" VARCHAR2(255 CHAR), 
	"PAIS_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PUESTA_MERCADO
--------------------------------------------------------

  CREATE TABLE "SECO_PUESTA_MERCADO" 
   (	"NOMBRE_COMERCIAL" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"ANTECEDENTES_PROCEDENCIA_ID" NUMBER(19,0), 
	"DATOS_IDENTIF_ADIC_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PUESTA_MERCADO_AG
--------------------------------------------------------

  CREATE TABLE "SECO_PUESTA_MERCADO_AG" 
   (	"INGREDIENTES" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"TIPO_PRODUCTO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_PUESTA_MERCADO_CA
--------------------------------------------------------

  CREATE TABLE "SECO_PUESTA_MERCADO_CA" 
   (	"ID" NUMBER(19,0), 
	"INGREDIENTES_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_REGISTROACTIVIDADES
--------------------------------------------------------

  CREATE TABLE "SECO_REGISTROACTIVIDADES" 
   (	"ID" NUMBER(19,0), 
	"COMENTARIO" VARCHAR2(4000 CHAR), 
	"FECHA" TIMESTAMP (6), 
	"ACCION" NUMBER(19,0), 
	"SOLICITUD_ID" NUMBER(19,0), 
	"USUARIO" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_REGISTRO_AGUAS
--------------------------------------------------------

  CREATE TABLE "SECO_REGISTRO_AGUAS" 
   (	"COMERCIAL_PRODUCT_NAME" VARCHAR2(255 CHAR), 
	"DATOS_OFICIALES" VARCHAR2(255 CHAR), 
	"DENOMINACION_VENTA" VARCHAR2(255 CHAR), 
	"LUGAR_EXPLOTACION" VARCHAR2(255 CHAR), 
	"NOMBRE_EN_ORIGEN" VARCHAR2(255 CHAR), 
	"NOMBRE_MANANTIAL" VARCHAR2(255 CHAR), 
	"TRATAMIENTO_AGUA" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"PAIS_ID" NUMBER(19,0), 
	"PAGO_SOLICITUD_ID" NUMBER(19,0), 
	"RGSEAA" VARCHAR2(255 CHAR)
   ) ;
   
--------------------------------------------------------
--  DDL for Table SECO_REGISTRO_INDUSTRIAS
--------------------------------------------------------

  CREATE TABLE "SECO_REGISTRO_INDUSTRIAS" 
   (	"RGSEAA" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"PAIS_ID" NUMBER(19,0), 
	"DATOS_ENVIO_ID" NUMBER(19,0), 
	"PAGO_SOLICITUD_ID" NUMBER(19,0), 
	"TIPO_ENVIO" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_REPARTO_CALORICO
--------------------------------------------------------

  CREATE TABLE "SECO_REPARTO_CALORICO" 
   (	"ID" NUMBER(19,0), 
	"HIDRATOS_CARBONO" VARCHAR2(255 CHAR), 
	"LIPIDOS" VARCHAR2(255 CHAR), 
	"PROTEINAS" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_REPRESENTANTES
--------------------------------------------------------

  CREATE TABLE "SECO_REPRESENTANTES" 
   (	"ID" NUMBER(19,0), 
	"DIRECCION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUM_IDENTIFICACION" VARCHAR2(255 CHAR), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"NUM_TELEFONO" VARCHAR2(20),
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_ROLES_USUARIOS
--------------------------------------------------------

  CREATE TABLE "SECO_ROLES_USUARIOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SERVICIOS
--------------------------------------------------------

  CREATE TABLE "SECO_SERVICIOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"TASA_MODELO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SITUACIONES
--------------------------------------------------------

  CREATE TABLE "SECO_SITUACIONES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOLICITANTES
--------------------------------------------------------

  CREATE TABLE "SECO_SOLICITANTES" 
   (	"ID" NUMBER(19,0), 
	"DIRECCION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUM_IDENTIFICACION" VARCHAR2(255 CHAR), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"NUM_TELEFONO" VARCHAR2(20 CHAR), 
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0), 
	"TIPO_PERSONA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOLICITUDES
--------------------------------------------------------

  CREATE TABLE "SECO_SOLICITUDES" 
   (	"ID" NUMBER(19,0), 
	"FECHA_CREACION" TIMESTAMP (6), 
	"IDENTIFICADOR_PETICION" VARCHAR2(255 CHAR), 
	"NUMERO_REGISTRO" VARCHAR2(255 CHAR), 
	"AREA_ID" NUMBER(19,0), 
	"DATOS_CONTACTO_ID" NUMBER(19,0), 
	"TIPO_ID" NUMBER(19,0), 
	"FORMULARIO_TIPO_ID" NUMBER(19,0), 
	"REPRESENTANTE_ID" NUMBER(19,0), 
	"SOLICITANTE_ID" NUMBER(19,0), 
	"ESTADO_ID" NUMBER(19,0), 
	"USUARIO_CREADOR_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOLICITUD_LOGO
--------------------------------------------------------

  CREATE TABLE "SECO_SOLICITUD_LOGO" 
   (	"FINALIDAD_Y_USO" VARCHAR2(255 CHAR), 
	"MATERIAL_SOLICITADO" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOL_LMR_FITOSANITARIOS
--------------------------------------------------------

  CREATE TABLE "SECO_SOL_LMR_FITOSANITARIOS" 
   (	"SUSTANCIA_ACTIVA" VARCHAR2(255 CHAR), 
	"ALIMENTOS" VARCHAR2(255 CHAR), 
	"PAIS_ORIGEN" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"PAGO_SOLICITUD_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOL_QUEJAS
--------------------------------------------------------

  CREATE TABLE "SECO_SOL_QUEJAS" 
   (	"FECHA_HORA_INCIDENCIA" TIMESTAMP (6), 
	"MOTIVO" VARCHAR2(255 CHAR), 
	"UNIDAD" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"TIPOCOMUNICACION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_SOL_SUGERE
--------------------------------------------------------

  CREATE TABLE "SECO_SOL_SUGERE" 
   (	"FECHA_HORA_INCIDENCIA" TIMESTAMP (6), 
	"MOTIVO" VARCHAR2(255 CHAR), 
	"UNIDAD" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0), 
	"TIPOCOMUNICACION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TASAS_MODELOS
--------------------------------------------------------

  CREATE TABLE "SECO_TASAS_MODELOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"MONTO" FLOAT(126), 
	"MODELO" VARCHAR2(255 CHAR), 
	"CODIGO_TASA" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_T_DOCUMENTOS_T_SOLICITUD
--------------------------------------------------------

  CREATE TABLE "SECO_T_DOCUMENTOS_T_SOLICITUD" 
   (	"TIPO_SOLICITUD_ID" NUMBER(19,0), 
	"TIPO_DOC_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPO_FORMULARIO
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_FORMULARIO" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"DOC_FIRMADO_TEMPLATE" VARCHAR2(255 CHAR), 
	"TIPO_SOLICITUD_ID" NUMBER(19,0), 
	"TASA_MODELO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPO_PAGOS
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_PAGOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPO_PAGO_SERVICIO
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_PAGO_SERVICIO" 
   (	"ID" NUMBER(19,0), 
	"TIPO_PAGO_ID" NUMBER(19,0), 
	"SERVICIO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOSCOM_SOLICITUD
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOSCOM_SOLICITUD" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOS_DOC_FORMULARIO
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOS_DOC_FORMULARIO" 
   (	"ID" NUMBER(19,0), 
	"TIPO_FORMULARIO_ID" NUMBER(19,0), 
	"TIPO_DOCUMENTACION_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOS_DOCUMENTOS
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOS_DOCUMENTOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOS_PERSONAS
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOS_PERSONAS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOS_PRODUCTOS
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOS_PRODUCTOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPOS_SOLICITUDES
--------------------------------------------------------

  CREATE TABLE "SECO_TIPOS_SOLICITUDES" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR), 
	"AREA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPO_SUSPENSION_FINA
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_SUSPENSION_FINA" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_TIPO_USUARIOS
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_USUARIOS" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_UBICACION_GEOGRAFICA
--------------------------------------------------------

  CREATE TABLE "SECO_UBICACION_GEOGRAFICA" 
   (	"ID" NUMBER(19,0), 
	"COD_POSTAL" VARCHAR2(255 CHAR), 
	"PAIS_ID" NUMBER(19,0), 
	"MUNICIPIO_ID" NUMBER(19,0), 
	"PROVINCIA_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_USUARIOS
--------------------------------------------------------

  CREATE TABLE "SECO_USUARIOS" 
   (	"ID" NUMBER(19,0), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUMERO_ID_PERSONA" VARCHAR2(255 CHAR), 
	"ULTIMO_ACCESO" TIMESTAMP (6), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"ROL_ID" NUMBER(19,0), 
	"TIPO_ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_USUARIOS_CLAVE
--------------------------------------------------------

  CREATE TABLE "SECO_USUARIOS_CLAVE" 
   (	"NIVEL_QQAA" VARCHAR2(255 CHAR), 
	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_USUARIOS_EXTERNOS
--------------------------------------------------------

  CREATE TABLE "SECO_USUARIOS_EXTERNOS" 
   (	"ID" NUMBER(19,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_USUARIOS_INTERNOS
--------------------------------------------------------

  CREATE TABLE "SECO_USUARIOS_INTERNOS" 
   (	"FECHA_HORA_BLOQUEO" TIMESTAMP (6), 
	"INTENTOS_FALLIDOS" NUMBER(10,0), 
	"LOGIN" VARCHAR2(255 CHAR), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"BLOQUEO" NUMBER(1,0), 
	"ID" NUMBER(19,0), 
	"AREA_ID" NUMBER(19,0),
	"BORRADO" NUMBER(1,0)
   ) ;
--------------------------------------------------------
--  DDL for Table SECO_USUARIOS_NOEU
--------------------------------------------------------

  CREATE TABLE "SECO_USUARIOS_NOEU" 
   (	"FECHA_HORA_BLOQUEO" TIMESTAMP (6), 
	"INTENTOS_FALLIDOS" NUMBER(10,0), 
	"LOGIN" VARCHAR2(255 CHAR), 
	"PASSWORD" VARCHAR2(255 CHAR), 
	"BLOQUEO" NUMBER(1,0), 
	"ID" NUMBER(19,0)
   ) ;
   
 -------------------------------------------------------
--  DDL for Table SECO_TIPO_ENVIO
--------------------------------------------------------

  CREATE TABLE "SECO_TIPO_ENVIO" 
   (	"ID" NUMBER(19,0), 
	"DESCRIPCION" VARCHAR2(255 CHAR)
   ) ;
   
--------------------------------------------------------
-- Archivo creado  - martes-enero-31-2017   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table SECO_DATOS_ENVIO
--------------------------------------------------------

  CREATE TABLE "SECO_DATOS_ENVIO" 
   (	"ID" NUMBER(19,0), 
	"DIRECCION" VARCHAR2(255 CHAR), 
	"EMAIL" VARCHAR2(255 CHAR), 
	"NUM_IDENTIFICACION" VARCHAR2(255 CHAR), 
	"APELLIDO" VARCHAR2(255 CHAR), 
	"NOMBRE" VARCHAR2(255 CHAR), 
	"SEGUNDO_APELLIDO" VARCHAR2(255 CHAR), 
	"NUM_TELEFONO" NUMBER(19,0), 
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0)
   ) ;

create table SECO_TIPO_PROCEDIMIENTO
(
  ID NUMBER(19) not null
    primary key,
  ACTIVO NUMBER(1),
  DESCRIPCION VARCHAR2(255 char) not null
);
create table SECO_HISTORIAL_SOLICITUD
(
  ID NUMBER(19) not null
    primary key,
  DESCRIPCION VARCHAR2(255 char),
  FECHA TIMESTAMP(6) not null,
  SOLICITUD_ID NUMBER(19) not null,

  ESTADO_ID NUMBER(19) not null
);

create table SECO_PROCEDIMIENTO_GENERAL
(
  ASUNTO VARCHAR2(255 char),
  EXPONE VARCHAR2(255 char),
  SOLICITA VARCHAR2(255 char),
  ID NUMBER(19) not null primary key,
  TIPO VARCHAR2(255 char)
);

create table SECO_LUGAR_MEDIO_NOTIFICACION
(
  ID NUMBER(19) not null
    primary key,
  EMAIL VARCHAR2(255 char),
  FAX VARCHAR2(255 char),
	"UBICACION_GEOGRAFICA_ID" NUMBER(19,0)
);

create table SECO_REGISTRO_REACU
(
  FECHA_BOE TIMESTAMP(6) not null,
  ID NUMBER(19) not null
    primary key,
  LUGAR_MEDIO_NOTIFICACION_ID NUMBER(19)
);

create table SECO_SUBVENCIONES_ASOCUAE
(
  ID NUMBER(19) not null primary key,
	LUGAR_MEDIO_NOTIFICACION_ID NUMBER(19)
);
--------------------------------------------------------
--  DDL for Index SYS_C0034260
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034260" ON "SECO_ACCIONES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034265
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034265" ON "SECO_AGREGACION_PAISES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034267
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034267" ON "SECO_ALTER_OFER_ALIM_UME" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034269
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034269" ON "SECO_ANTECEDENTES_PROCED" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034272
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034272" ON "SECO_AREAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034275
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034275" ON "SECO_BAJA_O_SUSPENSION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034277
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034277" ON "SECO_BAJA_OFER_ALIM_UME" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034279
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034279" ON "SECO_CESE_COMERCIALIZACION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034281
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034281" ON "SECO_CESE_COMERCIALIZACION_AG" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034283
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034283" ON "SECO_CESE_COMERCIALIZACION_CA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034287
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034287" ON "SECO_CONTINENTES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034289
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034289" ON "SECO_DATOS_CONTACTOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034291
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034291" ON "SECO_DATOS_IDENTIF_ADICIONALES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034293
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034293" ON "SECO_DATOS_PAGO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034295
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034295" ON "SECO_DIRECCION_EXT" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034298
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034298" ON "SECO_DOCUMENTOS_SOLICITUD" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034300
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034300" ON "SECO_ESTADO_FISICO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034304
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034304" ON "SECO_ESTADOS_AREAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034307
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034307" ON "SECO_ESTADOS_SOLICITUDES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034309
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034309" ON "SECO_EVALUACION_RIESGOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034311
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034311" ON "SECO_FIBRA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034315
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034315" ON "SECO_FICHEROS_INSTRUCC_PAGOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034319
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034319" ON "SECO_FICHEROS_INSTRUCCIONES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034321
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034321" ON "SECO_FINANCIACION_ALIMENTOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034324
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034324" ON "SECO_FORMA_PAGO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034327
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034327" ON "SECO_FORMAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034329
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034329" ON "SECO_GRUPO_PACIENTES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034331
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034331" ON "SECO_IDENTIFICADOR_PAGO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034333
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034333" ON "SECO_INC_OFER_ALIM_UME" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034336
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034336" ON "SECO_INGREDIENTES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034338
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034338" ON "SECO_JWTOKENKEYS" ("KEY") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034340
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034340" ON "SECO_MODIFICACION_DATOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034342
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034342" ON "SECO_MODIFICACION_DATOS_AG" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034344
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034344" ON "SECO_MODIFICACION_DATOS_CA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034347
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034347" ON "SECO_MODO_ADMINISTRACION" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034352
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034352" ON "SECO_MONITORIZACION_CONECTORES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034356
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034356" ON "SECO_MUNICIPIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034360
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034360" ON "SECO_PAGO_SOLICITUD" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034365
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034365" ON "SECO_PAISES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034368
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034368" ON "SECO_PERMISO_USUARIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034371
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034371" ON "SECO_PRESENTACIONES_ENVASES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034374
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034374" ON "SECO_PRESENTACIONES_PRODUCTOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034377
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034377" ON "SECO_PRODUCTOS_SUSPENDIDOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034379
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034379" ON "SECO_PROPUESTA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034384
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034384" ON "SECO_PROVINCIAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034386
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034386" ON "SECO_PUESTA_MERCADO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034388
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034388" ON "SECO_PUESTA_MERCADO_AG" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034390
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034390" ON "SECO_PUESTA_MERCADO_CA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034392
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034392" ON "SECO_REGISTRO_AGUAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034396
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034396" ON "SECO_REGISTROACTIVIDADES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034398
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034398" ON "SECO_REPARTO_CALORICO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034400
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034400" ON "SECO_REPRESENTANTES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034403
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034403" ON "SECO_ROLES_USUARIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034406
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034406" ON "SECO_SERVICIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034409
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034409" ON "SECO_SITUACIONES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034411
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034411" ON "SECO_SOL_LMR_FITOSANITARIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034414
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034414" ON "SECO_SOL_QUEJAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034417
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034417" ON "SECO_SOL_SUGERE" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034420
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034420" ON "SECO_SOLICITANTES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034422
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034422" ON "SECO_SOLICITUD_LOGO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034432
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034432" ON "SECO_SOLICITUDES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034440
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034440" ON "SECO_TASAS_MODELOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034443
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034443" ON "SECO_TIPO_FORMULARIO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034447
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034447" ON "SECO_TIPO_PAGO_SERVICIO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034450
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034450" ON "SECO_TIPO_PAGOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034453
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034453" ON "SECO_TIPO_SUSPENSION_FINA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034456
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034456" ON "SECO_TIPO_USUARIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034460
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034460" ON "SECO_TIPOS_DOC_FORMULARIO" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034463
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034463" ON "SECO_TIPOS_DOCUMENTOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034466
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034466" ON "SECO_TIPOS_PERSONAS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034469
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034469" ON "SECO_TIPOS_PRODUCTOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034472
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034472" ON "SECO_TIPOS_SOLICITUDES" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034475
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034475" ON "SECO_TIPOSCOM_SOLICITUD" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034477
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034477" ON "SECO_UBICACION_GEOGRAFICA" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034482
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034482" ON "SECO_USUARIOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034484
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034484" ON "SECO_USUARIOS_CLAVE" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034486
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034486" ON "SECO_USUARIOS_EXTERNOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034491
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034491" ON "SECO_USUARIOS_INTERNOS" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034492
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034492" ON "SECO_USUARIOS_INTERNOS" ("LOGIN") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034496
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034496" ON "SECO_USUARIOS_NOEU" ("ID") 
  ;
--------------------------------------------------------
--  DDL for Index SYS_C0034497
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034497" ON "SECO_USUARIOS_NOEU" ("LOGIN") 
  ;
  
 --------------------------------------------------------
--  DDL for Index SYS_C0034914
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034914" ON "SECO_TIPO_ENVIO" ("ID") 
  ;
  
--------------------------------------------------------
--  DDL for Index SYS_C0034909
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0034909" ON "SECO_DATOS_ENVIO" ("ID") 
  ;
--------------------------------------------------------
--  Constraints for Table SECO_ACCIONES
--------------------------------------------------------

  ALTER TABLE "SECO_ACCIONES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ACCIONES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_ACCIONES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_AGREGACION_PAISES
--------------------------------------------------------

  ALTER TABLE "SECO_AGREGACION_PAISES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_AGREGACION_PAISES" MODIFY ("CONTINENTE_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_AGREGACION_PAISES" MODIFY ("CODIGO_INE" NOT NULL ENABLE);
  ALTER TABLE "SECO_AGREGACION_PAISES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_AGREGACION_PAISES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ALTER_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_ALTER_OFER_ALIM_UME" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ALTER_OFER_ALIM_UME" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ANTECEDENTES_PROCED
--------------------------------------------------------

  ALTER TABLE "SECO_ANTECEDENTES_PROCED" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ANTECEDENTES_PROCED" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_AREAS
--------------------------------------------------------

  ALTER TABLE "SECO_AREAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_AREAS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_AREAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_BAJA_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_BAJA_OFER_ALIM_UME" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_BAJA_OFER_ALIM_UME" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_BAJA_O_SUSPENSION
--------------------------------------------------------

  ALTER TABLE "SECO_BAJA_O_SUSPENSION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_BAJA_O_SUSPENSION" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_BAJA_O_SUSPENSION" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_CESE_COMERCIALIZACION
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_CESE_COMERCIALIZACION" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_CESE_COMERCIALIZACION_AG
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION_AG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_CESE_COMERCIALIZACION_AG" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_CESE_COMERCIALIZACION_CA
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION_CA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_CESE_COMERCIALIZACION_CA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_CONTINENTES
--------------------------------------------------------

  ALTER TABLE "SECO_CONTINENTES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_CONTINENTES" MODIFY ("CODIGO_INE" NOT NULL ENABLE);
  ALTER TABLE "SECO_CONTINENTES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_CONTINENTES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DATOS_CONTACTOS
--------------------------------------------------------

  ALTER TABLE "SECO_DATOS_CONTACTOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DATOS_CONTACTOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DATOS_IDENTIF_ADICIONALES
--------------------------------------------------------

  ALTER TABLE "SECO_DATOS_IDENTIF_ADICIONALES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DATOS_IDENTIF_ADICIONALES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DATOS_PAGO
--------------------------------------------------------

  ALTER TABLE "SECO_DATOS_PAGO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DATOS_PAGO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DIRECCION_EXT
--------------------------------------------------------

  ALTER TABLE "SECO_DIRECCION_EXT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DIRECCION_EXT" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DOCUMENTOS_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_DOCUMENTOS_SOLICITUD" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DOCUMENTOS_SOLICITUD" MODIFY ("SOLICITUD_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_DOCUMENTOS_SOLICITUD" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ESTADO_FISICO
--------------------------------------------------------

  ALTER TABLE "SECO_ESTADO_FISICO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ESTADO_FISICO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ESTADOS_AREAS
--------------------------------------------------------

  ALTER TABLE "SECO_ESTADOS_AREAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ESTADOS_AREAS" MODIFY ("ESTADO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_ESTADOS_AREAS" MODIFY ("AREA_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_ESTADOS_AREAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ESTADOS_SOLICITUDES
--------------------------------------------------------

  ALTER TABLE "SECO_ESTADOS_SOLICITUDES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ESTADOS_SOLICITUDES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_ESTADOS_SOLICITUDES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_EVALUACION_RIESGOS
--------------------------------------------------------

  ALTER TABLE "SECO_EVALUACION_RIESGOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_EVALUACION_RIESGOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FIBRA
--------------------------------------------------------

  ALTER TABLE "SECO_FIBRA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FIBRA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FICHEROS_INSTRUCCIONES
--------------------------------------------------------

  ALTER TABLE "SECO_FICHEROS_INSTRUCCIONES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FICHEROS_INSTRUCCIONES" MODIFY ("TIPO_FORMULARIO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_FICHEROS_INSTRUCCIONES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_FICHEROS_INSTRUCCIONES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FICHEROS_INSTRUCC_PAGOS
--------------------------------------------------------

  ALTER TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" MODIFY ("TIPO_FORMULARIO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FINANCIACION_ALIMENTOS
--------------------------------------------------------

  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FORMA_PAGO
--------------------------------------------------------

  ALTER TABLE "SECO_FORMA_PAGO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FORMA_PAGO" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_FORMA_PAGO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_FORMAS
--------------------------------------------------------

  ALTER TABLE "SECO_FORMAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_FORMAS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_FORMAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_GRUPO_PACIENTES
--------------------------------------------------------

  ALTER TABLE "SECO_GRUPO_PACIENTES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_GRUPO_PACIENTES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_IDENTIFICADOR_PAGO
--------------------------------------------------------

  ALTER TABLE "SECO_IDENTIFICADOR_PAGO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_IDENTIFICADOR_PAGO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_INC_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_INC_OFER_ALIM_UME" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_INC_OFER_ALIM_UME" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_INGREDIENTES
--------------------------------------------------------

  ALTER TABLE "SECO_INGREDIENTES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_INGREDIENTES" MODIFY ("SITUACION_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_INGREDIENTES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_JWTOKENKEYS
--------------------------------------------------------

  ALTER TABLE "SECO_JWTOKENKEYS" ADD PRIMARY KEY ("KEY") ENABLE;
  ALTER TABLE "SECO_JWTOKENKEYS" MODIFY ("KEY" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MODIFICACION_DATOS
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MODIFICACION_DATOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MODIFICACION_DATOS_AG
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS_AG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MODIFICACION_DATOS_AG" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MODIFICACION_DATOS_CA
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS_CA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MODIFICACION_DATOS_CA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MODO_ADMINISTRACION
--------------------------------------------------------

  ALTER TABLE "SECO_MODO_ADMINISTRACION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MODO_ADMINISTRACION" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_MODO_ADMINISTRACION" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MONITORIZACION_CONECTORES
--------------------------------------------------------

  ALTER TABLE "SECO_MONITORIZACION_CONECTORES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MONITORIZACION_CONECTORES" MODIFY ("RESULTADO_OPERACION" NOT NULL ENABLE);
  ALTER TABLE "SECO_MONITORIZACION_CONECTORES" MODIFY ("FECHA_INVOCACION" NOT NULL ENABLE);
  ALTER TABLE "SECO_MONITORIZACION_CONECTORES" MODIFY ("SISTEMA" NOT NULL ENABLE);
  ALTER TABLE "SECO_MONITORIZACION_CONECTORES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_MUNICIPIOS
--------------------------------------------------------

  ALTER TABLE "SECO_MUNICIPIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_MUNICIPIOS" MODIFY ("PROVINCIA_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_MUNICIPIOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_MUNICIPIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PAGO_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PAGO_SOLICITUD" MODIFY ("IDENTIFICADOR_PAGO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PAGO_SOLICITUD" MODIFY ("IDENTIFICADOR_PETICION_SOL" NOT NULL ENABLE);
  ALTER TABLE "SECO_PAGO_SOLICITUD" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PAISES
--------------------------------------------------------

  ALTER TABLE "SECO_PAISES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PAISES" MODIFY ("AGREGACION_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PAISES" MODIFY ("CODIGO_INE" NOT NULL ENABLE);
  ALTER TABLE "SECO_PAISES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_PAISES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PERMISO_USUARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_PERMISO_USUARIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PERMISO_USUARIOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_PERMISO_USUARIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PRESENTACIONES_ENVASES
--------------------------------------------------------

  ALTER TABLE "SECO_PRESENTACIONES_ENVASES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PRESENTACIONES_ENVASES" MODIFY ("OFER_ALIM_UME_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PRESENTACIONES_ENVASES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PRESENTACIONES_PRODUCTOS
--------------------------------------------------------

  ALTER TABLE "SECO_PRESENTACIONES_PRODUCTOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PRESENTACIONES_PRODUCTOS" MODIFY ("PUESTA_MERCADO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PRESENTACIONES_PRODUCTOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PRODUCTOS_SUSPENDIDOS
--------------------------------------------------------

  ALTER TABLE "SECO_PRODUCTOS_SUSPENDIDOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PRODUCTOS_SUSPENDIDOS" MODIFY ("BAJA_ALIM_UME_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PRODUCTOS_SUSPENDIDOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PROPUESTA
--------------------------------------------------------

  ALTER TABLE "SECO_PROPUESTA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PROPUESTA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PROVINCIAS
--------------------------------------------------------

  ALTER TABLE "SECO_PROVINCIAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PROVINCIAS" MODIFY ("PAIS_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_PROVINCIAS" MODIFY ("CODIGO_INE" NOT NULL ENABLE);
  ALTER TABLE "SECO_PROVINCIAS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_PROVINCIAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PUESTA_MERCADO
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PUESTA_MERCADO_AG
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO_AG" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO_AG" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_PUESTA_MERCADO_CA
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO_CA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO_CA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_REGISTROACTIVIDADES
--------------------------------------------------------

  ALTER TABLE "SECO_REGISTROACTIVIDADES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTROACTIVIDADES" MODIFY ("ACCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_REGISTROACTIVIDADES" MODIFY ("FECHA" NOT NULL ENABLE);
  ALTER TABLE "SECO_REGISTROACTIVIDADES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_REGISTRO_AGUAS
--------------------------------------------------------

  ALTER TABLE "SECO_REGISTRO_AGUAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTRO_AGUAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_REGISTRO_INDUSTRIAS
--------------------------------------------------------

  ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_REPARTO_CALORICO
--------------------------------------------------------

  ALTER TABLE "SECO_REPARTO_CALORICO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_REPARTO_CALORICO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_REPRESENTANTES
--------------------------------------------------------

  ALTER TABLE "SECO_REPRESENTANTES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_REPRESENTANTES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_ROLES_USUARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_ROLES_USUARIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_ROLES_USUARIOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_ROLES_USUARIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SERVICIOS
--------------------------------------------------------

  ALTER TABLE "SECO_SERVICIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SERVICIOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_SERVICIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SITUACIONES
--------------------------------------------------------

  ALTER TABLE "SECO_SITUACIONES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SITUACIONES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_SITUACIONES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOLICITANTES
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITANTES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITANTES" MODIFY ("TIPO_PERSONA_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITANTES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOLICITUDES
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITUDES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("USUARIO_CREADOR_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("ESTADO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("SOLICITANTE_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("FORMULARIO_TIPO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("TIPO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("AREA_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("IDENTIFICADOR_PETICION" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("FECHA_CREACION" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOLICITUDES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOLICITUD_LOGO
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITUD_LOGO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUD_LOGO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOL_LMR_FITOSANITARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_LMR_FITOSANITARIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_LMR_FITOSANITARIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOL_QUEJAS
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_QUEJAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_QUEJAS" MODIFY ("TIPOCOMUNICACION_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOL_QUEJAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_SOL_SUGERE
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_SUGERE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_SUGERE" MODIFY ("TIPOCOMUNICACION_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_SOL_SUGERE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TASAS_MODELOS
--------------------------------------------------------

  ALTER TABLE "SECO_TASAS_MODELOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TASAS_MODELOS" MODIFY ("CODIGO_TASA" NOT NULL ENABLE);
  ALTER TABLE "SECO_TASAS_MODELOS" MODIFY ("MODELO" NOT NULL ENABLE);
  ALTER TABLE "SECO_TASAS_MODELOS" MODIFY ("MONTO" NOT NULL ENABLE);
  ALTER TABLE "SECO_TASAS_MODELOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TASAS_MODELOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_T_DOCUMENTOS_T_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_T_DOCUMENTOS_T_SOLICITUD" MODIFY ("TIPO_DOC_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_T_DOCUMENTOS_T_SOLICITUD" MODIFY ("TIPO_SOLICITUD_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_FORMULARIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_FORMULARIO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_FORMULARIO" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_FORMULARIO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_PAGOS
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_PAGOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_PAGOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_PAGOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_PAGO_SERVICIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" MODIFY ("SERVICIO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" MODIFY ("TIPO_PAGO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOSCOM_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOSCOM_SOLICITUD" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOSCOM_SOLICITUD" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOSCOM_SOLICITUD" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOS_DOC_FORMULARIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_DOC_FORMULARIO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOS_DOC_FORMULARIO" MODIFY ("TIPO_DOCUMENTACION_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_DOC_FORMULARIO" MODIFY ("TIPO_FORMULARIO_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_DOC_FORMULARIO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOS_DOCUMENTOS
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_DOCUMENTOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOS_DOCUMENTOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_DOCUMENTOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOS_PERSONAS
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_PERSONAS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOS_PERSONAS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_PERSONAS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOS_PRODUCTOS
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_PRODUCTOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOS_PRODUCTOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_PRODUCTOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPOS_SOLICITUDES
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_SOLICITUDES" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPOS_SOLICITUDES" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPOS_SOLICITUDES" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_SUSPENSION_FINA
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_SUSPENSION_FINA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_SUSPENSION_FINA" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_SUSPENSION_FINA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_USUARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_USUARIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_USUARIOS" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_USUARIOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_UBICACION_GEOGRAFICA
--------------------------------------------------------

  ALTER TABLE "SECO_UBICACION_GEOGRAFICA" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_UBICACION_GEOGRAFICA" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_USUARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS" MODIFY ("ROL_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS" MODIFY ("NUMERO_ID_PERSONA" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS" MODIFY ("TIPO_ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_USUARIOS_CLAVE
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_CLAVE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS_CLAVE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_USUARIOS_EXTERNOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_EXTERNOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS_EXTERNOS" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_USUARIOS_INTERNOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_INTERNOS" ADD UNIQUE ("LOGIN") ENABLE;
  ALTER TABLE "SECO_USUARIOS_INTERNOS" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS_INTERNOS" MODIFY ("AREA_ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS_INTERNOS" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS_INTERNOS" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS_INTERNOS" MODIFY ("LOGIN" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_USUARIOS_NOEU
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_NOEU" ADD UNIQUE ("LOGIN") ENABLE;
  ALTER TABLE "SECO_USUARIOS_NOEU" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS_NOEU" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS_NOEU" MODIFY ("PASSWORD" NOT NULL ENABLE);
  ALTER TABLE "SECO_USUARIOS_NOEU" MODIFY ("LOGIN" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table SECO_AGREGACION_PAISES
--------------------------------------------------------

------------------------------------------------
  ALTER TABLE "SECO_AGREGACION_PAISES" ADD CONSTRAINT "FK85647C6FDA77349C" FOREIGN KEY ("CONTINENTE_ID")
	  REFERENCES "SECO_CONTINENTES" ("ID") ENABLE;--------
--  Ref Constraints for Table SECO_ALTER_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_ALTER_OFER_ALIM_UME" ADD CONSTRAINT "FK95B8204EB1B15B37" FOREIGN KEY ("ID")
	  REFERENCES "SECO_FINANCIACION_ALIMENTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_ANTECEDENTES_PROCED
--------------------------------------------------------

  ALTER TABLE "SECO_ANTECEDENTES_PROCED" ADD CONSTRAINT "FK277BC53EC7965FC6" FOREIGN KEY ("PAIS_COMERC_PREVIA_ID")
	  REFERENCES "SECO_PAISES" ("ID") ENABLE;
  ALTER TABLE "SECO_ANTECEDENTES_PROCED" ADD CONSTRAINT "FK277BC53ED6B5577E" FOREIGN KEY ("PAIS_PROCEDENCIA_ID")
	  REFERENCES "SECO_PAISES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_BAJA_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_BAJA_OFER_ALIM_UME" ADD CONSTRAINT "FK8A5CE110408BFE9E" FOREIGN KEY ("BAJA_O_SUSPENSION_ID")
	  REFERENCES "SECO_BAJA_O_SUSPENSION" ("ID") ENABLE;
  ALTER TABLE "SECO_BAJA_OFER_ALIM_UME" ADD CONSTRAINT "FK8A5CE1106527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_CESE_COMERCIALIZACION
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION" ADD CONSTRAINT "FKDCF52EA06527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_CESE_COMERCIALIZACION_AG
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION_AG" ADD CONSTRAINT "FK181E3E25B7DF7CFF" FOREIGN KEY ("ID")
	  REFERENCES "SECO_CESE_COMERCIALIZACION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_CESE_COMERCIALIZACION_CA
--------------------------------------------------------

  ALTER TABLE "SECO_CESE_COMERCIALIZACION_CA" ADD CONSTRAINT "FK181E3E5DB7DF7CFF" FOREIGN KEY ("ID")
	  REFERENCES "SECO_CESE_COMERCIALIZACION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_DATOS_CONTACTOS
--------------------------------------------------------

  ALTER TABLE "SECO_DATOS_CONTACTOS" ADD CONSTRAINT "FK6EBF667F585AE83D" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID")
	  REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_DOCUMENTOS_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_DOCUMENTOS_SOLICITUD" ADD CONSTRAINT "FK3910FA05E3F9DD55" FOREIGN KEY ("TIPO_DOCUMENTACION_ID")
	  REFERENCES "SECO_TIPOS_DOCUMENTOS" ("ID") ENABLE;
  ALTER TABLE "SECO_DOCUMENTOS_SOLICITUD" ADD CONSTRAINT "FK3910FA05F3407294" FOREIGN KEY ("SOLICITUD_ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_ESTADOS_AREAS
--------------------------------------------------------

  ALTER TABLE "SECO_ESTADOS_AREAS" ADD CONSTRAINT "FK19CDA6533364510C" FOREIGN KEY ("ESTADO_ID")
	  REFERENCES "SECO_ESTADOS_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_ESTADOS_AREAS" ADD CONSTRAINT "FK19CDA6534CC63426" FOREIGN KEY ("AREA_ID")
	  REFERENCES "SECO_AREAS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_EVALUACION_RIESGOS
--------------------------------------------------------

  ALTER TABLE "SECO_EVALUACION_RIESGOS" ADD CONSTRAINT "FK3F12B3954F9E4774" FOREIGN KEY ("PAGO_SOLICITUD_ID")
	  REFERENCES "SECO_PAGO_SOLICITUD" ("ID") ENABLE;
  ALTER TABLE "SECO_EVALUACION_RIESGOS" ADD CONSTRAINT "FK3F12B3956527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_EVALUACION_RIESGOS" ADD CONSTRAINT "FK3F12B395DA14A34D" FOREIGN KEY ("TIPO_PAGO_SERVICIO_ID")
	  REFERENCES "SECO_TIPO_PAGO_SERVICIO" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_FICHEROS_INSTRUCCIONES
--------------------------------------------------------

  ALTER TABLE "SECO_FICHEROS_INSTRUCCIONES" ADD CONSTRAINT "FK1B7C1448D16008F" FOREIGN KEY ("TIPO_FORMULARIO_ID")
	  REFERENCES "SECO_TIPO_FORMULARIO" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_FICHEROS_INSTRUCC_PAGOS
--------------------------------------------------------

  ALTER TABLE "SECO_FICHEROS_INSTRUCC_PAGOS" ADD CONSTRAINT "FK42FE34A9D16008F" FOREIGN KEY ("TIPO_FORMULARIO_ID")
	  REFERENCES "SECO_TIPO_FORMULARIO" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_FINANCIACION_ALIMENTOS
--------------------------------------------------------

  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A143C90A9" FOREIGN KEY ("GRUPO_PACIENTES_ID")
	  REFERENCES "SECO_GRUPO_PACIENTES" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A25B9265E" FOREIGN KEY ("FIBRA_ID")
	  REFERENCES "SECO_FIBRA" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A2C8573C9" FOREIGN KEY ("ESTADO_FISICO_ID")
	  REFERENCES "SECO_ESTADO_FISICO" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A4F9E4774" FOREIGN KEY ("PAGO_SOLICITUD_ID")
	  REFERENCES "SECO_PAGO_SOLICITUD" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A6527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A8C66023E" FOREIGN KEY ("PROPUESTA_ID")
	  REFERENCES "SECO_PROPUESTA" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160A8EBDBB17" FOREIGN KEY ("MODO_ADMINISTRACION_ID")
	  REFERENCES "SECO_MODO_ADMINISTRACION" ("ID") ENABLE;
  ALTER TABLE "SECO_FINANCIACION_ALIMENTOS" ADD CONSTRAINT "FK6F8E160AC9B5EB11" FOREIGN KEY ("REPARTO_CALORICO_ID")
	  REFERENCES "SECO_REPARTO_CALORICO" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_IDENTIFICADOR_PAGO
--------------------------------------------------------

  ALTER TABLE "SECO_IDENTIFICADOR_PAGO" ADD CONSTRAINT "FK584208C585AE83D" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID")
	  REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;
  ALTER TABLE "SECO_IDENTIFICADOR_PAGO" ADD CONSTRAINT "FK584208CEA4912" FOREIGN KEY ("DIRECCION_EXTENDIDA")
	  REFERENCES "SECO_DIRECCION_EXT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_INC_OFER_ALIM_UME
--------------------------------------------------------

  ALTER TABLE "SECO_INC_OFER_ALIM_UME" ADD CONSTRAINT "FK1B3ECDD6B1B15B37" FOREIGN KEY ("ID")
	  REFERENCES "SECO_FINANCIACION_ALIMENTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_INGREDIENTES
--------------------------------------------------------

  ALTER TABLE "SECO_INGREDIENTES" ADD CONSTRAINT "FK3D5C7EA096C3E2A6" FOREIGN KEY ("SITUACION_ID")
	  REFERENCES "SECO_SITUACIONES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_MODIFICACION_DATOS
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS" ADD CONSTRAINT "FK979FB7EA6527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_MODIFICACION_DATOS_AG
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS_AG" ADD CONSTRAINT "FKA39CB89BF6CE7495" FOREIGN KEY ("ID")
	  REFERENCES "SECO_MODIFICACION_DATOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_MODIFICACION_DATOS_CA
--------------------------------------------------------

  ALTER TABLE "SECO_MODIFICACION_DATOS_CA" ADD CONSTRAINT "FKA39CB8D3F6CE7495" FOREIGN KEY ("ID")
	  REFERENCES "SECO_MODIFICACION_DATOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_MUNICIPIOS
--------------------------------------------------------

  ALTER TABLE "SECO_MUNICIPIOS" ADD CONSTRAINT "FK18493AC71E40D615" FOREIGN KEY ("PROVINCIA_ID")
	  REFERENCES "SECO_PROVINCIAS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PAGO_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD CONSTRAINT "FK78590C9F2ABAF315" FOREIGN KEY ("FORMULARIO_PAGADO")
	  REFERENCES "SECO_TIPO_FORMULARIO" ("ID") ENABLE;
  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD CONSTRAINT "FK78590C9F4113621E" FOREIGN KEY ("DATOS_PAGO_ID")
	  REFERENCES "SECO_DATOS_PAGO" ("ID") ENABLE;
  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD CONSTRAINT "FK78590C9FB3FE5662" FOREIGN KEY ("FORMA_PAGO_ID")
	  REFERENCES "SECO_FORMA_PAGO" ("ID") ENABLE;
  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD CONSTRAINT "FK78590C9FC7EB7582" FOREIGN KEY ("IDENTIFICADOR_PAGO_ID")
	  REFERENCES "SECO_IDENTIFICADOR_PAGO" ("ID") ENABLE;
  ALTER TABLE "SECO_PAGO_SOLICITUD" ADD CONSTRAINT "FK78590C9FF3407294" FOREIGN KEY ("SOLICITUD_ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PAISES
--------------------------------------------------------

  ALTER TABLE "SECO_PAISES" ADD CONSTRAINT "FKAE671EA673D0D37" FOREIGN KEY ("AGREGACION_ID")
	  REFERENCES "SECO_AGREGACION_PAISES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PRESENTACIONES_ENVASES
--------------------------------------------------------

  ALTER TABLE "SECO_PRESENTACIONES_ENVASES" ADD CONSTRAINT "FKC20132686EFE189E" FOREIGN KEY ("OFER_ALIM_UME_ID")
	  REFERENCES "SECO_FINANCIACION_ALIMENTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PRESENTACIONES_PRODUCTOS
--------------------------------------------------------

  ALTER TABLE "SECO_PRESENTACIONES_PRODUCTOS" ADD CONSTRAINT "FKD7E1F3EE885385E6" FOREIGN KEY ("FORMA_ID")
	  REFERENCES "SECO_FORMAS" ("ID") ENABLE;
  ALTER TABLE "SECO_PRESENTACIONES_PRODUCTOS" ADD CONSTRAINT "FKD7E1F3EE95D44DE5" FOREIGN KEY ("PUESTA_MERCADO_ID")
	  REFERENCES "SECO_PUESTA_MERCADO" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PRODUCTOS_SUSPENDIDOS
--------------------------------------------------------

  ALTER TABLE "SECO_PRODUCTOS_SUSPENDIDOS" ADD CONSTRAINT "FK1B79C40E731680DC" FOREIGN KEY ("BAJA_ALIM_UME_ID")
	  REFERENCES "SECO_BAJA_OFER_ALIM_UME" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PROVINCIAS
--------------------------------------------------------

  ALTER TABLE "SECO_PROVINCIAS" ADD CONSTRAINT "FK5A680F07CB4B101C" FOREIGN KEY ("PAIS_ID")
	  REFERENCES "SECO_PAISES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PUESTA_MERCADO
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO" ADD CONSTRAINT "FKACCBF04536472F27" FOREIGN KEY ("DATOS_IDENTIF_ADIC_ID")
	  REFERENCES "SECO_DATOS_IDENTIF_ADICIONALES" ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO" ADD CONSTRAINT "FKACCBF04548B9193B" FOREIGN KEY ("ANTECEDENTES_PROCEDENCIA_ID")
	  REFERENCES "SECO_ANTECEDENTES_PROCED" ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO" ADD CONSTRAINT "FKACCBF0456527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PUESTA_MERCADO_AG
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO_AG" ADD CONSTRAINT "FK888EDE60264A5FEA" FOREIGN KEY ("ID")
	  REFERENCES "SECO_PUESTA_MERCADO" ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO_AG" ADD CONSTRAINT "FK888EDE60817B1705" FOREIGN KEY ("TIPO_PRODUCTO_ID")
	  REFERENCES "SECO_TIPOS_PRODUCTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_PUESTA_MERCADO_CA
--------------------------------------------------------

  ALTER TABLE "SECO_PUESTA_MERCADO_CA" ADD CONSTRAINT "FK888EDE98264A5FEA" FOREIGN KEY ("ID")
	  REFERENCES "SECO_PUESTA_MERCADO" ("ID") ENABLE;
  ALTER TABLE "SECO_PUESTA_MERCADO_CA" ADD CONSTRAINT "FK888EDE98ECCC0EF4" FOREIGN KEY ("INGREDIENTES_ID")
	  REFERENCES "SECO_INGREDIENTES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_REGISTROACTIVIDADES
--------------------------------------------------------

  ALTER TABLE "SECO_REGISTROACTIVIDADES" ADD CONSTRAINT "FK12CDB4171A4D8700" FOREIGN KEY ("USUARIO")
	  REFERENCES "SECO_USUARIOS" ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTROACTIVIDADES" ADD CONSTRAINT "FK12CDB41793EBCBFE" FOREIGN KEY ("ACCION")
	  REFERENCES "SECO_ACCIONES" ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTROACTIVIDADES" ADD CONSTRAINT "FK12CDB417F3407294" FOREIGN KEY ("SOLICITUD_ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_REGISTRO_AGUAS
--------------------------------------------------------

  ALTER TABLE "SECO_REGISTRO_AGUAS" ADD CONSTRAINT "FKE7EE7A964F9E4774" FOREIGN KEY ("PAGO_SOLICITUD_ID")
	  REFERENCES "SECO_PAGO_SOLICITUD" ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTRO_AGUAS" ADD CONSTRAINT "FKE7EE7A966527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_REGISTRO_AGUAS" ADD CONSTRAINT "FKE7EE7A96CB4B101C" FOREIGN KEY ("PAIS_ID")
	  REFERENCES "SECO_PAISES" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table SECO_REPRESENTANTES
--------------------------------------------------------

  ALTER TABLE "SECO_REPRESENTANTES" ADD CONSTRAINT "FK217262CE585AE83D" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID")
	  REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SERVICIOS
--------------------------------------------------------

  ALTER TABLE "SECO_SERVICIOS" ADD CONSTRAINT "FKB4DD9D9CFFE00BEA" FOREIGN KEY ("TASA_MODELO_ID")
	  REFERENCES "SECO_TASAS_MODELOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOLICITANTES
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITANTES" ADD CONSTRAINT "FKF982B501585AE83D" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID")
	  REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITANTES" ADD CONSTRAINT "FKF982B5015ABCFEF5" FOREIGN KEY ("TIPO_PERSONA_ID")
	  REFERENCES "SECO_TIPOS_PERSONAS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOLICITUDES
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D121DBF48B" FOREIGN KEY ("FORMULARIO_TIPO_ID")
	  REFERENCES "SECO_TIPO_FORMULARIO" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D13364510C" FOREIGN KEY ("ESTADO_ID")
	  REFERENCES "SECO_ESTADOS_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D13D91350B" FOREIGN KEY ("DATOS_CONTACTO_ID")
	  REFERENCES "SECO_DATOS_CONTACTOS" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D14CC63426" FOREIGN KEY ("AREA_ID")
	  REFERENCES "SECO_AREAS" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D166D5E6B0" FOREIGN KEY ("TIPO_ID")
	  REFERENCES "SECO_TIPOS_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D189CE5A54" FOREIGN KEY ("REPRESENTANTE_ID")
	  REFERENCES "SECO_REPRESENTANTES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D1B0693334" FOREIGN KEY ("SOLICITANTE_ID")
	  REFERENCES "SECO_SOLICITANTES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOLICITUDES" ADD CONSTRAINT "FK8C3671D1FCE0861F" FOREIGN KEY ("USUARIO_CREADOR_ID")
	  REFERENCES "SECO_USUARIOS_EXTERNOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOLICITUD_LOGO
--------------------------------------------------------

  ALTER TABLE "SECO_SOLICITUD_LOGO" ADD CONSTRAINT "FKB370E2876527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOL_LMR_FITOSANITARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_LMR_FITOSANITARIOS" ADD CONSTRAINT "FK4F6F722D4F9E4774" FOREIGN KEY ("PAGO_SOLICITUD_ID")
	  REFERENCES "SECO_PAGO_SOLICITUD" ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_LMR_FITOSANITARIOS" ADD CONSTRAINT "FK4F6F722D6527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOL_QUEJAS
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_QUEJAS" ADD CONSTRAINT "FK902E91EB6527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_QUEJAS" ADD CONSTRAINT "FK902E91EBC1BD373C" FOREIGN KEY ("TIPOCOMUNICACION_ID")
	  REFERENCES "SECO_TIPOSCOM_SOLICITUD" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_SOL_SUGERE
--------------------------------------------------------

  ALTER TABLE "SECO_SOL_SUGERE" ADD CONSTRAINT "FK93991B236527FF19" FOREIGN KEY ("ID")
	  REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_SOL_SUGERE" ADD CONSTRAINT "FK93991B23C1BD373C" FOREIGN KEY ("TIPOCOMUNICACION_ID")
	  REFERENCES "SECO_TIPOSCOM_SOLICITUD" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_T_DOCUMENTOS_T_SOLICITUD
--------------------------------------------------------

  ALTER TABLE "SECO_T_DOCUMENTOS_T_SOLICITUD" ADD CONSTRAINT "FKCB5FF7A516A5706B" FOREIGN KEY ("TIPO_SOLICITUD_ID")
	  REFERENCES "SECO_TIPOS_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_T_DOCUMENTOS_T_SOLICITUD" ADD CONSTRAINT "FKCB5FF7A5C91C3B28" FOREIGN KEY ("TIPO_DOC_ID")
	  REFERENCES "SECO_TIPOS_DOCUMENTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_TIPO_FORMULARIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_FORMULARIO" ADD CONSTRAINT "FK65BDC7DC16A5706B" FOREIGN KEY ("TIPO_SOLICITUD_ID")
	  REFERENCES "SECO_TIPOS_SOLICITUDES" ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_FORMULARIO" ADD CONSTRAINT "FK65BDC7DCFFE00BEA" FOREIGN KEY ("TASA_MODELO_ID")
	  REFERENCES "SECO_TASAS_MODELOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_TIPO_PAGO_SERVICIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" ADD CONSTRAINT "FK15B8D992A347A6FB" FOREIGN KEY ("SERVICIO_ID")
	  REFERENCES "SECO_SERVICIOS" ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_PAGO_SERVICIO" ADD CONSTRAINT "FK15B8D992D18B5224" FOREIGN KEY ("TIPO_PAGO_ID")
	  REFERENCES "SECO_TIPO_PAGOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_TIPOS_DOC_FORMULARIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_DOC_FORMULARIO" ADD CONSTRAINT "FK8BE8C73AE3F9DD55" FOREIGN KEY ("TIPO_DOCUMENTACION_ID")
	  REFERENCES "SECO_TIPOS_DOCUMENTOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_TIPOS_SOLICITUDES
--------------------------------------------------------

  ALTER TABLE "SECO_TIPOS_SOLICITUDES" ADD CONSTRAINT "FK8F6062114CC63426" FOREIGN KEY ("AREA_ID")
	  REFERENCES "SECO_AREAS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_UBICACION_GEOGRAFICA
--------------------------------------------------------

  ALTER TABLE "SECO_UBICACION_GEOGRAFICA" ADD CONSTRAINT "FKA3EE3CC71E40D615" FOREIGN KEY ("PROVINCIA_ID")
	  REFERENCES "SECO_PROVINCIAS" ("ID") ENABLE;
  ALTER TABLE "SECO_UBICACION_GEOGRAFICA" ADD CONSTRAINT "FKA3EE3CC73D225FBE" FOREIGN KEY ("MUNICIPIO_ID")
	  REFERENCES "SECO_MUNICIPIOS" ("ID") ENABLE;
  ALTER TABLE "SECO_UBICACION_GEOGRAFICA" ADD CONSTRAINT "FKA3EE3CC7CB4B101C" FOREIGN KEY ("PAIS_ID")
	  REFERENCES "SECO_PAISES" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_USUARIOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS" ADD CONSTRAINT "FKBE29FBE6580AA118" FOREIGN KEY ("ROL_ID")
	  REFERENCES "SECO_ROLES_USUARIOS" ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS" ADD CONSTRAINT "FKBE29FBE6D84EAE84" FOREIGN KEY ("TIPO_ID")
	  REFERENCES "SECO_TIPO_USUARIOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_USUARIOS_CLAVE
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_CLAVE" ADD CONSTRAINT "FKA61D042E8FC7F16B" FOREIGN KEY ("ID")
	  REFERENCES "SECO_USUARIOS_EXTERNOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_USUARIOS_EXTERNOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_EXTERNOS" ADD CONSTRAINT "FK34BA9DBD2238A0ED" FOREIGN KEY ("ID")
	  REFERENCES "SECO_USUARIOS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_USUARIOS_INTERNOS
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_INTERNOS" ADD CONSTRAINT "FKC34100AF2238A0ED" FOREIGN KEY ("ID")
	  REFERENCES "SECO_USUARIOS" ("ID") ENABLE;
  ALTER TABLE "SECO_USUARIOS_INTERNOS" ADD CONSTRAINT "FKC34100AF4CC63426" FOREIGN KEY ("AREA_ID")
	  REFERENCES "SECO_AREAS" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SECO_USUARIOS_NOEU
--------------------------------------------------------

  ALTER TABLE "SECO_USUARIOS_NOEU" ADD CONSTRAINT "FK91C3EAEA8FC7F16B" FOREIGN KEY ("ID")
	  REFERENCES "SECO_USUARIOS_EXTERNOS" ("ID") ENABLE;
--------------------------------------------------------
--  Constraints for Table SECO_TIPO_ENVIO
--------------------------------------------------------

  ALTER TABLE "SECO_TIPO_ENVIO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_TIPO_ENVIO" MODIFY ("DESCRIPCION" NOT NULL ENABLE);
  ALTER TABLE "SECO_TIPO_ENVIO" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SECO_DATOS_ENVIO
--------------------------------------------------------

  ALTER TABLE "SECO_DATOS_ENVIO" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "SECO_DATOS_ENVIO" MODIFY ("ID" NOT NULL ENABLE);

--------------------------------------------------------
--  Ref Constraints for Table SECO_DATOS_ENVIO
--------------------------------------------------------

ALTER TABLE "SECO_DATOS_ENVIO" ADD CONSTRAINT "FKC4B340CE585AE83D" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID")
REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table SECO_REGISTRO_INDUSTRIAS
--------------------------------------------------------

ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD CONSTRAINT "FKFECFFA8B4F9E4774" FOREIGN KEY ("PAGO_SOLICITUD_ID")
REFERENCES "SECO_PAGO_SOLICITUD" ("ID") ENABLE;
ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD CONSTRAINT "FKFECFFA8B6527FF19" FOREIGN KEY ("ID")
REFERENCES "SECO_SOLICITUDES" ("ID") ENABLE;
ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD CONSTRAINT "FKFECFFA8B8026F602" FOREIGN KEY ("DATOS_ENVIO_ID")
REFERENCES "SECO_DATOS_ENVIO" ("ID") ENABLE;
ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD CONSTRAINT "FKFECFFA8BCB4B101C" FOREIGN KEY ("PAIS_ID")
REFERENCES "SECO_PAISES" ("ID") ENABLE;

ALTER TABLE "SECO_REGISTRO_INDUSTRIAS" ADD CONSTRAINT "FKFECFFA8B7749A12E" FOREIGN KEY ("TIPO_ENVIO")
REFERENCES "SECO_TIPO_ENVIO" ("ID") ENABLE;

ALTER TABLE SECO_SUBVENCIONES_ASOCUAE	add constraint FK70A252879136D202 foreign key ("LUGAR_MEDIO_NOTIFICACION_ID") references SECO_LUGAR_MEDIO_NOTIFICACION ENABLE;
ALTER TABLE SECO_REGISTRO_REACU	add constraint FK70A252879136D203 foreign key ("LUGAR_MEDIO_NOTIFICACION_ID") references SECO_LUGAR_MEDIO_NOTIFICACION ENABLE;


ALTER TABLE "SECO_LUGAR_MEDIO_NOTIFICACION" ADD CONSTRAINT "FK70A252879136D204" FOREIGN KEY ("UBICACION_GEOGRAFICA_ID") REFERENCES "SECO_UBICACION_GEOGRAFICA" ("ID") ENABLE;
