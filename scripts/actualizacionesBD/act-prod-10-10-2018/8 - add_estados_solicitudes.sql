insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) VALUES (16,'Presentación de solicitudes será de 20 días hábiles a partir del día siguiente a la publicación en el BOE de la convocatoria');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) VALUES (17,'Presentar solicitudes en el Registro o en sede electrónica en:sede.msssi.gob.es, aportar documentación según anexo');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) VALUES (18,'Requerimiento para subsanar documentación en un plazo de 10 días hábiles');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (19,'Valoración');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (20,'Entrevista');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (21,'Evaluación de las solicitudes');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (22,'Resolución Provisional');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (23,'Notificación de la resolución provisional a los seleccionados');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (24,'Aceptación en el plazo de 10 días');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (25,'Resolución Definitiva');
insert into SECO_ESTADOS_SOLICITUDES (ID,DESCRIPCION) values (26,'Recurso de Alzado');

