--estos tipos de documentos son huerfanos, es decir, no estan asociados con ningun tipo de solicitud
-- su uso es directamente desde el tramitador y estan solo para ser mostrados en el solicitante
DELETE FROM SECO_TIPOS_DOCUMENTOS where ID =  39;
DELETE FROM SECO_TIPOS_DOCUMENTOS where ID =  40;
INSERT INTO SECO_TIPOS_DOCUMENTOS(ID, DESCRIPCION)  VALUES (39,'Portafirmas');
INSERT INTO SECO_TIPOS_DOCUMENTOS(ID, DESCRIPCION)  VALUES (40,'Copia Autentica');