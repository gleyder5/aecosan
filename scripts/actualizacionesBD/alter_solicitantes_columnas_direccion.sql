-- Alter para añadir los nuevos campos de discriminación de la dirección del usuario.
alter table seco_solicitantes 
  add (
  TIPO_DIRECCION varchar2(255),
  NUMERO varchar2(255),
  ESCALERA varchar2(255),
  PISO varchar2(255),
  PUERTA varchar2(255)
);

alter table SECO_REPRESENTANTES
add (
  TIPO_DIRECCION varchar2(255),
  NUMERO varchar2(255),
  ESCALERA varchar2(255),
  PISO varchar2(255),
  PUERTA varchar2(255)
);

alter table SECO_DATOS_CONTACTOS
add (
  TIPO_DIRECCION varchar2(255),
  NUMERO varchar2(255),
  ESCALERA varchar2(255),
  PISO varchar2(255),
  PUERTA varchar2(255)
);
