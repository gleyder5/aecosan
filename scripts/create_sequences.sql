--------------------------------------------------------
--  DDL for Sequence SECO_ANTECEDENTES_PROC_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_ANTECEDENTES_PROC_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_BAJA_O_SUSPENSION_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_BAJA_O_SUSPENSION_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;

--------------------------------------------------------
--  DDL for Sequence SECO_DATOS_ENVIO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DATOS_ENVIO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20  NOCYCLE ;
   
   --------------------------------------------------------
--  DDL for Sequence SECO_DATOSCNT_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DATOSCNT_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_DATOS_IDENTIF_ADIC_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DATOS_IDENTIF_ADIC_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_DATOS_PAGO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DATOS_PAGO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_DIRECCIONEXT_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DIRECCIONEXT_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_DOCUMENTACION_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_DOCUMENTACION_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_ESTADO_FISICO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_ESTADO_FISICO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_ESTADOS_AREAS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_ESTADOS_AREAS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_ESTADO_SOLICITUD_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_ESTADO_SOLICITUD_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_FIBRA_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_FIBRA_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_FICHERO_INST_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_FICHERO_INST_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_FICHERO_INST_PAGOS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_FICHERO_INST_PAGOS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_FORMA_PAGO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_FORMA_PAGO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_GRUPO_PACIENTES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_GRUPO_PACIENTES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_IDENTIFICADOR_PAGO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_IDENTIFICADOR_PAGO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_INGREDIENTES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_INGREDIENTES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_MODO_ADMIN_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_MODO_ADMIN_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_MUNICIPIO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_MUNICIPIO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PAGO_SOLICITUD_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PAGO_SOLICITUD_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PAISES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PAISES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PRESENTAC_ENVASES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PRESENTAC_ENVASES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PRESENTACIONES_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PRESENTACIONES_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PRODUC_SUSPENDIDOS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PRODUC_SUSPENDIDOS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PROPUESTA_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PROPUESTA_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_PROVINCIAS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_PROVINCIAS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_REGISTRO_ACTIVIDAD_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_REGISTRO_ACTIVIDAD_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_REPARTO_CALORICO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_REPARTO_CALORICO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_REPRESENTANTE_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_REPRESENTANTE_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_SITUACION_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_SITUACION_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_SOLICITANTE_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_SOLICITANTE_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_SOLICITUD_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_SOLICITUD_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_TIPODOC_FORMULARIO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_TIPODOC_FORMULARIO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_TIPO_PAGO_SERVICIO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_TIPO_PAGO_SERVICIO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_TIPOS_PRODUCTOS_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_TIPOS_PRODUCTOS_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_UBIC_GEO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_UBIC_GEO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_USUARIO_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_USUARIO_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 12 CACHE 20 NOCYCLE ;
--------------------------------------------------------

--------------------------------------------------------
--  DDL for Sequence SECO_MONITOR_CONECTOR_ID_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SECO_MONITOR_CONECTOR_ID_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------

--------------------------------------------------------
--  DDL for Sequence SECO_TIPO_PROCEDIMIENTO_ID_SEQ
--------------------------------------------------------

  CREATE SEQUENCE SECO_TIPO_PROCEDIMIENTO_ID_SEQ MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;;

--------------------------------------------------------
--  DDL for Sequence SECO_HIST_SOLICITUD_SEQ
-------------------------------------------------------

  CREATE SEQUENCE SECO_HIST_SOLICITUD_SEQ MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SECO_LUGAR_MEDIO_ID_SEQ
--------------------------------------------------------
  CREATE SEQUENCE SECO_LUGAR_MEDIO_ID_SEQ MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;

--  DDL for Sequence SECO_IDIOMAS_BECAS_SEQ
--------------------------------------------------------
   CREATE SEQUENCE SECO_IDIOMAS_BECAS_SEQ MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOCYCLE ;