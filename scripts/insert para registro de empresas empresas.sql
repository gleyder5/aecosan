-- certificado de empresa
-- INSERT INTO SECO_TIPOS_SOLICITUDES (ID, DESCRIPCION, AREA_ID) VALUES (10, 'Solicitud para registro el registro de industrias', 1);

-- INSERT INTO SECO_TIPOS_DOCUMENTOS (ID, DESCRIPCION) VALUES (28, 'Certificación Oficial Organismo');
-- INSERT INTO SECO_TIPOS_DOCUMENTOS (ID, DESCRIPCION) VALUES (29, 'Convalidación');
-- INSERT INTO SECO_TIPOS_DOCUMENTOS (ID, DESCRIPCION) VALUES (30, 'Cambio de Titular');
-- INSERT INTO SECO_TIPOS_DOCUMENTOS (ID, DESCRIPCION) VALUES (31, 'Memoria Técnica');

-- Insert into SECO_TIPO_FORMULARIO (ID,DESCRIPCION,TIPO_SOLICITUD_ID,SING_DOCUMENT_TEMPLATE,TASA_MODELO_ID) values (16,'Solicitud para registro el registro de industrias',10,null,null);

INSERT INTO SECO_TIPOS_DOC_FORMULARIO (ID, TIPO_DOCUMENTACION_ID, TIPO_FORMULARIO_ID) VALUES ('69', '25', '16');/*Registro de aguas*/
INSERT INTO SECO_TIPOS_DOC_FORMULARIO (ID, TIPO_DOCUMENTACION_ID, TIPO_FORMULARIO_ID) VALUES ('70', '26', '16');
INSERT INTO SECO_TIPOS_DOC_FORMULARIO (ID, TIPO_DOCUMENTACION_ID, TIPO_FORMULARIO_ID) VALUES ('71', '27', '16');
-- insert into SECO_TIPOS_DOC_FORMULARIO (ID, TIPO_DOCUMENTACION_ID, TIPO_FORMULARIO_ID) values ('72', '1', '16');
-- insert into SECO_TIPOS_DOC_FORMULARIO (ID, TIPO_DOCUMENTACION_ID, TIPO_FORMULARIO_ID) values ('73', '1', '16');

/* TODO JD aclarar modelo en bd (este comentario esta en el script)*/
insert into SECO_FICHEROS_INSTRUCCIONES (ID,DESCRIPCION,TIPO_FORMULARIO_ID) values (SECO_FICHERO_INST_ID_SEQ.nextval,'default.pdf','10');

--
-- Insert into SECO_TIPO_ENVIO (ID,DESCRIPCION) values (1,'Recogida en persona (deberá esperar a que le sea comunicado que su certificado esta listo)');
-- Insert into SECO_TIPO_ENVIO (ID,DESCRIPCION) values (2,'Envío a través de mensajero (el coste correrá por cuenta del solicitante)');
-- Insert into SECO_TIPO_ENVIO (ID,DESCRIPCION) values (3,'Envío por correo ordinario.');
