package es.grupoavalon.aecosan.secosan.dao.entity.solicitud;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.ColumnNames;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_REPRESENTANTES)
public class RepresentanteEntity extends AbstractPersonAddressExtendedData {

	@Id
	@Column(name = ColumnNames.CATALOG_ID, nullable = false)
	@GeneratedValue(generator = SequenceNames.REPRESENTANTE_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.REPRESENTANTE_SEQUENCE, sequenceName = SequenceNames.REPRESENTANTE_SEQUENCE, allocationSize = 1)
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RepresentanteEntity other = (RepresentanteEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "RepresentanteEntity [id=" + id + "," + super.toString() + "]";
	}

}
