package test.es.grupoavalon.aecosan.secosan.rest;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.codehaus.jackson.JsonProcessingException;
import org.junit.Test;

import test.es.grupoavalon.aecosan.secosan.testutils.GenericRestTest;
import test.es.grupoavalon.aecosan.secosan.testutils.UtilRestTestClass;

/**
 * 
 *
 */
public class SolicitudQuejaSugerenciaRestServiceTest extends GenericRestTest {

	private static final String SECURE = "secure/";
	private static final String REST_SERVICE_PATH = "solicitudQuejaSugerencia/";
	private static final String REST_SERVICE_QUEJA = "queja/";
	private static final String REST_SERVICE_SUGERENCIA = "sugerencia/";
	private static final String REST_SERVICE_USER = "1";

	// Queja
	private static final String SENDED_JSON_FILE_QUEJA_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_QUEJA + "solicitudQuejaOk.json";
	private static final String SENDED_JSON_FILE_QUEJA_KO_UNITY = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_QUEJA + "solicitudQuejaKoUnity.json";
	private static final String SENDED_JSON_FILE_QUEJA_KO_MOTIVE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_QUEJA + "solicitudQuejaKoMotive.json";
	private static final String SENDED_JSON_FILE_QUEJA_KO_DATE_TIME_INCIDENCE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_QUEJA
			+ "solicitudQuejaKoDateTimeIncidence.json";

	private static final String SENDED_JSON_FILE_UPDATE_QUEJA_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_QUEJA + "solicitudQuejaUpdateOk.json";
	// Sugerencia

	private static final String SENDED_JSON_FILE_SUGERENCIA_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + "solicitudSugerenciaOk.json";
	private static final String SENDED_JSON_FILE_SUGERENCIA_KO_UNITY = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + "solicitudSugerenciaKoUnity.json";
	private static final String SENDED_JSON_FILE_SUGERENCIA_KO_MOTIVE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + "solicitudSugerenciaKoMotive.json";
	private static final String SENDED_JSON_FILE_SUGERENCIA_KO_DATE_TIME_INCIDENCE = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA
			+ "solicitudSugerenciaKoDateTimeIncidence.json";

	private static final String SENDED_JSON_FILE_UPDATE_SUGERENCIA_OK = UtilRestTestClass.SENDED_JSON_FOLDER + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + "solicitudSugerenciaOkUpdate.json";

	// Init --> Test Queja

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudQuejaOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_QUEJA + REST_SERVICE_USER, SENDED_JSON_FILE_QUEJA_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// KO Required fields

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudQuejaKoRequiredFieldUnityIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_QUEJA + REST_SERVICE_USER, SENDED_JSON_FILE_QUEJA_KO_UNITY);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudQuejaKoRequiredFieldMotiveIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_QUEJA + REST_SERVICE_USER, SENDED_JSON_FILE_QUEJA_KO_MOTIVE);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudQuejaKoRequiredFieldDateTimeIncidenceIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_QUEJA + REST_SERVICE_USER, SENDED_JSON_FILE_QUEJA_KO_DATE_TIME_INCIDENCE);
		UtilRestTestClass.assertReponse500(response2);
	}


	@Test
	public void testSolicitudQuejaSugerenciaRestServiceSolicitudQuejaUpdatedOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_QUEJA + REST_SERVICE_USER, SENDED_JSON_FILE_UPDATE_QUEJA_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// End --> Test Queja

	// Init --> Test Sugerencia
	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudSugerenciaOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + REST_SERVICE_USER, SENDED_JSON_FILE_SUGERENCIA_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// KO Required fields

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudSugerenciaKoRequiredFieldUnityIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + REST_SERVICE_USER, SENDED_JSON_FILE_SUGERENCIA_KO_UNITY);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudSugerenciaKoRequiredFieldMotiveIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + REST_SERVICE_USER, SENDED_JSON_FILE_SUGERENCIA_KO_MOTIVE);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceAddSolicitudSugerenciaKoRequiredFieldDateTimeIncidenceIsNull() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePostJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + REST_SERVICE_USER, SENDED_JSON_FILE_SUGERENCIA_KO_DATE_TIME_INCIDENCE);
		UtilRestTestClass.assertReponse500(response2);
	}

	@Test
	public void testSolicitudQuejaSugerenciaRestServiceSolicitudSugerenciaUpdatedOk() throws JsonProcessingException, IOException {
		Response response2 = UtilRestTestClass.invokeRestServicePutJson(SECURE + REST_SERVICE_PATH + REST_SERVICE_SUGERENCIA + REST_SERVICE_USER, SENDED_JSON_FILE_UPDATE_SUGERENCIA_OK);
		UtilRestTestClass.assertReponseOk(response2);
	}

	// End --> Test Sugerencia

}
