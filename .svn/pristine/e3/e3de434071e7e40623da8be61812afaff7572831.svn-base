
package es.map.funcionario.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for rfhCredencial complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rfhCredencial">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fhBaja" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhCreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fhModificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idBorrador" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="idCredencial" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idVigencia" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="regUsuario" type="{http://ws.funcionario.map.es/}regUsuario" minOccurs="0"/>
 *         &lt;element name="rfhAnexoCredencials" type="{http://ws.funcionario.map.es/}rfhAnexoCredencial" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rfhFuncionario" type="{http://ws.funcionario.map.es/}rfhFuncionario" minOccurs="0"/>
 *         &lt;element name="rfhNanCredencialHabs" type="{http://ws.funcionario.map.es/}rfhNanCredencialHab" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rfhCredencial", propOrder = {
    "fhBaja",
    "fhCreacion",
    "fhModificacion",
    "idBorrador",
    "idCredencial",
    "idVigencia",
    "regUsuario",
    "rfhAnexoCredencials",
    "rfhFuncionario",
    "rfhNanCredencialHabs"
})
public class RfhCredencial {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhBaja;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhCreacion;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fhModificacion;
    protected Boolean idBorrador;
    protected Long idCredencial;
    protected Boolean idVigencia;
    protected RegUsuario regUsuario;
    @XmlElement(nillable = true)
    protected List<RfhAnexoCredencial> rfhAnexoCredencials;
    protected RfhFuncionario rfhFuncionario;
    @XmlElement(nillable = true)
    protected List<RfhNanCredencialHab> rfhNanCredencialHabs;

    /**
     * Gets the value of the fhBaja property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhBaja() {
        return fhBaja;
    }

    /**
     * Sets the value of the fhBaja property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhBaja(XMLGregorianCalendar value) {
        this.fhBaja = value;
    }

    /**
     * Gets the value of the fhCreacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhCreacion() {
        return fhCreacion;
    }

    /**
     * Sets the value of the fhCreacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhCreacion(XMLGregorianCalendar value) {
        this.fhCreacion = value;
    }

    /**
     * Gets the value of the fhModificacion property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFhModificacion() {
        return fhModificacion;
    }

    /**
     * Sets the value of the fhModificacion property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFhModificacion(XMLGregorianCalendar value) {
        this.fhModificacion = value;
    }

    /**
     * Gets the value of the idBorrador property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdBorrador() {
        return idBorrador;
    }

    /**
     * Sets the value of the idBorrador property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdBorrador(Boolean value) {
        this.idBorrador = value;
    }

    /**
     * Gets the value of the idCredencial property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdCredencial() {
        return idCredencial;
    }

    /**
     * Sets the value of the idCredencial property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdCredencial(Long value) {
        this.idCredencial = value;
    }

    /**
     * Gets the value of the idVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdVigencia() {
        return idVigencia;
    }

    /**
     * Sets the value of the idVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdVigencia(Boolean value) {
        this.idVigencia = value;
    }

    /**
     * Gets the value of the regUsuario property.
     * 
     * @return
     *     possible object is
     *     {@link RegUsuario }
     *     
     */
    public RegUsuario getRegUsuario() {
        return regUsuario;
    }

    /**
     * Sets the value of the regUsuario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegUsuario }
     *     
     */
    public void setRegUsuario(RegUsuario value) {
        this.regUsuario = value;
    }

    /**
     * Gets the value of the rfhAnexoCredencials property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhAnexoCredencials property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhAnexoCredencials().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhAnexoCredencial }
     * 
     * 
     */
    public List<RfhAnexoCredencial> getRfhAnexoCredencials() {
        if (rfhAnexoCredencials == null) {
            rfhAnexoCredencials = new ArrayList<RfhAnexoCredencial>();
        }
        return this.rfhAnexoCredencials;
    }

    /**
     * Gets the value of the rfhFuncionario property.
     * 
     * @return
     *     possible object is
     *     {@link RfhFuncionario }
     *     
     */
    public RfhFuncionario getRfhFuncionario() {
        return rfhFuncionario;
    }

    /**
     * Sets the value of the rfhFuncionario property.
     * 
     * @param value
     *     allowed object is
     *     {@link RfhFuncionario }
     *     
     */
    public void setRfhFuncionario(RfhFuncionario value) {
        this.rfhFuncionario = value;
    }

    /**
     * Gets the value of the rfhNanCredencialHabs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rfhNanCredencialHabs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRfhNanCredencialHabs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RfhNanCredencialHab }
     * 
     * 
     */
    public List<RfhNanCredencialHab> getRfhNanCredencialHabs() {
        if (rfhNanCredencialHabs == null) {
            rfhNanCredencialHabs = new ArrayList<RfhNanCredencialHab>();
        }
        return this.rfhNanCredencialHabs;
    }

}
