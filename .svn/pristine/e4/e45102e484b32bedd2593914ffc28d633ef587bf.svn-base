package es.grupoavalon.aecosan.secosan.rest.facade;

import java.util.Collection;
import java.util.List;

import es.grupoavalon.aecosan.connector.apoderamiento.dto.ConsultarApoderamientoResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.*;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.TasaDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.justificante.JustificantePagoReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.justificante.JustifierResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentRequestDTO;
import es.grupoavalon.aecosan.secosan.business.dto.pagos.online.pago.OnlinePaymentResponseDTO;
import es.grupoavalon.aecosan.secosan.business.dto.report.ResolucionProvisionalReportDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.*;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.BecasSolicitanteDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.becas.IdiomasBecasDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsFileDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.instructions.InstructionsListDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.ChangePasswordDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.LoginDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserClaveDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserInternalDTO;
import es.grupoavalon.aecosan.secosan.business.dto.user.UserNoEUDTO;
import es.grupoavalon.aecosan.secosan.business.exception.AutenticateException;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.MonitoringStatisticsWraper;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.PaginatedListWrapper;
import es.grupoavalon.aecosan.secosan.rest.response.wrapper.SolicitudDTOReadWrapper;
import es.grupoavalon.aecosan.secosan.util.MonitorizableConnectors;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.BooleanWrapper;
import es.grupoavalon.aecosan.secosan.util.rest.wrapper.TokenJWTWrapper;

public interface BusinessFacade {

	PaginatedListWrapper<SolicitudListDTO> getPaginatedList(String[] otherParams, PaginationParams paginationParams);

	PaginatedListWrapper<SolicitudListDTO> getPaginatedListByArea(String[] otherParams, PaginationParams paginationParams);

//	List<HistorialSolicitudDTO> getHistorialSolicitudList(Integer solicitudId);
	PaginatedListWrapper<HistorialSolicitudDTO> getHistorialSolicitudList(String[] otherParams, PaginationParams paginationParams);

	HistorialSolicitudDTO getHistorialSolicitudBySolicitudAndStatus(Integer solicitudId,Integer statusId);

	PaginatedListWrapper<MonitoringConnectorDTO> getPaginatedMonitoringConnector(String[] otherParams, PaginationParams paginationParams);

	InstructionsFileDTO findInstructionsFileBySolicitudeType(String solicitudeType);

	InstructionsFileDTO findInstructionsFileByForm(String formId);

	InstructionsListDTO findInstructionsLists(String userId);

	AttachableFileDTO downloadAttachableFileByName(String fileName);

	IdentificacionPeticionDTO findIdentificationRequestByAreaId(String areaID);

	List<CatalogDTO> getCatalogItems(String catalogName);

	List<CatalogDTO> getCatalogItems(String catalogName, String parentElement, String parentId);

	List<CatalogDTO> getCatalogItems(String catalogName, String tag);

	ConsultarApoderamientoResponseDTO consultarApoderamiento(String nif);

	void addSolicitud(String user, SolicitudDTO solicitudDTO);

	void updateSolicitud(String user, SolicitudDTO solicitudDTO);

	SolicitudDTOReadWrapper findSolicitudById(String user, String id);

	void deleteDocumentacionById(String user, String id);

	List<ServicioDTO> findServicesByPayTypeId(String payTypeId);

	void deleteSolicitud(String user, String solicitudDTO);

	DocumentacionDTO downloadDocumentationById(String docId);

	TokenJWTWrapper doLogin(LoginDTO user) throws AutenticateException;

	TokenJWTWrapper doLoginAdmin(LoginDTO user) throws AutenticateException;

	TokenJWTWrapper doLoginClave(UserClaveDTO user);

	List<TipoDocumentacionDTO> findDocumentationByFormId(String formId);

	ImpresoFirmadoDTO getPDFToSign(String user, ImpresoFirmadoDTO impresoFirmadoDTO);

	List<EstadoSolicitudDTO> getStatusByAreaId(String areaID);

	void solicitudeUpdateStatusBySolicitudeId(String user, ChangeStatusDTO statusdto);

	void addUserNoUeMembership(UserNoEUDTO user);

	void addUserInternal(UserInternalDTO user);

	void addTipoProcedimiento(TipoProcedimientoDTO tipoProcedimientoDTO);

	void deleteTipoProcedimiento(String userId, String userToDeleteId);

	List<TipoProcedimientoDTO> getTipoProcedimientoList();

	void uploadInstructionsToPay(InstructionsFileDTO ficheroInstrucciones);

	void uploadInstructions(InstructionsFileDTO ficheroInstrucciones);

	void checkAdminToken(TokenJWTWrapper token) throws AutenticateException;

	void checkToken(TokenJWTWrapper token) throws AutenticateException;

	void addUserClave(UserClaveDTO userClaveDTO);

	PaginatedListWrapper<UserInternalDTO> getInternalUserList(PaginationParams paginationParams);

	void deleteInternalUser(String userId, String userToDeleteId);

	void updateInternalUser(String userId, UserInternalDTO userToUpdate);

	JustifierResponseDTO getPaymentJustifier(JustifierRequestDTO justRequest);

	JustificantePagoReportDTO getJustificantePago(String identificadorPeticion,Long serviceId);



	OnlinePaymentResponseDTO makePayment(OnlinePaymentRequestDTO paymentRequest);

	BooleanWrapper isPaidBySolicitudeID(String identificadorPeticion);

	TasaDTO getTasaByFormId(String identificadorFormulario);

	TasaDTO getTasaByServicioId(String serviceId);

	PaginatedListWrapper<TasaDTO> getTasaPaginatedList(PaginationParams params);

	void updateTasa(TasaDTO tasaDto);

	void monitoringSuccess(MonitorizableConnectors connector);

	void monitoringError(MonitorizableConnectors connector, Throwable e);

	TokenJWTWrapper doLoginAdminClave(UserClaveDTO user) throws AutenticateException;

	MonitoringStatisticsWraper getMonitoringStatistics();

	void changeUserPassword(ChangePasswordDTO changePasswordDto);

	BooleanWrapper checkInternalUserExists(String dni);

	void deleteSuspendedProduct(String id);

	LogDTO getLog(String fileName);

	Collection<String> getLogNames();

	PaginatedListWrapper<BecasSolicitanteDTO> getBecasSolicitantesDTOPaginated(String[] otherParams,PaginationParams paginationParams);

	List<BecasSolicitanteDTO> getBecasSolicitantesDTO();

	List<IdiomasBecasDTO> getIdiomasBecasByBeca(Long beacaID);

	ResolucionProvisionalReportDTO getResolucionProvisionalReporte(String userId,String solicitudeId);

	List<BecasSolicitanteDTO> getBecasSolicitantesByStatusDTO(Long statusId);


}