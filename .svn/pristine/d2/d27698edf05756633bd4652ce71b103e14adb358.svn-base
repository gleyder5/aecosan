package es.grupoavalon.aecosan.secosan.util.servlet.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.property.PropertyException;

public class HeadersSecurityFilter implements Filter {
	protected static final Logger logger = LoggerFactory.getLogger(HeadersSecurityFilter.class);

	private static final String X_FORWARDED_FOR = "x-forwarded-for";
	private static final String X_FORWARDED_HOST = "x-forwarded-host";
	private static final String X_FORWARDED_SERVER = "x-forwarded-server";
	private static final String X_FORWARDED_PROTO = "x-forwarded-proto";
	private static final String HOST = "host";
	private static final String PORT_OPTIONAL_REGEX = "(:*\\d+)*";
	private static final String BASIC_HOST_PATTERN = "^((http:\\/\\/)|(https:\\/\\/))*[A-Za-z0-9_\\-\\.]+";
	private static final String DEFAULT_TRUSTED = ".msc.es" + PORT_OPTIONAL_REGEX + ";.msssi.gob.es,gob.es" + PORT_OPTIONAL_REGEX + ";192.168.\\d{1,3}.\\d{1,3}" + PORT_OPTIONAL_REGEX;
	private static final String ADDITIONAL_TRUSTED_CONFIG_PARAM = "trusted";
	private static final String ACTIVATE_CONFIG_PARAM = "activate";
	private static final String REDIRECT_IF_NOT_TRUSTED = "redirectTo";
	private static final String PATTERN_RESOURCES_TYPE_IGNORE = "((.JPG|.jpg)|(.PNG|.png)|(.map|.MAP)|(.css|.CSS)|(.js|.JS)|(.bmp|.BMP)|(.gif|.GIF)|(.json|.JSON))";

	private List<String> trustedServersPatters;
	private Pattern trustedServersRegex;
	private boolean activate = true;
	private String headersValues = "";
	private String redirectToIfNotTrusted;
	private Pattern staticContentPattern;

	@Override
	public void init(FilterConfig config) throws ServletException {

		try {
			this.setInitParams(config);
			trustedServersPatters = getAllTrustedServers(config);
			trustedServersRegex = Pattern.compile(generateTrustedServerRegex(trustedServersPatters));
			staticContentPattern = Pattern.compile(PATTERN_RESOURCES_TYPE_IGNORE);
		} catch (Exception e) {
			logger.warn("Header Security deactivated due an error, the application will not check the headers", e);
			this.activate = false;
		}

	}

	private static List<String> getAllTrustedServers(FilterConfig config) {
		List<String> trustedServersPatters = new ArrayList<String>();

		trustedServersPatters.addAll(getDefaultTrustedServers());
		trustedServersPatters.addAll(getTrustedServers(config));
		trustedServersPatters.addAll(getExtraTrustedServerFromProperties());

		return trustedServersPatters;
	}

	private static List<String> getDefaultTrustedServers() {
		return getTrustedServers(DEFAULT_TRUSTED);
	}

	private static List<String> getTrustedServers(FilterConfig config) {
		List<String> trustedServersPatters = new ArrayList<String>();
		try {
			String comaSeparatedTrusted = config.getInitParameter(ADDITIONAL_TRUSTED_CONFIG_PARAM);
			if (StringUtils.isNotBlank(comaSeparatedTrusted)) {
				trustedServersPatters = getTrustedServers(comaSeparatedTrusted);
			}
		} catch (Exception e) {
			logger.warn("Can not obtain coma separated list of trusted domians from initial configuration param, using default only, check web.xml definition", e);
		}
		return trustedServersPatters;
	}

	private static List<String> getExtraTrustedServerFromProperties() {
		List<String> trustedServersPatterns = new ArrayList<String>();
		try {
			String extraTrustedServers = Properties.getString(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_EXTRA_TRUSTED_SERVER_REGEX_FILTER);
			if (StringUtils.isNotBlank(extraTrustedServers)) {
				trustedServersPatterns.addAll(getTrustedServers(extraTrustedServers));
			}
		} catch (PropertyException e) {
			logger.info("Not extra trusted server loaded from property file");
		}
		return trustedServersPatterns;
	}

	private static List<String> getTrustedServers(String comaSeparatedServers) {
		List<String> trustedServersPatters = new ArrayList<String>();
		String[] separatedServers = comaSeparatedServers.split(";");

		for (String servers : separatedServers) {
			trustedServersPatters.add(servers);
		}
		return trustedServersPatters;
	}

	private static String generateTrustedServerRegex(List<String> trustedServersPatters) {
		StringBuilder builder = new StringBuilder(BASIC_HOST_PATTERN);

		for (int i = 0; i < trustedServersPatters.size(); i++) {
			String pattern = trustedServersPatters.get(i);
			builder.append("(");
			builder.append(pattern);
			builder.append(")");
			if (i < (trustedServersPatters.size() - 1)) {
				builder.append("|");
			}

		}
		String regex = builder.toString();
		logger.info("Request regex: {} ", regex);
		return regex;
	}

	private void setInitParams(FilterConfig config) throws ServletException {
		this.activate = getActivated(config);
		this.redirectToIfNotTrusted = getRedirectIfNotTrusted(config);
	}

	private static boolean getActivated(FilterConfig config) {
		boolean activate = true;
		try {
			String activatedParam = config.getInitParameter(ACTIVATE_CONFIG_PARAM);
			if (StringUtils.isNotBlank(activatedParam) && StringUtils.equalsIgnoreCase(Boolean.FALSE.toString(), activatedParam)) {
				activate = false;
				logger.warn("Security header filter will not be activated, " + ACTIVATE_CONFIG_PARAM + " param = false in web.xml");
			}
		} catch (Exception e) {
			logger.warn("Can not obtain " + ACTIVATE_CONFIG_PARAM + " param, using default true value");
		}
		return activate;
	}

	private static String getRedirectIfNotTrusted(FilterConfig config) throws ServletException {
		String redirectTo = null;
		try {
			redirectTo = config.getInitParameter(REDIRECT_IF_NOT_TRUSTED);
			if (StringUtils.isBlank(redirectTo)) {

				throw new ServletException("It's requiered to configure the param " + REDIRECT_IF_NOT_TRUSTED + " for filter " + config.getFilterName());
			}
		} catch (Exception e) {
			throw new ServletException("The required param " + REDIRECT_IF_NOT_TRUSTED + " for filter " + config.getFilterName() + " can not be configured due an error: " + e.getMessage(), e);
		}
		return redirectTo;
	}

	@Override
	public void destroy() {
		this.trustedServersPatters = null;
		this.trustedServersRegex = null;
		this.headersValues = "";
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		this.headersValues = "";
		try {
			if (!checkIfRequestIsStaticContent(httpRequest)) {
				checkHeaders(httpRequest, httpResponse);
			}
		} catch (Exception e) {
			logger.warn("can not check headers due an error:", e);
		}
		chain.doFilter(request, response);
	}

	private boolean checkIfRequestIsStaticContent(HttpServletRequest httpRequest) {
		boolean isStaticContent = false;
		String requestUri = httpRequest.getRequestURI();
		Matcher match = staticContentPattern.matcher(requestUri);
		if (match.find()) {
			isStaticContent = true;
		}
		return isStaticContent;
	}

	private void checkHeaders(HttpServletRequest httpRequest, HttpServletResponse response) throws ServletException, IOException {

		if (this.activate) {

			boolean isValidForwardHeader = checkForwardHeader(httpRequest);
			boolean isValidHostHeader = checkHostHeader(httpRequest);
			if (isNotTrustedRequestHeaders(isValidForwardHeader, isValidHostHeader)) {
				handleNotTrustedXForward(httpRequest, response);
				logger.warn("Request headers(x-forwarded-*, {} do not match the expected pattern, values: {} ",HOST, headersValues);
			}
		}
	}



	private static boolean isNotTrustedRequestHeaders(boolean isValidForwardHeader, boolean isValidHostHeader) {
		boolean isNotTrusted = true;
		if ((isValidForwardHeader && !isValidHostHeader) || (isValidForwardHeader && isValidHostHeader)) {
			isNotTrusted = false;
		}
		return isNotTrusted;
	}

	private void handleNotTrustedXForward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String originalUrl = request.getRequestURL().toString();

		if (checkIfRequestIsForSpringAction(originalUrl)) {
			String originalUrlInBase64 = transformStringToBase64(originalUrl);
			request.setAttribute(SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL, originalUrlInBase64);

		} else if (checkIfRequestIsForRest(originalUrl)) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Request contains x-forwarded- with not trusted server:" + this.headersValues);
		} else {
			response.sendRedirect(this.redirectToIfNotTrusted + generateUrlParamsForForwarding(originalUrl));
		}

	}

	private static boolean checkIfRequestIsForSpringAction(String originalUrl) {
		boolean isForSpringAction = false;
		if (StringUtils.contains(originalUrl, ".do")) {
			isForSpringAction = true;
		}

		return isForSpringAction;
	}

	private static boolean checkIfRequestIsForRest(String originalUrl) {
		boolean isForRest = false;
		if (StringUtils.contains(originalUrl, "/rest/")) {
			isForRest = true;
		}

		return isForRest;
	}

	private static String generateUrlParamsForForwarding(String originalUrl) {
		String originalUrlInBase64 = transformStringToBase64(originalUrl);
		return "?" + SecosanConstants.REQUEST_PARAM_X_FORWARD_ORIGINAL + "=" + originalUrlInBase64;
	}

	private static String transformStringToBase64(String string) {
		return Base64.encodeBase64String(string.getBytes());
	}

	private boolean checkForwardHeader(final HttpServletRequest request) {

		final Enumeration<String> xForwardedFor = concatAllXForwaredHeaders(request);
		return validateUrlHeaders(xForwardedFor);

	}

	@SuppressWarnings("unchecked")
	private static Enumeration<String> concatAllXForwaredHeaders(HttpServletRequest request) {
		final Enumeration<String> xForwardedFor = request.getHeaders(X_FORWARDED_FOR);
		final Enumeration<String> xForwardedHost = request.getHeaders(X_FORWARDED_HOST);
		final Enumeration<String> xForwardedServer = request.getHeaders(X_FORWARDED_SERVER);
		final Enumeration<String> xForwardedProto = request.getHeaders(X_FORWARDED_PROTO);
		List<Enumeration<String>> mergedXforwardHeaders = new ArrayList<Enumeration<String>>();
		addNotEmptyHeader(mergedXforwardHeaders, xForwardedFor);
		addNotEmptyHeader(mergedXforwardHeaders, xForwardedServer);
		addNotEmptyHeader(mergedXforwardHeaders, xForwardedHost);
		addNotEmptyHeader(mergedXforwardHeaders, xForwardedProto);

		return new ConcatedEnum<String>(Collections.enumeration(mergedXforwardHeaders));
	}

	private static void addNotEmptyHeader(List<Enumeration<String>> mergedXforwardHeaders, Enumeration<String> headers) {
		if (headers.hasMoreElements()) {
			mergedXforwardHeaders.add(headers);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean checkHostHeader(final HttpServletRequest request) {

		final Enumeration<String> hostHeader = request.getHeaders(HOST);
		return validateUrlHeaders(hostHeader);

	}

	private boolean validateUrlHeaders(final Enumeration<String> headers) {
		boolean trustHeader = true;
		if (headers != null) {
			while (headers.hasMoreElements() && trustHeader) {
				final String[] ips = headers.nextElement().split(",");
				for (int i = 0; i < ips.length; i++) {
					final String proxy = ips[i].trim();

					if (!"unknown".equals(proxy) && !proxy.isEmpty()) {
						trustHeader = urlMacthesTrustedSites(proxy);
						if (!trustHeader) {
							this.headersValues += "|" + proxy;
							break;
						}
					}
				}
			}
		}
		return trustHeader;
	}

	private boolean urlMacthesTrustedSites(String url) {
		boolean urlMatch = true;
		if (StringUtils.isNotBlank(url) && !urlIsIp(url)) {

			Matcher matcher = this.trustedServersRegex.matcher(url);
			urlMatch = matcher.matches();
		}
		return urlMatch;
	}

	private static boolean urlIsIp(String url) {
		return url.matches("(\\d{1,3}.){4}");
	}
	
	private static class ConcatedEnum <T> extends Object implements Enumeration<T> {
		  /** enumeration of Enumerations */
		  private Enumeration<? extends Enumeration<? extends T>> en;

		  /** current enumeration */
		  private Enumeration<? extends T> current;

		  /** is {@link #current} up-to-date and has more elements?
		  * The combination <CODE>current == null</CODE> and
		  * <CODE>checked == true means there are no more elements
		  * in this enumeration.
		  */
		  private boolean checked = false;

		  /** Constructs new enumeration from already existing. The elements
		  * of <CODE>en</CODE> should be also enumerations. The resulting
		  * enumeration contains elements of such enumerations.
		  *
		  * @param en enumeration of Enumerations that should be sequenced
		  */
		  public ConcatedEnum(Enumeration<? extends Enumeration <? extends T>> en) {
		      this.en = en;
		  }

		  /** Ensures that current enumeration is set. If there aren't more
		  * elements in the Enumerations, sets the field <CODE>current</CODE> to null.
		  */
		  private void ensureCurrent() {
		      while ((current == null) || !current.hasMoreElements()) {
		          if (en.hasMoreElements()) {
		              current = en.nextElement();
		          } else {
		              // no next valid enumeration
		              current = null;

		              return;
		          }
		      }
		  }

		  /** @return true if we have more elements */
		  @Override
		public boolean hasMoreElements() {
		      if (!checked) {
		          ensureCurrent();
		          checked = true;
		      }

		      return current != null;
		  }

		  /** @return next element
		  * @exception NoSuchElementException if there is no next element
		  */
		  @Override
		public T nextElement() {
		      if (!checked) {
		          ensureCurrent();
		      }

		      if (current != null) {
		          checked = false;

		          return current.nextElement();
		      } else {
		          checked = true;
		          throw new java.util.NoSuchElementException();
		      }
		  }
	}
}
