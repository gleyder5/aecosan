package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.grupoavalon.aecosan.secosan.dao.AreaDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.AreaEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.library.NamedQueriesLibrary;
import es.grupoavalon.aecosan.secosan.dao.repository.AreaRepository;

@Repository
public class AreaDAOImpl extends GenericDao implements AreaDAO {

	@Autowired
	private AreaRepository areaRepository;

	@Override
	public AreaEntity findArea(Long pk) {

		AreaEntity area = null;
		try {
			area = areaRepository.findOne(pk);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findArea | " + pk + " ERROR: " + exception.getMessage());
		}

		return area;
	}

	@Override
	public AreaEntity findAreaByFormId(Long pk) {
		AreaEntity area = null;
		try {
			Map<String, Object> queryParams = new HashMap<String, Object>();
			queryParams.put("formId", Long.valueOf(pk));
			area = areaRepository.findOneByNamedQuery(NamedQueriesLibrary.GET_AREA_BY_FORMULARIO_ID, queryParams);
		} catch (Exception exception) {
			handleException(exception, DaoException.CRUD_OPERATION_ERROR + "findAreaByFormId | " + pk + " ERROR: " + exception.getMessage());
		}

		return area;
	}

}
