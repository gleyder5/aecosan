package es.grupoavalon.aecosan.secosan.business.dto.solicitud.alimentos;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import es.grupoavalon.aecosan.secosan.business.dto.IsNulable;
import es.grupoavalon.aecosan.secosan.util.dtofactory.factory.annotation.BeanToBeanMapping;

public class IngredientesDTO implements IsNulable<IngredientesDTO>{

	@JsonProperty("id")
	@XmlElement(name = "id")
	@BeanToBeanMapping(getValueFrom = "id")
	private Long id;

	@JsonProperty("incluyeVitaminas")
	@XmlElement(name = "incluyeVitaminas")
	@BeanToBeanMapping(getValueFrom = "incluyeVitaminas")
	private Boolean incluyeVitaminas;

	@JsonProperty("incluyeNuevosIngredientes")
	@XmlElement(name = "incluyeNuevosIngredientes")
	@BeanToBeanMapping(getValueFrom = "incluyeNuevosIngredientes")
	private Boolean incluyeNuevosIngredientes;

	@JsonProperty("nuevoIngrediente")
	@XmlElement(name = "nuevoIngrediente")
	@BeanToBeanMapping(getValueFrom = "nuevoIngrediente")
	private String nuevoIngrediente;

	@JsonProperty("otrasSustancias")
	@XmlElement(name = "otrasSustancias")
	@BeanToBeanMapping(getValueFrom = "otrasSustancias")
	private String otrasSustancias;

	@JsonProperty("situacion")
	@XmlElement(name = "situacion")
	@BeanToBeanMapping(generateOther = true, getValueFrom = "situacion")
	private SituacionDTO situacion;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIncluyeVitaminas() {
		return incluyeVitaminas;
	}

	public void setIncluyeVitaminas(Boolean incluyeVitaminas) {
		this.incluyeVitaminas = incluyeVitaminas;
	}

	public Boolean isIncluyeNuevosIngredientes() {
		return incluyeNuevosIngredientes;
	}

	public void setIncluyeNuevosIngredientes(Boolean incluyeNuevosIngredientes) {
		this.incluyeNuevosIngredientes = incluyeNuevosIngredientes;
	}

	public String getNuevoIngrediente() {
		return nuevoIngrediente;
	}

	public void setNuevoIngrediente(String nuevoIngrediente) {
		this.nuevoIngrediente = nuevoIngrediente;
	}

	public String getOtrasSustancias() {
		return otrasSustancias;
	}

	public void setOtrasSustancias(String otrasSustancias) {
		this.otrasSustancias = otrasSustancias;
	}

	public SituacionDTO getSituacion() {
		return situacion;
	}

	public void setSituacion(SituacionDTO situacion) {
		this.situacion = situacion;
	}

	@Override
	public String toString() {
		return "IngredientesDTO [id=" + id + ", incluyeVitaminas=" + incluyeVitaminas + ", incluyeNuevosIngredientes=" + incluyeNuevosIngredientes + ", nuevoIngrediente=" + nuevoIngrediente
				+ ", otrasSustancias=" + otrasSustancias + ", situacion=" + situacion + "]";
	}

	@Override
	public IngredientesDTO shouldBeNull() {
		if (checkNullability()) {
			return null;
		}
		return this;
	}
	
	protected boolean checkNullability(){
		boolean nullabillity = true;
		nullabillity = nullabillity && getId() == null;
		nullabillity = nullabillity && getIncluyeVitaminas() == null;
		nullabillity = nullabillity && isIncluyeNuevosIngredientes() == null;
		nullabillity = nullabillity && StringUtils.isEmpty(this.getNuevoIngrediente());
		nullabillity = nullabillity && StringUtils.isEmpty(this.getOtrasSustancias());
		nullabillity = nullabillity && situacion.shouldBeNull() == null;
		return nullabillity;
	}

}
