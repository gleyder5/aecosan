//http://manikanta.com/blog/2013/07/25/decorate-angularjs-http-service-to-convert-put/
/** AngularJS $http service decorator to convert PUT, DELETE as POST */
'use strict';
angular.module('secosanGeneric').config([ '$provide', '$injector', function($provide, $injector) {

	// configure http provider to convert 'PUT', 'DELETE' methods to 'POST'
	// requests
	$provide.decorator('$http', [ '$delegate', '$injector', function($http, $injector) {
		// create function which overrides $http function
		var httpStub = function(method) {
			return function(url, data, config) {
				// AngularJS $http.delete takes 2nd argument as 'config' object
				// 'data' will come as 'config.data'
				if (method === 'delete') {
					config = data;
					config && (data = config.data);
				}

				config || (config = {});
				config.headers || (config.headers = {});

				// override actual request method with 'POST' request
				config.method = 'POST';

				// set the actual method in the header
				config.headers['X-HTTP-Method-Override'] = method;

				return $http(angular.extend(config, {
					url : url,
					data : data
				}));
			};
		};

		// backup of original methods
		$http._put = $http.put;
		$http._delete = $http['delete'];

		// override the methods
		$http.put = httpStub('put');
		$http['delete'] = httpStub('delete');

		// console.log("$http override");
		return $http;
	} ]);

} ]);