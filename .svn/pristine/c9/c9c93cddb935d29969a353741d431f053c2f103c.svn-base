package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.financiacionAlimentos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_PRODUCTOS_SUSPENDIDOS)
public class ProductoSuspendidoEntity {

	@Id
	@GeneratedValue(generator = SequenceNames.PRODUCTOS_SUSPENDIDOS_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.PRODUCTOS_SUSPENDIDOS_SEQUENCE, sequenceName = SequenceNames.PRODUCTOS_SUSPENDIDOS_SEQUENCE, allocationSize = 1)
	@Column(name = "ID")
	private Long id;

	@Column(name = "NUM_REG_PROD")
	private String productRegistrationNumber;

	@Column(name = "COD_IDENTIFICATIVO")
	private String identificationCode;

	@Column(name = "DESCRIPCION")
	private String description;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "BAJA_ALIM_UME_ID", referencedColumnName = "ID")
	private BajaOferAlimUMEEntity bajaOferAlim;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductRegistrationNumber() {
		return productRegistrationNumber;
	}

	public void setProductRegistrationNumber(String productRegistrationNumber) {
		this.productRegistrationNumber = productRegistrationNumber;
	}

	public String getIdentificationCode() {
		return identificationCode;
	}

	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BajaOferAlimUMEEntity getBajaOferAlim() {
		return bajaOferAlim;
	}

	public void setBajaOferAlim(BajaOferAlimUMEEntity bajaOferAlim) {
		this.bajaOferAlim = bajaOferAlim;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bajaOferAlim == null) ? 0 : bajaOferAlim.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((identificationCode == null) ? 0 : identificationCode.hashCode());
		result = prime * result + ((productRegistrationNumber == null) ? 0 : productRegistrationNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductoSuspendidoEntity other = (ProductoSuspendidoEntity) obj;
		if (bajaOferAlim == null) {
			if (other.bajaOferAlim != null)
				return false;
		} else if (!bajaOferAlim.equals(other.bajaOferAlim))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (identificationCode == null) {
			if (other.identificationCode != null)
				return false;
		} else if (!identificationCode.equals(other.identificationCode))
			return false;
		if (productRegistrationNumber == null) {
			if (other.productRegistrationNumber != null)
				return false;
		} else if (!productRegistrationNumber.equals(other.productRegistrationNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ProductoSuspendidoEntity [id=" + id + ", productRegistrationNumber=" + productRegistrationNumber + ", identificationCode=" + identificationCode + ", description=" + description
				+ ", bajaOferAlim=" + bajaOferAlim + "]";
	}

}
