package es.grupoavalon.aecosan.secosan.dao.entity;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.SolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_REGISTRO_ACTIVIDADES)
@SequenceGenerator(name = SequenceNames.REGISTRO_ACTIVIDADES_SEQUENCE, sequenceName = SequenceNames.REGISTRO_ACTIVIDADES_SEQUENCE, initialValue = 1, allocationSize = 100)
public class RegistroActividadEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SequenceNames.REGISTRO_ACTIVIDADES_SEQUENCE)
	private Long id;

	@Column(name = "FECHA", nullable = false)
	private Timestamp date = new Timestamp(System.currentTimeMillis());

	@Column(name = "COMENTARIO", nullable = true, length = 4000)
	private String comentary;

	@ManyToOne(optional = true, fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "USUARIO")
	private UsuarioEntity user;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ACCION", nullable = false)
	private AccionesEntity action;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SOLICITUD_ID")
	private SolicitudEntity solicitud;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	public String getComentary() {
		return comentary;
	}

	public void setComentary(String comentary) {
		this.comentary = comentary;
	}

	public UsuarioEntity getUser() {
		return user;
	}

	public void setUser(UsuarioEntity user) {
		this.user = user;
	}

	public AccionesEntity getAction() {
		return action;
	}

	public void setAction(AccionesEntity action) {
		this.action = action;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.getIdAsString().hashCode());
		result = prime * result + ((comentary == null) ? 0 : comentary.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((solicitud == null) ? 0 : solicitud.getId().hashCode());
		result = prime * result + ((user == null) ? 0 : user.getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegistroActividadEntity other = (RegistroActividadEntity) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.getIdAsString().equals(other.action.getIdAsString()))
			return false;
		if (comentary == null) {
			if (other.comentary != null)
				return false;
		} else if (!comentary.equals(other.comentary))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (solicitud == null) {
			if (other.solicitud != null)
				return false;
		} else if (!solicitud.getId().equals(other.solicitud.getId()))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String userId = "";
		String actionId = "";
		String solicitudId = "";

		if (user != null) {
			userId = String.valueOf(user.getId());
		}

		if (action != null) {
			actionId = action.getIdAsString();
		}

		if (solicitud != null) {
			solicitudId = String.valueOf(solicitud.getId());
		}

		return "RegistroActividadEntity [id=" + id + ", date=" + date + ", comentary=" + comentary + ", user=" + userId + ", action=" + actionId + ", solicitud=" + solicitudId + "]";
	}

}
