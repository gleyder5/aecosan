package es.grupoavalon.aecosan.secosan.dao.entity.solicitud.alimentos;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import es.grupoavalon.aecosan.secosan.dao.entity.PaisEntity;
import es.grupoavalon.aecosan.secosan.dao.library.SequenceNames;
import es.grupoavalon.aecosan.secosan.dao.library.TableNames;

@Entity
@Table(name = TableNames.TABLA_ANTECEDENTES_PROCEDENCIA)
public class AntecedentesProcedenciaEntity {

	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(generator = SequenceNames.ANTECEDENTES_PROCEDENCIA_SEQUENCE)
	@SequenceGenerator(name = SequenceNames.ANTECEDENTES_PROCEDENCIA_SEQUENCE, sequenceName = SequenceNames.ANTECEDENTES_PROCEDENCIA_SEQUENCE, allocationSize = 1)
	private Long id;

	@Column(name = "PRIMERA_COMERCIALIZACION_UE")
	private Boolean primeraComercializacionUE;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PAIS_PROCEDENCIA_ID", referencedColumnName = "ID")
	private PaisEntity paisProcedencia;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "PAIS_COMERC_PREVIA_ID", referencedColumnName = "ID")
	private PaisEntity paisComercializacionPrevia;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getPrimeraComercializacionUE() {
		return primeraComercializacionUE;
	}

	public void setPrimeraComercializacionUE(Boolean primeraComercializacionUE) {
		this.primeraComercializacionUE = primeraComercializacionUE;
	}

	public PaisEntity getPaisProcedencia() {
		return paisProcedencia;
	}

	public void setPaisProcedencia(PaisEntity paisProcedencia) {
		this.paisProcedencia = paisProcedencia;
	}

	public PaisEntity getPaisComercializacionPrevia() {
		return paisComercializacionPrevia;
	}

	public void setPaisComercializacionPrevia(PaisEntity paisComercializacionPrevia) {
		this.paisComercializacionPrevia = paisComercializacionPrevia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((paisComercializacionPrevia == null) ? 0 : paisComercializacionPrevia.getIdPais().hashCode());
		result = prime * result + ((paisProcedencia == null) ? 0 : paisProcedencia.getIdPais().hashCode());
		result = prime * result + ((primeraComercializacionUE == null) ? 0 : primeraComercializacionUE.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AntecedentesProcedenciaEntity other = (AntecedentesProcedenciaEntity) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (paisComercializacionPrevia == null) {
			if (other.paisComercializacionPrevia != null)
				return false;
		} else if (!paisComercializacionPrevia.getIdAsString().equals(other.paisComercializacionPrevia.getIdAsString()))
			return false;
		if (paisProcedencia == null) {
			if (other.paisProcedencia != null)
				return false;
		} else if (!paisProcedencia.getIdAsString().equals(other.paisProcedencia.getIdAsString()))
			return false;
		if (primeraComercializacionUE == null) {
			if (other.primeraComercializacionUE != null)
				return false;
		} else if (!primeraComercializacionUE.equals(other.primeraComercializacionUE))
			return false;
		return true;
	}

	@Override
	public String toString() {
		String idPaisProcedencia = "";
		String idPaisComercializacionPrevia = "";

		if (paisProcedencia != null) {
			idPaisProcedencia = paisProcedencia.getIdAsString();
		}

		if (paisComercializacionPrevia != null) {
			idPaisComercializacionPrevia = paisComercializacionPrevia.getIdAsString();
		}

		return "AntecedentesProcedenciaEntity [id=" + id + ", primeraComercializacionUE=" + primeraComercializacionUE + ", paisProcedencia=" + idPaisProcedencia + ", paisComercializacionPrevia="
				+ idPaisComercializacionPrevia + "]";
	}

}
