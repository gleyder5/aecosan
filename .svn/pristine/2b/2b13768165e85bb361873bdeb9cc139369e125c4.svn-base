package es.grupoavalon.aecosan.secosan.business.impl;

import es.grupoavalon.aecosan.secosan.dao.HistorialSolicitudDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.grupoavalon.aecosan.secosan.business.RegistroIndustriasService;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.SolicitudDTO;
import es.grupoavalon.aecosan.secosan.business.dto.solicitud.registroIndustrias.RegistroIndustriasDTO;
import es.grupoavalon.aecosan.secosan.business.exception.ServiceException;
import es.grupoavalon.aecosan.secosan.dao.CatalogDAO;
import es.grupoavalon.aecosan.secosan.dao.RegistroIndustriasDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.registroIndustrias.RegistroIndustriasEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanUtil;

@Service
public class RegistroIndustriasServiceImpl extends AbstractSolicitudService implements RegistroIndustriasService {

	@Autowired
	private RegistroIndustriasDAO registroIndustriasDAO;
	@Autowired
	private CatalogDAO catalog;

	@Autowired
	private HistorialSolicitudDAO historialSolicitudDAO;

	@Override
	public void add(String user, SolicitudDTO registroIndustriasDTO) {
		try {
			solicitudPrepareNullDTO(registroIndustriasDTO);
			prepareRegistroIndustriasNullability((RegistroIndustriasDTO) registroIndustriasDTO);
			addRegistroIndustrias(user, (RegistroIndustriasDTO) registroIndustriasDTO);
		} catch (

		Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " add | ERROR: " + registroIndustriasDTO);
		}
	}

	@Override
	public void update(String user, SolicitudDTO registroIndustriasDTO) {
		try {
			solicitudPrepareNullDTO(registroIndustriasDTO);
			prepareRegistroIndustriasNullability((RegistroIndustriasDTO) registroIndustriasDTO);
			updateRegistroIndustrias(user, (RegistroIndustriasDTO) registroIndustriasDTO);
		} catch (Exception e) {
			handleException(e, ServiceException.SERVICE_ERROR + " update | ERROR: " + registroIndustriasDTO);
		}
	}

	private void prepareRegistroIndustriasNullability(RegistroIndustriasDTO registroIndustriasDTO) {
		registroIndustriasDTO.setPaisOrigen(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getPaisOrigen()));
		registroIndustriasDTO.setTipoEnvio(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getTipoEnvio()));
		registroIndustriasDTO.setShippingData(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getShippingData()));
		prepareNullUbicacionGeograficaShippingDataDTO(registroIndustriasDTO);

		registroIndustriasDTO.setSolicitudePayment(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getSolicitudePayment()));
		if (null != registroIndustriasDTO.getSolicitudePayment()) {
			registroIndustriasDTO.getSolicitudePayment().setPayment(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getSolicitudePayment().getPayment()));
			registroIndustriasDTO.getSolicitudePayment().setIdPayment(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getSolicitudePayment().getIdPayment()));
			registroIndustriasDTO.getSolicitudePayment().setDataPayment(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getSolicitudePayment().getDataPayment()));
			if (null != registroIndustriasDTO.getSolicitudePayment().getIdPayment()) {
				registroIndustriasDTO.getSolicitudePayment().getIdPayment().setLocation(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getSolicitudePayment().getIdPayment().getLocation()));
				prepareLocationNull(registroIndustriasDTO.getSolicitudePayment().getIdPayment().getLocation());
			}
		}
	}

	private void prepareNullUbicacionGeograficaShippingDataDTO(RegistroIndustriasDTO registroIndustriasDTO) {
		if (registroIndustriasDTO.getShippingData() != null) {
			prepareUbicacionShippingDat(registroIndustriasDTO);
		}
	}

	private void prepareUbicacionShippingDat(RegistroIndustriasDTO registroIndustriasDTO) {
		if (registroIndustriasDTO.getShippingData() != null) {
			registroIndustriasDTO.getShippingData().setLocation(SecosanUtil.checkDtoNullability(registroIndustriasDTO.getShippingData().getLocation()));
			prepareLocationNull(registroIndustriasDTO.getShippingData().getLocation());
		}
	}

	private void addRegistroIndustrias(String user, RegistroIndustriasDTO registroIndustriasDTO) {
		RegistroIndustriasEntity registroIndustriasEntity = generateRegistroIndustriasEntityFromDTO(registroIndustriasDTO);
		prepareAddSolicitudEntity(registroIndustriasEntity, registroIndustriasDTO);
		setUserCreatorByUserId(user, registroIndustriasEntity);
		RegistroIndustriasEntity registroIndustriasEntityNew  = registroIndustriasDAO.addRegistroIndustrias(registroIndustriasEntity);
		if(registroIndustriasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroIndustriasEntityNew);
		}
	}

	private RegistroIndustriasEntity generateRegistroIndustriasEntityFromDTO(RegistroIndustriasDTO registroIndustriasDTO) {
		logger.debug("generateRegistroIndustriasEntityFromDTO || registroIndustriasDTO: " + registroIndustriasDTO);
		prepareSolicitudDTO(registroIndustriasDTO);
		RegistroIndustriasEntity registroIndustriasEntity = transformDTOToEntity(registroIndustriasDTO, RegistroIndustriasEntity.class);
		logger.debug("generateRegistroIndustriasEntityFromDTO || registroIndustriasEntity: " + registroIndustriasEntity);
		return registroIndustriasEntity;
	}

	private void updateRegistroIndustrias(String user, RegistroIndustriasDTO registroIndustriasDTO) {
		RegistroIndustriasEntity registroIndustriasEntity = transformDTOToEntity(registroIndustriasDTO, RegistroIndustriasEntity.class);
		prepareUpdateSolicitudEntity(registroIndustriasEntity, registroIndustriasDTO);
		setUserCreatorByUserId(user, registroIndustriasEntity);
		registroIndustriasDAO.updateRegistroIndustrias(registroIndustriasEntity);
		if(registroIndustriasDTO.getStatus()!=1) {
			historialSolicitudDAO.addHistorialSolicitud(registroIndustriasEntity);
		}
	}
}
