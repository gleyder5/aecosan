package es.grupoavalon.aecosan.secosan.util.rest.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.business.JWTTokenKeyService;
import es.grupoavalon.aecosan.secosan.util.SecosanConstants;
import es.grupoavalon.aecosan.secosan.util.property.Properties;
import es.grupoavalon.aecosan.secosan.util.property.PropertiesFilesNames;
import es.grupoavalon.aecosan.secosan.util.property.PropertyException;

@Component
public class JwtUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);
	private static final long DEFAULT_DURATION_TIME = 600000;

	private static final String ISSUED_AT_PAYLOAD_KEY = "issuedAt";
	private static final String EXPIRATION_PAYLOAD_KEY = "expiration";

	public static final boolean ADMIN_TOKEN = true;
	public static final boolean NORMAL_TOKEN = false;

	private long milisecOfDuration;
	
	@Autowired
	private JWTTokenKeyService jWTTokenKeyService;

	@PostConstruct
	public void postConstruct() {
		
		this.milisecOfDuration = milisecOfTokenDuration();
		logger.info("Token duration in milisec: {}", this.milisecOfDuration);
	}

	
	public String getJwtToken(Map<String, Object> payload, Boolean... adminToken) {
		String jwtKey = getJwtKey(adminToken);
		logger.info("TokenKey for create token: {}",jwtKey);
		return Jwts.builder().setClaims(generatePayload(payload))
				.signWith(SignatureAlgorithm.HS512, jwtKey).compact();
	}

	private String getJwtKey(Boolean... adminToken) {
		String jwtKey;
		if (adminToken != null && adminToken.length > 0 && adminToken[0]) {
			jwtKey = jWTTokenKeyService.getTokenKeyAdmin();
		} else {
			jwtKey = jWTTokenKeyService.getTokenKey();
		}
		return jwtKey;
	}

	private Claims generatePayload(Map<String, Object> payload) {
		if (payload == null) {
			payload = new HashMap<String, Object>(1);
		}
		Date issueDate = new Date();
		Date expiration = new Date(issueDate.getTime() + this.milisecOfDuration);
		payload.put(ISSUED_AT_PAYLOAD_KEY, issueDate.getTime());
		payload.put(EXPIRATION_PAYLOAD_KEY, expiration.getTime());
		return Jwts.claims(payload);
	}

	public boolean isTokenValid(String compactJwt, Boolean... adminToken) {
		boolean isValid = true;
		String jwtKey = getJwtKey(adminToken);
		try {
			logger.info("TokenKey for validating token: {}", jwtKey);
			logger.info("Token: {} " , compactJwt);
			Jws<Claims> claims = Jwts.parser().setSigningKey(jwtKey)
					.parseClaimsJws(compactJwt);

			if (tokenIsExpired(claims)) {
				isValid = false;
			}

		} catch (SignatureException e) {
			isValid = false;
			logger.warn("JWT not valid: signature exception, " + compactJwt);
		} catch (ExpiredJwtException e) {
			isValid = false;
			logger.warn("JWT not valid: expired exception, " + compactJwt);
		}
		return isValid;
	}

	private boolean tokenIsExpired(Jws<Claims> claims) {
		Long expiredAt = (Long) claims.getBody().get(EXPIRATION_PAYLOAD_KEY);
		long now = System.currentTimeMillis();
		if (expiredAt != null && now >= expiredAt) {
			if(logger.isWarnEnabled()) {
				logger.warn("JWT is expired at: {} time checked: {}", new Date(expiredAt), new Date(now));
			}
			return true;
		} else {
			return false;
		}

	}
	private static long milisecOfTokenDuration() {
		long milisec = 0;
		try {
			milisec = Properties.getLong(PropertiesFilesNames.SECOSAN, SecosanConstants.PROPERTY_KEY_TOKEN_MAX_VALIDATION);
		} catch (PropertyException e) {
			milisec = DEFAULT_DURATION_TIME;
			logger.warn("Error while getting token duration, using default:"
					+ milisec, e);
		}

		return milisec;
	}

	public String renewToken(String compactJwt, Boolean... adminToken) {
		String jwtKey = this.getJwtKey(adminToken);
		Jws<Claims> claims = Jwts.parser().setSigningKey(jwtKey)
				.parseClaimsJws(compactJwt);
		return getJwtToken(claims.getBody(),adminToken);
	}

	public String getSubjectFromToken(String compactJwt, Boolean... adminToken) {
		String jwtKey = this.getJwtKey(adminToken);
		Jws<Claims> claims = Jwts.parser().setSigningKey(jwtKey)
				.parseClaimsJws(compactJwt);
		return claims.getBody().getSubject();
	}

	public long getRemaningValidationTime(String compactJwt, Boolean... adminToken) {
		logger.debug("token: {}", compactJwt);
		long remaningMilisec;
		try {
			String jwtKey = this.getJwtKey(adminToken);
			Jws<Claims> claims = Jwts.parser().setSigningKey(jwtKey)
					.parseClaimsJws(compactJwt);
			long expiration = (Long) claims.getBody().get(
					EXPIRATION_PAYLOAD_KEY);
			remaningMilisec = expiration - System.currentTimeMillis();

		} catch (Exception e) {
			remaningMilisec = 0;
		}
		return Math.abs(remaningMilisec);
	}
	
	public Object getClaimFromToken(String compactJwt, String claimKey, Boolean... adminToken) {
		String jwtKey = this.getJwtKey(adminToken);
		Jws<Claims> claims = Jwts.parser().setSigningKey(jwtKey)
				.parseClaimsJws(compactJwt);
		return claims.getBody().get(claimKey);
	}

}
