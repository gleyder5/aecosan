package es.grupoavalon.aecosan.secosan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.grupoavalon.aecosan.secosan.dao.MonitorizacionConectoresDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.MonitorizacionConectoresEntity;
import es.grupoavalon.aecosan.secosan.dao.exception.DaoException;
import es.grupoavalon.aecosan.secosan.dao.repository.MonitorizacionConectoresRepository;
import es.grupoavalon.aecosan.secosan.util.pagination.FilterParams;
import es.grupoavalon.aecosan.secosan.util.pagination.PaginationParams;

@Component
public class MonitorizacionConectoresDAOImpl extends GenericDao implements MonitorizacionConectoresDAO {

	private static final String ERROR = " ERROR: ";

	@Autowired
	private MonitorizacionConectoresRepository monitorizacionConectoresRepository;

	@Override
	public MonitorizacionConectoresEntity registerNewConnectorActivity(MonitorizacionConectoresEntity monitorEntity) {
		try {
			monitorizacionConectoresRepository.save(monitorEntity);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " registerNewConnectorActivity | ERROR: " + monitorEntity);
		}
		return monitorEntity;
	}

	@Override
	public List<MonitorizacionConectoresEntity> getListMonitoringConnector(PaginationParams paginationParams) {
		List<MonitorizacionConectoresEntity> results = new ArrayList<MonitorizacionConectoresEntity>();
		try {
			results = monitorizacionConectoresRepository.getListMonitoringConnector(paginationParams);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getListMonitoringConnector |" + paginationParams + ERROR + e.getMessage());
		}

		return results;
	}

	@Override
	public int getTotalListMonitoringConnector(List<FilterParams> filters) {
		int total = 0;
		try {
			total = monitorizacionConectoresRepository.getTotalListMonitoringConnector(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + " getTotalListMonitoringConnector | " + filters + ERROR + e.getMessage());
		}
		return total;
	}

	@Override
	public int getFilteredListMonitoringConnector(List<FilterParams> filters) {
		int totalFiltered = 0;
		try {
			totalFiltered = monitorizacionConectoresRepository.getFilteredListMonitoringConnector(filters);
		} catch (Exception e) {
			handleException(e, DaoException.CRUD_OPERATION_ERROR + "getFilteredListMonitoringConnector |" + filters + "|" + ERROR + e.getMessage());
		}
		return totalFiltered;
	}

}
