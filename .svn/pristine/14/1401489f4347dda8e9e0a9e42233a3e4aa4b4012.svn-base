package es.grupoavalon.aecosan.secosan.util.scheduled.jobs;

import es.grupoavalon.aecosan.secosan.dao.BecasDAO;
import es.grupoavalon.aecosan.secosan.dao.EstadoSolicitudDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.becas.BecasEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.solicitud.EstadoSolicitudEntity;
import es.grupoavalon.aecosan.secosan.dao.repository.EstadoSolicitudRepository;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SolicitudeStatusSubsanar10Dias extends QuartzJobBean {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void executeInternal(org.quartz.JobExecutionContext context) throws JobExecutionException {

        ApplicationContext springContext =
                WebApplicationContextUtils.getWebApplicationContext(
                        ContextLoaderListener.getCurrentWebApplicationContext().getServletContext()
                );
        BecasDAO becasDao = (BecasDAO) springContext.getBean("becasDAO");
        EstadoSolicitudDAO estadoSolicitudDAO = (EstadoSolicitudDAO) springContext.getBean("estadoSolicitudDAO");

        EstadoSolicitudEntity estadoSolicitudEntity = estadoSolicitudDAO.findEstadoSolicitud(Long.valueOf(18));
        List<BecasEntity> becasEntityList = becasDao.getBecasByStatusAndDate(estadoSolicitudEntity.getId(), new LocalDate());
        if(becasEntityList.size()>0){
            logger.info("Se han encontrado Solicitudes de  Becas pendiente por subsanar con 10 dias de limite.");
            for (BecasEntity becasEntity : becasEntityList) {
                EstadoSolicitudEntity estadoSolicitudEntity1 = estadoSolicitudDAO.findEstadoSolicitud(Long.valueOf(1));
                if(getWorkingDaysBetweenTwoDates(becasEntity.getFechaCreacion(),new Date())>=10) {
                    becasEntity.setStatus(estadoSolicitudEntity1);
                    becasDao.updateBecasEntity(becasEntity);
                }
            }
        }
    }
    private static int getWorkingDaysBetweenTwoDates(Date startDate, Date endDate) {
        Calendar startCal = Calendar.getInstance();
        startCal.setTime(startDate);

        Calendar endCal = Calendar.getInstance();
        endCal.setTime(endDate);

        int workDays = 0;

        //Return 0 if start and end are the same
        if (startCal.getTimeInMillis() == endCal.getTimeInMillis()) {
            return 0;
        }

        if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
            startCal.setTime(endDate);
            endCal.setTime(startDate);
        }
        do {
            //excluding start date
            startCal.add(Calendar.DAY_OF_MONTH, 1);
            if (startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && startCal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                ++workDays;
            }
        } while (startCal.getTimeInMillis() < endCal.getTimeInMillis()); //excluding end date

        return workDays;
    }
}
