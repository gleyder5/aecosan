package es.grupoavalon.aecosan.secosan.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.grupoavalon.aecosan.secosan.business.ActivityRegistry;
import es.grupoavalon.aecosan.secosan.dao.RegistroActividadDAO;
import es.grupoavalon.aecosan.secosan.dao.UsuarioDAO;
import es.grupoavalon.aecosan.secosan.dao.entity.AccionesEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.LoginUser;
import es.grupoavalon.aecosan.secosan.dao.entity.RegistroActividadEntity;
import es.grupoavalon.aecosan.secosan.dao.entity.UsuarioEntity;
import es.grupoavalon.aecosan.secosan.util.SecosanActivities;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = { Exception.class })
public class ActivityRegistryServiceImpl extends AbstractManager implements ActivityRegistry {

	@Autowired
	private RegistroActividadDAO activityRegistryDao;

	@Autowired
	private UsuarioDAO userDao;

	@Override
	public void registryLoginBasedActivity(String login, SecosanActivities activity) {
		this.logger.info("usuario:[{}] actividad: [{}]",login,activity.getDescription());
		try {
			RegistroActividadEntity activityRegistry = instanciateRegistroActividadForLoginBasedActivity(login, activity);
			this.activityRegistryDao.registerActivity(activityRegistry);
		} catch (Exception e) {
			this.logger.error("Can not registry the activity due an error:" + e.getMessage(), e);
		}
	}

	private RegistroActividadEntity instanciateRegistroActividadForLoginBasedActivity(String login, SecosanActivities activity) {
		LoginUser user = this.userDao.findUserByLogin(login);

		AccionesEntity actionEntity = instanciateAccionesEntityBySecosabnActivities(activity);

		RegistroActividadEntity registroActividad = new RegistroActividadEntity();
		registroActividad.setComentary(activity.getDescription());
		registroActividad.setAction(actionEntity);
		registroActividad.setUser((UsuarioEntity) user);

		return registroActividad;

	}

	@Override
	public void registryLoginClaveBasedActivity(String identificationDoc, SecosanActivities activity) {
		this.logger.info("usuario:[ {} ] actividad: [{}]",identificationDoc,activity.getDescription());
		try {
			AccionesEntity actionEntity = instanciateAccionesEntityBySecosabnActivities(activity);
			RegistroActividadEntity registroActividad = new RegistroActividadEntity();
			registroActividad.setComentary(identificationDoc);
			registroActividad.setAction(actionEntity);
			this.activityRegistryDao.registerActivity(registroActividad);
		} catch (Exception e) {
			this.logger.error("Can not registry the activity due an error:" + e.getMessage(), e);
		}

	}

	private static AccionesEntity instanciateAccionesEntityBySecosabnActivities(SecosanActivities activity) {
		AccionesEntity actionEntity = new AccionesEntity();
		actionEntity.setId(activity.getId());
		return actionEntity;
	}


}
